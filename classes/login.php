<?php
/**
 * @author Kerjin <kerjin@rakanoth.com>
 * @copyright 2015-2020 Rakanoth Sdn Bhd
 */

namespace Classes;


class login extends module {

    private $action,$username,$password,$salt,$pushID,$deviceID,$objAccount, $token, $HTTPClient, $hash, $tenantID;
    
    /*
    * @author Kerjin
    * @since 1.0.0 contructior
    * @param mixed[] $arrObjects Mixed array param
    * @param string $action todo action
    * */
	function __construct($arrObjects) {
		parent::__construct($arrObjects);
		parent::log(__CLASS__.' Invoked.',2);
		$this->HTTPClient = $arrObjects[HTTPCLIENT];
		
		try {

			$request 		= $arrObjects[HTTP_REQUEST];
			$header 		= $arrObjects[HTTP_HEADER];
			$this->username = isset($header->username) ? $header->username : die(array('Message'=>'Username Required'));
			$this->password = isset($header->password) ? $header->password : die(array('Message'=>'Password Required'));
			$this->salt 	= base64_decode($header->salt); 
			$this->action 	= $request->action;
			//$this->pushID 	= $request->pushID;
			//$this->deviceID = $request->deviceID;
			
		} catch (Exception $err){
			parent::log(__METHOD__.'Error Getting Params Error: '.$err->getTraceAsString(),2);
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
					RESPONSE_MESSAGE=>ERROR_INVALID_PARAMETER_MESSAGE));
			exit();
		}
		
		$this->run();
	}
	
	
	function run(){
		parent::log(__METHOD__,2);
		
		if( $this->login() ){
			
			//if( $this->isLogin() ){
				
				$checkImei = \ShAccountQuery::checkImei($this->deviceID, $this->username);
				$updateImei="";
				if(isset($checkImei->getDeviceID)){
					$updateImei = \ShAccountQuery::updateImei($checkImei->getID, '');
				}
				//push notification
				$formatImei = \ShAccountQuery::getImeiByUsername($this->username);
				if($this->deviceID != $formatImei){
					$msgBody	= "<Logout>";
					$arrPost = array('MSG_ID'=>md5("rakanoth".uniqid()),
								'TYPE'=>'18',
								'SUB_TYPE'=>'FORCE_LOGOUT',
								'CMD'=>'LOGOUT <'.$formatImei.'>',
								'TIME'=>time()
								);
					//\MessageQuery::pushNotif($formatImei,$msgBody, $arrPost, false, 1);
					//end push notification
					
					//\ShAccountQuery::updateImei($this->username, $this->deviceID);
					//exit();
				}
			//} 
						
			if( !$this->generateToken() ){
				parent::log('['.__METHOD__.'] Unable to generate token',1);
				echo json_encode(array(RESPONSE_CODE=>ERROR_ACCESS_DENIED,
						RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
				exit();
			} 
			
			if( !$this->insertTokenToDatabase() ){
				parent::log('['.__METHOD__.'] Unable to insert token to database',1);
				echo json_encode(array(RESPONSE_CODE=>ERROR_ACCESS_DENIED,
						RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
				exit();
			}
			
			//$this->responseAppInfo();
			
		} else {
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
					RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
			exit();
		}
	}
	
	private function responseAppInfo(){
		
		include 'project-cfg/config.php';
		exit();
	}
	
	private function generateToken(){
		parent::log('['.__METHOD__.'] Invoked',2);
		
		$raw_token = str_replace('[MD5_DATE]', md5( date("l jS \of F Y h:i:s A") ), TOKEN_FORMAT);
		$raw_token = str_replace('[DATE]', date("Y-m-d h:i:s"), $raw_token);
		$this->token = strtoupper(hash('sha256', $raw_token));
		
		parent::log('['.__METHOD__.'] Token: '.$this->token,2);
		if( isset($this->token) && strlen($this->token) == 64 )
			return true;
		else
			return false;
	}
	
	private function insertTokenToDatabase(){
		parent::log('['.__METHOD__.'] Invoked',2);
		$this->hash = hash_pbkdf2("sha256", $this->token, $this->salt, HASH_TOKEN_ITERATION, HASH_LENGTH, true);
		$token = \ShAccountQuery::updateToken($this->username, base64_encode($this->hash), date("Y-m-d h:i:s"), date("Y-m-d h:i:s"));
		
		$return = array();
		$return['Status']	= RESPONSE_STATUS_TRUE;
		$return['Token'] 	= $token->getToken();
		$return['Expired'] 	= date('Y-m-d H:i:s',strtotime($token->getExpireTs()->format('Y-m-d H:i:s').' +1 Month'));
		$return['Availability'] 	= $token->getStatus();
		echo json_encode($return);
		
		return true;
	}
	
	private function login(){
		parent::log('['.__METHOD__.'] Invoked',2);
		
		$salt = \ShAccountQuery::getSaltByUsername($this->username);
		parent::log(__METHOD__.' Username = '.$this->username,2);
		parent::log(__METHOD__.' Salt = '.$salt,2);
		if( isset($salt) ){
			$hashPassword = hash_pbkdf2("sha256", $this->password, base64_decode($salt), 7890, 32,true);
			$this->objAccount = \ShAccountQuery::getAccountByUsernamePassword($this->username,base64_encode($hashPassword));
			parent::log(__METHOD__.' Hash = '.$this->password,2);
			
			if( !is_a($this->objAccount,'ShAccount') ){
				echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
						RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
				exit();
			}
			
			return true;
		} else {
			return false;
		}
	}
	
}

?>
