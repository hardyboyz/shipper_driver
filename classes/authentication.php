<?php

/**
 * @author Hardyboyz <hardi84@gmail.com>
 */

namespace Classes;

class authentication extends module {
	
	public $isValidUser;
	private $headers;
	private $accInfo;
	
	function __construct($monolog,$headers) {
		$arrObjects[MONOLOG] = $monolog;
		parent::__construct($arrObjects);
		parent::log(__CLASS__.' Invoked.');
		$this->isValidUser = false;
		
		$this->headers = $headers;
		$this->identify();
	}

	private function identify(){
		parent::log('Identify Invoked.');
		if( isset($this->headers['token']) ){
			$this->verifyToken();
		} else {
			$this->isValidUser = false;
		}
	}	
	
	private function verifyToken(){
		
		/*$rawdata = base64_decode($this->headers['token']);
		$rawdata = substr($rawdata, 16, strlen($rawdata)-20);
		$arr = explode($this->headers['username'], $rawdata);

		if(is_array($arr) && sizeof($arr) == 2){
			$token = $arr[0];
			$salt = $arr[1];
		} else {
			$this->isValidUser = false;
			parent::log(__METHOD__.' Invalid Token',2);
			return;
		}
		
		$utf8Length = mb_strlen($token, 'utf-8');
		$token = substr($token, 0, $utf8Length);
		$hash = hash_pbkdf2("sha256", $token, $salt, HASH_TOKEN_ITERATION, HASH_LENGTH, true);
		
		$objAccount = \ShAccountQuery::getAccountByUsernameToken($this->headers['username'],base64_encode($hash));
		*/
		
		$objAccount = \ShAccountQuery::getAccountByUsernameToken($this->headers['username'],base64_decode($this->headers['token']));
		
		//echo base64_decode($this->headers['token']);
		
		if( isset($objAccount) && is_a($objAccount,'ShAccount') )
		{
			$this->isValidUser = true;
			$this->accInfo = $objAccount;
			parent::log(__METHOD__.': valid user.');
		} 
		else {
			$this->isValidUser = false;
			parent::log(__METHOD__.': invalid user.');
		}
	}
	
	function byPassedAuthentication(){
		parent::log('['.__METHOD__.'] Invoked.',2);
		global $_SERVER;
		$this->accInfo = array();
		
		// logic to be added
		parent::log('['.__METHOD__.'] REMOTE_ADDR: '.$_SERVER['REMOTE_ADDR'],2);
		if($_SERVER['REMOTE_ADDR'] == '192.168.1.1' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1'){
			$this->isValidUser = true;
		} else {
			$this->isValidUser = false;
		}
	}
	
	function responseAccessDenied(){
		echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
				RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
	}
	
	/*
	* @author Kerjin
	* @since 1.0.0 getAccInfo()
	* @return ShUser
	* */
	function getAccInfo(){
		parent::log(__METHOD__.' Invoked.',2);
		return $this->accInfo;
	}
}


?>
