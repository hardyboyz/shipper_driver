<?php
/**
 * @author Hardi <hardi84@gmail.com>
 */

namespace Classes;

class module {
	
	private $log;
	
	function __construct($arrObjects) {
		$this->log = $arrObjects[MONOLOG];
	}

	function log($message,$type=0){
		switch ($type){
			case 0:
			  $this->log->addInfo($message);
			break;
			case 1:
				$this->log->addError($message);
			break;
			case 2:
				$this->log->addDebug($message);
				break;
			default:
				$this->log->addInfo($message);
		}
	}
}

?>