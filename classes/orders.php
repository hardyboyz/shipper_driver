<?php

/**
 * @author hardi <hardi84@gmail.com>
 */

namespace Classes;

class orders extends module {

	private $XMLReader,$HTTPClient, $accInfo;	
	
	function __construct($arrObjects) {
		parent::__construct($arrObjects);
		parent::log('['.__CLASS__.'] Invoked.');
		$this->HTTPClient = $arrObjects[HTTPCLIENT];
		
		try {
			$this->request 		= $arrObjects[HTTP_REQUEST];
			$this->header 		= $arrObjects[HTTP_HEADER];
				
		} catch (Exception $err){
			parent::log(__METHOD__.'Error Getting Params Error: '.$err->getTraceAsString(),1);
			echo json_encode(array(RESPONSE_CODE=>ERROR_INVALID_PARAMETER,
					RESPONSE_MESSAGE=>ERROR_INVALID_PARAMETER_MESSAGE));
			exit();
		}
	}
	
	function getOrder()
	{	
		try{
			$this->request->method = 'GET';
			$drivers 	= $this->curl_get_contents(URL_ORDER, $this->request);
			//print_r($this->request);
			echo $drivers;
			
			/*$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
						RESPONSE_MESSAGE=>ERROR_SUCCESS_MESSAGE, 'Data'=>$return);
			echo json_encode($data);*/
		}catch (\Exception $e){
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_ERROR, RESPONSE_MESSAGE=>$e->getMessage()));
		}
		exit();
	}
	
	function getOrderById($id)
	{	
		$this->request->method = 'GET';
		$drivers 	= $this->curl_get_contents(URL_ORDER.'/'.$id);
		echo $drivers;
		
		/*$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
					RESPONSE_MESSAGE=>ERROR_SUCCESS_MESSAGE, 'Data'=>$return);
		echo json_encode($data);*/
		exit();
	}
	
	function updateBarcodeNumber()
	{	
		$this->request->method 	= 'PUT';
		$data 					= array('orderId'=>$post->orderId,'barcodeNumber'=>$post->barcodeNumber);
		$params 				= json_encode($data);
		$drivers 				= $this->curl_get_contents(URL_ORDER,$this->request, $params);
		echo $drivers;
		
		/*$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
					RESPONSE_MESSAGE=>ERROR_SUCCESS_MESSAGE, 'Data'=>$return);
		echo json_encode($data);*/
		exit();
	}
	
	function curl_get_contents($url, $post, $params=null)
	{
		try{
			$setHeader = array('User-Agent: Shipper/',
								'shipper-csrf-token: '.TOKEN_BACKEND,
								'Content-Type: application/json',
								'Content-Length: ' . strlen($params)
								);

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HTTPHEADER,$setHeader);
			
			strtoupper($post->method)=='POST' ? curl_setopt($ch, CURLOPT_POST, 1) : curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $post->method);
			
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			
			curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$data = curl_exec($ch);
			curl_close($ch);
		  return $data;
		} catch (\Exception $e){
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_ERROR, RESPONSE_MESSAGE=>$e->getMessage()));
		}
	}
	
}


?>
