<?php

/**
 * @author hardi <hardi84@gmail.com>
 */

namespace Classes;

class drivers extends module {

	private $XMLReader,$HTTPClient, $accInfo;
	private $request, $data; // param add here Eg: panelID,action,resType
	
	
	function __construct($arrObjects) {
		parent::__construct($arrObjects);
		parent::log('['.__CLASS__.'] Invoked.');
		$this->HTTPClient = $arrObjects[HTTPCLIENT];
		
		try {
			$this->request 		= $arrObjects[HTTP_REQUEST];
			$this->header 		= $arrObjects[HTTP_HEADER];
				
		} catch (Exception $err){
			parent::log(__METHOD__.'Error Getting Params Error: '.$err->getTraceAsString(),1);
			echo json_encode(array(RESPONSE_CODE=>ERROR_INVALID_PARAMETER,
					RESPONSE_MESSAGE=>ERROR_INVALID_PARAMETER_MESSAGE));
			exit();
		}
		
	}	
	
	function getDriverList()
	{	
		$drivers 	= \ShAccountQuery::getDriverList();
		$return = array();
		foreach($drivers as $q => $v){
			$return[$q]['Id'] 			= $v['ID'];
			$return[$q]['Fullname'] 	= $v['FullName'];
			$return[$q]['Phone'] 		= $v['Phone'];
			$return[$q]['Email'] 		= $v['Email'];
			$return[$q]['Address'] 		= $v['Address'];
			$return[$q]['IdcardNumber'] = $v['IdcardNumber'];
		}
		
		$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
					RESPONSE_MESSAGE=>RESPONSE_STATUS_TRUE, 'Data'=>$return);
		echo json_encode($data);
		exit();
	}
	
	function getDriver($id)
	{	
		$drivers 	= \ShAccountQuery::getDriver($id);
		$return = array();
		if($drivers != false){
			//foreach($drivers as $q => $v){
				$return['Id'] 			= $drivers['ID'];
				$return['Fullname'] 	= $drivers['FullName'];
				$return['Phone'] 		= $drivers['Phone'];
				$return['Email'] 		= $drivers['Email'];
				$return['Address'] 		= $drivers['Address'];
				$return['IdcardNumber'] = $drivers['IdcardNumber'];
			//}
			$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
					RESPONSE_MESSAGE=>RESPONSE_STATUS_TRUE, 'Data'=>$return);
		}else{
			$data = array(RESPONSE_CODE=>1000,
					RESPONSE_MESSAGE=>"Driver Not Found.");
		}
		
		echo json_encode($data);
		exit();
	}
	
	function insertPosition()
	{	
		$drivers 	= \DriversPositionQuery::insertPosition($this->request, $this->header);
		
		switch ($drivers) {
			case true:
			$data = array(RESPONSE_CODE=>RESPONSE_STATUS_TRUE);
			break;
			
			case false:
			$message = isset($this->request->latitude) ? "Param `latitude` OK" : "Param `latitude` not found.\n";
			$message .= isset($this->request->longitude) ? "Param `longitude` OK" : "Param `longitude` not found.";
			$data = array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL, REPONSE_MESSAGE=>$message);
			break;
			
			default:
			$data = array(RESPONSE_CODE=>RESPONSE_STATUS_ERROR,RESPONSE_MESSAGE=>$drivers);
		}
		
		echo json_encode($data);
	}
	
	function updateAvailability()
	{	
		$drivers 	= \ShAccountQuery::updateAvailability($this->request, $this->header);
		//$return = $drivers->toArray();
		
		if($drivers == true){		
			$data = array(RESPONSE_CODE=>RESPONSE_STATUS_TRUE);
		}
		else if($drivers == false){
			$message = isset($this->request->status) ? "Param `status` OK" : "Param `status` not found.";
			$data = array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL, RESPONSE_MESSAGE=>$message);
		}else{
			$data = array(RESPONSE_CODE=>RESPONSE_STATUS_ERROR,RESPONSE_MESSAGE=>$drivers);
		}
		
		echo json_encode($data);
		
	}
	
	function getSticker($data)
	{	
		try{
			$sticker 	= file_get_contents(URL_ORDER);
			echo $sticker;
			
			/*$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
						RESPONSE_MESSAGE=>ERROR_SUCCESS_MESSAGE, 'Data'=>$return);
			echo json_encode($data);*/
		}catch (\Exception $e){
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_ERROR, RESPONSE_MESSAGE=>$e->getMessage()));
		}
		exit();
	}
}


?>
