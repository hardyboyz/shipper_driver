<?php
/**
 * @author Kerjin <kerjin@rakanoth.com>
 * @copyright 2015-2020 Rakanoth Sdn Bhd
 */

namespace Classes;


class account extends module {

    private $action,$username,$password,$salt,$pushID,$deviceID,$objAccount, $token, $HTTPClient, $hash, $enable;
    
    /*
    * @author Kerjin
    * @since 1.0.0 contructior
    * @param mixed[] $arrObjects Mixed array param
    * @param string $action todo action
    * */
	function __construct($arrObjects) {
		parent::__construct($arrObjects);
		parent::log(__CLASS__.' Invoked.',2);
		$this->HTTPClient = $arrObjects[HTTPCLIENT];
		
		try {

			$request = $arrObjects[HTTP_REQUEST];
			$header = $arrObjects[HTTP_HEADER];
			$this->username = $header[PARAM_USERNAME];
			$this->password = base64_decode($header[PARAM_PASSWORD]);
			$this->salt = base64_decode($header[PARAM_SALT]); 
			$this->action = $request->action;
			$this->pushID = isset($request->pushID) ? $request->pushID : "";
			$this->deviceID = $request->deviceID;
			$this->enable = isset($request->enable) ? $request->enable : 0;
			
		} catch (Exception $err){
			parent::log(__METHOD__.'Error Getting Params Error: '.$err->getTraceAsString(),2);
			echo json_encode(array(RESPONSE_CODE=>ERROR_INVALID_PARAMETER,
					RESPONSE_MESSAGE=>ERROR_INVALID_PARAMETER_MESSAGE));
			exit();
		}
		
		$this->run();
	}
	
	
	function run(){
		parent::log(__METHOD__,2);
		
		if( $this->login() ){
						
			if( !$this->generateToken() ){
				parent::log('['.__METHOD__.'] Unable to generate token',1);
				echo json_encode(array(RESPONSE_CODE=>ERROR_ACCESS_DENIED,
						RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
				exit();
			} 
			
			if( !$this->insertTokenToDatabase() ){
				parent::log('['.__METHOD__.'] Unable to insert token to database',1);
				echo json_encode(array(RESPONSE_CODE=>ERROR_ACCESS_DENIED,
						RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
				exit();
			}
			
			if( !$this->updateTokenToLocalServer() ){
				parent::log('['.__METHOD__.'] Unable to update token to local server',1);
				echo json_encode(array(RESPONSE_CODE=>ERROR_ACCESS_DENIED,
						RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
				exit();
			} 
			
			$this->responseAppInfo();
			
		} else {
			echo json_encode(array(RESPONSE_CODE=>ERROR_ACCESS_DENIED,
					RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
			exit();
		}
	}
	
	private function responseAppInfo(){
		
		parent::log('['.__METHOD__.'] Invoked',2);
		
		
		$tenantID = substr($this->username,3, strlen($this->username) );
		$tenantID= substr($tenantID, 0, strlen($tenantID)-2);
		$panelList = \PanelQuery::getPanelByTenantID($tenantID);
		parent::log('['.__METHOD__.'] tenantID = '. $tenantID,2);
		
		//$panelList = \PanelQuery::getPanelByTenantID(substr($this->username,0,9));

		// to be remove
		/*$panelList = array(
			array('id'=>'1','IP'=>'192.168.1.42','type'=>'1','protocol'=>'udp','port'=>'9899'),
			array('id'=>'2','IP'=>'192.168.1.41','type'=>'1','protocol'=>'udp','port'=>'9899')
		);*/
		
		include 'project-cfg/'.substr($this->username,0,3).'.php';
		exit();
	}
	
	private function generateToken(){
		parent::log('['.__METHOD__.'] Invoked',2);
		
		$raw_token = str_replace('[MD5_DATE]', md5( date("l jS \of F Y h:i:s A") ), TOKEN_FORMAT);
		$raw_token = str_replace('[DATE]', date("Y-m-d h:i:s"), $raw_token);
		$this->token = strtoupper(hash('sha256', $raw_token));
		
		parent::log('['.__METHOD__.'] Token: '.$this->token,2);
		if( isset($this->token) && strlen($this->token) == 64 )
			return true;
		else
			return false;
	}
	
	private function insertTokenToDatabase(){
		parent::log('['.__METHOD__.'] Invoked',2);
		$this->hash = hash_pbkdf2("sha256", $this->token, $this->salt, HASH_TOKEN_ITERATION, HASH_LENGTH, true);
		\ShAccountQuery::updateToken($this->username, base64_encode($this->hash), date("Y-m-d h:i:s"), date("Y-m-d h:i:s"));
		return true;
	}
	
	private function updateTokenToLocalServer(){
		parent::log('['.__METHOD__.'] Invoked',2);

		$objProject = \ShProjectQuery::getProjectByID( substr($this->username, 0,3) );
		
		if(isset($objProject) && is_a($objProject, 'ShProject'))
		{
			parent::log('['.__METHOD__.'] Project found.',2);
			
			$request = new \GuzzleHttp\Psr7\Request('POST', $objProject->getGatewayProtocol().'://'.$objProject->getGatewayURL().':'.$objProject->getGatewayPort(),
					[PARAM_USERNAME => $this->username,PARAM_PASSWORD => base64_encode($this->hash)]);
			
			$response = $this->HTTPClient->send($request);
			
			$res = json_decode($response->getBody());
			
			parent::log('['.__METHOD__.'] Local Server response body: '.$response->getBody(),2);
			
			if($res->Code == ERROR_SUCCESS){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	private function login(){
		parent::log('['.__METHOD__.'] Invoked',2);
		
		$salt = \ShAccountQuery::getSaltByUsername($this->username);
		parent::log(__METHOD__.' Username = '.$this->username,2);
		parent::log(__METHOD__.' Salt = '.$salt,2);
		if( isset($salt) ){
			$hashPassword = hash_pbkdf2("sha256", $this->password, base64_decode($salt), HASH_ITERATION, HASH_LENGTH,true);
			$this->objAccount = \ShAccountQuery::getAccountByUsernamePassword($this->username,base64_encode($hashPassword));
			
			if( !is_a($this->objAccount,'ShAccount') ){
				echo json_encode(array(RESPONSE_CODE=>ERROR_ACCESS_DENIED,
						RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
				exit();
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	private function updateImei(){
		\ShAccountQuery::updateImei($this->username, $this->deviceID);
		return true;
	}
	
	private function enablePN(){
		\ShAccountQuery::enablePN($this->username, $this->deviceID, $this->enable);
		return true;
	}
	
}

?>