<?php

/**
 * @author Kerjin <kerjin@rakanoth.com>
 * @copyright 2015-2020 Rakanoth Sdn Bhd
 */

namespace Classes;

class configuration extends module {

	private $XMLReader,$HTTPClient, $accInfo;
	private $projectID, $lastEbulletinID, $request, $userId; // param add here Eg: panelID,action,resType
	
	
	function __construct($arrObjects) {
		parent::__construct($arrObjects);
		parent::log('['.__CLASS__.'] Invoked.');
		$this->HTTPClient = $arrObjects[HTTPCLIENT];
		$this->accInfo = $arrObjects[ACCOUNT_INFO];
		
		try {
			$this->request 		= $arrObjects[HTTP_REQUEST];
			$this->header 		= $arrObjects[HTTP_HEADER];
			$vprojectID			= $this->header[PARAM_USERNAME];
			$this->projectID		= substr($vprojectID,0,3);
			$this->action 		= $this->request->action;
			$this->userId		= $this->header[PARAM_USERNAME];
			//print_r($header);
				
		} catch (Exception $err){
			parent::log(__METHOD__.'Error Getting Params Error: '.$err->getTraceAsString(),1);
			echo json_encode(array(RESPONSE_CODE=>ERROR_INVALID_PARAMETER,
					RESPONSE_MESSAGE=>ERROR_INVALID_PARAMETER_MESSAGE));
			exit();
		}
		
		$this->run();
	}

	function run()
	{	
		switch($this->action){
			case GET_COMMANDLIST:
						$this->commandList();
					break;
			case GET_MODULES:
						$this->modules();
					break;
			case GET_HOMEMODULES:
						$this->homeModules();
					break;
			case GET_SHORTMODULES:
						$this->shortModules();
					break;
			case GET_CCTV:
						$this->CCTV();
					break;
			case GET_PROJECTINFO:
						$this->getProjectInfo();
					break;
			case GET_DOMAINLIST:
						$this->getDomainList();
					break;
			case GET_INTERCOM:
						$this->getIntercom();
					break;
			case GET_LIFT:
						$this->getLift();
					break;
			default:
					parent::log('['.__METHOD__.'] Unable to get data',1);
							echo json_encode(array(RESPONSE_CODE=>ERROR_ACCESS_DENIED,
									RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
							break;
					
		}
	}	
	
	function commandList()
	{	
		$commandList 	= \CommandQuery::getCommandList();
		/*$arrCommandList	=  $commandList->toArray();
		$data			= array();
		foreach($arrCommandList as $q => $v){
			$data[$v['Action']] = $v['Command'];
		}

		print_r( $data );*/
		
		exit();
	}
	
	function modules()
	{	
		$modules 	= \AppMenuListQuery::getModules();
	}
	
	function homeModules()
	{	
		$modules 	= \AppMenuListQuery::getHomeModules();
	}
	
	function shortModules()
	{	
		$modules 	= \AppMenuListQuery::getShortModules();
	}
	
	function CCTV()
	{	
		$modules 	= \CommonAreaCameraQuery::getCCTV();
	}
	
	function getProjectInfo()
	{	
		$projectInfo 	= \PropertyProjectQuery::getProjectInfo($this->userId);
		//print_r($projectInfo);exit;
		$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
					RESPONSE_MESSAGE=>ERROR_SUCCESS_MESSAGE, 'Data'=>$projectInfo);
		echo json_encode($data);
	}
	
	function getDomainList()
	{	
		$projectInfo 	= \AppClientSettingQuery::getDomainList(substr($this->userId,0,3));
		//print_r($projectInfo);exit;
		$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
					RESPONSE_MESSAGE=>ERROR_SUCCESS_MESSAGE, 'Data'=>$projectInfo);
		echo json_encode($data);
	}
	
	function getIntercom()
	{	
		$projectInfo 	= \IntercomExtensionQuery::getIntercom(substr($this->userId,0,3));
		//print_r($projectInfo);exit;
		$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
					RESPONSE_MESSAGE=>ERROR_SUCCESS_MESSAGE, 'Data'=>$projectInfo);
		echo json_encode($data);
	}
	
	function getLift()
	{	
		$Lift 	= \LiftQuery::getLift(substr($this->userId,0,3));
		//print_r($projectInfo);exit;
		$data = array(RESPONSE_CODE=>ERROR_SUCCESS,
					RESPONSE_MESSAGE=>ERROR_SUCCESS_MESSAGE, 'Data'=>$Lift);
		echo json_encode($data);
	}
	
}


?>
