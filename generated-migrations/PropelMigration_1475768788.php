<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1475768788.
 * Generated on 2016-10-06 17:46:28 
 */
class PropelMigration_1475768788
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `customer`;

DROP TABLE IF EXISTS `migrations`;

DROP TABLE IF EXISTS `user`;

CREATE TABLE `userEventLog`
(
    `id` VARCHAR(15) NOT NULL,
    `projectId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(10),
    `actionType` VARCHAR(30),
    `description` VARCHAR(255),
    `eventDate` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET=\'utf8\' COLLATE=\'utf8_general_ci\';

CREATE TABLE `sh_account`
(
    `fullName` VARCHAR(50) NOT NULL,
    `hashPassword` VARCHAR(100) NOT NULL,
    `salt` VARCHAR(100) NOT NULL,
    `tenantId` VARCHAR(20) NOT NULL,
    `licenceKey` VARCHAR(20) NOT NULL,
    `created_by` VARCHAR(10) NOT NULL,
    `createTS` DATETIME NOT NULL,
    `pushID` VARCHAR(250),
    `deviceID` VARCHAR(100),
    `enable_pn` INTEGER DEFAULT 1 NOT NULL,
    `login_ts` DATETIME NOT NULL,
    `expire_ts` DATETIME NOT NULL,
    `token` VARCHAR(100) NOT NULL COMMENT \'Hash Value with client provided salt\'
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `userEventLog`;

DROP TABLE IF EXISTS `sh_account`;

CREATE TABLE `customer`
(
    `id` VARCHAR(25) NOT NULL,
    `lastname` VARCHAR(255),
    `firstname` VARCHAR(255),
    `phone` VARCHAR(35) NOT NULL,
    `address` TEXT,
    `email` VARCHAR(255) NOT NULL,
    `sex` VARCHAR(2) DEFAULT \'M\' NOT NULL,
    `active` TINYINT(1) DEFAULT 1 NOT NULL,
    `created_at` DATETIME DEFAULT \'0000-00-00 00:00:00\' NOT NULL,
    `updated_at` DATETIME DEFAULT \'0000-00-00 00:00:00\' NOT NULL,
    UNIQUE INDEX `customer_email_unique` (`email`)
) ENGINE=InnoDB;

CREATE TABLE `migrations`
(
    `migration` VARCHAR(255) NOT NULL,
    `batch` INTEGER NOT NULL
) ENGINE=InnoDB;

CREATE TABLE `user`
(
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `phone` VARCHAR(255),
    `lastname` VARCHAR(255),
    `firstname` VARCHAR(255),
    `email` VARCHAR(255) NOT NULL,
    `password` VARCHAR(60) NOT NULL,
    `active` TINYINT(1) DEFAULT 1 NOT NULL,
    `remember_token` VARCHAR(100),
    `last_login` DATETIME NOT NULL,
    `created_at` DATETIME DEFAULT \'0000-00-00 00:00:00\' NOT NULL,
    `updated_at` DATETIME DEFAULT \'0000-00-00 00:00:00\' NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `user_email_unique` (`email`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}