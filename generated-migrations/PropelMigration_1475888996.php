<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1475888996.
 * Generated on 2016-10-08 03:09:56 
 */
class PropelMigration_1475888996
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `order`;

DROP TABLE IF EXISTS `usereventlog`;

DROP TABLE IF EXISTS `userrole_module`;

CREATE TABLE `userRole_Module`
(
    `id` VARCHAR(15) NOT NULL,
    `userRoleId` VARCHAR(15) NOT NULL,
    `ModuleId` VARCHAR(15) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `userEventLog`
(
    `id` VARCHAR(15) NOT NULL,
    `projectId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(10),
    `actionType` VARCHAR(30),
    `description` VARCHAR(255),
    `eventDate` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET=\'utf8\' COLLATE=\'utf8_general_ci\';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `userRole_Module`;

DROP TABLE IF EXISTS `userEventLog`;

CREATE TABLE `order`
(
    `order_id` VARCHAR(20) NOT NULL,
    `driver_id` VARCHAR(20),
    `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`order_id`)
) ENGINE=InnoDB;

CREATE TABLE `usereventlog`
(
    `id` VARCHAR(15) NOT NULL,
    `projectId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(10),
    `actionType` VARCHAR(30),
    `description` VARCHAR(255),
    `eventDate` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `userrole_module`
(
    `id` VARCHAR(15) NOT NULL,
    `userRoleId` VARCHAR(15) NOT NULL,
    `ModuleId` VARCHAR(15) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}