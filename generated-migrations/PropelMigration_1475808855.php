<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1475808855.
 * Generated on 2016-10-07 04:54:15 
 */
class PropelMigration_1475808855
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `usereventlog`;

DROP TABLE IF EXISTS `userrole_module`;

ALTER TABLE `sh_account`

  ADD `phone` VARCHAR(50) NOT NULL AFTER `fullName`,

  ADD `email` VARCHAR(50) NOT NULL AFTER `phone`,

  ADD `address` VARCHAR(100) NOT NULL AFTER `email`,

  ADD `idcardnumber` VARCHAR(25) NOT NULL AFTER `address`;

CREATE TABLE `userRole_Module`
(
    `id` VARCHAR(15) NOT NULL,
    `userRoleId` VARCHAR(15) NOT NULL,
    `ModuleId` VARCHAR(15) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `userEventLog`
(
    `id` VARCHAR(15) NOT NULL,
    `projectId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(10),
    `actionType` VARCHAR(30),
    `description` VARCHAR(255),
    `eventDate` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET=\'utf8\' COLLATE=\'utf8_general_ci\';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `userRole_Module`;

DROP TABLE IF EXISTS `userEventLog`;

ALTER TABLE `sh_account`

  DROP `phone`,

  DROP `email`,

  DROP `address`,

  DROP `idcardnumber`;

CREATE TABLE `usereventlog`
(
    `id` VARCHAR(15) NOT NULL,
    `projectId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(10),
    `actionType` VARCHAR(30),
    `description` VARCHAR(255),
    `eventDate` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `userrole_module`
(
    `id` VARCHAR(15) NOT NULL,
    `userRoleId` VARCHAR(15) NOT NULL,
    `ModuleId` VARCHAR(15) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}