<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1475769036.
 * Generated on 2016-10-06 17:50:36 
 */
class PropelMigration_1475769036
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `usereventlog`;

CREATE TABLE `user_role`
(
    `id` VARCHAR(15) NOT NULL,
    `project_id` VARCHAR(4) NOT NULL,
    `name` VARCHAR(50) NOT NULL,
    `description` TEXT,
    `active` TINYINT(1) NOT NULL,
    `created_by` VARCHAR(10) NOT NULL,
    `created_date` DATETIME NOT NULL,
    `modified_by` VARCHAR(10),
    `modified_date` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `module`
(
    `id` VARCHAR(15) NOT NULL,
    `module` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `userRole_Module`
(
    `id` VARCHAR(15) NOT NULL,
    `userRoleId` VARCHAR(15) NOT NULL,
    `ModuleId` VARCHAR(15) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `user`
(
    `id` VARCHAR(15) NOT NULL,
    `project_id` VARCHAR(4) NOT NULL,
    `userRoleId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(30) NOT NULL,
    `password` VARCHAR(64) NOT NULL,
    `name` VARCHAR(50) NOT NULL,
    `email` VARCHAR(64),
    `dob` DATE,
    `active` TINYINT(1) NOT NULL,
    `created_by` VARCHAR(10) NOT NULL,
    `created_date` DATETIME NOT NULL,
    `modified_by` VARCHAR(10),
    `modified_date` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `userEventLog`
(
    `id` VARCHAR(15) NOT NULL,
    `projectId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(10),
    `actionType` VARCHAR(30),
    `description` VARCHAR(255),
    `eventDate` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET=\'utf8\' COLLATE=\'utf8_general_ci\';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `user_role`;

DROP TABLE IF EXISTS `module`;

DROP TABLE IF EXISTS `userRole_Module`;

DROP TABLE IF EXISTS `user`;

DROP TABLE IF EXISTS `userEventLog`;

CREATE TABLE `usereventlog`
(
    `id` VARCHAR(15) NOT NULL,
    `projectId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(10),
    `actionType` VARCHAR(30),
    `description` VARCHAR(255),
    `eventDate` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}