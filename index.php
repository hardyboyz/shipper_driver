<?php
use Classes\authentication;
use Classes\ebulletin;

/**
 * @author Hardyboyz <hardi84@gmail.com>
 */

require 'config/setting.php';
require 'vendor/autoload.php';
require 'config/init.php';

//unitTestingLogin();
//unitTestingTokenAuth();


$app->group('/'.API, function () use ($app,$monolog,$HTTPClient,$allPutVars,$httpHeaders) 
{	
	$app->get('/orders/:id', function ($id) use ($monolog,$HTTPClient,$allPutVars,$httpHeaders)
	{		
		$arrObjects[MONOLOG] = $monolog;
		$arrObjects[HTTPCLIENT] = $HTTPClient;
		$arrObjects[HTTP_REQUEST] = $allPutVars;
		$arrObjects[HTTP_HEADER] = $httpHeaders;
		
		$objAuth = new authentication($monolog, $httpHeaders);
		
		if($objAuth->isValidUser){
			
			$orders = new Classes\orders($arrObjects);
			$orders->getOrderById($id);
			
		}else{
			
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
									RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
		}
		
	});
	
	$app->get('/orders', function () use ($monolog,$HTTPClient,$allPutVars,$httpHeaders)
	{		
		$arrObjects[MONOLOG] = $monolog;
		$arrObjects[HTTPCLIENT] = $HTTPClient;
		$arrObjects[HTTP_REQUEST] = $allPutVars;
		$arrObjects[HTTP_HEADER] = $httpHeaders;
		$objAuth = new authentication($monolog, $httpHeaders);
		
		if($objAuth->isValidUser){
			
			$orders = new Classes\orders($arrObjects);
			$orders->getOrder();
			
		}else{
			
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
									RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
		}
		
	});
	
	$app->put('/orders', function () use ($monolog,$HTTPClient,$allPutVars,$httpHeaders)
	{		
		$arrObjects[MONOLOG] = $monolog;
		$arrObjects[HTTPCLIENT] = $HTTPClient;
		$arrObjects[HTTP_REQUEST] = $allPutVars;
		$arrObjects[HTTP_HEADER] = $httpHeaders;
		$objAuth = new authentication($monolog, $httpHeaders);
		
		if($objAuth->isValidUser){
			
			$orders = new Classes\orders($arrObjects);
			$orders->updateBarcodeNumber();
			
		}else{
			
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
									RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
		}
		
	});
	
	$app->get('/drivers', function () use ($monolog,$HTTPClient,$allPutVars,$httpHeaders)
	{
		$arrObjects[MONOLOG] = $monolog;
		$arrObjects[HTTPCLIENT] = $HTTPClient;
		$arrObjects[HTTP_REQUEST] = $allPutVars;
		$arrObjects[HTTP_HEADER] = $httpHeaders;
		$objAuth = new authentication($monolog, $httpHeaders);
		
		if($objAuth->isValidUser){
			
			$drivers = new Classes\drivers($arrObjects);
			$drivers->getDriverList();
			
		}else{
			
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
									RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
		}
		
	});
	
	$app->put('/drivers', function () use ($monolog,$HTTPClient,$allPutVars,$httpHeaders)
	{
		$arrObjects[MONOLOG] 		= $monolog;
		$arrObjects[HTTPCLIENT] 	= $HTTPClient;
		$arrObjects[HTTP_REQUEST] 	= $allPutVars;
		$arrObjects[HTTP_HEADER] 	= $httpHeaders;
		$objAuth = new authentication($monolog, $httpHeaders);
		
		if($objAuth->isValidUser){
			
			$drivers = new Classes\drivers($arrObjects);
			$drivers->updateAvailability();
			
		}else{
			
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
									RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
		}
		
	});
	
	$app->post('/drivers', function () use ($monolog,$HTTPClient,$allPutVars,$httpHeaders)
	{
		$arrObjects[MONOLOG] 		= $monolog;
		$arrObjects[HTTPCLIENT] 	= $HTTPClient;
		$arrObjects[HTTP_REQUEST] 	= $allPutVars;
		$arrObjects[HTTP_HEADER] 	= $httpHeaders;
		$objAuth = new authentication($monolog, $httpHeaders);
		
		if($objAuth->isValidUser){
			
			$drivers = new Classes\drivers($arrObjects);
			$drivers->insertPosition();
			
		}else{
			
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
									RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
		}
		
	});
	
	$app->get('/drivers/:id', function ($id) use ($monolog,$HTTPClient,$allPutVars,$httpHeaders)
	{
		$arrObjects[MONOLOG] 		= $monolog;
		$arrObjects[HTTPCLIENT] 	= $HTTPClient;
		$arrObjects[HTTP_REQUEST] 	= $allPutVars;
		$arrObjects[HTTP_HEADER] 	= $httpHeaders;
		$objAuth = new authentication($monolog, $httpHeaders);
		
		if($objAuth->isValidUser){
			
			$drivers = new Classes\drivers($arrObjects);
			$drivers->getDriver($id);
			
		}else{
			
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
									RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
		}
		
	});
	
	//API to Check Sticker
	$app->get('/drivers/:stickerId', function ($stickerId) use ($monolog,$HTTPClient,$allPutVars,$httpHeaders)
	{
		$arrObjects[MONOLOG] 		= $monolog;
		$arrObjects[HTTPCLIENT] 	= $HTTPClient;
		$arrObjects[HTTP_REQUEST] 	= $allPutVars;
		$arrObjects[HTTP_HEADER] 	= $httpHeaders;
		$objAuth = new authentication($monolog, $httpHeaders);
		
		if($objAuth->isValidUser){
			
			$drivers = new Classes\drivers($arrObjects);
			$drivers->getSticker($stickerId);
			
		}else{
			
			echo json_encode(array(RESPONSE_CODE=>RESPONSE_STATUS_FAIL,
									RESPONSE_MESSAGE=>ERROR_ACCESS_DENIED_MESSAGE));
		}
		
	});
	
	$app->put('/'.ACCOUNT_MOD, function () use ($monolog,$HTTPClient,$allPutVars,$httpHeaders)
	{	
		$arrObjects[MONOLOG] = $monolog;
		$arrObjects[HTTPCLIENT] = $HTTPClient;
		$arrObjects[HTTP_REQUEST] = $allPutVars;
		$arrObjects[HTTP_HEADER] = $httpHeaders;
		
		switch ($allPutVars->action)
		{
			case UPDATE_IMEI:
			case ENABLE_PN:
				new Classes\account($arrObjects);
				break;
			default:
				new Classes\module($arrObjects);
		}
	});
	
	$app->put('/'.AUTH_MOD, function () use ($monolog,$HTTPClient,$allPutVars,$httpHeaders) 
	{
		$arrObjects[MONOLOG] = $monolog;
		$arrObjects[HTTPCLIENT] = $HTTPClient;
		$arrObjects[HTTP_REQUEST] = $allPutVars;
		$arrObjects[HTTP_HEADER] = $httpHeaders;
		
		switch ($allPutVars->action)
		{
			case LOGIN:
				new Classes\login($arrObjects);
				break;
			default:
				new Classes\module($arrObjects);
		}
	});
	
});

$app->run();


?>
