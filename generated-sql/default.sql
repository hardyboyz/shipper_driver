
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- user_role
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role`
(
    `id` VARCHAR(15) NOT NULL,
    `project_id` VARCHAR(4) NOT NULL,
    `name` VARCHAR(50) NOT NULL,
    `description` TEXT,
    `active` TINYINT(1) NOT NULL,
    `created_by` VARCHAR(10) NOT NULL,
    `created_date` DATETIME NOT NULL,
    `modified_by` VARCHAR(10),
    `modified_date` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- module
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `module`;

CREATE TABLE `module`
(
    `id` VARCHAR(15) NOT NULL,
    `module` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- userRole_Module
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `userRole_Module`;

CREATE TABLE `userRole_Module`
(
    `id` VARCHAR(15) NOT NULL,
    `userRoleId` VARCHAR(15) NOT NULL,
    `ModuleId` VARCHAR(15) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `id` VARCHAR(15) NOT NULL,
    `userRoleId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(30) NOT NULL,
    `password` VARCHAR(64) NOT NULL,
    `name` VARCHAR(50) NOT NULL,
    `email` VARCHAR(64),
    `dob` DATE,
    `active` TINYINT(1) NOT NULL,
    `created_by` VARCHAR(10) NOT NULL,
    `created_date` DATETIME NOT NULL,
    `modified_by` VARCHAR(10),
    `modified_date` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- userEventLog
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `userEventLog`;

CREATE TABLE `userEventLog`
(
    `id` VARCHAR(15) NOT NULL,
    `projectId` VARCHAR(15) NOT NULL,
    `username` VARCHAR(10),
    `actionType` VARCHAR(30),
    `description` VARCHAR(255),
    `eventDate` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_general_ci';

-- ---------------------------------------------------------------------
-- drivers_position
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `drivers_position`;

CREATE TABLE `drivers_position`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `driver_id` VARCHAR(20),
    `latitude` VARCHAR(50),
    `longitude` VARCHAR(50),
    `created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_general_ci';

-- ---------------------------------------------------------------------
-- sh_account
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sh_account`;

CREATE TABLE `sh_account`
(
    `id` VARCHAR(20) NOT NULL COMMENT 'Only free varchar',
    `photo` VARCHAR(35) NOT NULL,
    `dob` VARCHAR(15) NOT NULL,
    `fullName` VARCHAR(50) NOT NULL,
    `phone` VARCHAR(50) NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `address` VARCHAR(100) NOT NULL,
    `idcardnumber` VARCHAR(25) NOT NULL,
    `hashPassword` VARCHAR(100) NOT NULL,
    `salt` VARCHAR(100) NOT NULL,
    `created_by` VARCHAR(10) NOT NULL,
    `createTS` DATETIME NOT NULL,
    `pushID` VARCHAR(250),
    `deviceID` VARCHAR(100),
    `enable_pn` INTEGER DEFAULT 1 NOT NULL,
    `login_ts` DATETIME NOT NULL,
    `expire_ts` DATETIME NOT NULL,
    `token` VARCHAR(100) NOT NULL COMMENT 'Hash Value with client provided salt',
    `status` INTEGER DEFAULT 1 NOT NULL COMMENT '0 = not available, 1=available, 2=disabled',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
