<?php
/**
 * @author Kerjin <kerjin@rakanoth.com>
 * @copyright 2015-2020 Rakanoth Sdn Bhd
 */


// Object Key
define('RESPONSE_CODE','Status');
define('RESPONSE_TITLE','Title');
define('RESPONSE_MESSAGE','Message');
define('RESPONSE_STATUS','Status');
define('RESPONSE_INFO','Info');

// Status Response
define('RESPONSE_STATUS_TRUE','success');
define('RESPONSE_STATUS_FAIL','fail');
define('RESPONSE_STATUS_ERROR','error');

?>
