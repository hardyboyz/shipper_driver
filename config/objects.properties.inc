<?php
/**
 * @author Kerjin <kerjin@rakanoth.com>
 * @copyright 2015-2020 Rakanoth Sdn Bhd
 */


// Object Key
define('MONOLOG','MONOLOG');
define('HTTPCLIENT','HTTPCLIENT');
define('XMLREADER','XMLREADER');
define('HTTP_REQUEST','HTTP_REQUEST');
define('SERVER_VAR','SERVER_VAR');
define('HTTP_HEADER','HTTP_HEADER');
define('UDP_SOCKET_FACTORY','UDP_SOCKET_FACTORY');
define('ACCOUNT_INFO','ACCOUNT_INFO');
?>