<?php

/**
 * @author HardyBoyz <hardi84@gmail.com>
 */

date_default_timezone_set('Asia/Jakarta');

define('PROJECT_NAME', 'Shipper Driver');
define('ENV_MODE', 'development');

// Path Configuration
define('CLASS_PATH', 'classes/');

// Log Configuration
define('LOG_PATH', 'log/SCS.log');
define('LOG_MAX_FILES', 10);
define('WEB_LOG_PATH', 'log/web.log');

define('TOKEN_BACKEND', 'AZRAX');
define('HASH_ITERATION', 7890);
define('HASH_TOKEN_ITERATION', 7373);
define('HASH_LENGTH', 32);
define('MASTER_SALT','5KweRM0i+i21r3Eq921zVVZAoTPGaO92n3O6lERmoK0=');
define('MASTER_TOKEN','0287ee25b1a1a1a1097d3aed9e57de892f65d75313d66942f81e51d93add9bf4');
define('TOKEN_FORMAT','[MD5_DATE]_RAKANOTH_SMARTHOME_[DATE]');

define('URL_ORDER','https://shipper.id/api/v1/order');

header('Content-type:application/json;charset=utf-8');

require 'config/action.properties.inc';
require 'config/objects.properties.inc';
require 'config/error.properties.inc';
require 'config/response.properties.inc';
require 'config/parameters.properties.inc';

?>