<?php

/**
 * @author Kerjin <kerjin@rakanoth.com>
 * @copyright 2015-2020 Rakanoth Sdn Bhd
 */

// Init Monolog
$monolog = new Monolog\Logger(PROJECT_NAME);
$monolog->pushHandler(new Monolog\Handler\RotatingFileHandler(LOG_PATH, LOG_MAX_FILES,Monolog\Logger::DEBUG));

// init SlimFramework
$slim = new \Slim\Slim();

// Init Slim Framework
$app = new \Slim\Slim(array(
		'mode' => ENV_MODE
));

// Only invoked if mode is "production"
$app->configureMode('production', function () use ($app) {
	$app->config(array(
			'log.enable' => true,
			'debug' => false
	));
});

// Only invoked if mode is "development"
$app->configureMode('development', function () use ($app) {
	$app->config(array(
			'log.enable' => false,
			'debug' => true
		));
	});
$app->setName(PROJECT_NAME);

// init HTTP Client
$HTTPClient = new GuzzleHttp\Client(['base_uri' => 'http://shipper.co.id','timeout'  => 100.0,]);

// Init XML Parser
$XMLReader = new Sabre\Xml\Reader();

// get all PUT requests
$temp = $app->request->put();
//print_r($temp);
/*$allPutVars = "";
foreach($temp as $k=>$v){
	$allPutVars = json_decode($k);
}*/

$allPutVars = (object) $temp;
//print_r($allPutVars);

// get all headers
$httpHeaders = $app->request->headers;

require CLASS_PATH.'module.php';
require CLASS_PATH.'login.php';
require CLASS_PATH.'authentication.php';
require CLASS_PATH.'configuration.php';
require CLASS_PATH.'account.php';
require CLASS_PATH.'drivers.php';
require CLASS_PATH.'orders.php';
require CLASS_PATH.'RestCurl.php';
// setup Propel
require_once 'generated-conf/config.php';

?>
