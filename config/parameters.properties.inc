<?php
/**
 * @author Kerjin <kerjin@rakanoth.com>
 * @copyright 2015-2020 Rakanoth Sdn Bhd
 */


// Object Key
define('PARAM_TOKEN','X-RAKAN-TOKEN');
define('PARAM_USERNAME','X-RAKAN-ID');
define('PARAM_PASSWORD','X-RAKAN-KEY');
define('PARAM_SALT','X-RAKAN-SA');

?>