<?php
//Author : HardyBoyz


// Error Code
define('ERROR_SUCCESS',"success");
define('ERROR_FAILED',"fail");

define('ERROR_ACCESS_DENIED','9011');
define('ERROR_INVALID_PARAMETER','9001');
define('ERROR_NO_PARAMETER','9021');
define('ERROR_GENERAL_ERROR','9100');

// Error Message
define('ERROR_SUCCESS_MESSAGE','status');
define('ERROR_FAILED_MESSAGE','Failed');
define('ERROR_ACCESS_DENIED_MESSAGE','Access Denied');
define('ERROR_INVALID_PARAMETER_MESSAGE','Invalid passing parameter');
define('ERROR_NO_PARAMETER_MESSAGE','Parameter required');
define('ERROR_GENERAL_ERROR_MESSAGE','Unknown Error. Please try again later');



?>
