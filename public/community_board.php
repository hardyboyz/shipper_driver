<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php
session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';
$username = '';
$name = '';
$email = '';
$dob = '';
$errName = '';

$apiCommBoard 	= $baseUrl."/api/community";
$projectId 		= $_SESSION['projectId'];
$userId 		= $_SESSION['username'];
$expiryDate		= date("Y-m-d", strtotime("+3 days", strtotime(date("Y-m-d"))));

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$sid = isset($_REQUEST['sid']) ? $_REQUEST['sid'] : '';
	$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
	$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
	$dob = isset($_REQUEST['dob']) ? $_REQUEST['dob'] : '';

	$hasError = false;
	
	if ($name == '') {
		$hasError = true;
		$errName = 'Please enter the name.';
	}
	
	if (!$hasError) {
		$userDetails = UserQuery::create()->findPK($_SESSION['userId']);
		$userDetails->setName($name);
		$userDetails->setEmail($email);
		$userDetails->setDob(substr($dob, -4).'-'.substr($dob, 3, 2).'-'.substr($dob, 0, 2));
		$userDetails->save();		
	}
}

if(isset($_GET['action'])){
	if($_GET['action'] == 'removeRoom'){
		$id = (int) $_GET['id'];
		ChatRoomQuery::create()->filterById($id)->update(array('Status'=>0));
	}
	
	if($_GET['action'] == 'editRoom'){
		$id = (int) $_GET['id'];
		$topic = mysqli_real_escape_string($_GET['topic']);
		ChatRoomQuery::create()->filterById($id)->update(array('Title'=>$topic));
	}
	
	echo "<script>window.location='community_board.php';</script>";
}

$userDetails = UserQuery::create()->findPK($_SESSION['userId']);
if (count($userDetails) == 0) {
	session_destroy();		
	header("Location: ./login.php");
	die();
} else {
	$username = $userDetails->getUsername();
	$name = $userDetails->getName();
	$email = $userDetails->getEmail();
	//$dob = $userDetails->getDob()->format('d/m/Y');
}
	
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
	<style>
	.datepicker{
		z-index : 10000;
	}
	
	.dataTables_length{
		display:none;
	}
	
	.dataTables_empty{
		display:none;
	}
	
	.dataTables_filter{
		display:none;
	}
	</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">

					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->		
			<?php $sidemenu['Community Board'] = true; ?>	
			<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							General
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Community Board
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Community List</h4>
						</div>
						<div class="widget-body form">
							<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#createroom">Create New</button>
							<!-- Modal -->
							<div id="createroom" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Create New Topic</h4>
							      </div>
							      
							      <div class="modal-body">
							        <form class="form-horizontal" role="form" id="frmTopic">
									
								
								<div class="form-group" style="padding:8px">
									<div class="control-group">
										<label class="control-label col-sm-2" for="durationEnable">Duration Enable &nbsp;</label>
										<div class="col-sm-10">
											<input type="checkbox" class="form-control" name="durationEnable" value="1" id="durationEnable">
										</div>
									</div>
								</div>
								
								<div class="form-group" style="padding:8px">
									<div>
										<label class="control-label col-sm-2" for="expiryDate">Expiry Date &nbsp;</label>
										<div class="col-sm-10">
											<input class="form-control input-small date-picker" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" size="16" type="text" value="<?php echo $expiryDate; ?>" id="expiryDate" name="expiryDate"/>
										</div>
									</div>
								</div>
									
								  <div class="form-group" style="padding:8px">
								    <label class="control-label col-sm-2" for="topic" required="true">Topic &nbsp; </label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="topic">
								    </div>
								  </div>
								  
								  <div class="form-group" style="padding:8px">
								    <label class="control-label col-sm-2" for="pwd">People &nbsp; </label>
								    <div class="col-sm-10"> 
								      <select name="people" class="form-control" id="people">
								      <option value="0">All</option>
								      <option value="1">Invite By Unit No.</option>
								      </select>
								    </div>
								  </div>
								
								<div id="unit_no" style="display:none">
								  <div class="form-group" style="padding:8px"> 
								    <label class="control-label col-sm-2" for="unit">Unit No &nbsp; </label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="unit"> <button type="button" class="btn btn-default" id="invite">Invite</button>
									  
								    </div>
								  </div>
								  
								  <div class="form-group" style="padding:8px">
								    <label class="control-label col-sm-2" for="arrUnitId" required="true">Unit Invited &nbsp; </label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control" id="arrUnitId" readonly>
								    </div>
								  </div>
								</div>
								  
								  <div class="form-group" style="padding:8px"> 
								    <div style="text-align: center;padding:10px">
								      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> <button type="button" class="btn btn-default" id="continue">continue</button>
								    </div>
								  </div>
								</form>
							   </div>
							    </div>

							  </div>
							</div>
							
							<!-- CHAT ROOM -->
							<div id="room" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">CHAT ROOM </h4>
							      </div>
							      
							      <div class="modal-body"><div id="Title"></div></div>
							</div>
							</div>
							</div>
								      
						</div>
					</div>
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Room List</h4>
							<div class="widget-body form">
								<table class="table table-striped table-bordered"  id="tableRoomList">
								<thead>
									<tr>
										<th>Topic Name</th>
										<th>Edit Topic Name</th>
										<th>Remove Topic</th>
									</tr>
								</thead>
								<tbody id="roomList">
								
								
								
								</tbody>
								</table>
								
							</div>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd. 
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->	
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
			
			$('#tableRoomList').dataTable( {
		         "aaSorting": [[ 1, "asc" ]],
			 "iDisplayLength": 100
			} );
			
			jQuery(document).on("click", ".open-AddBookDialog", function () {
			     var myBookId = jQuery(this).data('id');
			     jQuery(".modal-body #Title").html( myBookId );
			});
			
			 jQuery.ajax({
					type: "PUT",
					url: "<?php echo $apiCommBoard ?>",
					data: '{"action":"TcT29yBql8","userId":"<?php echo $userId ?>","unitId":"<?php echo $userId ?>","projectId":"<?php echo $projectId ?>","fromCMS":"true"}',
					success: function(info){
						//alert(info.Data);
						info = info.Data;
						for (var i = 0; i < info.length; i++) {
						    //console.log("PAIR " + i + ": " + info[i].Title);
						    //console.log("PAIR " + i + ": " + info[i].Id);
						    jQuery("#roomList").append('<tr><td><a href="community_chatroom.php?id='+info[i].Id+'" id="btn-room" class="btn btn-info btn-lg btn-room" data-target="#room">'+info[i].Title+'</a></td> <td><a href="#" onclick="editRoom('+info[i].Id+')" id="btn-room" class="btn btn-default btn-lg btn-room" data-target="#room">Edit Room</a></td><td><a href="#" onclick="removeRoom('+info[i].Id+')" id="btn-room" class="btn btn-danger btn-lg btn-room" data-target="#room">Remove</a></td></tr>');
						    
						}
					}			
				});
			
			   jQuery("#invite").click(function(){
				   unitID =  jQuery("#unit").val();
				   jQuery.ajax({
					type: "PUT",
					url: "<?php echo $apiCommBoard ?>",
					data: '{"action":"wAa9Wo2IdM","userId":"<?php echo $userId ?>","projectId":"<?php echo $projectId ?>","unitId":"'+unitID+'"}',
					success: function(info){
						if(info.Data.UnitId == unitID){
							if(jQuery("#arrUnitId").val() == ""){
								jQuery("#arrUnitId").val(info.Data.UnitId);
							}else{
								jQuery("#arrUnitId").val(jQuery("#arrUnitId").val()+','+info.Data.UnitId);
							}
							unitID =  jQuery("#unit").val('');
						}else{
							alert('Unit ID not exist');
						}
						//console.log(info);
					}			
				});
			});
				
			 jQuery('#people').on('change', function(){
			    var selected = $(this).find("option:selected").val();
			    //alert(selected);
			    if(selected == 1){
				     jQuery('#unit_no').toggle('slow');
			    }else{
				     jQuery('#unit_no').toggle();
			    }
			  });
			 
			 jQuery("#continue").click(function(){
				   //unitID =  jQuery("#unit").val();
				   //unitID =  ["212","213","214"];
				   Title =  jQuery("#topic").val();
				   arrUnitId =  jQuery("#arrUnitId").val();
				    
					dE = 0;
					eD = "0000-00-00 00:00:00";
							
					if(jQuery("#durationEnable").is(":checked")){
						dE = 1;
						eD = $("#expiryDate").val();
					}
				   //alert(arrUnitId);
				   if(Title == "") {
					   alert('What is the Topic of this chat room ?');
					   return false;
				   }
				   jQuery.ajax({
					type: "PUT",
					url: "<?php echo $apiCommBoard ?>",
					data: '{"action":"Hq4Eyhihz6","projectId":"<?php echo $projectId ?>","unitId":"'+arrUnitId+'","userId":"<?php echo $userId ?>","title":"'+Title+'","adminId":"<?php echo $_SESSION['userId'] ?>","adminUnitId":"A00","durationEnable":"'+dE+'","expiryDate":"'+eD+'"}=>"JSON"',
					success: function(info){
						if (info.Code == '1000'){
							alert('Room '+Title+' has been created');
							window.location.reload();
						}
					}			
				});
			});
			
		});
		
		function removeRoom(id){
			x = confirm ("Are you sure ?");
				if(x==true){
					window.location="?action=removeRoom&id="+id;
				}
		}
		
		function editRoom(id){
			var x = prompt("Please enter new topic");
				if(x != null){
					window.location="?action=editRoom&topic="+x+"&id="+id;
				}
		}
	</script>
	
	
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
