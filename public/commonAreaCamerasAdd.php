<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$location = '';
$cameraName = '';
$protocol = '';
$ipAddress = '';
$port = '';
$cameraType = '';
$dvrUsername = '';
$dvrPassword = '';
$dvrChannel = '';
$ftpPath = '';
$displayOrder = '';
$enable = '';
$errLocation = '';
$errCameraName = '';
$errProtocol = '';
$errIpAddress = '';
$errPort = '';
$errCameraType = '';
$errDisplayOrder = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$location = isset($_REQUEST['location']) ? $_REQUEST['location'] : '';
	$cameraName = isset($_REQUEST['cameraName']) ? $_REQUEST['cameraName'] : '';
	$protocol = isset($_REQUEST['protocol']) ? $_REQUEST['protocol'] : '';
	$ipAddress = isset($_REQUEST['ipAddress']) ? $_REQUEST['ipAddress'] : '';
	$port = isset($_REQUEST['port']) ? $_REQUEST['port'] : '';
	$cameraType = isset($_REQUEST['cameraType']) ? $_REQUEST['cameraType'] : '';
	$dvrUsername = isset($_REQUEST['dvrUsername']) ? $_REQUEST['dvrUsername'] : '';
	$dvrPassword = isset($_REQUEST['dvrPassword']) ? $_REQUEST['dvrPassword'] : '';
	$dvrChannel = isset($_REQUEST['dvrChannel']) ? $_REQUEST['dvrChannel'] : '';
	$ftpPath = isset($_REQUEST['ftpPath']) ? $_REQUEST['ftpPath'] : '';
	$displayOrder = isset($_REQUEST['displayOrder']) ? $_REQUEST['displayOrder'] : '';
	$enable = isset($_REQUEST['enable']) ? $_REQUEST['enable'] : '';

	$hasError = false;
	
	if ($location == '') {
		$hasError = true;
		$errLocation = 'Please enter the location.';
	}
	
	if ($cameraName == '') {
		$hasError = true;
		$errCameraName = "Please enter the camera name.";			
	}
	
	if ($protocol == '') {
		$hasError = true;
		$errProtocol = "Please enter the Procotol.";			
	}
	
	if ($ipAddress == '') {
		$hasError = true;
		$errIpAddress = "Please enter the IP Address.";			
	}
	
	if ($port == '') {
		$hasError = true;
		$errPort = "Please enter the Port.";			
	}

	if ($cameraType == '') {
		$hasError = true;
		$errCameraType = "Please select the camera type.";			
	}

	if ($displayOrder == '') {
		$hasError = true;
		$errDisplayOrder = "Please enter the display order.";			
	}
		
	if (!$hasError) {
		$uniqid = uniqid();
		$commonAreaCamera = new CommonAreaCamera();
		$commonAreaCamera->setID($uniqid);
		$commonAreaCamera->setProjectId($_SESSION['projectId']);
		$commonAreaCamera->setLocation($location);
		$commonAreaCamera->setCameraName($cameraName);
		$commonAreaCamera->setProtocol($protocol);
		$commonAreaCamera->setIpAddress($ipAddress);
		$commonAreaCamera->setPort($port);
		$commonAreaCamera->setCameraType($cameraType);
		$commonAreaCamera->setDvrUsername($dvrUsername);
		$commonAreaCamera->setDvrPassword($dvrPassword);
		$commonAreaCamera->setDvrChannel($dvrChannel);
		$commonAreaCamera->setFtpPath($ftpPath);
		$commonAreaCamera->setDisplayOrder($displayOrder);
		$commonAreaCamera->setEnable($enable);
		$commonAreaCamera->setCreatedBy($_SESSION['username']);
		$commonAreaCamera->setCreatedDate(date('Y-m-d H:i:s'));
		$commonAreaCamera->setModifiedBy($_SESSION['username']);
		$commonAreaCamera->setModifiedDate(date('Y-m-d H:i:s'));
		$commonAreaCamera->save();
		
		header("Location: ./commonAreaCamerasList.php");
		die();
	}
}
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['System Configuration'] = true; ?>
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Common Area Cameras Creation
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> System Configuration <span class="divider">/</span> Common Area Cameras Creation
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Common Area Cameras Creation</h4>
						</div>
						
						<div class="widget-body">
							<form action="./commonAreaCamerasAdd.php" method="post" class="form-horizontal">
								<div class="control-group <?php if ($errLocation <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Location</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Location" name="location" value="<?php echo $location; ?>" />
										<span class="help-block"><?php echo $errLocation; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errCameraName <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Camera Name</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Camera Name" name="cameraName" value="<?php echo $cameraName; ?>"/>
										<span class="help-block"><?php echo $errCameraName; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errProtocol <> '') echo 'has-error'; ?>">
									<label class="control-label" for="protocol">Protocol</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="protocol" placeholder="<PROTOCOL>" name="protocol" value="<?php echo $protocol; ?>"/>
										<span class="help_inline">Format example: http, https</span>
										<span class="help-block"><?php echo $errProtocol; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errIpAddress <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">CCTV File Name</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="IP Address" name="ipAddress" value="<?php echo $ipAddress; ?>"/>
										<span class="help_inline">Format example: grandstream.jpg</span>
										<span class="help-block"><?php echo $errIpAddress; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errPort <> '') echo 'has-error'; ?>">
									<label class="control-label" for="port">Port</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="port" placeholder="<PORT>" name="port" value="<?php echo $port; ?>"/>
										<span class="help_inline">Format example: 7373</span>
										<span class="help-block"><?php echo $errPort; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errCameraType <> '') echo 'has-error'; ?>">
									<label class="control-label">Camera Type</label>
									<div class="controls">
										<select class="form-control span6" data-placeholder="Please Select" tabindex="1" name="cameraType">
											<option value="" <?php if ($cameraType == '') echo 'selected'; ?>>Please Select</option>
											<option value="Indigo Vision Dome IV400" <?php if ($cameraType == 'Indigo Vision Dome IV400') echo 'selected'; ?>>Indigo Vision Dome IV400</option>
											<option value="Indigo Vision Box IV11000" <?php if ($cameraType == 'Indigo Vision Box IV11000') echo 'selected'; ?>>Indigo Vision Box IV11000</option>
											<option value="FTP_VIDEO" <?php if ($cameraType == 'FTP_VIDEO') echo 'selected'; ?>>FTP VIDEO</option>
											<option value="Honeywell DVR" <?php if ($cameraType == 'Honeywell DVR') echo 'selected'; ?>>Honeywell DVR</option>
										</select>
										<span class="help-block"><?php echo $errCameraType; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">DVR Username</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="DVR Username" name="dvrUsername" value="<?php echo $dvrUsername; ?>"/>
										<span class="help_inline">Mandatory only if camera type is Honeywell DVR</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">DVR Password</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="DVR Password" name="dvrPassword" value="<?php echo $dvrPassword; ?>"/>
										<span class="help_inline">Mandatory only if camera type is Honeywell DVR</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">DVR Channel</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="DVR Channel" name="dvrChannel" value="<?php echo $dvrChannel; ?>"/>
										<span class="help_inline">Mandatory only if camera type is Honeywell DVR</span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">FTP Video File Path</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="FTP Video File Path" name="ftpPath" value="<?php echo $ftpPath; ?>"/>
									</div>
								</div>
								<div class="control-group <?php if ($errDisplayOrder <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Display Order</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Display Order" name="displayOrder" value="<?php echo $displayOrder; ?>"/>
										<span class="help-block"><?php echo $errDisplayOrder; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Enable Flag</label>
									<div class="controls">
										<input type="checkbox" value="1" name="enable" <?php if ($enable == 1) echo "checked=\"1\""; ?> /> 
									</div>
								</div>                             
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="reset" class="btn">Clear</button>
									<button type="button" class="btn" onclick="window.location.href='commonAreaCamerasList.php'">Cancel</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
