<?php
session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

function showImage(){
	
	$alarmStatusList = AlarmStatusQuery::create()
				->filterByProjectId($_SESSION['projectId'])
				->groupBy('UnitId')
				->groupBy('Status')
				->orderByStatus()->find()->getData();
	
		 foreach ($alarmStatusList as $alarmStatus) {
			 if ($alarmStatus->getStatus() == 'ALARM') {
				$sirenOn = true;
			}
		}
		if($sirenOn) {
			echo '<center><img src="./assets/img/alarm_banner.gif" /></center>';
			$myAudioFile = "./assets/sound/guardalarm.mp3";
			?>
			<audio autoplay>
			  <source src="<?php echo $myAudioFile ?>" type="audio/mpeg">
			Your browser does not support the alarm sound.
			</audio>
		<?php
		}
	
}

function getData(){
	$alarmStatusList = AlarmStatusQuery::create()
				->filterByProjectId($_SESSION['projectId'])
				->groupBy('UnitId')
				->groupBy('Status')
				->orderByStatus()->find()->getData();
	if (count($alarmStatusList) == 0) {
		echo "No alarm status from unit.";
	} else {
	?>
		
	<?php
		$sirenOn = false;
		$alarmSetting 	= AlarmSettingQuery::create()->filterByProjectId($_SESSION['projectId'])->findOneByAllowDetail(1);
		$enabledFlag	=	0;
		if($alarmSetting) $enabledFlag = $alarmSetting->getAllowDetail();
		//echo $enabledFlag;
		foreach ($alarmStatusList as $alarmStatus) {
			$alarmHistory = AlarmStatusHistoryQuery::create()->filterByUnitId($alarmStatus->getUnitId())
											->orderByAcknowledgedDate('DESC')
											->findOne();	
			$dataAlarm = $alarmHistory->toArray();
											
	?>									
				<tr class="odd gradeX">
					<?php if ($alarmStatus->getStatus() == 'ALARM') {
						$sirenOn = true;
					?>
					<?php
					if($enabledFlag == 1){
					?>
						<td style="color: red"><b><a href="alarmCmsHistory.php?unitId=<?php echo $alarmStatus->getUnitId(); ?>"><?php echo $alarmStatus->getUnitId(); ?></a></b></td>
					<?php
					}else{
					?>
						<td style="color: red"><b><?php echo $alarmStatus->getUnitId(); ?></b></td>
					<?php
					}
					?>
					<td style="color: red"><b><?php echo $alarmStatus->getStatus(); ?></b></td>
					<td><?php echo $alarmStatus->getTriggeredDate()->format('m/d/Y H:i A'); ?></td>
					<td><a href="./alarmCmsStatusAck.php?sid=<?php echo $alarmStatus->getId(); ?>&sidHistory=<?php echo $alarmStatus->getAlarmHistoryId(); ?>&unitId=<?php echo $alarmStatus->getUnitId(); ?>&triggered=<?php echo $alarmStatus->getTriggeredDate()->format('l, jS \of F Y h:i A'); ?>" class="btn btn-mini">Acknowledge</a></td>
					<?php } else { ?>
					<td><?php echo $alarmStatus->getUnitId(); ?></td>
					<td><?php echo $alarmStatus->getStatus(); ?></td>
					
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<?php } ?>
					<td><?php echo $dataAlarm['AcknowledgedBy'] != "" ? $dataAlarm['AcknowledgedBy'] : "No user"; ?></td>
					<td><?php echo $dataAlarm['AcknowledgedBy'] != '' ? date('l, jS \of F Y h:i A',strtotime($dataAlarm['AcknowledgedDate'])) : "Never acknowledged"?></td>
				</tr>
	<?php
		}
	?>
			
	<?php
	}
}


if(isset($_GET['refresh'])){
	getData();
}else if(isset($_GET['showImage'])){
	showImage();	
}else{
	
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
	<style>
	tr th {
		font-size:12px;
	}
	
	.odd{
		font-size:12px;
	}
	</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['Alarm CMS Status'] = true; ?>						
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Alarm CMS Status
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Alarm CMS Status
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Alarm CMS Status</h4>
						</div>
						
						<div class="widget-body">
						<span id="showImage"><?php showImage() ?></span>
						<table class="table table-striped table-bordered"  id="tableAlarmStatus">
						<thead>
							<tr>
								<th>Unit ID</th>
								<th>Alarm Status</th>
								<th>Alarm Date</th>
								<th>Acknowledgement</th>
								<th>Last Acknowledged By</th>
								<th>Last Acknowledged Date Time</th>
							</tr>
						</thead>
						<tbody id="alarmList">
						
						<?php getData() ?>
						
						</tbody>
						</table>
						</form>
						</div>
					</div>
					<!-- END FORM-->
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>

	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
			
			$('#tableAlarmStatus').dataTable( {
		         "aaSorting": [[ 1, "asc" ]],
			 "iDisplayLength": 100
			} );		
			
			setInterval(function() {
				$('#alarmList').load('?refresh=1');
			}, 3000)
			
			setInterval(function() {
				$('#showImage').load('?showImage=1');
			}, 3000)
			
		});
		
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<?php
}
?>