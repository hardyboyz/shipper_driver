<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$errTenantId = '';
$errUnitId = '';
$errName = '';

$unitId = '';
$unitBlock = '';
$unitFloor = '';
$unitNo = '';
$unitLayoutType = '';
$lift = '';
$sipServerIpAddress = '';
$username = '';
$password = '';
$name = '';
$dateOfBirth = '';
$homeNo = '';
$mobileNo = '';
$email = '';
$multipleIntercom='';

if ($_REQUEST['sid'] == '') {
	header("Location: ./tenantDetailsList.php");
	die();
} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	print_r($_REQUEST);
	$sid = isset($_REQUEST['sid']) ? $_REQUEST['sid'] : '';
	$tenantId = isset($_REQUEST['tenantId']) ? $_REQUEST['tenantId'] : '';
	$unitId = isset($_REQUEST['unitId']) ? $_REQUEST['unitId'] : '';
	$unitBlock = isset($_REQUEST['unitBlock']) ? $_REQUEST['unitBlock'] : '';
	$unitFloor = isset($_REQUEST['unitFloor']) ? $_REQUEST['unitFloor'] : '';
	$unitNo = isset($_REQUEST['unitNo']) ? $_REQUEST['unitNo'] : '';
	$unitLayoutType = isset($_REQUEST['unitLayoutType']) ? $_REQUEST['unitLayoutType'] : '';
	$lift = isset($_REQUEST['lift']) ? $_REQUEST['lift'] : '';
	$sipServerIpAddress = isset($_REQUEST['sipServerIpAddress']) ? $_REQUEST['sipServerIpAddress'] : '';
	$username = isset($_REQUEST['username']) ? $_REQUEST['username'] : '';
	$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : '';
	$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
	$dateOfBirth = isset($_REQUEST['dateOfBirth']) ? $_REQUEST['dateOfBirth'] : '';
	$homeNo = isset($_REQUEST['homeNo']) ? $_REQUEST['homeNo'] : '';
	$mobileNo = isset($_REQUEST['mobileNo']) ? $_REQUEST['mobileNo'] : '';
	$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
	$multipleIntercom = isset($_REQUEST['multiple_intercom']) ? $_REQUEST['multiple_intercom'] : '0';
	
	$hasError = false;
		
	if ($unitId == '') {
		$hasError = true;
		$errUnitId = 'Please enter the unit ID.';
	}
	
	if ($name == '') {
		$hasError = true;
		$errName = 'Please select the tenant name.';
	}
	
	if (!$hasError) {
		$tenant = TenantQuery::create()->filterById($sid)->findOne();
		$tenant->setId($tenantId);
		$tenant->setUnitId($unitId);
		$tenant->setUnitBlock($unitBlock);
		$tenant->setUnitFloor($unitFloor);
		$tenant->setUnitNo($unitNo);
		$tenant->setUnitLayoutPlan($unitLayoutType);
		$tenant->setLiftId($lift);
		$tenant->setSipServerIpAddress($sipServerIpAddress);
		$tenant->setUsername($username);
		$tenant->setPassword($password);
		$tenant->setName($name);
		$tenant->setHomeNo($homeNo);
		$tenant->setMobileNo($mobileNo);
		$tenant->setEmail($email);
		$tenant->setMultipleIntercom($multipleIntercom);
		$tenant->setDateOfBirth($dateOfBirth);
		$tenant->setModifiedBy($_SESSION['username']);
		$tenant->setModifiedDate(date('Y-m-d H:i:s'));
		$tenant->save();
		
		header("Location: ./tenantDetailsList.php");
		die();
	}
} else {
	$tenantSelected = TenantQuery::create()->filterById($_REQUEST['sid'])->findOne();
	if (count($tenantSelected) == 0) {
		header("Location: ./tenantDetailsList.php");
		die();
	} else {
		$sid = $tenantSelected->getId();
		$tenantId = $tenantSelected->getId();
		$projectId = $tenantSelected->getProjectId();
		$unitId = $tenantSelected->getUnitId();
		$unitBlock = $tenantSelected->getUnitBlock();
		$unitFloor = $tenantSelected->getUnitFloor();
		$unitNo = $tenantSelected->getUnitNo();
		$unitLayoutType = $tenantSelected->getUnitLayoutPlan();
		$lift = $tenantSelected->getLiftId();
		$sipServerIpAddress = $tenantSelected->getSipServerIpAddress();
		$username = $tenantSelected->getUsername();
		$password = $tenantSelected->getPassword();
		$name = $tenantSelected->getName();
		$homeNo = $tenantSelected->getHomeNo();
		$mobileNo = $tenantSelected->getMobileNo();
		$email = $tenantSelected->getEmail();
		$dateOfBirth = $tenantSelected->getDateOfBirth();
		$multipleIntercom = $tenantSelected->getMultipleIntercom();
	}
}

?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['System Configuration'] = true; ?>			
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Tenant Creation
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> System Configuration <span class="divider">/</span> Tenant Creation
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Tenant Creation</h4>
						</div>
						
						<div class="widget-body">
							<form action="./tenantDetails.php" method="post" class="form-horizontal">
								<div class="control-group <?php if ($errTenantId <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Tenant ID</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Tenant ID" name="tenantId" value="<?php echo $tenantId; ?>" readonly="true"/>
										<span class="help-block"><?php echo $errTenantId; ?></span>
									</div>
								</div>
																
								<div class="control-group <?php if ($errUnitId <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input2">Unit ID</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Unit ID" name="unitId" value="<?php echo $unitId; ?>"/>
										<span class="help-block"><?php echo $errUnitId; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Unit Block</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Unit Block" name="unitBlock" value="<?php echo $unitBlock; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Unit Floor</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Unit Floor" name="unitFloor" value="<?php echo $unitFloor; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Unit No.</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Unit No." name="unitNo" value="<?php echo $unitNo; ?>"/>
									</div>
								</div>
								<?php
								$unitLayout = UnitLayoutQuery::create()->filterByProjectId($_SESSION['projectId'])->filterByStatus(0)->orderById()->find()->getData();
								?>
								<div class="control-group">
									<label class="control-label">Unit Layout Type</label>
									<div class="controls">
										<select class="span6" data-placeholder="Please Select" tabindex="1" name="unitLayoutType">
											<option value="" <?php if ($unitLayoutType == '') echo 'selected'; ?>>Please Select</option>
											<?php foreach($unitLayout as $unit) : ?>
											<option value="<?php echo $unit->getId() ?>" <?php echo $unit->getId()==$unitLayoutType ? "selected" : "" ?>><?php echo $unit->getFloorPlanName() ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<?php
								$lifts = LiftQuery::create()->filterByProjectId($_SESSION['projectId'])->orderById()->find()->getData();
								?>
								<div class="control-group">
									<label class="control-label">Lift System</label>
									<div class="controls">
										<select class="span6" data-placeholder="Please Select" tabindex="1" name="lift">
											<option value="" <?php if ($lift == '') echo 'selected'; ?>>Please Select</option>
											<?php foreach($lifts as $liftDb) : ?>
											<option value="<?php echo $liftDb->getId() ?>" <?php echo $liftDb->getId()==$lift ? "selected" : "" ?>><?php echo $liftDb->getLabel() ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label">SIP Address</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="SIP Server IP Address" name="sipServerIpAddress" value="<?php echo $sipServerIpAddress; ?>"/>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label" for="input1">System Control</label>
									<div class="controls">
										<input type="text" class="form-control span3" id="input1" placeholder="Username" name="username" value="<?php echo $username; ?>"/>
										<input type="text" class="form-control span3" id="input1" placeholder="Password" name="password" value="<?php echo $password; ?>"/>
									</div>
								</div>
								
								<div class="control-group <?php if ($errName <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Name</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Name" name="name" value="<?php echo $name; ?>"/>
										<span class="help-block"><?php echo $errName; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">D.O.B. (dd/mm/yyyy)</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="D.O.B. (dd/mm/yyyy)" name="dateOfBirth" value="<?php echo $dateOfBirth; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Home Line</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Home Line" name="homeNo" value="<?php echo $homeNo; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Mobile Line</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Mobile Line" name="mobileNo" value="<?php echo $mobileNo; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Email Address</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Email Address" name="email" value="<?php echo $email; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="multiple_intercom">Enable Multiple Intercom</label>
									<div class="controls">
										<input type="checkbox" class="span6" id="multiple_intercom" name="multiple_intercom" value="1" <?php echo $multipleIntercom==1 ? "checked=true" : "" ?>/>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Update</button>
									<button type="reset" class="btn">Clear</button>
									<button type="button" class="btn" onclick="window.location.href='tenantDetailsList.php'">Cancel</button>
									<input type="hidden" name="sid" value="<?php echo $sid; ?>">
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
