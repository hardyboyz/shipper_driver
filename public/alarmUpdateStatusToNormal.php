<?php

require_once '../vendor/autoload.php';
require_once '../generated-conf/config.php';

function alarmUpdateStatusToNormal()
{
	$day = date('Y-m-d H:i:s');
	$timer = date('Y-m-d H:i:s',strtotime($day . "+1 hour"));
	$filter = \AlarmStatusQuery::create()
	->filterByStatus("ACKNOWLEDGED")
	->filterByModifiedDate(array("max"=>$timer))
	//->where("ModifiedDate <= NOW() - INTERVAL 1 HOUR")
	->update(array('Status'=>"NORMAL",
			'ModifiedDate'=>date('Y-m-d H:i:s')
	));
}
	
alarmUpdateStatusToNormal();

?>
