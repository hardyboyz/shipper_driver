<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php
session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$errProjectId = '';
$errProjectName = '';
$errProjectPrefix = '';

$projectId = '';
$projectName = '';
$projectPrefix = '';
$blockNo1 = '';
$blockNo2 = '';
$blockNo3 = '';
$blockNo4 = '';
$blockNo5 = '';
$blockNo6 = '';
$blockNo7 = '';
$blockNo8 = '';
$blockLabel1 = '';
$blockLabel2 = '';
$blockLabel3 = '';
$blockLabel4 = '';
$blockLabel5 = '';
$blockLabel6 = '';
$blockLabel7 = '';
$blockLabel8 = '';

var_dump ($_REQUEST);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$projectId = isset($_REQUEST['projectId']) ? $_REQUEST['projectId'] : '';
	$projectName = isset($_REQUEST['projectName']) ? $_REQUEST['projectName'] : '';
	$projectPrefix = isset($_REQUEST['projectPrefix']) ? $_REQUEST['projectPrefix'] : '';
	$blockNo1 = isset($_REQUEST['blockNo1']) ? $_REQUEST['blockNo1'] : '';
	$blockLabel1 = isset($_REQUEST['blockLabel1']) ? $_REQUEST['blockLabel1'] : '';
	$blockNo2 = isset($_REQUEST['blockNo1']) ? $_REQUEST['blockNo2'] : '';
	$blockLabel2 = isset($_REQUEST['blockLabel1']) ? $_REQUEST['blockLabel2'] : '';
	$blockNo3 = isset($_REQUEST['blockNo1']) ? $_REQUEST['blockNo3'] : '';
	$blockLabel3 = isset($_REQUEST['blockLabel1']) ? $_REQUEST['blockLabel3'] : '';
	$blockNo4 = isset($_REQUEST['blockNo1']) ? $_REQUEST['blockNo4'] : '';
	$blockLabel4 = isset($_REQUEST['blockLabel1']) ? $_REQUEST['blockLabel4'] : '';
	$blockNo5 = isset($_REQUEST['blockNo1']) ? $_REQUEST['blockNo5'] : '';
	$blockLabel5 = isset($_REQUEST['blockLabel1']) ? $_REQUEST['blockLabel5'] : '';
	$blockNo6 = isset($_REQUEST['blockNo1']) ? $_REQUEST['blockNo6'] : '';
	$blockLabel6 = isset($_REQUEST['blockLabel1']) ? $_REQUEST['blockLabel6'] : '';
	$blockNo7 = isset($_REQUEST['blockNo1']) ? $_REQUEST['blockNo7'] : '';
	$blockLabel7 = isset($_REQUEST['blockLabel1']) ? $_REQUEST['blockLabel7'] : '';
	$blockNo8 = isset($_REQUEST['blockNo1']) ? $_REQUEST['blockNo8'] : '';
	$blockLabel8 = isset($_REQUEST['blockLabel1']) ? $_REQUEST['blockLabel8'] : '';
	
	$arrBlockNo = array($blockNo1, $blockNo2, $blockNo3, $blockNo4, $blockNo5, $blockNo6, $blockNo7, $blockNo8);
	$arrBlockLabel = array($blockLabel1, $blockLabel2, $blockLabel3, $blockLabel4, $blockLabel5, $blockLabel6, $blockLabel7, $blockLabel8);

	$hasError = false;

	if ($projectId == '') {
		$hasError = true;
		$errProjectId = 'Please enter the project ID.';
	}
	
	if ($projectName == '') {
		$hasError = true;
		$errProjectName = 'Please enter the project name.';
	}
	
	if ($projectPrefix == '') {
		$hasError = true;
		$errProjectPrefix = 'Please enter the project prefix.';
	}
	
	if (!$hasError) {
		$propertyProject = new PropertyProject();
		$propertyProject->setId($projectId);
		$propertyProject->setProjectName($projectName);
		$propertyProject->setProjectPrefix($projectPrefix);
		$propertyProject->setCreatedBy($_SESSION['username']);
		$propertyProject->setCreatedDate(date('Y-m-d H:i:s'));
		$propertyProject->setModifiedBy($_SESSION['username']);
		$propertyProject->setModifiedDate(date('Y-m-d H:i:s'));
		$propertyProject->save();
		
		for ($i = 0; $i < 8; $i++) {
			if ($arrBlockNo[$i] != '' && $arrBlockLabel[$i] != '') {
				$propertyBlock = new PropertyBlock();
				$propertyBlock->setId(uniqid());
				$propertyBlock->setProjectId($propertyProject->getId());
				$propertyBlock->setBlockNo($arrBlockNo[$i]);
				$propertyBlock->setBlockLabel($arrBlockLabel[$i]);
				$propertyBlock->setCreatedBy($_SESSION['username']);
				$propertyBlock->setCreatedDate(date('Y-m-d H:i:s'));
				$propertyBlock->setModifiedBy($_SESSION['username']);
				$propertyBlock->setModifiedDate(date('Y-m-d H:i:s'));
				$propertyBlock->save();
			} else {
				break;
			}
		}
		
		header("Location: ./propertyProjectList.php");
		die();		
	}
}
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['System Configuration'] = true; ?>			
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Property Project Creation
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> System Configuration <span class="divider">/</span> Property Project Creation
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Property Project Creation</h4>
						</div>
						
						<div class="widget-body">
							<form action="./propertyProjectAdd.php" method="post" class="form-horizontal">
								<div class="control-group <?php if ($errProjectName <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Intercom Project ID</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Project ID" name="projectId" value="<?php echo $projectId; ?>"/>
										<span class="help-block"><?php echo $errProjectId; ?></span>
									</div>
								</div>

								<div class="control-group <?php if ($errProjectName <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Project Name</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Project Name" name="projectName" value="<?php echo $projectName; ?>"/>
										<span class="help-block"><?php echo $errProjectName; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errProjectPrefix <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Project Prefix</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Project Prefix" name="projectPrefix" value="<?php echo $projectPrefix; ?>"/>
										<span class="help-block"><?php echo $errProjectPrefix; ?></span>
									</div>
								</div>
								
								<table>
								<tr><td>
								<div class="control-group">
									<label class="control-label" for="blockNo1">Block Start No.</label>
									<div class="controls">
										<select name="blockNo1" id="blockNo1">
											<option value=""></option>
											<?php
											for($i=0; $i<=9; $i++){
												$selected = $i == $blockNo1 ? "selected" : "";
												echo '<option value='.$i.' '.$selected.'>'.$i.'</option>';
											}
											?>
										</select>
										<!--<input type="text" class="span12" id="input1" placeholder="Block No." name="blockNo1" value="<?php echo $blockNo1; ?>"/>-->
										
									</div>
								</div>
								</td><td>
								<div class="control-group">
									<label class="control-label" for="input2">Block Labels</label>
									<div class="controls">
										<input type="text" class="span12" id="input1" placeholder="Block Labels" name="blockLabel1" value="<?php echo $blockLabel1; ?>"/>
									</div>
								</div>
								</td></tr>
								<tr><td>
								<div class="control-group">
									<label class="control-label" for="blockNo2">Block Start No.</label>
									<div class="controls">
										<select name="blockNo2" id="blockNo2">
											<option value=""></option>
											<?php
											for($i=0; $i<=9; $i++){
												$selected = $i == $blockNo2 ? "selected" : "";
												echo '<option value='.$i.' '.$selected.'>'.$i.'</option>';
											}
											?>
										</select>
										<!--<input type="text" class="span12" id="input1" placeholder="Block No." name="blockNo2"  value="<?php echo $blockNo2; ?>"/>-->
									</div>
								</div>
								</td><td>
								<div class="control-group">
									<label class="control-label" for="input2">Block Labels</label>
									<div class="controls">
										<input type="text" class="span12" id="input1" placeholder="Block Labels" name="blockLabel2" value="<?php echo $blockLabel2; ?>"/>
									</div>
								</div>
								</td></tr>
								<tr><td>
								<div class="control-group">
									<label class="control-label" for="input2">Block Start No.</label>
									<div class="controls">
										<select name="blockNo3" id="blockNo3">
											<option value=""></option>
											<?php
											for($i=0; $i<=9; $i++){
												$selected = $i == $blockNo3 ? "selected" : "";
												echo '<option value='.$i.' '.$selected.'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								</td><td>
								<div class="control-group">
									<label class="control-label" for="input2">Block Labels</label>
									<div class="controls">
										<input type="text" class="span12" id="input1" placeholder="Block Labels" name="blockLabel3" value="<?php echo $blockLabel3; ?>"/>
									</div>
								</div>
								</td></tr>
								<tr><td>
								<div class="control-group">
									<label class="control-label" for="blockNo4">Block Start No.</label>
									<div class="controls">
										<select name="blockNo4" id="blockNo4">
											<option value=""></option>
											<?php
											for($i=0; $i<=9; $i++){
												$selected = $i == $blockNo4 ? "selected" : "";
												echo '<option value='.$i.' '.$selected.'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								</td><td>
								<div class="control-group">
									<label class="control-label" for="input2">Block Labels</label>
									<div class="controls">
										<input type="text" class="span12" id="input1" placeholder="Block Labels" name="blockLabel4" value="<?php echo $blockLabel4; ?>"/>
									</div>
								</div>
								</td></tr>
								<tr><td>
								<div class="control-group">
									<label class="control-label" for="blockNo5">Block Start No.</label>
									<div class="controls">
										<select name="blockNo5" id="blockNo5">
											<option value=""></option>
											<?php
											for($i=0; $i<=9; $i++){
												$selected = $i == $blockNo5 ? "selected" : "";
												echo '<option value='.$i.' '.$selected.'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								</td><td>
								<div class="control-group">
									<label class="control-label" for="input2">Block Labels</label>
									<div class="controls">
										<input type="text" class="span12" id="input1" placeholder="Block Labels" name="blockLabel5" value="<?php echo $blockLabel5; ?>"/>
									</div>
								</div>
								</td></tr>
									<tr><td>
								<div class="control-group">
									<label class="control-label" for="blockNo6">Block Start No.</label>
									<div class="controls">
										<select name="blockNo6" id="blockNo6">
											<option value=""></option>
											<?php
											for($i=0; $i<=9; $i++){
												$selected = $i == $blockNo6 ? "selected" : "";
												echo '<option value='.$i.' '.$selected.'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								</td><td>
								<div class="control-group">
									<label class="control-label" for="input2">Block Labels</label>
									<div class="controls">
										<input type="text" class="span12" id="input1" placeholder="Block Labels" name="blockLabel6" value="<?php echo $blockLabel6; ?>"/>
									</div>
								</div>
								</td></tr>
								<tr><td>
								<div class="control-group">
									<label class="control-label" for="blockNo7">Block Start No.</label>
									<div class="controls">
										<select name="blockNo7" id="blockNo7">
											<option value=""></option>
											<?php
											for($i=0; $i<=9; $i++){
												$selected = $i == $blockNo7 ? "selected" : "";
												echo '<option value='.$i.' '.$selected.'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								</td><td>
								<div class="control-group">
									<label class="control-label" for="input2">Block Labels</label>
									<div class="controls">
										<input type="text" class="span12" id="input1" placeholder="Block Labels" name="blockLabel7" value="<?php echo $blockLabel7; ?>"/>
									</div>
								</div>
								</td></tr>
								<tr><td>
								<div class="control-group">
									<label class="control-label" for="blockNo8">Block Start No.</label>
									<div class="controls">
										<select name="blockNo8" id="blockNo8">
											<option value=""></option>
											<?php
											for($i=0; $i<=9; $i++){
												$selected = $i == $blockNo8 ? "selected" : "";
												echo '<option value='.$i.' '.$selected.'>'.$i.'</option>';
											}
											?>
										</select>
									</div>
								</div>
								</td><td>
								<div class="control-group">
									<label class="control-label" for="input2">Block Labels</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Block Labels" name="blockLabel8" value="<?php echo $blockLabel8; ?>"/>
									</div>
								</div>
								</td></tr>
								</table>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="button" class="btn" onclick="window.location.href='propertyProjectList.php'">Cancel</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
