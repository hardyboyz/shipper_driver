<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';
if ($_REQUEST['sid'] == '') {
	header("Location: ./lifestyleList.php");
	die();
} else {
	$lifestyle = LifestyleQuery::create()->findPK($_REQUEST['sid']);
	if (count($lifestyle) == 0) {
		header("Location: ./lifestyleList.php");
		die();
	} else {
		$sid = $lifestyle->getId();
		$cat = $lifestyle->getType();
		$title = $lifestyle->getTitle();
		$companyName = $lifestyle->getCompanyName();
		$icon1 = $lifestyle->getIcon1();
		$icon2 = $lifestyle->getIcon2();
		$icon3 = $lifestyle->getIcon3();
		$icon4 = $lifestyle->getIcon4();
		$icon5 = $lifestyle->getIcon5();
		$validDate = $lifestyle->getValidDate()->format('Y-m-d');
		$description = $lifestyle->getDescription();
		$type = $lifestyle->getType();
		$pcatId = $lifestyle->getPcatId();
		
		//echo $validDate;exit;
	}
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$id = $_POST['id'];
	$title = isset($_REQUEST['title']) ? $_REQUEST['title'] : '';
	$category = isset($_REQUEST['category']) ? $_REQUEST['category'] : '';
	$companyName = isset($_REQUEST['companyName']) ? $_REQUEST['companyName'] : '';
	$icon1 = isset($_REQUEST['icon1']) ? $_REQUEST['icon1'] : '';
	$icon2 = isset($_REQUEST['icon2']) ? $_REQUEST['icon2'] : '';
	$icon3 = isset($_REQUEST['icon3']) ? $_REQUEST['icon3'] : '';
	$icon4 = isset($_REQUEST['icon4']) ? $_REQUEST['icon4'] : '';
	$icon5 = isset($_REQUEST['icon5']) ? $_REQUEST['icon5'] : '';
	$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
	$pcatId = isset($_REQUEST['pcatId']) ? $_REQUEST['pcatId'] : '';
	$description = isset($_REQUEST['description']) ? $_REQUEST['description'] : '';
	$validDate = isset($_REQUEST['validDate']) ? $_REQUEST['validDate'] : '';
$hasError = false;
		
		if (!$hasError) {
			$lifestyle = LifestyleQuery::Create()->filterById($id)->findOne();
			//$lifestyle->setId($uniqid);
			$lifestyle->setProjectId($_SESSION['projectId']);
			$lifestyle->setPcatId($pcatId);
			$lifestyle->setTitle($title);
			$lifestyle->setCompanyName($companyName);
			$lifestyle->setIcon1($icon1);
			$lifestyle->setIcon2($icon2);
			$lifestyle->setIcon3($icon3);
			$lifestyle->setIcon4($icon4);
			$lifestyle->setIcon5($icon5);
			$lifestyle->setType($category);
			$lifestyle->setDescription($description);
			$lifestyle->setValidDate(date('Y-m-d H:i:s', strtotime($validDate)));
			$lifestyle->setCreatedBy($_SESSION['username']);
			$lifestyle->setCreatedDate(date('Y-m-d H:i:s'));
			$lifestyle->setModifiedBy($_SESSION['username']);
			$lifestyle->setModifiedDate(date('Y-m-d H:i:s'));
			$lifestyle->save();
			$getLifestyle = $lifestyle->toArray(); 
			//print_r($getEbulletin);
			//exit;
			
			insertUserEvent('Lifestyle', 'Has performed Lifestyle (Title: ' . $title . ')');
			header("Location: ./lifestyleList.php");
			die();
		}
	}


?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script>tinymce.init({
		  selector: 'textarea',
		  height: 500,
		  theme: 'modern',
		  plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools'
		  ],
		 toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | sizeselect | bold italic | fontselect fontsizeselect link preview | forecolor backcolor emoticons',
		  fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
		  image_advtab: true,
		  content_css: [
			'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			'//www.tinymce.com/css/codepen.min.css'
		  ]
		 });
		 
		 $('form').bind('form-pre-serialize', function(e) {
			tinyMCE.triggerSave();
		});
	</script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['Operation Configuration'] = true; ?>				
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Lifestyle Details
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Operation Management <span class="divider">/</span> Lifestyle
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Lifestyle Details</h4>
						</div>
						
						<div class="widget-body">
							<form action="<?php //echo $action ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="lifestyle">
							<input type="hidden" name="id" value="<?php echo $sid ?>">
								<div class="control-group">
									<label class="control-label" for="input1">Title</label>
									<div class="controls">
										<input type="text" name="title" value="<?php echo $title; ?>"></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Parent Category</label>
									<?php 
			$pcat = \LifestylePcategoriesQuery::Create()->filterByProjectId($_SESSION['projectId'])->find();
									$pcat = $pcat->toArray();
									?>
									<div class="controls">
										<select name="pcatId">
											<?php foreach($pcat as $pc){ 
												$select = $pc['Id'] == $pcatId ? "selected" : "";
												?>
												
											<option value="<?php echo $pc['Id'] ?>" <?php echo $select ?>><?php echo $pc['PcatName'] ?></option>
											<?php } ?>
										</select>
										<span class="help-block"><?php ?></span>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label" for="input1">Category</label>
									<?php 
									$cat = \LifestyleCategoriesQuery::Create()->filterByProjectId($_SESSION['projectId'])->find();
									$cat = $cat->toArray();
									?>
									<div class="controls">
										<select name="category">
											<?php foreach($cat as $c){ 
													$selCat = $c['Id'] == $type ? "selected" : "";
												?>
											<option value="<?php echo $c['Id'] ?>" <?php echo $selCat ?>><?php echo $c['CatName'] ?></option>
											<?php } ?>
										</select>
										<span class="help-block"></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Valid Date</label>
									<div class="controls">
										<input class="form-control input-small date-picker" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" size="16" type="text" value="<?php echo date('Y-m-d', strtotime($validDate)); ?>" id="validDate" name="validDate"/>
										<span class="help-block"></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input2">Company Name</label>
									<div class="controls">
										<label><input type="text" name="companyName" value="<?php echo $companyName; ?>"></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Description</label>
									<div class="controls">
										<textarea class="textarea span12" rows="6" id="textareaMessage" name="description"><?php echo $description; ?></textarea>
									</div>
								</div>
								
								<div class="control-group" id="photo">
									<label class="control-label">Icon 1</label>
									<div class="controls">															
										<div class="form-control fileupload fileupload-new" data-provides="fileupload">
											<div class="fileupload-new thumbnail" style="width: 100px; height: 100px;"><img src="<?php echo $imgUrl .'/'. $icon1.'/lifestyle'; ?>"></div>
											<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;"></div>
											<div>
												<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default selectFile1" name="fileicon1"  id="fileicon1"/></span>
												<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
											<div id="progressNumberIcon1" style="color:red"></div>
											<input type="hidden" name="icon1" id="icon1" value="<?php echo $icon1 ?>">
										</div>							
									</div>
								</div>
								<div class="control-group" id="photo">
									<label class="control-label">Icon 2</label>
									<div class="controls">															
										<div class="form-control fileupload fileupload-new" data-provides="fileupload">
											<div class="fileupload-new thumbnail" style="width: 100px; height: 100px;"><img src="<?php echo $imgUrl .'/'. $icon2.'/lifestyle'; ?>"></div>
											<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;"></div>
											<div>
												<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default selectFile2" name="fileicon2"  id="fileicon2"/></span>
												<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
											<div id="progressNumberIcon2" style="color:red"></div>
											<input type="hidden" name="icon2" id="icon2"  value="<?php echo $icon2 ?>">
										</div>							
									</div>
								</div>
								
								<div class="control-group" id="photo">
									<label class="control-label">Icon 3</label>
									<div class="controls">															
										<div class="form-control fileupload fileupload-new" data-provides="fileupload">
											<div class="fileupload-new thumbnail" style="width: 100px; height: 100px;"><img src="<?php echo $imgUrl .'/'. $icon3.'/lifestyle'; ?>"></div>
											<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;"></div>
											<div>
												<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default selectFile3" name="fileicon3"  id="fileicon3"/></span>
												<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
											<div id="progressNumberIcon3" style="color:red"></div>
											<input type="hidden" name="icon3" id="icon3" value="<?php echo $icon3 ?>">
										</div>							
									</div>
								</div>
								<div class="control-group" id="photo">
									<label class="control-label">Icon 4</label>
									<div class="controls">															
										<div class="form-control fileupload fileupload-new" data-provides="fileupload">
											<div class="fileupload-new thumbnail" style="width: 100px; height: 100px;"><img src="<?php echo $imgUrl .'/'. $icon4.'/lifestyle'; ?>"></div>
											<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;"></div>
											<div>
												<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default selectFile4" name="fileicon4"  id="fileicon4"/></span>
												<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
											<div id="progressNumberIcon4" style="color:red"></div>
											<input type="hidden" name="icon4" id="icon4"  value="<?php echo $icon4 ?>">
										</div>							
									</div>
								</div>
								
								<div class="control-group" id="photo">
									<label class="control-label">Icon 5</label>
									<div class="controls">															
										<div class="form-control fileupload fileupload-new" data-provides="fileupload">
											<div class="fileupload-new thumbnail" style="width: 100px; height: 100px;"><img src="<?php echo $imgUrl .'/'. $icon5.'/lifestyle'; ?>"></div>
											<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;"></div>
											<div>
												<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default selectFile5" name="fileicon5"  id="fileicon5"/></span>
												<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
											<div id="progressNumberIcon5" style="color:red"></div>
											<input type="hidden" name="icon5" id="icon5"  value="<?php echo $icon5 ?>">
										</div>							
									</div>
								</div>
																
								<div class="form-actions">
									<button type="submit" class="btn">Submit</button>
									<button type="button" class="btn" onclick="window.location.href='lifestyleList.php'">Back</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
			jQuery(".selectFile1").change(function(event) {
				     event.preventDefault();
				    var formData = new FormData($("#lifestyle")[0]);

				    $.ajax({
				        url: '<?php echo $action ?>', //Server script to process data
				        type: 'POST',
				       xhr: function () { // Custom XMLHttpRequest
					  var myXhr = jQuery.ajaxSettings.xhr();
					  if (myXhr.upload) { // Check if upload property exists
					      myXhr.upload.addEventListener('progress', uploadProgress1, false); // For handling the progress of the upload
					  }
					  return myXhr;
				        },
				        //Ajax events
				        beforeSend: function (stuff) {
					  console.log("BeforeSend");
					  console.log(stuff);
				        },
				        success: function (data) {
					  console.log("Success!");
					  console.log(data);
					  d = jQuery.parseJSON(data);
					  //if(d.type=="icon1"){
						  jQuery("#icon1").val(d.name);
					  //}
				        },
				        error: function (error) {
					  console.log("Error!");
					  console.log(error);
				        },
				        // Form data
				        data: formData,
				        //Options to tell jQuery not to process data or worry about content-type.
				        cache: false,
				        contentType: false,
				        processData: false
				    });	
				    
					    function uploadProgress1(evt) {
						selId	= document.getElementById('progressNumberIcon1');
						if (evt.lengthComputable) {
							var percentComplete = Math.round(evt.loaded * 100 / evt.total);
							selId.innerHTML = percentComplete.toString() + '% File Uploaded.';
						}
						else {
							selId.innerHTML = 'unable to upload';
					        }
					      }
				    
				 });
				 
				 jQuery(".selectFile2").change(function(event) {
				     event.preventDefault();
				    var formData = new FormData($("#lifestyle")[0]);

				    $.ajax({
				        url: '<?php echo $action ?>', //Server script to process data
				        type: 'POST',
				       xhr: function () { // Custom XMLHttpRequest
					  var myXhr = jQuery.ajaxSettings.xhr();
					  if (myXhr.upload) { // Check if upload property exists
					      myXhr.upload.addEventListener('progress', uploadProgress2, false); // For handling the progress of the upload
					  }
					  return myXhr;
				        },
				        //Ajax events
				        beforeSend: function (stuff) {
					  console.log("BeforeSend");
					  console.log(stuff);
				        },
				        success: function (data) {
					  console.log("Success!");
					  console.log(data);
					  d = jQuery.parseJSON(data);
					  //if(d.type=="icon1"){
						  jQuery("#icon2").val(d.name);
					  //}
				        },
				        error: function (error) {
					  console.log("Error!");
					  console.log(error);
				        },
				        // Form data
				        data: formData,
				        //Options to tell jQuery not to process data or worry about content-type.
				        cache: false,
				        contentType: false,
				        processData: false
				    });	
				    
					    function uploadProgress2(evt) {
						selId	= document.getElementById('progressNumberIcon2');
						if (evt.lengthComputable) {
							var percentComplete = Math.round(evt.loaded * 100 / evt.total);
							selId.innerHTML = percentComplete.toString() + '% File Uploaded.';
						}
						else {
							selId.innerHTML = 'unable to upload';
					        }
					      }
				    
				 });
				 
				  jQuery(".selectFile3").change(function(event) {
				     event.preventDefault();
				    var formData = new FormData($("#lifestyle")[0]);

				    $.ajax({
				        url: '<?php echo $action ?>', //Server script to process data
				        type: 'POST',
				       xhr: function () { // Custom XMLHttpRequest
					  var myXhr = jQuery.ajaxSettings.xhr();
					  if (myXhr.upload) { // Check if upload property exists
					      myXhr.upload.addEventListener('progress', uploadProgress3, false); // For handling the progress of the upload
					  }
					  return myXhr;
				        },
				        //Ajax events
				        beforeSend: function (stuff) {
					  console.log("BeforeSend");
					  console.log(stuff);
				        },
				        success: function (data) {
					  console.log("Success!");
					  console.log(data);
					  d = jQuery.parseJSON(data);
					  //if(d.type=="icon1"){
						  jQuery("#icon3").val(d.name);
					  //}
				        },
				        error: function (error) {
					  console.log("Error!");
					  console.log(error);
				        },
				        // Form data
				        data: formData,
				        //Options to tell jQuery not to process data or worry about content-type.
				        cache: false,
				        contentType: false,
				        processData: false
				    });	
				    
					    function uploadProgress3(evt) {
						selId	= document.getElementById('progressNumberIcon3');
						if (evt.lengthComputable) {
							var percentComplete = Math.round(evt.loaded * 100 / evt.total);
							selId.innerHTML = percentComplete.toString() + '% File Uploaded.';
						}
						else {
							selId.innerHTML = 'unable to upload';
					        }
					      }
				    
				 });
				 
				   jQuery(".selectFile4").change(function(event) {
				     event.preventDefault();
				    var formData = new FormData($("#lifestyle")[0]);

				    $.ajax({
				        url: '<?php echo $action ?>', //Server script to process data
				        type: 'POST',
				       xhr: function () { // Custom XMLHttpRequest
					  var myXhr = jQuery.ajaxSettings.xhr();
					  if (myXhr.upload) { // Check if upload property exists
					      myXhr.upload.addEventListener('progress', uploadProgress4, false); // For handling the progress of the upload
					  }
					  return myXhr;
				        },
				        //Ajax events
				        beforeSend: function (stuff) {
					  console.log("BeforeSend");
					  console.log(stuff);
				        },
				        success: function (data) {
					  console.log("Success!");
					  console.log(data);
					  d = jQuery.parseJSON(data);
					  //if(d.type=="icon1"){
						  jQuery("#icon4").val(d.name);
					  //}
				        },
				        error: function (error) {
					  console.log("Error!");
					  console.log(error);
				        },
				        // Form data
				        data: formData,
				        //Options to tell jQuery not to process data or worry about content-type.
				        cache: false,
				        contentType: false,
				        processData: false
				    });	
				    
					    function uploadProgress4(evt) {
						selId	= document.getElementById('progressNumberIcon4');
						if (evt.lengthComputable) {
							var percentComplete = Math.round(evt.loaded * 100 / evt.total);
							selId.innerHTML = percentComplete.toString() + '% File Uploaded.';
						}
						else {
							selId.innerHTML = 'unable to upload';
					        }
					      }
				    
				 });
				 
				   jQuery(".selectFile5").change(function(event) {
				     event.preventDefault();
				    var formData = new FormData($("#lifestyle")[0]);

				    $.ajax({
				        url: '<?php echo $action ?>', //Server script to process data
				        type: 'POST',
				       xhr: function () { // Custom XMLHttpRequest
					  var myXhr = jQuery.ajaxSettings.xhr();
					  if (myXhr.upload) { // Check if upload property exists
					      myXhr.upload.addEventListener('progress', uploadProgress5, false); // For handling the progress of the upload
					  }
					  return myXhr;
				        },
				        //Ajax events
				        beforeSend: function (stuff) {
					  console.log("BeforeSend");
					  console.log(stuff);
				        },
				        success: function (data) {
					  console.log("Success!");
					  console.log(data);
					  d = jQuery.parseJSON(data);
					  //if(d.type=="icon1"){
						  jQuery("#icon5").val(d.name);
					  //}
				        },
				        error: function (error) {
					  console.log("Error!");
					  console.log(error);
				        },
				        // Form data
				        data: formData,
				        //Options to tell jQuery not to process data or worry about content-type.
				        cache: false,
				        contentType: false,
				        processData: false
				    });	
				    
					    function uploadProgress5(evt) {
						selId	= document.getElementById('progressNumberIcon5');
						if (evt.lengthComputable) {
							var percentComplete = Math.round(evt.loaded * 100 / evt.total);
							selId.innerHTML = percentComplete.toString() + '% File Uploaded.';
						}
						else {
							selId.innerHTML = 'unable to upload';
					        }
					      }
				    
				 });
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
