<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$server = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	if ($_REQUEST['action'] == 'intercomExtension') {
		$checkbox = $_REQUEST['intercomId'];
		for ($i=0; $i<count($checkbox);$i++){
			$intercomExtensionSelected = IntercomExtensionQuery::create()->findOneById($checkbox[$i]);
			$intercomExtensionSelected->delete();
		}
	} else if ($_REQUEST['action'] == 'videoCallSetting') {
		$sid = isset($_REQUEST['sid']) ? $_REQUEST['sid'] : '';
		$callDuration = isset($_REQUEST['callDuration']) ? $_REQUEST['callDuration'] : '';
		$server = isset($_REQUEST['server']) ? $_REQUEST['server'] : '';
		$username = isset($_REQUEST['username']) ? $_REQUEST['username'] : '';
		$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : '';
		$callLimitEnable = isset($_REQUEST['callLimitEnable']) ? $_REQUEST['callLimitEnable'] : '';
		$tenantCallDuration = isset($_REQUEST['tenantCallDuration']) ? $_REQUEST['tenantCallDuration'] : '';
					
		if ($sid == '') {
			$uniqid = uniqid();
			$videoCallSettingSelected = new VideoCallSetting();
			$videoCallSettingSelected->setID($uniqid);
			$videoCallSettingSelected->setProjectId($_SESSION['projectId']);
			$videoCallSettingSelected->setServer($server);
			$videoCallSettingSelected->setUsername($username);
			$videoCallSettingSelected->setPassword($password);
			$videoCallSettingSelected->setCallDuration($callDuration);
			$videoCallSettingSelected->setCallLimitEnable($callLimitEnable);
			$videoCallSettingSelected->setTenantCallDuration($tenantCallDuration);
			$videoCallSettingSelected->setModifiedBy($_SESSION['username']);
			$videoCallSettingSelected->setModifiedDate(date('Y-m-d H:i:s'));
			$videoCallSettingSelected->save();
			
			header("Location: ./intercomList.php");
			die();
		} else {
			$videoCallSettingSelected = VideoCallSettingQuery::create()->findPK($sid);
			$videoCallSettingSelected->setServer($server);
			$videoCallSettingSelected->setUsername($username);
			$videoCallSettingSelected->setPassword($password);
			$videoCallSettingSelected->setCallDuration($callDuration);
			$videoCallSettingSelected->setCallLimitEnable($callLimitEnable);
			$videoCallSettingSelected->setTenantCallDuration($tenantCallDuration);
			$videoCallSettingSelected->setModifiedBy($_SESSION['username']);
			$videoCallSettingSelected->setModifiedDate(date('Y-m-d H:i:s'));
			$videoCallSettingSelected->save();
			
			header("Location: ./intercomList.php");
			die();			
		}
	}
}

?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['System Configuration'] = true; ?>			
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Intercom List
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> System Configuration <span class="divider">/</span> Intercom
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Intercom List</h4>
						</div>
						
						<div class="widget-body">
							<?php
								$intercomExtensionList = IntercomExtensionQuery::create()->filterByProjectId($_SESSION['projectId'])->find()->getData();
								if ($intercomExtensionList == '') {
								
								} else if (count($intercomExtensionList) == 0) {
							?>
								No record found on intercom. Please add.
							<?php
								} else {
							?>
							<form action="./intercomList.php" method="post" class="form-horizontal" id="frmList">
							<table class="table table-striped table-bordered" id="sample_1">
								<thead>
									<tr>
										<th style="width:8px"><input type="checkbox" class="group-checkable" data-set=".checkboxes" /></th>
										<th>Intercom To</th>
										<th>Extension No</th>
										<th>FTP URL </th>										<th>Type</th>
										<th class="hidden-phone">Created By</th>
										<th class="hidden-phone">Created Date</th>
									</tr>
								</thead>
								<tbody>
							<?php
									foreach ($intercomExtensionList as $intercomExtension) {
							?>									
									<tr class="odd gradeX">
										<td><input type="checkbox" class="checkboxes" value="<?php echo $intercomExtension->getId(); ?>" name="intercomId[]" /></td>
										<td><a href="intercomDetails.php?sid=<?php echo $intercomExtension->getId(); ?>"><?php echo $intercomExtension->getIntercomTo(); ?></a></td>
										<td><?php echo $intercomExtension->getExtensionNo(); ?></td>
										<td><?php echo $intercomExtension->getIpAddress(); ?></td>
										<td><?php echo $intercomExtension->getCameraType(); ?></td>
										<td class="hidden-phone"><?php echo $intercomExtension->getCreatedBy(); ?></td>
										<td class="hidden-phone"><?php echo $intercomExtension->getCreatedDate()->format('d/m/Y H:i A'); ?></td>
									</tr>
							<?php
									}
							?>
								</tbody>
							</table>
							<input type="hidden" name="action" value="intercomExtension">
							</form>
							<?php
								}
							?>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary" onclick="window.location.href='intercomAdd.php'">Add</button>
							<a href="#dialogConfirmDelete" role="button" class="btn" data-toggle="modal">Delete</a>
							
							<div id="dialogConfirmDelete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h3 id="myModalLabel3">Confirm?</h3>
								</div>
								<div class="modal-body">
									<p>Confirm delete the selected intercom?</p>
								</div>
								<div class="modal-footer">
									<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
									<button data-dismiss="modal" class="btn btn-primary" onclick="document.forms['frmList'].submit();">Confirm</button>
								</div>
							</div>
						</div>
					</div>
					<!-- END FORM-->
					
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Intercom Setting</h4>
						</div>
						
						<div class="widget-body">
							<?php
								$sid = '';
								$callDuration = '';
								$callLimitEnable = '';
								$tenantCallDuration = '';
								
								$videoCallSetting = VideoCallSettingQuery::create()->filterByProjectId($_SESSION['projectId'])->find()->getData();
								if ($videoCallSetting == '') {
								
								} else if (count($videoCallSetting) == 0) {

								} else {
									$videoCallSetting = $videoCallSetting[0];
									$sid = $videoCallSetting->getId();
									$projectId = $videoCallSetting->getProjectId();
									$server = $videoCallSetting->getServer();
									$username = $videoCallSetting->getUsername();
									$password = $videoCallSetting->getPassword();
									$callDuration = $videoCallSetting->getCallDuration();
									$callLimitEnable = $videoCallSetting->getCallLimitEnable();
									$tenantCallDuration = $videoCallSetting->getTenantCallDuration();
								}
							?>
							<form action="./intercomList.php" method="post" class="form-horizontal">
								<div class="control-group">
									<label class="control-label" for="server">Server</label>
									<div class="controls">
									<input type="text" class="span6" id="server" placeholder="example : 192.168.1.12" name="server"  value="<?php echo $server; ?>"/>
									</div>
								</div>
								<!--<div class="control-group">
									<label class="control-label" for="username">Username</label>
									<div class="controls">
									<input type="text" class="span6" id="username" placeholder="example : 010101" name="username"  value="<?php echo $username; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="password">Password</label>
									<div class="controls">
									<input type="text" class="span6" id="password" placeholder="example : password" name="password"  value="<?php echo $password; ?>"/>
									</div>
								</div>-->
								<div class="control-group">
									<label class="control-label" for="input2">Call Duration</label>
									<div class="controls">
										<label><?php echo ($callDuration == '') ? 'Not Set': $callDuration; ?></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">New Duration</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="New Duration (in seconds)" name="callDuration" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Tenant Call Limit</label>
									<div class="controls">
										<input type="checkbox" value="1" name="callLimitEnable" <?php if ($callLimitEnable == 1) echo "checked=\"1\""; ?> /> 
									</div>
								</div>                             
								<div class="control-group">
									<label class="control-label" for="input1">Tenant Duration</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Tenant Call Duration (in seconds)" name="tenantCallDuration" value="<?php echo $tenantCallDuration; ?>" />
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Update</button>
									<input type="hidden" name="action" value="videoCallSetting">
									<input type="hidden" name="sid" value="<?php echo $sid; ?>">
								</div>
							</form>
						</div>
					</div>
					<!-- END FORM-->					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
