<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$bookingRecord = '';

$bookingNumber = isset($_REQUEST['bookingNumber']) ? $_REQUEST['bookingNumber'] : '';
if ($bookingNumber == '') {
  echo 'We are unable to proceed further with you request. Please try again. If this problem persists, please contact Administrator.';
  die();
} else {
  $bookingRecord = FacilityBookingQuery::create()->filterByBookingNumber($bookingNumber)->find();
  
  if (count($bookingRecord) == 0) {
    echo '1We are unable to proceed further with you request. Please try again. If this problem persists, please contact Administrator.';
    die();
  } else {
    $bookingRecord = $bookingRecord[0];
    
    $appClientSetting = AppClientSettingQuery::create()->filterByProjectId($_SESSION['projectId'])->find()->getData();
  }
?>
<html>
  <head></head>
  <body onload="window.print();">
    <img src="<?php echo $imgUrl.'/'.$appClientSetting[0]->getReceiptLogoImage().'/ebulletin'; ?>" width="500" height="45">
      <br>
        <p><b>Facility Booking</b></p>
      <hr>
      <table border="0">
<tbody><tr>
<td>Booking Number</td>
<td>:</td>
<td><?php echo $bookingRecord->getBookingNumber(); ?></td>
</tr>
<tr>
<td>Facility Name</td>
<td>:</td>
<td><?php echo $bookingRecord->getFacilityName(); ?></td>
</tr>
<tr>
<td>Unit ID</td>
<td>:</td>
<td><?php echo $bookingRecord->getTenantId(); ?></td>
</tr>
<tr>
<td>Booking Owner</td>
<td>:</td>
<td><?php echo $bookingRecord->getBookingOwner(); ?></td>
</tr>
<tr>
<td>Booking Date</td>
<td>:</td>
<td><?php echo $bookingRecord->getBookingDate()->format('d/m/Y'); ?></td>
</tr>
<tr>
<td>Time Slot</td>
<td>:</td>
<td><?php echo $bookingRecord->getBookingTime(); ?></td>
</tr>
<tr>
<td>Booking Status</td>
<td>:</td>
<td><?php echo $bookingRecord->getBookingStatus(); ?></td>
</tr>
<tr>
<td>Deposit (RM)</td>
<td>:</td>
<td><?php echo number_format($bookingRecord->getDeposit(), 2, '.', ''); ?></td>
</tr>
<tr>
<td>Charge (RM)</td>
<td>:</td>
<td><?php echo number_format($bookingRecord->getCharge(), 2, '.', ''); ?></td>
</tr>
<tr>
<td>Total (RM)</td>
<td>:</td>
<td><?php echo number_format($bookingRecord->getTotal(), 2, '.', ''); ?></td>
</tr>
<tr>
<td>Payment Date</td>
<td>:</td>
<td><?php echo $bookingRecord->getPaymentDate()->format('d/m/Y H:i A'); ?></td>
</tr>
</tbody></table>
</body></html>
<?php
}
?>
