		<?php
 
require_once '../vendor/autoload.php';
require_once '../generated-conf/config.php';

	/*
	* @author Hardi
	* @since 1.0.0 FacilityCoolingOff
	* @param string $projectID get from sh_project
	* @param string $bookingNumber
	* return boolean
	 */
	function BookingFacilityCoolingOff(){
		//$facility 	= new FacilityBookingQuery();
		$facilityList 	= getAllFacilityList();
		//print_r($facilityList);
		$day = date('Y-m-d H:i:s');
		$NewDate = new DateTime($day);
		$coolingDate = "";
		$facilityCheck = "";
		$fbId	= array();
		foreach($facilityList as $f){
			
			$coolingDate = date('Y-m-d H:i:s',strtotime($day . "-".$f->getCoolingOff()." days"));	
			
			//get Tenant Id First
			$facilityCheck = FacilityBookingQuery::create()
						->filterByFacilityId($f->getId())
						->filterByCreatedDate(array('max'=>$coolingDate))
						->filterByBookingStatus('PRE-BOOK')
						->find();
			if($facilityCheck){
				$fbId = $facilityCheck->toArray();
				//print_r($fbId);exit;
				foreach($fbId as $id){
					$FacilityCancel = FacilityBookingQuery::create()
								->filterById($id['Id'])
								//->filterByBookingDate(array('max'=>$coolingDate))
								//->filterByBookingStatus('PRE-BOOK')
								//->where('DATE(BookingDate) <= CURDATE() + INTERVAL "'.$f->getCoolingOff().'" DAY')
								//->where('FacilityId="'.$f->getId().'" BookingDate <= "'.$coolingOffDate.'"')
								->update(array('BookingStatus'=>'AUTO CANCELLED',
											'PaymentStatus'=>'AUTO CANCELLED',
											'CancelDate'=>date('Y-m-d H:i:s'),
											'CancelReason'=>'Cooling Off Payment expired',
											'CancelBy'=>'SYSTEM',
											'ModifiedDate'=>date('Y-m-d H:i:s')
										));
					if($FacilityCancel){
						$data['ProjectId']		= $id['ProjectId'];
						$data['Subject']		= "Facility Booking Notification";
						$data['PreviewMessage']	= "Auto Cancellation of Booking Facility";
						$data['Message']		= "
						<style>
						table tr td {
							font-family: arial;
						}
						</style>
						Auto Cancellation of a Booking on facility ".$id['FacilityName']." <br />
										<table>
											<tr><td><b>Booking Number:</b></td><tr>
											<tr><td> ".$id['BookingNumber']."<p>&nbsp;</p></td></tr>
											
											<tr><td><b>Transaction Date:</b></td></tr>
											<tr><td> ".date('l, jS F Y h:i A',strtotime($id['CreatedDate']))."<p>&nbsp;</p></td></tr>
											
											<tr><td><b>Booking Date:</b></td></tr>
											<tr><td> ".date('l, jS F Y h:i A',strtotime($id['BookingDate']))."<p>&nbsp;</p></td></tr>
											
											<tr><td><b>Time Slot:</b></td></tr>
											<tr><td> ".$id['BookingTime']."<p>&nbsp;</p></td></tr>
											
											<tr><td><b>Booking Status:</b></td></tr>
											<tr><td> AUTO CANCELLED<p>&nbsp;</p></td></tr>
											
											<tr><td><b>Cancellation Date:</b></td></tr>
											<tr><td> ".date('l, jS F Y h:i A')."<p>&nbsp;</p></td></tr>
										</table>
										";
						$data['UserId']	= $id['BookingOwner'];
						$data['TenantId']	= $id['TenantId'];
						EBulletinQuery::postEbulletin($data);
						
						//push notification
						$formatImei = \ShAccountQuery::getImei(null, $tenantId);
						$msgBody	= "Facility Booking Autocancelled.";
						$arrPost = array();
						$arrPost['MSG_ID']		= hash( 'sha256', "rakanoth".time());
						$arrPost['TYPE']		= '4';
						$arrPost['DESC']		= 'AUTOCANCELLED';
						$arrPost['FB_NUMBER'] 	= $id['BookingNumber'];
						$arrPost['FB_DATE'] 	= $id['BookingDate'];
						$arrPost['FB_ADMINTID'] = $id['TenantId'];
						$arrPost['FB_ADMINUID'] = $id['BookingOwner'];
						$arrPost['TIME']		= time();
						MessageQuery::pushNotif($formatImei, $msgBody, $arrPost);
						//end push notification
						
						//delete reminder
						ReminderQuery::create()->filterByUniqId($id['UniqId'])->delete();
					}
					echo "Booking Number : ".$id['BookingNumber']. " has been auto cancelled";
				}
			}
			
		}
	}
	
	/*
	* @author Hardi
	* @since 1.0.0 getAllFacilityList
	* @param string $facilityID
	* return mixed[] facility
	 */
	function getAllFacilityList(){
		$Facility = FacilityQuery::create()->find()->getData();
		return $Facility;
	}


	BookingFacilityCoolingOff();
	
?>
