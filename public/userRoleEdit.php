<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$errUserRole = '';
$errDescription = '';
$errModule = '';
$checkbox = array();

if ($_REQUEST['sid'] == '') {
	header("Location: ./facilityManagementSettingsList.php");
	die();
} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$hasError = false;
	
	if ($_REQUEST['userRole'] == '') {
		$hasError = true;
		$errUserRole = 'Please enter the user role name.';
	}

	if ($_REQUEST['description'] == '') {
		$hasError = true;
		$errDescription = 'Please enter the description.';
	}
	
	if (!isset($_REQUEST['module'])) {
		$hasError = true;
		$errModule = 'Please select the module for the user role.';
	}
	
	if (!$hasError) {
		$userRole = UserRoleQuery::create()->findPK($_REQUEST['sid']);
		$userRole->setName($_REQUEST['userRole']);
		$userRole->setDescription($_REQUEST['description']);
		$userRole->setActive(isset($_REQUEST['active']) ? $_REQUEST['active'] : 0);
		$userRole->setModifiedBy($_SESSION['username']);
		$userRole->setModifiedDate(date('Y-m-d H:i:s'));
		$userRole->save();
		
		UserRoleModuleQuery::create()->filterByUserRoleId($_REQUEST['sid'])->delete();
		$checkbox = $_REQUEST['module'];
		for ($i=0; $i<count($checkbox); $i++){
			$userRoleModule = new UserRoleModule();
			$userRoleModule->setId(uniqid());
			$userRoleModule->setUserRoleId($_REQUEST['sid']);
			$userRoleModule->setModuleId($checkbox[$i]);
			$userRoleModule->save();
		}

		header("Location: ./userRoleListing.php");
		die();		
	} else {
		$userRole = $_REQUEST['userRole'];
		$description = $_REQUEST['description'];
		$active = isset($_REQUEST['active']) ? $_REQUEST['active'] : 0;
		$checkbox = isset($_REQUEST['module']) ? $_REQUEST['module'] : array();
	}
} else {
	$userRoleSelected = UserRoleQuery::create()->findPK($_REQUEST['sid']);
	if (count($userRoleSelected) == 0) {
		header("Location: ./userRoleListing.php");
		die();
	} else {
		$sid = $userRoleSelected->getId();
		$userRole = $userRoleSelected->getName();
		$description = $userRoleSelected->getDescription();
		$active = $userRoleSelected->getActive();
		
		$moduleResult = UserRoleModuleQuery::create()->filterByUserRoleId($sid)->orderByModuleId()->find()->getData();
		
		$i = 0;
		foreach ($moduleResult as $moduleSelected) {
			$checkbox[$i] = $moduleSelected->getModuleId();
			$i++;
		}
	}
}
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['User Management'] = true; ?>
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							User Role Creation
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> User Management <span class="divider">/</span> User Role Listing <span class="divider">/</span> User Role Creation
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<!-- BEGIN FORM-->
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>User Role Creation</h4>
						</div>
							<!-- BEGIN FORM-->
							<div class="widget-body form">
							<form action="./userRoleEdit.php" method="post" class="form-horizontal">
								<div class="control-group <?php if ($errUserRole <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">User Role Name</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="User Role Name" name="userRole" value="<?php echo $userRole; ?>"/>
										<span class="help-block"><?php echo $errUserRole; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errDescription <> '') echo 'has-error'; ?>">
									<label class="control-label" for="inputRemarks">Role Description</label>
									<div class="controls">
										<textarea class="form-control span6" rows="3" id="inputRemarks" placeholder="Role Description" name="description"><?php echo $description; ?></textarea>
										<span class="help-block"><?php echo $errDescription; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Enabled</label>
									<div class="controls">
										<label class="checkbox">
										<input type="checkbox" value="1" name="active" <?php if ($active == 1) echo 'checked=1'; ?> />
										</label>
									</div>
								</div>                             
								
								<!-- BEGIN TABLE PORTLET-->
								<div class="widget">
									<div class="widget-title">
										<h4><i class="icon-reorder"></i>Modules</h4>
									</div>

									<div class="widget-body <?php if ($errModule <> '') echo 'has-error'; ?>">
									<?php
										$moduleList = ModuleQuery::create()->find()->getData();

										if (count($moduleList) == 0) {
									?>
										No record found. Please contact Administrator to add Module.
									<?php
										} else {
									?>							
										<table class="table table-striped table-bordered form-control">
											<tbody>
									<?php
										foreach ($moduleList as $module) {
									?>
												<tr class="odd gradeX ">
													<td><input type="checkbox" class="checkboxes" value="<?php echo $module->getId(); ?>" name="module[]" <?php if (array_search($module->getId(), $checkbox) !== false) echo ' checked'; ?>/></td>
													<td><?php echo $module->getModule(); ?></td>
												</tr>
									<?php
											}
									?>
											</tbody>
										</table>
										<span class="help-block"><?php echo $errModule; ?></span>
									<?php
										}
									?>
									</div>
								</div>
								<!-- END FORM-->
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Update</button>
									<button type="reset" class="btn">Clear</button>
									<button type="button" class="btn" onclick="window.location.href='userRoleListing.php'">Cancel</button>
									<input type="hidden" name="sid" value="<?php echo $_REQUEST['sid'];?>">
								</div>
							</form>
						</div>
					</div>
					<!-- END FORM-->
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>