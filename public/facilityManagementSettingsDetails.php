<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$errName = '';
$errAdvancedBooking = '';
$errCoolingOff = '';

if ($_REQUEST['sid'] == '') {
	header("Location: ./facilityManagementSettingsList.php");
	die();
}
else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//print_r($_POST);exit;
	$hasError = false;
	
	if ($_REQUEST['Name'] == '') {
		$hasError = true;
		$errName = 'Please enter the facility name.';
	}

	if ($_REQUEST['AdvancedBooking'] == '') {
		$hasError = true;
		$errAdvancedBooking = 'Please enter days of advanced booking needed.';
	}
	
	if ($_REQUEST['CoolingOff'] == '') {
		$hasError = true;
		$errCoolingOff = 'Please enter days of cooling off needed.';
	}
	
	$Rules = str_replace("\r\n","<br />",$_REQUEST['Rules']);
	if (!$hasError) {
		$facility = FacilityQuery::create()->findPK($_REQUEST['sid']);
		$facility->setName($_REQUEST['Name']);
		$facility->setType($_REQUEST['Type']);
		$facility->setRequireBooking(isset($_REQUEST['RequireBooking']) ? $_REQUEST['RequireBooking'] : 0);
		$facility->setAdvancedBooking($_REQUEST['AdvancedBooking']);
		$facility->setMaxHour($_REQUEST['MaxHour']);
		$facility->setCoolingOff($_REQUEST['CoolingOff']);
		$facility->setStartTime($_REQUEST['StartTime']);
		$facility->setEndTime($_REQUEST['EndTime']);
		$facility->setDeposit($_REQUEST['Deposit']);
		$facility->setCharge($_REQUEST['Charge']);
		$facility->setRules($Rules);
		$facility->setEnabled(isset($_REQUEST['EnableFlag']) ? $_REQUEST['EnableFlag'] : 0);
		$facility->setModifiedBy($_SESSION['username']);
		$facility->setModifiedDate(date('Y-m-d H:i:s'));
		$facility->save();
		
		//echo $facility->getType();exit;
		
		if ($facility->getType() == "Per Hour" || $facility->getType() == "Free") {
			$facilityTimeslots = FacilityTimeslotQuery::create()->filterByFacilityId($_REQUEST['sid'])->orderById()->find();
			if(sizeof($facilityTimeslots) > 0){
				foreach ($facilityTimeslots as $facilityTimeslot) {
					if (($_REQUEST['EndTime']) == '12:00 AM') {
						if ($facilityTimeslot->getStartTime()->format('H:i:s') >= date_format(date_create($_REQUEST['StartTime']), 'H:i:s')) {
							$facilityTimeslot->setActive(1);	
						} else {
							$facilityTimeslot->setActive(0);
						}
					} else {
						if ($facilityTimeslot->getEndTime()->format('H:i:s') == '00:00:00') {
							$facilityTimeslot->setActive(0);
						} else if (($facilityTimeslot->getStartTime()->format('H:i:s') >= date_format(date_create($_REQUEST['StartTime']), 'H:i:s')) &&
							($facilityTimeslot->getEndTime()->format('H:i:s') <= date_format(date_create($_REQUEST['EndTime']), 'H:i:s'))) {
							$facilityTimeslot->setActive(1);
						} else {
							$facilityTimeslot->setActive(0);
						}
					}
					$facilityTimeslot->save();
				}
		
			}else{
				for ($i = 0; $i < 24;$i++){
					$facilityTimeslot = new FacilityTimeslot();
					$facilityTimeslot->setId(uniqid().substr('00'.$i, -2));
					$facilityTimeslot->setFacilityId($facility->getId());
					$facilityTimeslot->setStartTime(substr('00'.$i, -2).':00:00');
					$facilityTimeslot->setEndTime(substr('00'.$i + 1, -2).':00:00');

					if (($_REQUEST['EndTime']) == '12:00 AM') {
						if ((substr('00'.$i, -2).':00:00') >= date_format(date_create($_REQUEST['StartTime']), 'H:i:s')) {
							$facilityTimeslot->setActive(1);	
						} else {
							$facilityTimeslot->setActive(0);
						}
					} else {
						if (((substr('00'.$i, -2).':00:00') >= date_format(date_create($_REQUEST['StartTime']), 'H:i:s')) &&
							((substr('00'.$i, -2).':00:00') < date_format(date_create($_REQUEST['EndTime']), 'H:i:s'))) {
							$facilityTimeslot->setActive(1);
						} else {
							$facilityTimeslot->setActive(0);
						}
					}
					$facilityTimeslot->save();
				}
			}
		}
		
		header("Location: ./facilityManagementSettingsList.php");
		die();
	} else {
		$sid = $_REQUEST['sid'];
		$name = $_REQUEST['Name'];
		$type = $_REQUEST['Type'];
		$requireBooking = isset($_REQUEST['RequireBooking']) ? $_REQUEST['RequireBooking'] : 0;
		$advancedBooking = $_REQUEST['AdvancedBooking'];
		$maxHour = $_REQUEST['MaxHour'];
		$coolingOff = $_REQUEST['CoolingOff'];
		$startTime = date_format(new DateTime($_REQUEST['StartTime']), 'H:i:s');
		$endTime = date_format(new DateTime($_REQUEST['EndTime']), 'H:i:s');
		$deposit = $_REQUEST['Deposit'];
		$charge = $_REQUEST['Charge'];
		$rules = $_REQUEST['Rules'];
		$enabled = isset($_REQUEST['EnableFlag']) ? $_REQUEST['EnableFlag'] : 0;	
	}
} else {
	$facility = FacilityQuery::create()->findPK($_REQUEST['sid']);
	if (count($facility) == 0) {
		header("Location: ./facilityManagementSettingsList.php");
		die();
	} else {
		$sid = $facility->getId();
		$name = $facility->getName();
		$type = $facility->getType();
		$requireBooking = $facility->getRequireBooking();
		$advancedBooking = $facility->getAdvancedBooking();
		$maxHour = $facility->getMaxHour();
		$coolingOff = $facility->getCoolingOff();
		$startTime = $facility->getStartTime()->format('H:i:s');
		$endTime = $facility->getEndTime()->format('H:i:s');
		$deposit = $facility->getDeposit();
		$charge = $facility->getCharge();
		$rules = str_replace("<br />","\n",$facility->getRules());
		$enabled = $facility->getEnabled();
	}
}
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['System Configuration'] = true; ?>				
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Facility Management Settings Details
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Operation Management <span class="divider">/</span> Facility Management Settings Details
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Facility Management Settings Details</h4>
						</div>
						
						<div class="widget-body">
							<form action="./facilityManagementSettingsDetails.php" method="post" class="form-horizontal">
								<div class="control-group <?php if ($errName <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Name</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Name" name="Name" value="<?php echo $name; ?>" />
										<span class="help-block"><?php echo $errName; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Type</label>
									<div class="controls">
										<select class="span6" data-placeholder="Please Select" tabindex="1" name="Type">
											<option value="Free"<?php if ($type == 'Free') echo ' selected'; ?>>Free</option>
											<option value="Per Hour"<?php if ($type == 'Per Hour') echo ' selected'; ?>>Per Hour</option>
											<option value="Per Day"<?php if ($type == 'Per Day') echo ' selected'; ?>>Per Day</option>
										</select>
										<?php //echo $type ?>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Require Booking</label>
									<div class="controls">
										<input type="checkbox" value="1" <?php if ($requireBooking == 1) echo 'checked=1'; ?> name="RequireBooking" /> 
									</div>
								</div>                             
								<div class="control-group <?php if ($errAdvancedBooking <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Advanced Booking Day</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Advanced Booking Day" name="AdvancedBooking" value="<?php echo $advancedBooking; ?>" />
										<span class="help-block"><?php echo $errAdvancedBooking; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Max Hour</label>
									<div class="controls">
										<select class="span6" data-placeholder="Please Select" tabindex="1" name="MaxHour">
											<option value="0"<?php if ($maxHour == '0') echo ' selected'; ?>>0</option>
											<option value="1"<?php if ($maxHour == '1') echo ' selected'; ?>>1</option>
											<option value="2"<?php if ($maxHour == '2') echo ' selected'; ?>>2</option>
											<option value="3"<?php if ($maxHour == '3') echo ' selected'; ?>>3</option>
											<option value="4"<?php if ($maxHour == '4') echo ' selected'; ?>>4</option>
											<option value="5"<?php if ($maxHour == '5') echo ' selected'; ?>>5</option>
											<option value="6"<?php if ($maxHour == '6') echo ' selected'; ?>>6</option>
											<option value="7"<?php if ($maxHour == '7') echo ' selected'; ?>>7</option>
											<option value="8"<?php if ($maxHour == '8') echo ' selected'; ?>>8</option>
											<option value="9"<?php if ($maxHour == '9') echo ' selected'; ?>>9</option>
											<option value="10"<?php if ($maxHour == '10') echo ' selected'; ?>>10</option>
											<option value="11"<?php if ($maxHour == '11') echo ' selected'; ?>>11</option>
											<option value="12"<?php if ($maxHour == '12') echo ' selected'; ?>>12</option>
										</select>
									</div>
								</div>
								<div class="control-group <?php if ($errCoolingOff <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Cooling Off Day</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Cooling Off Day" name="CoolingOff" value="<?php echo $coolingOff ?>"/>
										<span class="help-block"><?php echo $errCoolingOff; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Start Time</label>
									<div class="controls">
										<select class="span6" data-placeholder="Please Select" tabindex="1" name="StartTime">
											<option value="12:00 AM"<?php if ($startTime == '00:00:00') echo ' selected'; ?>>12:00 AM</option>
											<option value="01:00 AM"<?php if ($startTime == '01:00:00') echo ' selected'; ?>>01:00 AM</option>
											<option value="02:00 AM"<?php if ($startTime == '02:00:00') echo ' selected'; ?>>02:00 AM</option>
											<option value="03:00 AM"<?php if ($startTime == '03:00:00') echo ' selected'; ?>>03:00 AM</option>
											<option value="04:00 AM"<?php if ($startTime == '04:00:00') echo ' selected'; ?>>04:00 AM</option>
											<option value="05:00 AM"<?php if ($startTime == '05:00:00') echo ' selected'; ?>>05:00 AM</option>
											<option value="06:00 AM"<?php if ($startTime == '06:00:00') echo ' selected'; ?>>06:00 AM</option>
											<option value="07:00 AM"<?php if ($startTime == '07:00:00') echo ' selected'; ?>>07:00 AM</option>
											<option value="08:00 AM"<?php if ($startTime == '08:00:00') echo ' selected'; ?>>08:00 AM</option>
											<option value="09:00 AM"<?php if ($startTime == '09:00:00') echo ' selected'; ?>>09:00 AM</option>
											<option value="10:00 AM"<?php if ($startTime == '10:00:00') echo ' selected'; ?>>10:00 AM</option>
											<option value="11:00 AM"<?php if ($startTime == '11:00:00') echo ' selected'; ?>>11:00 AM</option>
											<option value="12:00 PM"<?php if ($startTime == '12:00:00') echo ' selected'; ?>>12:00 PM</option>
											<option value="01:00 PM"<?php if ($startTime == '13:00:00') echo ' selected'; ?>>01:00 PM</option>
											<option value="02:00 PM"<?php if ($startTime == '14:00:00') echo ' selected'; ?>>02:00 PM</option>
											<option value="03:00 PM"<?php if ($startTime == '15:00:00') echo ' selected'; ?>>03:00 PM</option>
											<option value="04:00 PM"<?php if ($startTime == '16:00:00') echo ' selected'; ?>>04:00 PM</option>
											<option value="05:00 PM"<?php if ($startTime == '17:00:00') echo ' selected'; ?>>05:00 PM</option>
											<option value="06:00 PM"<?php if ($startTime == '18:00:00') echo ' selected'; ?>>06:00 PM</option>
											<option value="07:00 PM"<?php if ($startTime == '19:00:00') echo ' selected'; ?>>07:00 PM</option>
											<option value="08:00 PM"<?php if ($startTime == '20:00:00') echo ' selected'; ?>>08:00 PM</option>
											<option value="09:00 PM"<?php if ($startTime == '21:00:00') echo ' selected'; ?>>09:00 PM</option>
											<option value="10:00 PM"<?php if ($startTime == '22:00:00') echo ' selected'; ?>>10:00 PM</option>
											<option value="11:00 PM"<?php if ($startTime == '23:00:00') echo ' selected'; ?>>11:00 PM</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">End Time</label>
									<div class="controls">
										<select class="span6" data-placeholder="Please Select" tabindex="1" name="EndTime">
											<option value="01:00 AM"<?php if ($endTime == '01:00:00') echo ' selected'; ?>>01:00 AM</option>
											<option value="02:00 AM"<?php if ($endTime == '02:00:00') echo ' selected'; ?>>02:00 AM</option>
											<option value="03:00 AM"<?php if ($endTime == '03:00:00') echo ' selected'; ?>>03:00 AM</option>
											<option value="04:00 AM"<?php if ($endTime == '04:00:00') echo ' selected'; ?>>04:00 AM</option>
											<option value="05:00 AM"<?php if ($endTime == '05:00:00') echo ' selected'; ?>>05:00 AM</option>
											<option value="06:00 AM"<?php if ($endTime == '06:00:00') echo ' selected'; ?>>06:00 AM</option>
											<option value="07:00 AM"<?php if ($endTime == '07:00:00') echo ' selected'; ?>>07:00 AM</option>
											<option value="08:00 AM"<?php if ($endTime == '08:00:00') echo ' selected'; ?>>08:00 AM</option>
											<option value="09:00 AM"<?php if ($endTime == '09:00:00') echo ' selected'; ?>>09:00 AM</option>
											<option value="10:00 AM"<?php if ($endTime == '10:00:00') echo ' selected'; ?>>10:00 AM</option>
											<option value="11:00 AM"<?php if ($endTime == '11:00:00') echo ' selected'; ?>>11:00 AM</option>
											<option value="12:00 PM"<?php if ($endTime == '12:00:00') echo ' selected'; ?>>12:00 PM</option>
											<option value="01:00 PM"<?php if ($endTime == '13:00:00') echo ' selected'; ?>>01:00 PM</option>
											<option value="02:00 PM"<?php if ($endTime == '14:00:00') echo ' selected'; ?>>02:00 PM</option>
											<option value="03:00 PM"<?php if ($endTime == '15:00:00') echo ' selected'; ?>>03:00 PM</option>
											<option value="04:00 PM"<?php if ($endTime == '16:00:00') echo ' selected'; ?>>04:00 PM</option>
											<option value="05:00 PM"<?php if ($endTime == '17:00:00') echo ' selected'; ?>>05:00 PM</option>
											<option value="06:00 PM"<?php if ($endTime == '18:00:00') echo ' selected'; ?>>06:00 PM</option>
											<option value="07:00 PM"<?php if ($endTime == '19:00:00') echo ' selected'; ?>>07:00 PM</option>
											<option value="08:00 PM"<?php if ($endTime == '20:00:00') echo ' selected'; ?>>08:00 PM</option>
											<option value="09:00 PM"<?php if ($endTime == '21:00:00') echo ' selected'; ?>>09:00 PM</option>
											<option value="10:00 PM"<?php if ($endTime == '22:00:00') echo ' selected'; ?>>10:00 PM</option>
											<option value="11:00 PM"<?php if ($endTime == '23:00:00') echo ' selected'; ?>>11:00 PM</option>
											<option value="12:00 AM"<?php if ($endTime == '00:00:00') echo ' selected'; ?>>12:00 AM</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Deposit (RM)</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Deposit (RM)" name="Deposit" value="<?php echo number_format($deposit, 2, '.', ''); ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Charge (RM)</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Charge (RM)" name="Charge" value="<?php echo number_format($charge, 2, '.', ''); ?>" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="inputRemarks">Rules</label>
									<div class="controls">
										<textarea class="span6" rows="3" id="inputRemarks" name="Rules"><?php echo $rules; ?></textarea>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Enable Flag</label>
									<div class="controls">
										<input type="checkbox" value="1" <?php if ($enabled == 1) echo 'checked=1'; ?> name="EnableFlag" /> 
									</div>
								</div>                             

								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Update</button>
									<button type="reset" class="btn">Clear</button>
									<button type="button" class="btn" onclick="window.location.href='facilityManagementSettingsList.php'">Cancel</button>
									<input type="hidden" name="sid" value="<?php echo $sid;?>">
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
