<!-- BEGIN SIDEBAR -->
<div id="sidebar" class="nav-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->
	<ul>
		<?php if (isset($_SESSION['User Profile']) && $_SESSION['User Profile'] == 1) { ?>
		<li class="has-sub active"> 
			<a href="javascript:;" class="">
			<i class="icon-user"></i>User Profile
			<span class="arrow"></span>
			</a>
			<ul class="sub">
				<li><a class="" href="index.php">General</a></li>
				<li><a class="" href="changeLoginPassword.php">Change Login Password</a></li>
			</ul>
		</li>
		<?php } ?>
		
		<?php if (isset($_SESSION['User Management']) && $_SESSION['User Management'] == 1) { ?>
		<li class="has-sub">
			<a href="javascript:;" class="">
			<i class="icon-user"></i>User Management
			<span class="arrow"></span>
			</a>
			<ul class="sub">
				<li><a class="" href="userListing.php">User Listing</a></li>
				<li><a class="" href="userRoleListing.php">User Role Listing</a></li>
			</ul>
		</li>
		<?php } ?>
		
		<?php if (isset($_SESSION['System Configuration']) && $_SESSION['System Configuration'] == 1) { ?>				
		<li class="has-sub">
			<a href="javascript:;" class=""><i class="icon-tasks"></i>System Configuration<span class="arrow"></span>
			</a>
			<ul class="sub">
				<li><a class="" href="unitLayoutList.php">Unit Layout</a></li>
				<li><a class="" href="intercomAudioList.php">Intercom Audio</a></li>
				<li><a class="" href="tenantDeviceAccountList.php">Tenant Device Account</a></li>
				<li><a class="" href="guiMenuList.php">GUI Menu List</a></li>
				<li><a class="" href="commonAreaCamerasList.php">Common Area Cameras</a></li>
				<li><a class="" href="intercomList.php">Intercom</a></li>
				<li><a class="" href="alarmSetting.php">Alarm Setting</a></li>
				<li><a class="" href="tenantDetailsList.php">Tenant Details</a></li>
				<li><a class="" href="appClientSetting.php">App Client Setting</a></li>
				<li><a class="" href="deviceAppMonitoringList.php">Device Apps Monitoring</a></li>
				<li><a class="" href="propertyProjectList.php">Property Project</a></li>
			</ul>
		</li>
		<?php } ?>
		
		<?php if (isset($_SESSION['Operation Configuration']) && $_SESSION['Operation Configuration'] == 1) { ?>
		<li class="has-sub">
			<a href="javascript:;" class=""><i class="icon-tasks"></i>Operation Configuration<span class="arrow"></span>
			</a>
			<ul class="sub">
				<li><a class="" href="facilityManagementSettingsList.php">Facility Management Settings</a></li>
				<li><a class="" href="facilityBookingList.php">Facility Booking</a></li>
				<li><a class="" href="updateFacilityBookingStatusList.php">Update Booking Status</a></li>
				<li><a class="" href="eBulletinList.php">E-Bulletin</a></li>
			</ul>
		</li>
		<?php } ?>
		
		<?php if (isset($_SESSION['Reports']) && $_SESSION['Reports'] == 1) { ?>				
		<li class="has-sub">
			<a href="javascript:;" class=""><i class="icon-bookmark"></i>Reports<span class="arrow"></span>
			</a>
			<ul class="sub">
				<li><a class="" href="facilityBookingReport.php">Facility Booking Report</a></li>
				<li><a class="" href="tenantBookingReport.php">Tenant Booking Report</a></li>
				<li><a class="" href="tenantAlarmReport.php">Tenant Alarm Report</a></li>
				<li><a class="" href="userEventLog.php">User Event Log</a></li>
			</ul>
		</li>
		<?php } ?>
		
		<?php if (isset($_SESSION['Alarm CMS Status']) && $_SESSION['Alarm CMS Status'] == 1) { ?>				
		<li><a class="" href="alarmCmsStatus.php"><i class="icon-bell"></i>Alarm CMS Status</a></li>
		<?php } ?>	
		<li><a class="" href="community_board.php"><i class="icon-bell"></i>Community Board</a></li>	
	</ul>
	<!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
