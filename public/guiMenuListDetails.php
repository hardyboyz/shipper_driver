<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$name = '';
$description = '';
$image = '';
$sideMenuOrder = '';
$sideMenuEnable = '';
$homeMenuOrder = '';
$homeMenuEnable = '';
$topRightMenuOrder = '';
$topRightMenuEnable = '';
$tutorialImage = '';
$tutorialEnable = '';
$phoneTutorialEnable='';
$createdBy = '';
$createdDate = '';
$errName = '';
$errImage = '';
$errTutorialImage = '';

if ($_REQUEST['sid'] == '') {
	header("Location: ./guiMenuList.php");
	die();
} else if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_REQUEST['name'])) {
	$target = "./img/guiMenu/";
	$publicUrl = "http://localhost/smarthome-central-server/public/img/guiMenu/";
	//print_r($_GET['phoneIcon']); exit;
	$sid = isset($_REQUEST['sid']) ? $_REQUEST['sid'] : '';
	$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
	$description = isset($_REQUEST['description']) ? $_REQUEST['description'] : '';
	$image = $_REQUEST['image'];
	$imageTemp = isset($_REQUEST['imageTemp']) ? $_REQUEST['imageTemp'] : '';
	$sideMenuOrder = isset($_REQUEST['sideMenuOrder']) ? $_REQUEST['sideMenuOrder'] : '';
	$sideMenuEnable = isset($_REQUEST['sideMenuEnable']) ? $_REQUEST['sideMenuEnable'] : '';
	$homeMenuOrder = isset($_REQUEST['homeMenuOrder']) ? $_REQUEST['homeMenuOrder'] : '';
	$homeMenuEnable = isset($_REQUEST['homeMenuEnable']) ? $_REQUEST['homeMenuEnable'] : '';
	$topRightMenuOrder = isset($_REQUEST['topRightMenuOrder']) ? $_REQUEST['topRightMenuOrder'] : '';
	$topRightMenuEnable = isset($_REQUEST['topRightMenuEnable']) ? $_REQUEST['topRightMenuEnable'] : '';
	$tutorialImage = $_REQUEST['tutorialImage'];
	$tutorialEnable = isset($_REQUEST['tutorialEnable']) ? $_REQUEST['tutorialEnable'] : '';
	$tutorialImageTemp = isset($_REQUEST['tutorialImageTemp']) ? $_REQUEST['tutorialImageTemp'] : '';
	
	$phoneTutorialImage = isset($_REQUEST['phoneTutorialImage']) ? $_REQUEST['phoneTutorialImage'] : '';
	$phoneTutorialImageTemp = isset($_REQUEST['phoneTutorialImageTemp']) ? $_REQUEST['phoneTutorialImageTemp'] : '';
	
	$phoneTutorialEnable = isset($_REQUEST['phoneTutorialEnable']) ? $_REQUEST['phoneTutorialEnable'] : '';
	$phoneIcon = isset($_REQUEST['phoneIcon']) ? $_REQUEST['phoneIcon'] : '';
	$phoneIconTemp = isset($_REQUEST['phoneIconTemp']) ? $_REQUEST['phoneIconTemp'] : '';
	
	$createdBy = isset($_REQUEST['createdBy']) ? $_REQUEST['createdBy'] : '';
	$createdDate = isset($_REQUEST['createdDate']) ? $_REQUEST['createdDate'] : '';

	$hasError = false;
	
	if ($name == '') {
		$hasError = true;
		$errName = 'Please enter the app menu name.';
	}
			
	if (!$hasError) {
		$uniqid = uniqid();
		/*if ($image <> '') {
			if (move_uploaded_file($_FILES['image']['tmp_name'], $target.$uniqid.'-'.$image)) {
				//Tells you if its all ok
				$image = $target.$uniqid.'-'.$image;
			} else {
				$hasError = true;
				$errImage = "Error in uploading your app menu image file. Please try again.";
			}
		} 

		if ($tutorialImage <> '') {
			if (move_uploaded_file($_FILES['tutorialImage']['tmp_name'], $target.$uniqid.'-'.$tutorialImage)) {
				//Tells you if its all ok
				$tutorialImage = $target.$uniqid.'-'.$tutorialImage;
			} else {
				$hasError = true;
				$errImage = "Error in uploading your tutorial image file. Please try again.";
			}
		} */
				
		if (!$hasError) {		
			$appMenu = AppMenuListQuery::create()->findPK($sid);
			$appMenu->setName($name);
			$appMenu->setDescription($description);
			if ($image <> '') {
				$appMenu->setImage($image);
			}
			$appMenu->setSideMenuOrder($sideMenuOrder);
			$appMenu->setSideMenuEnable($sideMenuEnable);
			$appMenu->setHomeMenuOrder($homeMenuOrder);
			$appMenu->setHomeMenuEnable($homeMenuEnable);
			$appMenu->setTopRightMenuOrder($topRightMenuOrder);
			$appMenu->setTopRightMenuEnable($topRightMenuEnable);
			if ($tutorialImage <> '') {
				$appMenu->setTutorialImage($tutorialImage);
			}
			$appMenu->setTutorialEnable($tutorialEnable);	
			
			if ($phoneTutorialImage <> '') {
				$appMenu->setPhoneTutorialImage($phoneTutorialImage);
			}
			//$appMenu->setPhoneTutorialImage($phoneTutorialImage);		
			//$appMenu->setPhoneTutorialEnable($phoneTutorialEnable);
			
			if ($phoneIcon <> '') {
				$appMenu->setPhoneIcon($phoneIcon);
			}
			//$appMenu->setPhoneIcon($phoneIcon);
					
			$appMenu->setModifiedBy($_SESSION['username']);
			$appMenu->setModifiedDate(date('Y-m-d H:i:s'));
			$appMenu->save();
			
			header("Location: ./guiMenuList.php");
			die();
		}
	}
} else {
	$appMenuSelected = AppMenuListQuery::create()->findPK($_REQUEST['sid']);
	if (count($appMenuSelected) == 0) {
		header("Location: ./guiMenuList.php");
		die();
	} else {
		$sid = $appMenuSelected->getId();
		$name = $appMenuSelected->getName();
		$description = $appMenuSelected->getDescription();
		$image = $appMenuSelected->getImage();
		$imageTemp = $appMenuSelected->getImage();
		$sideMenuOrder = $appMenuSelected->getSideMenuOrder();
		$sideMenuEnable = $appMenuSelected->getSideMenuEnable();
		$homeMenuOrder = $appMenuSelected->getHomeMenuOrder();
		$homeMenuEnable = $appMenuSelected->getHomeMenuEnable();
		$topRightMenuOrder = $appMenuSelected->getTopRightMenuOrder();
		$topRightMenuEnable = $appMenuSelected->getTopRightMenuEnable();
		$tutorialImage = $appMenuSelected->getTutorialImage();
		$tutorialImageTemp = $appMenuSelected->getTutorialImage();
		$phoneTutorialImageTemp = $appMenuSelected->getPhoneTutorialImage();
		$phoneIconTemp = $appMenuSelected->getPhoneIcon();
		$phoneTutorialEnable = $appMenuSelected->getPhoneTutorialEnable();
		$tutorialEnable = $appMenuSelected->getTutorialEnable();
		$createdBy = $appMenuSelected->getCreatedBy();
		$createdDate = $appMenuSelected->getCreatedDate()->format('d/m/Y H:i A');
	}
}

?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['System Configuration'] = true; ?>
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							GUI Menu Creation
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> System Configuration <span class="divider">/</span> GUI Menu Creation
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>GUI Menu Creation</h4>
						</div>
						
						<div class="widget-body">
							<form action="<?php echo $action ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
								<div class="control-group <?php if ($errName <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Name</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Name" name="name" value="<?php echo $name; ?>"/>
										<span class="help-block"><?php echo $errName; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Description</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Description" name="description" value="<?php echo $description; ?>"/>
									</div>
								</div>
								<div class="control-group <?php if ($errImage <> '') echo 'has-error'; ?>">
									<label class="control-label">Icon (220x220)</label>								
									<div class="controls">															
										<div class="fileupload fileupload-exists" data-provides="fileupload">
												<div class="form-control fileupload-new thumbnail" style="width: 100px; height: 100px;"></div>
												<div class="form-control fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;"><?php if ($imageTemp <> '') { ?>
													<img src="<?php echo $imgUrl.'/'.$imageTemp.'/eBulletin'; ?>"><?php } ?></div>
												<div>
													<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default" name="image"></span>
													<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
												</div>
										</div>
										<span class="help-block"><?php echo $errImage; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Side Menu Order</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Side Menu Order" name="sideMenuOrder" value="<?php echo $sideMenuOrder; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Side Menu Enable</label>
									<div class="controls">
										<input type="checkbox" value="1" name="sideMenuEnable" <?php if ($sideMenuEnable == 1) echo "checked=\"1\""; ?>/> 
										<span class="label label-important">Important</span> Caution: disable the flag will affect the App Module shortcuts
									</div>
								</div>                             
								<div class="control-group">
									<label class="control-label" for="input1">Home Menu Order</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Home Menu Order" name="homeMenuOrder" value="<?php echo $homeMenuOrder; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Home Menu Enable</label>
									<div class="controls">
										<input type="checkbox" value="1" name="homeMenuEnable" <?php if ($homeMenuEnable == 1) echo "checked=\"1\""; ?>/> 
										<span class="label label-important">Important</span> Caution: disable the flag will affect the App Module shortcuts
									</div>
								</div>                             
								<div class="control-group">
									<label class="control-label" for="input1">Top Right Menu Order</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Top Right Menu Order" name="topRightMenuOrder" value="<?php echo $topRightMenuOrder; ?>" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Top Right Menu Enable</label>
									<div class="controls">
										<input type="checkbox" value="1" name="topRightMenuEnable" <?php if ($topRightMenuEnable == 1) echo "checked=\"1\""; ?>/> 
										<span class="label label-important">Important</span> Caution: disable the flag will affect the App Module shortcuts
									</div>
								</div>                             
								<div class="control-group <?php if ($errTutorialImage <> '') echo 'has-error'; ?>">
									<label class="control-label">Tutorial Image</label>
									<div class="controls">															
										<div class="fileupload <?php if ($tutorialImageTemp <> '') { echo 'fileupload-exists'; } else { echo 'fileupload-new'; } ?>" data-provides="fileupload">
												<div class="fileupload-new thumbnail" style="width: 100px; height: 100px;"></div>
												<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;">
												<?php if ($tutorialImageTemp <> '') { ?>												
												<img src="<?php echo $imgUrl.'/'.$tutorialImageTemp.'/eBulletin'; ?>" />
												<?php } ?>
												</div>
												<div>
														<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default" name="tutorialImage" /></span>
														<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
												</div>
										</div>
										<span class="help-block"><?php echo $errTutorialImage; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Enable Tutorial</label>
									<div class="controls">
										<input type="checkbox" value="1" name="tutorialEnable" <?php if ($tutorialEnable == 1) echo "checked=\"1\""; ?>/> 
										<span class="label label-important">Important</span> Caution: disable the flag will remove tutorial image
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Phone Tutorial Image</label>
									<div class="controls">															
										<div class="fileupload <?php if ($phoneTutorialImageTemp <> '') { echo 'fileupload-exists'; } else { echo 'fileupload-new'; } ?>" data-provides="fileupload">
												<div class="fileupload-new thumbnail" style="width: 100px; height: 100px;"></div>
												<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;">
												<?php if ($phoneTutorialImageTemp <> '') { ?>												
												<img src="<?php echo $imgUrl.'/'.$phoneTutorialImageTemp.'/eBulletin'; ?>" />
												<?php } ?>
												</div>
												<div>
														<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default" name="phoneTutorialImage" /></span>
														<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
												</div>
										</div>
										<span class="help-block"><?php  ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Enable Phone Tutorial</label>
									<div class="controls">
										<input type="checkbox" value="1" name="phoneTutorialEnable" <?php if ($phoneTutorialEnable == 1) echo "checked=\"1\""; ?>/> 
										<?php echo $phoneTutorialEnable ?>
										<span class="label label-important">Important</span> Caution: disable the flag will remove phone tutorial image
									</div>
								</div>     
								<div class="control-group">
									<label class="control-label">Phone Icon</label>
									<div class="controls">															
										<div class="fileupload <?php if ($phoneIconTemp <> '') { echo 'fileupload-exists'; } else { echo 'fileupload-new'; } ?>" data-provides="fileupload">
												<div class="fileupload-new thumbnail" style="width: 100px; height: 100px;"></div>
												<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;">
												<?php if ($phoneIconTemp <> '') { ?>												
												<img src="<?php echo $imgUrl.'/'.$phoneIconTemp.'/eBulletin'; ?>" />
												<?php } ?>
												</div>
												<div>
														<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default" name="phoneIcon" /></span>
														<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
												</div>
										</div>
										<span class="help-block"><?php  ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input2">Created By</label>
									<div class="controls">
										<label><?php echo $createdBy; ?></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input2">Created Date</label>
									<div class="controls">
										<label><?php echo $createdDate; ?></label>
									</div>
								</div>
								
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="reset" class="btn">Clear</button>
									<button type="button" class="btn" onclick="window.location.href='guiMenuList.php'">Cancel</button>
									<input type="hidden" name="sid" value="<?php echo $sid; ?>">
									<input type="hidden" name="imageTemp" value="<?php echo $imageTemp; ?>">
									<input type="hidden" name="tutorialImageTemp" value="<?php echo $tutorialImageTemp; ?>">
									<input type="hidden" name="createdBy" value="<?php echo $createdBy; ?>">
									<input type="hidden" name="createdDate" value="<?php echo $createdDate; ?>">									
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
