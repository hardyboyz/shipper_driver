<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';
$errBookingOwner = '';
$errTenantId = '';
$tenantId = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
	$facilityId = isset($_REQUEST['facilityId']) ? $_REQUEST['facilityId'] : '';
	$maxHour = isset($_REQUEST['maxHour']) ? $_REQUEST['maxHour'] : '';
	$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
	$advanceBooking = isset($_REQUEST['advanceBooking']) ? $_REQUEST['advanceBooking'] : 0;
	$timeslot = isset($_REQUEST['timeslot']) ? $_REQUEST['timeslot'] : '';
	$timeslotId = isset($_REQUEST['timeslotId']) ? $_REQUEST['timeslotId'] : '';
	$bookDate = isset($_REQUEST['bookDate']) ? $_REQUEST['bookDate'] : '';
	$deposit = isset($_REQUEST['deposit']) ? $_REQUEST['deposit'] : '';
	$charge = isset($_REQUEST['charge']) ? $_REQUEST['charge'] : '';
	$total = isset($_REQUEST['total']) ? $_REQUEST['total'] : '';
	$bookingOwner = isset($_REQUEST['bookingOwner']) ? $_REQUEST['bookingOwner'] : '';
	$tenantId = isset($_REQUEST['tenantId']) ? $_REQUEST['tenantId'] : '';
	
	$hasError = false;
	
	if ($bookingOwner == '') {
		$hasError = true;
		$errBookingOwner = 'Please enter the owner name of the booking.';
	}
	
	if ($tenantId == '') {
		$hasError = true;
		$errTenantId = 'Please select the tenant of the booking.';
	}
	
	if ($advanceBooking > 0) {
		$date1 = new DateTime(substr($bookDate, -4).'-'.substr($bookDate, 3, 2).'-'.substr($bookDate, 0, 2));
		$date2 = new DateTime(date('Y-m-d'));

		$diff = $date2->diff($date1)->format("%a");

		if (intVal($diff) < intVal($advanceBooking)) {
			echo "inside";
			$hasError = true;
			$errBookingOwner = 'Booking must be made ' . $advanceBooking . ' days in advance.';
		}	
	}
	
	if ($type == "Per Hour") {
		$getMaxHourBooked = FacilityBookingQuery::create()->filterByTenantId($tenantId)->filterByFacilityId($facilityId)->filterByBookingDate(substr($bookDate, -4).'-'.substr($bookDate, 3, 2).'-'.substr($bookDate, 0, 2))->find();
		
		$checkBooker = $getMaxHourBooked->toArray();
		$timeslotCount = 0;
		foreach($checkBooker as $cb){
			$checkDate = $cb['CreatedDate'];
			if(strpos($cb['BookingTimeslot'],"|") > 0){
				$arrTimeslotCount	= explode("|", $cb['BookingTimeslot']);//strpos($cb['BookingTimeslot'], "|");
				$timeslotCount = $timeslotCount + count($arrTimeslotCount);
			}else{
				$timeslotCount++;
			}
		}
		if ($timeslotCount >= $maxHour) {
			$hasError = true;
			$errBookingOwner = 'Max Hour for each Tenant to use ' . $name . " on a day is " . $maxHour . ". Tenant:" . $unitId . " has already used " . $timeslotCount . " hours.";
		}
	}
	
	if (!$hasError) {
		$projectId = $_SESSION['projectId'];
		$getMaxBookingNumber = FacilityBookingQuery::create()->filterByProjectId($projectId)->orderByBookingNumber('desc')->findOne();
		
		if (count($getMaxBookingNumber) == 0) {
			$runningNumber = '000001';
		} else {
			$runningNumber = substr('0000000' . (substr($getMaxBookingNumber->getBookingNumber(), -6) + 1), -6);
		}
		$bookingNumber = 'P'.$projectId.'-'.$runningNumber;
		
		$booking = new FacilityBooking();
		$booking->setId(uniqid());
		$booking->setBookingNumber($bookingNumber);
		$booking->setFacilityId($facilityId);
		$booking->setFacilityName($name);
		$booking->setBookingDate(substr($bookDate, -4).'-'.substr($bookDate, 3, 2).'-'.substr($bookDate, 0, 2));	
		$booking->setBookingTime($timeslot);
		$booking->setBookingTimeslot($timeslotId);
		$booking->setBookingOwner($bookingOwner);
		$booking->setProjectId($projectId);
		$booking->setTenantId($tenantId);
		$booking->setBookingStatus('PRE-BOOK');
		$booking->setPaymentStatus('NOT YET');
		$booking->setDeposit($deposit);
		$booking->setCharge($charge);
		$booking->setTotal($total);
		$booking->setCreatedBy($_SESSION['username']);
		$booking->setCreatedDate(date('Y-m-d H:i:s'));
		$booking->setModifiedBy($_SESSION['username']);
		$booking->setModifiedDate(date('Y-m-d H:i:s'));
		$booking->save();
		
		$getFacilityBooking = FacilityBookingQuery::create()->filterByBookingNumber($bookingNumber)->findOne();
		$arrFacility = $getFacilityBooking->toArray();
		if(is_array($arrFacility)){
			$data['ProjectId']		= $projectId;
			$data['Subject']		= "Facility Booking Notification";
			$data['PreviewMessage']	= "Your booked facility.";
			$data['Message']		= "
				<style>
				table tr td {
					font-family: arial;
				}
				</style>
				You have pre-booked a facility of ".$arrFacility['FacilityName']." <br />
							<table>
								<tr><td><b>Booking Number:</b></td></tr>
								<tr><td> ".$bookingNumber."<p>&nbsp;</p></td></tr>
								
								<tr><td><b>Transaction Date:</b></td></tr>
								<tr><td> ".date('l, jS F Y h:i A',strtotime($arrFacility['CreatedDate']))."<p>&nbsp;</p></td></tr>
								
								<tr><tr><td><b>Booking Date:</b></td></tr>
								<tr><td> ".date('l, jS F Y h:i A',strtotime($arrFacility['BookingDate']))."<p>&nbsp;</p></td></tr>
								
								<tr><td><b>Time Slot:</b></td></tr>
								<tr><td> ".$timeslot."<p>&nbsp;</p></td></tr>
								
								<tr><td><b>Booking Status:</b></td></tr>
								<tr><td> ".$arrFacility['BookingStatus']."<p>&nbsp;</p></td></tr>
								
								<tr><td><b>Total Amount (RM):</b></td></tr>
								<tr><td> ".number_format($arrFacility['Total'],2)."<p>&nbsp;</p></td></tr>
							</table>
							";
			$selectedDate	= $arrFacility['BookingDate'];
			$alert1 = date('Y-m-d H:i:s',strtotime('-1 hour',strtotime($selectedDate)));
			$alert2 = date('Y-m-d H:i:s',strtotime('-2 hour',strtotime($selectedDate)));
			$data['AlertTime']	= $alert1.','.$alert2;
			$data['UserId']		= $tenantId;
			$data['TenantId']	= $tenantId;
			$data['BookingDate']	= $selectedDate;
			$data['Type']			= 1;
			$data['ReminderDate']	= $selectedDate;
			$data['BookingNumber']	= $bookingNumber;
			$data['Checksum']		= time();
			$data['Title']			= "Booking " .$bookingNumber. " Notification";
			
			EBulletinQuery::postEbulletin($data);
			ReminderQuery::createReminder($data);
			
			//push notification
			/*$formatImei = \ShAccountQuery::getImei(null, $tenantId);
			$msgBody	= "You have new booked facility.";
			$arrPost = array();
			$arrPost['MSG_ID']		= hash( 'sha256', "rakanoth".time());
			$arrPost['TYPE']		= '4';
			$arrPost['DESC']		= 'NEW_BOOKING';
			$arrPost['FB_NUMBER'] 	= $arrFacility['BookingNumber'];
			$arrPost['FB_DATE'] 	= $arrFacility['BookingDate'];
			$arrPost['FB_ADMINTID'] = $tenantId;
			$arrPost['FB_ADMINUID'] = $bookingOwner;
			$arrPost['TIME']		= time();
			MessageQuery::pushNotif($formatImei, $msgBody, $arrPost, false, 0);*/
			//end push notification
			//pushNotif($formatImei, $msgBody);
		}
		
		insertUserEvent('Facility Booking', 'Has performed Facility Booking (Booking Number:' . $bookingNumber . ').');
		header("Location: ./facilityBookingList.php");
		die();
	} else {
		$facilitySelected = FacilityQuery::create()->filterById($facilityId)->find();
		
		$facilitySelected = $facilitySelected[0];
	}
} else {
	$facilityId = isset($_REQUEST['facilityId']) ? $_REQUEST['facilityId'] : '';
	$timeslotId = isset($_REQUEST['timeslotId']) ? $_REQUEST['timeslotId'] : '';
	$bookDate = isset($_REQUEST['bookDate']) ? $_REQUEST['bookDate'] : '';

	if ($facilityId == '' && $bookDate == '') {
		header("Location: facilityBookingList.php?");
		die();
	} else {
		$facilitySelected = FacilityQuery::create()->filterById($facilityId)->find();
		
		$facilitySelected = $facilitySelected[0];
	
		if ($timeslotId == '' || $timeslotId == 'XX') {
			$timeslot = '[' . $facilitySelected->getStartTime()->format('h:i A') . ' - ' . $facilitySelected->getEndTime()->format('h:i A') . ']';
		} else {
			/*$facilityTimeslots = FacilityTimeslotQuery::create()->filterById($_REQUEST['timeslotId'])->find();
			$facilityTimeslots = $facilityTimeslots[0];
			$timeslot = '[' . $facilityTimeslots->getStartTime()->format('H:i A') . ' - ' . $facilityTimeslots->getEndTime()->format('H:i A') . ']';*/
			foreach($timeslotId as $ts){
				//print_r($ts);
				$qfacilityTimeslots = FacilityTimeslotQuery::create()->filterById($ts)->findOne();
				$timeslot[] = $qfacilityTimeslots->getStartTime()->format('h:i A') . ' - ' . $qfacilityTimeslots->getEndTime()->format('h:i A') ;
			}
			$facilityTimeslots = $qfacilityTimeslots;
			$timeslot = implode(" | ",$timeslot);
			$timeslotId = implode("|", $timeslotId);
		}
	}
	
	$timeslotCounter = 1;
	$arrCounter = array();
	if(strpos($timeslotId, "|") > 0){
		$arrCounter			= explode("|", $timeslotId);
		$timeslotCounter = count($arrCounter);
	}
	
}
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['Operation Configuration'] = true; ?>				
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Facility Booking
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Operation Management <span class="divider">/</span> Facility Booking
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Facility Booking</h4>
						</div>
						
						<div class="widget-body">
							<form action="./facilityBookingAdd.php" method="post" class="form-horizontal">
								<div class="control-group">
									<label class="control-label" for="input1">Name</label>
									<div class="controls">
										<label><?php echo $facilitySelected->getName(); ?></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Booking Date</label>
									<div class="controls">
										<label><?php echo $bookDate; ?></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Time</label>
									<div class="controls">
										<label><?php echo $timeslot; ?></label>
									</div>
								</div>
								<?php
								$unitIdDropDown = TenantQuery::create()->filterByProjectId($_SESSION['projectId'])->orderByUnitId()->find()->getData();
								?>
								<div class="control-group <?php if ($errTenantId <> '') echo 'has-error'; ?>">
									<label class="control-label">Unit ID</label>
									<div class="controls">
										<select class="form-control span6" data-placeholder="Please Select" tabindex="1" name="tenantId">
											<option value="" <?php if ($tenantId == '') echo 'selected'; ?>>Please Select</option>
											<?php foreach($unitIdDropDown as $idDropDown) : ?>
											<option value="<?php echo $idDropDown->getUnitId() ?>" <?php echo $idDropDown->getUnitId()==$tenantId ? "selected" : "" ?>><?php echo $idDropDown->getUnitId() ?></option>
											<?php endforeach; ?>
										</select>
										<span class="help-block"><?php echo $errTenantId; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errBookingOwner <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Booking Owner</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Booking Owner" name="bookingOwner" value="<?php $bookingOwner; ?>"/>
										<span class="help-block"><?php echo $errBookingOwner; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Deposit (RM)</label>
									<div class="controls">
										<label><?php echo number_format($facilitySelected->getDeposit(), 2, '.', ''); ?></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Charge (RM)</label>
									<div class="controls">
										<label><?php $totalCharge = $facilitySelected->getCharge() * $timeslotCounter; echo number_format($totalCharge, 2, '.', ''); ?></label>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn" onclick="window.location.href='facilityBookingList.php'">Back</button>
									<button type="submit" class="btn btn-primary">Book</button>
								</div>
								
								<input type="hidden" name="name" value="<?php echo $facilitySelected->getName(); ?>" />
								<input type="hidden" name="maxHour" value="<?php echo $facilitySelected->getMaxHour(); ?>" />
								<input type="hidden" name="type" value="<?php echo $facilitySelected->getType(); ?>" />
								<input type="hidden" name="advanceBooking" value="<?php echo $facilitySelected->getAdvancedBooking(); ?>" />
								<input type="hidden" name="facilityId" value="<?php echo $facilityId; ?>" />
								<input type="hidden" name="timeslot" value="<?php echo $timeslot; ?>" />
								<input type="hidden" name="timeslotId" value="<?php echo $timeslotId; ?>" />
								<input type="hidden" name="bookDate" value="<?php echo $bookDate; ?>" />
								<input type="hidden" name="deposit" value="<?php echo $facilitySelected->getDeposit(); ?>" />
								<input type="hidden" name="charge" value="<?php echo $totalCharge; ?>" />
								<input type="hidden" name="total" value="<?php echo $facilitySelected->getDeposit() + ($facilitySelected->getCharge() * $timeslotCounter); ?>" />
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
