<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$bookingResult = '';

$bookingNumber = '';
$tenantId = '';
$startDate = '';
$endDate = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$bookingNumber = isset($_REQUEST['bookingNumber']) ? $_REQUEST['bookingNumber'] : '';
	$tenantId = isset($_REQUEST['tenantId']) ? $_REQUEST['tenantId'] : '';
	$startDate = isset($_REQUEST['startDate']) ? $_REQUEST['startDate'] : '';
	$endDate = isset($_REQUEST['endDate']) ? $_REQUEST['endDate'] : '';
	
	if (!($bookingNumber == '' && $tenantId == '' && $startDate == '' && $endDate == '')) {
		$query = FacilityBookingQuery::create();
		
		if ($bookingNumber <> '') {
			$query = $query->filterByBookingNumber('%'.$bookingNumber.'%');
		}
		
		if ($tenantId <> '') {
			$query = $query->filterByTenantId($tenantId);
		}
		
		if ($startDate <> '') {
			$query = $query->filterByBookingDate(array('min' => substr($startDate, -4).'-'.substr($startDate, 3, 2).'-'.substr($startDate, 0, 2)));
		}
		
		if ($endDate <> '') {
			$query = $query->filterByBookingDate(array('max' => substr($endDate, -4).'-'.substr($endDate, 3, 2).'-'.substr($endDate, 0, 2)));
		}

		$bookingResult = $query->filterByProjectId($_SESSION['projectId'])->find();
	}
}
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['Operation Configuration'] = true; ?>				
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Booking Payment
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Operation Management <span class="divider">/</span> Booking Payment
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Booking Payment</h4>
						</div>
						
						<div class="widget-body">
							<form action="./updateFacilityBookingStatusList.php" method="post" class="form-horizontal">
								<div class="control-group">
									<label class="control-label" for="input2">Booking Number</label>
									<div class="controls">
										<input type="text" class="span6" id="input2" placeholder="Booking Number" name="bookingNumber" value="<?php echo $bookingNumber; ?>"/>
									</div>
								</div>
								<?php
								$tenant = TenantQuery::create()->filterByProjectId($_SESSION['projectId'])->orderByUnitId()->find()->getData();
								?>
								<div class="control-group">
									<label class="control-label">Tenant ID</label>
									<div class="controls">
										<select class="span6" data-placeholder="Please Select" tabindex="1" name="tenantId">
											<option value="">Please Select</option>
											<?php foreach($tenant as $t) : ?>
											<option value="<?php echo $t->getUnitId() ?>" <?php echo $tenantId == $t->getUnitId() ? 'selected' : ""; ?>><?php echo $t->getUnitId() ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Start Date</label>
									<div class="controls">
										<input class="input-small date-picker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy" size="16" type="text" value="<?php echo $startDate; ?>" id="startDate" name="startDate"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">End Date</label>
									<div class="controls">
										<input class="input-small date-picker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy" size="16" type="text" value="<?php echo $endDate; ?>" id="endDate" name="endDate"/>
									</div>
								</div>

								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Search</button>
								</div>
							</form>

							<?php
								if ($bookingResult == '') {
								
								} else if (count($bookingResult) == 0) {
							?>
								No record found based on your search. Try again.
							<?php
								} else {
							?>
							<table class="table table-striped table-bordered" id="tableBookingHistory">
								<thead>
									<tr>
										<th>Booking Number</th>
										<th>Facility</th>
										<th>Unit ID</th>
										<th>Booking Owner</th>
										<th>Booking Date</th>
										<th>Time</th>
										<th>Booking Status</th>
										<th>Payment Status</th>
										<th>Transaction Date</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
							<?php
									foreach ($bookingResult as $booking) {
							?>
									<tr class="odd gradeX">
										<td><?php echo $booking->getBookingNumber(); ?></td>
										<td><?php echo $booking->getFacilityName(); ?></td>
										<td><?php echo $booking->getTenantId(); ?></td>
										<td><?php echo $booking->getBookingOwner(); ?></td>
										<td><?php echo $booking->getBookingDate()->format('d/m/Y'); ?></td>
										<td><?php echo $booking->getBookingTime(); ?></td>
										<td><?php echo $booking->getBookingStatus(); ?></td>
										<td><?php echo $booking->getPaymentStatus(); ?></td>
										<td><?php echo $booking->getModifiedDate()->format('d/m/Y H:i A'); ?></td>
										<td><?php
											if ($booking->getBookingStatus() == 'PRE-BOOK') echo '<a href="./updateFacilityBookingStatusPayment.php?bookingNumber='.$booking->getBookingNumber().'">|Payment|</a> ';
											if ($booking->getPaymentStatus() == 'PAID') echo '<a target="_blank" href="./receipt.php?bookingNumber='.$booking->getBookingNumber().'">|Receipt|</a> ';
											if ($booking->getPaymentStatus() == 'NOT YET') echo '<a href="./updateFacilityBookingStatusCancel.php?bookingNumber='.$booking->getBookingNumber().'">|Cancellation|</a>';
										?>
										</td>
									</tr>
							<?php
									}
							?>
								</tbody>
							</table>
							<?php
								}
							?>
						</div>
					</div>
					<!-- END FORM-->
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
			
			$('#tableBookingHistory').dataTable( {
		         "iDisplayLength": 25
			} );
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
