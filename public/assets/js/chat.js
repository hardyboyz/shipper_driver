/* 
Created by: Kenrick Beckett

Name: Chat Engine
*/

var instanse = false;
var state;
var mes;
var file;
var url = "http://localhost/smarthome-central-server/api/community";

function Chat () {
    this.update = updateChat;
    this.send = sendChat;
	this.getState = getStateOfChat;
}

//gets the state of the chat
function getStateOfChat(){
	if(!instanse){
		 instanse = true;
		 $.ajax({
			   type: "PUT",
			   url: url,
			   data: '{"action":"LTBJTuE25W","chatroomId":"3","userId":"<?php echo $userId ?>","lastId":"0"}','function':'getState',			
			   success: function(data){
				   state = data.state;
				   instanse = false;
			   },
			});
	}	 
}

//Updates the chat
function updateChat(){
	 if(!instanse){
		 instanse = true;
	     $.ajax({
			   type: "PUT",
			   url: url,
			   data: '{"action":"LTBJTuE25W","chatroomId":"3","userId":"<?php echo $userId ?>","lastId":"0"}','function': 'update','state': state,
			   success: function(data){
				   if(data.text){
						for (var i = 0; i < data.text.length; i++) {
                            $('#chat-area').append($("<p>"+ data.text[i] +"</p>"));
                        }
				   }
				   document.getElementById('chat-area').scrollTop = document.getElementById('chat-area').scrollHeight;
				   instanse = false;
				   state = data.state;
			   },
			});
	 }
	 else {
		 setTimeout(updateChat, 1500);
	 }
}

//send the message
function sendChat(message, nickname)
{
    updateChat();
     $.ajax({
		   type: "PUT",
		   url: url,
		   data: '{"action":"7V0iHpCVwi","unitId":"001","userId":"<?php echo $userId ?>","chatroomId":"3","message":"message"}',
		   success: function(data){
			   updateChat();
		   },
		});
}
