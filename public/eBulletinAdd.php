<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$errSubject = '';
$errPreviewMessage = '';
$errExpiryDate = '';
$errMessage = '';
$errTenantId = '';
$errPhoto = '';
$errVideo = '';
$errAudio = '';

$subject = '';
$previewMessage = '';
$expiryDate = '';
$message = '';
$tenantId = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST' &&  isset($_REQUEST['subject'])) {
	//print_r($_POST);exit;
	$target = "./img/eBulletin/";
	$publicUrl = "http://localhost/smarthome-central-server/public/img/eBulletin/";
	
	$subject = isset($_REQUEST['subject']) ? $_REQUEST['subject'] : '';
	$previewMessage = isset($_REQUEST['previewMessage']) ? $_REQUEST['previewMessage'] : '';
	$expiryDate = isset($_REQUEST['expiryDate']) ? $_REQUEST['expiryDate'] : '';
	$message = isset($_REQUEST['message']) ? $_REQUEST['message'] : '';
	$photo = isset($_REQUEST['photoname']) ? $_REQUEST['photoname'] : "" ;
	$video = isset( $_REQUEST['videoname']) ?  $_REQUEST['videoname'] : "";
	$audio = isset( $_REQUEST['audioname']) ?  $_REQUEST['audioname'] : "";
	$tenantId = isset($_REQUEST['tenantId']) ? $_REQUEST['tenantId'] : array();

	$hasError = false;
	
	if ($subject == '') {
		$hasError = true;
		$errSubject = 'Please enter the subject.';
	}
	
	if ($previewMessage == '') {
		$hasError = true;
		$errPreviewMessage = 'Please enter the preview message.';
	}

	if ($expiryDate == '') {
		$hasError = true;
		$errExpiryDate = 'Please enter the expiry date.';
	}

	if ($message == '') {
		$hasError = true;
		$errMessage = 'Please enter the message.';
	}

	if ($tenantId == '' || count($tenantId) == 0) {
		$hasError = true;
		$errTenantId = 'Please select the tenant.';
	}
		
		if (!$hasError) {
			$eBulletin = new EBulletin();
			//$eBulletin->setId($uniqid);
			$eBulletin->setProjectId($_SESSION['projectId']);
			$eBulletin->setSubject($subject);
			$eBulletin->setPreviewMessage($previewMessage);
			$eBulletin->setExpiryDate(substr($expiryDate, -4).'-'.substr($expiryDate, 3, 2).'-'.substr($expiryDate, 0, 2));
			$eBulletin->setMessage($message);
			$eBulletin->setPhoto($photo);
			$eBulletin->setVideo($video);
			$eBulletin->setAudio($audio);
			$eBulletin->setCreatedBy($_SESSION['username']);
			$eBulletin->setCreatedDate(date('Y-m-d H:i:s'));
			$eBulletin->setModifiedBy($_SESSION['username']);
			$eBulletin->setModifiedDate(date('Y-m-d H:i:s'));
			$eBulletin->save();
			$getEbulletin = $eBulletin->toArray(); 
			//print_r($getEbulletin);
			//exit;
			
			if (array_search('all', $tenantId) !== false) {
				$tenant = TenantQuery::create()->filterByProjectId($_SESSION['projectId'])->orderByUnitId()->find()->getData();
				foreach($tenant as $t) {
					$recipient = new EBulletinRecipient();
					$recipient->setId(uniqid());
					$recipient->setEBulletinId($getEbulletin['Id']);
					$recipient->setTenantId($t->getId());
					$recipient->setCreatedBy($_SESSION['username']);
					$recipient->setCreatedDate(date('Y-m-d H:i:s'));
					$recipient->setModifiedBy($_SESSION['username']);
					$recipient->setModifiedDate(date('Y-m-d H:i:s'));
					$recipient->save();
					
					//push notification
					$formatImei = \ShAccountQuery::getImei(null, $t->getId());
					$msgBody	= "You have an ebulletin message.";
					$arrPost = array('MSG_ID'=>md5("rakanoth".uniqid()),
								'TYPE'=>'2',
								'SUB_TYPE'=>'NEW_EBULLETIN',
								'TIME'=>time(),
								'PREVIEW' => $previewMessage
								);
					$PN = \MessageQuery::pushNotif($formatImei, $msgBody, $arrPost, true, 0);
					//end push notification
				}
				/*$recipient = new EBulletinRecipient();
				$recipient->setId(uniqid());
				$recipient->setEBulletinId($getEbulletin['Id']);
				$recipient->setTenantId('010606');
				$recipient->setCreatedBy($_SESSION['username']);
				$recipient->setCreatedDate(date('Y-m-d H:i:s'));
				$recipient->setModifiedBy($_SESSION['username']);
				$recipient->setModifiedDate(date('Y-m-d H:i:s'));
				$recipient->save();
				
				$recipient = new EBulletinRecipient();
				$recipient->setId(uniqid());
				$recipient->setEBulletinId($getEbulletin['Id']);
				$recipient->setTenantId('010808');
				$recipient->setCreatedBy($_SESSION['username']);
				$recipient->setCreatedDate(date('Y-m-d H:i:s'));
				$recipient->setModifiedBy($_SESSION['username']);
				$recipient->setModifiedDate(date('Y-m-d H:i:s'));
				$recipient->save();*/
			} else {
				foreach ($tenantId as $tenantSelected) {
					$recipient = new EBulletinRecipient();
					$recipient->setId(uniqid());
					$recipient->setEBulletinId($getEbulletin['Id']);
					$recipient->setTenantId($tenantSelected);
					$recipient->setCreatedBy($_SESSION['username']);
					$recipient->setCreatedDate(date('Y-m-d H:i:s'));
					$recipient->setModifiedBy($_SESSION['username']);
					$recipient->setModifiedDate(date('Y-m-d H:i:s'));
					$recipient->save();
					
					//push notification
					$formatImei = \ShAccountQuery::getImei(null, $tenantSelected);
					$msgBody	= "You have an ebulletin message.";
					$arrPost = array('MSG_ID'=>md5("rakanoth".uniqid()),
								'TYPE'=>'2',
								'SUB_TYPE'=>'NEW_EBULLETIN',
								'TIME'=>time(),
								'PREVIEW' => $previewMessage
								);
					$PN = \MessageQuery::pushNotif($formatImei, $msgBody, $arrPost, true, 0);
					//end push notification
					//print_r($PN);
				}
			}
			
			insertUserEvent('E-Bulletin', 'Has performed E-Bulletin (Subject: ' . $subject . ')');
			header("Location: ./eBulletinList.php");
			die();
		}
	}
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script>tinymce.init({
		  selector: 'textarea',
		  height: 500,
		  theme: 'modern',
		  plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code fullscreen',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern imagetools'
		  ],
		  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | sizeselect | bold italic | fontselect fontsizeselect link preview | forecolor backcolor emoticons',
		  fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
		  toolbar2: '',
		  image_advtab: true,
		  content_css: [
			'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
			'//www.tinymce.com/css/codepen.min.css'
		  ]
		 });
		 
		 $('form').bind('form-pre-serialize', function(e) {
			tinyMCE.triggerSave();
		});
	</script>
	
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['Operation Configuration'] = true; ?>				
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							E-Bulletin Details
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Operation Management <span class="divider">/</span> E-Bulletin
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>E-Bulletin Details</h4>
						</div>
						<div class="widget-body">
							<form action="<?php //echo $action ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="ebulletin">
								<div class="control-group <?php if ($errSubject <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Subject</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Subject" name="subject" value="<?php echo $subject; ?>"/>
										<span class="help-block"><?php echo $errSubject; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errPreviewMessage <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input2">Preview Message</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Preview Message" name="previewMessage" value="<?php echo $previewMessage; ?>"/>
										<span class="help-block"><?php echo $errPreviewMessage; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errExpiryDate <> '') echo 'has-error'; ?>">
									<label class="control-label">Expiry Date</label>
									<div class="controls">
										<input class="form-control input-small date-picker" data-date="<?php echo date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy" size="16" type="text" value="<?php echo $expiryDate; ?>" id="expiryDate" name="expiryDate"/>
										<span class="help-block"><?php echo $errExpiryDate; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errMessage <> '') echo 'has-error'; ?>">
									<label class="control-label">Message</label>
									<div class="controls">
										<textarea class="form-control textarea span12" rows="6" id="message" name="message"><?php echo $message; ?></textarea>
										<span class="help-block"><?php echo $errMessage; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Media Upload</label>
									<div class="controls">
										<select name="media_upload" id="media_upload">
										<option value="0">-------</option>
										<option value="photo">Photo</option>
										<option value="video">Video</option>
										<option value="audio">Audio</option>
										</select>
									</div>
								</div>
								<div class="control-group <?php if ($errPhoto <> '') echo 'has-error'; ?>" id="photo" style="display:none">
									<label class="control-label">Photo</label>
									<div class="controls">															
										<div class="form-control fileupload fileupload-new" data-provides="fileupload">
											<div class="fileupload-new thumbnail" style="width: 100px; height: 100px;"></div>
											<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;"></div>
											<div>
													<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default selectFile" name="photo"  id="filephoto"/></span>
													<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
											Only accept jpg, gif and png file. Maximum file size 500kb
											<div id="progressNumberPhoto" style="color:red"></div>
											<span class="help-block"><?php echo $errPhoto; ?></span>
											<input type="hidden" name="photoname" id="photoname">
										</div>							
									</div>
								</div>
								<div class="control-group <?php if ($errVideo <> '') echo 'has-error'; ?>" id="video" style="display:none">
									<label class="control-label">Video</label>
									<div class="controls">
										<div class="form-control fileupload fileupload-new" data-provides="fileupload">
											<div class="input-append">
												<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
												<input type="file" class="default selectFile" name="video" id="filevideo"/>
													</span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
										</div>
										Only accept .mp4 .mpg file. Maximum file size 3MB
										<div id="progressNumberVideo" style="color:red"></div>
										<span class="help-block"><?php echo $errVideo; ?></span>
										<input type="hidden" name="videoname" id="videoname">
									</div>
								</div>
								<div class="control-group <?php if ($errAudio <> '') echo 'has-error'; ?>"  id="audio" style="display:none">
									<label class="control-label">Audio</label>
									<div class="controls">
										<div class="form-control fileupload fileupload-new" data-provides="fileupload">
											<div class="input-append">
												<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span>
												<input type="file" class="default selectFile" name="audio"  id="fileaudio"/>
													</span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
										</div>
										Only accept .wav .mp3 file. Maximum file size 3MB
										<div id="progressNumberAudio" style="color:red"></div>
										<span class="help-block"><?php echo $errAudio; ?></span>
										<input type="hidden" name="audioname" id="audioname">
									</div>
								</div>
								<?php
								$tenant = TenantQuery::create()->filterByProjectId($_SESSION['projectId'])->orderByUnitId()->find()->getData();
								?>
								<div class="control-group <?php if ($errTenantId <> '') echo 'has-error'; ?>">
									<label class="control-label">Tenant</label>
									<div class="controls">
										<select data-placeholder="Select Tenant" class="form-control span6 chosen" multiple name="tenantId[]">
											<option value="all" <?php if (array_search('all', $tenantId) !== false) echo ' selected'; ?>>Send To All</option>
											<?php foreach($tenant as $t) : ?>
											<option value="<?php echo $t->getUnitId() ?>" <?php echo $tenantId == $t->getUnitId() ? 'selected' : ""; ?>><?php echo $t->getUnitId() ?></option>
											<?php endforeach; ?>
										</select>
										<span class="help-block"><?php echo $errTenantId; ?></span>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary" id="send">Send</button>
									<button type="reset" class="btn">Clear</button>
									<button type="button" class="btn" onclick="window.location.href='eBulletinList.php'">Cancel</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
			
			jQuery(".selectFile").change(function(event) {
				     event.preventDefault();
				    // var arr = [ "jpg","JPG","jpeg","JPEG","gif","GIF","png","PNG","WAV","wav","mp3","MP3","mpg","MPG","MPEG","mpeg" ];
				    // var str = "How are you doing today?.jpg";
					// var res = str.split(".");
				    // if(jQuery.inArray()
				    
				 });
				 
			$('input:file').change(
			function(e) {
				var files = e.originalEvent.target.files;
				for (var i=0, len=files.length; i<len; i++){
					var n = files[i].name,
						s = files[i].size,
						t = files[i].type;
						typefile = t.toString();
						
					if ((t != 'image/jpeg') 
						&& (t != 'image/png') 
						&& (t != 'image/gif') 
						&& (t != 'audio/mpeg') 
						&& (t != 'video/mpeg') 
						&& (t != 'video/mp4')
						&& (t != 'application/x-wav')
						) {
						alert('File ' + t + ' is not supported. Please choose another supported file. Thank you.');
						selId.innerHTML = 'File ' + t + ' is not supported. Please choose another supported file. Thank you.';
						$("#fileupload-preview").html('');
					}else{
					
					var formData = new FormData($("#ebulletin")[0]);
					//console.log(formData);
				    $.ajax({
				        url: '<?php echo $action ?>', //Server script to process data
				        type: 'POST',
				       xhr: function () { // Custom XMLHttpRequest
					  var myXhr = jQuery.ajaxSettings.xhr();
					  if (myXhr.upload) { // Check if upload property exists
					      myXhr.upload.addEventListener('progress', uploadProgress, false); // For handling the progress of the upload
					  }
					  return myXhr;
				        },
				        //Ajax events
				        beforeSend: function (stuff) {
					  console.log("BeforeSend");
					  console.log(stuff);
				        },
				        success: function (data) {
					  console.log("Success!");
					  console.log(data);
					  d = jQuery.parseJSON(data);
					  if(d.type=="photo"){
						  jQuery("#photoname").val(d.name);
					  }
					  if(d.type=="video"){
						  jQuery("#videoname").val(d.name);
					  }
					  if(d.type=="audio"){
						  jQuery("#audioname").val(d.name);
					  }
				        },
				        error: function (error) {
					  console.log("Error!");
					  console.log(error);
				        },
				        // Form data
				        data: formData,
				        //Options to tell jQuery not to process data or worry about content-type.
				        cache: false,
				        contentType: false,
				        processData: false
				    });	
				}
				function uploadProgress(evt) {
						sel = jQuery("#media_upload").val();
						if(sel == "photo")  selId	= document.getElementById('progressNumberPhoto');
						if(sel == "video")	selId	= document.getElementById('progressNumberVideo');
						if(sel == "audio")	selId	= document.getElementById('progressNumberAudio');
						if (evt.lengthComputable) {
							var percentComplete = Math.round(evt.loaded * 100 / evt.total);
							selId.innerHTML = percentComplete.toString() + '% File Uploaded.';
						}
						else {
							selId.innerHTML = 'unable to upload';
					        }
					      }
				}
			});
			
			jQuery("#media_upload").change(function(){
				sel = jQuery("#media_upload").val();
				if(sel == "photo") {
					jQuery("#photo").show("slow");
					jQuery("#video").hide("slow");
					jQuery("#audio").hide("slow");
					jQuery(".fileupload-preview").html("");
					jQuery("#filevideo").val("");
					jQuery("#fileaudio").val("");
					jQuery("#videoname").val("");
					jQuery("#audioname").val("");
				}
				if(sel == "video") {
					jQuery("#video").show("slow");
					jQuery("#photo").hide("slow");
					jQuery("#audio").hide("slow");
					jQuery(".fileupload-preview").html("");
					jQuery("#filephoto").val("");
					jQuery("#fileaudio").val("");
					jQuery("#photoname").val("");
					jQuery("#audioname").val("");
				}
				if(sel == "audio") {
					jQuery("#photo").hide("slow");
					jQuery("#video").hide("slow");
					jQuery("#filephoto").val("");
					jQuery("#filevideo").val("");
					jQuery("#photoname").val("");
					jQuery("#videoname").val("");
					jQuery("#audio").show("slow");
					jQuery(".fileupload-preview").html("");
				}
				if(sel == "0"){
					jQuery("#photo").hide("slow");
					jQuery("#video").hide("slow");
					jQuery("#audio").hide("slow");
					jQuery("#filephoto").val("");
					jQuery("#filevideo").val("");
					jQuery("#fileaudio").val("");
					jQuery("#photoname").val("");
					jQuery("#videoname").val("");
					jQuery("#audioname").val("");
					jQuery(".fileupload-preview").html("");
				}
			});
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
