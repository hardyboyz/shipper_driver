<?php 

define('PUBLIC_COMMON_PATH', dirname(__FILE__).'/common/');

require_once '../vendor/autoload.php';
require_once '../generated-conf/config.php';

//$imgUrl2 = "http://dev1.rakanoth.com:7171/img/1dA50HPwzF";
//$action2 = 'http://dev1.rakanoth.com:7171/api/upload'; 
if(isset($_SESSION['projectId'])){
    $upload_url = ShProjectQuery::getProjectByName($_SESSION['projectId'], "upload_url");
    $download_url = ShProjectQuery::getProjectByName($_SESSION['projectId'], "download_url");
    $action = $imgUrl = "";
	
	$urlNotif = "http://push.rakanoth.com:7878/?act=send&uid=sh_push_gateway&pwd=sh_push_gateway";
    
    if($upload_url){
		$port = strlen($upload_url->getGatewayPort()) > 0 ? ":".$upload_url->getGatewayPort() : "";
        $action = $upload_url->getGatewayProtocol().'://'.$upload_url->getGatewayUrl().$port.'/'.$upload_url->getSsid();
        //$baseUrl = $upload_url->getGatewayProtocol().'://'.$upload_url->getGatewayUrl().$port.$upload_url->getGatewayPort();
        $baseUrl = $upload_url->getGatewayProtocol().'://dev2.rakanoth.com:7272';
    }
    
    if($download_url){
        $imgUrl = $download_url->getGatewayProtocol().'://'.$download_url->getGatewayUrl().':'.$download_url->getGatewayPort().'/'.$download_url->getSsid();
    }
}

function insertUserEvent($actionType, $description)
{
    $userEventLog = new UserEventLog();
    $userEventLog->setId(uniqid());
    $userEventLog->setProjectId($_SESSION['projectId']);
    $userEventLog->setUsername($_SESSION['username']);
    $userEventLog->setActionType($actionType);
    $userEventLog->setDescription($description);
    $userEventLog->setEventDate(date('Y-m-d H:i:s'));
    $userEventLog->save();
}

function pushNotif($did = null, $message = null){
	try{
		
		$did = "&did=".$did;
		$message = "&message=".$message;
		$url = "http://push.rakanoth.com:7878/?act=send&uid=sh_push_gateway&pwd=sh_push_gateway".$did.$message;

		if($url == NULL) return false;
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		if($httpcode==404){
			return true;
		} else {
			return false;
		}
		
	} catch (exception $e){
		return $e->getMessage();
	}
}
?>
