<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php
session_start();
$userId	= $_SESSION['username'];
$roomId	= (int) $_GET['id'];
if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';
$username = '';
$name = '';
$email = '';
$dob = '';
$errName = '';

$apiCommBoard = $baseUrl."/api/community";
$projectId = $_SESSION['projectId'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$sid = isset($_REQUEST['sid']) ? $_REQUEST['sid'] : '';
	$name = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
	$email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
	$dob = isset($_REQUEST['dob']) ? $_REQUEST['dob'] : '';

	$hasError = false;
	
	if ($name == '') {
		$hasError = true;
		$errName = 'Please enter the name.';
	}
	
	if (!$hasError) {
		$userDetails = UserQuery::create()->findPK($_SESSION['userId']);
		$userDetails->setName($name);
		$userDetails->setEmail($email);
		$userDetails->setDob(substr($dob, -4).'-'.substr($dob, 3, 2).'-'.substr($dob, 0, 2));
		$userDetails->save();		
	}
}

$userDetails = UserQuery::create()->findPK($_SESSION['userId']);
if (count($userDetails) == 0) {
	session_destroy();		
	header("Location: ./login.php");
	die();
} else {
	$username = $userDetails->getUsername();
	$name = $userDetails->getName();
	$email = $userDetails->getEmail();
	//$dob = $userDetails->getDob()->format('d/m/Y');
}
	
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
	
    <!--<script src="assets/js/chat.js"></script>-->
    
	<style>
		#page-wrap                      { width: 500px; margin: 30px auto; position: relative; }
		#chat-wrap                      { border: 1px solid #eee; margin: 0 0 15px 0; }
		#chat-area                      { height: 200px; overflow: auto; border: 1px solid #666; padding: 20px; background: white; }
		#chat-area span                 { color: white; background: #333; padding: 4px 8px; -moz-border-radius: 5px; -webkit-border-radius: 8px; margin: 0 5px 0 0; }
		#chat-area p                    { padding: 8px 0; }
		#people-area                      { height: 200px; overflow: auto; border: 1px solid #666; padding: 20px; background: white; }
		#people-area span                 { color: white; background: #1878C1; padding: 4px 8px; -moz-border-radius: 5px; -webkit-border-radius: 8px; margin: 0 5px 0 0; }
		#people-area p                    { padding: 8px 0; border-bottom: 1px solid #ccc; }

		#name-area                      { position: absolute; top: 12px; right: 0; color: white; font: bold 12px "Lucida Grande", Sans-Serif; text-align: right; }   
		#name-area span                 { color: #fa9f00; }

		#send-message-area p            { float: left; color: black; padding-top: 27px; font-size: 14px; }
		#sendie                         { border: 3px solid #999; width: 360px; padding: 10px; font: 12px "Lucida Grande", Sans-Serif; }
	</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top"  onload="setInterval('chat.update()', 1000)">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">

					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->		
			<?php $sidemenu['Community Board'] = true; ?>	
			<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							General
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Community Board <span class="divider">/</span> Chatroom
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<?php
								$chatroom = \ChatRoomQuery::create()
															->filterById($_GET['id'])
															->findOne();
															
								//$chatroom = $chatroom->toArray();
								
								?>
							<h4><i class="icon-reorder"></i><?php echo $chatroom->getTitle(); ?></h4>
						</div>						
						
						<div class="widget-body form">						
								<p id="name-area"></p>
								<div id="chat-wrap">
										<div id="chat-area" style="float:left;width:65%"></div>
										<div id="people-area" style="float:left;width:25%">
										<!--<p><span style="background:#1878B1">Peoples</span></p>-->
										</div>
								</div>
								<form id="send-message-area">
									<textarea id="sendie" maxlength = '100' placeholder="Your Message"></textarea>
								</form>
							<!-- Modal -->
							<div id="createroom" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Chatroom</h4>
							      </div>
							      
							    </div>

							  </div>
							</div>
								      
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	
	<script src="assets/js/app.js"></script>		

	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
		
		/* 
Created by: Kenrick Beckett

Name: Chat Engine
*/


var instanse = false;
var state;
var mes;
var file;
var url = "<?php echo $apiCommBoard; ?>";

function Chat () {
    this.update = updateChat;
    this.send = sendChat;
	this.getState = getStateOfChat;
	this.getPeople = getPeopleList;
}

//gets the state of the chat
function getStateOfChat(){
	if(!instanse){
		setTimeout(getPeopleList, 1500);
		 instanse = true;
		 $.ajax({
			   type: "PUT",
			   url: url,
			   data: '{"action":"LTBJTuE25W","chatroomId":"<?php echo $roomId ?>","userId":"<?php echo $userId ?>","lastId":"0","projectId":"<?php echo $projectId ?>"}','function':'getState',			
			   success: function(data){
				  // console.log(data.Data[0].Message);
				   if(data.Data){
						for (var i = 0; i < data.Data.length; i++) {
                            $('#chat-area').append($("<p><span>" +data.Data[i].UnitId +"</span>"+ data.Data[i].Message +"</p>"));
                        }
				   }
				   state = data.state;
				   instanse = false;
			   },
			});
	}	 
}

//Updates the chat
function updateChat(lastId){
	 if(!instanse){
		 instanse = true;
		 var user = "<?php echo $userId ?>";
	     $.ajax({
			   type: "PUT",
			   url: url,
			   data: '{"action":"LTBJTuE25W","chatroomId":"<?php echo $roomId ?>","userId":"<?php echo $userId ?>","lastId":"0","projectId":"<?php echo $projectId ?>"}','function': 'update','state': state,
			   success: function(data){
				   //console.log(data);
				   if(data.Data){
					   $('#chat-area').html("");
						for (var i = 0; i < data.Data.length; i++) {
							date = new Date(data.Data[i].SenderTime);
							//theTime = date.getMinutes()+":"+date.getSeconds();
							theTime = data.Data[i].SenderTime;
							if(user == data.Data[i].UnitId){
                            $('#chat-area').append($("<p><span style='float:right'>"+ data.Data[i].Message +"<br><span style='font-size:xx-small'>"+theTime+"</span></p>"));
							}else{
							$('#chat-area').append($("<p><span>" +data.Data[i].UnitId + "</span>"+ data.Data[i].Message +"<br><span style='font-size:xx-small'>"+theTime+"</span></p>"));
							}
									lastId = data.Data[i].UnitId;
                        }
				   }
				   document.getElementById('chat-area').scrollTop = document.getElementById('chat-area').scrollHeight;
				   instanse = false;
				   state = data.state;
			   },
			});
	 }
	 else {
		 setTimeout(updateChat, 1500);
	 }
}

//send the message
function sendChat(message, nickname)
{
    updateChat();
     $.ajax({
		   type: "PUT",
		   url: url,
		   data: '{"action":"7V0iHpCVwi","projectId":"<?php echo $projectId ?>","unitId":"<?php echo $userId ?>","userId":"<?php echo $userId ?>","chatroomId":"<?php echo $roomId ?>","message":"'+message.trim()+'"}','function':'send',
		   success: function(data){
			   updateChat();
		   },
		});
}

//People
function getPeopleList()
{
     $.ajax({
		   type: "PUT",
		   url: url,
		   data: '{"action":"obGKSP5ezF","chatroomId":"<?php echo $roomId ?>","projectId":"<?php echo $projectId ?>","userId":"<?php echo $userId ?>"}',
		   success: function(data){
			   if(data.Data){
						$('#people-area').html("");
						for (var i = 0; i < data.Data.length; i++) {
                            $('#people-area').append($("<p><span>" +data.Data[i].UnitId + "</span></p>"));
                        }
				   }
		   },
		});
}

		
	</script>
	
	<script type="text/javascript">
    	
    	// display name on page
    	$("#name-area").html("<span> <?php echo ($_SESSION['name']) ?> </span>");
    	
    	// kick off chat
        var chat =  new Chat();
    	$(function() {
    	
    		 chat.getState(); 
    		 
    		 // watch textarea for key presses
             $("#sendie").keydown(function(event) {  
             
                 var key = event.which;  
           
                 //all keys including return.  
                 if (key >= 33) {
                   
                     var maxLength = $(this).attr("maxlength");  
                     var length = this.value.length;  
                     
                     // don't allow new content if length is maxed out
                     if (length >= maxLength) {  
                         event.preventDefault();  
                     }  
                  }  
             });
    		 // watch textarea for release of key press
    		 $('#sendie').keyup(function(e) {	
    		 					 
    			  if (e.keyCode == 13) { 
    			  
                    var text = $(this).val();
    				var maxLength = $(this).attr("maxlength");  
                    var length = text.length; 
                     
                    // send 
                    if (length <= maxLength + 1) { 
                     
    			        chat.send(text, name);	
    			        $(this).val("");
    			        
                    } else {
                    
    					$(this).val(text.substring(0, maxLength));
    					
    				}	
    				
    				
    			  }
             });
            
    	});
    </script>
	
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
