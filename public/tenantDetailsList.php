<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();
if(isset($_SESSION['name'])){
	if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
	}	
}

require_once 'header.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$checkbox = $_REQUEST['sid'];

	for ($i=0; $i<count($checkbox);$i++){
		$tenant = TenantQuery::create()->findOneById($checkbox[$i]);
		$tenant->delete();
	}
}

?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['System Configuration'] = true; ?>			
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Tenant Details 
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> System Configuration <span class="divider">/</span> Tenant Details
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Tenant List</h4>
						</div>
						
						<div class="widget-body">
						<?php
						$tenantList = TenantQuery::create()->filterByProjectId($_SESSION['projectId'])->find()->getData();
						if (count($tenantList) == 0) {
							echo "No tenant found. Please add.";
						} else {
						?>
							<form action="./tenantDetailsList.php" method="post" class="form-horizontal" id="frmList">
							<table class="table table-striped table-bordered" id="sample_1">
								<thead>
									<tr>
										<th style="width:8px"><input type="checkbox" class="group-checkable" data-set=".checkboxes" /></th>
										<th>Tenant ID</th>
										<th>Name</th>
										<th>Project ID</th>
										<th>Unit ID</th>
										<th>Unit Block</th>
										<th>Unit Floor</th>
										<th>Unit No</th>
										<th class="hidden-phone">Unit Layout Type</th>
										<th class="hidden-phone">Home Line</th>
										<th class="hidden-phone">Mobile Line</th>
										<th class="hidden-phone">Email Address</th>
										<th class="hidden-phone">Modified By</th>
										<th class="hidden-phone">Modified Date</th>
									</tr>
								</thead>
								<tbody>
						<?php
							foreach ($tenantList as $tenant) {
								$unitLayout = UnitLayoutQuery::create()->filterById($tenant->getUnitLayoutPlan())->filterByProjectId($_SESSION['projectId'])->findOne();
								$layout['FloorPlanName'] = "";
								if($unitLayout) $layout = $unitLayout->toArray();
								//print_r($unitLayout);
						?>									
									<tr class="odd gradeX">
										<td><input type="checkbox" class="checkboxes" value="<?php echo $tenant->getId(); ?>" name="sid[]" /></td>
										<td><?php echo $tenant->getId(); ?></td>
										<td><a href="tenantDetails.php?sid=<?php echo $tenant->getId(); ?>"><?php echo $tenant->getName(); ?></a></td>
										<td><?php echo $tenant->getProjectId(); ?></td>
										<td><?php echo $tenant->getUnitId(); ?></td>
										<td><?php echo $tenant->getUnitBlock(); ?></td>
										<td><?php echo $tenant->getUnitFloor(); ?></td>
										<td><?php echo $tenant->getUnitNo(); ?></td>
										<td class="hidden-phone"><?php echo $layout['FloorPlanName']; ?></td>
										<td class="hidden-phone"><?php echo $tenant->getHomeNo(); ?></td>
										<td class="hidden-phone"><?php echo $tenant->getMobileNo(); ?></td>
										<td class="hidden-phone"><?php echo $tenant->getEmail(); ?></td>
										<td class="hidden-phone"><?php echo $tenant->getModifiedBy(); ?></td>
										<td class="hidden-phone"><?php echo $tenant->getModifiedDate()->format('d/m/Y H:i A'); ?></td>
									</tr>
						<?php
							}
						?>
								</tbody>
							</table>
							</form>
						<?php
						}
						?>
						</div>
						<div class="form-actions">
							<!--<button type="button" class="btn btn-primary" onclick="window.location.href='tenantDetailsAdd.php'">Add</button>-->
						<?php
							if (count($tenantList) > 0) {
						?>
							<a href="#dialogConfirmDelete" role="button" class="btn" data-toggle="modal">Delete</a>

							<div id="dialogConfirmDelete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h3 id="myModalLabel3">Confirm?</h3>
								</div>
								<div class="modal-body">
									<p>Confirm delete the selected tenant details?</p>
								</div>
								<div class="modal-footer">
									<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
									<button data-dismiss="modal" class="btn btn-primary" onclick="document.forms['frmList'].submit();">Confirm</button>
								</div>
							</div>
						<?php } ?>
						</div>
					</div>
					<!-- END FORM-->
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
