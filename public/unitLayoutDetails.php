<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$unitLayoutName = '';
$unitLayoutImage = '';
$errUnitLayoutName = '';
$errUnitLayoutImage = '';

if ($_REQUEST['sid'] == '') {
	header("Location: ./unitLayoutList.php");
	die();
} else if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['unitLayoutName'])) {
	
	$target = "./img/unitLayout/";
	$publicUrl = "http://localhost/smarthome-central-server/public/img/unitLayout/";
	
	$unitLayoutName = isset($_REQUEST['unitLayoutName']) ? $_REQUEST['unitLayoutName'] : '';
	$unitLayoutImage = $_GET['unitLayoutImage'];
	$sid = isset($_REQUEST['sid']) ? $_REQUEST['sid'] : '';
	
	$hasError = false;
	
	if ($unitLayoutName == '') {
		$hasError = true;
		$errUnitLayoutName = 'Please enter the unit layout name.';
		$unitLayoutImage = $_REQUEST['layoutImage'];
	}

	if (!$hasError) {
		$uniqid = uniqid();
		/*if ($unitLayoutImage <> '') {
			if (move_uploaded_file($_FILES['unitLayoutImage']['tmp_name'], $target.$uniqid.'-'.$unitLayoutImage)) {
				//Tells you if its all ok
				$unitLayoutImage = $target.$uniqid.'-'.$unitLayoutImage;
			} else {
				$hasError = true;
				$errUnitLayoutImage = "Error in uploading your unit layout image. Please try again.";
			}
			
		} */
				
		if (!$hasError) {
			$unitLayoutSelected = UnitLayoutQuery::create()->findPK($sid);
			$unitLayoutSelected->setFloorPlanName($unitLayoutName);
			if ($unitLayoutImage <> '') {
				$unitLayoutSelected->setFloorPlanImage($unitLayoutImage);
			}
			$unitLayoutSelected->setModifiedBy($_SESSION['username']);
			$unitLayoutSelected->setModifiedDate(date('Y-m-d H:i:s'));
			$unitLayoutSelected->save();
			
			header("Location: ./unitLayoutList.php");
			die();
		}
	}
} else {
	$unitLayoutSelected = UnitLayoutQuery::create()->findPK($_REQUEST['sid']);
	if (count($unitLayoutSelected) == 0) {
		header("Location: ./unitLayoutList.php");
		die();
	} else {
		$sid = $unitLayoutSelected->getId();
		$unitLayoutName = $unitLayoutSelected->getFloorPlanName();
		$unitLayoutImage = $unitLayoutSelected->getFloorPlanImage();		
	}
}

?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['System Configuration'] = true; ?>
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Unit Layout Creation
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> System Configuration <span class="divider">/</span> Unit Layout Creation
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Unit Layout Creation</h4>
						</div>
						
						<div class="widget-body">
							<form action="<?php echo $action ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
								<div class="control-group <?php if ($errUnitLayoutName <> '') echo 'has-error'; ?>">
									<label class="control-label" for="input1">Unit Layout Name</label>
									<div class="controls">
										<input type="text" class="form-control span6" id="input1" placeholder="Unit Layout Name" name="unitLayoutName" value="<?php echo $unitLayoutName; ?>"/>
										<span class="help-block"><?php echo $errUnitLayoutName; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errUnitLayoutImage <> '') echo 'has-error'; ?>">
									<label class="control-label">Map (Image)</label>
									<div class="controls">
															
									<div class="form-control fileupload fileupload-new" data-provides="fileupload">
											<div class="form-control fileupload-new thumbnail" style="width: 100px; height: 100px;"><img src="<?php echo $imgUrl.'/'.$unitLayoutImage.'/eBulletin'; ?>" alt=""/></div>
											<div class="form-control fileupload-preview fileupload-exists thumbnail" style="max-width: 100px; max-height: 100px; line-height: 20px;"></div>
											<div>
													<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default" name="unitLayoutImage" /></span>
													<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
											<span class="help-block"><?php echo $errUnitLayoutImage; ?></span>
									</div>							
										<span class="help-inline">Size: 750px x 750px</span>
									</div>
								</div>								

								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Save</button>
									<button type="reset" class="btn">Clear</button>
									<button type="button" class="btn" onclick="window.location.href='unitLayoutList.php'">Cancel</button>
									<input type="hidden" name="sid" value="<?php echo $sid; ?>">
									<input type="hidden" name="layoutImage" value="<?php echo $unitLayoutImage; ?>">
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
