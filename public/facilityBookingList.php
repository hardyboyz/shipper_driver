<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';
$maxHour = "";
$facilitySelected[0] = '';
$errMessage = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'booking') {
		$timeslotId = $_REQUEST['timeslotId'];
		
		if ($timeslotId == '') {
			$errMessage = "Please select your booking timeslot.";
		} else {
			$data = array('facilityId'=>$_REQUEST['facility'],
			'timeslotId'=>$timeslotId,
			'bookDate'=>$_REQUEST['BookDate']);
			header("Location: facilityBookingAdd.php?".http_build_query($data));
		}
	}
	
	if ($_REQUEST['facility'] <> '') {
		$facilitySelected = FacilityQuery::create()->filterById($_REQUEST['facility'])->find();
		$maxHour = $facilitySelected[0]->getMaxHour();
		if ($facilitySelected[0] <> '' && $facilitySelected[0]->getType() == 'Per Day') {
			$facilityAvailable = FacilityBookingQuery::create()
													->filterByFacilityId($_REQUEST['facility'])
													->filterByProjectId($_SESSION['projectId'])
													->filterByBookingStatus('PRE-BOOK')
													->filterByBookingDate(substr($_REQUEST['BookDate'], -4).'-'.substr($_REQUEST['BookDate'], 3, 2).'-'.substr($_REQUEST['BookDate'], 0, 2))
													->find()->getData();
		} else if ($facilitySelected[0] <> '' && $facilitySelected[0]->getType() == 'Per Hour') {
			$facilityTimeslots = FacilityTimeslotQuery::create()->filterByFacilityId($_REQUEST['facility'])->orderById()->find()->getData();
			$facilityAvailables = FacilityBookingQuery::create()
													->filterByFacilityId($_REQUEST['facility'])
													->filterByBookingDate(substr($_REQUEST['BookDate'], -4).'-'.substr($_REQUEST['BookDate'], 3, 2).'-'.substr($_REQUEST['BookDate'], 0, 2))
													->filterByProjectId($_SESSION['projectId'])
													->filterByBookingStatus('PRE-BOOK')
													->find()->getData();
		}
	}
}
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['Operation Configuration'] = true; ?>				
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Facility Booking
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Operation Management <span class="divider">/</span> Facility Booking
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<?php
						if ($errMessage <> '') {
					?>
					<div class="alert alert-danger">
						<strong>Error!</strong> <?php echo $errMessage; ?>
					</div>
					<?php
						}
					?>
					
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Facility Booking</h4>
						</div>
						
						<div class="widget-body">
							<form action="./facilityBookingList.php" method="post" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">Facility</label>
									<div class="controls">
							<?php
								$facilities = FacilityQuery::create()->filterByProjectId($_SESSION['projectId'])->filterByEnabled('1')->find();

								if (count($facilities) == 0) {
									echo "No facility found. Please add facility first.";
								} else {
							?>
										<select class="span6" data-placeholder="Please Select" tabindex="1" name="facility">
											<option value="">Please Select</option>
							<?php
									foreach($facilities as $facility) {
							?>
											<option value="<?php echo $facility->getId(); ?>" <?php if ($facilitySelected[0] <> '' && $facilitySelected[0]->getName() == $facility->getName()) echo 'selected';?>><?php echo $facility->getName(); ?></option>
							<?php
									}
							?>
										</select>
							<?php
								}
							?>
									</div>
								</div>
									<div class="control-group">
									<label class="control-label">Date of Booking</label>
									<div class="controls">
										<input class="input-small date-picker" data-date="<?php echo isset($_REQUEST['BookDate']) ? $_REQUEST['BookDate'] : date('d/m/Y'); ?>" data-date-format="dd/mm/yyyy" size="16" type="text" value="<?php echo isset($_REQUEST['BookDate']) ? $_REQUEST['BookDate'] : date('d/m/Y'); ?>" id="BookDate" name="BookDate"/>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Search</button>
								</div>
							</form>

						<form action="./facilityBookingList.php" method="post" class="form-horizontal" name="bookingForm">
							<?php
								if ($facilitySelected[0] == '') {

								}
								else if ($facilitySelected[0]->getType() == "Free") {
							?>
								Free facility, no need to book.
							<?php									
								} else if ($facilitySelected[0]->getType() == "Per Hour") {
							?>
							<table class="table table-striped table-bordered" id="tableTimeSlot">
								<thead>
									<tr>
										<th style="width:8px"></th>
										<th>Start Time</th>
										<th>End Time</th>
										<th>Availability</th>
									</tr>
								</thead>
								<tbody>
							<?php
									$counter = 0;
									foreach ($facilityTimeslots as $facilityTimeslot) {
										$status = '';
										if ($facilityTimeslot->getActive() == 0) {
											$status = 'Closed';
										} else {
											$alreadyBook = false;
											foreach ($facilityAvailables as $facilityAvailable) {
												if(strpos($facilityAvailable->getBookingTimeslot(),"|") > 0){
													$arrFacilityA = explode("|", $facilityAvailable->getBookingTimeslot());
													foreach($arrFacilityA as $avail){
														if($avail == $facilityTimeslot->getId()){
															$alreadyBook = true;
														}
													}
												}
												else if ($facilityAvailable->getBookingTimeslot() == $facilityTimeslot->getId()) {
													$alreadyBook = true;
													break;
												}
												
											}
											if ($alreadyBook) {
												$status = "Not Available";
											} else {
												$status = "Available";
											}
										}
							?>
									<tr class="odd gradeX">
										<!--<td><input type="radio" class="radio" value="<?php echo $facilityTimeslot->getId(); ?>" name="timeslotId" <?php if ($status <> 'Available') echo 'disabled'; ?> /></td>-->
										<td><input type="checkbox" class="checkbox" value="<?php echo $facilityTimeslot->getId(); ?>" name="timeslotId[]" <?php if ($status <> 'Available') echo 'disabled'; ?> /></td>
										<td><?php echo $facilityTimeslot->getStartTime()->format('H:i A'); ?></td>
										<td><?php echo $facilityTimeslot->getEndTime()->format('H:i A'); ?></td>
										<td><?php echo $status; ?></td>
									</tr>
							<?php
									}
							?>
								</tbody>
							</table>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Book</button>
						</div>
							<?php
								} else if ($facilitySelected[0]->getType() == "Per Day") {
									$status = "Available";
									
									foreach($facilityAvailable as $available) {
									  $status = "Not Available";
									}													
							?>
													
							<table class="table table-striped table-bordered" id="sample_1">
								<thead>
									<tr>
										<th style="width:8px"></th>
										<th>Start Time</th>
										<th>End Time</th>
										<th>Availability</th>
									</tr>
								</thead>
								<tbody>
									<tr class="odd gradeX">
										<td><input type="radio" class="radio" value="XX" name="timeslotId" <?php if ($status <> 'Available') echo 'disabled'; ?> /></td>
										<td><?php echo $facilitySelected[0]->getStartTime()->format('H:i A') ?></td>
										<td><?php echo $facilitySelected[0]->getEndTime()->format('H:i A') ?></td>
										<td><?php echo $status; ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Book</button>
						</div>
							<?php
								}
							?>
					</div>
							<input type="hidden" name="action" value="booking">
							<input type="hidden" name="facility" value="<?php echo $_REQUEST['facility']; ?>">
							<input type="hidden" name="BookDate" value="<?php echo $_REQUEST['BookDate']; ?>">
						</form>
					</div>
					<!-- END FORM-->
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
			
			$('#tableTimeSlot').dataTable( {
		         "iDisplayLength": 25
			} );
			
			//check maxhour
			jQuery(".checkbox").change(function(){
				var maxHour = <?php echo $maxHour ?>;
				$('input[type="checkbox"]').each(function() {
				        if ($(this).is(":checked")) {
					        
					  if ($(":checkbox[name='timeslotId[]']:checked").length == maxHour){
						   $(':checkbox:not(:checked)').prop('disabled', true);
					  }else{
						$(':checkbox:not(:checked)').prop('disabled', false); 
					  }
				        }
				    });
			});
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
