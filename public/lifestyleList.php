<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';
$postList = "";
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$title = isset($_REQUEST['title']) ? $_REQUEST['title'] : '';
	$companyName = isset($_REQUEST['companyName']) ? $_REQUEST['companyName'] : '';
	$icon1 = isset($_REQUEST['icon1']) ? $_REQUEST['icon1'] : '';
	$icon2 = isset($_REQUEST['icon2']) ? $_REQUEST['icon2'] : '';
	$icon3 = isset($_REQUEST['icon3']) ? $_REQUEST['icon3'] : '';
	$icon4 = isset($_REQUEST['icon4']) ? $_REQUEST['icon4'] : '';
	$icon5 = isset($_REQUEST['icon5']) ? $_REQUEST['icon5'] : '';
	
	$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
	$validDate = isset($_REQUEST['validDate']) ? $_REQUEST['validDate'] : '';
	
		if ($title <> '') {
			$query = $query->filterByTitle('%'.$title.'%');
		}
		
}else{
		$query = LifestyleQuery::create();
		$postList = $query->filterByProjectId($_SESSION['projectId'])->find();
		//print_r($postList->toArray());exit;
		//$postList = $postList->toArray();
		//print_r($postList);
	}


?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['Operation Configuration'] = true; ?>						
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							Lifestyle List
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> Operation Management <span class="divider">/</span> Lifestyle
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>Lifestyle List</h4>
						</div>
						
						<div class="widget-body">
							<!--<form action="<?php echo $_SERVER['PHP_SELF']  ?>" method="post" class="form-horizontal">
							<div class="control-group">
									<label class="control-label" for="input1">Parent Category</label>
									<?php 
			$pcat = \LifestylePcategoriesQuery::Create()->filterByProjectId($_SESSION['projectId'])->find();
									$pcat = $pcat->toArray();
									?>
									<div class="controls">
										<select name="pcatId">
											<?php foreach($pcat as $pc){ ?>
											<option value="<?php echo $pc['Id'] ?>"><?php echo $pc['PcatName'] ?></option>
											<?php } ?>
										</select>
									</div>
								</div>-->
								
								  <form class="form-inline" role="form">
									<div class="form-group">
									  <label for="category">Category :</label>
									<?php 
									$cat = \LifestyleCategoriesQuery::Create()->filterByProjectId($_SESSION['projectId'])->find();
									$cat = $cat->toArray();
									?>
										  <select name="category" class="form-control" id="category">
											<?php foreach($cat as $c){ ?>
											<option value="<?php echo $c['Id'] ?>"><?php echo $c['CatName'] ?></option>
											<?php } ?>
										</select>
										<button type="button" class="btn btn-primary" id="btnEdit">Edit</button>
										<button type="button" class="btn btn-primary" onclick="window.location.href='lifestyleCategoriesAdd.php'">Add Category</button>
									</form>
									</div>
							<div class="form-actions">
								<button type="button" class="btn btn-primary" onclick="window.location.href='lifestyleAdd.php'">Compose</button>
							</div>			
							<table class="table table-striped table-bordered" id="sample_1">
								<thead>
									<tr>
										<th>Title</th>
										<th>Company Name</th>
										<th>Type</th>
										<th>Category</th>
										<th>Icon 1</th>
										<th>Icon 2</th>
										<th>Icon 3</th>
										<th>Icon 4</th>
										<th>Icon 5</th>
										<th>Valid Date</th>
									</tr>
								</thead>
								<tbody>
							<?php
									$ads =  LifestyleQuery::create()->filterByProjectId($_SESSION['projectId'])->find();
									$ads = $ads->toArray();
									$data = array();
									$data['projectID'] = $_SESSION['projectId'];
									foreach($ads as $post){
										$type = $post['PcatId'] == 1 ? "Premium" : "Regular";
										$category = \LifestyleCategoriesQuery::getTypeName($post['Type'], $data);
										$licon1 = strlen($post['Icon1'] ) > 3 ? "<img src=".$imgUrl.'/'.$post['Icon1'].'/lifestyle'." width='100px'>" : "<img src=".$imgUrl.'/no_image.png/lifestyle'." width='100px'>";
										$licon2 = strlen($post['Icon2'] ) > 3 ? "<img src=".$imgUrl.'/'.$post['Icon2'].'/lifestyle'." width='100px'>" : "<img src=".$imgUrl.'/no_image.png/lifestyle'." width='100px'>";
										$licon3 = strlen($post['Icon3'] ) > 3 ? "<img src=".$imgUrl.'/'.$post['Icon3'].'/lifestyle'." width='100px'>" : "<img src=".$imgUrl.'/no_image.png/lifestyle'." width='100px'>";
										$licon4 = strlen($post['Icon4'] ) > 3 ? "<img src=".$imgUrl.'/'.$post['Icon4'].'/lifestyle'." width='100px'>" : "<img src=".$imgUrl.'/no_image.png/lifestyle'." width='100px'>";
										$licon5 = strlen($post['Icon5'] ) > 3 ? "<img src=".$imgUrl.'/'.$post['Icon5'].'/lifestyle'." width='100px'>" : "<img src=".$imgUrl.'/no_image.png/lifestyle'." width='100px'>";
										echo "<tr>
														<td><a href='lifestyleDetails.php?sid=".$post['Id']."'>".$post['Title']."</a></td>
														<td>".$post['CompanyName']."</td>
														<td>".$type."</td>
														<td>".$category."</td>
														<td>".$licon1."</td>
														<td>".$licon2."</td>
														<td>".$licon3."</td>
														<td>".$licon4."</td>
														<td>".$licon5."</td>
														<td>".date("Y-m-d",strtotime($post['ValidDate']))."</td>
										</tr>";
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- END FORM-->
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/gritter/js/jquery.gritter.js"></script>
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
		
		$("#btnEdit").click(function(){
			id = $("#category").val();
			window.location='lifestyleCategories.php?action=edit&id='+id;
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
