<!DOCTYPE html>
<!--  
Template Name: Conquer Responsive Admin Dashboard Template build with Twitter Bootstrap 2.2.2
Version: 1.2
Author: KeenThemes
Website: http://www.keenthemes.com
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 10]> <html lang="en" class="ie10"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php
session_start();

if ($_SESSION['name'] == '' || $_SESSION['username'] == '' || $_SESSION['userRoleId'] == '') {
	session_destroy();		
	header("Location: ./login.php");
	die();        	
}

require_once 'header.php';

$sid = '';
$appHomeIdleTime = '';
$appStandbyIdleTime = '';
$appHomeLogoImage = '';
$receiptLogoImage = '';
$facilityCheckingOnlyFlag = '';
$callingFormatPattern = '';
$interFacilityFlag = '';
$projectName = '';
$projectNameId = '';
$centralizedServerUrl = '';
$remoteServerDomain = '';
$remoteServerProtocol = '';
$remoteServerPort = '';
$localServerDomain = '';
$localServerProtocol = '';
$localServerPort = '';
$remoteCctvDomain = '';
$remoteCctvProtocol = '';
$remoteCctvPort = '';
$localCctvDomain = '';
$localCctvProtocol = '';
$localCctvPort = '';

$PNDomain = '';
$PNProtocol = '';
$PNPort = '';

$tutorialImage = '';
$tutorialImageEnable = '';
$modifiedBy = '';
$modifiedDate = '';
$appAutoSyncInterval ='';
$errAppHomeLogoImage = '';
$errReceiptLogoImage = '';
$errTutorialImage = '';
$appHomeLogoImageTemp = '';

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_REQUEST['projectNameId'])) {
	$target = "./img/appClientSetting/";
	$publicUrl = "http://localhost/smarthome-central-server/public/img/appClientSetting/";
	
	$sid = isset($_REQUEST['sid']) ? $_REQUEST['sid'] : '';
	$appHomeIdleTime = isset($_REQUEST['appHomeIdleTime']) ? $_REQUEST['appHomeIdleTime'] : '';
	$appStandbyIdleTime = isset($_REQUEST['appStandbyIdleTime']) ? $_REQUEST['appStandbyIdleTime'] : '';
	$appHomeLogoImage = $_REQUEST['appHomeLogoImage'];
	$appHomeLogoImageTemp = isset($_REQUEST['appHomeLogoImage']) ? $_REQUEST['appHomeLogoImage'] : '';
	$receiptLogoImage = $_REQUEST['receiptLogoImage'];
	$receiptLogoImageTemp = isset($_REQUEST['receiptLogoImage']) ? $_REQUEST['receiptLogoImage'] : '';
	$facilityCheckingOnlyFlag = isset($_REQUEST['facilityCheckingOnlyFlag']) ? $_REQUEST['facilityCheckingOnlyFlag'] : '';
	$callingFormatPattern = isset($_REQUEST['callingFormatPattern']) ? $_REQUEST['callingFormatPattern'] : '';
	$interFacilityFlag = isset($_REQUEST['interFacilityFlag']) ? $_REQUEST['interFacilityFlag'] : '';
	$projectName = isset($_REQUEST['projectName']) ? $_REQUEST['projectName'] : '';
	$projectNameId = isset($_REQUEST['projectNameId']) ? $_REQUEST['projectNameId'] : '';
	$centralizedServerUrl = isset($_REQUEST['centralizedServerUrl']) ? $_REQUEST['centralizedServerUrl'] : '';
	$remoteServerDomain = isset($_REQUEST['remoteServerDomain']) ? $_REQUEST['remoteServerDomain'] : '';
	$remoteServerProtocol = isset($_REQUEST['remoteServerProtocol']) ? $_REQUEST['remoteServerProtocol'] : '';
	$remoteServerPort = isset($_REQUEST['remoteServerPort']) ? $_REQUEST['remoteServerPort'] : '';
	$localServerDomain = isset($_REQUEST['localServerDomain']) ? $_REQUEST['localServerDomain'] : '';
	$localServerProtocol = isset($_REQUEST['localServerProtocol']) ? $_REQUEST['localServerProtocol'] : '';
	$localServerPort = isset($_REQUEST['localServerPort']) ? $_REQUEST['localServerPort'] : '';
	$remoteCctvDomain = isset($_REQUEST['remoteCctvDomain']) ? $_REQUEST['remoteCctvDomain'] : '';
	$remoteCctvProtocol = isset($_REQUEST['remoteCctvProtocol']) ? $_REQUEST['remoteCctvProtocol'] : '';
	$remoteCctvPort = isset($_REQUEST['remoteCctvPort']) ? $_REQUEST['remoteCctvPort'] : '';
	$localCctvDomain = isset($_REQUEST['localCctvDomain']) ? $_REQUEST['localCctvDomain'] : '';
	$localCctvProtocol = isset($_REQUEST['localCctvProtocol']) ? $_REQUEST['localCctvProtocol'] : '';
	$localCctvPort = isset($_REQUEST['localCctvPort']) ? $_REQUEST['localCctvPort'] : '';
	
	$PNDomain = isset($_REQUEST['PNDomain']) ? $_REQUEST['PNDomain'] : '';
	$PNProtocol = isset($_REQUEST['PNProtocol']) ? $_REQUEST['PNProtocol'] : '';
	$PNPort = isset($_REQUEST['PNPort']) ? $_REQUEST['PNPort'] : '';
	
	$tutorialImage = $_REQUEST['tutorialImage'];
	$tutorialImageTemp = isset($_REQUEST['tutorialImage']) ? $_REQUEST['tutorialImage'] : '';
	$tutorialImageEnable = isset($_REQUEST['tutorialImageEnable']) ? $_REQUEST['tutorialImageEnable'] : '';
	$appAutoSyncInterval = isset($_REQUEST['appAutoSyncInterval']) ? $_REQUEST['appAutoSyncInterval'] : '';
	$modifiedBy = isset($_REQUEST['modifiedBy']) ? $_REQUEST['modifiedBy'] : '';
	$modifiedDate = isset($_REQUEST['modifiedDate']) ? $_REQUEST['modifiedDate'] : '';

	$hasError = false;
	
	$uniqid = uniqid();
	
	echo "Hello".$appHomeLogoImage."Hello";
	/*if ($appHomeLogoImage <> '') {
		if (move_uploaded_file($_FILES['appHomeLogoImage']['tmp_name'], $target.$uniqid.'-'.$appHomeLogoImage)) {
			//Tells you if its all ok
			$appHomeLogoImage = $target.$uniqid.'-'.$appHomeLogoImage;
		} else {
			$hasError = true;
			$errAppHomeLogoImage = "Error in uploading your app home logo image. Please try again.";
		}
	} 

	if ($hasError != true && $receiptLogoImage <> '') {
		if (move_uploaded_file($_FILES['receiptLogoImage']['tmp_name'], $target.$uniqid.'-'.$receiptLogoImage)) {
			//Tells you if its all ok
			$receiptLogoImage = $target.$uniqid.'-'.$receiptLogoImage;
		} else {
			$hasError = true;
			$errReceiptLogoImage = "Error in uploading your receipt logo image. Please try again.";
		}
	} 

	if ($hasError != true && $tutorialImage <> '') {
		if (move_uploaded_file($_FILES['tutorialImage']['tmp_name'], $target.$uniqid.'-'.$tutorialImage)) {
			//Tells you if its all ok
			$tutorialImage = $target.$uniqid.'-'.$tutorialImage;
		} else {
			$hasError = true;
			$errTutorialImage = "Error in uploading your tutorial image. Please try again.";
		}
	}
	*/
	if (!$hasError) {	
		if ($sid == '') {
			$appClientSettingSelected = new AppClientSetting();
			$appClientSettingSelected->setID($uniqid);
			$appClientSettingSelected->setProjectId($_SESSION['projectId']);
			$appClientSettingSelected->setAppHomeIdleTime($appHomeIdleTime);
			$appClientSettingSelected->setAppStandbyIdleTime($appStandbyIdleTime);
			$appClientSettingSelected->setAppHomeLogoImage($appHomeLogoImage);
			$appClientSettingSelected->setReceiptLogoImage($receiptLogoImage);
			$appClientSettingSelected->setFacilityCheckingOnlyFlag($facilityCheckingOnlyFlag);
			$appClientSettingSelected->setCallingFormatPattern($callingFormatPattern);
			$appClientSettingSelected->setInterFacilityFlag($interFacilityFlag);
			$appClientSettingSelected->setProjectName($projectName);
			$appClientSettingSelected->setProjectNameId($projectNameId);
			$appClientSettingSelected->setCentralizedServerUrl($centralizedServerUrl);
			$appClientSettingSelected->setRemoteServerDomain($remoteServerDomain);
			$appClientSettingSelected->setRemoteServerProtocol($remoteServerProtocol);
			$appClientSettingSelected->setRemoteServerPort($remoteServerPort);
			$appClientSettingSelected->setLocalServerDomain($localServerDomain);
			$appClientSettingSelected->setLocalServerProtocol($localServerProtocol);
			$appClientSettingSelected->setLocalServerPort($localServerPort);
			$appClientSettingSelected->setRemoteCctvDomain($remoteCctvDomain);
			$appClientSettingSelected->setRemoteCctvProtocol($remoteCctvProtocol);
			$appClientSettingSelected->setRemoteCctvPort($remoteCctvPort);
			$appClientSettingSelected->setLocalCctvDomain($localCctvDomain);
			$appClientSettingSelected->setLocalCctvProtocol($localCctvProtocol);
			$appClientSettingSelected->setLocalCctvPort($localCctvPort);
			
			$appClientSettingSelected->setPNDomain($PNDomain);
			$appClientSettingSelected->setPNProtocol($PNProtocol);
			$appClientSettingSelected->setPNPort($PNPort);
			
			$appClientSettingSelected->setTutorialImage($tutorialImage);
			$appClientSettingSelected->setTutorialImageEnable($tutorialImageEnable);
			$appClientSettingSelected->setAppAutoSyncInterval($appAutoSyncInterval);
			$appClientSettingSelected->setModifiedBy($_SESSION['username']);
			$appClientSettingSelected->setModifiedDate(date('Y-m-d H:i:s'));
			$appClientSettingSelected->save();
			
			header("Location: ./appClientSetting.php");
			die();
		} else {
			$appClientSettingSelected = AppClientSettingQuery::create()->findPK($sid);
			$appClientSettingSelected->setAppHomeIdleTime($appHomeIdleTime);
			$appClientSettingSelected->setAppStandbyIdleTime($appStandbyIdleTime);
			if ($appHomeLogoImage <> '') $appClientSettingSelected->setAppHomeLogoImage($appHomeLogoImage);
			if ($receiptLogoImage <> '') $appClientSettingSelected->setReceiptLogoImage($receiptLogoImage);
			$appClientSettingSelected->setFacilityCheckingOnlyFlag($facilityCheckingOnlyFlag);
			$appClientSettingSelected->setCallingFormatPattern($callingFormatPattern);
			$appClientSettingSelected->setInterFacilityFlag($interFacilityFlag);
			$appClientSettingSelected->setProjectName($projectName);
			$appClientSettingSelected->setProjectNameId($projectNameId);
			$appClientSettingSelected->setCentralizedServerUrl($centralizedServerUrl);
			$appClientSettingSelected->setRemoteServerDomain($remoteServerDomain);
			$appClientSettingSelected->setRemoteServerProtocol($remoteServerProtocol);
			$appClientSettingSelected->setRemoteServerPort($remoteServerPort);
			$appClientSettingSelected->setLocalServerDomain($localServerDomain);
			$appClientSettingSelected->setLocalServerProtocol($localServerProtocol);
			$appClientSettingSelected->setLocalServerPort($localServerPort);
			
			$appClientSettingSelected->setPNDomain($PNDomain);
			$appClientSettingSelected->setPNProtocol($PNProtocol);
			$appClientSettingSelected->setPNPort($PNPort);
			
			$appClientSettingSelected->setRemoteCctvDomain($remoteCctvDomain);
			$appClientSettingSelected->setRemoteCctvProtocol($remoteCctvProtocol);
			$appClientSettingSelected->setRemoteCctvPort($remoteCctvPort);
			$appClientSettingSelected->setLocalCctvDomain($localCctvDomain);
			$appClientSettingSelected->setLocalCctvProtocol($localCctvProtocol);
			$appClientSettingSelected->setLocalCctvPort($localCctvPort);			
			if ($tutorialImage <> '') $appClientSettingSelected->setTutorialImage($tutorialImage);
			$appClientSettingSelected->setTutorialImageEnable($tutorialImageEnable);
			$appClientSettingSelected->setAppAutoSyncInterval($appAutoSyncInterval);
			$appClientSettingSelected->setModifiedBy($_SESSION['username']);
			$appClientSettingSelected->setModifiedDate(date('Y-m-d H:i:s'));
			$appClientSettingSelected->save();
			
			//header("Location: ./appClientSetting.php");
			//die();			
		}
	}
}
?>
<head>
	<meta charset="utf-8" />
	<title>Smart Home System Administration Portal</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link href="assets/css/style_responsive.css" rel="stylesheet" />
	<link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link href="#" rel="stylesheet" id="style_metro" />
	<link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
	<link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div id="header" class="navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="index.php">
				<img src="assets/img/logo_rakanoth.png" alt="Conquer" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="arrow"></span>
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<div class="top-nav">
					<!-- BEGIN TOP NAVIGATION MENU -->					
					<ul class="nav pull-right" id="top_menu">
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i>
							<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
					<!-- END TOP NAVIGATION MENU -->	
				</div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div id="container" class="row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div id="sidebar" class="nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
<?php $sidemenu['System Configuration'] = true; ?>			
<?php include PUBLIC_COMMON_PATH.'sidemenu.php'; ?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div id="body">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="widget-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button">×</button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							App Client Setting
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i> Home <span class="divider">/</span> System Configuration <span class="divider">/</span> App Client Setting
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div id="page" class="dashboard">
					<div class="widget">
						<div class="widget-title">
							<h4><i class="icon-reorder"></i>App Client Setting</h4>
						</div>
						
						<div class="widget-body">
							<?php		
								$appClientSetting = AppClientSettingQuery::create()->filterByProjectId($_SESSION['projectId'])->find()->getData();
								if ($appClientSetting == '') {
								
								} else if (count($appClientSetting) == 0) {

								} else {
									$appClientSetting = $appClientSetting[0];
									$sid = $appClientSetting->getId();
									$appHomeIdleTime = $appClientSetting->getAppHomeIdleTime();
									$appStandbyIdleTime = $appClientSetting->getAppStandbyIdleTime();
									$appHomeLogoImage = $appClientSetting->getAppHomeLogoImage();
									$appHomeLogoImageTemp = $appClientSetting->getAppHomeLogoImage();
									$receiptLogoImage = $appClientSetting->getReceiptLogoImage();
									$receiptLogoImageTemp = $appClientSetting->getReceiptLogoImage();
									$facilityCheckingOnlyFlag = $appClientSetting->getFacilityCheckingOnlyFlag();
									$callingFormatPattern = $appClientSetting->getCallingFormatPattern();
									$interFacilityFlag = $appClientSetting->getInterFacilityFlag();
									$projectName = $appClientSetting->getProjectName();
									$projectNameId = $appClientSetting->getProjectNameId();
									$centralizedServerUrl = $appClientSetting->getCentralizedServerUrl();
									$remoteServerDomain = $appClientSetting->getRemoteServerDomain();
									$remoteServerProtocol = $appClientSetting->getRemoteServerProtocol();
									$remoteServerPort = $appClientSetting->getRemoteServerPort();
									$localServerDomain = $appClientSetting->getLocalServerDomain();
									$localServerProtocol = $appClientSetting->getLocalServerProtocol();
									$localServerPort = $appClientSetting->getLocalServerPort();
									$remoteCctvDomain = $appClientSetting->getRemoteCctvDomain();
									$remoteCctvProtocol = $appClientSetting->getRemoteCctvProtocol();
									$remoteCctvPort = $appClientSetting->getRemoteCctvPort();
									$localCctvDomain = $appClientSetting->getLocalCctvDomain();
									$localCctvProtocol = $appClientSetting->getLocalCctvProtocol();
									$localCctvPort = $appClientSetting->getLocalCctvPort();
									
									$PNDomain = $appClientSetting->getPNDomain();
									$PNProtocol = $appClientSetting->getPNProtocol();
									$PNPort = $appClientSetting->getPNPort();

									$tutorialImage = $appClientSetting->getTutorialImage();
									$tutorialImageTemp = $appClientSetting->getTutorialImage();
									$tutorialImageEnable = $appClientSetting->getTutorialImageEnable();
									$modifiedBy = $appClientSetting->getModifiedBy();
									$modifiedDate = $appClientSetting->getModifiedDate()->format('d/m/Y H:i A');
								}
							?>							
							
							<form action="<?php echo $action ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
								<div class="control-group">
									<label class="control-label" for="input1">App Home Idle Time</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="App Home Idle Time" name="appHomeIdleTime" value="<?php echo $appHomeIdleTime; ?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">App Standby Idle Time</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="App Standby Idle Time" name="appStandbyIdleTime" value="<?php echo $appStandbyIdleTime; ?>"/>
									</div>
								</div>
								<div class="control-group <?php if ($errAppHomeLogoImage <> '') echo 'has-error'; ?>">
									<label class="control-label">App Home Logo Image</label>
									<div class="controls">															
										<div class="fileupload <?php if ($appHomeLogoImageTemp <> '') { echo 'fileupload-exists'; } else { echo 'fileupload-new'; } ?>" data-provides="fileupload">
												<div class="form-control fileupload-new thumbnail" style="width: 342px; height: 129px;"></div>
												<div class="form-control fileupload-preview fileupload-exists thumbnail" style="max-width: 342px; max-height: 129px; line-height: 20px;"><?php if ($appHomeLogoImageTemp <> '') { ?><img src="<?php echo $imgUrl.'/'.$appHomeLogoImageTemp.'/ebulletin'; ?>"><?php } ?></div>
												<div>
														<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default" name="appHomeLogoImage" /></span>
														<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>Image Size (1280 x 385)
												</div>
										</div>
										<span class="help-block"><?php echo $errAppHomeLogoImage; ?></span>
									</div>
								</div>
								<div class="control-group <?php if ($errReceiptLogoImage <> '') echo 'has-error'; ?>">
									<label class="control-label">Receipt Logo Image</label>
									<div class="controls">															
										<div class="fileupload <?php if ($receiptLogoImageTemp <> '') { echo 'fileupload-exists'; } else { echo 'fileupload-new'; } ?>" data-provides="fileupload">
												<div class="form-control fileupload-new thumbnail" style="width: 500px; height: 45px;"></div>
												<div class="form-control fileupload-preview fileupload-exists thumbnail" style="max-width: 500px; max-height: 45px; line-height: 20px;"><?php if ($receiptLogoImageTemp <> '') { ?><img src="<?php echo $imgUrl.'/'.$receiptLogoImageTemp.'/ebulletin'; ?>"><?php } ?></div>
												<div>
														<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default" name="receiptLogoImage" /></span>
														<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
												</div>
										</div>
										Size: 500px * 45px
										<span class="help-block"><?php echo $errReceiptLogoImage; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Enable Facility Checking Only</label>
									<div class="controls">
										<input type="checkbox" value="1" name="facilityCheckingOnlyFlag" <?php if ($facilityCheckingOnlyFlag == 1) echo "checked=\"1\""; ?> /> 
									</div>
								</div>                             
								<div class="control-group">
									<label class="control-label" for="input1">Calling # Format Pattern</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Calling # Format Pattern" name="callingFormatPattern" value="<?php echo $callingFormatPattern; ?>" />
										 PBLLUU (P-Project B-Block L-Level U-Unit)
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Enable Inter-Facility</label>
									<div class="controls">
										<input type="checkbox" value="1" name="interFacilityFlag" <?php if ($interFacilityFlag == 1) echo "checked=\"1\""; ?>/> 
									</div>
								</div>                             
								<div class="control-group">
									<label class="control-label" for="input1">Project Name</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Project Name" name="projectName" value="<?php echo $projectName; ?>" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Project ID</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Project ID" name="projectNameId" value="<?php echo $projectNameId; ?>" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Centralized Server URL</label>
									<div class="controls">
										<input type="text" class="span6" id="input1" placeholder="Centralized Server URL" name="centralizedServerUrl"  value="<?php echo $centralizedServerUrl; ?>" 	/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Remote Server Details</label>
									<div class="controls">
										<input type="text" class="span2" id="input2" size="10" placeholder="Protocol" name="remoteServerProtocol"  value="<?php echo $remoteServerProtocol; ?>" />
										<input type="text" class="span6" id="input1" placeholder="Domain" name="remoteServerDomain"  value="<?php echo $remoteServerDomain; ?>" />
										<input type="text" class="span1" id="input3" size="5" placeholder="Port" name="remoteServerPort"  value="<?php echo $remoteServerPort; ?>" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Local Server Details</label>
									<div class="controls">
										<input type="text" class="span2" id="input2" size="10" placeholder="Protocol" name="localServerProtocol"  value="<?php echo $localServerProtocol; ?>" />
										<input type="text" class="span6" id="input1" placeholder="Domain" name="localServerDomain"  value="<?php echo $localServerDomain; ?>" />
										<input type="text" class="span1" id="input3" size="5" placeholder="Port" name="localServerPort"  value="<?php echo $localServerPort; ?>" />
									</div>
								</div>

								<div class="control-group">
									<label class="control-label" for="input1">Remote CCTV Details</label>
									<div class="controls">
										<input type="text" class="span2" id="input2" size="10" placeholder="Protocol" name="remoteCctvProtocol"  value="<?php echo $remoteCctvProtocol; ?>" />
										<input type="text" class="span6" id="input1" placeholder="Domain" name="remoteCctvDomain"  value="<?php echo $remoteCctvDomain; ?>" />
										<input type="text" class="span1" id="input3" size="5" placeholder="Port" name="remoteCctvPort"  value="<?php echo $remoteCctvPort; ?>" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input1">Local CCTV Details</label>
									<div class="controls">
										<input type="text" class="span2" id="input2" size="10" placeholder="Protocol" name="localCctvProtocol"  value="<?php echo $localCctvProtocol; ?>" />
										<input type="text" class="span6" id="input1" placeholder="Domain" name="localCctvDomain"  value="<?php echo $localCctvDomain; ?>" />
										<input type="text" class="span1" id="input3" size="5" placeholder="Port" name="localCctvPort"  value="<?php echo $localCctvPort; ?>" />
									</div>
								</div>
								
								<div class="control-group">
									<label class="control-label" for="input1">Push Notification URL</label>
									<div class="controls">
										<input type="text" class="span2" id="input2" size="10" placeholder="Protocol" name="PNProtocol"  value="<?php echo $PNProtocol; ?>" />
										<input type="text" class="span6" id="input1" placeholder="Domain" name="PNDomain"  value="<?php echo $PNDomain; ?>" />
										<input type="text" class="span1" id="input3" size="5" placeholder="Port" name="PNPort"  value="<?php echo $PNPort; ?>" />
									</div>
								</div>
								
								<div class="control-group <?php if ($errTutorialImage <> '') echo 'has-error'; ?>">
									<label class="control-label">Tutorial Image</label>
									<div class="controls">															
										<div class="fileupload <?php if ($tutorialImageTemp <> '') { echo 'fileupload-exists'; } else { echo 'fileupload-new'; } ?>" data-provides="fileupload">
												<div class="form-control fileupload-new thumbnail" style="width: 200px; height: 200px;"></div>
												<div class="form-control fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 200px; line-height: 20px;"><?php if ($tutorialImageTemp <> '') { ?><img src="<?php echo $imgUrl.'/'.$tutorialImageTemp.'/ebulletin'; ?>"><?php } ?></div>
												<div>
														<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" class="default" name="tutorialImage" /></span>
														<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
												</div>
										</div>
										<span class="help-block"><?php echo $errTutorialImage; ?></span>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Enable Tutorial</label>
									<div class="controls">
										<input type="checkbox" value="1" name="tutorialImageEnable" <?php if ($tutorialImageEnable == 1) echo "checked=\"1\""; ?>/> 
										<span class="label label-important">Important</span> Caution: disable the flag will remove tutorial image
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="autosync">App Autosync Interval</label>
									<div class="controls">
										<input type="text" class="span2" id="autosync" size="10" placeholder="App Autosync Interval" name="appAutoSyncInterval"  value="<?php echo $appAutoSyncInterval; ?>" /> Day
									</div>
								</div>                             
								<div class="control-group">
									<label class="control-label" for="input2">Created By</label>
									<div class="controls">
										<label><?php echo $modifiedBy; ?></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="input2">Created Date</label>
									<div class="controls">
										<label><?php echo $modifiedDate; ?></label>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn btn-primary">Update</button>
									<input type="hidden" name="sid" value="<?php echo $sid; ?>">
									<input type="hidden" name="appHomeLogoImageTemp" value="<?php echo $appHomeLogoImageTemp; ?>">
									<input type="hidden" name="receiptLogoImageTemp" value="<?php echo $receiptLogoImageTemp; ?>">
									<input type="hidden" name="tutorialImageTemp" value="<?php echo $tutorialImageTemp; ?>">
									<input type="hidden" name="modifiedBy" value="<?php echo $modifiedBy; ?>">
									<input type="hidden" name="modifiedDate" value="<?php echo $modifiedDate; ?>">																		
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div id="footer">
		2015 &copy; Rakanoth Sdn Bhd.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-arrow-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="assets/js/jquery-1.8.2.min.js"></script>		
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.blockui.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap-fileupload.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>	
	<script type="text/javascript" src="assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
	<script type="text/javascript" src="assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>   
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>	
	<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="assets/fancybox/source/jquery.fancybox.pack.js"></script>
	<script src="assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {		
			// initiate layout and plugins
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
