<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<?php

require_once 'header.php';

$errUsername = '';
$errPassword = '';

$projectId = '';
$username = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $hasError = false;
  
  $username = isset($_REQUEST['username']) ? $_REQUEST['username'] : '';
  $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : '';


  if ($username == '') {
    $hasError = true;
    $errUsername = 'Please enter the username.';
  }

  if ($password == '') {
    $hasError = true;
    $errPassword = 'Please enter the password.';
  }
  
  if (!$hasError) {
    $userSelected = UserQuery::create()->filterByUsername($username)->find()->getData();
    if (count($userSelected) == 0) {
      $errUsername = 'Invalid Project ID, Username or Password.';
      $errPassword = 'Invalid Project ID, Username or Password.';
    } else {
      $userSelected = $userSelected[0];
      $passwordDb = $userSelected->getPassword();
      if (hash('sha256', $password) != $passwordDb) {
        $errUsername = 'Invalid Username or Password.';
        $errPassword = 'Invalid Username or Password.';        
      } else {
        session_start();
        $_SESSION['name'] = $userSelected->getName();
        $_SESSION['userId'] = $userSelected->getId();
        $_SESSION['username'] = $userSelected->getUsername();
        $_SESSION['userRoleId'] = $userSelected->getUserRoleId();

//print_r($_SESSION); exit;
        
        // check Role is active/inactive
        $roleSelected = UserRoleQuery::create()->filterById($_SESSION['userRoleId'])->filterByActive('1')->find()->getData();
        if (count($roleSelected) == 0) {
          session_destroy();
          $errUsername = 'Your account has been suspended. Please check with Administrator.';
        } else {
          $userRoleSelected = UserRoleModuleQuery::create()->filterByUserRoleId($_SESSION['userRoleId'])->find()->getData();
          foreach ($userRoleSelected as $userRole) {
            $moduleSelected = ModuleQuery::create()->findPK($userRole->getModuleId());
            $_SESSION[$moduleSelected->getModule()] = '1';
          }
          
          header("Location: ./index.php");
          die();                  
        }        
      }
    }
  }
}
?>
<head>
  <meta charset="utf-8" />
  <title>Smart Home System Administration Portal</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="Rakanoth Sdn Bhd" name="author" />
  <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="assets/css/style.css" rel="stylesheet" />
  <link href="assets/css/style_responsive.css" rel="stylesheet" />
  <link href="assets/css/style_default.css" rel="stylesheet" id="style_color" />
  <link rel="stylesheet" type="text/css" href="assets/uniform/css/uniform.default.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
  <!-- BEGIN LOGO -->
  <div id="logo" class="center">
    
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div id="login">
    <!-- BEGIN LOGIN FORM -->
    <form id="loginform" class="form-vertical no-padding no-margin" action="./login.php" method="post">
      <div class="control-group <?php if ($errUsername <> '') echo 'has-error'; ?>">
        <div class="controls">
          <div class="input-prepend">
            <span class="add-on"><i class="icon-user"></i></span><input class="form-control" id="input-username" type="text" placeholder="Login" name="username" value="<?php echo $username; ?>" />
          </div>
          <span class="help-block"><?php echo $errUsername; ?></span>
        </div>
      </div>
      <div class="control-group <?php if ($errPassword <> '') echo 'has-error'; ?>">
        <div class="controls">
          <div class="input-prepend">
            <span class="add-on"><i class="icon-lock"></i></span><input class="form-control" id="input-password" type="password" placeholder="Password" name="password" />
          </div>
          <span class="help-block"><?php echo $errPassword; ?></span>
          <div class="clearfix space5"></div>
        </div>
      </div>
      <input type="submit" id="login-btn" class="btn btn-block btn-inverse" value="Login" />
    </form>
    <!-- END LOGIN FORM -->        
  </div>
  <!-- END LOGIN -->
  <!-- BEGIN COPYRIGHT -->
  <div id="login-copyright">
    2016 &copy; Shipper.co.id
  </div>
  <!-- END COPYRIGHT -->
  <!-- BEGIN JAVASCRIPTS -->
  <script src="assets/js/jquery-1.8.2.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.blockui.js"></script>
  <script src="assets/js/app.js"></script>
  <script>
    jQuery(document).ready(function() {     
      App.initLogin();
    });
  </script>
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
