<?php

use Base\DriversPositionQuery as BaseDriversPositionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'drivers_position' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DriversPositionQuery extends BaseDriversPositionQuery
{
	
	function insertPosition($data, $header){
		
		//check position is same or not
		$position = self::Create()->filterByDriverId($header->username)->filterByLatitude($data->latitude)->filterByLongitude($data->longitude)->Count();
		if($position > 0) {
			exit;
		}else{		
			$position = new DriversPosition();
			$position->setDriverId($header->username);
			$position->setLatitude($data->latitude);
			$position->setLongitude($data->longitude);
			$position->save();
			return $position;	
		}			
	}
	
}
