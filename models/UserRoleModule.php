<?php

use Base\UserRoleModule as BaseUserRoleModule;

/**
 * Skeleton subclass for representing a row from the 'userRole_Module' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserRoleModule extends BaseUserRoleModule
{

}
