<?php

namespace Base;

use \CommonAreaCameraQuery as ChildCommonAreaCameraQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\CommonAreaCameraTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'commonAreaCamera' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class CommonAreaCamera implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\CommonAreaCameraTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the projectid field.
     *
     * @var        string
     */
    protected $projectid;

    /**
     * The value for the location field.
     *
     * @var        string
     */
    protected $location;

    /**
     * The value for the cameraname field.
     *
     * @var        string
     */
    protected $cameraname;

    /**
     * The value for the protocol field.
     *
     * @var        string
     */
    protected $protocol;

    /**
     * The value for the ipaddress field.
     *
     * @var        string
     */
    protected $ipaddress;

    /**
     * The value for the port field.
     *
     * @var        string
     */
    protected $port;

    /**
     * The value for the cameratype field.
     *
     * @var        string
     */
    protected $cameratype;

    /**
     * The value for the dvrusername field.
     *
     * @var        string
     */
    protected $dvrusername;

    /**
     * The value for the dvrpassword field.
     *
     * @var        string
     */
    protected $dvrpassword;

    /**
     * The value for the dvrchannel field.
     *
     * @var        string
     */
    protected $dvrchannel;

    /**
     * The value for the ftppath field.
     *
     * @var        string
     */
    protected $ftppath;

    /**
     * The value for the displayorder field.
     *
     * @var        int
     */
    protected $displayorder;

    /**
     * The value for the enable field.
     * 1=Active, 0=Inactive
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $enable;

    /**
     * The value for the created_by field.
     *
     * @var        string
     */
    protected $created_by;

    /**
     * The value for the created_date field.
     *
     * @var        \DateTime
     */
    protected $created_date;

    /**
     * The value for the modified_by field.
     *
     * @var        string
     */
    protected $modified_by;

    /**
     * The value for the modified_date field.
     *
     * @var        \DateTime
     */
    protected $modified_date;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->enable = 0;
    }

    /**
     * Initializes internal state of Base\CommonAreaCamera object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>CommonAreaCamera</code> instance.  If
     * <code>obj</code> is an instance of <code>CommonAreaCamera</code>, delegates to
     * <code>equals(CommonAreaCamera)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|CommonAreaCamera The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [projectid] column value.
     *
     * @return string
     */
    public function getProjectId()
    {
        return $this->projectid;
    }

    /**
     * Get the [location] column value.
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Get the [cameraname] column value.
     *
     * @return string
     */
    public function getCameraName()
    {
        return $this->cameraname;
    }

    /**
     * Get the [protocol] column value.
     *
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * Get the [ipaddress] column value.
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipaddress;
    }

    /**
     * Get the [port] column value.
     *
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Get the [cameratype] column value.
     *
     * @return string
     */
    public function getCameraType()
    {
        return $this->cameratype;
    }

    /**
     * Get the [dvrusername] column value.
     *
     * @return string
     */
    public function getDvrUsername()
    {
        return $this->dvrusername;
    }

    /**
     * Get the [dvrpassword] column value.
     *
     * @return string
     */
    public function getDvrPassword()
    {
        return $this->dvrpassword;
    }

    /**
     * Get the [dvrchannel] column value.
     *
     * @return string
     */
    public function getDvrChannel()
    {
        return $this->dvrchannel;
    }

    /**
     * Get the [ftppath] column value.
     *
     * @return string
     */
    public function getFtpPath()
    {
        return $this->ftppath;
    }

    /**
     * Get the [displayorder] column value.
     *
     * @return int
     */
    public function getDisplayOrder()
    {
        return $this->displayorder;
    }

    /**
     * Get the [enable] column value.
     * 1=Active, 0=Inactive
     * @return int
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Get the [created_by] column value.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Get the [optionally formatted] temporal [created_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedDate($format = NULL)
    {
        if ($format === null) {
            return $this->created_date;
        } else {
            return $this->created_date instanceof \DateTime ? $this->created_date->format($format) : null;
        }
    }

    /**
     * Get the [modified_by] column value.
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Get the [optionally formatted] temporal [modified_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModifiedDate($format = NULL)
    {
        if ($format === null) {
            return $this->modified_date;
        } else {
            return $this->modified_date instanceof \DateTime ? $this->modified_date->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [projectid] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setProjectId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->projectid !== $v) {
            $this->projectid = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_PROJECTID] = true;
        }

        return $this;
    } // setProjectId()

    /**
     * Set the value of [location] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setLocation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->location !== $v) {
            $this->location = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_LOCATION] = true;
        }

        return $this;
    } // setLocation()

    /**
     * Set the value of [cameraname] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setCameraName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cameraname !== $v) {
            $this->cameraname = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_CAMERANAME] = true;
        }

        return $this;
    } // setCameraName()

    /**
     * Set the value of [protocol] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setProtocol($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->protocol !== $v) {
            $this->protocol = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_PROTOCOL] = true;
        }

        return $this;
    } // setProtocol()

    /**
     * Set the value of [ipaddress] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setIpAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ipaddress !== $v) {
            $this->ipaddress = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_IPADDRESS] = true;
        }

        return $this;
    } // setIpAddress()

    /**
     * Set the value of [port] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setPort($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->port !== $v) {
            $this->port = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_PORT] = true;
        }

        return $this;
    } // setPort()

    /**
     * Set the value of [cameratype] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setCameraType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cameratype !== $v) {
            $this->cameratype = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_CAMERATYPE] = true;
        }

        return $this;
    } // setCameraType()

    /**
     * Set the value of [dvrusername] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setDvrUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dvrusername !== $v) {
            $this->dvrusername = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_DVRUSERNAME] = true;
        }

        return $this;
    } // setDvrUsername()

    /**
     * Set the value of [dvrpassword] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setDvrPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dvrpassword !== $v) {
            $this->dvrpassword = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_DVRPASSWORD] = true;
        }

        return $this;
    } // setDvrPassword()

    /**
     * Set the value of [dvrchannel] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setDvrChannel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dvrchannel !== $v) {
            $this->dvrchannel = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_DVRCHANNEL] = true;
        }

        return $this;
    } // setDvrChannel()

    /**
     * Set the value of [ftppath] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setFtpPath($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ftppath !== $v) {
            $this->ftppath = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_FTPPATH] = true;
        }

        return $this;
    } // setFtpPath()

    /**
     * Set the value of [displayorder] column.
     *
     * @param int $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setDisplayOrder($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->displayorder !== $v) {
            $this->displayorder = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_DISPLAYORDER] = true;
        }

        return $this;
    } // setDisplayOrder()

    /**
     * Set the value of [enable] column.
     * 1=Active, 0=Inactive
     * @param int $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setEnable($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->enable !== $v) {
            $this->enable = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_ENABLE] = true;
        }

        return $this;
    } // setEnable()

    /**
     * Set the value of [created_by] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setCreatedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->created_by !== $v) {
            $this->created_by = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_CREATED_BY] = true;
        }

        return $this;
    } // setCreatedBy()

    /**
     * Sets the value of [created_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setCreatedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_date !== null || $dt !== null) {
            if ($this->created_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->created_date->format("Y-m-d H:i:s")) {
                $this->created_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CommonAreaCameraTableMap::COL_CREATED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedDate()

    /**
     * Set the value of [modified_by] column.
     *
     * @param string $v new value
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setModifiedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modified_by !== $v) {
            $this->modified_by = $v;
            $this->modifiedColumns[CommonAreaCameraTableMap::COL_MODIFIED_BY] = true;
        }

        return $this;
    } // setModifiedBy()

    /**
     * Sets the value of [modified_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\CommonAreaCamera The current object (for fluent API support)
     */
    public function setModifiedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified_date !== null || $dt !== null) {
            if ($this->modified_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->modified_date->format("Y-m-d H:i:s")) {
                $this->modified_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CommonAreaCameraTableMap::COL_MODIFIED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setModifiedDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->enable !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CommonAreaCameraTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CommonAreaCameraTableMap::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->projectid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CommonAreaCameraTableMap::translateFieldName('Location', TableMap::TYPE_PHPNAME, $indexType)];
            $this->location = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CommonAreaCameraTableMap::translateFieldName('CameraName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cameraname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CommonAreaCameraTableMap::translateFieldName('Protocol', TableMap::TYPE_PHPNAME, $indexType)];
            $this->protocol = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CommonAreaCameraTableMap::translateFieldName('IpAddress', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ipaddress = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CommonAreaCameraTableMap::translateFieldName('Port', TableMap::TYPE_PHPNAME, $indexType)];
            $this->port = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CommonAreaCameraTableMap::translateFieldName('CameraType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cameratype = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CommonAreaCameraTableMap::translateFieldName('DvrUsername', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dvrusername = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CommonAreaCameraTableMap::translateFieldName('DvrPassword', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dvrpassword = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CommonAreaCameraTableMap::translateFieldName('DvrChannel', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dvrchannel = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CommonAreaCameraTableMap::translateFieldName('FtpPath', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ftppath = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : CommonAreaCameraTableMap::translateFieldName('DisplayOrder', TableMap::TYPE_PHPNAME, $indexType)];
            $this->displayorder = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : CommonAreaCameraTableMap::translateFieldName('Enable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->enable = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : CommonAreaCameraTableMap::translateFieldName('CreatedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : CommonAreaCameraTableMap::translateFieldName('CreatedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : CommonAreaCameraTableMap::translateFieldName('ModifiedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modified_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : CommonAreaCameraTableMap::translateFieldName('ModifiedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->modified_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 18; // 18 = CommonAreaCameraTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\CommonAreaCamera'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CommonAreaCameraTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCommonAreaCameraQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see CommonAreaCamera::setDeleted()
     * @see CommonAreaCamera::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommonAreaCameraTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCommonAreaCameraQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommonAreaCameraTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonAreaCameraTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_PROJECTID)) {
            $modifiedColumns[':p' . $index++]  = 'projectId';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_LOCATION)) {
            $modifiedColumns[':p' . $index++]  = 'location';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_CAMERANAME)) {
            $modifiedColumns[':p' . $index++]  = 'cameraName';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_PROTOCOL)) {
            $modifiedColumns[':p' . $index++]  = 'protocol';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_IPADDRESS)) {
            $modifiedColumns[':p' . $index++]  = 'ipAddress';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_PORT)) {
            $modifiedColumns[':p' . $index++]  = 'port';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_CAMERATYPE)) {
            $modifiedColumns[':p' . $index++]  = 'cameraType';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_DVRUSERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'dvrUsername';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_DVRPASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'dvrPassword';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_DVRCHANNEL)) {
            $modifiedColumns[':p' . $index++]  = 'dvrChannel';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_FTPPATH)) {
            $modifiedColumns[':p' . $index++]  = 'ftpPath';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_DISPLAYORDER)) {
            $modifiedColumns[':p' . $index++]  = 'displayOrder';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_ENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'enable';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_CREATED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'created_by';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_CREATED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'created_date';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_MODIFIED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'modified_by';
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_MODIFIED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'modified_date';
        }

        $sql = sprintf(
            'INSERT INTO commonAreaCamera (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                    case 'projectId':
                        $stmt->bindValue($identifier, $this->projectid, PDO::PARAM_STR);
                        break;
                    case 'location':
                        $stmt->bindValue($identifier, $this->location, PDO::PARAM_STR);
                        break;
                    case 'cameraName':
                        $stmt->bindValue($identifier, $this->cameraname, PDO::PARAM_STR);
                        break;
                    case 'protocol':
                        $stmt->bindValue($identifier, $this->protocol, PDO::PARAM_STR);
                        break;
                    case 'ipAddress':
                        $stmt->bindValue($identifier, $this->ipaddress, PDO::PARAM_STR);
                        break;
                    case 'port':
                        $stmt->bindValue($identifier, $this->port, PDO::PARAM_STR);
                        break;
                    case 'cameraType':
                        $stmt->bindValue($identifier, $this->cameratype, PDO::PARAM_STR);
                        break;
                    case 'dvrUsername':
                        $stmt->bindValue($identifier, $this->dvrusername, PDO::PARAM_STR);
                        break;
                    case 'dvrPassword':
                        $stmt->bindValue($identifier, $this->dvrpassword, PDO::PARAM_STR);
                        break;
                    case 'dvrChannel':
                        $stmt->bindValue($identifier, $this->dvrchannel, PDO::PARAM_STR);
                        break;
                    case 'ftpPath':
                        $stmt->bindValue($identifier, $this->ftppath, PDO::PARAM_STR);
                        break;
                    case 'displayOrder':
                        $stmt->bindValue($identifier, $this->displayorder, PDO::PARAM_INT);
                        break;
                    case 'enable':
                        $stmt->bindValue($identifier, $this->enable, PDO::PARAM_INT);
                        break;
                    case 'created_by':
                        $stmt->bindValue($identifier, $this->created_by, PDO::PARAM_STR);
                        break;
                    case 'created_date':
                        $stmt->bindValue($identifier, $this->created_date ? $this->created_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'modified_by':
                        $stmt->bindValue($identifier, $this->modified_by, PDO::PARAM_STR);
                        break;
                    case 'modified_date':
                        $stmt->bindValue($identifier, $this->modified_date ? $this->modified_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CommonAreaCameraTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getProjectId();
                break;
            case 2:
                return $this->getLocation();
                break;
            case 3:
                return $this->getCameraName();
                break;
            case 4:
                return $this->getProtocol();
                break;
            case 5:
                return $this->getIpAddress();
                break;
            case 6:
                return $this->getPort();
                break;
            case 7:
                return $this->getCameraType();
                break;
            case 8:
                return $this->getDvrUsername();
                break;
            case 9:
                return $this->getDvrPassword();
                break;
            case 10:
                return $this->getDvrChannel();
                break;
            case 11:
                return $this->getFtpPath();
                break;
            case 12:
                return $this->getDisplayOrder();
                break;
            case 13:
                return $this->getEnable();
                break;
            case 14:
                return $this->getCreatedBy();
                break;
            case 15:
                return $this->getCreatedDate();
                break;
            case 16:
                return $this->getModifiedBy();
                break;
            case 17:
                return $this->getModifiedDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['CommonAreaCamera'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonAreaCamera'][$this->hashCode()] = true;
        $keys = CommonAreaCameraTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getProjectId(),
            $keys[2] => $this->getLocation(),
            $keys[3] => $this->getCameraName(),
            $keys[4] => $this->getProtocol(),
            $keys[5] => $this->getIpAddress(),
            $keys[6] => $this->getPort(),
            $keys[7] => $this->getCameraType(),
            $keys[8] => $this->getDvrUsername(),
            $keys[9] => $this->getDvrPassword(),
            $keys[10] => $this->getDvrChannel(),
            $keys[11] => $this->getFtpPath(),
            $keys[12] => $this->getDisplayOrder(),
            $keys[13] => $this->getEnable(),
            $keys[14] => $this->getCreatedBy(),
            $keys[15] => $this->getCreatedDate(),
            $keys[16] => $this->getModifiedBy(),
            $keys[17] => $this->getModifiedDate(),
        );
        if ($result[$keys[15]] instanceof \DateTime) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\CommonAreaCamera
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CommonAreaCameraTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\CommonAreaCamera
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setProjectId($value);
                break;
            case 2:
                $this->setLocation($value);
                break;
            case 3:
                $this->setCameraName($value);
                break;
            case 4:
                $this->setProtocol($value);
                break;
            case 5:
                $this->setIpAddress($value);
                break;
            case 6:
                $this->setPort($value);
                break;
            case 7:
                $this->setCameraType($value);
                break;
            case 8:
                $this->setDvrUsername($value);
                break;
            case 9:
                $this->setDvrPassword($value);
                break;
            case 10:
                $this->setDvrChannel($value);
                break;
            case 11:
                $this->setFtpPath($value);
                break;
            case 12:
                $this->setDisplayOrder($value);
                break;
            case 13:
                $this->setEnable($value);
                break;
            case 14:
                $this->setCreatedBy($value);
                break;
            case 15:
                $this->setCreatedDate($value);
                break;
            case 16:
                $this->setModifiedBy($value);
                break;
            case 17:
                $this->setModifiedDate($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CommonAreaCameraTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setProjectId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setLocation($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCameraName($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setProtocol($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIpAddress($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPort($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCameraType($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDvrUsername($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDvrPassword($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDvrChannel($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setFtpPath($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setDisplayOrder($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setEnable($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setCreatedBy($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setCreatedDate($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setModifiedBy($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setModifiedDate($arr[$keys[17]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\CommonAreaCamera The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonAreaCameraTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_ID)) {
            $criteria->add(CommonAreaCameraTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_PROJECTID)) {
            $criteria->add(CommonAreaCameraTableMap::COL_PROJECTID, $this->projectid);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_LOCATION)) {
            $criteria->add(CommonAreaCameraTableMap::COL_LOCATION, $this->location);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_CAMERANAME)) {
            $criteria->add(CommonAreaCameraTableMap::COL_CAMERANAME, $this->cameraname);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_PROTOCOL)) {
            $criteria->add(CommonAreaCameraTableMap::COL_PROTOCOL, $this->protocol);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_IPADDRESS)) {
            $criteria->add(CommonAreaCameraTableMap::COL_IPADDRESS, $this->ipaddress);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_PORT)) {
            $criteria->add(CommonAreaCameraTableMap::COL_PORT, $this->port);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_CAMERATYPE)) {
            $criteria->add(CommonAreaCameraTableMap::COL_CAMERATYPE, $this->cameratype);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_DVRUSERNAME)) {
            $criteria->add(CommonAreaCameraTableMap::COL_DVRUSERNAME, $this->dvrusername);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_DVRPASSWORD)) {
            $criteria->add(CommonAreaCameraTableMap::COL_DVRPASSWORD, $this->dvrpassword);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_DVRCHANNEL)) {
            $criteria->add(CommonAreaCameraTableMap::COL_DVRCHANNEL, $this->dvrchannel);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_FTPPATH)) {
            $criteria->add(CommonAreaCameraTableMap::COL_FTPPATH, $this->ftppath);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_DISPLAYORDER)) {
            $criteria->add(CommonAreaCameraTableMap::COL_DISPLAYORDER, $this->displayorder);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_ENABLE)) {
            $criteria->add(CommonAreaCameraTableMap::COL_ENABLE, $this->enable);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_CREATED_BY)) {
            $criteria->add(CommonAreaCameraTableMap::COL_CREATED_BY, $this->created_by);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_CREATED_DATE)) {
            $criteria->add(CommonAreaCameraTableMap::COL_CREATED_DATE, $this->created_date);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_MODIFIED_BY)) {
            $criteria->add(CommonAreaCameraTableMap::COL_MODIFIED_BY, $this->modified_by);
        }
        if ($this->isColumnModified(CommonAreaCameraTableMap::COL_MODIFIED_DATE)) {
            $criteria->add(CommonAreaCameraTableMap::COL_MODIFIED_DATE, $this->modified_date);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCommonAreaCameraQuery::create();
        $criteria->add(CommonAreaCameraTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \CommonAreaCamera (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setProjectId($this->getProjectId());
        $copyObj->setLocation($this->getLocation());
        $copyObj->setCameraName($this->getCameraName());
        $copyObj->setProtocol($this->getProtocol());
        $copyObj->setIpAddress($this->getIpAddress());
        $copyObj->setPort($this->getPort());
        $copyObj->setCameraType($this->getCameraType());
        $copyObj->setDvrUsername($this->getDvrUsername());
        $copyObj->setDvrPassword($this->getDvrPassword());
        $copyObj->setDvrChannel($this->getDvrChannel());
        $copyObj->setFtpPath($this->getFtpPath());
        $copyObj->setDisplayOrder($this->getDisplayOrder());
        $copyObj->setEnable($this->getEnable());
        $copyObj->setCreatedBy($this->getCreatedBy());
        $copyObj->setCreatedDate($this->getCreatedDate());
        $copyObj->setModifiedBy($this->getModifiedBy());
        $copyObj->setModifiedDate($this->getModifiedDate());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \CommonAreaCamera Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->projectid = null;
        $this->location = null;
        $this->cameraname = null;
        $this->protocol = null;
        $this->ipaddress = null;
        $this->port = null;
        $this->cameratype = null;
        $this->dvrusername = null;
        $this->dvrpassword = null;
        $this->dvrchannel = null;
        $this->ftppath = null;
        $this->displayorder = null;
        $this->enable = null;
        $this->created_by = null;
        $this->created_date = null;
        $this->modified_by = null;
        $this->modified_date = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonAreaCameraTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
