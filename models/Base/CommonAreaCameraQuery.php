<?php

namespace Base;

use \CommonAreaCamera as ChildCommonAreaCamera;
use \CommonAreaCameraQuery as ChildCommonAreaCameraQuery;
use \Exception;
use \PDO;
use Map\CommonAreaCameraTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'commonAreaCamera' table.
 *
 *
 *
 * @method     ChildCommonAreaCameraQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCommonAreaCameraQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildCommonAreaCameraQuery orderByLocation($order = Criteria::ASC) Order by the location column
 * @method     ChildCommonAreaCameraQuery orderByCameraName($order = Criteria::ASC) Order by the cameraName column
 * @method     ChildCommonAreaCameraQuery orderByProtocol($order = Criteria::ASC) Order by the protocol column
 * @method     ChildCommonAreaCameraQuery orderByIpAddress($order = Criteria::ASC) Order by the ipAddress column
 * @method     ChildCommonAreaCameraQuery orderByPort($order = Criteria::ASC) Order by the port column
 * @method     ChildCommonAreaCameraQuery orderByCameraType($order = Criteria::ASC) Order by the cameraType column
 * @method     ChildCommonAreaCameraQuery orderByDvrUsername($order = Criteria::ASC) Order by the dvrUsername column
 * @method     ChildCommonAreaCameraQuery orderByDvrPassword($order = Criteria::ASC) Order by the dvrPassword column
 * @method     ChildCommonAreaCameraQuery orderByDvrChannel($order = Criteria::ASC) Order by the dvrChannel column
 * @method     ChildCommonAreaCameraQuery orderByFtpPath($order = Criteria::ASC) Order by the ftpPath column
 * @method     ChildCommonAreaCameraQuery orderByDisplayOrder($order = Criteria::ASC) Order by the displayOrder column
 * @method     ChildCommonAreaCameraQuery orderByEnable($order = Criteria::ASC) Order by the enable column
 * @method     ChildCommonAreaCameraQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildCommonAreaCameraQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildCommonAreaCameraQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildCommonAreaCameraQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildCommonAreaCameraQuery groupById() Group by the id column
 * @method     ChildCommonAreaCameraQuery groupByProjectId() Group by the projectId column
 * @method     ChildCommonAreaCameraQuery groupByLocation() Group by the location column
 * @method     ChildCommonAreaCameraQuery groupByCameraName() Group by the cameraName column
 * @method     ChildCommonAreaCameraQuery groupByProtocol() Group by the protocol column
 * @method     ChildCommonAreaCameraQuery groupByIpAddress() Group by the ipAddress column
 * @method     ChildCommonAreaCameraQuery groupByPort() Group by the port column
 * @method     ChildCommonAreaCameraQuery groupByCameraType() Group by the cameraType column
 * @method     ChildCommonAreaCameraQuery groupByDvrUsername() Group by the dvrUsername column
 * @method     ChildCommonAreaCameraQuery groupByDvrPassword() Group by the dvrPassword column
 * @method     ChildCommonAreaCameraQuery groupByDvrChannel() Group by the dvrChannel column
 * @method     ChildCommonAreaCameraQuery groupByFtpPath() Group by the ftpPath column
 * @method     ChildCommonAreaCameraQuery groupByDisplayOrder() Group by the displayOrder column
 * @method     ChildCommonAreaCameraQuery groupByEnable() Group by the enable column
 * @method     ChildCommonAreaCameraQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildCommonAreaCameraQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildCommonAreaCameraQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildCommonAreaCameraQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildCommonAreaCameraQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCommonAreaCameraQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCommonAreaCameraQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCommonAreaCameraQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCommonAreaCameraQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCommonAreaCameraQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCommonAreaCamera findOne(ConnectionInterface $con = null) Return the first ChildCommonAreaCamera matching the query
 * @method     ChildCommonAreaCamera findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCommonAreaCamera matching the query, or a new ChildCommonAreaCamera object populated from the query conditions when no match is found
 *
 * @method     ChildCommonAreaCamera findOneById(string $id) Return the first ChildCommonAreaCamera filtered by the id column
 * @method     ChildCommonAreaCamera findOneByProjectId(string $projectId) Return the first ChildCommonAreaCamera filtered by the projectId column
 * @method     ChildCommonAreaCamera findOneByLocation(string $location) Return the first ChildCommonAreaCamera filtered by the location column
 * @method     ChildCommonAreaCamera findOneByCameraName(string $cameraName) Return the first ChildCommonAreaCamera filtered by the cameraName column
 * @method     ChildCommonAreaCamera findOneByProtocol(string $protocol) Return the first ChildCommonAreaCamera filtered by the protocol column
 * @method     ChildCommonAreaCamera findOneByIpAddress(string $ipAddress) Return the first ChildCommonAreaCamera filtered by the ipAddress column
 * @method     ChildCommonAreaCamera findOneByPort(string $port) Return the first ChildCommonAreaCamera filtered by the port column
 * @method     ChildCommonAreaCamera findOneByCameraType(string $cameraType) Return the first ChildCommonAreaCamera filtered by the cameraType column
 * @method     ChildCommonAreaCamera findOneByDvrUsername(string $dvrUsername) Return the first ChildCommonAreaCamera filtered by the dvrUsername column
 * @method     ChildCommonAreaCamera findOneByDvrPassword(string $dvrPassword) Return the first ChildCommonAreaCamera filtered by the dvrPassword column
 * @method     ChildCommonAreaCamera findOneByDvrChannel(string $dvrChannel) Return the first ChildCommonAreaCamera filtered by the dvrChannel column
 * @method     ChildCommonAreaCamera findOneByFtpPath(string $ftpPath) Return the first ChildCommonAreaCamera filtered by the ftpPath column
 * @method     ChildCommonAreaCamera findOneByDisplayOrder(int $displayOrder) Return the first ChildCommonAreaCamera filtered by the displayOrder column
 * @method     ChildCommonAreaCamera findOneByEnable(int $enable) Return the first ChildCommonAreaCamera filtered by the enable column
 * @method     ChildCommonAreaCamera findOneByCreatedBy(string $created_by) Return the first ChildCommonAreaCamera filtered by the created_by column
 * @method     ChildCommonAreaCamera findOneByCreatedDate(string $created_date) Return the first ChildCommonAreaCamera filtered by the created_date column
 * @method     ChildCommonAreaCamera findOneByModifiedBy(string $modified_by) Return the first ChildCommonAreaCamera filtered by the modified_by column
 * @method     ChildCommonAreaCamera findOneByModifiedDate(string $modified_date) Return the first ChildCommonAreaCamera filtered by the modified_date column *

 * @method     ChildCommonAreaCamera requirePk($key, ConnectionInterface $con = null) Return the ChildCommonAreaCamera by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOne(ConnectionInterface $con = null) Return the first ChildCommonAreaCamera matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommonAreaCamera requireOneById(string $id) Return the first ChildCommonAreaCamera filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByProjectId(string $projectId) Return the first ChildCommonAreaCamera filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByLocation(string $location) Return the first ChildCommonAreaCamera filtered by the location column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByCameraName(string $cameraName) Return the first ChildCommonAreaCamera filtered by the cameraName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByProtocol(string $protocol) Return the first ChildCommonAreaCamera filtered by the protocol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByIpAddress(string $ipAddress) Return the first ChildCommonAreaCamera filtered by the ipAddress column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByPort(string $port) Return the first ChildCommonAreaCamera filtered by the port column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByCameraType(string $cameraType) Return the first ChildCommonAreaCamera filtered by the cameraType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByDvrUsername(string $dvrUsername) Return the first ChildCommonAreaCamera filtered by the dvrUsername column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByDvrPassword(string $dvrPassword) Return the first ChildCommonAreaCamera filtered by the dvrPassword column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByDvrChannel(string $dvrChannel) Return the first ChildCommonAreaCamera filtered by the dvrChannel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByFtpPath(string $ftpPath) Return the first ChildCommonAreaCamera filtered by the ftpPath column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByDisplayOrder(int $displayOrder) Return the first ChildCommonAreaCamera filtered by the displayOrder column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByEnable(int $enable) Return the first ChildCommonAreaCamera filtered by the enable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByCreatedBy(string $created_by) Return the first ChildCommonAreaCamera filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByCreatedDate(string $created_date) Return the first ChildCommonAreaCamera filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByModifiedBy(string $modified_by) Return the first ChildCommonAreaCamera filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommonAreaCamera requireOneByModifiedDate(string $modified_date) Return the first ChildCommonAreaCamera filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommonAreaCamera[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCommonAreaCamera objects based on current ModelCriteria
 * @method     ChildCommonAreaCamera[]|ObjectCollection findById(string $id) Return ChildCommonAreaCamera objects filtered by the id column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByProjectId(string $projectId) Return ChildCommonAreaCamera objects filtered by the projectId column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByLocation(string $location) Return ChildCommonAreaCamera objects filtered by the location column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByCameraName(string $cameraName) Return ChildCommonAreaCamera objects filtered by the cameraName column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByProtocol(string $protocol) Return ChildCommonAreaCamera objects filtered by the protocol column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByIpAddress(string $ipAddress) Return ChildCommonAreaCamera objects filtered by the ipAddress column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByPort(string $port) Return ChildCommonAreaCamera objects filtered by the port column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByCameraType(string $cameraType) Return ChildCommonAreaCamera objects filtered by the cameraType column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByDvrUsername(string $dvrUsername) Return ChildCommonAreaCamera objects filtered by the dvrUsername column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByDvrPassword(string $dvrPassword) Return ChildCommonAreaCamera objects filtered by the dvrPassword column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByDvrChannel(string $dvrChannel) Return ChildCommonAreaCamera objects filtered by the dvrChannel column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByFtpPath(string $ftpPath) Return ChildCommonAreaCamera objects filtered by the ftpPath column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByDisplayOrder(int $displayOrder) Return ChildCommonAreaCamera objects filtered by the displayOrder column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByEnable(int $enable) Return ChildCommonAreaCamera objects filtered by the enable column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildCommonAreaCamera objects filtered by the created_by column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildCommonAreaCamera objects filtered by the created_date column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildCommonAreaCamera objects filtered by the modified_by column
 * @method     ChildCommonAreaCamera[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildCommonAreaCamera objects filtered by the modified_date column
 * @method     ChildCommonAreaCamera[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CommonAreaCameraQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CommonAreaCameraQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CommonAreaCamera', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCommonAreaCameraQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCommonAreaCameraQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCommonAreaCameraQuery) {
            return $criteria;
        }
        $query = new ChildCommonAreaCameraQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCommonAreaCamera|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonAreaCameraTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CommonAreaCameraTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCommonAreaCamera A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, location, cameraName, protocol, ipAddress, port, cameraType, dvrUsername, dvrPassword, dvrChannel, ftpPath, displayOrder, enable, created_by, created_date, modified_by, modified_date FROM commonAreaCamera WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCommonAreaCamera $obj */
            $obj = new ChildCommonAreaCamera();
            $obj->hydrate($row);
            CommonAreaCameraTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCommonAreaCamera|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the location column
     *
     * Example usage:
     * <code>
     * $query->filterByLocation('fooValue');   // WHERE location = 'fooValue'
     * $query->filterByLocation('%fooValue%'); // WHERE location LIKE '%fooValue%'
     * </code>
     *
     * @param     string $location The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByLocation($location = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($location)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $location)) {
                $location = str_replace('*', '%', $location);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_LOCATION, $location, $comparison);
    }

    /**
     * Filter the query on the cameraName column
     *
     * Example usage:
     * <code>
     * $query->filterByCameraName('fooValue');   // WHERE cameraName = 'fooValue'
     * $query->filterByCameraName('%fooValue%'); // WHERE cameraName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cameraName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByCameraName($cameraName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cameraName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cameraName)) {
                $cameraName = str_replace('*', '%', $cameraName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_CAMERANAME, $cameraName, $comparison);
    }

    /**
     * Filter the query on the protocol column
     *
     * Example usage:
     * <code>
     * $query->filterByProtocol('fooValue');   // WHERE protocol = 'fooValue'
     * $query->filterByProtocol('%fooValue%'); // WHERE protocol LIKE '%fooValue%'
     * </code>
     *
     * @param     string $protocol The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByProtocol($protocol = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($protocol)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $protocol)) {
                $protocol = str_replace('*', '%', $protocol);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_PROTOCOL, $protocol, $comparison);
    }

    /**
     * Filter the query on the ipAddress column
     *
     * Example usage:
     * <code>
     * $query->filterByIpAddress('fooValue');   // WHERE ipAddress = 'fooValue'
     * $query->filterByIpAddress('%fooValue%'); // WHERE ipAddress LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipAddress The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByIpAddress($ipAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipAddress)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ipAddress)) {
                $ipAddress = str_replace('*', '%', $ipAddress);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_IPADDRESS, $ipAddress, $comparison);
    }

    /**
     * Filter the query on the port column
     *
     * Example usage:
     * <code>
     * $query->filterByPort('fooValue');   // WHERE port = 'fooValue'
     * $query->filterByPort('%fooValue%'); // WHERE port LIKE '%fooValue%'
     * </code>
     *
     * @param     string $port The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByPort($port = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($port)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $port)) {
                $port = str_replace('*', '%', $port);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_PORT, $port, $comparison);
    }

    /**
     * Filter the query on the cameraType column
     *
     * Example usage:
     * <code>
     * $query->filterByCameraType('fooValue');   // WHERE cameraType = 'fooValue'
     * $query->filterByCameraType('%fooValue%'); // WHERE cameraType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cameraType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByCameraType($cameraType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cameraType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cameraType)) {
                $cameraType = str_replace('*', '%', $cameraType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_CAMERATYPE, $cameraType, $comparison);
    }

    /**
     * Filter the query on the dvrUsername column
     *
     * Example usage:
     * <code>
     * $query->filterByDvrUsername('fooValue');   // WHERE dvrUsername = 'fooValue'
     * $query->filterByDvrUsername('%fooValue%'); // WHERE dvrUsername LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dvrUsername The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByDvrUsername($dvrUsername = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dvrUsername)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dvrUsername)) {
                $dvrUsername = str_replace('*', '%', $dvrUsername);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_DVRUSERNAME, $dvrUsername, $comparison);
    }

    /**
     * Filter the query on the dvrPassword column
     *
     * Example usage:
     * <code>
     * $query->filterByDvrPassword('fooValue');   // WHERE dvrPassword = 'fooValue'
     * $query->filterByDvrPassword('%fooValue%'); // WHERE dvrPassword LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dvrPassword The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByDvrPassword($dvrPassword = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dvrPassword)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dvrPassword)) {
                $dvrPassword = str_replace('*', '%', $dvrPassword);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_DVRPASSWORD, $dvrPassword, $comparison);
    }

    /**
     * Filter the query on the dvrChannel column
     *
     * Example usage:
     * <code>
     * $query->filterByDvrChannel('fooValue');   // WHERE dvrChannel = 'fooValue'
     * $query->filterByDvrChannel('%fooValue%'); // WHERE dvrChannel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dvrChannel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByDvrChannel($dvrChannel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dvrChannel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dvrChannel)) {
                $dvrChannel = str_replace('*', '%', $dvrChannel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_DVRCHANNEL, $dvrChannel, $comparison);
    }

    /**
     * Filter the query on the ftpPath column
     *
     * Example usage:
     * <code>
     * $query->filterByFtpPath('fooValue');   // WHERE ftpPath = 'fooValue'
     * $query->filterByFtpPath('%fooValue%'); // WHERE ftpPath LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ftpPath The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByFtpPath($ftpPath = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ftpPath)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ftpPath)) {
                $ftpPath = str_replace('*', '%', $ftpPath);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_FTPPATH, $ftpPath, $comparison);
    }

    /**
     * Filter the query on the displayOrder column
     *
     * Example usage:
     * <code>
     * $query->filterByDisplayOrder(1234); // WHERE displayOrder = 1234
     * $query->filterByDisplayOrder(array(12, 34)); // WHERE displayOrder IN (12, 34)
     * $query->filterByDisplayOrder(array('min' => 12)); // WHERE displayOrder > 12
     * </code>
     *
     * @param     mixed $displayOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByDisplayOrder($displayOrder = null, $comparison = null)
    {
        if (is_array($displayOrder)) {
            $useMinMax = false;
            if (isset($displayOrder['min'])) {
                $this->addUsingAlias(CommonAreaCameraTableMap::COL_DISPLAYORDER, $displayOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($displayOrder['max'])) {
                $this->addUsingAlias(CommonAreaCameraTableMap::COL_DISPLAYORDER, $displayOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_DISPLAYORDER, $displayOrder, $comparison);
    }

    /**
     * Filter the query on the enable column
     *
     * Example usage:
     * <code>
     * $query->filterByEnable(1234); // WHERE enable = 1234
     * $query->filterByEnable(array(12, 34)); // WHERE enable IN (12, 34)
     * $query->filterByEnable(array('min' => 12)); // WHERE enable > 12
     * </code>
     *
     * @param     mixed $enable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByEnable($enable = null, $comparison = null)
    {
        if (is_array($enable)) {
            $useMinMax = false;
            if (isset($enable['min'])) {
                $this->addUsingAlias(CommonAreaCameraTableMap::COL_ENABLE, $enable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enable['max'])) {
                $this->addUsingAlias(CommonAreaCameraTableMap::COL_ENABLE, $enable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_ENABLE, $enable, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(CommonAreaCameraTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(CommonAreaCameraTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(CommonAreaCameraTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(CommonAreaCameraTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAreaCameraTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCommonAreaCamera $commonAreaCamera Object to remove from the list of results
     *
     * @return $this|ChildCommonAreaCameraQuery The current query, for fluid interface
     */
    public function prune($commonAreaCamera = null)
    {
        if ($commonAreaCamera) {
            $this->addUsingAlias(CommonAreaCameraTableMap::COL_ID, $commonAreaCamera->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the commonAreaCamera table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommonAreaCameraTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonAreaCameraTableMap::clearInstancePool();
            CommonAreaCameraTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommonAreaCameraTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CommonAreaCameraTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CommonAreaCameraTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CommonAreaCameraTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CommonAreaCameraQuery
