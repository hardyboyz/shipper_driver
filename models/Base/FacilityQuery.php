<?php

namespace Base;

use \Facility as ChildFacility;
use \FacilityQuery as ChildFacilityQuery;
use \Exception;
use \PDO;
use Map\FacilityTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'facility' table.
 *
 *
 *
 * @method     ChildFacilityQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFacilityQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildFacilityQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildFacilityQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildFacilityQuery orderByRequireBooking($order = Criteria::ASC) Order by the requirebooking column
 * @method     ChildFacilityQuery orderByAdvancedBooking($order = Criteria::ASC) Order by the advancedbooking column
 * @method     ChildFacilityQuery orderByMaxHour($order = Criteria::ASC) Order by the maxhour column
 * @method     ChildFacilityQuery orderByCoolingOff($order = Criteria::ASC) Order by the coolingoff column
 * @method     ChildFacilityQuery orderByStartTime($order = Criteria::ASC) Order by the starttime column
 * @method     ChildFacilityQuery orderByEndTime($order = Criteria::ASC) Order by the endtime column
 * @method     ChildFacilityQuery orderByDeposit($order = Criteria::ASC) Order by the deposit column
 * @method     ChildFacilityQuery orderByCharge($order = Criteria::ASC) Order by the charge column
 * @method     ChildFacilityQuery orderByRules($order = Criteria::ASC) Order by the rules column
 * @method     ChildFacilityQuery orderByEnabled($order = Criteria::ASC) Order by the enabled column
 * @method     ChildFacilityQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildFacilityQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildFacilityQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildFacilityQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildFacilityQuery groupById() Group by the id column
 * @method     ChildFacilityQuery groupByName() Group by the name column
 * @method     ChildFacilityQuery groupByProjectId() Group by the project_id column
 * @method     ChildFacilityQuery groupByType() Group by the type column
 * @method     ChildFacilityQuery groupByRequireBooking() Group by the requirebooking column
 * @method     ChildFacilityQuery groupByAdvancedBooking() Group by the advancedbooking column
 * @method     ChildFacilityQuery groupByMaxHour() Group by the maxhour column
 * @method     ChildFacilityQuery groupByCoolingOff() Group by the coolingoff column
 * @method     ChildFacilityQuery groupByStartTime() Group by the starttime column
 * @method     ChildFacilityQuery groupByEndTime() Group by the endtime column
 * @method     ChildFacilityQuery groupByDeposit() Group by the deposit column
 * @method     ChildFacilityQuery groupByCharge() Group by the charge column
 * @method     ChildFacilityQuery groupByRules() Group by the rules column
 * @method     ChildFacilityQuery groupByEnabled() Group by the enabled column
 * @method     ChildFacilityQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildFacilityQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildFacilityQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildFacilityQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildFacilityQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFacilityQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFacilityQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFacilityQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFacilityQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFacilityQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFacilityQuery leftJoinFacilityTimeslot($relationAlias = null) Adds a LEFT JOIN clause to the query using the FacilityTimeslot relation
 * @method     ChildFacilityQuery rightJoinFacilityTimeslot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FacilityTimeslot relation
 * @method     ChildFacilityQuery innerJoinFacilityTimeslot($relationAlias = null) Adds a INNER JOIN clause to the query using the FacilityTimeslot relation
 *
 * @method     ChildFacilityQuery joinWithFacilityTimeslot($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FacilityTimeslot relation
 *
 * @method     ChildFacilityQuery leftJoinWithFacilityTimeslot() Adds a LEFT JOIN clause and with to the query using the FacilityTimeslot relation
 * @method     ChildFacilityQuery rightJoinWithFacilityTimeslot() Adds a RIGHT JOIN clause and with to the query using the FacilityTimeslot relation
 * @method     ChildFacilityQuery innerJoinWithFacilityTimeslot() Adds a INNER JOIN clause and with to the query using the FacilityTimeslot relation
 *
 * @method     \FacilityTimeslotQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFacility findOne(ConnectionInterface $con = null) Return the first ChildFacility matching the query
 * @method     ChildFacility findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFacility matching the query, or a new ChildFacility object populated from the query conditions when no match is found
 *
 * @method     ChildFacility findOneById(string $id) Return the first ChildFacility filtered by the id column
 * @method     ChildFacility findOneByName(string $name) Return the first ChildFacility filtered by the name column
 * @method     ChildFacility findOneByProjectId(string $project_id) Return the first ChildFacility filtered by the project_id column
 * @method     ChildFacility findOneByType(string $type) Return the first ChildFacility filtered by the type column
 * @method     ChildFacility findOneByRequireBooking(int $requirebooking) Return the first ChildFacility filtered by the requirebooking column
 * @method     ChildFacility findOneByAdvancedBooking(int $advancedbooking) Return the first ChildFacility filtered by the advancedbooking column
 * @method     ChildFacility findOneByMaxHour(int $maxhour) Return the first ChildFacility filtered by the maxhour column
 * @method     ChildFacility findOneByCoolingOff(int $coolingoff) Return the first ChildFacility filtered by the coolingoff column
 * @method     ChildFacility findOneByStartTime(string $starttime) Return the first ChildFacility filtered by the starttime column
 * @method     ChildFacility findOneByEndTime(string $endtime) Return the first ChildFacility filtered by the endtime column
 * @method     ChildFacility findOneByDeposit(double $deposit) Return the first ChildFacility filtered by the deposit column
 * @method     ChildFacility findOneByCharge(double $charge) Return the first ChildFacility filtered by the charge column
 * @method     ChildFacility findOneByRules(string $rules) Return the first ChildFacility filtered by the rules column
 * @method     ChildFacility findOneByEnabled(int $enabled) Return the first ChildFacility filtered by the enabled column
 * @method     ChildFacility findOneByCreatedBy(string $created_by) Return the first ChildFacility filtered by the created_by column
 * @method     ChildFacility findOneByCreatedDate(string $created_date) Return the first ChildFacility filtered by the created_date column
 * @method     ChildFacility findOneByModifiedBy(string $modified_by) Return the first ChildFacility filtered by the modified_by column
 * @method     ChildFacility findOneByModifiedDate(string $modified_date) Return the first ChildFacility filtered by the modified_date column *

 * @method     ChildFacility requirePk($key, ConnectionInterface $con = null) Return the ChildFacility by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOne(ConnectionInterface $con = null) Return the first ChildFacility matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFacility requireOneById(string $id) Return the first ChildFacility filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByName(string $name) Return the first ChildFacility filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByProjectId(string $project_id) Return the first ChildFacility filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByType(string $type) Return the first ChildFacility filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByRequireBooking(int $requirebooking) Return the first ChildFacility filtered by the requirebooking column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByAdvancedBooking(int $advancedbooking) Return the first ChildFacility filtered by the advancedbooking column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByMaxHour(int $maxhour) Return the first ChildFacility filtered by the maxhour column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByCoolingOff(int $coolingoff) Return the first ChildFacility filtered by the coolingoff column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByStartTime(string $starttime) Return the first ChildFacility filtered by the starttime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByEndTime(string $endtime) Return the first ChildFacility filtered by the endtime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByDeposit(double $deposit) Return the first ChildFacility filtered by the deposit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByCharge(double $charge) Return the first ChildFacility filtered by the charge column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByRules(string $rules) Return the first ChildFacility filtered by the rules column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByEnabled(int $enabled) Return the first ChildFacility filtered by the enabled column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByCreatedBy(string $created_by) Return the first ChildFacility filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByCreatedDate(string $created_date) Return the first ChildFacility filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByModifiedBy(string $modified_by) Return the first ChildFacility filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacility requireOneByModifiedDate(string $modified_date) Return the first ChildFacility filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFacility[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFacility objects based on current ModelCriteria
 * @method     ChildFacility[]|ObjectCollection findById(string $id) Return ChildFacility objects filtered by the id column
 * @method     ChildFacility[]|ObjectCollection findByName(string $name) Return ChildFacility objects filtered by the name column
 * @method     ChildFacility[]|ObjectCollection findByProjectId(string $project_id) Return ChildFacility objects filtered by the project_id column
 * @method     ChildFacility[]|ObjectCollection findByType(string $type) Return ChildFacility objects filtered by the type column
 * @method     ChildFacility[]|ObjectCollection findByRequireBooking(int $requirebooking) Return ChildFacility objects filtered by the requirebooking column
 * @method     ChildFacility[]|ObjectCollection findByAdvancedBooking(int $advancedbooking) Return ChildFacility objects filtered by the advancedbooking column
 * @method     ChildFacility[]|ObjectCollection findByMaxHour(int $maxhour) Return ChildFacility objects filtered by the maxhour column
 * @method     ChildFacility[]|ObjectCollection findByCoolingOff(int $coolingoff) Return ChildFacility objects filtered by the coolingoff column
 * @method     ChildFacility[]|ObjectCollection findByStartTime(string $starttime) Return ChildFacility objects filtered by the starttime column
 * @method     ChildFacility[]|ObjectCollection findByEndTime(string $endtime) Return ChildFacility objects filtered by the endtime column
 * @method     ChildFacility[]|ObjectCollection findByDeposit(double $deposit) Return ChildFacility objects filtered by the deposit column
 * @method     ChildFacility[]|ObjectCollection findByCharge(double $charge) Return ChildFacility objects filtered by the charge column
 * @method     ChildFacility[]|ObjectCollection findByRules(string $rules) Return ChildFacility objects filtered by the rules column
 * @method     ChildFacility[]|ObjectCollection findByEnabled(int $enabled) Return ChildFacility objects filtered by the enabled column
 * @method     ChildFacility[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildFacility objects filtered by the created_by column
 * @method     ChildFacility[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildFacility objects filtered by the created_date column
 * @method     ChildFacility[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildFacility objects filtered by the modified_by column
 * @method     ChildFacility[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildFacility objects filtered by the modified_date column
 * @method     ChildFacility[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FacilityQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\FacilityQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Facility', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFacilityQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFacilityQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFacilityQuery) {
            return $criteria;
        }
        $query = new ChildFacilityQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFacility|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FacilityTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FacilityTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFacility A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, project_id, type, requirebooking, advancedbooking, maxhour, coolingoff, starttime, endtime, deposit, charge, rules, enabled, created_by, created_date, modified_by, modified_date FROM facility WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFacility $obj */
            $obj = new ChildFacility();
            $obj->hydrate($row);
            FacilityTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFacility|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FacilityTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FacilityTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the requirebooking column
     *
     * Example usage:
     * <code>
     * $query->filterByRequireBooking(1234); // WHERE requirebooking = 1234
     * $query->filterByRequireBooking(array(12, 34)); // WHERE requirebooking IN (12, 34)
     * $query->filterByRequireBooking(array('min' => 12)); // WHERE requirebooking > 12
     * </code>
     *
     * @param     mixed $requireBooking The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByRequireBooking($requireBooking = null, $comparison = null)
    {
        if (is_array($requireBooking)) {
            $useMinMax = false;
            if (isset($requireBooking['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_REQUIREBOOKING, $requireBooking['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($requireBooking['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_REQUIREBOOKING, $requireBooking['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_REQUIREBOOKING, $requireBooking, $comparison);
    }

    /**
     * Filter the query on the advancedbooking column
     *
     * Example usage:
     * <code>
     * $query->filterByAdvancedBooking(1234); // WHERE advancedbooking = 1234
     * $query->filterByAdvancedBooking(array(12, 34)); // WHERE advancedbooking IN (12, 34)
     * $query->filterByAdvancedBooking(array('min' => 12)); // WHERE advancedbooking > 12
     * </code>
     *
     * @param     mixed $advancedBooking The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByAdvancedBooking($advancedBooking = null, $comparison = null)
    {
        if (is_array($advancedBooking)) {
            $useMinMax = false;
            if (isset($advancedBooking['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_ADVANCEDBOOKING, $advancedBooking['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($advancedBooking['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_ADVANCEDBOOKING, $advancedBooking['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_ADVANCEDBOOKING, $advancedBooking, $comparison);
    }

    /**
     * Filter the query on the maxhour column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxHour(1234); // WHERE maxhour = 1234
     * $query->filterByMaxHour(array(12, 34)); // WHERE maxhour IN (12, 34)
     * $query->filterByMaxHour(array('min' => 12)); // WHERE maxhour > 12
     * </code>
     *
     * @param     mixed $maxHour The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByMaxHour($maxHour = null, $comparison = null)
    {
        if (is_array($maxHour)) {
            $useMinMax = false;
            if (isset($maxHour['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_MAXHOUR, $maxHour['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxHour['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_MAXHOUR, $maxHour['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_MAXHOUR, $maxHour, $comparison);
    }

    /**
     * Filter the query on the coolingoff column
     *
     * Example usage:
     * <code>
     * $query->filterByCoolingOff(1234); // WHERE coolingoff = 1234
     * $query->filterByCoolingOff(array(12, 34)); // WHERE coolingoff IN (12, 34)
     * $query->filterByCoolingOff(array('min' => 12)); // WHERE coolingoff > 12
     * </code>
     *
     * @param     mixed $coolingOff The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByCoolingOff($coolingOff = null, $comparison = null)
    {
        if (is_array($coolingOff)) {
            $useMinMax = false;
            if (isset($coolingOff['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_COOLINGOFF, $coolingOff['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($coolingOff['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_COOLINGOFF, $coolingOff['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_COOLINGOFF, $coolingOff, $comparison);
    }

    /**
     * Filter the query on the starttime column
     *
     * Example usage:
     * <code>
     * $query->filterByStartTime('2011-03-14'); // WHERE starttime = '2011-03-14'
     * $query->filterByStartTime('now'); // WHERE starttime = '2011-03-14'
     * $query->filterByStartTime(array('max' => 'yesterday')); // WHERE starttime > '2011-03-13'
     * </code>
     *
     * @param     mixed $startTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByStartTime($startTime = null, $comparison = null)
    {
        if (is_array($startTime)) {
            $useMinMax = false;
            if (isset($startTime['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_STARTTIME, $startTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startTime['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_STARTTIME, $startTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_STARTTIME, $startTime, $comparison);
    }

    /**
     * Filter the query on the endtime column
     *
     * Example usage:
     * <code>
     * $query->filterByEndTime('2011-03-14'); // WHERE endtime = '2011-03-14'
     * $query->filterByEndTime('now'); // WHERE endtime = '2011-03-14'
     * $query->filterByEndTime(array('max' => 'yesterday')); // WHERE endtime > '2011-03-13'
     * </code>
     *
     * @param     mixed $endTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByEndTime($endTime = null, $comparison = null)
    {
        if (is_array($endTime)) {
            $useMinMax = false;
            if (isset($endTime['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_ENDTIME, $endTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endTime['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_ENDTIME, $endTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_ENDTIME, $endTime, $comparison);
    }

    /**
     * Filter the query on the deposit column
     *
     * Example usage:
     * <code>
     * $query->filterByDeposit(1234); // WHERE deposit = 1234
     * $query->filterByDeposit(array(12, 34)); // WHERE deposit IN (12, 34)
     * $query->filterByDeposit(array('min' => 12)); // WHERE deposit > 12
     * </code>
     *
     * @param     mixed $deposit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByDeposit($deposit = null, $comparison = null)
    {
        if (is_array($deposit)) {
            $useMinMax = false;
            if (isset($deposit['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_DEPOSIT, $deposit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deposit['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_DEPOSIT, $deposit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_DEPOSIT, $deposit, $comparison);
    }

    /**
     * Filter the query on the charge column
     *
     * Example usage:
     * <code>
     * $query->filterByCharge(1234); // WHERE charge = 1234
     * $query->filterByCharge(array(12, 34)); // WHERE charge IN (12, 34)
     * $query->filterByCharge(array('min' => 12)); // WHERE charge > 12
     * </code>
     *
     * @param     mixed $charge The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByCharge($charge = null, $comparison = null)
    {
        if (is_array($charge)) {
            $useMinMax = false;
            if (isset($charge['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_CHARGE, $charge['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($charge['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_CHARGE, $charge['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_CHARGE, $charge, $comparison);
    }

    /**
     * Filter the query on the rules column
     *
     * Example usage:
     * <code>
     * $query->filterByRules('fooValue');   // WHERE rules = 'fooValue'
     * $query->filterByRules('%fooValue%'); // WHERE rules LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rules The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByRules($rules = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rules)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $rules)) {
                $rules = str_replace('*', '%', $rules);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_RULES, $rules, $comparison);
    }

    /**
     * Filter the query on the enabled column
     *
     * Example usage:
     * <code>
     * $query->filterByEnabled(1234); // WHERE enabled = 1234
     * $query->filterByEnabled(array(12, 34)); // WHERE enabled IN (12, 34)
     * $query->filterByEnabled(array('min' => 12)); // WHERE enabled > 12
     * </code>
     *
     * @param     mixed $enabled The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByEnabled($enabled = null, $comparison = null)
    {
        if (is_array($enabled)) {
            $useMinMax = false;
            if (isset($enabled['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_ENABLED, $enabled['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enabled['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_ENABLED, $enabled['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_ENABLED, $enabled, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(FacilityTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(FacilityTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query by a related \FacilityTimeslot object
     *
     * @param \FacilityTimeslot|ObjectCollection $facilityTimeslot the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFacilityQuery The current query, for fluid interface
     */
    public function filterByFacilityTimeslot($facilityTimeslot, $comparison = null)
    {
        if ($facilityTimeslot instanceof \FacilityTimeslot) {
            return $this
                ->addUsingAlias(FacilityTableMap::COL_ID, $facilityTimeslot->getFacilityId(), $comparison);
        } elseif ($facilityTimeslot instanceof ObjectCollection) {
            return $this
                ->useFacilityTimeslotQuery()
                ->filterByPrimaryKeys($facilityTimeslot->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFacilityTimeslot() only accepts arguments of type \FacilityTimeslot or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FacilityTimeslot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function joinFacilityTimeslot($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FacilityTimeslot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FacilityTimeslot');
        }

        return $this;
    }

    /**
     * Use the FacilityTimeslot relation FacilityTimeslot object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \FacilityTimeslotQuery A secondary query class using the current class as primary query
     */
    public function useFacilityTimeslotQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFacilityTimeslot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FacilityTimeslot', '\FacilityTimeslotQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFacility $facility Object to remove from the list of results
     *
     * @return $this|ChildFacilityQuery The current query, for fluid interface
     */
    public function prune($facility = null)
    {
        if ($facility) {
            $this->addUsingAlias(FacilityTableMap::COL_ID, $facility->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the facility table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FacilityTableMap::clearInstancePool();
            FacilityTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FacilityTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FacilityTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FacilityTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FacilityQuery
