<?php

namespace Base;

use \AppClientSettingQuery as ChildAppClientSettingQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\AppClientSettingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'appClientSetting' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class AppClientSetting implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\AppClientSettingTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the projectid field.
     *
     * @var        string
     */
    protected $projectid;

    /**
     * The value for the apphomeidletime field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $apphomeidletime;

    /**
     * The value for the appstandbyidletime field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $appstandbyidletime;

    /**
     * The value for the apphomelogoimage field.
     *
     * @var        string
     */
    protected $apphomelogoimage;

    /**
     * The value for the receiptlogoimage field.
     *
     * @var        string
     */
    protected $receiptlogoimage;

    /**
     * The value for the facilitycheckingonlyflag field.
     * 1=Ative, 0=Inactive
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $facilitycheckingonlyflag;

    /**
     * The value for the callingformatpattern field.
     *
     * @var        string
     */
    protected $callingformatpattern;

    /**
     * The value for the interfacilityflag field.
     * 1=Ative, 0=Inactive
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $interfacilityflag;

    /**
     * The value for the projectname field.
     *
     * @var        string
     */
    protected $projectname;

    /**
     * The value for the projectnameid field.
     *
     * @var        string
     */
    protected $projectnameid;

    /**
     * The value for the centralizedserverurl field.
     *
     * @var        string
     */
    protected $centralizedserverurl;

    /**
     * The value for the remoteserverdomain field.
     *
     * @var        string
     */
    protected $remoteserverdomain;

    /**
     * The value for the remoteserverprotocol field.
     *
     * @var        string
     */
    protected $remoteserverprotocol;

    /**
     * The value for the remoteserverport field.
     *
     * @var        string
     */
    protected $remoteserverport;

    /**
     * The value for the localserverdomain field.
     *
     * @var        string
     */
    protected $localserverdomain;

    /**
     * The value for the localserverprotocol field.
     *
     * @var        string
     */
    protected $localserverprotocol;

    /**
     * The value for the localserverport field.
     *
     * @var        string
     */
    protected $localserverport;

    /**
     * The value for the pndomain field.
     *
     * @var        string
     */
    protected $pndomain;

    /**
     * The value for the pnprotocol field.
     *
     * @var        string
     */
    protected $pnprotocol;

    /**
     * The value for the pnport field.
     *
     * @var        string
     */
    protected $pnport;

    /**
     * The value for the remotecctvdomain field.
     *
     * @var        string
     */
    protected $remotecctvdomain;

    /**
     * The value for the remotecctvprotocol field.
     *
     * @var        string
     */
    protected $remotecctvprotocol;

    /**
     * The value for the remotecctvport field.
     *
     * @var        string
     */
    protected $remotecctvport;

    /**
     * The value for the localcctvdomain field.
     *
     * @var        string
     */
    protected $localcctvdomain;

    /**
     * The value for the localcctvprotocol field.
     *
     * @var        string
     */
    protected $localcctvprotocol;

    /**
     * The value for the localcctvport field.
     *
     * @var        string
     */
    protected $localcctvport;

    /**
     * The value for the tutorialimage field.
     *
     * @var        string
     */
    protected $tutorialimage;

    /**
     * The value for the tutorialimageenable field.
     * 1=Ative, 0=Inactive
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $tutorialimageenable;

    /**
     * The value for the appautosyncinterval field.
     *
     * @var        string
     */
    protected $appautosyncinterval;

    /**
     * The value for the modified_by field.
     *
     * @var        string
     */
    protected $modified_by;

    /**
     * The value for the modified_date field.
     *
     * @var        \DateTime
     */
    protected $modified_date;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->apphomeidletime = 0;
        $this->appstandbyidletime = 0;
        $this->facilitycheckingonlyflag = 0;
        $this->interfacilityflag = 0;
        $this->tutorialimageenable = 0;
    }

    /**
     * Initializes internal state of Base\AppClientSetting object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>AppClientSetting</code> instance.  If
     * <code>obj</code> is an instance of <code>AppClientSetting</code>, delegates to
     * <code>equals(AppClientSetting)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|AppClientSetting The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [projectid] column value.
     *
     * @return string
     */
    public function getProjectId()
    {
        return $this->projectid;
    }

    /**
     * Get the [apphomeidletime] column value.
     *
     * @return int
     */
    public function getAppHomeIdleTime()
    {
        return $this->apphomeidletime;
    }

    /**
     * Get the [appstandbyidletime] column value.
     *
     * @return int
     */
    public function getAppStandbyIdleTime()
    {
        return $this->appstandbyidletime;
    }

    /**
     * Get the [apphomelogoimage] column value.
     *
     * @return string
     */
    public function getAppHomeLogoImage()
    {
        return $this->apphomelogoimage;
    }

    /**
     * Get the [receiptlogoimage] column value.
     *
     * @return string
     */
    public function getReceiptLogoImage()
    {
        return $this->receiptlogoimage;
    }

    /**
     * Get the [facilitycheckingonlyflag] column value.
     * 1=Ative, 0=Inactive
     * @return int
     */
    public function getFacilityCheckingOnlyFlag()
    {
        return $this->facilitycheckingonlyflag;
    }

    /**
     * Get the [callingformatpattern] column value.
     *
     * @return string
     */
    public function getCallingFormatPattern()
    {
        return $this->callingformatpattern;
    }

    /**
     * Get the [interfacilityflag] column value.
     * 1=Ative, 0=Inactive
     * @return int
     */
    public function getinterFacilityFlag()
    {
        return $this->interfacilityflag;
    }

    /**
     * Get the [projectname] column value.
     *
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectname;
    }

    /**
     * Get the [projectnameid] column value.
     *
     * @return string
     */
    public function getProjectNameId()
    {
        return $this->projectnameid;
    }

    /**
     * Get the [centralizedserverurl] column value.
     *
     * @return string
     */
    public function getCentralizedServerUrl()
    {
        return $this->centralizedserverurl;
    }

    /**
     * Get the [remoteserverdomain] column value.
     *
     * @return string
     */
    public function getRemoteServerDomain()
    {
        return $this->remoteserverdomain;
    }

    /**
     * Get the [remoteserverprotocol] column value.
     *
     * @return string
     */
    public function getRemoteServerProtocol()
    {
        return $this->remoteserverprotocol;
    }

    /**
     * Get the [remoteserverport] column value.
     *
     * @return string
     */
    public function getRemoteServerPort()
    {
        return $this->remoteserverport;
    }

    /**
     * Get the [localserverdomain] column value.
     *
     * @return string
     */
    public function getLocalServerDomain()
    {
        return $this->localserverdomain;
    }

    /**
     * Get the [localserverprotocol] column value.
     *
     * @return string
     */
    public function getLocalServerProtocol()
    {
        return $this->localserverprotocol;
    }

    /**
     * Get the [localserverport] column value.
     *
     * @return string
     */
    public function getLocalServerPort()
    {
        return $this->localserverport;
    }

    /**
     * Get the [pndomain] column value.
     *
     * @return string
     */
    public function getPNDomain()
    {
        return $this->pndomain;
    }

    /**
     * Get the [pnprotocol] column value.
     *
     * @return string
     */
    public function getPNProtocol()
    {
        return $this->pnprotocol;
    }

    /**
     * Get the [pnport] column value.
     *
     * @return string
     */
    public function getPNPort()
    {
        return $this->pnport;
    }

    /**
     * Get the [remotecctvdomain] column value.
     *
     * @return string
     */
    public function getRemoteCctvDomain()
    {
        return $this->remotecctvdomain;
    }

    /**
     * Get the [remotecctvprotocol] column value.
     *
     * @return string
     */
    public function getRemoteCctvProtocol()
    {
        return $this->remotecctvprotocol;
    }

    /**
     * Get the [remotecctvport] column value.
     *
     * @return string
     */
    public function getRemoteCctvPort()
    {
        return $this->remotecctvport;
    }

    /**
     * Get the [localcctvdomain] column value.
     *
     * @return string
     */
    public function getLocalCctvDomain()
    {
        return $this->localcctvdomain;
    }

    /**
     * Get the [localcctvprotocol] column value.
     *
     * @return string
     */
    public function getLocalCctvProtocol()
    {
        return $this->localcctvprotocol;
    }

    /**
     * Get the [localcctvport] column value.
     *
     * @return string
     */
    public function getLocalCctvPort()
    {
        return $this->localcctvport;
    }

    /**
     * Get the [tutorialimage] column value.
     *
     * @return string
     */
    public function getTutorialImage()
    {
        return $this->tutorialimage;
    }

    /**
     * Get the [tutorialimageenable] column value.
     * 1=Ative, 0=Inactive
     * @return int
     */
    public function getTutorialImageEnable()
    {
        return $this->tutorialimageenable;
    }

    /**
     * Get the [appautosyncinterval] column value.
     *
     * @return string
     */
    public function getAppAutoSyncInterval()
    {
        return $this->appautosyncinterval;
    }

    /**
     * Get the [modified_by] column value.
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Get the [optionally formatted] temporal [modified_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModifiedDate($format = NULL)
    {
        if ($format === null) {
            return $this->modified_date;
        } else {
            return $this->modified_date instanceof \DateTime ? $this->modified_date->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [projectid] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setProjectId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->projectid !== $v) {
            $this->projectid = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_PROJECTID] = true;
        }

        return $this;
    } // setProjectId()

    /**
     * Set the value of [apphomeidletime] column.
     *
     * @param int $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setAppHomeIdleTime($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->apphomeidletime !== $v) {
            $this->apphomeidletime = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_APPHOMEIDLETIME] = true;
        }

        return $this;
    } // setAppHomeIdleTime()

    /**
     * Set the value of [appstandbyidletime] column.
     *
     * @param int $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setAppStandbyIdleTime($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->appstandbyidletime !== $v) {
            $this->appstandbyidletime = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_APPSTANDBYIDLETIME] = true;
        }

        return $this;
    } // setAppStandbyIdleTime()

    /**
     * Set the value of [apphomelogoimage] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setAppHomeLogoImage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->apphomelogoimage !== $v) {
            $this->apphomelogoimage = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_APPHOMELOGOIMAGE] = true;
        }

        return $this;
    } // setAppHomeLogoImage()

    /**
     * Set the value of [receiptlogoimage] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setReceiptLogoImage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->receiptlogoimage !== $v) {
            $this->receiptlogoimage = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_RECEIPTLOGOIMAGE] = true;
        }

        return $this;
    } // setReceiptLogoImage()

    /**
     * Set the value of [facilitycheckingonlyflag] column.
     * 1=Ative, 0=Inactive
     * @param int $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setFacilityCheckingOnlyFlag($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->facilitycheckingonlyflag !== $v) {
            $this->facilitycheckingonlyflag = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG] = true;
        }

        return $this;
    } // setFacilityCheckingOnlyFlag()

    /**
     * Set the value of [callingformatpattern] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setCallingFormatPattern($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->callingformatpattern !== $v) {
            $this->callingformatpattern = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_CALLINGFORMATPATTERN] = true;
        }

        return $this;
    } // setCallingFormatPattern()

    /**
     * Set the value of [interfacilityflag] column.
     * 1=Ative, 0=Inactive
     * @param int $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setinterFacilityFlag($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->interfacilityflag !== $v) {
            $this->interfacilityflag = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_INTERFACILITYFLAG] = true;
        }

        return $this;
    } // setinterFacilityFlag()

    /**
     * Set the value of [projectname] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setProjectName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->projectname !== $v) {
            $this->projectname = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_PROJECTNAME] = true;
        }

        return $this;
    } // setProjectName()

    /**
     * Set the value of [projectnameid] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setProjectNameId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->projectnameid !== $v) {
            $this->projectnameid = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_PROJECTNAMEID] = true;
        }

        return $this;
    } // setProjectNameId()

    /**
     * Set the value of [centralizedserverurl] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setCentralizedServerUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->centralizedserverurl !== $v) {
            $this->centralizedserverurl = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_CENTRALIZEDSERVERURL] = true;
        }

        return $this;
    } // setCentralizedServerUrl()

    /**
     * Set the value of [remoteserverdomain] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setRemoteServerDomain($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remoteserverdomain !== $v) {
            $this->remoteserverdomain = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_REMOTESERVERDOMAIN] = true;
        }

        return $this;
    } // setRemoteServerDomain()

    /**
     * Set the value of [remoteserverprotocol] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setRemoteServerProtocol($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remoteserverprotocol !== $v) {
            $this->remoteserverprotocol = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_REMOTESERVERPROTOCOL] = true;
        }

        return $this;
    } // setRemoteServerProtocol()

    /**
     * Set the value of [remoteserverport] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setRemoteServerPort($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remoteserverport !== $v) {
            $this->remoteserverport = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_REMOTESERVERPORT] = true;
        }

        return $this;
    } // setRemoteServerPort()

    /**
     * Set the value of [localserverdomain] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setLocalServerDomain($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->localserverdomain !== $v) {
            $this->localserverdomain = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_LOCALSERVERDOMAIN] = true;
        }

        return $this;
    } // setLocalServerDomain()

    /**
     * Set the value of [localserverprotocol] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setLocalServerProtocol($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->localserverprotocol !== $v) {
            $this->localserverprotocol = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_LOCALSERVERPROTOCOL] = true;
        }

        return $this;
    } // setLocalServerProtocol()

    /**
     * Set the value of [localserverport] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setLocalServerPort($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->localserverport !== $v) {
            $this->localserverport = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_LOCALSERVERPORT] = true;
        }

        return $this;
    } // setLocalServerPort()

    /**
     * Set the value of [pndomain] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setPNDomain($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pndomain !== $v) {
            $this->pndomain = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_PNDOMAIN] = true;
        }

        return $this;
    } // setPNDomain()

    /**
     * Set the value of [pnprotocol] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setPNProtocol($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pnprotocol !== $v) {
            $this->pnprotocol = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_PNPROTOCOL] = true;
        }

        return $this;
    } // setPNProtocol()

    /**
     * Set the value of [pnport] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setPNPort($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pnport !== $v) {
            $this->pnport = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_PNPORT] = true;
        }

        return $this;
    } // setPNPort()

    /**
     * Set the value of [remotecctvdomain] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setRemoteCctvDomain($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remotecctvdomain !== $v) {
            $this->remotecctvdomain = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_REMOTECCTVDOMAIN] = true;
        }

        return $this;
    } // setRemoteCctvDomain()

    /**
     * Set the value of [remotecctvprotocol] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setRemoteCctvProtocol($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remotecctvprotocol !== $v) {
            $this->remotecctvprotocol = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_REMOTECCTVPROTOCOL] = true;
        }

        return $this;
    } // setRemoteCctvProtocol()

    /**
     * Set the value of [remotecctvport] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setRemoteCctvPort($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remotecctvport !== $v) {
            $this->remotecctvport = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_REMOTECCTVPORT] = true;
        }

        return $this;
    } // setRemoteCctvPort()

    /**
     * Set the value of [localcctvdomain] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setLocalCctvDomain($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->localcctvdomain !== $v) {
            $this->localcctvdomain = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_LOCALCCTVDOMAIN] = true;
        }

        return $this;
    } // setLocalCctvDomain()

    /**
     * Set the value of [localcctvprotocol] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setLocalCctvProtocol($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->localcctvprotocol !== $v) {
            $this->localcctvprotocol = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_LOCALCCTVPROTOCOL] = true;
        }

        return $this;
    } // setLocalCctvProtocol()

    /**
     * Set the value of [localcctvport] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setLocalCctvPort($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->localcctvport !== $v) {
            $this->localcctvport = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_LOCALCCTVPORT] = true;
        }

        return $this;
    } // setLocalCctvPort()

    /**
     * Set the value of [tutorialimage] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setTutorialImage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tutorialimage !== $v) {
            $this->tutorialimage = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_TUTORIALIMAGE] = true;
        }

        return $this;
    } // setTutorialImage()

    /**
     * Set the value of [tutorialimageenable] column.
     * 1=Ative, 0=Inactive
     * @param int $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setTutorialImageEnable($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->tutorialimageenable !== $v) {
            $this->tutorialimageenable = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE] = true;
        }

        return $this;
    } // setTutorialImageEnable()

    /**
     * Set the value of [appautosyncinterval] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setAppAutoSyncInterval($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->appautosyncinterval !== $v) {
            $this->appautosyncinterval = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_APPAUTOSYNCINTERVAL] = true;
        }

        return $this;
    } // setAppAutoSyncInterval()

    /**
     * Set the value of [modified_by] column.
     *
     * @param string $v new value
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setModifiedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modified_by !== $v) {
            $this->modified_by = $v;
            $this->modifiedColumns[AppClientSettingTableMap::COL_MODIFIED_BY] = true;
        }

        return $this;
    } // setModifiedBy()

    /**
     * Sets the value of [modified_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\AppClientSetting The current object (for fluent API support)
     */
    public function setModifiedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified_date !== null || $dt !== null) {
            if ($this->modified_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->modified_date->format("Y-m-d H:i:s")) {
                $this->modified_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AppClientSettingTableMap::COL_MODIFIED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setModifiedDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->apphomeidletime !== 0) {
                return false;
            }

            if ($this->appstandbyidletime !== 0) {
                return false;
            }

            if ($this->facilitycheckingonlyflag !== 0) {
                return false;
            }

            if ($this->interfacilityflag !== 0) {
                return false;
            }

            if ($this->tutorialimageenable !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : AppClientSettingTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : AppClientSettingTableMap::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->projectid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : AppClientSettingTableMap::translateFieldName('AppHomeIdleTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->apphomeidletime = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : AppClientSettingTableMap::translateFieldName('AppStandbyIdleTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->appstandbyidletime = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : AppClientSettingTableMap::translateFieldName('AppHomeLogoImage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->apphomelogoimage = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : AppClientSettingTableMap::translateFieldName('ReceiptLogoImage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->receiptlogoimage = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : AppClientSettingTableMap::translateFieldName('FacilityCheckingOnlyFlag', TableMap::TYPE_PHPNAME, $indexType)];
            $this->facilitycheckingonlyflag = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : AppClientSettingTableMap::translateFieldName('CallingFormatPattern', TableMap::TYPE_PHPNAME, $indexType)];
            $this->callingformatpattern = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : AppClientSettingTableMap::translateFieldName('interFacilityFlag', TableMap::TYPE_PHPNAME, $indexType)];
            $this->interfacilityflag = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : AppClientSettingTableMap::translateFieldName('ProjectName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->projectname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : AppClientSettingTableMap::translateFieldName('ProjectNameId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->projectnameid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : AppClientSettingTableMap::translateFieldName('CentralizedServerUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->centralizedserverurl = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : AppClientSettingTableMap::translateFieldName('RemoteServerDomain', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remoteserverdomain = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : AppClientSettingTableMap::translateFieldName('RemoteServerProtocol', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remoteserverprotocol = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : AppClientSettingTableMap::translateFieldName('RemoteServerPort', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remoteserverport = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : AppClientSettingTableMap::translateFieldName('LocalServerDomain', TableMap::TYPE_PHPNAME, $indexType)];
            $this->localserverdomain = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : AppClientSettingTableMap::translateFieldName('LocalServerProtocol', TableMap::TYPE_PHPNAME, $indexType)];
            $this->localserverprotocol = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : AppClientSettingTableMap::translateFieldName('LocalServerPort', TableMap::TYPE_PHPNAME, $indexType)];
            $this->localserverport = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : AppClientSettingTableMap::translateFieldName('PNDomain', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pndomain = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : AppClientSettingTableMap::translateFieldName('PNProtocol', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pnprotocol = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : AppClientSettingTableMap::translateFieldName('PNPort', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pnport = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : AppClientSettingTableMap::translateFieldName('RemoteCctvDomain', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remotecctvdomain = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : AppClientSettingTableMap::translateFieldName('RemoteCctvProtocol', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remotecctvprotocol = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : AppClientSettingTableMap::translateFieldName('RemoteCctvPort', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remotecctvport = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : AppClientSettingTableMap::translateFieldName('LocalCctvDomain', TableMap::TYPE_PHPNAME, $indexType)];
            $this->localcctvdomain = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : AppClientSettingTableMap::translateFieldName('LocalCctvProtocol', TableMap::TYPE_PHPNAME, $indexType)];
            $this->localcctvprotocol = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : AppClientSettingTableMap::translateFieldName('LocalCctvPort', TableMap::TYPE_PHPNAME, $indexType)];
            $this->localcctvport = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : AppClientSettingTableMap::translateFieldName('TutorialImage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tutorialimage = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : AppClientSettingTableMap::translateFieldName('TutorialImageEnable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tutorialimageenable = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : AppClientSettingTableMap::translateFieldName('AppAutoSyncInterval', TableMap::TYPE_PHPNAME, $indexType)];
            $this->appautosyncinterval = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : AppClientSettingTableMap::translateFieldName('ModifiedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modified_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : AppClientSettingTableMap::translateFieldName('ModifiedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->modified_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 32; // 32 = AppClientSettingTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\AppClientSetting'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AppClientSettingTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildAppClientSettingQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see AppClientSetting::setDeleted()
     * @see AppClientSetting::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppClientSettingTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildAppClientSettingQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppClientSettingTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AppClientSettingTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AppClientSettingTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PROJECTID)) {
            $modifiedColumns[':p' . $index++]  = 'projectId';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_APPHOMEIDLETIME)) {
            $modifiedColumns[':p' . $index++]  = 'appHomeIdleTime';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_APPSTANDBYIDLETIME)) {
            $modifiedColumns[':p' . $index++]  = 'appStandbyIdleTime';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_APPHOMELOGOIMAGE)) {
            $modifiedColumns[':p' . $index++]  = 'appHomeLogoImage';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_RECEIPTLOGOIMAGE)) {
            $modifiedColumns[':p' . $index++]  = 'receiptLogoImage';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG)) {
            $modifiedColumns[':p' . $index++]  = 'facilityCheckingOnlyFlag';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_CALLINGFORMATPATTERN)) {
            $modifiedColumns[':p' . $index++]  = 'callingFormatPattern';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_INTERFACILITYFLAG)) {
            $modifiedColumns[':p' . $index++]  = 'interFacilityFlag';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PROJECTNAME)) {
            $modifiedColumns[':p' . $index++]  = 'projectName';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PROJECTNAMEID)) {
            $modifiedColumns[':p' . $index++]  = 'projectNameId';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_CENTRALIZEDSERVERURL)) {
            $modifiedColumns[':p' . $index++]  = 'centralizedServerUrl';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTESERVERDOMAIN)) {
            $modifiedColumns[':p' . $index++]  = 'remoteServerDomain';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTESERVERPROTOCOL)) {
            $modifiedColumns[':p' . $index++]  = 'remoteServerProtocol';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTESERVERPORT)) {
            $modifiedColumns[':p' . $index++]  = 'remoteServerPort';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALSERVERDOMAIN)) {
            $modifiedColumns[':p' . $index++]  = 'localServerDomain';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALSERVERPROTOCOL)) {
            $modifiedColumns[':p' . $index++]  = 'localServerProtocol';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALSERVERPORT)) {
            $modifiedColumns[':p' . $index++]  = 'localServerPort';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PNDOMAIN)) {
            $modifiedColumns[':p' . $index++]  = 'pnDomain';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PNPROTOCOL)) {
            $modifiedColumns[':p' . $index++]  = 'pnProtocol';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PNPORT)) {
            $modifiedColumns[':p' . $index++]  = 'pnPort';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTECCTVDOMAIN)) {
            $modifiedColumns[':p' . $index++]  = 'remoteCctvDomain';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTECCTVPROTOCOL)) {
            $modifiedColumns[':p' . $index++]  = 'remoteCctvProtocol';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTECCTVPORT)) {
            $modifiedColumns[':p' . $index++]  = 'remoteCctvPort';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALCCTVDOMAIN)) {
            $modifiedColumns[':p' . $index++]  = 'localCctvDomain';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALCCTVPROTOCOL)) {
            $modifiedColumns[':p' . $index++]  = 'localCctvProtocol';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALCCTVPORT)) {
            $modifiedColumns[':p' . $index++]  = 'localCctvPort';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_TUTORIALIMAGE)) {
            $modifiedColumns[':p' . $index++]  = 'tutorialImage';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'tutorialImageEnable';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_APPAUTOSYNCINTERVAL)) {
            $modifiedColumns[':p' . $index++]  = 'appAutoSyncInterval';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_MODIFIED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'modified_by';
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_MODIFIED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'modified_date';
        }

        $sql = sprintf(
            'INSERT INTO appClientSetting (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                    case 'projectId':
                        $stmt->bindValue($identifier, $this->projectid, PDO::PARAM_STR);
                        break;
                    case 'appHomeIdleTime':
                        $stmt->bindValue($identifier, $this->apphomeidletime, PDO::PARAM_INT);
                        break;
                    case 'appStandbyIdleTime':
                        $stmt->bindValue($identifier, $this->appstandbyidletime, PDO::PARAM_INT);
                        break;
                    case 'appHomeLogoImage':
                        $stmt->bindValue($identifier, $this->apphomelogoimage, PDO::PARAM_STR);
                        break;
                    case 'receiptLogoImage':
                        $stmt->bindValue($identifier, $this->receiptlogoimage, PDO::PARAM_STR);
                        break;
                    case 'facilityCheckingOnlyFlag':
                        $stmt->bindValue($identifier, $this->facilitycheckingonlyflag, PDO::PARAM_INT);
                        break;
                    case 'callingFormatPattern':
                        $stmt->bindValue($identifier, $this->callingformatpattern, PDO::PARAM_STR);
                        break;
                    case 'interFacilityFlag':
                        $stmt->bindValue($identifier, $this->interfacilityflag, PDO::PARAM_INT);
                        break;
                    case 'projectName':
                        $stmt->bindValue($identifier, $this->projectname, PDO::PARAM_STR);
                        break;
                    case 'projectNameId':
                        $stmt->bindValue($identifier, $this->projectnameid, PDO::PARAM_STR);
                        break;
                    case 'centralizedServerUrl':
                        $stmt->bindValue($identifier, $this->centralizedserverurl, PDO::PARAM_STR);
                        break;
                    case 'remoteServerDomain':
                        $stmt->bindValue($identifier, $this->remoteserverdomain, PDO::PARAM_STR);
                        break;
                    case 'remoteServerProtocol':
                        $stmt->bindValue($identifier, $this->remoteserverprotocol, PDO::PARAM_STR);
                        break;
                    case 'remoteServerPort':
                        $stmt->bindValue($identifier, $this->remoteserverport, PDO::PARAM_STR);
                        break;
                    case 'localServerDomain':
                        $stmt->bindValue($identifier, $this->localserverdomain, PDO::PARAM_STR);
                        break;
                    case 'localServerProtocol':
                        $stmt->bindValue($identifier, $this->localserverprotocol, PDO::PARAM_STR);
                        break;
                    case 'localServerPort':
                        $stmt->bindValue($identifier, $this->localserverport, PDO::PARAM_STR);
                        break;
                    case 'pnDomain':
                        $stmt->bindValue($identifier, $this->pndomain, PDO::PARAM_STR);
                        break;
                    case 'pnProtocol':
                        $stmt->bindValue($identifier, $this->pnprotocol, PDO::PARAM_STR);
                        break;
                    case 'pnPort':
                        $stmt->bindValue($identifier, $this->pnport, PDO::PARAM_STR);
                        break;
                    case 'remoteCctvDomain':
                        $stmt->bindValue($identifier, $this->remotecctvdomain, PDO::PARAM_STR);
                        break;
                    case 'remoteCctvProtocol':
                        $stmt->bindValue($identifier, $this->remotecctvprotocol, PDO::PARAM_STR);
                        break;
                    case 'remoteCctvPort':
                        $stmt->bindValue($identifier, $this->remotecctvport, PDO::PARAM_STR);
                        break;
                    case 'localCctvDomain':
                        $stmt->bindValue($identifier, $this->localcctvdomain, PDO::PARAM_STR);
                        break;
                    case 'localCctvProtocol':
                        $stmt->bindValue($identifier, $this->localcctvprotocol, PDO::PARAM_STR);
                        break;
                    case 'localCctvPort':
                        $stmt->bindValue($identifier, $this->localcctvport, PDO::PARAM_STR);
                        break;
                    case 'tutorialImage':
                        $stmt->bindValue($identifier, $this->tutorialimage, PDO::PARAM_STR);
                        break;
                    case 'tutorialImageEnable':
                        $stmt->bindValue($identifier, $this->tutorialimageenable, PDO::PARAM_INT);
                        break;
                    case 'appAutoSyncInterval':
                        $stmt->bindValue($identifier, $this->appautosyncinterval, PDO::PARAM_STR);
                        break;
                    case 'modified_by':
                        $stmt->bindValue($identifier, $this->modified_by, PDO::PARAM_STR);
                        break;
                    case 'modified_date':
                        $stmt->bindValue($identifier, $this->modified_date ? $this->modified_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = AppClientSettingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getProjectId();
                break;
            case 2:
                return $this->getAppHomeIdleTime();
                break;
            case 3:
                return $this->getAppStandbyIdleTime();
                break;
            case 4:
                return $this->getAppHomeLogoImage();
                break;
            case 5:
                return $this->getReceiptLogoImage();
                break;
            case 6:
                return $this->getFacilityCheckingOnlyFlag();
                break;
            case 7:
                return $this->getCallingFormatPattern();
                break;
            case 8:
                return $this->getinterFacilityFlag();
                break;
            case 9:
                return $this->getProjectName();
                break;
            case 10:
                return $this->getProjectNameId();
                break;
            case 11:
                return $this->getCentralizedServerUrl();
                break;
            case 12:
                return $this->getRemoteServerDomain();
                break;
            case 13:
                return $this->getRemoteServerProtocol();
                break;
            case 14:
                return $this->getRemoteServerPort();
                break;
            case 15:
                return $this->getLocalServerDomain();
                break;
            case 16:
                return $this->getLocalServerProtocol();
                break;
            case 17:
                return $this->getLocalServerPort();
                break;
            case 18:
                return $this->getPNDomain();
                break;
            case 19:
                return $this->getPNProtocol();
                break;
            case 20:
                return $this->getPNPort();
                break;
            case 21:
                return $this->getRemoteCctvDomain();
                break;
            case 22:
                return $this->getRemoteCctvProtocol();
                break;
            case 23:
                return $this->getRemoteCctvPort();
                break;
            case 24:
                return $this->getLocalCctvDomain();
                break;
            case 25:
                return $this->getLocalCctvProtocol();
                break;
            case 26:
                return $this->getLocalCctvPort();
                break;
            case 27:
                return $this->getTutorialImage();
                break;
            case 28:
                return $this->getTutorialImageEnable();
                break;
            case 29:
                return $this->getAppAutoSyncInterval();
                break;
            case 30:
                return $this->getModifiedBy();
                break;
            case 31:
                return $this->getModifiedDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['AppClientSetting'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['AppClientSetting'][$this->hashCode()] = true;
        $keys = AppClientSettingTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getProjectId(),
            $keys[2] => $this->getAppHomeIdleTime(),
            $keys[3] => $this->getAppStandbyIdleTime(),
            $keys[4] => $this->getAppHomeLogoImage(),
            $keys[5] => $this->getReceiptLogoImage(),
            $keys[6] => $this->getFacilityCheckingOnlyFlag(),
            $keys[7] => $this->getCallingFormatPattern(),
            $keys[8] => $this->getinterFacilityFlag(),
            $keys[9] => $this->getProjectName(),
            $keys[10] => $this->getProjectNameId(),
            $keys[11] => $this->getCentralizedServerUrl(),
            $keys[12] => $this->getRemoteServerDomain(),
            $keys[13] => $this->getRemoteServerProtocol(),
            $keys[14] => $this->getRemoteServerPort(),
            $keys[15] => $this->getLocalServerDomain(),
            $keys[16] => $this->getLocalServerProtocol(),
            $keys[17] => $this->getLocalServerPort(),
            $keys[18] => $this->getPNDomain(),
            $keys[19] => $this->getPNProtocol(),
            $keys[20] => $this->getPNPort(),
            $keys[21] => $this->getRemoteCctvDomain(),
            $keys[22] => $this->getRemoteCctvProtocol(),
            $keys[23] => $this->getRemoteCctvPort(),
            $keys[24] => $this->getLocalCctvDomain(),
            $keys[25] => $this->getLocalCctvProtocol(),
            $keys[26] => $this->getLocalCctvPort(),
            $keys[27] => $this->getTutorialImage(),
            $keys[28] => $this->getTutorialImageEnable(),
            $keys[29] => $this->getAppAutoSyncInterval(),
            $keys[30] => $this->getModifiedBy(),
            $keys[31] => $this->getModifiedDate(),
        );
        if ($result[$keys[31]] instanceof \DateTime) {
            $result[$keys[31]] = $result[$keys[31]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\AppClientSetting
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = AppClientSettingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\AppClientSetting
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setProjectId($value);
                break;
            case 2:
                $this->setAppHomeIdleTime($value);
                break;
            case 3:
                $this->setAppStandbyIdleTime($value);
                break;
            case 4:
                $this->setAppHomeLogoImage($value);
                break;
            case 5:
                $this->setReceiptLogoImage($value);
                break;
            case 6:
                $this->setFacilityCheckingOnlyFlag($value);
                break;
            case 7:
                $this->setCallingFormatPattern($value);
                break;
            case 8:
                $this->setinterFacilityFlag($value);
                break;
            case 9:
                $this->setProjectName($value);
                break;
            case 10:
                $this->setProjectNameId($value);
                break;
            case 11:
                $this->setCentralizedServerUrl($value);
                break;
            case 12:
                $this->setRemoteServerDomain($value);
                break;
            case 13:
                $this->setRemoteServerProtocol($value);
                break;
            case 14:
                $this->setRemoteServerPort($value);
                break;
            case 15:
                $this->setLocalServerDomain($value);
                break;
            case 16:
                $this->setLocalServerProtocol($value);
                break;
            case 17:
                $this->setLocalServerPort($value);
                break;
            case 18:
                $this->setPNDomain($value);
                break;
            case 19:
                $this->setPNProtocol($value);
                break;
            case 20:
                $this->setPNPort($value);
                break;
            case 21:
                $this->setRemoteCctvDomain($value);
                break;
            case 22:
                $this->setRemoteCctvProtocol($value);
                break;
            case 23:
                $this->setRemoteCctvPort($value);
                break;
            case 24:
                $this->setLocalCctvDomain($value);
                break;
            case 25:
                $this->setLocalCctvProtocol($value);
                break;
            case 26:
                $this->setLocalCctvPort($value);
                break;
            case 27:
                $this->setTutorialImage($value);
                break;
            case 28:
                $this->setTutorialImageEnable($value);
                break;
            case 29:
                $this->setAppAutoSyncInterval($value);
                break;
            case 30:
                $this->setModifiedBy($value);
                break;
            case 31:
                $this->setModifiedDate($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = AppClientSettingTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setProjectId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAppHomeIdleTime($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAppStandbyIdleTime($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setAppHomeLogoImage($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setReceiptLogoImage($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setFacilityCheckingOnlyFlag($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCallingFormatPattern($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setinterFacilityFlag($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setProjectName($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setProjectNameId($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setCentralizedServerUrl($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setRemoteServerDomain($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setRemoteServerProtocol($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setRemoteServerPort($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setLocalServerDomain($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setLocalServerProtocol($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setLocalServerPort($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setPNDomain($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setPNProtocol($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setPNPort($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setRemoteCctvDomain($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setRemoteCctvProtocol($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setRemoteCctvPort($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setLocalCctvDomain($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setLocalCctvProtocol($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setLocalCctvPort($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setTutorialImage($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setTutorialImageEnable($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setAppAutoSyncInterval($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setModifiedBy($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setModifiedDate($arr[$keys[31]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\AppClientSetting The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AppClientSettingTableMap::DATABASE_NAME);

        if ($this->isColumnModified(AppClientSettingTableMap::COL_ID)) {
            $criteria->add(AppClientSettingTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PROJECTID)) {
            $criteria->add(AppClientSettingTableMap::COL_PROJECTID, $this->projectid);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_APPHOMEIDLETIME)) {
            $criteria->add(AppClientSettingTableMap::COL_APPHOMEIDLETIME, $this->apphomeidletime);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_APPSTANDBYIDLETIME)) {
            $criteria->add(AppClientSettingTableMap::COL_APPSTANDBYIDLETIME, $this->appstandbyidletime);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_APPHOMELOGOIMAGE)) {
            $criteria->add(AppClientSettingTableMap::COL_APPHOMELOGOIMAGE, $this->apphomelogoimage);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_RECEIPTLOGOIMAGE)) {
            $criteria->add(AppClientSettingTableMap::COL_RECEIPTLOGOIMAGE, $this->receiptlogoimage);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG)) {
            $criteria->add(AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG, $this->facilitycheckingonlyflag);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_CALLINGFORMATPATTERN)) {
            $criteria->add(AppClientSettingTableMap::COL_CALLINGFORMATPATTERN, $this->callingformatpattern);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_INTERFACILITYFLAG)) {
            $criteria->add(AppClientSettingTableMap::COL_INTERFACILITYFLAG, $this->interfacilityflag);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PROJECTNAME)) {
            $criteria->add(AppClientSettingTableMap::COL_PROJECTNAME, $this->projectname);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PROJECTNAMEID)) {
            $criteria->add(AppClientSettingTableMap::COL_PROJECTNAMEID, $this->projectnameid);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_CENTRALIZEDSERVERURL)) {
            $criteria->add(AppClientSettingTableMap::COL_CENTRALIZEDSERVERURL, $this->centralizedserverurl);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTESERVERDOMAIN)) {
            $criteria->add(AppClientSettingTableMap::COL_REMOTESERVERDOMAIN, $this->remoteserverdomain);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTESERVERPROTOCOL)) {
            $criteria->add(AppClientSettingTableMap::COL_REMOTESERVERPROTOCOL, $this->remoteserverprotocol);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTESERVERPORT)) {
            $criteria->add(AppClientSettingTableMap::COL_REMOTESERVERPORT, $this->remoteserverport);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALSERVERDOMAIN)) {
            $criteria->add(AppClientSettingTableMap::COL_LOCALSERVERDOMAIN, $this->localserverdomain);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALSERVERPROTOCOL)) {
            $criteria->add(AppClientSettingTableMap::COL_LOCALSERVERPROTOCOL, $this->localserverprotocol);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALSERVERPORT)) {
            $criteria->add(AppClientSettingTableMap::COL_LOCALSERVERPORT, $this->localserverport);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PNDOMAIN)) {
            $criteria->add(AppClientSettingTableMap::COL_PNDOMAIN, $this->pndomain);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PNPROTOCOL)) {
            $criteria->add(AppClientSettingTableMap::COL_PNPROTOCOL, $this->pnprotocol);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_PNPORT)) {
            $criteria->add(AppClientSettingTableMap::COL_PNPORT, $this->pnport);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTECCTVDOMAIN)) {
            $criteria->add(AppClientSettingTableMap::COL_REMOTECCTVDOMAIN, $this->remotecctvdomain);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTECCTVPROTOCOL)) {
            $criteria->add(AppClientSettingTableMap::COL_REMOTECCTVPROTOCOL, $this->remotecctvprotocol);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_REMOTECCTVPORT)) {
            $criteria->add(AppClientSettingTableMap::COL_REMOTECCTVPORT, $this->remotecctvport);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALCCTVDOMAIN)) {
            $criteria->add(AppClientSettingTableMap::COL_LOCALCCTVDOMAIN, $this->localcctvdomain);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALCCTVPROTOCOL)) {
            $criteria->add(AppClientSettingTableMap::COL_LOCALCCTVPROTOCOL, $this->localcctvprotocol);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_LOCALCCTVPORT)) {
            $criteria->add(AppClientSettingTableMap::COL_LOCALCCTVPORT, $this->localcctvport);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_TUTORIALIMAGE)) {
            $criteria->add(AppClientSettingTableMap::COL_TUTORIALIMAGE, $this->tutorialimage);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE)) {
            $criteria->add(AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE, $this->tutorialimageenable);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_APPAUTOSYNCINTERVAL)) {
            $criteria->add(AppClientSettingTableMap::COL_APPAUTOSYNCINTERVAL, $this->appautosyncinterval);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_MODIFIED_BY)) {
            $criteria->add(AppClientSettingTableMap::COL_MODIFIED_BY, $this->modified_by);
        }
        if ($this->isColumnModified(AppClientSettingTableMap::COL_MODIFIED_DATE)) {
            $criteria->add(AppClientSettingTableMap::COL_MODIFIED_DATE, $this->modified_date);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildAppClientSettingQuery::create();
        $criteria->add(AppClientSettingTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \AppClientSetting (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setProjectId($this->getProjectId());
        $copyObj->setAppHomeIdleTime($this->getAppHomeIdleTime());
        $copyObj->setAppStandbyIdleTime($this->getAppStandbyIdleTime());
        $copyObj->setAppHomeLogoImage($this->getAppHomeLogoImage());
        $copyObj->setReceiptLogoImage($this->getReceiptLogoImage());
        $copyObj->setFacilityCheckingOnlyFlag($this->getFacilityCheckingOnlyFlag());
        $copyObj->setCallingFormatPattern($this->getCallingFormatPattern());
        $copyObj->setinterFacilityFlag($this->getinterFacilityFlag());
        $copyObj->setProjectName($this->getProjectName());
        $copyObj->setProjectNameId($this->getProjectNameId());
        $copyObj->setCentralizedServerUrl($this->getCentralizedServerUrl());
        $copyObj->setRemoteServerDomain($this->getRemoteServerDomain());
        $copyObj->setRemoteServerProtocol($this->getRemoteServerProtocol());
        $copyObj->setRemoteServerPort($this->getRemoteServerPort());
        $copyObj->setLocalServerDomain($this->getLocalServerDomain());
        $copyObj->setLocalServerProtocol($this->getLocalServerProtocol());
        $copyObj->setLocalServerPort($this->getLocalServerPort());
        $copyObj->setPNDomain($this->getPNDomain());
        $copyObj->setPNProtocol($this->getPNProtocol());
        $copyObj->setPNPort($this->getPNPort());
        $copyObj->setRemoteCctvDomain($this->getRemoteCctvDomain());
        $copyObj->setRemoteCctvProtocol($this->getRemoteCctvProtocol());
        $copyObj->setRemoteCctvPort($this->getRemoteCctvPort());
        $copyObj->setLocalCctvDomain($this->getLocalCctvDomain());
        $copyObj->setLocalCctvProtocol($this->getLocalCctvProtocol());
        $copyObj->setLocalCctvPort($this->getLocalCctvPort());
        $copyObj->setTutorialImage($this->getTutorialImage());
        $copyObj->setTutorialImageEnable($this->getTutorialImageEnable());
        $copyObj->setAppAutoSyncInterval($this->getAppAutoSyncInterval());
        $copyObj->setModifiedBy($this->getModifiedBy());
        $copyObj->setModifiedDate($this->getModifiedDate());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \AppClientSetting Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->projectid = null;
        $this->apphomeidletime = null;
        $this->appstandbyidletime = null;
        $this->apphomelogoimage = null;
        $this->receiptlogoimage = null;
        $this->facilitycheckingonlyflag = null;
        $this->callingformatpattern = null;
        $this->interfacilityflag = null;
        $this->projectname = null;
        $this->projectnameid = null;
        $this->centralizedserverurl = null;
        $this->remoteserverdomain = null;
        $this->remoteserverprotocol = null;
        $this->remoteserverport = null;
        $this->localserverdomain = null;
        $this->localserverprotocol = null;
        $this->localserverport = null;
        $this->pndomain = null;
        $this->pnprotocol = null;
        $this->pnport = null;
        $this->remotecctvdomain = null;
        $this->remotecctvprotocol = null;
        $this->remotecctvport = null;
        $this->localcctvdomain = null;
        $this->localcctvprotocol = null;
        $this->localcctvport = null;
        $this->tutorialimage = null;
        $this->tutorialimageenable = null;
        $this->appautosyncinterval = null;
        $this->modified_by = null;
        $this->modified_date = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AppClientSettingTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
