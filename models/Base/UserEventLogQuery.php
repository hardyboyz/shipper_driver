<?php

namespace Base;

use \UserEventLog as ChildUserEventLog;
use \UserEventLogQuery as ChildUserEventLogQuery;
use \Exception;
use \PDO;
use Map\UserEventLogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'userEventLog' table.
 *
 * 
 *
 * @method     ChildUserEventLogQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserEventLogQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildUserEventLogQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildUserEventLogQuery orderByActionType($order = Criteria::ASC) Order by the actionType column
 * @method     ChildUserEventLogQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildUserEventLogQuery orderByEventDate($order = Criteria::ASC) Order by the eventDate column
 *
 * @method     ChildUserEventLogQuery groupById() Group by the id column
 * @method     ChildUserEventLogQuery groupByProjectId() Group by the projectId column
 * @method     ChildUserEventLogQuery groupByUsername() Group by the username column
 * @method     ChildUserEventLogQuery groupByActionType() Group by the actionType column
 * @method     ChildUserEventLogQuery groupByDescription() Group by the description column
 * @method     ChildUserEventLogQuery groupByEventDate() Group by the eventDate column
 *
 * @method     ChildUserEventLogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserEventLogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserEventLogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserEventLogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserEventLogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserEventLogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserEventLog findOne(ConnectionInterface $con = null) Return the first ChildUserEventLog matching the query
 * @method     ChildUserEventLog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserEventLog matching the query, or a new ChildUserEventLog object populated from the query conditions when no match is found
 *
 * @method     ChildUserEventLog findOneById(string $id) Return the first ChildUserEventLog filtered by the id column
 * @method     ChildUserEventLog findOneByProjectId(string $projectId) Return the first ChildUserEventLog filtered by the projectId column
 * @method     ChildUserEventLog findOneByUsername(string $username) Return the first ChildUserEventLog filtered by the username column
 * @method     ChildUserEventLog findOneByActionType(string $actionType) Return the first ChildUserEventLog filtered by the actionType column
 * @method     ChildUserEventLog findOneByDescription(string $description) Return the first ChildUserEventLog filtered by the description column
 * @method     ChildUserEventLog findOneByEventDate(string $eventDate) Return the first ChildUserEventLog filtered by the eventDate column *

 * @method     ChildUserEventLog requirePk($key, ConnectionInterface $con = null) Return the ChildUserEventLog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserEventLog requireOne(ConnectionInterface $con = null) Return the first ChildUserEventLog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserEventLog requireOneById(string $id) Return the first ChildUserEventLog filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserEventLog requireOneByProjectId(string $projectId) Return the first ChildUserEventLog filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserEventLog requireOneByUsername(string $username) Return the first ChildUserEventLog filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserEventLog requireOneByActionType(string $actionType) Return the first ChildUserEventLog filtered by the actionType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserEventLog requireOneByDescription(string $description) Return the first ChildUserEventLog filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserEventLog requireOneByEventDate(string $eventDate) Return the first ChildUserEventLog filtered by the eventDate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserEventLog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserEventLog objects based on current ModelCriteria
 * @method     ChildUserEventLog[]|ObjectCollection findById(string $id) Return ChildUserEventLog objects filtered by the id column
 * @method     ChildUserEventLog[]|ObjectCollection findByProjectId(string $projectId) Return ChildUserEventLog objects filtered by the projectId column
 * @method     ChildUserEventLog[]|ObjectCollection findByUsername(string $username) Return ChildUserEventLog objects filtered by the username column
 * @method     ChildUserEventLog[]|ObjectCollection findByActionType(string $actionType) Return ChildUserEventLog objects filtered by the actionType column
 * @method     ChildUserEventLog[]|ObjectCollection findByDescription(string $description) Return ChildUserEventLog objects filtered by the description column
 * @method     ChildUserEventLog[]|ObjectCollection findByEventDate(string $eventDate) Return ChildUserEventLog objects filtered by the eventDate column
 * @method     ChildUserEventLog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserEventLogQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UserEventLogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\UserEventLog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserEventLogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserEventLogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserEventLogQuery) {
            return $criteria;
        }
        $query = new ChildUserEventLogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserEventLog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserEventLogTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserEventLogTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserEventLog A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, username, actionType, description, eventDate FROM userEventLog WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserEventLog $obj */
            $obj = new ChildUserEventLog();
            $obj->hydrate($row);
            UserEventLogTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserEventLog|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserEventLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserEventLogTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserEventLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserEventLogTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%', Criteria::LIKE); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserEventLogQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserEventLogTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%', Criteria::LIKE); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserEventLogQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserEventLogTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserEventLogQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserEventLogTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the actionType column
     *
     * Example usage:
     * <code>
     * $query->filterByActionType('fooValue');   // WHERE actionType = 'fooValue'
     * $query->filterByActionType('%fooValue%', Criteria::LIKE); // WHERE actionType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $actionType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserEventLogQuery The current query, for fluid interface
     */
    public function filterByActionType($actionType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($actionType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserEventLogTableMap::COL_ACTIONTYPE, $actionType, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserEventLogQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserEventLogTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the eventDate column
     *
     * Example usage:
     * <code>
     * $query->filterByEventDate('2011-03-14'); // WHERE eventDate = '2011-03-14'
     * $query->filterByEventDate('now'); // WHERE eventDate = '2011-03-14'
     * $query->filterByEventDate(array('max' => 'yesterday')); // WHERE eventDate > '2011-03-13'
     * </code>
     *
     * @param     mixed $eventDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserEventLogQuery The current query, for fluid interface
     */
    public function filterByEventDate($eventDate = null, $comparison = null)
    {
        if (is_array($eventDate)) {
            $useMinMax = false;
            if (isset($eventDate['min'])) {
                $this->addUsingAlias(UserEventLogTableMap::COL_EVENTDATE, $eventDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($eventDate['max'])) {
                $this->addUsingAlias(UserEventLogTableMap::COL_EVENTDATE, $eventDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserEventLogTableMap::COL_EVENTDATE, $eventDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserEventLog $userEventLog Object to remove from the list of results
     *
     * @return $this|ChildUserEventLogQuery The current query, for fluid interface
     */
    public function prune($userEventLog = null)
    {
        if ($userEventLog) {
            $this->addUsingAlias(UserEventLogTableMap::COL_ID, $userEventLog->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the userEventLog table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserEventLogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserEventLogTableMap::clearInstancePool();
            UserEventLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserEventLogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserEventLogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            UserEventLogTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            UserEventLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserEventLogQuery
