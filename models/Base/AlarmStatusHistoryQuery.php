<?php

namespace Base;

use \AlarmStatusHistory as ChildAlarmStatusHistory;
use \AlarmStatusHistoryQuery as ChildAlarmStatusHistoryQuery;
use \Exception;
use \PDO;
use Map\AlarmStatusHistoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'alarmStatusHistory' table.
 *
 *
 *
 * @method     ChildAlarmStatusHistoryQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildAlarmStatusHistoryQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildAlarmStatusHistoryQuery orderByUnitId($order = Criteria::ASC) Order by the unitId column
 * @method     ChildAlarmStatusHistoryQuery orderByZoneId($order = Criteria::ASC) Order by the zoneId column
 * @method     ChildAlarmStatusHistoryQuery orderByZoneName($order = Criteria::ASC) Order by the zoneName column
 * @method     ChildAlarmStatusHistoryQuery orderByZoneType($order = Criteria::ASC) Order by the zoneType column
 * @method     ChildAlarmStatusHistoryQuery orderByAreaId($order = Criteria::ASC) Order by the areaId column
 * @method     ChildAlarmStatusHistoryQuery orderByAreaName($order = Criteria::ASC) Order by the areaName column
 * @method     ChildAlarmStatusHistoryQuery orderByTriggeredDate($order = Criteria::ASC) Order by the triggered_date column
 * @method     ChildAlarmStatusHistoryQuery orderByAcknowledgedDate($order = Criteria::ASC) Order by the acknowledged_date column
 * @method     ChildAlarmStatusHistoryQuery orderByAcknowledgedBy($order = Criteria::ASC) Order by the acknowledged_by column
 * @method     ChildAlarmStatusHistoryQuery orderByRemarks($order = Criteria::ASC) Order by the remarks column
 *
 * @method     ChildAlarmStatusHistoryQuery groupById() Group by the id column
 * @method     ChildAlarmStatusHistoryQuery groupByProjectId() Group by the projectId column
 * @method     ChildAlarmStatusHistoryQuery groupByUnitId() Group by the unitId column
 * @method     ChildAlarmStatusHistoryQuery groupByZoneId() Group by the zoneId column
 * @method     ChildAlarmStatusHistoryQuery groupByZoneName() Group by the zoneName column
 * @method     ChildAlarmStatusHistoryQuery groupByZoneType() Group by the zoneType column
 * @method     ChildAlarmStatusHistoryQuery groupByAreaId() Group by the areaId column
 * @method     ChildAlarmStatusHistoryQuery groupByAreaName() Group by the areaName column
 * @method     ChildAlarmStatusHistoryQuery groupByTriggeredDate() Group by the triggered_date column
 * @method     ChildAlarmStatusHistoryQuery groupByAcknowledgedDate() Group by the acknowledged_date column
 * @method     ChildAlarmStatusHistoryQuery groupByAcknowledgedBy() Group by the acknowledged_by column
 * @method     ChildAlarmStatusHistoryQuery groupByRemarks() Group by the remarks column
 *
 * @method     ChildAlarmStatusHistoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAlarmStatusHistoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAlarmStatusHistoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAlarmStatusHistoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAlarmStatusHistoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAlarmStatusHistoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAlarmStatusHistory findOne(ConnectionInterface $con = null) Return the first ChildAlarmStatusHistory matching the query
 * @method     ChildAlarmStatusHistory findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAlarmStatusHistory matching the query, or a new ChildAlarmStatusHistory object populated from the query conditions when no match is found
 *
 * @method     ChildAlarmStatusHistory findOneById(string $id) Return the first ChildAlarmStatusHistory filtered by the id column
 * @method     ChildAlarmStatusHistory findOneByProjectId(string $projectId) Return the first ChildAlarmStatusHistory filtered by the projectId column
 * @method     ChildAlarmStatusHistory findOneByUnitId(string $unitId) Return the first ChildAlarmStatusHistory filtered by the unitId column
 * @method     ChildAlarmStatusHistory findOneByZoneId(string $zoneId) Return the first ChildAlarmStatusHistory filtered by the zoneId column
 * @method     ChildAlarmStatusHistory findOneByZoneName(string $zoneName) Return the first ChildAlarmStatusHistory filtered by the zoneName column
 * @method     ChildAlarmStatusHistory findOneByZoneType(string $zoneType) Return the first ChildAlarmStatusHistory filtered by the zoneType column
 * @method     ChildAlarmStatusHistory findOneByAreaId(string $areaId) Return the first ChildAlarmStatusHistory filtered by the areaId column
 * @method     ChildAlarmStatusHistory findOneByAreaName(string $areaName) Return the first ChildAlarmStatusHistory filtered by the areaName column
 * @method     ChildAlarmStatusHistory findOneByTriggeredDate(string $triggered_date) Return the first ChildAlarmStatusHistory filtered by the triggered_date column
 * @method     ChildAlarmStatusHistory findOneByAcknowledgedDate(string $acknowledged_date) Return the first ChildAlarmStatusHistory filtered by the acknowledged_date column
 * @method     ChildAlarmStatusHistory findOneByAcknowledgedBy(string $acknowledged_by) Return the first ChildAlarmStatusHistory filtered by the acknowledged_by column
 * @method     ChildAlarmStatusHistory findOneByRemarks(string $remarks) Return the first ChildAlarmStatusHistory filtered by the remarks column *

 * @method     ChildAlarmStatusHistory requirePk($key, ConnectionInterface $con = null) Return the ChildAlarmStatusHistory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOne(ConnectionInterface $con = null) Return the first ChildAlarmStatusHistory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAlarmStatusHistory requireOneById(string $id) Return the first ChildAlarmStatusHistory filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByProjectId(string $projectId) Return the first ChildAlarmStatusHistory filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByUnitId(string $unitId) Return the first ChildAlarmStatusHistory filtered by the unitId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByZoneId(string $zoneId) Return the first ChildAlarmStatusHistory filtered by the zoneId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByZoneName(string $zoneName) Return the first ChildAlarmStatusHistory filtered by the zoneName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByZoneType(string $zoneType) Return the first ChildAlarmStatusHistory filtered by the zoneType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByAreaId(string $areaId) Return the first ChildAlarmStatusHistory filtered by the areaId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByAreaName(string $areaName) Return the first ChildAlarmStatusHistory filtered by the areaName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByTriggeredDate(string $triggered_date) Return the first ChildAlarmStatusHistory filtered by the triggered_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByAcknowledgedDate(string $acknowledged_date) Return the first ChildAlarmStatusHistory filtered by the acknowledged_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByAcknowledgedBy(string $acknowledged_by) Return the first ChildAlarmStatusHistory filtered by the acknowledged_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatusHistory requireOneByRemarks(string $remarks) Return the first ChildAlarmStatusHistory filtered by the remarks column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAlarmStatusHistory[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAlarmStatusHistory objects based on current ModelCriteria
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findById(string $id) Return ChildAlarmStatusHistory objects filtered by the id column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByProjectId(string $projectId) Return ChildAlarmStatusHistory objects filtered by the projectId column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByUnitId(string $unitId) Return ChildAlarmStatusHistory objects filtered by the unitId column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByZoneId(string $zoneId) Return ChildAlarmStatusHistory objects filtered by the zoneId column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByZoneName(string $zoneName) Return ChildAlarmStatusHistory objects filtered by the zoneName column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByZoneType(string $zoneType) Return ChildAlarmStatusHistory objects filtered by the zoneType column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByAreaId(string $areaId) Return ChildAlarmStatusHistory objects filtered by the areaId column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByAreaName(string $areaName) Return ChildAlarmStatusHistory objects filtered by the areaName column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByTriggeredDate(string $triggered_date) Return ChildAlarmStatusHistory objects filtered by the triggered_date column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByAcknowledgedDate(string $acknowledged_date) Return ChildAlarmStatusHistory objects filtered by the acknowledged_date column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByAcknowledgedBy(string $acknowledged_by) Return ChildAlarmStatusHistory objects filtered by the acknowledged_by column
 * @method     ChildAlarmStatusHistory[]|ObjectCollection findByRemarks(string $remarks) Return ChildAlarmStatusHistory objects filtered by the remarks column
 * @method     ChildAlarmStatusHistory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AlarmStatusHistoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AlarmStatusHistoryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\AlarmStatusHistory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAlarmStatusHistoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAlarmStatusHistoryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAlarmStatusHistoryQuery) {
            return $criteria;
        }
        $query = new ChildAlarmStatusHistoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAlarmStatusHistory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AlarmStatusHistoryTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AlarmStatusHistoryTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAlarmStatusHistory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, unitId, zoneId, zoneName, zoneType, areaId, areaName, triggered_date, acknowledged_date, acknowledged_by, remarks FROM alarmStatusHistory WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAlarmStatusHistory $obj */
            $obj = new ChildAlarmStatusHistory();
            $obj->hydrate($row);
            AlarmStatusHistoryTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAlarmStatusHistory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the unitId column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId('fooValue');   // WHERE unitId = 'fooValue'
     * $query->filterByUnitId('%fooValue%'); // WHERE unitId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitId)) {
                $unitId = str_replace('*', '%', $unitId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_UNITID, $unitId, $comparison);
    }

    /**
     * Filter the query on the zoneId column
     *
     * Example usage:
     * <code>
     * $query->filterByZoneId('fooValue');   // WHERE zoneId = 'fooValue'
     * $query->filterByZoneId('%fooValue%'); // WHERE zoneId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zoneId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByZoneId($zoneId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zoneId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $zoneId)) {
                $zoneId = str_replace('*', '%', $zoneId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ZONEID, $zoneId, $comparison);
    }

    /**
     * Filter the query on the zoneName column
     *
     * Example usage:
     * <code>
     * $query->filterByZoneName('fooValue');   // WHERE zoneName = 'fooValue'
     * $query->filterByZoneName('%fooValue%'); // WHERE zoneName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zoneName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByZoneName($zoneName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zoneName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $zoneName)) {
                $zoneName = str_replace('*', '%', $zoneName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ZONENAME, $zoneName, $comparison);
    }

    /**
     * Filter the query on the zoneType column
     *
     * Example usage:
     * <code>
     * $query->filterByZoneType('fooValue');   // WHERE zoneType = 'fooValue'
     * $query->filterByZoneType('%fooValue%'); // WHERE zoneType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zoneType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByZoneType($zoneType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zoneType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $zoneType)) {
                $zoneType = str_replace('*', '%', $zoneType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ZONETYPE, $zoneType, $comparison);
    }

    /**
     * Filter the query on the areaId column
     *
     * Example usage:
     * <code>
     * $query->filterByAreaId('fooValue');   // WHERE areaId = 'fooValue'
     * $query->filterByAreaId('%fooValue%'); // WHERE areaId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $areaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByAreaId($areaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($areaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $areaId)) {
                $areaId = str_replace('*', '%', $areaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_AREAID, $areaId, $comparison);
    }

    /**
     * Filter the query on the areaName column
     *
     * Example usage:
     * <code>
     * $query->filterByAreaName('fooValue');   // WHERE areaName = 'fooValue'
     * $query->filterByAreaName('%fooValue%'); // WHERE areaName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $areaName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByAreaName($areaName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($areaName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $areaName)) {
                $areaName = str_replace('*', '%', $areaName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_AREANAME, $areaName, $comparison);
    }

    /**
     * Filter the query on the triggered_date column
     *
     * Example usage:
     * <code>
     * $query->filterByTriggeredDate('2011-03-14'); // WHERE triggered_date = '2011-03-14'
     * $query->filterByTriggeredDate('now'); // WHERE triggered_date = '2011-03-14'
     * $query->filterByTriggeredDate(array('max' => 'yesterday')); // WHERE triggered_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $triggeredDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByTriggeredDate($triggeredDate = null, $comparison = null)
    {
        if (is_array($triggeredDate)) {
            $useMinMax = false;
            if (isset($triggeredDate['min'])) {
                $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE, $triggeredDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($triggeredDate['max'])) {
                $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE, $triggeredDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE, $triggeredDate, $comparison);
    }

    /**
     * Filter the query on the acknowledged_date column
     *
     * Example usage:
     * <code>
     * $query->filterByAcknowledgedDate('2011-03-14'); // WHERE acknowledged_date = '2011-03-14'
     * $query->filterByAcknowledgedDate('now'); // WHERE acknowledged_date = '2011-03-14'
     * $query->filterByAcknowledgedDate(array('max' => 'yesterday')); // WHERE acknowledged_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $acknowledgedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByAcknowledgedDate($acknowledgedDate = null, $comparison = null)
    {
        if (is_array($acknowledgedDate)) {
            $useMinMax = false;
            if (isset($acknowledgedDate['min'])) {
                $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE, $acknowledgedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($acknowledgedDate['max'])) {
                $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE, $acknowledgedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE, $acknowledgedDate, $comparison);
    }

    /**
     * Filter the query on the acknowledged_by column
     *
     * Example usage:
     * <code>
     * $query->filterByAcknowledgedBy('fooValue');   // WHERE acknowledged_by = 'fooValue'
     * $query->filterByAcknowledgedBy('%fooValue%'); // WHERE acknowledged_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acknowledgedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByAcknowledgedBy($acknowledgedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acknowledgedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acknowledgedBy)) {
                $acknowledgedBy = str_replace('*', '%', $acknowledgedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_BY, $acknowledgedBy, $comparison);
    }

    /**
     * Filter the query on the remarks column
     *
     * Example usage:
     * <code>
     * $query->filterByRemarks('fooValue');   // WHERE remarks = 'fooValue'
     * $query->filterByRemarks('%fooValue%'); // WHERE remarks LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remarks The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function filterByRemarks($remarks = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remarks)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $remarks)) {
                $remarks = str_replace('*', '%', $remarks);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_REMARKS, $remarks, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAlarmStatusHistory $alarmStatusHistory Object to remove from the list of results
     *
     * @return $this|ChildAlarmStatusHistoryQuery The current query, for fluid interface
     */
    public function prune($alarmStatusHistory = null)
    {
        if ($alarmStatusHistory) {
            $this->addUsingAlias(AlarmStatusHistoryTableMap::COL_ID, $alarmStatusHistory->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the alarmStatusHistory table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusHistoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AlarmStatusHistoryTableMap::clearInstancePool();
            AlarmStatusHistoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusHistoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AlarmStatusHistoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AlarmStatusHistoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AlarmStatusHistoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AlarmStatusHistoryQuery
