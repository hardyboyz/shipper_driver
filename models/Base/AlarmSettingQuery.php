<?php

namespace Base;

use \AlarmSetting as ChildAlarmSetting;
use \AlarmSettingQuery as ChildAlarmSettingQuery;
use \Exception;
use \PDO;
use Map\AlarmSettingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'alarmSetting' table.
 *
 *
 *
 * @method     ChildAlarmSettingQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildAlarmSettingQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildAlarmSettingQuery orderByallowDetail($order = Criteria::ASC) Order by the allowDetail column
 * @method     ChildAlarmSettingQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildAlarmSettingQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildAlarmSettingQuery groupById() Group by the id column
 * @method     ChildAlarmSettingQuery groupByProjectId() Group by the projectId column
 * @method     ChildAlarmSettingQuery groupByallowDetail() Group by the allowDetail column
 * @method     ChildAlarmSettingQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildAlarmSettingQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildAlarmSettingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAlarmSettingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAlarmSettingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAlarmSettingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAlarmSettingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAlarmSettingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAlarmSetting findOne(ConnectionInterface $con = null) Return the first ChildAlarmSetting matching the query
 * @method     ChildAlarmSetting findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAlarmSetting matching the query, or a new ChildAlarmSetting object populated from the query conditions when no match is found
 *
 * @method     ChildAlarmSetting findOneById(string $id) Return the first ChildAlarmSetting filtered by the id column
 * @method     ChildAlarmSetting findOneByProjectId(string $projectId) Return the first ChildAlarmSetting filtered by the projectId column
 * @method     ChildAlarmSetting findOneByallowDetail(int $allowDetail) Return the first ChildAlarmSetting filtered by the allowDetail column
 * @method     ChildAlarmSetting findOneByModifiedBy(string $modified_by) Return the first ChildAlarmSetting filtered by the modified_by column
 * @method     ChildAlarmSetting findOneByModifiedDate(string $modified_date) Return the first ChildAlarmSetting filtered by the modified_date column *

 * @method     ChildAlarmSetting requirePk($key, ConnectionInterface $con = null) Return the ChildAlarmSetting by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmSetting requireOne(ConnectionInterface $con = null) Return the first ChildAlarmSetting matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAlarmSetting requireOneById(string $id) Return the first ChildAlarmSetting filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmSetting requireOneByProjectId(string $projectId) Return the first ChildAlarmSetting filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmSetting requireOneByallowDetail(int $allowDetail) Return the first ChildAlarmSetting filtered by the allowDetail column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmSetting requireOneByModifiedBy(string $modified_by) Return the first ChildAlarmSetting filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmSetting requireOneByModifiedDate(string $modified_date) Return the first ChildAlarmSetting filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAlarmSetting[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAlarmSetting objects based on current ModelCriteria
 * @method     ChildAlarmSetting[]|ObjectCollection findById(string $id) Return ChildAlarmSetting objects filtered by the id column
 * @method     ChildAlarmSetting[]|ObjectCollection findByProjectId(string $projectId) Return ChildAlarmSetting objects filtered by the projectId column
 * @method     ChildAlarmSetting[]|ObjectCollection findByallowDetail(int $allowDetail) Return ChildAlarmSetting objects filtered by the allowDetail column
 * @method     ChildAlarmSetting[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildAlarmSetting objects filtered by the modified_by column
 * @method     ChildAlarmSetting[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildAlarmSetting objects filtered by the modified_date column
 * @method     ChildAlarmSetting[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AlarmSettingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AlarmSettingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\AlarmSetting', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAlarmSettingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAlarmSettingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAlarmSettingQuery) {
            return $criteria;
        }
        $query = new ChildAlarmSettingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAlarmSetting|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AlarmSettingTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AlarmSettingTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAlarmSetting A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, allowDetail, modified_by, modified_date FROM alarmSetting WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAlarmSetting $obj */
            $obj = new ChildAlarmSetting();
            $obj->hydrate($row);
            AlarmSettingTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAlarmSetting|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAlarmSettingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AlarmSettingTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAlarmSettingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AlarmSettingTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmSettingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmSettingTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmSettingQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmSettingTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the allowDetail column
     *
     * Example usage:
     * <code>
     * $query->filterByallowDetail(1234); // WHERE allowDetail = 1234
     * $query->filterByallowDetail(array(12, 34)); // WHERE allowDetail IN (12, 34)
     * $query->filterByallowDetail(array('min' => 12)); // WHERE allowDetail > 12
     * </code>
     *
     * @param     mixed $allowDetail The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmSettingQuery The current query, for fluid interface
     */
    public function filterByallowDetail($allowDetail = null, $comparison = null)
    {
        if (is_array($allowDetail)) {
            $useMinMax = false;
            if (isset($allowDetail['min'])) {
                $this->addUsingAlias(AlarmSettingTableMap::COL_ALLOWDETAIL, $allowDetail['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($allowDetail['max'])) {
                $this->addUsingAlias(AlarmSettingTableMap::COL_ALLOWDETAIL, $allowDetail['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlarmSettingTableMap::COL_ALLOWDETAIL, $allowDetail, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmSettingQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmSettingTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmSettingQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(AlarmSettingTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(AlarmSettingTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlarmSettingTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAlarmSetting $alarmSetting Object to remove from the list of results
     *
     * @return $this|ChildAlarmSettingQuery The current query, for fluid interface
     */
    public function prune($alarmSetting = null)
    {
        if ($alarmSetting) {
            $this->addUsingAlias(AlarmSettingTableMap::COL_ID, $alarmSetting->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the alarmSetting table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmSettingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AlarmSettingTableMap::clearInstancePool();
            AlarmSettingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmSettingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AlarmSettingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AlarmSettingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AlarmSettingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AlarmSettingQuery
