<?php

namespace Base;

use \ShProject as ChildShProject;
use \ShProjectQuery as ChildShProjectQuery;
use \Exception;
use \PDO;
use Map\ShProjectTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sh_project' table.
 *
 *
 *
 * @method     ChildShProjectQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildShProjectQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildShProjectQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildShProjectQuery orderByGatewayProtocol($order = Criteria::ASC) Order by the gatewayProtocol column
 * @method     ChildShProjectQuery orderByGatewayURL($order = Criteria::ASC) Order by the gatewayURL column
 * @method     ChildShProjectQuery orderByGatewayPort($order = Criteria::ASC) Order by the gatewayPort column
 * @method     ChildShProjectQuery orderBySSID($order = Criteria::ASC) Order by the ssid column
 * @method     ChildShProjectQuery orderByPublicIP($order = Criteria::ASC) Order by the publicIP column
 *
 * @method     ChildShProjectQuery groupById() Group by the id column
 * @method     ChildShProjectQuery groupByProjectId() Group by the project_id column
 * @method     ChildShProjectQuery groupByName() Group by the name column
 * @method     ChildShProjectQuery groupByGatewayProtocol() Group by the gatewayProtocol column
 * @method     ChildShProjectQuery groupByGatewayURL() Group by the gatewayURL column
 * @method     ChildShProjectQuery groupByGatewayPort() Group by the gatewayPort column
 * @method     ChildShProjectQuery groupBySSID() Group by the ssid column
 * @method     ChildShProjectQuery groupByPublicIP() Group by the publicIP column
 *
 * @method     ChildShProjectQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildShProjectQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildShProjectQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildShProjectQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildShProjectQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildShProjectQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildShProject findOne(ConnectionInterface $con = null) Return the first ChildShProject matching the query
 * @method     ChildShProject findOneOrCreate(ConnectionInterface $con = null) Return the first ChildShProject matching the query, or a new ChildShProject object populated from the query conditions when no match is found
 *
 * @method     ChildShProject findOneById(int $id) Return the first ChildShProject filtered by the id column
 * @method     ChildShProject findOneByProjectId(string $project_id) Return the first ChildShProject filtered by the project_id column
 * @method     ChildShProject findOneByName(string $name) Return the first ChildShProject filtered by the name column
 * @method     ChildShProject findOneByGatewayProtocol(string $gatewayProtocol) Return the first ChildShProject filtered by the gatewayProtocol column
 * @method     ChildShProject findOneByGatewayURL(string $gatewayURL) Return the first ChildShProject filtered by the gatewayURL column
 * @method     ChildShProject findOneByGatewayPort(string $gatewayPort) Return the first ChildShProject filtered by the gatewayPort column
 * @method     ChildShProject findOneBySSID(string $ssid) Return the first ChildShProject filtered by the ssid column
 * @method     ChildShProject findOneByPublicIP(string $publicIP) Return the first ChildShProject filtered by the publicIP column *

 * @method     ChildShProject requirePk($key, ConnectionInterface $con = null) Return the ChildShProject by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShProject requireOne(ConnectionInterface $con = null) Return the first ChildShProject matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildShProject requireOneById(int $id) Return the first ChildShProject filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShProject requireOneByProjectId(string $project_id) Return the first ChildShProject filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShProject requireOneByName(string $name) Return the first ChildShProject filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShProject requireOneByGatewayProtocol(string $gatewayProtocol) Return the first ChildShProject filtered by the gatewayProtocol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShProject requireOneByGatewayURL(string $gatewayURL) Return the first ChildShProject filtered by the gatewayURL column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShProject requireOneByGatewayPort(string $gatewayPort) Return the first ChildShProject filtered by the gatewayPort column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShProject requireOneBySSID(string $ssid) Return the first ChildShProject filtered by the ssid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShProject requireOneByPublicIP(string $publicIP) Return the first ChildShProject filtered by the publicIP column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildShProject[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildShProject objects based on current ModelCriteria
 * @method     ChildShProject[]|ObjectCollection findById(int $id) Return ChildShProject objects filtered by the id column
 * @method     ChildShProject[]|ObjectCollection findByProjectId(string $project_id) Return ChildShProject objects filtered by the project_id column
 * @method     ChildShProject[]|ObjectCollection findByName(string $name) Return ChildShProject objects filtered by the name column
 * @method     ChildShProject[]|ObjectCollection findByGatewayProtocol(string $gatewayProtocol) Return ChildShProject objects filtered by the gatewayProtocol column
 * @method     ChildShProject[]|ObjectCollection findByGatewayURL(string $gatewayURL) Return ChildShProject objects filtered by the gatewayURL column
 * @method     ChildShProject[]|ObjectCollection findByGatewayPort(string $gatewayPort) Return ChildShProject objects filtered by the gatewayPort column
 * @method     ChildShProject[]|ObjectCollection findBySSID(string $ssid) Return ChildShProject objects filtered by the ssid column
 * @method     ChildShProject[]|ObjectCollection findByPublicIP(string $publicIP) Return ChildShProject objects filtered by the publicIP column
 * @method     ChildShProject[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ShProjectQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ShProjectQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ShProject', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildShProjectQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildShProjectQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildShProjectQuery) {
            return $criteria;
        }
        $query = new ChildShProjectQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildShProject|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ShProjectTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ShProjectTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildShProject A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, project_id, name, gatewayProtocol, gatewayURL, gatewayPort, ssid, publicIP FROM sh_project WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildShProject $obj */
            $obj = new ChildShProject();
            $obj->hydrate($row);
            ShProjectTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildShProject|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ShProjectTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ShProjectTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ShProjectTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ShProjectTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShProjectTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ShProjectTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ShProjectTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the gatewayProtocol column
     *
     * Example usage:
     * <code>
     * $query->filterByGatewayProtocol('fooValue');   // WHERE gatewayProtocol = 'fooValue'
     * $query->filterByGatewayProtocol('%fooValue%'); // WHERE gatewayProtocol LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gatewayProtocol The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterByGatewayProtocol($gatewayProtocol = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gatewayProtocol)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gatewayProtocol)) {
                $gatewayProtocol = str_replace('*', '%', $gatewayProtocol);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ShProjectTableMap::COL_GATEWAYPROTOCOL, $gatewayProtocol, $comparison);
    }

    /**
     * Filter the query on the gatewayURL column
     *
     * Example usage:
     * <code>
     * $query->filterByGatewayURL('fooValue');   // WHERE gatewayURL = 'fooValue'
     * $query->filterByGatewayURL('%fooValue%'); // WHERE gatewayURL LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gatewayURL The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterByGatewayURL($gatewayURL = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gatewayURL)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gatewayURL)) {
                $gatewayURL = str_replace('*', '%', $gatewayURL);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ShProjectTableMap::COL_GATEWAYURL, $gatewayURL, $comparison);
    }

    /**
     * Filter the query on the gatewayPort column
     *
     * Example usage:
     * <code>
     * $query->filterByGatewayPort('fooValue');   // WHERE gatewayPort = 'fooValue'
     * $query->filterByGatewayPort('%fooValue%'); // WHERE gatewayPort LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gatewayPort The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterByGatewayPort($gatewayPort = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gatewayPort)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gatewayPort)) {
                $gatewayPort = str_replace('*', '%', $gatewayPort);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ShProjectTableMap::COL_GATEWAYPORT, $gatewayPort, $comparison);
    }

    /**
     * Filter the query on the ssid column
     *
     * Example usage:
     * <code>
     * $query->filterBySSID('fooValue');   // WHERE ssid = 'fooValue'
     * $query->filterBySSID('%fooValue%'); // WHERE ssid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sSID The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterBySSID($sSID = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sSID)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sSID)) {
                $sSID = str_replace('*', '%', $sSID);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ShProjectTableMap::COL_SSID, $sSID, $comparison);
    }

    /**
     * Filter the query on the publicIP column
     *
     * Example usage:
     * <code>
     * $query->filterByPublicIP('fooValue');   // WHERE publicIP = 'fooValue'
     * $query->filterByPublicIP('%fooValue%'); // WHERE publicIP LIKE '%fooValue%'
     * </code>
     *
     * @param     string $publicIP The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function filterByPublicIP($publicIP = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($publicIP)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $publicIP)) {
                $publicIP = str_replace('*', '%', $publicIP);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ShProjectTableMap::COL_PUBLICIP, $publicIP, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildShProject $shProject Object to remove from the list of results
     *
     * @return $this|ChildShProjectQuery The current query, for fluid interface
     */
    public function prune($shProject = null)
    {
        if ($shProject) {
            $this->addUsingAlias(ShProjectTableMap::COL_ID, $shProject->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sh_project table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShProjectTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ShProjectTableMap::clearInstancePool();
            ShProjectTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShProjectTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ShProjectTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ShProjectTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ShProjectTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ShProjectQuery
