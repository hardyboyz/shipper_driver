<?php

namespace Base;

use \AppMenuList as ChildAppMenuList;
use \AppMenuListQuery as ChildAppMenuListQuery;
use \Exception;
use \PDO;
use Map\AppMenuListTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'appMenuList' table.
 *
 *
 *
 * @method     ChildAppMenuListQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildAppMenuListQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildAppMenuListQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildAppMenuListQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildAppMenuListQuery orderByImage($order = Criteria::ASC) Order by the image column
 * @method     ChildAppMenuListQuery orderBySideMenuOrder($order = Criteria::ASC) Order by the sideMenuOrder column
 * @method     ChildAppMenuListQuery orderBySideMenuEnable($order = Criteria::ASC) Order by the sideMenuEnable column
 * @method     ChildAppMenuListQuery orderByHomeMenuOrder($order = Criteria::ASC) Order by the homeMenuOrder column
 * @method     ChildAppMenuListQuery orderByHomeMenuEnable($order = Criteria::ASC) Order by the homeMenuEnable column
 * @method     ChildAppMenuListQuery orderByTopRightMenuOrder($order = Criteria::ASC) Order by the topRightMenuOrder column
 * @method     ChildAppMenuListQuery orderByTopRightMenuEnable($order = Criteria::ASC) Order by the topRightMenuEnable column
 * @method     ChildAppMenuListQuery orderByTutorialImage($order = Criteria::ASC) Order by the tutorialImage column
 * @method     ChildAppMenuListQuery orderByTutorialEnable($order = Criteria::ASC) Order by the tutorialEnable column
 * @method     ChildAppMenuListQuery orderByPhoneTutorialImage($order = Criteria::ASC) Order by the phoneTutorialImage column
 * @method     ChildAppMenuListQuery orderByPhoneTutorialEnable($order = Criteria::ASC) Order by the phoneTutorialEnable column
 * @method     ChildAppMenuListQuery orderByPhoneIcon($order = Criteria::ASC) Order by the phoneIcon column
 * @method     ChildAppMenuListQuery orderByModuleType($order = Criteria::ASC) Order by the module_type column
 * @method     ChildAppMenuListQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildAppMenuListQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildAppMenuListQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildAppMenuListQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildAppMenuListQuery groupById() Group by the id column
 * @method     ChildAppMenuListQuery groupByProjectId() Group by the projectId column
 * @method     ChildAppMenuListQuery groupByName() Group by the name column
 * @method     ChildAppMenuListQuery groupByDescription() Group by the description column
 * @method     ChildAppMenuListQuery groupByImage() Group by the image column
 * @method     ChildAppMenuListQuery groupBySideMenuOrder() Group by the sideMenuOrder column
 * @method     ChildAppMenuListQuery groupBySideMenuEnable() Group by the sideMenuEnable column
 * @method     ChildAppMenuListQuery groupByHomeMenuOrder() Group by the homeMenuOrder column
 * @method     ChildAppMenuListQuery groupByHomeMenuEnable() Group by the homeMenuEnable column
 * @method     ChildAppMenuListQuery groupByTopRightMenuOrder() Group by the topRightMenuOrder column
 * @method     ChildAppMenuListQuery groupByTopRightMenuEnable() Group by the topRightMenuEnable column
 * @method     ChildAppMenuListQuery groupByTutorialImage() Group by the tutorialImage column
 * @method     ChildAppMenuListQuery groupByTutorialEnable() Group by the tutorialEnable column
 * @method     ChildAppMenuListQuery groupByPhoneTutorialImage() Group by the phoneTutorialImage column
 * @method     ChildAppMenuListQuery groupByPhoneTutorialEnable() Group by the phoneTutorialEnable column
 * @method     ChildAppMenuListQuery groupByPhoneIcon() Group by the phoneIcon column
 * @method     ChildAppMenuListQuery groupByModuleType() Group by the module_type column
 * @method     ChildAppMenuListQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildAppMenuListQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildAppMenuListQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildAppMenuListQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildAppMenuListQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAppMenuListQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAppMenuListQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAppMenuListQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAppMenuListQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAppMenuListQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAppMenuList findOne(ConnectionInterface $con = null) Return the first ChildAppMenuList matching the query
 * @method     ChildAppMenuList findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAppMenuList matching the query, or a new ChildAppMenuList object populated from the query conditions when no match is found
 *
 * @method     ChildAppMenuList findOneById(string $id) Return the first ChildAppMenuList filtered by the id column
 * @method     ChildAppMenuList findOneByProjectId(string $projectId) Return the first ChildAppMenuList filtered by the projectId column
 * @method     ChildAppMenuList findOneByName(string $name) Return the first ChildAppMenuList filtered by the name column
 * @method     ChildAppMenuList findOneByDescription(string $description) Return the first ChildAppMenuList filtered by the description column
 * @method     ChildAppMenuList findOneByImage(string $image) Return the first ChildAppMenuList filtered by the image column
 * @method     ChildAppMenuList findOneBySideMenuOrder(int $sideMenuOrder) Return the first ChildAppMenuList filtered by the sideMenuOrder column
 * @method     ChildAppMenuList findOneBySideMenuEnable(int $sideMenuEnable) Return the first ChildAppMenuList filtered by the sideMenuEnable column
 * @method     ChildAppMenuList findOneByHomeMenuOrder(int $homeMenuOrder) Return the first ChildAppMenuList filtered by the homeMenuOrder column
 * @method     ChildAppMenuList findOneByHomeMenuEnable(int $homeMenuEnable) Return the first ChildAppMenuList filtered by the homeMenuEnable column
 * @method     ChildAppMenuList findOneByTopRightMenuOrder(int $topRightMenuOrder) Return the first ChildAppMenuList filtered by the topRightMenuOrder column
 * @method     ChildAppMenuList findOneByTopRightMenuEnable(int $topRightMenuEnable) Return the first ChildAppMenuList filtered by the topRightMenuEnable column
 * @method     ChildAppMenuList findOneByTutorialImage(string $tutorialImage) Return the first ChildAppMenuList filtered by the tutorialImage column
 * @method     ChildAppMenuList findOneByTutorialEnable(int $tutorialEnable) Return the first ChildAppMenuList filtered by the tutorialEnable column
 * @method     ChildAppMenuList findOneByPhoneTutorialImage(string $phoneTutorialImage) Return the first ChildAppMenuList filtered by the phoneTutorialImage column
 * @method     ChildAppMenuList findOneByPhoneTutorialEnable(int $phoneTutorialEnable) Return the first ChildAppMenuList filtered by the phoneTutorialEnable column
 * @method     ChildAppMenuList findOneByPhoneIcon(string $phoneIcon) Return the first ChildAppMenuList filtered by the phoneIcon column
 * @method     ChildAppMenuList findOneByModuleType(int $module_type) Return the first ChildAppMenuList filtered by the module_type column
 * @method     ChildAppMenuList findOneByCreatedBy(string $created_by) Return the first ChildAppMenuList filtered by the created_by column
 * @method     ChildAppMenuList findOneByCreatedDate(string $created_date) Return the first ChildAppMenuList filtered by the created_date column
 * @method     ChildAppMenuList findOneByModifiedBy(string $modified_by) Return the first ChildAppMenuList filtered by the modified_by column
 * @method     ChildAppMenuList findOneByModifiedDate(string $modified_date) Return the first ChildAppMenuList filtered by the modified_date column *

 * @method     ChildAppMenuList requirePk($key, ConnectionInterface $con = null) Return the ChildAppMenuList by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOne(ConnectionInterface $con = null) Return the first ChildAppMenuList matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAppMenuList requireOneById(string $id) Return the first ChildAppMenuList filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByProjectId(string $projectId) Return the first ChildAppMenuList filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByName(string $name) Return the first ChildAppMenuList filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByDescription(string $description) Return the first ChildAppMenuList filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByImage(string $image) Return the first ChildAppMenuList filtered by the image column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneBySideMenuOrder(int $sideMenuOrder) Return the first ChildAppMenuList filtered by the sideMenuOrder column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneBySideMenuEnable(int $sideMenuEnable) Return the first ChildAppMenuList filtered by the sideMenuEnable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByHomeMenuOrder(int $homeMenuOrder) Return the first ChildAppMenuList filtered by the homeMenuOrder column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByHomeMenuEnable(int $homeMenuEnable) Return the first ChildAppMenuList filtered by the homeMenuEnable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByTopRightMenuOrder(int $topRightMenuOrder) Return the first ChildAppMenuList filtered by the topRightMenuOrder column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByTopRightMenuEnable(int $topRightMenuEnable) Return the first ChildAppMenuList filtered by the topRightMenuEnable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByTutorialImage(string $tutorialImage) Return the first ChildAppMenuList filtered by the tutorialImage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByTutorialEnable(int $tutorialEnable) Return the first ChildAppMenuList filtered by the tutorialEnable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByPhoneTutorialImage(string $phoneTutorialImage) Return the first ChildAppMenuList filtered by the phoneTutorialImage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByPhoneTutorialEnable(int $phoneTutorialEnable) Return the first ChildAppMenuList filtered by the phoneTutorialEnable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByPhoneIcon(string $phoneIcon) Return the first ChildAppMenuList filtered by the phoneIcon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByModuleType(int $module_type) Return the first ChildAppMenuList filtered by the module_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByCreatedBy(string $created_by) Return the first ChildAppMenuList filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByCreatedDate(string $created_date) Return the first ChildAppMenuList filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByModifiedBy(string $modified_by) Return the first ChildAppMenuList filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppMenuList requireOneByModifiedDate(string $modified_date) Return the first ChildAppMenuList filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAppMenuList[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAppMenuList objects based on current ModelCriteria
 * @method     ChildAppMenuList[]|ObjectCollection findById(string $id) Return ChildAppMenuList objects filtered by the id column
 * @method     ChildAppMenuList[]|ObjectCollection findByProjectId(string $projectId) Return ChildAppMenuList objects filtered by the projectId column
 * @method     ChildAppMenuList[]|ObjectCollection findByName(string $name) Return ChildAppMenuList objects filtered by the name column
 * @method     ChildAppMenuList[]|ObjectCollection findByDescription(string $description) Return ChildAppMenuList objects filtered by the description column
 * @method     ChildAppMenuList[]|ObjectCollection findByImage(string $image) Return ChildAppMenuList objects filtered by the image column
 * @method     ChildAppMenuList[]|ObjectCollection findBySideMenuOrder(int $sideMenuOrder) Return ChildAppMenuList objects filtered by the sideMenuOrder column
 * @method     ChildAppMenuList[]|ObjectCollection findBySideMenuEnable(int $sideMenuEnable) Return ChildAppMenuList objects filtered by the sideMenuEnable column
 * @method     ChildAppMenuList[]|ObjectCollection findByHomeMenuOrder(int $homeMenuOrder) Return ChildAppMenuList objects filtered by the homeMenuOrder column
 * @method     ChildAppMenuList[]|ObjectCollection findByHomeMenuEnable(int $homeMenuEnable) Return ChildAppMenuList objects filtered by the homeMenuEnable column
 * @method     ChildAppMenuList[]|ObjectCollection findByTopRightMenuOrder(int $topRightMenuOrder) Return ChildAppMenuList objects filtered by the topRightMenuOrder column
 * @method     ChildAppMenuList[]|ObjectCollection findByTopRightMenuEnable(int $topRightMenuEnable) Return ChildAppMenuList objects filtered by the topRightMenuEnable column
 * @method     ChildAppMenuList[]|ObjectCollection findByTutorialImage(string $tutorialImage) Return ChildAppMenuList objects filtered by the tutorialImage column
 * @method     ChildAppMenuList[]|ObjectCollection findByTutorialEnable(int $tutorialEnable) Return ChildAppMenuList objects filtered by the tutorialEnable column
 * @method     ChildAppMenuList[]|ObjectCollection findByPhoneTutorialImage(string $phoneTutorialImage) Return ChildAppMenuList objects filtered by the phoneTutorialImage column
 * @method     ChildAppMenuList[]|ObjectCollection findByPhoneTutorialEnable(int $phoneTutorialEnable) Return ChildAppMenuList objects filtered by the phoneTutorialEnable column
 * @method     ChildAppMenuList[]|ObjectCollection findByPhoneIcon(string $phoneIcon) Return ChildAppMenuList objects filtered by the phoneIcon column
 * @method     ChildAppMenuList[]|ObjectCollection findByModuleType(int $module_type) Return ChildAppMenuList objects filtered by the module_type column
 * @method     ChildAppMenuList[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildAppMenuList objects filtered by the created_by column
 * @method     ChildAppMenuList[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildAppMenuList objects filtered by the created_date column
 * @method     ChildAppMenuList[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildAppMenuList objects filtered by the modified_by column
 * @method     ChildAppMenuList[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildAppMenuList objects filtered by the modified_date column
 * @method     ChildAppMenuList[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AppMenuListQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AppMenuListQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\AppMenuList', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAppMenuListQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAppMenuListQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAppMenuListQuery) {
            return $criteria;
        }
        $query = new ChildAppMenuListQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAppMenuList|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AppMenuListTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AppMenuListTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAppMenuList A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, name, description, image, sideMenuOrder, sideMenuEnable, homeMenuOrder, homeMenuEnable, topRightMenuOrder, topRightMenuEnable, tutorialImage, tutorialEnable, phoneTutorialImage, phoneTutorialEnable, phoneIcon, module_type, created_by, created_date, modified_by, modified_date FROM appMenuList WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAppMenuList $obj */
            $obj = new ChildAppMenuList();
            $obj->hydrate($row);
            AppMenuListTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAppMenuList|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AppMenuListTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AppMenuListTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the image column
     *
     * Example usage:
     * <code>
     * $query->filterByImage('fooValue');   // WHERE image = 'fooValue'
     * $query->filterByImage('%fooValue%'); // WHERE image LIKE '%fooValue%'
     * </code>
     *
     * @param     string $image The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByImage($image = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($image)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $image)) {
                $image = str_replace('*', '%', $image);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_IMAGE, $image, $comparison);
    }

    /**
     * Filter the query on the sideMenuOrder column
     *
     * Example usage:
     * <code>
     * $query->filterBySideMenuOrder(1234); // WHERE sideMenuOrder = 1234
     * $query->filterBySideMenuOrder(array(12, 34)); // WHERE sideMenuOrder IN (12, 34)
     * $query->filterBySideMenuOrder(array('min' => 12)); // WHERE sideMenuOrder > 12
     * </code>
     *
     * @param     mixed $sideMenuOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterBySideMenuOrder($sideMenuOrder = null, $comparison = null)
    {
        if (is_array($sideMenuOrder)) {
            $useMinMax = false;
            if (isset($sideMenuOrder['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_SIDEMENUORDER, $sideMenuOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sideMenuOrder['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_SIDEMENUORDER, $sideMenuOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_SIDEMENUORDER, $sideMenuOrder, $comparison);
    }

    /**
     * Filter the query on the sideMenuEnable column
     *
     * Example usage:
     * <code>
     * $query->filterBySideMenuEnable(1234); // WHERE sideMenuEnable = 1234
     * $query->filterBySideMenuEnable(array(12, 34)); // WHERE sideMenuEnable IN (12, 34)
     * $query->filterBySideMenuEnable(array('min' => 12)); // WHERE sideMenuEnable > 12
     * </code>
     *
     * @param     mixed $sideMenuEnable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterBySideMenuEnable($sideMenuEnable = null, $comparison = null)
    {
        if (is_array($sideMenuEnable)) {
            $useMinMax = false;
            if (isset($sideMenuEnable['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_SIDEMENUENABLE, $sideMenuEnable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sideMenuEnable['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_SIDEMENUENABLE, $sideMenuEnable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_SIDEMENUENABLE, $sideMenuEnable, $comparison);
    }

    /**
     * Filter the query on the homeMenuOrder column
     *
     * Example usage:
     * <code>
     * $query->filterByHomeMenuOrder(1234); // WHERE homeMenuOrder = 1234
     * $query->filterByHomeMenuOrder(array(12, 34)); // WHERE homeMenuOrder IN (12, 34)
     * $query->filterByHomeMenuOrder(array('min' => 12)); // WHERE homeMenuOrder > 12
     * </code>
     *
     * @param     mixed $homeMenuOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByHomeMenuOrder($homeMenuOrder = null, $comparison = null)
    {
        if (is_array($homeMenuOrder)) {
            $useMinMax = false;
            if (isset($homeMenuOrder['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_HOMEMENUORDER, $homeMenuOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($homeMenuOrder['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_HOMEMENUORDER, $homeMenuOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_HOMEMENUORDER, $homeMenuOrder, $comparison);
    }

    /**
     * Filter the query on the homeMenuEnable column
     *
     * Example usage:
     * <code>
     * $query->filterByHomeMenuEnable(1234); // WHERE homeMenuEnable = 1234
     * $query->filterByHomeMenuEnable(array(12, 34)); // WHERE homeMenuEnable IN (12, 34)
     * $query->filterByHomeMenuEnable(array('min' => 12)); // WHERE homeMenuEnable > 12
     * </code>
     *
     * @param     mixed $homeMenuEnable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByHomeMenuEnable($homeMenuEnable = null, $comparison = null)
    {
        if (is_array($homeMenuEnable)) {
            $useMinMax = false;
            if (isset($homeMenuEnable['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_HOMEMENUENABLE, $homeMenuEnable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($homeMenuEnable['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_HOMEMENUENABLE, $homeMenuEnable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_HOMEMENUENABLE, $homeMenuEnable, $comparison);
    }

    /**
     * Filter the query on the topRightMenuOrder column
     *
     * Example usage:
     * <code>
     * $query->filterByTopRightMenuOrder(1234); // WHERE topRightMenuOrder = 1234
     * $query->filterByTopRightMenuOrder(array(12, 34)); // WHERE topRightMenuOrder IN (12, 34)
     * $query->filterByTopRightMenuOrder(array('min' => 12)); // WHERE topRightMenuOrder > 12
     * </code>
     *
     * @param     mixed $topRightMenuOrder The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByTopRightMenuOrder($topRightMenuOrder = null, $comparison = null)
    {
        if (is_array($topRightMenuOrder)) {
            $useMinMax = false;
            if (isset($topRightMenuOrder['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_TOPRIGHTMENUORDER, $topRightMenuOrder['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($topRightMenuOrder['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_TOPRIGHTMENUORDER, $topRightMenuOrder['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_TOPRIGHTMENUORDER, $topRightMenuOrder, $comparison);
    }

    /**
     * Filter the query on the topRightMenuEnable column
     *
     * Example usage:
     * <code>
     * $query->filterByTopRightMenuEnable(1234); // WHERE topRightMenuEnable = 1234
     * $query->filterByTopRightMenuEnable(array(12, 34)); // WHERE topRightMenuEnable IN (12, 34)
     * $query->filterByTopRightMenuEnable(array('min' => 12)); // WHERE topRightMenuEnable > 12
     * </code>
     *
     * @param     mixed $topRightMenuEnable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByTopRightMenuEnable($topRightMenuEnable = null, $comparison = null)
    {
        if (is_array($topRightMenuEnable)) {
            $useMinMax = false;
            if (isset($topRightMenuEnable['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_TOPRIGHTMENUENABLE, $topRightMenuEnable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($topRightMenuEnable['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_TOPRIGHTMENUENABLE, $topRightMenuEnable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_TOPRIGHTMENUENABLE, $topRightMenuEnable, $comparison);
    }

    /**
     * Filter the query on the tutorialImage column
     *
     * Example usage:
     * <code>
     * $query->filterByTutorialImage('fooValue');   // WHERE tutorialImage = 'fooValue'
     * $query->filterByTutorialImage('%fooValue%'); // WHERE tutorialImage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tutorialImage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByTutorialImage($tutorialImage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tutorialImage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tutorialImage)) {
                $tutorialImage = str_replace('*', '%', $tutorialImage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_TUTORIALIMAGE, $tutorialImage, $comparison);
    }

    /**
     * Filter the query on the tutorialEnable column
     *
     * Example usage:
     * <code>
     * $query->filterByTutorialEnable(1234); // WHERE tutorialEnable = 1234
     * $query->filterByTutorialEnable(array(12, 34)); // WHERE tutorialEnable IN (12, 34)
     * $query->filterByTutorialEnable(array('min' => 12)); // WHERE tutorialEnable > 12
     * </code>
     *
     * @param     mixed $tutorialEnable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByTutorialEnable($tutorialEnable = null, $comparison = null)
    {
        if (is_array($tutorialEnable)) {
            $useMinMax = false;
            if (isset($tutorialEnable['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_TUTORIALENABLE, $tutorialEnable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tutorialEnable['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_TUTORIALENABLE, $tutorialEnable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_TUTORIALENABLE, $tutorialEnable, $comparison);
    }

    /**
     * Filter the query on the phoneTutorialImage column
     *
     * Example usage:
     * <code>
     * $query->filterByPhoneTutorialImage('fooValue');   // WHERE phoneTutorialImage = 'fooValue'
     * $query->filterByPhoneTutorialImage('%fooValue%'); // WHERE phoneTutorialImage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phoneTutorialImage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByPhoneTutorialImage($phoneTutorialImage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phoneTutorialImage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phoneTutorialImage)) {
                $phoneTutorialImage = str_replace('*', '%', $phoneTutorialImage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_PHONETUTORIALIMAGE, $phoneTutorialImage, $comparison);
    }

    /**
     * Filter the query on the phoneTutorialEnable column
     *
     * Example usage:
     * <code>
     * $query->filterByPhoneTutorialEnable(1234); // WHERE phoneTutorialEnable = 1234
     * $query->filterByPhoneTutorialEnable(array(12, 34)); // WHERE phoneTutorialEnable IN (12, 34)
     * $query->filterByPhoneTutorialEnable(array('min' => 12)); // WHERE phoneTutorialEnable > 12
     * </code>
     *
     * @param     mixed $phoneTutorialEnable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByPhoneTutorialEnable($phoneTutorialEnable = null, $comparison = null)
    {
        if (is_array($phoneTutorialEnable)) {
            $useMinMax = false;
            if (isset($phoneTutorialEnable['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_PHONETUTORIALENABLE, $phoneTutorialEnable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($phoneTutorialEnable['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_PHONETUTORIALENABLE, $phoneTutorialEnable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_PHONETUTORIALENABLE, $phoneTutorialEnable, $comparison);
    }

    /**
     * Filter the query on the phoneIcon column
     *
     * Example usage:
     * <code>
     * $query->filterByPhoneIcon('fooValue');   // WHERE phoneIcon = 'fooValue'
     * $query->filterByPhoneIcon('%fooValue%'); // WHERE phoneIcon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phoneIcon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByPhoneIcon($phoneIcon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phoneIcon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $phoneIcon)) {
                $phoneIcon = str_replace('*', '%', $phoneIcon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_PHONEICON, $phoneIcon, $comparison);
    }

    /**
     * Filter the query on the module_type column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleType(1234); // WHERE module_type = 1234
     * $query->filterByModuleType(array(12, 34)); // WHERE module_type IN (12, 34)
     * $query->filterByModuleType(array('min' => 12)); // WHERE module_type > 12
     * </code>
     *
     * @param     mixed $moduleType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByModuleType($moduleType = null, $comparison = null)
    {
        if (is_array($moduleType)) {
            $useMinMax = false;
            if (isset($moduleType['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_MODULE_TYPE, $moduleType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($moduleType['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_MODULE_TYPE, $moduleType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_MODULE_TYPE, $moduleType, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(AppMenuListTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppMenuListTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAppMenuList $appMenuList Object to remove from the list of results
     *
     * @return $this|ChildAppMenuListQuery The current query, for fluid interface
     */
    public function prune($appMenuList = null)
    {
        if ($appMenuList) {
            $this->addUsingAlias(AppMenuListTableMap::COL_ID, $appMenuList->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the appMenuList table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppMenuListTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AppMenuListTableMap::clearInstancePool();
            AppMenuListTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppMenuListTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AppMenuListTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AppMenuListTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AppMenuListTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AppMenuListQuery
