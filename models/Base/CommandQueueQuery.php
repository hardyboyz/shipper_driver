<?php

namespace Base;

use \CommandQueue as ChildCommandQueue;
use \CommandQueueQuery as ChildCommandQueueQuery;
use \Exception;
use \PDO;
use Map\CommandQueueTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'command_queue' table.
 *
 *
 *
 * @method     ChildCommandQueueQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildCommandQueueQuery orderByPanelId($order = Criteria::ASC) Order by the panel_id column
 * @method     ChildCommandQueueQuery orderByAction($order = Criteria::ASC) Order by the action column
 * @method     ChildCommandQueueQuery orderByToken($order = Criteria::ASC) Order by the token column
 * @method     ChildCommandQueueQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildCommandQueueQuery orderByDescription($order = Criteria::ASC) Order by the description column
 *
 * @method     ChildCommandQueueQuery groupByProjectId() Group by the project_id column
 * @method     ChildCommandQueueQuery groupByPanelId() Group by the panel_id column
 * @method     ChildCommandQueueQuery groupByAction() Group by the action column
 * @method     ChildCommandQueueQuery groupByToken() Group by the token column
 * @method     ChildCommandQueueQuery groupByStatus() Group by the status column
 * @method     ChildCommandQueueQuery groupByDescription() Group by the description column
 *
 * @method     ChildCommandQueueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCommandQueueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCommandQueueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCommandQueueQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCommandQueueQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCommandQueueQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCommandQueue findOne(ConnectionInterface $con = null) Return the first ChildCommandQueue matching the query
 * @method     ChildCommandQueue findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCommandQueue matching the query, or a new ChildCommandQueue object populated from the query conditions when no match is found
 *
 * @method     ChildCommandQueue findOneByProjectId(string $project_id) Return the first ChildCommandQueue filtered by the project_id column
 * @method     ChildCommandQueue findOneByPanelId(int $panel_id) Return the first ChildCommandQueue filtered by the panel_id column
 * @method     ChildCommandQueue findOneByAction(string $action) Return the first ChildCommandQueue filtered by the action column
 * @method     ChildCommandQueue findOneByToken(string $token) Return the first ChildCommandQueue filtered by the token column
 * @method     ChildCommandQueue findOneByStatus(string $status) Return the first ChildCommandQueue filtered by the status column
 * @method     ChildCommandQueue findOneByDescription(string $description) Return the first ChildCommandQueue filtered by the description column *

 * @method     ChildCommandQueue requirePk($key, ConnectionInterface $con = null) Return the ChildCommandQueue by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommandQueue requireOne(ConnectionInterface $con = null) Return the first ChildCommandQueue matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommandQueue requireOneByProjectId(string $project_id) Return the first ChildCommandQueue filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommandQueue requireOneByPanelId(int $panel_id) Return the first ChildCommandQueue filtered by the panel_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommandQueue requireOneByAction(string $action) Return the first ChildCommandQueue filtered by the action column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommandQueue requireOneByToken(string $token) Return the first ChildCommandQueue filtered by the token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommandQueue requireOneByStatus(string $status) Return the first ChildCommandQueue filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommandQueue requireOneByDescription(string $description) Return the first ChildCommandQueue filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommandQueue[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCommandQueue objects based on current ModelCriteria
 * @method     ChildCommandQueue[]|ObjectCollection findByProjectId(string $project_id) Return ChildCommandQueue objects filtered by the project_id column
 * @method     ChildCommandQueue[]|ObjectCollection findByPanelId(int $panel_id) Return ChildCommandQueue objects filtered by the panel_id column
 * @method     ChildCommandQueue[]|ObjectCollection findByAction(string $action) Return ChildCommandQueue objects filtered by the action column
 * @method     ChildCommandQueue[]|ObjectCollection findByToken(string $token) Return ChildCommandQueue objects filtered by the token column
 * @method     ChildCommandQueue[]|ObjectCollection findByStatus(string $status) Return ChildCommandQueue objects filtered by the status column
 * @method     ChildCommandQueue[]|ObjectCollection findByDescription(string $description) Return ChildCommandQueue objects filtered by the description column
 * @method     ChildCommandQueue[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CommandQueueQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CommandQueueQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\CommandQueue', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCommandQueueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCommandQueueQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCommandQueueQuery) {
            return $criteria;
        }
        $query = new ChildCommandQueueQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCommandQueue|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommandQueueTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CommandQueueTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCommandQueue A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT project_id, panel_id, action, token, status, description FROM command_queue WHERE action = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCommandQueue $obj */
            $obj = new ChildCommandQueue();
            $obj->hydrate($row);
            CommandQueueTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCommandQueue|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCommandQueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommandQueueTableMap::COL_ACTION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCommandQueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommandQueueTableMap::COL_ACTION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommandQueueQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommandQueueTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the panel_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPanelId(1234); // WHERE panel_id = 1234
     * $query->filterByPanelId(array(12, 34)); // WHERE panel_id IN (12, 34)
     * $query->filterByPanelId(array('min' => 12)); // WHERE panel_id > 12
     * </code>
     *
     * @param     mixed $panelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommandQueueQuery The current query, for fluid interface
     */
    public function filterByPanelId($panelId = null, $comparison = null)
    {
        if (is_array($panelId)) {
            $useMinMax = false;
            if (isset($panelId['min'])) {
                $this->addUsingAlias(CommandQueueTableMap::COL_PANEL_ID, $panelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panelId['max'])) {
                $this->addUsingAlias(CommandQueueTableMap::COL_PANEL_ID, $panelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommandQueueTableMap::COL_PANEL_ID, $panelId, $comparison);
    }

    /**
     * Filter the query on the action column
     *
     * Example usage:
     * <code>
     * $query->filterByAction('fooValue');   // WHERE action = 'fooValue'
     * $query->filterByAction('%fooValue%'); // WHERE action LIKE '%fooValue%'
     * </code>
     *
     * @param     string $action The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommandQueueQuery The current query, for fluid interface
     */
    public function filterByAction($action = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($action)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $action)) {
                $action = str_replace('*', '%', $action);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommandQueueTableMap::COL_ACTION, $action, $comparison);
    }

    /**
     * Filter the query on the token column
     *
     * Example usage:
     * <code>
     * $query->filterByToken('fooValue');   // WHERE token = 'fooValue'
     * $query->filterByToken('%fooValue%'); // WHERE token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $token The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommandQueueQuery The current query, for fluid interface
     */
    public function filterByToken($token = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($token)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $token)) {
                $token = str_replace('*', '%', $token);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommandQueueTableMap::COL_TOKEN, $token, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommandQueueQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommandQueueTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommandQueueQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommandQueueTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCommandQueue $commandQueue Object to remove from the list of results
     *
     * @return $this|ChildCommandQueueQuery The current query, for fluid interface
     */
    public function prune($commandQueue = null)
    {
        if ($commandQueue) {
            $this->addUsingAlias(CommandQueueTableMap::COL_ACTION, $commandQueue->getAction(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the command_queue table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommandQueueTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommandQueueTableMap::clearInstancePool();
            CommandQueueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommandQueueTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CommandQueueTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CommandQueueTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CommandQueueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CommandQueueQuery
