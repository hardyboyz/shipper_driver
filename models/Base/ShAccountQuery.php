<?php

namespace Base;

use \ShAccount as ChildShAccount;
use \ShAccountQuery as ChildShAccountQuery;
use \Exception;
use \PDO;
use Map\ShAccountTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sh_account' table.
 *
 * 
 *
 * @method     ChildShAccountQuery orderByID($order = Criteria::ASC) Order by the id column
 * @method     ChildShAccountQuery orderByPhoto($order = Criteria::ASC) Order by the photo column
 * @method     ChildShAccountQuery orderByDob($order = Criteria::ASC) Order by the dob column
 * @method     ChildShAccountQuery orderByFullName($order = Criteria::ASC) Order by the fullName column
 * @method     ChildShAccountQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildShAccountQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildShAccountQuery orderByAddress($order = Criteria::ASC) Order by the address column
 * @method     ChildShAccountQuery orderByIdcardNumber($order = Criteria::ASC) Order by the idcardnumber column
 * @method     ChildShAccountQuery orderByHashPassword($order = Criteria::ASC) Order by the hashPassword column
 * @method     ChildShAccountQuery orderBySalt($order = Criteria::ASC) Order by the salt column
 * @method     ChildShAccountQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildShAccountQuery orderByCreateTS($order = Criteria::ASC) Order by the createTS column
 * @method     ChildShAccountQuery orderByPushID($order = Criteria::ASC) Order by the pushID column
 * @method     ChildShAccountQuery orderByDeviceID($order = Criteria::ASC) Order by the deviceID column
 * @method     ChildShAccountQuery orderByEnablePN($order = Criteria::ASC) Order by the enable_pn column
 * @method     ChildShAccountQuery orderByLoginTs($order = Criteria::ASC) Order by the login_ts column
 * @method     ChildShAccountQuery orderByExpireTs($order = Criteria::ASC) Order by the expire_ts column
 * @method     ChildShAccountQuery orderByToken($order = Criteria::ASC) Order by the token column
 * @method     ChildShAccountQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildShAccountQuery groupByID() Group by the id column
 * @method     ChildShAccountQuery groupByPhoto() Group by the photo column
 * @method     ChildShAccountQuery groupByDob() Group by the dob column
 * @method     ChildShAccountQuery groupByFullName() Group by the fullName column
 * @method     ChildShAccountQuery groupByPhone() Group by the phone column
 * @method     ChildShAccountQuery groupByEmail() Group by the email column
 * @method     ChildShAccountQuery groupByAddress() Group by the address column
 * @method     ChildShAccountQuery groupByIdcardNumber() Group by the idcardnumber column
 * @method     ChildShAccountQuery groupByHashPassword() Group by the hashPassword column
 * @method     ChildShAccountQuery groupBySalt() Group by the salt column
 * @method     ChildShAccountQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildShAccountQuery groupByCreateTS() Group by the createTS column
 * @method     ChildShAccountQuery groupByPushID() Group by the pushID column
 * @method     ChildShAccountQuery groupByDeviceID() Group by the deviceID column
 * @method     ChildShAccountQuery groupByEnablePN() Group by the enable_pn column
 * @method     ChildShAccountQuery groupByLoginTs() Group by the login_ts column
 * @method     ChildShAccountQuery groupByExpireTs() Group by the expire_ts column
 * @method     ChildShAccountQuery groupByToken() Group by the token column
 * @method     ChildShAccountQuery groupByStatus() Group by the status column
 *
 * @method     ChildShAccountQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildShAccountQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildShAccountQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildShAccountQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildShAccountQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildShAccountQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildShAccount findOne(ConnectionInterface $con = null) Return the first ChildShAccount matching the query
 * @method     ChildShAccount findOneOrCreate(ConnectionInterface $con = null) Return the first ChildShAccount matching the query, or a new ChildShAccount object populated from the query conditions when no match is found
 *
 * @method     ChildShAccount findOneByID(string $id) Return the first ChildShAccount filtered by the id column
 * @method     ChildShAccount findOneByPhoto(string $photo) Return the first ChildShAccount filtered by the photo column
 * @method     ChildShAccount findOneByDob(string $dob) Return the first ChildShAccount filtered by the dob column
 * @method     ChildShAccount findOneByFullName(string $fullName) Return the first ChildShAccount filtered by the fullName column
 * @method     ChildShAccount findOneByPhone(string $phone) Return the first ChildShAccount filtered by the phone column
 * @method     ChildShAccount findOneByEmail(string $email) Return the first ChildShAccount filtered by the email column
 * @method     ChildShAccount findOneByAddress(string $address) Return the first ChildShAccount filtered by the address column
 * @method     ChildShAccount findOneByIdcardNumber(string $idcardnumber) Return the first ChildShAccount filtered by the idcardnumber column
 * @method     ChildShAccount findOneByHashPassword(string $hashPassword) Return the first ChildShAccount filtered by the hashPassword column
 * @method     ChildShAccount findOneBySalt(string $salt) Return the first ChildShAccount filtered by the salt column
 * @method     ChildShAccount findOneByCreatedBy(string $created_by) Return the first ChildShAccount filtered by the created_by column
 * @method     ChildShAccount findOneByCreateTS(string $createTS) Return the first ChildShAccount filtered by the createTS column
 * @method     ChildShAccount findOneByPushID(string $pushID) Return the first ChildShAccount filtered by the pushID column
 * @method     ChildShAccount findOneByDeviceID(string $deviceID) Return the first ChildShAccount filtered by the deviceID column
 * @method     ChildShAccount findOneByEnablePN(int $enable_pn) Return the first ChildShAccount filtered by the enable_pn column
 * @method     ChildShAccount findOneByLoginTs(string $login_ts) Return the first ChildShAccount filtered by the login_ts column
 * @method     ChildShAccount findOneByExpireTs(string $expire_ts) Return the first ChildShAccount filtered by the expire_ts column
 * @method     ChildShAccount findOneByToken(string $token) Return the first ChildShAccount filtered by the token column
 * @method     ChildShAccount findOneByStatus(int $status) Return the first ChildShAccount filtered by the status column *

 * @method     ChildShAccount requirePk($key, ConnectionInterface $con = null) Return the ChildShAccount by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOne(ConnectionInterface $con = null) Return the first ChildShAccount matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildShAccount requireOneByID(string $id) Return the first ChildShAccount filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByPhoto(string $photo) Return the first ChildShAccount filtered by the photo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByDob(string $dob) Return the first ChildShAccount filtered by the dob column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByFullName(string $fullName) Return the first ChildShAccount filtered by the fullName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByPhone(string $phone) Return the first ChildShAccount filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByEmail(string $email) Return the first ChildShAccount filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByAddress(string $address) Return the first ChildShAccount filtered by the address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByIdcardNumber(string $idcardnumber) Return the first ChildShAccount filtered by the idcardnumber column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByHashPassword(string $hashPassword) Return the first ChildShAccount filtered by the hashPassword column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneBySalt(string $salt) Return the first ChildShAccount filtered by the salt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByCreatedBy(string $created_by) Return the first ChildShAccount filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByCreateTS(string $createTS) Return the first ChildShAccount filtered by the createTS column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByPushID(string $pushID) Return the first ChildShAccount filtered by the pushID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByDeviceID(string $deviceID) Return the first ChildShAccount filtered by the deviceID column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByEnablePN(int $enable_pn) Return the first ChildShAccount filtered by the enable_pn column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByLoginTs(string $login_ts) Return the first ChildShAccount filtered by the login_ts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByExpireTs(string $expire_ts) Return the first ChildShAccount filtered by the expire_ts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByToken(string $token) Return the first ChildShAccount filtered by the token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShAccount requireOneByStatus(int $status) Return the first ChildShAccount filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildShAccount[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildShAccount objects based on current ModelCriteria
 * @method     ChildShAccount[]|ObjectCollection findByID(string $id) Return ChildShAccount objects filtered by the id column
 * @method     ChildShAccount[]|ObjectCollection findByPhoto(string $photo) Return ChildShAccount objects filtered by the photo column
 * @method     ChildShAccount[]|ObjectCollection findByDob(string $dob) Return ChildShAccount objects filtered by the dob column
 * @method     ChildShAccount[]|ObjectCollection findByFullName(string $fullName) Return ChildShAccount objects filtered by the fullName column
 * @method     ChildShAccount[]|ObjectCollection findByPhone(string $phone) Return ChildShAccount objects filtered by the phone column
 * @method     ChildShAccount[]|ObjectCollection findByEmail(string $email) Return ChildShAccount objects filtered by the email column
 * @method     ChildShAccount[]|ObjectCollection findByAddress(string $address) Return ChildShAccount objects filtered by the address column
 * @method     ChildShAccount[]|ObjectCollection findByIdcardNumber(string $idcardnumber) Return ChildShAccount objects filtered by the idcardnumber column
 * @method     ChildShAccount[]|ObjectCollection findByHashPassword(string $hashPassword) Return ChildShAccount objects filtered by the hashPassword column
 * @method     ChildShAccount[]|ObjectCollection findBySalt(string $salt) Return ChildShAccount objects filtered by the salt column
 * @method     ChildShAccount[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildShAccount objects filtered by the created_by column
 * @method     ChildShAccount[]|ObjectCollection findByCreateTS(string $createTS) Return ChildShAccount objects filtered by the createTS column
 * @method     ChildShAccount[]|ObjectCollection findByPushID(string $pushID) Return ChildShAccount objects filtered by the pushID column
 * @method     ChildShAccount[]|ObjectCollection findByDeviceID(string $deviceID) Return ChildShAccount objects filtered by the deviceID column
 * @method     ChildShAccount[]|ObjectCollection findByEnablePN(int $enable_pn) Return ChildShAccount objects filtered by the enable_pn column
 * @method     ChildShAccount[]|ObjectCollection findByLoginTs(string $login_ts) Return ChildShAccount objects filtered by the login_ts column
 * @method     ChildShAccount[]|ObjectCollection findByExpireTs(string $expire_ts) Return ChildShAccount objects filtered by the expire_ts column
 * @method     ChildShAccount[]|ObjectCollection findByToken(string $token) Return ChildShAccount objects filtered by the token column
 * @method     ChildShAccount[]|ObjectCollection findByStatus(int $status) Return ChildShAccount objects filtered by the status column
 * @method     ChildShAccount[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ShAccountQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ShAccountQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ShAccount', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildShAccountQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildShAccountQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildShAccountQuery) {
            return $criteria;
        }
        $query = new ChildShAccountQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildShAccount|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ShAccountTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ShAccountTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildShAccount A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, photo, dob, fullName, phone, email, address, idcardnumber, hashPassword, salt, created_by, createTS, pushID, deviceID, enable_pn, login_ts, expire_ts, token, status FROM sh_account WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildShAccount $obj */
            $obj = new ChildShAccount();
            $obj->hydrate($row);
            ShAccountTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildShAccount|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ShAccountTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ShAccountTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByID('fooValue');   // WHERE id = 'fooValue'
     * $query->filterByID('%fooValue%', Criteria::LIKE); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iD The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByID($iD = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iD)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_ID, $iD, $comparison);
    }

    /**
     * Filter the query on the photo column
     *
     * Example usage:
     * <code>
     * $query->filterByPhoto('fooValue');   // WHERE photo = 'fooValue'
     * $query->filterByPhoto('%fooValue%', Criteria::LIKE); // WHERE photo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $photo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByPhoto($photo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($photo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_PHOTO, $photo, $comparison);
    }

    /**
     * Filter the query on the dob column
     *
     * Example usage:
     * <code>
     * $query->filterByDob('fooValue');   // WHERE dob = 'fooValue'
     * $query->filterByDob('%fooValue%', Criteria::LIKE); // WHERE dob LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dob The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByDob($dob = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dob)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_DOB, $dob, $comparison);
    }

    /**
     * Filter the query on the fullName column
     *
     * Example usage:
     * <code>
     * $query->filterByFullName('fooValue');   // WHERE fullName = 'fooValue'
     * $query->filterByFullName('%fooValue%', Criteria::LIKE); // WHERE fullName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fullName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByFullName($fullName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fullName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_FULLNAME, $fullName, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the address column
     *
     * Example usage:
     * <code>
     * $query->filterByAddress('fooValue');   // WHERE address = 'fooValue'
     * $query->filterByAddress('%fooValue%', Criteria::LIKE); // WHERE address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $address The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByAddress($address = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($address)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_ADDRESS, $address, $comparison);
    }

    /**
     * Filter the query on the idcardnumber column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcardNumber('fooValue');   // WHERE idcardnumber = 'fooValue'
     * $query->filterByIdcardNumber('%fooValue%', Criteria::LIKE); // WHERE idcardnumber LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idcardNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByIdcardNumber($idcardNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idcardNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_IDCARDNUMBER, $idcardNumber, $comparison);
    }

    /**
     * Filter the query on the hashPassword column
     *
     * Example usage:
     * <code>
     * $query->filterByHashPassword('fooValue');   // WHERE hashPassword = 'fooValue'
     * $query->filterByHashPassword('%fooValue%', Criteria::LIKE); // WHERE hashPassword LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hashPassword The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByHashPassword($hashPassword = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hashPassword)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_HASHPASSWORD, $hashPassword, $comparison);
    }

    /**
     * Filter the query on the salt column
     *
     * Example usage:
     * <code>
     * $query->filterBySalt('fooValue');   // WHERE salt = 'fooValue'
     * $query->filterBySalt('%fooValue%', Criteria::LIKE); // WHERE salt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterBySalt($salt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_SALT, $salt, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%', Criteria::LIKE); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the createTS column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateTS('2011-03-14'); // WHERE createTS = '2011-03-14'
     * $query->filterByCreateTS('now'); // WHERE createTS = '2011-03-14'
     * $query->filterByCreateTS(array('max' => 'yesterday')); // WHERE createTS > '2011-03-13'
     * </code>
     *
     * @param     mixed $createTS The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByCreateTS($createTS = null, $comparison = null)
    {
        if (is_array($createTS)) {
            $useMinMax = false;
            if (isset($createTS['min'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_CREATETS, $createTS['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createTS['max'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_CREATETS, $createTS['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_CREATETS, $createTS, $comparison);
    }

    /**
     * Filter the query on the pushID column
     *
     * Example usage:
     * <code>
     * $query->filterByPushID('fooValue');   // WHERE pushID = 'fooValue'
     * $query->filterByPushID('%fooValue%', Criteria::LIKE); // WHERE pushID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pushID The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByPushID($pushID = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pushID)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_PUSHID, $pushID, $comparison);
    }

    /**
     * Filter the query on the deviceID column
     *
     * Example usage:
     * <code>
     * $query->filterByDeviceID('fooValue');   // WHERE deviceID = 'fooValue'
     * $query->filterByDeviceID('%fooValue%', Criteria::LIKE); // WHERE deviceID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deviceID The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByDeviceID($deviceID = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deviceID)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_DEVICEID, $deviceID, $comparison);
    }

    /**
     * Filter the query on the enable_pn column
     *
     * Example usage:
     * <code>
     * $query->filterByEnablePN(1234); // WHERE enable_pn = 1234
     * $query->filterByEnablePN(array(12, 34)); // WHERE enable_pn IN (12, 34)
     * $query->filterByEnablePN(array('min' => 12)); // WHERE enable_pn > 12
     * </code>
     *
     * @param     mixed $enablePN The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByEnablePN($enablePN = null, $comparison = null)
    {
        if (is_array($enablePN)) {
            $useMinMax = false;
            if (isset($enablePN['min'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_ENABLE_PN, $enablePN['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enablePN['max'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_ENABLE_PN, $enablePN['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_ENABLE_PN, $enablePN, $comparison);
    }

    /**
     * Filter the query on the login_ts column
     *
     * Example usage:
     * <code>
     * $query->filterByLoginTs('2011-03-14'); // WHERE login_ts = '2011-03-14'
     * $query->filterByLoginTs('now'); // WHERE login_ts = '2011-03-14'
     * $query->filterByLoginTs(array('max' => 'yesterday')); // WHERE login_ts > '2011-03-13'
     * </code>
     *
     * @param     mixed $loginTs The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByLoginTs($loginTs = null, $comparison = null)
    {
        if (is_array($loginTs)) {
            $useMinMax = false;
            if (isset($loginTs['min'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_LOGIN_TS, $loginTs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($loginTs['max'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_LOGIN_TS, $loginTs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_LOGIN_TS, $loginTs, $comparison);
    }

    /**
     * Filter the query on the expire_ts column
     *
     * Example usage:
     * <code>
     * $query->filterByExpireTs('2011-03-14'); // WHERE expire_ts = '2011-03-14'
     * $query->filterByExpireTs('now'); // WHERE expire_ts = '2011-03-14'
     * $query->filterByExpireTs(array('max' => 'yesterday')); // WHERE expire_ts > '2011-03-13'
     * </code>
     *
     * @param     mixed $expireTs The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByExpireTs($expireTs = null, $comparison = null)
    {
        if (is_array($expireTs)) {
            $useMinMax = false;
            if (isset($expireTs['min'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_EXPIRE_TS, $expireTs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expireTs['max'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_EXPIRE_TS, $expireTs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_EXPIRE_TS, $expireTs, $comparison);
    }

    /**
     * Filter the query on the token column
     *
     * Example usage:
     * <code>
     * $query->filterByToken('fooValue');   // WHERE token = 'fooValue'
     * $query->filterByToken('%fooValue%', Criteria::LIKE); // WHERE token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $token The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByToken($token = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($token)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_TOKEN, $token, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(ShAccountTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShAccountTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildShAccount $shAccount Object to remove from the list of results
     *
     * @return $this|ChildShAccountQuery The current query, for fluid interface
     */
    public function prune($shAccount = null)
    {
        if ($shAccount) {
            $this->addUsingAlias(ShAccountTableMap::COL_ID, $shAccount->getID(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sh_account table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShAccountTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ShAccountTableMap::clearInstancePool();
            ShAccountTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShAccountTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ShAccountTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            ShAccountTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            ShAccountTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ShAccountQuery
