<?php

namespace Base;

use \PropertyBlock as ChildPropertyBlock;
use \PropertyBlockQuery as ChildPropertyBlockQuery;
use \Exception;
use \PDO;
use Map\PropertyBlockTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'propertyBlock' table.
 *
 *
 *
 * @method     ChildPropertyBlockQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPropertyBlockQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildPropertyBlockQuery orderByBlockNo($order = Criteria::ASC) Order by the blockNo column
 * @method     ChildPropertyBlockQuery orderByBlockLabel($order = Criteria::ASC) Order by the blockLabel column
 * @method     ChildPropertyBlockQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildPropertyBlockQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildPropertyBlockQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildPropertyBlockQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildPropertyBlockQuery groupById() Group by the id column
 * @method     ChildPropertyBlockQuery groupByProjectId() Group by the projectId column
 * @method     ChildPropertyBlockQuery groupByBlockNo() Group by the blockNo column
 * @method     ChildPropertyBlockQuery groupByBlockLabel() Group by the blockLabel column
 * @method     ChildPropertyBlockQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildPropertyBlockQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildPropertyBlockQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildPropertyBlockQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildPropertyBlockQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPropertyBlockQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPropertyBlockQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPropertyBlockQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPropertyBlockQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPropertyBlockQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPropertyBlock findOne(ConnectionInterface $con = null) Return the first ChildPropertyBlock matching the query
 * @method     ChildPropertyBlock findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPropertyBlock matching the query, or a new ChildPropertyBlock object populated from the query conditions when no match is found
 *
 * @method     ChildPropertyBlock findOneById(string $id) Return the first ChildPropertyBlock filtered by the id column
 * @method     ChildPropertyBlock findOneByProjectId(string $projectId) Return the first ChildPropertyBlock filtered by the projectId column
 * @method     ChildPropertyBlock findOneByBlockNo(string $blockNo) Return the first ChildPropertyBlock filtered by the blockNo column
 * @method     ChildPropertyBlock findOneByBlockLabel(string $blockLabel) Return the first ChildPropertyBlock filtered by the blockLabel column
 * @method     ChildPropertyBlock findOneByCreatedBy(string $created_by) Return the first ChildPropertyBlock filtered by the created_by column
 * @method     ChildPropertyBlock findOneByCreatedDate(string $created_date) Return the first ChildPropertyBlock filtered by the created_date column
 * @method     ChildPropertyBlock findOneByModifiedBy(string $modified_by) Return the first ChildPropertyBlock filtered by the modified_by column
 * @method     ChildPropertyBlock findOneByModifiedDate(string $modified_date) Return the first ChildPropertyBlock filtered by the modified_date column *

 * @method     ChildPropertyBlock requirePk($key, ConnectionInterface $con = null) Return the ChildPropertyBlock by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyBlock requireOne(ConnectionInterface $con = null) Return the first ChildPropertyBlock matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPropertyBlock requireOneById(string $id) Return the first ChildPropertyBlock filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyBlock requireOneByProjectId(string $projectId) Return the first ChildPropertyBlock filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyBlock requireOneByBlockNo(string $blockNo) Return the first ChildPropertyBlock filtered by the blockNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyBlock requireOneByBlockLabel(string $blockLabel) Return the first ChildPropertyBlock filtered by the blockLabel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyBlock requireOneByCreatedBy(string $created_by) Return the first ChildPropertyBlock filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyBlock requireOneByCreatedDate(string $created_date) Return the first ChildPropertyBlock filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyBlock requireOneByModifiedBy(string $modified_by) Return the first ChildPropertyBlock filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyBlock requireOneByModifiedDate(string $modified_date) Return the first ChildPropertyBlock filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPropertyBlock[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPropertyBlock objects based on current ModelCriteria
 * @method     ChildPropertyBlock[]|ObjectCollection findById(string $id) Return ChildPropertyBlock objects filtered by the id column
 * @method     ChildPropertyBlock[]|ObjectCollection findByProjectId(string $projectId) Return ChildPropertyBlock objects filtered by the projectId column
 * @method     ChildPropertyBlock[]|ObjectCollection findByBlockNo(string $blockNo) Return ChildPropertyBlock objects filtered by the blockNo column
 * @method     ChildPropertyBlock[]|ObjectCollection findByBlockLabel(string $blockLabel) Return ChildPropertyBlock objects filtered by the blockLabel column
 * @method     ChildPropertyBlock[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildPropertyBlock objects filtered by the created_by column
 * @method     ChildPropertyBlock[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildPropertyBlock objects filtered by the created_date column
 * @method     ChildPropertyBlock[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildPropertyBlock objects filtered by the modified_by column
 * @method     ChildPropertyBlock[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildPropertyBlock objects filtered by the modified_date column
 * @method     ChildPropertyBlock[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PropertyBlockQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PropertyBlockQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PropertyBlock', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPropertyBlockQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPropertyBlockQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPropertyBlockQuery) {
            return $criteria;
        }
        $query = new ChildPropertyBlockQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPropertyBlock|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PropertyBlockTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PropertyBlockTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPropertyBlock A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, blockNo, blockLabel, created_by, created_date, modified_by, modified_date FROM propertyBlock WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPropertyBlock $obj */
            $obj = new ChildPropertyBlock();
            $obj->hydrate($row);
            PropertyBlockTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPropertyBlock|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PropertyBlockTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PropertyBlockTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyBlockTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyBlockTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the blockNo column
     *
     * Example usage:
     * <code>
     * $query->filterByBlockNo('fooValue');   // WHERE blockNo = 'fooValue'
     * $query->filterByBlockNo('%fooValue%'); // WHERE blockNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $blockNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterByBlockNo($blockNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($blockNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $blockNo)) {
                $blockNo = str_replace('*', '%', $blockNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyBlockTableMap::COL_BLOCKNO, $blockNo, $comparison);
    }

    /**
     * Filter the query on the blockLabel column
     *
     * Example usage:
     * <code>
     * $query->filterByBlockLabel('fooValue');   // WHERE blockLabel = 'fooValue'
     * $query->filterByBlockLabel('%fooValue%'); // WHERE blockLabel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $blockLabel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterByBlockLabel($blockLabel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($blockLabel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $blockLabel)) {
                $blockLabel = str_replace('*', '%', $blockLabel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyBlockTableMap::COL_BLOCKLABEL, $blockLabel, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyBlockTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(PropertyBlockTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(PropertyBlockTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyBlockTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyBlockTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(PropertyBlockTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(PropertyBlockTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyBlockTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPropertyBlock $propertyBlock Object to remove from the list of results
     *
     * @return $this|ChildPropertyBlockQuery The current query, for fluid interface
     */
    public function prune($propertyBlock = null)
    {
        if ($propertyBlock) {
            $this->addUsingAlias(PropertyBlockTableMap::COL_ID, $propertyBlock->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the propertyBlock table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyBlockTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PropertyBlockTableMap::clearInstancePool();
            PropertyBlockTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyBlockTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PropertyBlockTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PropertyBlockTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PropertyBlockTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PropertyBlockQuery
