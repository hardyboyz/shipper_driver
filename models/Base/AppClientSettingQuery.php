<?php

namespace Base;

use \AppClientSetting as ChildAppClientSetting;
use \AppClientSettingQuery as ChildAppClientSettingQuery;
use \Exception;
use \PDO;
use Map\AppClientSettingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'appClientSetting' table.
 *
 *
 *
 * @method     ChildAppClientSettingQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildAppClientSettingQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildAppClientSettingQuery orderByAppHomeIdleTime($order = Criteria::ASC) Order by the appHomeIdleTime column
 * @method     ChildAppClientSettingQuery orderByAppStandbyIdleTime($order = Criteria::ASC) Order by the appStandbyIdleTime column
 * @method     ChildAppClientSettingQuery orderByAppHomeLogoImage($order = Criteria::ASC) Order by the appHomeLogoImage column
 * @method     ChildAppClientSettingQuery orderByReceiptLogoImage($order = Criteria::ASC) Order by the receiptLogoImage column
 * @method     ChildAppClientSettingQuery orderByFacilityCheckingOnlyFlag($order = Criteria::ASC) Order by the facilityCheckingOnlyFlag column
 * @method     ChildAppClientSettingQuery orderByCallingFormatPattern($order = Criteria::ASC) Order by the callingFormatPattern column
 * @method     ChildAppClientSettingQuery orderByinterFacilityFlag($order = Criteria::ASC) Order by the interFacilityFlag column
 * @method     ChildAppClientSettingQuery orderByProjectName($order = Criteria::ASC) Order by the projectName column
 * @method     ChildAppClientSettingQuery orderByProjectNameId($order = Criteria::ASC) Order by the projectNameId column
 * @method     ChildAppClientSettingQuery orderByCentralizedServerUrl($order = Criteria::ASC) Order by the centralizedServerUrl column
 * @method     ChildAppClientSettingQuery orderByRemoteServerDomain($order = Criteria::ASC) Order by the remoteServerDomain column
 * @method     ChildAppClientSettingQuery orderByRemoteServerProtocol($order = Criteria::ASC) Order by the remoteServerProtocol column
 * @method     ChildAppClientSettingQuery orderByRemoteServerPort($order = Criteria::ASC) Order by the remoteServerPort column
 * @method     ChildAppClientSettingQuery orderByLocalServerDomain($order = Criteria::ASC) Order by the localServerDomain column
 * @method     ChildAppClientSettingQuery orderByLocalServerProtocol($order = Criteria::ASC) Order by the localServerProtocol column
 * @method     ChildAppClientSettingQuery orderByLocalServerPort($order = Criteria::ASC) Order by the localServerPort column
 * @method     ChildAppClientSettingQuery orderByPNDomain($order = Criteria::ASC) Order by the pnDomain column
 * @method     ChildAppClientSettingQuery orderByPNProtocol($order = Criteria::ASC) Order by the pnProtocol column
 * @method     ChildAppClientSettingQuery orderByPNPort($order = Criteria::ASC) Order by the pnPort column
 * @method     ChildAppClientSettingQuery orderByRemoteCctvDomain($order = Criteria::ASC) Order by the remoteCctvDomain column
 * @method     ChildAppClientSettingQuery orderByRemoteCctvProtocol($order = Criteria::ASC) Order by the remoteCctvProtocol column
 * @method     ChildAppClientSettingQuery orderByRemoteCctvPort($order = Criteria::ASC) Order by the remoteCctvPort column
 * @method     ChildAppClientSettingQuery orderByLocalCctvDomain($order = Criteria::ASC) Order by the localCctvDomain column
 * @method     ChildAppClientSettingQuery orderByLocalCctvProtocol($order = Criteria::ASC) Order by the localCctvProtocol column
 * @method     ChildAppClientSettingQuery orderByLocalCctvPort($order = Criteria::ASC) Order by the localCctvPort column
 * @method     ChildAppClientSettingQuery orderByTutorialImage($order = Criteria::ASC) Order by the tutorialImage column
 * @method     ChildAppClientSettingQuery orderByTutorialImageEnable($order = Criteria::ASC) Order by the tutorialImageEnable column
 * @method     ChildAppClientSettingQuery orderByAppAutoSyncInterval($order = Criteria::ASC) Order by the appAutoSyncInterval column
 * @method     ChildAppClientSettingQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildAppClientSettingQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildAppClientSettingQuery groupById() Group by the id column
 * @method     ChildAppClientSettingQuery groupByProjectId() Group by the projectId column
 * @method     ChildAppClientSettingQuery groupByAppHomeIdleTime() Group by the appHomeIdleTime column
 * @method     ChildAppClientSettingQuery groupByAppStandbyIdleTime() Group by the appStandbyIdleTime column
 * @method     ChildAppClientSettingQuery groupByAppHomeLogoImage() Group by the appHomeLogoImage column
 * @method     ChildAppClientSettingQuery groupByReceiptLogoImage() Group by the receiptLogoImage column
 * @method     ChildAppClientSettingQuery groupByFacilityCheckingOnlyFlag() Group by the facilityCheckingOnlyFlag column
 * @method     ChildAppClientSettingQuery groupByCallingFormatPattern() Group by the callingFormatPattern column
 * @method     ChildAppClientSettingQuery groupByinterFacilityFlag() Group by the interFacilityFlag column
 * @method     ChildAppClientSettingQuery groupByProjectName() Group by the projectName column
 * @method     ChildAppClientSettingQuery groupByProjectNameId() Group by the projectNameId column
 * @method     ChildAppClientSettingQuery groupByCentralizedServerUrl() Group by the centralizedServerUrl column
 * @method     ChildAppClientSettingQuery groupByRemoteServerDomain() Group by the remoteServerDomain column
 * @method     ChildAppClientSettingQuery groupByRemoteServerProtocol() Group by the remoteServerProtocol column
 * @method     ChildAppClientSettingQuery groupByRemoteServerPort() Group by the remoteServerPort column
 * @method     ChildAppClientSettingQuery groupByLocalServerDomain() Group by the localServerDomain column
 * @method     ChildAppClientSettingQuery groupByLocalServerProtocol() Group by the localServerProtocol column
 * @method     ChildAppClientSettingQuery groupByLocalServerPort() Group by the localServerPort column
 * @method     ChildAppClientSettingQuery groupByPNDomain() Group by the pnDomain column
 * @method     ChildAppClientSettingQuery groupByPNProtocol() Group by the pnProtocol column
 * @method     ChildAppClientSettingQuery groupByPNPort() Group by the pnPort column
 * @method     ChildAppClientSettingQuery groupByRemoteCctvDomain() Group by the remoteCctvDomain column
 * @method     ChildAppClientSettingQuery groupByRemoteCctvProtocol() Group by the remoteCctvProtocol column
 * @method     ChildAppClientSettingQuery groupByRemoteCctvPort() Group by the remoteCctvPort column
 * @method     ChildAppClientSettingQuery groupByLocalCctvDomain() Group by the localCctvDomain column
 * @method     ChildAppClientSettingQuery groupByLocalCctvProtocol() Group by the localCctvProtocol column
 * @method     ChildAppClientSettingQuery groupByLocalCctvPort() Group by the localCctvPort column
 * @method     ChildAppClientSettingQuery groupByTutorialImage() Group by the tutorialImage column
 * @method     ChildAppClientSettingQuery groupByTutorialImageEnable() Group by the tutorialImageEnable column
 * @method     ChildAppClientSettingQuery groupByAppAutoSyncInterval() Group by the appAutoSyncInterval column
 * @method     ChildAppClientSettingQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildAppClientSettingQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildAppClientSettingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAppClientSettingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAppClientSettingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAppClientSettingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAppClientSettingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAppClientSettingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAppClientSetting findOne(ConnectionInterface $con = null) Return the first ChildAppClientSetting matching the query
 * @method     ChildAppClientSetting findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAppClientSetting matching the query, or a new ChildAppClientSetting object populated from the query conditions when no match is found
 *
 * @method     ChildAppClientSetting findOneById(string $id) Return the first ChildAppClientSetting filtered by the id column
 * @method     ChildAppClientSetting findOneByProjectId(string $projectId) Return the first ChildAppClientSetting filtered by the projectId column
 * @method     ChildAppClientSetting findOneByAppHomeIdleTime(int $appHomeIdleTime) Return the first ChildAppClientSetting filtered by the appHomeIdleTime column
 * @method     ChildAppClientSetting findOneByAppStandbyIdleTime(int $appStandbyIdleTime) Return the first ChildAppClientSetting filtered by the appStandbyIdleTime column
 * @method     ChildAppClientSetting findOneByAppHomeLogoImage(string $appHomeLogoImage) Return the first ChildAppClientSetting filtered by the appHomeLogoImage column
 * @method     ChildAppClientSetting findOneByReceiptLogoImage(string $receiptLogoImage) Return the first ChildAppClientSetting filtered by the receiptLogoImage column
 * @method     ChildAppClientSetting findOneByFacilityCheckingOnlyFlag(int $facilityCheckingOnlyFlag) Return the first ChildAppClientSetting filtered by the facilityCheckingOnlyFlag column
 * @method     ChildAppClientSetting findOneByCallingFormatPattern(string $callingFormatPattern) Return the first ChildAppClientSetting filtered by the callingFormatPattern column
 * @method     ChildAppClientSetting findOneByinterFacilityFlag(int $interFacilityFlag) Return the first ChildAppClientSetting filtered by the interFacilityFlag column
 * @method     ChildAppClientSetting findOneByProjectName(string $projectName) Return the first ChildAppClientSetting filtered by the projectName column
 * @method     ChildAppClientSetting findOneByProjectNameId(string $projectNameId) Return the first ChildAppClientSetting filtered by the projectNameId column
 * @method     ChildAppClientSetting findOneByCentralizedServerUrl(string $centralizedServerUrl) Return the first ChildAppClientSetting filtered by the centralizedServerUrl column
 * @method     ChildAppClientSetting findOneByRemoteServerDomain(string $remoteServerDomain) Return the first ChildAppClientSetting filtered by the remoteServerDomain column
 * @method     ChildAppClientSetting findOneByRemoteServerProtocol(string $remoteServerProtocol) Return the first ChildAppClientSetting filtered by the remoteServerProtocol column
 * @method     ChildAppClientSetting findOneByRemoteServerPort(string $remoteServerPort) Return the first ChildAppClientSetting filtered by the remoteServerPort column
 * @method     ChildAppClientSetting findOneByLocalServerDomain(string $localServerDomain) Return the first ChildAppClientSetting filtered by the localServerDomain column
 * @method     ChildAppClientSetting findOneByLocalServerProtocol(string $localServerProtocol) Return the first ChildAppClientSetting filtered by the localServerProtocol column
 * @method     ChildAppClientSetting findOneByLocalServerPort(string $localServerPort) Return the first ChildAppClientSetting filtered by the localServerPort column
 * @method     ChildAppClientSetting findOneByPNDomain(string $pnDomain) Return the first ChildAppClientSetting filtered by the pnDomain column
 * @method     ChildAppClientSetting findOneByPNProtocol(string $pnProtocol) Return the first ChildAppClientSetting filtered by the pnProtocol column
 * @method     ChildAppClientSetting findOneByPNPort(string $pnPort) Return the first ChildAppClientSetting filtered by the pnPort column
 * @method     ChildAppClientSetting findOneByRemoteCctvDomain(string $remoteCctvDomain) Return the first ChildAppClientSetting filtered by the remoteCctvDomain column
 * @method     ChildAppClientSetting findOneByRemoteCctvProtocol(string $remoteCctvProtocol) Return the first ChildAppClientSetting filtered by the remoteCctvProtocol column
 * @method     ChildAppClientSetting findOneByRemoteCctvPort(string $remoteCctvPort) Return the first ChildAppClientSetting filtered by the remoteCctvPort column
 * @method     ChildAppClientSetting findOneByLocalCctvDomain(string $localCctvDomain) Return the first ChildAppClientSetting filtered by the localCctvDomain column
 * @method     ChildAppClientSetting findOneByLocalCctvProtocol(string $localCctvProtocol) Return the first ChildAppClientSetting filtered by the localCctvProtocol column
 * @method     ChildAppClientSetting findOneByLocalCctvPort(string $localCctvPort) Return the first ChildAppClientSetting filtered by the localCctvPort column
 * @method     ChildAppClientSetting findOneByTutorialImage(string $tutorialImage) Return the first ChildAppClientSetting filtered by the tutorialImage column
 * @method     ChildAppClientSetting findOneByTutorialImageEnable(int $tutorialImageEnable) Return the first ChildAppClientSetting filtered by the tutorialImageEnable column
 * @method     ChildAppClientSetting findOneByAppAutoSyncInterval(string $appAutoSyncInterval) Return the first ChildAppClientSetting filtered by the appAutoSyncInterval column
 * @method     ChildAppClientSetting findOneByModifiedBy(string $modified_by) Return the first ChildAppClientSetting filtered by the modified_by column
 * @method     ChildAppClientSetting findOneByModifiedDate(string $modified_date) Return the first ChildAppClientSetting filtered by the modified_date column *

 * @method     ChildAppClientSetting requirePk($key, ConnectionInterface $con = null) Return the ChildAppClientSetting by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOne(ConnectionInterface $con = null) Return the first ChildAppClientSetting matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAppClientSetting requireOneById(string $id) Return the first ChildAppClientSetting filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByProjectId(string $projectId) Return the first ChildAppClientSetting filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByAppHomeIdleTime(int $appHomeIdleTime) Return the first ChildAppClientSetting filtered by the appHomeIdleTime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByAppStandbyIdleTime(int $appStandbyIdleTime) Return the first ChildAppClientSetting filtered by the appStandbyIdleTime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByAppHomeLogoImage(string $appHomeLogoImage) Return the first ChildAppClientSetting filtered by the appHomeLogoImage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByReceiptLogoImage(string $receiptLogoImage) Return the first ChildAppClientSetting filtered by the receiptLogoImage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByFacilityCheckingOnlyFlag(int $facilityCheckingOnlyFlag) Return the first ChildAppClientSetting filtered by the facilityCheckingOnlyFlag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByCallingFormatPattern(string $callingFormatPattern) Return the first ChildAppClientSetting filtered by the callingFormatPattern column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByinterFacilityFlag(int $interFacilityFlag) Return the first ChildAppClientSetting filtered by the interFacilityFlag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByProjectName(string $projectName) Return the first ChildAppClientSetting filtered by the projectName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByProjectNameId(string $projectNameId) Return the first ChildAppClientSetting filtered by the projectNameId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByCentralizedServerUrl(string $centralizedServerUrl) Return the first ChildAppClientSetting filtered by the centralizedServerUrl column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByRemoteServerDomain(string $remoteServerDomain) Return the first ChildAppClientSetting filtered by the remoteServerDomain column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByRemoteServerProtocol(string $remoteServerProtocol) Return the first ChildAppClientSetting filtered by the remoteServerProtocol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByRemoteServerPort(string $remoteServerPort) Return the first ChildAppClientSetting filtered by the remoteServerPort column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByLocalServerDomain(string $localServerDomain) Return the first ChildAppClientSetting filtered by the localServerDomain column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByLocalServerProtocol(string $localServerProtocol) Return the first ChildAppClientSetting filtered by the localServerProtocol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByLocalServerPort(string $localServerPort) Return the first ChildAppClientSetting filtered by the localServerPort column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByPNDomain(string $pnDomain) Return the first ChildAppClientSetting filtered by the pnDomain column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByPNProtocol(string $pnProtocol) Return the first ChildAppClientSetting filtered by the pnProtocol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByPNPort(string $pnPort) Return the first ChildAppClientSetting filtered by the pnPort column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByRemoteCctvDomain(string $remoteCctvDomain) Return the first ChildAppClientSetting filtered by the remoteCctvDomain column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByRemoteCctvProtocol(string $remoteCctvProtocol) Return the first ChildAppClientSetting filtered by the remoteCctvProtocol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByRemoteCctvPort(string $remoteCctvPort) Return the first ChildAppClientSetting filtered by the remoteCctvPort column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByLocalCctvDomain(string $localCctvDomain) Return the first ChildAppClientSetting filtered by the localCctvDomain column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByLocalCctvProtocol(string $localCctvProtocol) Return the first ChildAppClientSetting filtered by the localCctvProtocol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByLocalCctvPort(string $localCctvPort) Return the first ChildAppClientSetting filtered by the localCctvPort column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByTutorialImage(string $tutorialImage) Return the first ChildAppClientSetting filtered by the tutorialImage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByTutorialImageEnable(int $tutorialImageEnable) Return the first ChildAppClientSetting filtered by the tutorialImageEnable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByAppAutoSyncInterval(string $appAutoSyncInterval) Return the first ChildAppClientSetting filtered by the appAutoSyncInterval column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByModifiedBy(string $modified_by) Return the first ChildAppClientSetting filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAppClientSetting requireOneByModifiedDate(string $modified_date) Return the first ChildAppClientSetting filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAppClientSetting[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAppClientSetting objects based on current ModelCriteria
 * @method     ChildAppClientSetting[]|ObjectCollection findById(string $id) Return ChildAppClientSetting objects filtered by the id column
 * @method     ChildAppClientSetting[]|ObjectCollection findByProjectId(string $projectId) Return ChildAppClientSetting objects filtered by the projectId column
 * @method     ChildAppClientSetting[]|ObjectCollection findByAppHomeIdleTime(int $appHomeIdleTime) Return ChildAppClientSetting objects filtered by the appHomeIdleTime column
 * @method     ChildAppClientSetting[]|ObjectCollection findByAppStandbyIdleTime(int $appStandbyIdleTime) Return ChildAppClientSetting objects filtered by the appStandbyIdleTime column
 * @method     ChildAppClientSetting[]|ObjectCollection findByAppHomeLogoImage(string $appHomeLogoImage) Return ChildAppClientSetting objects filtered by the appHomeLogoImage column
 * @method     ChildAppClientSetting[]|ObjectCollection findByReceiptLogoImage(string $receiptLogoImage) Return ChildAppClientSetting objects filtered by the receiptLogoImage column
 * @method     ChildAppClientSetting[]|ObjectCollection findByFacilityCheckingOnlyFlag(int $facilityCheckingOnlyFlag) Return ChildAppClientSetting objects filtered by the facilityCheckingOnlyFlag column
 * @method     ChildAppClientSetting[]|ObjectCollection findByCallingFormatPattern(string $callingFormatPattern) Return ChildAppClientSetting objects filtered by the callingFormatPattern column
 * @method     ChildAppClientSetting[]|ObjectCollection findByinterFacilityFlag(int $interFacilityFlag) Return ChildAppClientSetting objects filtered by the interFacilityFlag column
 * @method     ChildAppClientSetting[]|ObjectCollection findByProjectName(string $projectName) Return ChildAppClientSetting objects filtered by the projectName column
 * @method     ChildAppClientSetting[]|ObjectCollection findByProjectNameId(string $projectNameId) Return ChildAppClientSetting objects filtered by the projectNameId column
 * @method     ChildAppClientSetting[]|ObjectCollection findByCentralizedServerUrl(string $centralizedServerUrl) Return ChildAppClientSetting objects filtered by the centralizedServerUrl column
 * @method     ChildAppClientSetting[]|ObjectCollection findByRemoteServerDomain(string $remoteServerDomain) Return ChildAppClientSetting objects filtered by the remoteServerDomain column
 * @method     ChildAppClientSetting[]|ObjectCollection findByRemoteServerProtocol(string $remoteServerProtocol) Return ChildAppClientSetting objects filtered by the remoteServerProtocol column
 * @method     ChildAppClientSetting[]|ObjectCollection findByRemoteServerPort(string $remoteServerPort) Return ChildAppClientSetting objects filtered by the remoteServerPort column
 * @method     ChildAppClientSetting[]|ObjectCollection findByLocalServerDomain(string $localServerDomain) Return ChildAppClientSetting objects filtered by the localServerDomain column
 * @method     ChildAppClientSetting[]|ObjectCollection findByLocalServerProtocol(string $localServerProtocol) Return ChildAppClientSetting objects filtered by the localServerProtocol column
 * @method     ChildAppClientSetting[]|ObjectCollection findByLocalServerPort(string $localServerPort) Return ChildAppClientSetting objects filtered by the localServerPort column
 * @method     ChildAppClientSetting[]|ObjectCollection findByPNDomain(string $pnDomain) Return ChildAppClientSetting objects filtered by the pnDomain column
 * @method     ChildAppClientSetting[]|ObjectCollection findByPNProtocol(string $pnProtocol) Return ChildAppClientSetting objects filtered by the pnProtocol column
 * @method     ChildAppClientSetting[]|ObjectCollection findByPNPort(string $pnPort) Return ChildAppClientSetting objects filtered by the pnPort column
 * @method     ChildAppClientSetting[]|ObjectCollection findByRemoteCctvDomain(string $remoteCctvDomain) Return ChildAppClientSetting objects filtered by the remoteCctvDomain column
 * @method     ChildAppClientSetting[]|ObjectCollection findByRemoteCctvProtocol(string $remoteCctvProtocol) Return ChildAppClientSetting objects filtered by the remoteCctvProtocol column
 * @method     ChildAppClientSetting[]|ObjectCollection findByRemoteCctvPort(string $remoteCctvPort) Return ChildAppClientSetting objects filtered by the remoteCctvPort column
 * @method     ChildAppClientSetting[]|ObjectCollection findByLocalCctvDomain(string $localCctvDomain) Return ChildAppClientSetting objects filtered by the localCctvDomain column
 * @method     ChildAppClientSetting[]|ObjectCollection findByLocalCctvProtocol(string $localCctvProtocol) Return ChildAppClientSetting objects filtered by the localCctvProtocol column
 * @method     ChildAppClientSetting[]|ObjectCollection findByLocalCctvPort(string $localCctvPort) Return ChildAppClientSetting objects filtered by the localCctvPort column
 * @method     ChildAppClientSetting[]|ObjectCollection findByTutorialImage(string $tutorialImage) Return ChildAppClientSetting objects filtered by the tutorialImage column
 * @method     ChildAppClientSetting[]|ObjectCollection findByTutorialImageEnable(int $tutorialImageEnable) Return ChildAppClientSetting objects filtered by the tutorialImageEnable column
 * @method     ChildAppClientSetting[]|ObjectCollection findByAppAutoSyncInterval(string $appAutoSyncInterval) Return ChildAppClientSetting objects filtered by the appAutoSyncInterval column
 * @method     ChildAppClientSetting[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildAppClientSetting objects filtered by the modified_by column
 * @method     ChildAppClientSetting[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildAppClientSetting objects filtered by the modified_date column
 * @method     ChildAppClientSetting[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AppClientSettingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AppClientSettingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\AppClientSetting', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAppClientSettingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAppClientSettingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAppClientSettingQuery) {
            return $criteria;
        }
        $query = new ChildAppClientSettingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAppClientSetting|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AppClientSettingTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AppClientSettingTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAppClientSetting A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, appHomeIdleTime, appStandbyIdleTime, appHomeLogoImage, receiptLogoImage, facilityCheckingOnlyFlag, callingFormatPattern, interFacilityFlag, projectName, projectNameId, centralizedServerUrl, remoteServerDomain, remoteServerProtocol, remoteServerPort, localServerDomain, localServerProtocol, localServerPort, pnDomain, pnProtocol, pnPort, remoteCctvDomain, remoteCctvProtocol, remoteCctvPort, localCctvDomain, localCctvProtocol, localCctvPort, tutorialImage, tutorialImageEnable, appAutoSyncInterval, modified_by, modified_date FROM appClientSetting WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAppClientSetting $obj */
            $obj = new ChildAppClientSetting();
            $obj->hydrate($row);
            AppClientSettingTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAppClientSetting|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AppClientSettingTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AppClientSettingTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the appHomeIdleTime column
     *
     * Example usage:
     * <code>
     * $query->filterByAppHomeIdleTime(1234); // WHERE appHomeIdleTime = 1234
     * $query->filterByAppHomeIdleTime(array(12, 34)); // WHERE appHomeIdleTime IN (12, 34)
     * $query->filterByAppHomeIdleTime(array('min' => 12)); // WHERE appHomeIdleTime > 12
     * </code>
     *
     * @param     mixed $appHomeIdleTime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByAppHomeIdleTime($appHomeIdleTime = null, $comparison = null)
    {
        if (is_array($appHomeIdleTime)) {
            $useMinMax = false;
            if (isset($appHomeIdleTime['min'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_APPHOMEIDLETIME, $appHomeIdleTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($appHomeIdleTime['max'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_APPHOMEIDLETIME, $appHomeIdleTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_APPHOMEIDLETIME, $appHomeIdleTime, $comparison);
    }

    /**
     * Filter the query on the appStandbyIdleTime column
     *
     * Example usage:
     * <code>
     * $query->filterByAppStandbyIdleTime(1234); // WHERE appStandbyIdleTime = 1234
     * $query->filterByAppStandbyIdleTime(array(12, 34)); // WHERE appStandbyIdleTime IN (12, 34)
     * $query->filterByAppStandbyIdleTime(array('min' => 12)); // WHERE appStandbyIdleTime > 12
     * </code>
     *
     * @param     mixed $appStandbyIdleTime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByAppStandbyIdleTime($appStandbyIdleTime = null, $comparison = null)
    {
        if (is_array($appStandbyIdleTime)) {
            $useMinMax = false;
            if (isset($appStandbyIdleTime['min'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_APPSTANDBYIDLETIME, $appStandbyIdleTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($appStandbyIdleTime['max'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_APPSTANDBYIDLETIME, $appStandbyIdleTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_APPSTANDBYIDLETIME, $appStandbyIdleTime, $comparison);
    }

    /**
     * Filter the query on the appHomeLogoImage column
     *
     * Example usage:
     * <code>
     * $query->filterByAppHomeLogoImage('fooValue');   // WHERE appHomeLogoImage = 'fooValue'
     * $query->filterByAppHomeLogoImage('%fooValue%'); // WHERE appHomeLogoImage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appHomeLogoImage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByAppHomeLogoImage($appHomeLogoImage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appHomeLogoImage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $appHomeLogoImage)) {
                $appHomeLogoImage = str_replace('*', '%', $appHomeLogoImage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_APPHOMELOGOIMAGE, $appHomeLogoImage, $comparison);
    }

    /**
     * Filter the query on the receiptLogoImage column
     *
     * Example usage:
     * <code>
     * $query->filterByReceiptLogoImage('fooValue');   // WHERE receiptLogoImage = 'fooValue'
     * $query->filterByReceiptLogoImage('%fooValue%'); // WHERE receiptLogoImage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $receiptLogoImage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByReceiptLogoImage($receiptLogoImage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($receiptLogoImage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $receiptLogoImage)) {
                $receiptLogoImage = str_replace('*', '%', $receiptLogoImage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_RECEIPTLOGOIMAGE, $receiptLogoImage, $comparison);
    }

    /**
     * Filter the query on the facilityCheckingOnlyFlag column
     *
     * Example usage:
     * <code>
     * $query->filterByFacilityCheckingOnlyFlag(1234); // WHERE facilityCheckingOnlyFlag = 1234
     * $query->filterByFacilityCheckingOnlyFlag(array(12, 34)); // WHERE facilityCheckingOnlyFlag IN (12, 34)
     * $query->filterByFacilityCheckingOnlyFlag(array('min' => 12)); // WHERE facilityCheckingOnlyFlag > 12
     * </code>
     *
     * @param     mixed $facilityCheckingOnlyFlag The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByFacilityCheckingOnlyFlag($facilityCheckingOnlyFlag = null, $comparison = null)
    {
        if (is_array($facilityCheckingOnlyFlag)) {
            $useMinMax = false;
            if (isset($facilityCheckingOnlyFlag['min'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG, $facilityCheckingOnlyFlag['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($facilityCheckingOnlyFlag['max'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG, $facilityCheckingOnlyFlag['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG, $facilityCheckingOnlyFlag, $comparison);
    }

    /**
     * Filter the query on the callingFormatPattern column
     *
     * Example usage:
     * <code>
     * $query->filterByCallingFormatPattern('fooValue');   // WHERE callingFormatPattern = 'fooValue'
     * $query->filterByCallingFormatPattern('%fooValue%'); // WHERE callingFormatPattern LIKE '%fooValue%'
     * </code>
     *
     * @param     string $callingFormatPattern The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByCallingFormatPattern($callingFormatPattern = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($callingFormatPattern)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $callingFormatPattern)) {
                $callingFormatPattern = str_replace('*', '%', $callingFormatPattern);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_CALLINGFORMATPATTERN, $callingFormatPattern, $comparison);
    }

    /**
     * Filter the query on the interFacilityFlag column
     *
     * Example usage:
     * <code>
     * $query->filterByinterFacilityFlag(1234); // WHERE interFacilityFlag = 1234
     * $query->filterByinterFacilityFlag(array(12, 34)); // WHERE interFacilityFlag IN (12, 34)
     * $query->filterByinterFacilityFlag(array('min' => 12)); // WHERE interFacilityFlag > 12
     * </code>
     *
     * @param     mixed $interFacilityFlag The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByinterFacilityFlag($interFacilityFlag = null, $comparison = null)
    {
        if (is_array($interFacilityFlag)) {
            $useMinMax = false;
            if (isset($interFacilityFlag['min'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_INTERFACILITYFLAG, $interFacilityFlag['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($interFacilityFlag['max'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_INTERFACILITYFLAG, $interFacilityFlag['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_INTERFACILITYFLAG, $interFacilityFlag, $comparison);
    }

    /**
     * Filter the query on the projectName column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectName('fooValue');   // WHERE projectName = 'fooValue'
     * $query->filterByProjectName('%fooValue%'); // WHERE projectName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByProjectName($projectName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectName)) {
                $projectName = str_replace('*', '%', $projectName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_PROJECTNAME, $projectName, $comparison);
    }

    /**
     * Filter the query on the projectNameId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectNameId('fooValue');   // WHERE projectNameId = 'fooValue'
     * $query->filterByProjectNameId('%fooValue%'); // WHERE projectNameId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectNameId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByProjectNameId($projectNameId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectNameId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectNameId)) {
                $projectNameId = str_replace('*', '%', $projectNameId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_PROJECTNAMEID, $projectNameId, $comparison);
    }

    /**
     * Filter the query on the centralizedServerUrl column
     *
     * Example usage:
     * <code>
     * $query->filterByCentralizedServerUrl('fooValue');   // WHERE centralizedServerUrl = 'fooValue'
     * $query->filterByCentralizedServerUrl('%fooValue%'); // WHERE centralizedServerUrl LIKE '%fooValue%'
     * </code>
     *
     * @param     string $centralizedServerUrl The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByCentralizedServerUrl($centralizedServerUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($centralizedServerUrl)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $centralizedServerUrl)) {
                $centralizedServerUrl = str_replace('*', '%', $centralizedServerUrl);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_CENTRALIZEDSERVERURL, $centralizedServerUrl, $comparison);
    }

    /**
     * Filter the query on the remoteServerDomain column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteServerDomain('fooValue');   // WHERE remoteServerDomain = 'fooValue'
     * $query->filterByRemoteServerDomain('%fooValue%'); // WHERE remoteServerDomain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteServerDomain The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByRemoteServerDomain($remoteServerDomain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteServerDomain)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $remoteServerDomain)) {
                $remoteServerDomain = str_replace('*', '%', $remoteServerDomain);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_REMOTESERVERDOMAIN, $remoteServerDomain, $comparison);
    }

    /**
     * Filter the query on the remoteServerProtocol column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteServerProtocol('fooValue');   // WHERE remoteServerProtocol = 'fooValue'
     * $query->filterByRemoteServerProtocol('%fooValue%'); // WHERE remoteServerProtocol LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteServerProtocol The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByRemoteServerProtocol($remoteServerProtocol = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteServerProtocol)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $remoteServerProtocol)) {
                $remoteServerProtocol = str_replace('*', '%', $remoteServerProtocol);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_REMOTESERVERPROTOCOL, $remoteServerProtocol, $comparison);
    }

    /**
     * Filter the query on the remoteServerPort column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteServerPort('fooValue');   // WHERE remoteServerPort = 'fooValue'
     * $query->filterByRemoteServerPort('%fooValue%'); // WHERE remoteServerPort LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteServerPort The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByRemoteServerPort($remoteServerPort = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteServerPort)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $remoteServerPort)) {
                $remoteServerPort = str_replace('*', '%', $remoteServerPort);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_REMOTESERVERPORT, $remoteServerPort, $comparison);
    }

    /**
     * Filter the query on the localServerDomain column
     *
     * Example usage:
     * <code>
     * $query->filterByLocalServerDomain('fooValue');   // WHERE localServerDomain = 'fooValue'
     * $query->filterByLocalServerDomain('%fooValue%'); // WHERE localServerDomain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $localServerDomain The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByLocalServerDomain($localServerDomain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($localServerDomain)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $localServerDomain)) {
                $localServerDomain = str_replace('*', '%', $localServerDomain);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_LOCALSERVERDOMAIN, $localServerDomain, $comparison);
    }

    /**
     * Filter the query on the localServerProtocol column
     *
     * Example usage:
     * <code>
     * $query->filterByLocalServerProtocol('fooValue');   // WHERE localServerProtocol = 'fooValue'
     * $query->filterByLocalServerProtocol('%fooValue%'); // WHERE localServerProtocol LIKE '%fooValue%'
     * </code>
     *
     * @param     string $localServerProtocol The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByLocalServerProtocol($localServerProtocol = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($localServerProtocol)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $localServerProtocol)) {
                $localServerProtocol = str_replace('*', '%', $localServerProtocol);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_LOCALSERVERPROTOCOL, $localServerProtocol, $comparison);
    }

    /**
     * Filter the query on the localServerPort column
     *
     * Example usage:
     * <code>
     * $query->filterByLocalServerPort('fooValue');   // WHERE localServerPort = 'fooValue'
     * $query->filterByLocalServerPort('%fooValue%'); // WHERE localServerPort LIKE '%fooValue%'
     * </code>
     *
     * @param     string $localServerPort The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByLocalServerPort($localServerPort = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($localServerPort)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $localServerPort)) {
                $localServerPort = str_replace('*', '%', $localServerPort);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_LOCALSERVERPORT, $localServerPort, $comparison);
    }

    /**
     * Filter the query on the pnDomain column
     *
     * Example usage:
     * <code>
     * $query->filterByPNDomain('fooValue');   // WHERE pnDomain = 'fooValue'
     * $query->filterByPNDomain('%fooValue%'); // WHERE pnDomain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pNDomain The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByPNDomain($pNDomain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pNDomain)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pNDomain)) {
                $pNDomain = str_replace('*', '%', $pNDomain);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_PNDOMAIN, $pNDomain, $comparison);
    }

    /**
     * Filter the query on the pnProtocol column
     *
     * Example usage:
     * <code>
     * $query->filterByPNProtocol('fooValue');   // WHERE pnProtocol = 'fooValue'
     * $query->filterByPNProtocol('%fooValue%'); // WHERE pnProtocol LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pNProtocol The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByPNProtocol($pNProtocol = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pNProtocol)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pNProtocol)) {
                $pNProtocol = str_replace('*', '%', $pNProtocol);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_PNPROTOCOL, $pNProtocol, $comparison);
    }

    /**
     * Filter the query on the pnPort column
     *
     * Example usage:
     * <code>
     * $query->filterByPNPort('fooValue');   // WHERE pnPort = 'fooValue'
     * $query->filterByPNPort('%fooValue%'); // WHERE pnPort LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pNPort The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByPNPort($pNPort = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pNPort)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pNPort)) {
                $pNPort = str_replace('*', '%', $pNPort);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_PNPORT, $pNPort, $comparison);
    }

    /**
     * Filter the query on the remoteCctvDomain column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteCctvDomain('fooValue');   // WHERE remoteCctvDomain = 'fooValue'
     * $query->filterByRemoteCctvDomain('%fooValue%'); // WHERE remoteCctvDomain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteCctvDomain The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByRemoteCctvDomain($remoteCctvDomain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteCctvDomain)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $remoteCctvDomain)) {
                $remoteCctvDomain = str_replace('*', '%', $remoteCctvDomain);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_REMOTECCTVDOMAIN, $remoteCctvDomain, $comparison);
    }

    /**
     * Filter the query on the remoteCctvProtocol column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteCctvProtocol('fooValue');   // WHERE remoteCctvProtocol = 'fooValue'
     * $query->filterByRemoteCctvProtocol('%fooValue%'); // WHERE remoteCctvProtocol LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteCctvProtocol The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByRemoteCctvProtocol($remoteCctvProtocol = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteCctvProtocol)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $remoteCctvProtocol)) {
                $remoteCctvProtocol = str_replace('*', '%', $remoteCctvProtocol);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_REMOTECCTVPROTOCOL, $remoteCctvProtocol, $comparison);
    }

    /**
     * Filter the query on the remoteCctvPort column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteCctvPort('fooValue');   // WHERE remoteCctvPort = 'fooValue'
     * $query->filterByRemoteCctvPort('%fooValue%'); // WHERE remoteCctvPort LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteCctvPort The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByRemoteCctvPort($remoteCctvPort = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteCctvPort)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $remoteCctvPort)) {
                $remoteCctvPort = str_replace('*', '%', $remoteCctvPort);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_REMOTECCTVPORT, $remoteCctvPort, $comparison);
    }

    /**
     * Filter the query on the localCctvDomain column
     *
     * Example usage:
     * <code>
     * $query->filterByLocalCctvDomain('fooValue');   // WHERE localCctvDomain = 'fooValue'
     * $query->filterByLocalCctvDomain('%fooValue%'); // WHERE localCctvDomain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $localCctvDomain The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByLocalCctvDomain($localCctvDomain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($localCctvDomain)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $localCctvDomain)) {
                $localCctvDomain = str_replace('*', '%', $localCctvDomain);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_LOCALCCTVDOMAIN, $localCctvDomain, $comparison);
    }

    /**
     * Filter the query on the localCctvProtocol column
     *
     * Example usage:
     * <code>
     * $query->filterByLocalCctvProtocol('fooValue');   // WHERE localCctvProtocol = 'fooValue'
     * $query->filterByLocalCctvProtocol('%fooValue%'); // WHERE localCctvProtocol LIKE '%fooValue%'
     * </code>
     *
     * @param     string $localCctvProtocol The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByLocalCctvProtocol($localCctvProtocol = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($localCctvProtocol)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $localCctvProtocol)) {
                $localCctvProtocol = str_replace('*', '%', $localCctvProtocol);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_LOCALCCTVPROTOCOL, $localCctvProtocol, $comparison);
    }

    /**
     * Filter the query on the localCctvPort column
     *
     * Example usage:
     * <code>
     * $query->filterByLocalCctvPort('fooValue');   // WHERE localCctvPort = 'fooValue'
     * $query->filterByLocalCctvPort('%fooValue%'); // WHERE localCctvPort LIKE '%fooValue%'
     * </code>
     *
     * @param     string $localCctvPort The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByLocalCctvPort($localCctvPort = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($localCctvPort)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $localCctvPort)) {
                $localCctvPort = str_replace('*', '%', $localCctvPort);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_LOCALCCTVPORT, $localCctvPort, $comparison);
    }

    /**
     * Filter the query on the tutorialImage column
     *
     * Example usage:
     * <code>
     * $query->filterByTutorialImage('fooValue');   // WHERE tutorialImage = 'fooValue'
     * $query->filterByTutorialImage('%fooValue%'); // WHERE tutorialImage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tutorialImage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByTutorialImage($tutorialImage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tutorialImage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tutorialImage)) {
                $tutorialImage = str_replace('*', '%', $tutorialImage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_TUTORIALIMAGE, $tutorialImage, $comparison);
    }

    /**
     * Filter the query on the tutorialImageEnable column
     *
     * Example usage:
     * <code>
     * $query->filterByTutorialImageEnable(1234); // WHERE tutorialImageEnable = 1234
     * $query->filterByTutorialImageEnable(array(12, 34)); // WHERE tutorialImageEnable IN (12, 34)
     * $query->filterByTutorialImageEnable(array('min' => 12)); // WHERE tutorialImageEnable > 12
     * </code>
     *
     * @param     mixed $tutorialImageEnable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByTutorialImageEnable($tutorialImageEnable = null, $comparison = null)
    {
        if (is_array($tutorialImageEnable)) {
            $useMinMax = false;
            if (isset($tutorialImageEnable['min'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE, $tutorialImageEnable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tutorialImageEnable['max'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE, $tutorialImageEnable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE, $tutorialImageEnable, $comparison);
    }

    /**
     * Filter the query on the appAutoSyncInterval column
     *
     * Example usage:
     * <code>
     * $query->filterByAppAutoSyncInterval('fooValue');   // WHERE appAutoSyncInterval = 'fooValue'
     * $query->filterByAppAutoSyncInterval('%fooValue%'); // WHERE appAutoSyncInterval LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appAutoSyncInterval The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByAppAutoSyncInterval($appAutoSyncInterval = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appAutoSyncInterval)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $appAutoSyncInterval)) {
                $appAutoSyncInterval = str_replace('*', '%', $appAutoSyncInterval);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_APPAUTOSYNCINTERVAL, $appAutoSyncInterval, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(AppClientSettingTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppClientSettingTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAppClientSetting $appClientSetting Object to remove from the list of results
     *
     * @return $this|ChildAppClientSettingQuery The current query, for fluid interface
     */
    public function prune($appClientSetting = null)
    {
        if ($appClientSetting) {
            $this->addUsingAlias(AppClientSettingTableMap::COL_ID, $appClientSetting->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the appClientSetting table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppClientSettingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AppClientSettingTableMap::clearInstancePool();
            AppClientSettingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppClientSettingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AppClientSettingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AppClientSettingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AppClientSettingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AppClientSettingQuery
