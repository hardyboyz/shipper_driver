<?php

namespace Base;

use \DeviceAppMonitoring as ChildDeviceAppMonitoring;
use \DeviceAppMonitoringQuery as ChildDeviceAppMonitoringQuery;
use \Exception;
use \PDO;
use Map\DeviceAppMonitoringTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'deviceAppMonitoring' table.
 *
 *
 *
 * @method     ChildDeviceAppMonitoringQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildDeviceAppMonitoringQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildDeviceAppMonitoringQuery orderByDeviceName($order = Criteria::ASC) Order by the deviceName column
 * @method     ChildDeviceAppMonitoringQuery orderByRefNo($order = Criteria::ASC) Order by the refNo column
 * @method     ChildDeviceAppMonitoringQuery orderByAppName($order = Criteria::ASC) Order by the appName column
 * @method     ChildDeviceAppMonitoringQuery orderByAppVersion($order = Criteria::ASC) Order by the appVersion column
 * @method     ChildDeviceAppMonitoringQuery orderByAppStatus($order = Criteria::ASC) Order by the appStatus column
 * @method     ChildDeviceAppMonitoringQuery orderByRemarks($order = Criteria::ASC) Order by the remarks column
 * @method     ChildDeviceAppMonitoringQuery orderByLastUpdateDate($order = Criteria::ASC) Order by the lastUpdateDate column
 *
 * @method     ChildDeviceAppMonitoringQuery groupById() Group by the id column
 * @method     ChildDeviceAppMonitoringQuery groupByProjectId() Group by the projectId column
 * @method     ChildDeviceAppMonitoringQuery groupByDeviceName() Group by the deviceName column
 * @method     ChildDeviceAppMonitoringQuery groupByRefNo() Group by the refNo column
 * @method     ChildDeviceAppMonitoringQuery groupByAppName() Group by the appName column
 * @method     ChildDeviceAppMonitoringQuery groupByAppVersion() Group by the appVersion column
 * @method     ChildDeviceAppMonitoringQuery groupByAppStatus() Group by the appStatus column
 * @method     ChildDeviceAppMonitoringQuery groupByRemarks() Group by the remarks column
 * @method     ChildDeviceAppMonitoringQuery groupByLastUpdateDate() Group by the lastUpdateDate column
 *
 * @method     ChildDeviceAppMonitoringQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDeviceAppMonitoringQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDeviceAppMonitoringQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDeviceAppMonitoringQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDeviceAppMonitoringQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDeviceAppMonitoringQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDeviceAppMonitoring findOne(ConnectionInterface $con = null) Return the first ChildDeviceAppMonitoring matching the query
 * @method     ChildDeviceAppMonitoring findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDeviceAppMonitoring matching the query, or a new ChildDeviceAppMonitoring object populated from the query conditions when no match is found
 *
 * @method     ChildDeviceAppMonitoring findOneById(string $id) Return the first ChildDeviceAppMonitoring filtered by the id column
 * @method     ChildDeviceAppMonitoring findOneByProjectId(string $projectId) Return the first ChildDeviceAppMonitoring filtered by the projectId column
 * @method     ChildDeviceAppMonitoring findOneByDeviceName(string $deviceName) Return the first ChildDeviceAppMonitoring filtered by the deviceName column
 * @method     ChildDeviceAppMonitoring findOneByRefNo(string $refNo) Return the first ChildDeviceAppMonitoring filtered by the refNo column
 * @method     ChildDeviceAppMonitoring findOneByAppName(string $appName) Return the first ChildDeviceAppMonitoring filtered by the appName column
 * @method     ChildDeviceAppMonitoring findOneByAppVersion(string $appVersion) Return the first ChildDeviceAppMonitoring filtered by the appVersion column
 * @method     ChildDeviceAppMonitoring findOneByAppStatus(string $appStatus) Return the first ChildDeviceAppMonitoring filtered by the appStatus column
 * @method     ChildDeviceAppMonitoring findOneByRemarks(string $remarks) Return the first ChildDeviceAppMonitoring filtered by the remarks column
 * @method     ChildDeviceAppMonitoring findOneByLastUpdateDate(string $lastUpdateDate) Return the first ChildDeviceAppMonitoring filtered by the lastUpdateDate column *

 * @method     ChildDeviceAppMonitoring requirePk($key, ConnectionInterface $con = null) Return the ChildDeviceAppMonitoring by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceAppMonitoring requireOne(ConnectionInterface $con = null) Return the first ChildDeviceAppMonitoring matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDeviceAppMonitoring requireOneById(string $id) Return the first ChildDeviceAppMonitoring filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceAppMonitoring requireOneByProjectId(string $projectId) Return the first ChildDeviceAppMonitoring filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceAppMonitoring requireOneByDeviceName(string $deviceName) Return the first ChildDeviceAppMonitoring filtered by the deviceName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceAppMonitoring requireOneByRefNo(string $refNo) Return the first ChildDeviceAppMonitoring filtered by the refNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceAppMonitoring requireOneByAppName(string $appName) Return the first ChildDeviceAppMonitoring filtered by the appName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceAppMonitoring requireOneByAppVersion(string $appVersion) Return the first ChildDeviceAppMonitoring filtered by the appVersion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceAppMonitoring requireOneByAppStatus(string $appStatus) Return the first ChildDeviceAppMonitoring filtered by the appStatus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceAppMonitoring requireOneByRemarks(string $remarks) Return the first ChildDeviceAppMonitoring filtered by the remarks column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceAppMonitoring requireOneByLastUpdateDate(string $lastUpdateDate) Return the first ChildDeviceAppMonitoring filtered by the lastUpdateDate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDeviceAppMonitoring objects based on current ModelCriteria
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection findById(string $id) Return ChildDeviceAppMonitoring objects filtered by the id column
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection findByProjectId(string $projectId) Return ChildDeviceAppMonitoring objects filtered by the projectId column
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection findByDeviceName(string $deviceName) Return ChildDeviceAppMonitoring objects filtered by the deviceName column
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection findByRefNo(string $refNo) Return ChildDeviceAppMonitoring objects filtered by the refNo column
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection findByAppName(string $appName) Return ChildDeviceAppMonitoring objects filtered by the appName column
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection findByAppVersion(string $appVersion) Return ChildDeviceAppMonitoring objects filtered by the appVersion column
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection findByAppStatus(string $appStatus) Return ChildDeviceAppMonitoring objects filtered by the appStatus column
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection findByRemarks(string $remarks) Return ChildDeviceAppMonitoring objects filtered by the remarks column
 * @method     ChildDeviceAppMonitoring[]|ObjectCollection findByLastUpdateDate(string $lastUpdateDate) Return ChildDeviceAppMonitoring objects filtered by the lastUpdateDate column
 * @method     ChildDeviceAppMonitoring[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DeviceAppMonitoringQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\DeviceAppMonitoringQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\DeviceAppMonitoring', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDeviceAppMonitoringQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDeviceAppMonitoringQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDeviceAppMonitoringQuery) {
            return $criteria;
        }
        $query = new ChildDeviceAppMonitoringQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDeviceAppMonitoring|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DeviceAppMonitoringTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DeviceAppMonitoringTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDeviceAppMonitoring A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, deviceName, refNo, appName, appVersion, appStatus, remarks, lastUpdateDate FROM deviceAppMonitoring WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDeviceAppMonitoring $obj */
            $obj = new ChildDeviceAppMonitoring();
            $obj->hydrate($row);
            DeviceAppMonitoringTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDeviceAppMonitoring|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the deviceName column
     *
     * Example usage:
     * <code>
     * $query->filterByDeviceName('fooValue');   // WHERE deviceName = 'fooValue'
     * $query->filterByDeviceName('%fooValue%'); // WHERE deviceName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deviceName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByDeviceName($deviceName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deviceName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $deviceName)) {
                $deviceName = str_replace('*', '%', $deviceName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_DEVICENAME, $deviceName, $comparison);
    }

    /**
     * Filter the query on the refNo column
     *
     * Example usage:
     * <code>
     * $query->filterByRefNo('fooValue');   // WHERE refNo = 'fooValue'
     * $query->filterByRefNo('%fooValue%'); // WHERE refNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByRefNo($refNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $refNo)) {
                $refNo = str_replace('*', '%', $refNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_REFNO, $refNo, $comparison);
    }

    /**
     * Filter the query on the appName column
     *
     * Example usage:
     * <code>
     * $query->filterByAppName('fooValue');   // WHERE appName = 'fooValue'
     * $query->filterByAppName('%fooValue%'); // WHERE appName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByAppName($appName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $appName)) {
                $appName = str_replace('*', '%', $appName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_APPNAME, $appName, $comparison);
    }

    /**
     * Filter the query on the appVersion column
     *
     * Example usage:
     * <code>
     * $query->filterByAppVersion('fooValue');   // WHERE appVersion = 'fooValue'
     * $query->filterByAppVersion('%fooValue%'); // WHERE appVersion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appVersion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByAppVersion($appVersion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appVersion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $appVersion)) {
                $appVersion = str_replace('*', '%', $appVersion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_APPVERSION, $appVersion, $comparison);
    }

    /**
     * Filter the query on the appStatus column
     *
     * Example usage:
     * <code>
     * $query->filterByAppStatus('fooValue');   // WHERE appStatus = 'fooValue'
     * $query->filterByAppStatus('%fooValue%'); // WHERE appStatus LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appStatus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByAppStatus($appStatus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appStatus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $appStatus)) {
                $appStatus = str_replace('*', '%', $appStatus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_APPSTATUS, $appStatus, $comparison);
    }

    /**
     * Filter the query on the remarks column
     *
     * Example usage:
     * <code>
     * $query->filterByRemarks('fooValue');   // WHERE remarks = 'fooValue'
     * $query->filterByRemarks('%fooValue%'); // WHERE remarks LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remarks The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByRemarks($remarks = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remarks)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $remarks)) {
                $remarks = str_replace('*', '%', $remarks);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_REMARKS, $remarks, $comparison);
    }

    /**
     * Filter the query on the lastUpdateDate column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdateDate('2011-03-14'); // WHERE lastUpdateDate = '2011-03-14'
     * $query->filterByLastUpdateDate('now'); // WHERE lastUpdateDate = '2011-03-14'
     * $query->filterByLastUpdateDate(array('max' => 'yesterday')); // WHERE lastUpdateDate > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdateDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function filterByLastUpdateDate($lastUpdateDate = null, $comparison = null)
    {
        if (is_array($lastUpdateDate)) {
            $useMinMax = false;
            if (isset($lastUpdateDate['min'])) {
                $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_LASTUPDATEDATE, $lastUpdateDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdateDate['max'])) {
                $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_LASTUPDATEDATE, $lastUpdateDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_LASTUPDATEDATE, $lastUpdateDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDeviceAppMonitoring $deviceAppMonitoring Object to remove from the list of results
     *
     * @return $this|ChildDeviceAppMonitoringQuery The current query, for fluid interface
     */
    public function prune($deviceAppMonitoring = null)
    {
        if ($deviceAppMonitoring) {
            $this->addUsingAlias(DeviceAppMonitoringTableMap::COL_ID, $deviceAppMonitoring->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the deviceAppMonitoring table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceAppMonitoringTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DeviceAppMonitoringTableMap::clearInstancePool();
            DeviceAppMonitoringTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceAppMonitoringTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DeviceAppMonitoringTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DeviceAppMonitoringTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DeviceAppMonitoringTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // DeviceAppMonitoringQuery
