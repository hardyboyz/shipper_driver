<?php

namespace Base;

use \Facility as ChildFacility;
use \FacilityQuery as ChildFacilityQuery;
use \FacilityTimeslot as ChildFacilityTimeslot;
use \FacilityTimeslotQuery as ChildFacilityTimeslotQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\FacilityTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'facility' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class Facility implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\FacilityTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the name field.
     *
     * @var        string
     */
    protected $name;

    /**
     * The value for the project_id field.
     *
     * @var        string
     */
    protected $project_id;

    /**
     * The value for the type field.
     *
     * @var        string
     */
    protected $type;

    /**
     * The value for the requirebooking field.
     *
     * @var        int
     */
    protected $requirebooking;

    /**
     * The value for the advancedbooking field.
     *
     * @var        int
     */
    protected $advancedbooking;

    /**
     * The value for the maxhour field.
     *
     * @var        int
     */
    protected $maxhour;

    /**
     * The value for the coolingoff field.
     *
     * @var        int
     */
    protected $coolingoff;

    /**
     * The value for the starttime field.
     *
     * @var        \DateTime
     */
    protected $starttime;

    /**
     * The value for the endtime field.
     *
     * @var        \DateTime
     */
    protected $endtime;

    /**
     * The value for the deposit field.
     *
     * @var        double
     */
    protected $deposit;

    /**
     * The value for the charge field.
     *
     * @var        double
     */
    protected $charge;

    /**
     * The value for the rules field.
     *
     * @var        string
     */
    protected $rules;

    /**
     * The value for the enabled field.
     *
     * @var        int
     */
    protected $enabled;

    /**
     * The value for the created_by field.
     *
     * @var        string
     */
    protected $created_by;

    /**
     * The value for the created_date field.
     *
     * @var        \DateTime
     */
    protected $created_date;

    /**
     * The value for the modified_by field.
     *
     * @var        string
     */
    protected $modified_by;

    /**
     * The value for the modified_date field.
     *
     * @var        \DateTime
     */
    protected $modified_date;

    /**
     * @var        ObjectCollection|ChildFacilityTimeslot[] Collection to store aggregation of ChildFacilityTimeslot objects.
     */
    protected $collFacilityTimeslots;
    protected $collFacilityTimeslotsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFacilityTimeslot[]
     */
    protected $facilityTimeslotsScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Facility object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Facility</code> instance.  If
     * <code>obj</code> is an instance of <code>Facility</code>, delegates to
     * <code>equals(Facility)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Facility The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [project_id] column value.
     *
     * @return string
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [requirebooking] column value.
     *
     * @return int
     */
    public function getRequireBooking()
    {
        return $this->requirebooking;
    }

    /**
     * Get the [advancedbooking] column value.
     *
     * @return int
     */
    public function getAdvancedBooking()
    {
        return $this->advancedbooking;
    }

    /**
     * Get the [maxhour] column value.
     *
     * @return int
     */
    public function getMaxHour()
    {
        return $this->maxhour;
    }

    /**
     * Get the [coolingoff] column value.
     *
     * @return int
     */
    public function getCoolingOff()
    {
        return $this->coolingoff;
    }

    /**
     * Get the [optionally formatted] temporal [starttime] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStartTime($format = NULL)
    {
        if ($format === null) {
            return $this->starttime;
        } else {
            return $this->starttime instanceof \DateTime ? $this->starttime->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [endtime] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEndTime($format = NULL)
    {
        if ($format === null) {
            return $this->endtime;
        } else {
            return $this->endtime instanceof \DateTime ? $this->endtime->format($format) : null;
        }
    }

    /**
     * Get the [deposit] column value.
     *
     * @return double
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Get the [charge] column value.
     *
     * @return double
     */
    public function getCharge()
    {
        return $this->charge;
    }

    /**
     * Get the [rules] column value.
     *
     * @return string
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Get the [enabled] column value.
     *
     * @return int
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get the [created_by] column value.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Get the [optionally formatted] temporal [created_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedDate($format = NULL)
    {
        if ($format === null) {
            return $this->created_date;
        } else {
            return $this->created_date instanceof \DateTime ? $this->created_date->format($format) : null;
        }
    }

    /**
     * Get the [modified_by] column value.
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Get the [optionally formatted] temporal [modified_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModifiedDate($format = NULL)
    {
        if ($format === null) {
            return $this->modified_date;
        } else {
            return $this->modified_date instanceof \DateTime ? $this->modified_date->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[FacilityTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     *
     * @param string $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[FacilityTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [project_id] column.
     *
     * @param string $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setProjectId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->project_id !== $v) {
            $this->project_id = $v;
            $this->modifiedColumns[FacilityTableMap::COL_PROJECT_ID] = true;
        }

        return $this;
    } // setProjectId()

    /**
     * Set the value of [type] column.
     *
     * @param string $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[FacilityTableMap::COL_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Set the value of [requirebooking] column.
     *
     * @param int $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setRequireBooking($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->requirebooking !== $v) {
            $this->requirebooking = $v;
            $this->modifiedColumns[FacilityTableMap::COL_REQUIREBOOKING] = true;
        }

        return $this;
    } // setRequireBooking()

    /**
     * Set the value of [advancedbooking] column.
     *
     * @param int $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setAdvancedBooking($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->advancedbooking !== $v) {
            $this->advancedbooking = $v;
            $this->modifiedColumns[FacilityTableMap::COL_ADVANCEDBOOKING] = true;
        }

        return $this;
    } // setAdvancedBooking()

    /**
     * Set the value of [maxhour] column.
     *
     * @param int $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setMaxHour($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->maxhour !== $v) {
            $this->maxhour = $v;
            $this->modifiedColumns[FacilityTableMap::COL_MAXHOUR] = true;
        }

        return $this;
    } // setMaxHour()

    /**
     * Set the value of [coolingoff] column.
     *
     * @param int $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setCoolingOff($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->coolingoff !== $v) {
            $this->coolingoff = $v;
            $this->modifiedColumns[FacilityTableMap::COL_COOLINGOFF] = true;
        }

        return $this;
    } // setCoolingOff()

    /**
     * Sets the value of [starttime] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setStartTime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->starttime !== null || $dt !== null) {
            if ($this->starttime === null || $dt === null || $dt->format("H:i:s") !== $this->starttime->format("H:i:s")) {
                $this->starttime = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FacilityTableMap::COL_STARTTIME] = true;
            }
        } // if either are not null

        return $this;
    } // setStartTime()

    /**
     * Sets the value of [endtime] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setEndTime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->endtime !== null || $dt !== null) {
            if ($this->endtime === null || $dt === null || $dt->format("H:i:s") !== $this->endtime->format("H:i:s")) {
                $this->endtime = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FacilityTableMap::COL_ENDTIME] = true;
            }
        } // if either are not null

        return $this;
    } // setEndTime()

    /**
     * Set the value of [deposit] column.
     *
     * @param double $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setDeposit($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->deposit !== $v) {
            $this->deposit = $v;
            $this->modifiedColumns[FacilityTableMap::COL_DEPOSIT] = true;
        }

        return $this;
    } // setDeposit()

    /**
     * Set the value of [charge] column.
     *
     * @param double $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setCharge($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->charge !== $v) {
            $this->charge = $v;
            $this->modifiedColumns[FacilityTableMap::COL_CHARGE] = true;
        }

        return $this;
    } // setCharge()

    /**
     * Set the value of [rules] column.
     *
     * @param string $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setRules($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->rules !== $v) {
            $this->rules = $v;
            $this->modifiedColumns[FacilityTableMap::COL_RULES] = true;
        }

        return $this;
    } // setRules()

    /**
     * Set the value of [enabled] column.
     *
     * @param int $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setEnabled($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->enabled !== $v) {
            $this->enabled = $v;
            $this->modifiedColumns[FacilityTableMap::COL_ENABLED] = true;
        }

        return $this;
    } // setEnabled()

    /**
     * Set the value of [created_by] column.
     *
     * @param string $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setCreatedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->created_by !== $v) {
            $this->created_by = $v;
            $this->modifiedColumns[FacilityTableMap::COL_CREATED_BY] = true;
        }

        return $this;
    } // setCreatedBy()

    /**
     * Sets the value of [created_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setCreatedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_date !== null || $dt !== null) {
            if ($this->created_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->created_date->format("Y-m-d H:i:s")) {
                $this->created_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FacilityTableMap::COL_CREATED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedDate()

    /**
     * Set the value of [modified_by] column.
     *
     * @param string $v new value
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setModifiedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modified_by !== $v) {
            $this->modified_by = $v;
            $this->modifiedColumns[FacilityTableMap::COL_MODIFIED_BY] = true;
        }

        return $this;
    } // setModifiedBy()

    /**
     * Sets the value of [modified_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function setModifiedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified_date !== null || $dt !== null) {
            if ($this->modified_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->modified_date->format("Y-m-d H:i:s")) {
                $this->modified_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FacilityTableMap::COL_MODIFIED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setModifiedDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : FacilityTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : FacilityTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : FacilityTableMap::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->project_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : FacilityTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : FacilityTableMap::translateFieldName('RequireBooking', TableMap::TYPE_PHPNAME, $indexType)];
            $this->requirebooking = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : FacilityTableMap::translateFieldName('AdvancedBooking', TableMap::TYPE_PHPNAME, $indexType)];
            $this->advancedbooking = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : FacilityTableMap::translateFieldName('MaxHour', TableMap::TYPE_PHPNAME, $indexType)];
            $this->maxhour = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : FacilityTableMap::translateFieldName('CoolingOff', TableMap::TYPE_PHPNAME, $indexType)];
            $this->coolingoff = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : FacilityTableMap::translateFieldName('StartTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->starttime = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : FacilityTableMap::translateFieldName('EndTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->endtime = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : FacilityTableMap::translateFieldName('Deposit', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deposit = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : FacilityTableMap::translateFieldName('Charge', TableMap::TYPE_PHPNAME, $indexType)];
            $this->charge = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : FacilityTableMap::translateFieldName('Rules', TableMap::TYPE_PHPNAME, $indexType)];
            $this->rules = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : FacilityTableMap::translateFieldName('Enabled', TableMap::TYPE_PHPNAME, $indexType)];
            $this->enabled = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : FacilityTableMap::translateFieldName('CreatedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : FacilityTableMap::translateFieldName('CreatedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : FacilityTableMap::translateFieldName('ModifiedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modified_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : FacilityTableMap::translateFieldName('ModifiedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->modified_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 18; // 18 = FacilityTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Facility'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FacilityTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildFacilityQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collFacilityTimeslots = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Facility::setDeleted()
     * @see Facility::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildFacilityQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FacilityTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->facilityTimeslotsScheduledForDeletion !== null) {
                if (!$this->facilityTimeslotsScheduledForDeletion->isEmpty()) {
                    \FacilityTimeslotQuery::create()
                        ->filterByPrimaryKeys($this->facilityTimeslotsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->facilityTimeslotsScheduledForDeletion = null;
                }
            }

            if ($this->collFacilityTimeslots !== null) {
                foreach ($this->collFacilityTimeslots as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FacilityTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_PROJECT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'project_id';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'type';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_REQUIREBOOKING)) {
            $modifiedColumns[':p' . $index++]  = 'requirebooking';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_ADVANCEDBOOKING)) {
            $modifiedColumns[':p' . $index++]  = 'advancedbooking';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_MAXHOUR)) {
            $modifiedColumns[':p' . $index++]  = 'maxhour';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_COOLINGOFF)) {
            $modifiedColumns[':p' . $index++]  = 'coolingoff';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_STARTTIME)) {
            $modifiedColumns[':p' . $index++]  = 'starttime';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_ENDTIME)) {
            $modifiedColumns[':p' . $index++]  = 'endtime';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_DEPOSIT)) {
            $modifiedColumns[':p' . $index++]  = 'deposit';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_CHARGE)) {
            $modifiedColumns[':p' . $index++]  = 'charge';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_RULES)) {
            $modifiedColumns[':p' . $index++]  = 'rules';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_ENABLED)) {
            $modifiedColumns[':p' . $index++]  = 'enabled';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_CREATED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'created_by';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_CREATED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'created_date';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_MODIFIED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'modified_by';
        }
        if ($this->isColumnModified(FacilityTableMap::COL_MODIFIED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'modified_date';
        }

        $sql = sprintf(
            'INSERT INTO facility (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'project_id':
                        $stmt->bindValue($identifier, $this->project_id, PDO::PARAM_STR);
                        break;
                    case 'type':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case 'requirebooking':
                        $stmt->bindValue($identifier, $this->requirebooking, PDO::PARAM_INT);
                        break;
                    case 'advancedbooking':
                        $stmt->bindValue($identifier, $this->advancedbooking, PDO::PARAM_INT);
                        break;
                    case 'maxhour':
                        $stmt->bindValue($identifier, $this->maxhour, PDO::PARAM_INT);
                        break;
                    case 'coolingoff':
                        $stmt->bindValue($identifier, $this->coolingoff, PDO::PARAM_INT);
                        break;
                    case 'starttime':
                        $stmt->bindValue($identifier, $this->starttime ? $this->starttime->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'endtime':
                        $stmt->bindValue($identifier, $this->endtime ? $this->endtime->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'deposit':
                        $stmt->bindValue($identifier, $this->deposit, PDO::PARAM_STR);
                        break;
                    case 'charge':
                        $stmt->bindValue($identifier, $this->charge, PDO::PARAM_STR);
                        break;
                    case 'rules':
                        $stmt->bindValue($identifier, $this->rules, PDO::PARAM_STR);
                        break;
                    case 'enabled':
                        $stmt->bindValue($identifier, $this->enabled, PDO::PARAM_INT);
                        break;
                    case 'created_by':
                        $stmt->bindValue($identifier, $this->created_by, PDO::PARAM_STR);
                        break;
                    case 'created_date':
                        $stmt->bindValue($identifier, $this->created_date ? $this->created_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'modified_by':
                        $stmt->bindValue($identifier, $this->modified_by, PDO::PARAM_STR);
                        break;
                    case 'modified_date':
                        $stmt->bindValue($identifier, $this->modified_date ? $this->modified_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FacilityTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getProjectId();
                break;
            case 3:
                return $this->getType();
                break;
            case 4:
                return $this->getRequireBooking();
                break;
            case 5:
                return $this->getAdvancedBooking();
                break;
            case 6:
                return $this->getMaxHour();
                break;
            case 7:
                return $this->getCoolingOff();
                break;
            case 8:
                return $this->getStartTime();
                break;
            case 9:
                return $this->getEndTime();
                break;
            case 10:
                return $this->getDeposit();
                break;
            case 11:
                return $this->getCharge();
                break;
            case 12:
                return $this->getRules();
                break;
            case 13:
                return $this->getEnabled();
                break;
            case 14:
                return $this->getCreatedBy();
                break;
            case 15:
                return $this->getCreatedDate();
                break;
            case 16:
                return $this->getModifiedBy();
                break;
            case 17:
                return $this->getModifiedDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Facility'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Facility'][$this->hashCode()] = true;
        $keys = FacilityTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getProjectId(),
            $keys[3] => $this->getType(),
            $keys[4] => $this->getRequireBooking(),
            $keys[5] => $this->getAdvancedBooking(),
            $keys[6] => $this->getMaxHour(),
            $keys[7] => $this->getCoolingOff(),
            $keys[8] => $this->getStartTime(),
            $keys[9] => $this->getEndTime(),
            $keys[10] => $this->getDeposit(),
            $keys[11] => $this->getCharge(),
            $keys[12] => $this->getRules(),
            $keys[13] => $this->getEnabled(),
            $keys[14] => $this->getCreatedBy(),
            $keys[15] => $this->getCreatedDate(),
            $keys[16] => $this->getModifiedBy(),
            $keys[17] => $this->getModifiedDate(),
        );
        if ($result[$keys[8]] instanceof \DateTime) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTime) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTime) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collFacilityTimeslots) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'facilityTimeslots';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'facility_timeslots';
                        break;
                    default:
                        $key = 'FacilityTimeslots';
                }

                $result[$key] = $this->collFacilityTimeslots->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Facility
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FacilityTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Facility
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setProjectId($value);
                break;
            case 3:
                $this->setType($value);
                break;
            case 4:
                $this->setRequireBooking($value);
                break;
            case 5:
                $this->setAdvancedBooking($value);
                break;
            case 6:
                $this->setMaxHour($value);
                break;
            case 7:
                $this->setCoolingOff($value);
                break;
            case 8:
                $this->setStartTime($value);
                break;
            case 9:
                $this->setEndTime($value);
                break;
            case 10:
                $this->setDeposit($value);
                break;
            case 11:
                $this->setCharge($value);
                break;
            case 12:
                $this->setRules($value);
                break;
            case 13:
                $this->setEnabled($value);
                break;
            case 14:
                $this->setCreatedBy($value);
                break;
            case 15:
                $this->setCreatedDate($value);
                break;
            case 16:
                $this->setModifiedBy($value);
                break;
            case 17:
                $this->setModifiedDate($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = FacilityTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setProjectId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setType($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setRequireBooking($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setAdvancedBooking($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setMaxHour($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCoolingOff($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setStartTime($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setEndTime($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDeposit($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setCharge($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setRules($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setEnabled($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setCreatedBy($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setCreatedDate($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setModifiedBy($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setModifiedDate($arr[$keys[17]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Facility The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(FacilityTableMap::DATABASE_NAME);

        if ($this->isColumnModified(FacilityTableMap::COL_ID)) {
            $criteria->add(FacilityTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_NAME)) {
            $criteria->add(FacilityTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_PROJECT_ID)) {
            $criteria->add(FacilityTableMap::COL_PROJECT_ID, $this->project_id);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_TYPE)) {
            $criteria->add(FacilityTableMap::COL_TYPE, $this->type);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_REQUIREBOOKING)) {
            $criteria->add(FacilityTableMap::COL_REQUIREBOOKING, $this->requirebooking);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_ADVANCEDBOOKING)) {
            $criteria->add(FacilityTableMap::COL_ADVANCEDBOOKING, $this->advancedbooking);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_MAXHOUR)) {
            $criteria->add(FacilityTableMap::COL_MAXHOUR, $this->maxhour);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_COOLINGOFF)) {
            $criteria->add(FacilityTableMap::COL_COOLINGOFF, $this->coolingoff);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_STARTTIME)) {
            $criteria->add(FacilityTableMap::COL_STARTTIME, $this->starttime);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_ENDTIME)) {
            $criteria->add(FacilityTableMap::COL_ENDTIME, $this->endtime);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_DEPOSIT)) {
            $criteria->add(FacilityTableMap::COL_DEPOSIT, $this->deposit);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_CHARGE)) {
            $criteria->add(FacilityTableMap::COL_CHARGE, $this->charge);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_RULES)) {
            $criteria->add(FacilityTableMap::COL_RULES, $this->rules);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_ENABLED)) {
            $criteria->add(FacilityTableMap::COL_ENABLED, $this->enabled);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_CREATED_BY)) {
            $criteria->add(FacilityTableMap::COL_CREATED_BY, $this->created_by);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_CREATED_DATE)) {
            $criteria->add(FacilityTableMap::COL_CREATED_DATE, $this->created_date);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_MODIFIED_BY)) {
            $criteria->add(FacilityTableMap::COL_MODIFIED_BY, $this->modified_by);
        }
        if ($this->isColumnModified(FacilityTableMap::COL_MODIFIED_DATE)) {
            $criteria->add(FacilityTableMap::COL_MODIFIED_DATE, $this->modified_date);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildFacilityQuery::create();
        $criteria->add(FacilityTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Facility (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setName($this->getName());
        $copyObj->setProjectId($this->getProjectId());
        $copyObj->setType($this->getType());
        $copyObj->setRequireBooking($this->getRequireBooking());
        $copyObj->setAdvancedBooking($this->getAdvancedBooking());
        $copyObj->setMaxHour($this->getMaxHour());
        $copyObj->setCoolingOff($this->getCoolingOff());
        $copyObj->setStartTime($this->getStartTime());
        $copyObj->setEndTime($this->getEndTime());
        $copyObj->setDeposit($this->getDeposit());
        $copyObj->setCharge($this->getCharge());
        $copyObj->setRules($this->getRules());
        $copyObj->setEnabled($this->getEnabled());
        $copyObj->setCreatedBy($this->getCreatedBy());
        $copyObj->setCreatedDate($this->getCreatedDate());
        $copyObj->setModifiedBy($this->getModifiedBy());
        $copyObj->setModifiedDate($this->getModifiedDate());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getFacilityTimeslots() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFacilityTimeslot($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Facility Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('FacilityTimeslot' == $relationName) {
            return $this->initFacilityTimeslots();
        }
    }

    /**
     * Clears out the collFacilityTimeslots collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFacilityTimeslots()
     */
    public function clearFacilityTimeslots()
    {
        $this->collFacilityTimeslots = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFacilityTimeslots collection loaded partially.
     */
    public function resetPartialFacilityTimeslots($v = true)
    {
        $this->collFacilityTimeslotsPartial = $v;
    }

    /**
     * Initializes the collFacilityTimeslots collection.
     *
     * By default this just sets the collFacilityTimeslots collection to an empty array (like clearcollFacilityTimeslots());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFacilityTimeslots($overrideExisting = true)
    {
        if (null !== $this->collFacilityTimeslots && !$overrideExisting) {
            return;
        }
        $this->collFacilityTimeslots = new ObjectCollection();
        $this->collFacilityTimeslots->setModel('\FacilityTimeslot');
    }

    /**
     * Gets an array of ChildFacilityTimeslot objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFacility is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFacilityTimeslot[] List of ChildFacilityTimeslot objects
     * @throws PropelException
     */
    public function getFacilityTimeslots(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFacilityTimeslotsPartial && !$this->isNew();
        if (null === $this->collFacilityTimeslots || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFacilityTimeslots) {
                // return empty collection
                $this->initFacilityTimeslots();
            } else {
                $collFacilityTimeslots = ChildFacilityTimeslotQuery::create(null, $criteria)
                    ->filterByFacility($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFacilityTimeslotsPartial && count($collFacilityTimeslots)) {
                        $this->initFacilityTimeslots(false);

                        foreach ($collFacilityTimeslots as $obj) {
                            if (false == $this->collFacilityTimeslots->contains($obj)) {
                                $this->collFacilityTimeslots->append($obj);
                            }
                        }

                        $this->collFacilityTimeslotsPartial = true;
                    }

                    return $collFacilityTimeslots;
                }

                if ($partial && $this->collFacilityTimeslots) {
                    foreach ($this->collFacilityTimeslots as $obj) {
                        if ($obj->isNew()) {
                            $collFacilityTimeslots[] = $obj;
                        }
                    }
                }

                $this->collFacilityTimeslots = $collFacilityTimeslots;
                $this->collFacilityTimeslotsPartial = false;
            }
        }

        return $this->collFacilityTimeslots;
    }

    /**
     * Sets a collection of ChildFacilityTimeslot objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $facilityTimeslots A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFacility The current object (for fluent API support)
     */
    public function setFacilityTimeslots(Collection $facilityTimeslots, ConnectionInterface $con = null)
    {
        /** @var ChildFacilityTimeslot[] $facilityTimeslotsToDelete */
        $facilityTimeslotsToDelete = $this->getFacilityTimeslots(new Criteria(), $con)->diff($facilityTimeslots);


        $this->facilityTimeslotsScheduledForDeletion = $facilityTimeslotsToDelete;

        foreach ($facilityTimeslotsToDelete as $facilityTimeslotRemoved) {
            $facilityTimeslotRemoved->setFacility(null);
        }

        $this->collFacilityTimeslots = null;
        foreach ($facilityTimeslots as $facilityTimeslot) {
            $this->addFacilityTimeslot($facilityTimeslot);
        }

        $this->collFacilityTimeslots = $facilityTimeslots;
        $this->collFacilityTimeslotsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FacilityTimeslot objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FacilityTimeslot objects.
     * @throws PropelException
     */
    public function countFacilityTimeslots(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFacilityTimeslotsPartial && !$this->isNew();
        if (null === $this->collFacilityTimeslots || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFacilityTimeslots) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFacilityTimeslots());
            }

            $query = ChildFacilityTimeslotQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFacility($this)
                ->count($con);
        }

        return count($this->collFacilityTimeslots);
    }

    /**
     * Method called to associate a ChildFacilityTimeslot object to this object
     * through the ChildFacilityTimeslot foreign key attribute.
     *
     * @param  ChildFacilityTimeslot $l ChildFacilityTimeslot
     * @return $this|\Facility The current object (for fluent API support)
     */
    public function addFacilityTimeslot(ChildFacilityTimeslot $l)
    {
        if ($this->collFacilityTimeslots === null) {
            $this->initFacilityTimeslots();
            $this->collFacilityTimeslotsPartial = true;
        }

        if (!$this->collFacilityTimeslots->contains($l)) {
            $this->doAddFacilityTimeslot($l);
        }

        return $this;
    }

    /**
     * @param ChildFacilityTimeslot $facilityTimeslot The ChildFacilityTimeslot object to add.
     */
    protected function doAddFacilityTimeslot(ChildFacilityTimeslot $facilityTimeslot)
    {
        $this->collFacilityTimeslots[]= $facilityTimeslot;
        $facilityTimeslot->setFacility($this);
    }

    /**
     * @param  ChildFacilityTimeslot $facilityTimeslot The ChildFacilityTimeslot object to remove.
     * @return $this|ChildFacility The current object (for fluent API support)
     */
    public function removeFacilityTimeslot(ChildFacilityTimeslot $facilityTimeslot)
    {
        if ($this->getFacilityTimeslots()->contains($facilityTimeslot)) {
            $pos = $this->collFacilityTimeslots->search($facilityTimeslot);
            $this->collFacilityTimeslots->remove($pos);
            if (null === $this->facilityTimeslotsScheduledForDeletion) {
                $this->facilityTimeslotsScheduledForDeletion = clone $this->collFacilityTimeslots;
                $this->facilityTimeslotsScheduledForDeletion->clear();
            }
            $this->facilityTimeslotsScheduledForDeletion[]= clone $facilityTimeslot;
            $facilityTimeslot->setFacility(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->project_id = null;
        $this->type = null;
        $this->requirebooking = null;
        $this->advancedbooking = null;
        $this->maxhour = null;
        $this->coolingoff = null;
        $this->starttime = null;
        $this->endtime = null;
        $this->deposit = null;
        $this->charge = null;
        $this->rules = null;
        $this->enabled = null;
        $this->created_by = null;
        $this->created_date = null;
        $this->modified_by = null;
        $this->modified_date = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collFacilityTimeslots) {
                foreach ($this->collFacilityTimeslots as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collFacilityTimeslots = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(FacilityTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
