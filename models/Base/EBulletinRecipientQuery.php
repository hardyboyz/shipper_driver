<?php

namespace Base;

use \EBulletinRecipient as ChildEBulletinRecipient;
use \EBulletinRecipientQuery as ChildEBulletinRecipientQuery;
use \Exception;
use \PDO;
use Map\EBulletinRecipientTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'ebulletin_recipient' table.
 *
 *
 *
 * @method     ChildEBulletinRecipientQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildEBulletinRecipientQuery orderByEBulletinId($order = Criteria::ASC) Order by the eBulletinId column
 * @method     ChildEBulletinRecipientQuery orderByTenantId($order = Criteria::ASC) Order by the tenant_id column
 * @method     ChildEBulletinRecipientQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildEBulletinRecipientQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildEBulletinRecipientQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildEBulletinRecipientQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildEBulletinRecipientQuery groupById() Group by the id column
 * @method     ChildEBulletinRecipientQuery groupByEBulletinId() Group by the eBulletinId column
 * @method     ChildEBulletinRecipientQuery groupByTenantId() Group by the tenant_id column
 * @method     ChildEBulletinRecipientQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildEBulletinRecipientQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildEBulletinRecipientQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildEBulletinRecipientQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildEBulletinRecipientQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEBulletinRecipientQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEBulletinRecipientQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEBulletinRecipientQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEBulletinRecipientQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEBulletinRecipientQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEBulletinRecipientQuery leftJoinEBulletin($relationAlias = null) Adds a LEFT JOIN clause to the query using the EBulletin relation
 * @method     ChildEBulletinRecipientQuery rightJoinEBulletin($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EBulletin relation
 * @method     ChildEBulletinRecipientQuery innerJoinEBulletin($relationAlias = null) Adds a INNER JOIN clause to the query using the EBulletin relation
 *
 * @method     ChildEBulletinRecipientQuery joinWithEBulletin($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EBulletin relation
 *
 * @method     ChildEBulletinRecipientQuery leftJoinWithEBulletin() Adds a LEFT JOIN clause and with to the query using the EBulletin relation
 * @method     ChildEBulletinRecipientQuery rightJoinWithEBulletin() Adds a RIGHT JOIN clause and with to the query using the EBulletin relation
 * @method     ChildEBulletinRecipientQuery innerJoinWithEBulletin() Adds a INNER JOIN clause and with to the query using the EBulletin relation
 *
 * @method     \EBulletinQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEBulletinRecipient findOne(ConnectionInterface $con = null) Return the first ChildEBulletinRecipient matching the query
 * @method     ChildEBulletinRecipient findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEBulletinRecipient matching the query, or a new ChildEBulletinRecipient object populated from the query conditions when no match is found
 *
 * @method     ChildEBulletinRecipient findOneById(string $id) Return the first ChildEBulletinRecipient filtered by the id column
 * @method     ChildEBulletinRecipient findOneByEBulletinId(string $eBulletinId) Return the first ChildEBulletinRecipient filtered by the eBulletinId column
 * @method     ChildEBulletinRecipient findOneByTenantId(string $tenant_id) Return the first ChildEBulletinRecipient filtered by the tenant_id column
 * @method     ChildEBulletinRecipient findOneByCreatedBy(string $created_by) Return the first ChildEBulletinRecipient filtered by the created_by column
 * @method     ChildEBulletinRecipient findOneByCreatedDate(string $created_date) Return the first ChildEBulletinRecipient filtered by the created_date column
 * @method     ChildEBulletinRecipient findOneByModifiedBy(string $modified_by) Return the first ChildEBulletinRecipient filtered by the modified_by column
 * @method     ChildEBulletinRecipient findOneByModifiedDate(string $modified_date) Return the first ChildEBulletinRecipient filtered by the modified_date column *

 * @method     ChildEBulletinRecipient requirePk($key, ConnectionInterface $con = null) Return the ChildEBulletinRecipient by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletinRecipient requireOne(ConnectionInterface $con = null) Return the first ChildEBulletinRecipient matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEBulletinRecipient requireOneById(string $id) Return the first ChildEBulletinRecipient filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletinRecipient requireOneByEBulletinId(string $eBulletinId) Return the first ChildEBulletinRecipient filtered by the eBulletinId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletinRecipient requireOneByTenantId(string $tenant_id) Return the first ChildEBulletinRecipient filtered by the tenant_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletinRecipient requireOneByCreatedBy(string $created_by) Return the first ChildEBulletinRecipient filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletinRecipient requireOneByCreatedDate(string $created_date) Return the first ChildEBulletinRecipient filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletinRecipient requireOneByModifiedBy(string $modified_by) Return the first ChildEBulletinRecipient filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletinRecipient requireOneByModifiedDate(string $modified_date) Return the first ChildEBulletinRecipient filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEBulletinRecipient[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEBulletinRecipient objects based on current ModelCriteria
 * @method     ChildEBulletinRecipient[]|ObjectCollection findById(string $id) Return ChildEBulletinRecipient objects filtered by the id column
 * @method     ChildEBulletinRecipient[]|ObjectCollection findByEBulletinId(string $eBulletinId) Return ChildEBulletinRecipient objects filtered by the eBulletinId column
 * @method     ChildEBulletinRecipient[]|ObjectCollection findByTenantId(string $tenant_id) Return ChildEBulletinRecipient objects filtered by the tenant_id column
 * @method     ChildEBulletinRecipient[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildEBulletinRecipient objects filtered by the created_by column
 * @method     ChildEBulletinRecipient[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildEBulletinRecipient objects filtered by the created_date column
 * @method     ChildEBulletinRecipient[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildEBulletinRecipient objects filtered by the modified_by column
 * @method     ChildEBulletinRecipient[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildEBulletinRecipient objects filtered by the modified_date column
 * @method     ChildEBulletinRecipient[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EBulletinRecipientQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\EBulletinRecipientQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\EBulletinRecipient', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEBulletinRecipientQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEBulletinRecipientQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEBulletinRecipientQuery) {
            return $criteria;
        }
        $query = new ChildEBulletinRecipientQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEBulletinRecipient|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = EBulletinRecipientTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EBulletinRecipientTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEBulletinRecipient A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, eBulletinId, tenant_id, created_by, created_date, modified_by, modified_date FROM ebulletin_recipient WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEBulletinRecipient $obj */
            $obj = new ChildEBulletinRecipient();
            $obj->hydrate($row);
            EBulletinRecipientTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEBulletinRecipient|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EBulletinRecipientTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EBulletinRecipientTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinRecipientTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the eBulletinId column
     *
     * Example usage:
     * <code>
     * $query->filterByEBulletinId('fooValue');   // WHERE eBulletinId = 'fooValue'
     * $query->filterByEBulletinId('%fooValue%'); // WHERE eBulletinId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $eBulletinId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterByEBulletinId($eBulletinId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($eBulletinId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $eBulletinId)) {
                $eBulletinId = str_replace('*', '%', $eBulletinId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinRecipientTableMap::COL_EBULLETINID, $eBulletinId, $comparison);
    }

    /**
     * Filter the query on the tenant_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTenantId('fooValue');   // WHERE tenant_id = 'fooValue'
     * $query->filterByTenantId('%fooValue%'); // WHERE tenant_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tenantId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterByTenantId($tenantId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tenantId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tenantId)) {
                $tenantId = str_replace('*', '%', $tenantId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinRecipientTableMap::COL_TENANT_ID, $tenantId, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinRecipientTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(EBulletinRecipientTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(EBulletinRecipientTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EBulletinRecipientTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinRecipientTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(EBulletinRecipientTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(EBulletinRecipientTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EBulletinRecipientTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query by a related \EBulletin object
     *
     * @param \EBulletin|ObjectCollection $eBulletin The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function filterByEBulletin($eBulletin, $comparison = null)
    {
        if ($eBulletin instanceof \EBulletin) {
            return $this
                ->addUsingAlias(EBulletinRecipientTableMap::COL_EBULLETINID, $eBulletin->getId(), $comparison);
        } elseif ($eBulletin instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EBulletinRecipientTableMap::COL_EBULLETINID, $eBulletin->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEBulletin() only accepts arguments of type \EBulletin or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EBulletin relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function joinEBulletin($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EBulletin');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EBulletin');
        }

        return $this;
    }

    /**
     * Use the EBulletin relation EBulletin object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EBulletinQuery A secondary query class using the current class as primary query
     */
    public function useEBulletinQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEBulletin($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EBulletin', '\EBulletinQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEBulletinRecipient $eBulletinRecipient Object to remove from the list of results
     *
     * @return $this|ChildEBulletinRecipientQuery The current query, for fluid interface
     */
    public function prune($eBulletinRecipient = null)
    {
        if ($eBulletinRecipient) {
            $this->addUsingAlias(EBulletinRecipientTableMap::COL_ID, $eBulletinRecipient->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the ebulletin_recipient table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EBulletinRecipientTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EBulletinRecipientTableMap::clearInstancePool();
            EBulletinRecipientTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EBulletinRecipientTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EBulletinRecipientTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EBulletinRecipientTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EBulletinRecipientTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EBulletinRecipientQuery
