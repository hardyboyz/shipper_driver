<?php

namespace Base;

use \UnitLayout as ChildUnitLayout;
use \UnitLayoutQuery as ChildUnitLayoutQuery;
use \Exception;
use \PDO;
use Map\UnitLayoutTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'unitLayout' table.
 *
 *
 *
 * @method     ChildUnitLayoutQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUnitLayoutQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildUnitLayoutQuery orderByFloorPlanName($order = Criteria::ASC) Order by the floorPlanName column
 * @method     ChildUnitLayoutQuery orderByFloorPlanImage($order = Criteria::ASC) Order by the floorPlanImage column
 * @method     ChildUnitLayoutQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildUnitLayoutQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildUnitLayoutQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildUnitLayoutQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildUnitLayoutQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildUnitLayoutQuery groupById() Group by the id column
 * @method     ChildUnitLayoutQuery groupByProjectId() Group by the projectId column
 * @method     ChildUnitLayoutQuery groupByFloorPlanName() Group by the floorPlanName column
 * @method     ChildUnitLayoutQuery groupByFloorPlanImage() Group by the floorPlanImage column
 * @method     ChildUnitLayoutQuery groupByStatus() Group by the status column
 * @method     ChildUnitLayoutQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildUnitLayoutQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildUnitLayoutQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildUnitLayoutQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildUnitLayoutQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUnitLayoutQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUnitLayoutQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUnitLayoutQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUnitLayoutQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUnitLayoutQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUnitLayout findOne(ConnectionInterface $con = null) Return the first ChildUnitLayout matching the query
 * @method     ChildUnitLayout findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUnitLayout matching the query, or a new ChildUnitLayout object populated from the query conditions when no match is found
 *
 * @method     ChildUnitLayout findOneById(string $id) Return the first ChildUnitLayout filtered by the id column
 * @method     ChildUnitLayout findOneByProjectId(string $projectId) Return the first ChildUnitLayout filtered by the projectId column
 * @method     ChildUnitLayout findOneByFloorPlanName(string $floorPlanName) Return the first ChildUnitLayout filtered by the floorPlanName column
 * @method     ChildUnitLayout findOneByFloorPlanImage(string $floorPlanImage) Return the first ChildUnitLayout filtered by the floorPlanImage column
 * @method     ChildUnitLayout findOneByStatus(int $status) Return the first ChildUnitLayout filtered by the status column
 * @method     ChildUnitLayout findOneByCreatedBy(string $created_by) Return the first ChildUnitLayout filtered by the created_by column
 * @method     ChildUnitLayout findOneByCreatedDate(string $created_date) Return the first ChildUnitLayout filtered by the created_date column
 * @method     ChildUnitLayout findOneByModifiedBy(string $modified_by) Return the first ChildUnitLayout filtered by the modified_by column
 * @method     ChildUnitLayout findOneByModifiedDate(string $modified_date) Return the first ChildUnitLayout filtered by the modified_date column *

 * @method     ChildUnitLayout requirePk($key, ConnectionInterface $con = null) Return the ChildUnitLayout by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitLayout requireOne(ConnectionInterface $con = null) Return the first ChildUnitLayout matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUnitLayout requireOneById(string $id) Return the first ChildUnitLayout filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitLayout requireOneByProjectId(string $projectId) Return the first ChildUnitLayout filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitLayout requireOneByFloorPlanName(string $floorPlanName) Return the first ChildUnitLayout filtered by the floorPlanName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitLayout requireOneByFloorPlanImage(string $floorPlanImage) Return the first ChildUnitLayout filtered by the floorPlanImage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitLayout requireOneByStatus(int $status) Return the first ChildUnitLayout filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitLayout requireOneByCreatedBy(string $created_by) Return the first ChildUnitLayout filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitLayout requireOneByCreatedDate(string $created_date) Return the first ChildUnitLayout filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitLayout requireOneByModifiedBy(string $modified_by) Return the first ChildUnitLayout filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUnitLayout requireOneByModifiedDate(string $modified_date) Return the first ChildUnitLayout filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUnitLayout[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUnitLayout objects based on current ModelCriteria
 * @method     ChildUnitLayout[]|ObjectCollection findById(string $id) Return ChildUnitLayout objects filtered by the id column
 * @method     ChildUnitLayout[]|ObjectCollection findByProjectId(string $projectId) Return ChildUnitLayout objects filtered by the projectId column
 * @method     ChildUnitLayout[]|ObjectCollection findByFloorPlanName(string $floorPlanName) Return ChildUnitLayout objects filtered by the floorPlanName column
 * @method     ChildUnitLayout[]|ObjectCollection findByFloorPlanImage(string $floorPlanImage) Return ChildUnitLayout objects filtered by the floorPlanImage column
 * @method     ChildUnitLayout[]|ObjectCollection findByStatus(int $status) Return ChildUnitLayout objects filtered by the status column
 * @method     ChildUnitLayout[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildUnitLayout objects filtered by the created_by column
 * @method     ChildUnitLayout[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildUnitLayout objects filtered by the created_date column
 * @method     ChildUnitLayout[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildUnitLayout objects filtered by the modified_by column
 * @method     ChildUnitLayout[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildUnitLayout objects filtered by the modified_date column
 * @method     ChildUnitLayout[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UnitLayoutQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UnitLayoutQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\UnitLayout', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUnitLayoutQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUnitLayoutQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUnitLayoutQuery) {
            return $criteria;
        }
        $query = new ChildUnitLayoutQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUnitLayout|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = UnitLayoutTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UnitLayoutTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUnitLayout A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, floorPlanName, floorPlanImage, status, created_by, created_date, modified_by, modified_date FROM unitLayout WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUnitLayout $obj */
            $obj = new ChildUnitLayout();
            $obj->hydrate($row);
            UnitLayoutTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUnitLayout|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UnitLayoutTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UnitLayoutTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnitLayoutTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnitLayoutTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the floorPlanName column
     *
     * Example usage:
     * <code>
     * $query->filterByFloorPlanName('fooValue');   // WHERE floorPlanName = 'fooValue'
     * $query->filterByFloorPlanName('%fooValue%'); // WHERE floorPlanName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $floorPlanName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByFloorPlanName($floorPlanName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($floorPlanName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $floorPlanName)) {
                $floorPlanName = str_replace('*', '%', $floorPlanName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnitLayoutTableMap::COL_FLOORPLANNAME, $floorPlanName, $comparison);
    }

    /**
     * Filter the query on the floorPlanImage column
     *
     * Example usage:
     * <code>
     * $query->filterByFloorPlanImage('fooValue');   // WHERE floorPlanImage = 'fooValue'
     * $query->filterByFloorPlanImage('%fooValue%'); // WHERE floorPlanImage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $floorPlanImage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByFloorPlanImage($floorPlanImage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($floorPlanImage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $floorPlanImage)) {
                $floorPlanImage = str_replace('*', '%', $floorPlanImage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnitLayoutTableMap::COL_FLOORPLANIMAGE, $floorPlanImage, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(UnitLayoutTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(UnitLayoutTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitLayoutTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnitLayoutTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(UnitLayoutTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(UnitLayoutTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitLayoutTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnitLayoutTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(UnitLayoutTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(UnitLayoutTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnitLayoutTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUnitLayout $unitLayout Object to remove from the list of results
     *
     * @return $this|ChildUnitLayoutQuery The current query, for fluid interface
     */
    public function prune($unitLayout = null)
    {
        if ($unitLayout) {
            $this->addUsingAlias(UnitLayoutTableMap::COL_ID, $unitLayout->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the unitLayout table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitLayoutTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UnitLayoutTableMap::clearInstancePool();
            UnitLayoutTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UnitLayoutTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UnitLayoutTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UnitLayoutTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UnitLayoutTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UnitLayoutQuery
