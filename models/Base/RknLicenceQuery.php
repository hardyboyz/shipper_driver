<?php

namespace Base;

use \RknLicence as ChildRknLicence;
use \RknLicenceQuery as ChildRknLicenceQuery;
use \Exception;
use \PDO;
use Map\RknLicenceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'rkn_license' table.
 *
 *
 *
 * @method     ChildRknLicenceQuery orderByKey($order = Criteria::ASC) Order by the key column
 * @method     ChildRknLicenceQuery orderByGenerateTime($order = Criteria::ASC) Order by the generateTime column
 * @method     ChildRknLicenceQuery orderByPackageID($order = Criteria::ASC) Order by the packageId column
 * @method     ChildRknLicenceQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildRknLicenceQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildRknLicenceQuery groupByKey() Group by the key column
 * @method     ChildRknLicenceQuery groupByGenerateTime() Group by the generateTime column
 * @method     ChildRknLicenceQuery groupByPackageID() Group by the packageId column
 * @method     ChildRknLicenceQuery groupByType() Group by the type column
 * @method     ChildRknLicenceQuery groupByStatus() Group by the status column
 *
 * @method     ChildRknLicenceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRknLicenceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRknLicenceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRknLicenceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRknLicenceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRknLicenceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRknLicenceQuery leftJoinRknPackage($relationAlias = null) Adds a LEFT JOIN clause to the query using the RknPackage relation
 * @method     ChildRknLicenceQuery rightJoinRknPackage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RknPackage relation
 * @method     ChildRknLicenceQuery innerJoinRknPackage($relationAlias = null) Adds a INNER JOIN clause to the query using the RknPackage relation
 *
 * @method     ChildRknLicenceQuery joinWithRknPackage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RknPackage relation
 *
 * @method     ChildRknLicenceQuery leftJoinWithRknPackage() Adds a LEFT JOIN clause and with to the query using the RknPackage relation
 * @method     ChildRknLicenceQuery rightJoinWithRknPackage() Adds a RIGHT JOIN clause and with to the query using the RknPackage relation
 * @method     ChildRknLicenceQuery innerJoinWithRknPackage() Adds a INNER JOIN clause and with to the query using the RknPackage relation
 *
 * @method     ChildRknLicenceQuery leftJoinRknLicenceType($relationAlias = null) Adds a LEFT JOIN clause to the query using the RknLicenceType relation
 * @method     ChildRknLicenceQuery rightJoinRknLicenceType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RknLicenceType relation
 * @method     ChildRknLicenceQuery innerJoinRknLicenceType($relationAlias = null) Adds a INNER JOIN clause to the query using the RknLicenceType relation
 *
 * @method     ChildRknLicenceQuery joinWithRknLicenceType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RknLicenceType relation
 *
 * @method     ChildRknLicenceQuery leftJoinWithRknLicenceType() Adds a LEFT JOIN clause and with to the query using the RknLicenceType relation
 * @method     ChildRknLicenceQuery rightJoinWithRknLicenceType() Adds a RIGHT JOIN clause and with to the query using the RknLicenceType relation
 * @method     ChildRknLicenceQuery innerJoinWithRknLicenceType() Adds a INNER JOIN clause and with to the query using the RknLicenceType relation
 *
 * @method     \RknPackageQuery|\RknLicenceTypeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRknLicence findOne(ConnectionInterface $con = null) Return the first ChildRknLicence matching the query
 * @method     ChildRknLicence findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRknLicence matching the query, or a new ChildRknLicence object populated from the query conditions when no match is found
 *
 * @method     ChildRknLicence findOneByKey(string $key) Return the first ChildRknLicence filtered by the key column
 * @method     ChildRknLicence findOneByGenerateTime(string $generateTime) Return the first ChildRknLicence filtered by the generateTime column
 * @method     ChildRknLicence findOneByPackageID(string $packageId) Return the first ChildRknLicence filtered by the packageId column
 * @method     ChildRknLicence findOneByType(int $type) Return the first ChildRknLicence filtered by the type column
 * @method     ChildRknLicence findOneByStatus(int $status) Return the first ChildRknLicence filtered by the status column *

 * @method     ChildRknLicence requirePk($key, ConnectionInterface $con = null) Return the ChildRknLicence by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknLicence requireOne(ConnectionInterface $con = null) Return the first ChildRknLicence matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRknLicence requireOneByKey(string $key) Return the first ChildRknLicence filtered by the key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknLicence requireOneByGenerateTime(string $generateTime) Return the first ChildRknLicence filtered by the generateTime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknLicence requireOneByPackageID(string $packageId) Return the first ChildRknLicence filtered by the packageId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknLicence requireOneByType(int $type) Return the first ChildRknLicence filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknLicence requireOneByStatus(int $status) Return the first ChildRknLicence filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRknLicence[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRknLicence objects based on current ModelCriteria
 * @method     ChildRknLicence[]|ObjectCollection findByKey(string $key) Return ChildRknLicence objects filtered by the key column
 * @method     ChildRknLicence[]|ObjectCollection findByGenerateTime(string $generateTime) Return ChildRknLicence objects filtered by the generateTime column
 * @method     ChildRknLicence[]|ObjectCollection findByPackageID(string $packageId) Return ChildRknLicence objects filtered by the packageId column
 * @method     ChildRknLicence[]|ObjectCollection findByType(int $type) Return ChildRknLicence objects filtered by the type column
 * @method     ChildRknLicence[]|ObjectCollection findByStatus(int $status) Return ChildRknLicence objects filtered by the status column
 * @method     ChildRknLicence[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RknLicenceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RknLicenceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\RknLicence', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRknLicenceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRknLicenceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRknLicenceQuery) {
            return $criteria;
        }
        $query = new ChildRknLicenceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRknLicence|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RknLicenceTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RknLicenceTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRknLicence A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT key, generateTime, packageId, type, status FROM rkn_license WHERE key = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRknLicence $obj */
            $obj = new ChildRknLicence();
            $obj->hydrate($row);
            RknLicenceTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRknLicence|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RknLicenceTableMap::COL_KEY, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RknLicenceTableMap::COL_KEY, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the key column
     *
     * Example usage:
     * <code>
     * $query->filterByKey('fooValue');   // WHERE key = 'fooValue'
     * $query->filterByKey('%fooValue%'); // WHERE key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $key The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function filterByKey($key = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($key)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $key)) {
                $key = str_replace('*', '%', $key);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RknLicenceTableMap::COL_KEY, $key, $comparison);
    }

    /**
     * Filter the query on the generateTime column
     *
     * Example usage:
     * <code>
     * $query->filterByGenerateTime('2011-03-14'); // WHERE generateTime = '2011-03-14'
     * $query->filterByGenerateTime('now'); // WHERE generateTime = '2011-03-14'
     * $query->filterByGenerateTime(array('max' => 'yesterday')); // WHERE generateTime > '2011-03-13'
     * </code>
     *
     * @param     mixed $generateTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function filterByGenerateTime($generateTime = null, $comparison = null)
    {
        if (is_array($generateTime)) {
            $useMinMax = false;
            if (isset($generateTime['min'])) {
                $this->addUsingAlias(RknLicenceTableMap::COL_GENERATETIME, $generateTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($generateTime['max'])) {
                $this->addUsingAlias(RknLicenceTableMap::COL_GENERATETIME, $generateTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RknLicenceTableMap::COL_GENERATETIME, $generateTime, $comparison);
    }

    /**
     * Filter the query on the packageId column
     *
     * Example usage:
     * <code>
     * $query->filterByPackageID('fooValue');   // WHERE packageId = 'fooValue'
     * $query->filterByPackageID('%fooValue%'); // WHERE packageId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $packageID The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function filterByPackageID($packageID = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($packageID)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $packageID)) {
                $packageID = str_replace('*', '%', $packageID);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RknLicenceTableMap::COL_PACKAGEID, $packageID, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType(1234); // WHERE type = 1234
     * $query->filterByType(array(12, 34)); // WHERE type IN (12, 34)
     * $query->filterByType(array('min' => 12)); // WHERE type > 12
     * </code>
     *
     * @see       filterByRknLicenceType()
     *
     * @param     mixed $type The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (is_array($type)) {
            $useMinMax = false;
            if (isset($type['min'])) {
                $this->addUsingAlias(RknLicenceTableMap::COL_TYPE, $type['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($type['max'])) {
                $this->addUsingAlias(RknLicenceTableMap::COL_TYPE, $type['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RknLicenceTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(RknLicenceTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(RknLicenceTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RknLicenceTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query by a related \RknPackage object
     *
     * @param \RknPackage|ObjectCollection $rknPackage The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRknLicenceQuery The current query, for fluid interface
     */
    public function filterByRknPackage($rknPackage, $comparison = null)
    {
        if ($rknPackage instanceof \RknPackage) {
            return $this
                ->addUsingAlias(RknLicenceTableMap::COL_PACKAGEID, $rknPackage->getID(), $comparison);
        } elseif ($rknPackage instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RknLicenceTableMap::COL_PACKAGEID, $rknPackage->toKeyValue('PrimaryKey', 'ID'), $comparison);
        } else {
            throw new PropelException('filterByRknPackage() only accepts arguments of type \RknPackage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RknPackage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function joinRknPackage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RknPackage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RknPackage');
        }

        return $this;
    }

    /**
     * Use the RknPackage relation RknPackage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RknPackageQuery A secondary query class using the current class as primary query
     */
    public function useRknPackageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRknPackage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RknPackage', '\RknPackageQuery');
    }

    /**
     * Filter the query by a related \RknLicenceType object
     *
     * @param \RknLicenceType|ObjectCollection $rknLicenceType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRknLicenceQuery The current query, for fluid interface
     */
    public function filterByRknLicenceType($rknLicenceType, $comparison = null)
    {
        if ($rknLicenceType instanceof \RknLicenceType) {
            return $this
                ->addUsingAlias(RknLicenceTableMap::COL_TYPE, $rknLicenceType->getID(), $comparison);
        } elseif ($rknLicenceType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RknLicenceTableMap::COL_TYPE, $rknLicenceType->toKeyValue('PrimaryKey', 'ID'), $comparison);
        } else {
            throw new PropelException('filterByRknLicenceType() only accepts arguments of type \RknLicenceType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RknLicenceType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function joinRknLicenceType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RknLicenceType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RknLicenceType');
        }

        return $this;
    }

    /**
     * Use the RknLicenceType relation RknLicenceType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RknLicenceTypeQuery A secondary query class using the current class as primary query
     */
    public function useRknLicenceTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRknLicenceType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RknLicenceType', '\RknLicenceTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRknLicence $rknLicence Object to remove from the list of results
     *
     * @return $this|ChildRknLicenceQuery The current query, for fluid interface
     */
    public function prune($rknLicence = null)
    {
        if ($rknLicence) {
            $this->addUsingAlias(RknLicenceTableMap::COL_KEY, $rknLicence->getKey(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the rkn_license table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RknLicenceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RknLicenceTableMap::clearInstancePool();
            RknLicenceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RknLicenceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RknLicenceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RknLicenceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RknLicenceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RknLicenceQuery
