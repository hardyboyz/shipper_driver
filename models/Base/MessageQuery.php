<?php

namespace Base;

use \Message as ChildMessage;
use \MessageQuery as ChildMessageQuery;
use \Exception;
use \PDO;
use Map\MessageTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'message' table.
 *
 *
 *
 * @method     ChildMessageQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMessageQuery orderBySenderTime($order = Criteria::ASC) Order by the sendertime column
 * @method     ChildMessageQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildMessageQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildMessageQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildMessageQuery orderByModifiedBy($order = Criteria::ASC) Order by the modifed_by column
 * @method     ChildMessageQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 * @method     ChildMessageQuery orderByChatroomId($order = Criteria::ASC) Order by the chatroom_id column
 * @method     ChildMessageQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildMessageQuery orderByUnitId($order = Criteria::ASC) Order by the unit_id column
 * @method     ChildMessageQuery orderByMessage($order = Criteria::ASC) Order by the message column
 * @method     ChildMessageQuery orderByEpochTime($order = Criteria::ASC) Order by the epochtime column
 *
 * @method     ChildMessageQuery groupById() Group by the id column
 * @method     ChildMessageQuery groupBySenderTime() Group by the sendertime column
 * @method     ChildMessageQuery groupByProjectId() Group by the project_id column
 * @method     ChildMessageQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildMessageQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildMessageQuery groupByModifiedBy() Group by the modifed_by column
 * @method     ChildMessageQuery groupByModifiedDate() Group by the modified_date column
 * @method     ChildMessageQuery groupByChatroomId() Group by the chatroom_id column
 * @method     ChildMessageQuery groupByUserId() Group by the user_id column
 * @method     ChildMessageQuery groupByUnitId() Group by the unit_id column
 * @method     ChildMessageQuery groupByMessage() Group by the message column
 * @method     ChildMessageQuery groupByEpochTime() Group by the epochtime column
 *
 * @method     ChildMessageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMessageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMessageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMessageQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMessageQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMessageQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMessage findOne(ConnectionInterface $con = null) Return the first ChildMessage matching the query
 * @method     ChildMessage findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMessage matching the query, or a new ChildMessage object populated from the query conditions when no match is found
 *
 * @method     ChildMessage findOneById(string $id) Return the first ChildMessage filtered by the id column
 * @method     ChildMessage findOneBySenderTime(string $sendertime) Return the first ChildMessage filtered by the sendertime column
 * @method     ChildMessage findOneByProjectId(string $project_id) Return the first ChildMessage filtered by the project_id column
 * @method     ChildMessage findOneByCreatedBy(string $created_by) Return the first ChildMessage filtered by the created_by column
 * @method     ChildMessage findOneByCreatedDate(string $created_date) Return the first ChildMessage filtered by the created_date column
 * @method     ChildMessage findOneByModifiedBy(string $modifed_by) Return the first ChildMessage filtered by the modifed_by column
 * @method     ChildMessage findOneByModifiedDate(string $modified_date) Return the first ChildMessage filtered by the modified_date column
 * @method     ChildMessage findOneByChatroomId(string $chatroom_id) Return the first ChildMessage filtered by the chatroom_id column
 * @method     ChildMessage findOneByUserId(string $user_id) Return the first ChildMessage filtered by the user_id column
 * @method     ChildMessage findOneByUnitId(string $unit_id) Return the first ChildMessage filtered by the unit_id column
 * @method     ChildMessage findOneByMessage(string $message) Return the first ChildMessage filtered by the message column
 * @method     ChildMessage findOneByEpochTime(string $epochtime) Return the first ChildMessage filtered by the epochtime column *

 * @method     ChildMessage requirePk($key, ConnectionInterface $con = null) Return the ChildMessage by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOne(ConnectionInterface $con = null) Return the first ChildMessage matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMessage requireOneById(string $id) Return the first ChildMessage filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneBySenderTime(string $sendertime) Return the first ChildMessage filtered by the sendertime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByProjectId(string $project_id) Return the first ChildMessage filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByCreatedBy(string $created_by) Return the first ChildMessage filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByCreatedDate(string $created_date) Return the first ChildMessage filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByModifiedBy(string $modifed_by) Return the first ChildMessage filtered by the modifed_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByModifiedDate(string $modified_date) Return the first ChildMessage filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByChatroomId(string $chatroom_id) Return the first ChildMessage filtered by the chatroom_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByUserId(string $user_id) Return the first ChildMessage filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByUnitId(string $unit_id) Return the first ChildMessage filtered by the unit_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByMessage(string $message) Return the first ChildMessage filtered by the message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMessage requireOneByEpochTime(string $epochtime) Return the first ChildMessage filtered by the epochtime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMessage[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMessage objects based on current ModelCriteria
 * @method     ChildMessage[]|ObjectCollection findById(string $id) Return ChildMessage objects filtered by the id column
 * @method     ChildMessage[]|ObjectCollection findBySenderTime(string $sendertime) Return ChildMessage objects filtered by the sendertime column
 * @method     ChildMessage[]|ObjectCollection findByProjectId(string $project_id) Return ChildMessage objects filtered by the project_id column
 * @method     ChildMessage[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildMessage objects filtered by the created_by column
 * @method     ChildMessage[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildMessage objects filtered by the created_date column
 * @method     ChildMessage[]|ObjectCollection findByModifiedBy(string $modifed_by) Return ChildMessage objects filtered by the modifed_by column
 * @method     ChildMessage[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildMessage objects filtered by the modified_date column
 * @method     ChildMessage[]|ObjectCollection findByChatroomId(string $chatroom_id) Return ChildMessage objects filtered by the chatroom_id column
 * @method     ChildMessage[]|ObjectCollection findByUserId(string $user_id) Return ChildMessage objects filtered by the user_id column
 * @method     ChildMessage[]|ObjectCollection findByUnitId(string $unit_id) Return ChildMessage objects filtered by the unit_id column
 * @method     ChildMessage[]|ObjectCollection findByMessage(string $message) Return ChildMessage objects filtered by the message column
 * @method     ChildMessage[]|ObjectCollection findByEpochTime(string $epochtime) Return ChildMessage objects filtered by the epochtime column
 * @method     ChildMessage[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MessageQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MessageQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Message', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMessageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMessageQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMessageQuery) {
            return $criteria;
        }
        $query = new ChildMessageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMessage|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MessageTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MessageTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMessage A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, sendertime, project_id, created_by, created_date, modifed_by, modified_date, chatroom_id, user_id, unit_id, message, epochtime FROM message WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMessage $obj */
            $obj = new ChildMessage();
            $obj->hydrate($row);
            MessageTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMessage|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MessageTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MessageTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the sendertime column
     *
     * Example usage:
     * <code>
     * $query->filterBySenderTime('fooValue');   // WHERE sendertime = 'fooValue'
     * $query->filterBySenderTime('%fooValue%'); // WHERE sendertime LIKE '%fooValue%'
     * </code>
     *
     * @param     string $senderTime The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterBySenderTime($senderTime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($senderTime)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $senderTime)) {
                $senderTime = str_replace('*', '%', $senderTime);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_SENDERTIME, $senderTime, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(MessageTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(MessageTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modifed_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modifed_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modifed_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_MODIFED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(MessageTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(MessageTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query on the chatroom_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatroomId('fooValue');   // WHERE chatroom_id = 'fooValue'
     * $query->filterByChatroomId('%fooValue%'); // WHERE chatroom_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chatroomId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByChatroomId($chatroomId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chatroomId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $chatroomId)) {
                $chatroomId = str_replace('*', '%', $chatroomId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_CHATROOM_ID, $chatroomId, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId('fooValue');   // WHERE user_id = 'fooValue'
     * $query->filterByUserId('%fooValue%'); // WHERE user_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $userId)) {
                $userId = str_replace('*', '%', $userId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the unit_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId('fooValue');   // WHERE unit_id = 'fooValue'
     * $query->filterByUnitId('%fooValue%'); // WHERE unit_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitId)) {
                $unitId = str_replace('*', '%', $unitId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_UNIT_ID, $unitId, $comparison);
    }

    /**
     * Filter the query on the message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
     * $query->filterByMessage('%fooValue%'); // WHERE message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $message)) {
                $message = str_replace('*', '%', $message);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the epochtime column
     *
     * Example usage:
     * <code>
     * $query->filterByEpochTime('fooValue');   // WHERE epochtime = 'fooValue'
     * $query->filterByEpochTime('%fooValue%'); // WHERE epochtime LIKE '%fooValue%'
     * </code>
     *
     * @param     string $epochTime The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function filterByEpochTime($epochTime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($epochTime)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $epochTime)) {
                $epochTime = str_replace('*', '%', $epochTime);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MessageTableMap::COL_EPOCHTIME, $epochTime, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMessage $message Object to remove from the list of results
     *
     * @return $this|ChildMessageQuery The current query, for fluid interface
     */
    public function prune($message = null)
    {
        if ($message) {
            $this->addUsingAlias(MessageTableMap::COL_ID, $message->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the message table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MessageTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MessageTableMap::clearInstancePool();
            MessageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MessageTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MessageTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MessageTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MessageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MessageQuery
