<?php

namespace Base;

use \ChatRoom as ChildChatRoom;
use \ChatRoomQuery as ChildChatRoomQuery;
use \Exception;
use \PDO;
use Map\ChatRoomTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'chatroom' table.
 *
 *
 *
 * @method     ChildChatRoomQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildChatRoomQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildChatRoomQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildChatRoomQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildChatRoomQuery orderByModifiedBy($order = Criteria::ASC) Order by the modifed_by column
 * @method     ChildChatRoomQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 * @method     ChildChatRoomQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildChatRoomQuery orderByAdminId($order = Criteria::ASC) Order by the admin_id column
 * @method     ChildChatRoomQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildChatRoomQuery orderByTotalMessage($order = Criteria::ASC) Order by the total_message column
 * @method     ChildChatRoomQuery orderByReportAbuseFlag($order = Criteria::ASC) Order by the report_abuse_flag column
 * @method     ChildChatRoomQuery orderByReportAbuseBy($order = Criteria::ASC) Order by the report_abuse_by column
 * @method     ChildChatRoomQuery orderByReportAbuseDate($order = Criteria::ASC) Order by the report_abuse_date column
 * @method     ChildChatRoomQuery orderBySchedulerModified($order = Criteria::ASC) Order by the scheduler_modified column
 * @method     ChildChatRoomQuery orderByReason($order = Criteria::ASC) Order by the reason column
 * @method     ChildChatRoomQuery orderByModifiedByAdmin($order = Criteria::ASC) Order by the modifed_by_admin column
 * @method     ChildChatRoomQuery orderByModifiedDateByAdmin($order = Criteria::ASC) Order by the modified_date_by_admin column
 *
 * @method     ChildChatRoomQuery groupById() Group by the id column
 * @method     ChildChatRoomQuery groupByProjectId() Group by the project_id column
 * @method     ChildChatRoomQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildChatRoomQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildChatRoomQuery groupByModifiedBy() Group by the modifed_by column
 * @method     ChildChatRoomQuery groupByModifiedDate() Group by the modified_date column
 * @method     ChildChatRoomQuery groupByTitle() Group by the title column
 * @method     ChildChatRoomQuery groupByAdminId() Group by the admin_id column
 * @method     ChildChatRoomQuery groupByStatus() Group by the status column
 * @method     ChildChatRoomQuery groupByTotalMessage() Group by the total_message column
 * @method     ChildChatRoomQuery groupByReportAbuseFlag() Group by the report_abuse_flag column
 * @method     ChildChatRoomQuery groupByReportAbuseBy() Group by the report_abuse_by column
 * @method     ChildChatRoomQuery groupByReportAbuseDate() Group by the report_abuse_date column
 * @method     ChildChatRoomQuery groupBySchedulerModified() Group by the scheduler_modified column
 * @method     ChildChatRoomQuery groupByReason() Group by the reason column
 * @method     ChildChatRoomQuery groupByModifiedByAdmin() Group by the modifed_by_admin column
 * @method     ChildChatRoomQuery groupByModifiedDateByAdmin() Group by the modified_date_by_admin column
 *
 * @method     ChildChatRoomQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildChatRoomQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildChatRoomQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildChatRoomQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildChatRoomQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildChatRoomQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildChatRoomQuery leftJoinChatroomSetting($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatroomSetting relation
 * @method     ChildChatRoomQuery rightJoinChatroomSetting($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatroomSetting relation
 * @method     ChildChatRoomQuery innerJoinChatroomSetting($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatroomSetting relation
 *
 * @method     ChildChatRoomQuery joinWithChatroomSetting($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatroomSetting relation
 *
 * @method     ChildChatRoomQuery leftJoinWithChatroomSetting() Adds a LEFT JOIN clause and with to the query using the ChatroomSetting relation
 * @method     ChildChatRoomQuery rightJoinWithChatroomSetting() Adds a RIGHT JOIN clause and with to the query using the ChatroomSetting relation
 * @method     ChildChatRoomQuery innerJoinWithChatroomSetting() Adds a INNER JOIN clause and with to the query using the ChatroomSetting relation
 *
 * @method     \ChatroomSettingQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildChatRoom findOne(ConnectionInterface $con = null) Return the first ChildChatRoom matching the query
 * @method     ChildChatRoom findOneOrCreate(ConnectionInterface $con = null) Return the first ChildChatRoom matching the query, or a new ChildChatRoom object populated from the query conditions when no match is found
 *
 * @method     ChildChatRoom findOneById(int $id) Return the first ChildChatRoom filtered by the id column
 * @method     ChildChatRoom findOneByProjectId(string $project_id) Return the first ChildChatRoom filtered by the project_id column
 * @method     ChildChatRoom findOneByCreatedBy(string $created_by) Return the first ChildChatRoom filtered by the created_by column
 * @method     ChildChatRoom findOneByCreatedDate(string $created_date) Return the first ChildChatRoom filtered by the created_date column
 * @method     ChildChatRoom findOneByModifiedBy(string $modifed_by) Return the first ChildChatRoom filtered by the modifed_by column
 * @method     ChildChatRoom findOneByModifiedDate(string $modified_date) Return the first ChildChatRoom filtered by the modified_date column
 * @method     ChildChatRoom findOneByTitle(string $title) Return the first ChildChatRoom filtered by the title column
 * @method     ChildChatRoom findOneByAdminId(string $admin_id) Return the first ChildChatRoom filtered by the admin_id column
 * @method     ChildChatRoom findOneByStatus(int $status) Return the first ChildChatRoom filtered by the status column
 * @method     ChildChatRoom findOneByTotalMessage(string $total_message) Return the first ChildChatRoom filtered by the total_message column
 * @method     ChildChatRoom findOneByReportAbuseFlag(string $report_abuse_flag) Return the first ChildChatRoom filtered by the report_abuse_flag column
 * @method     ChildChatRoom findOneByReportAbuseBy(string $report_abuse_by) Return the first ChildChatRoom filtered by the report_abuse_by column
 * @method     ChildChatRoom findOneByReportAbuseDate(string $report_abuse_date) Return the first ChildChatRoom filtered by the report_abuse_date column
 * @method     ChildChatRoom findOneBySchedulerModified(string $scheduler_modified) Return the first ChildChatRoom filtered by the scheduler_modified column
 * @method     ChildChatRoom findOneByReason(string $reason) Return the first ChildChatRoom filtered by the reason column
 * @method     ChildChatRoom findOneByModifiedByAdmin(string $modifed_by_admin) Return the first ChildChatRoom filtered by the modifed_by_admin column
 * @method     ChildChatRoom findOneByModifiedDateByAdmin(string $modified_date_by_admin) Return the first ChildChatRoom filtered by the modified_date_by_admin column *

 * @method     ChildChatRoom requirePk($key, ConnectionInterface $con = null) Return the ChildChatRoom by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOne(ConnectionInterface $con = null) Return the first ChildChatRoom matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatRoom requireOneById(int $id) Return the first ChildChatRoom filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByProjectId(string $project_id) Return the first ChildChatRoom filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByCreatedBy(string $created_by) Return the first ChildChatRoom filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByCreatedDate(string $created_date) Return the first ChildChatRoom filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByModifiedBy(string $modifed_by) Return the first ChildChatRoom filtered by the modifed_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByModifiedDate(string $modified_date) Return the first ChildChatRoom filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByTitle(string $title) Return the first ChildChatRoom filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByAdminId(string $admin_id) Return the first ChildChatRoom filtered by the admin_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByStatus(int $status) Return the first ChildChatRoom filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByTotalMessage(string $total_message) Return the first ChildChatRoom filtered by the total_message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByReportAbuseFlag(string $report_abuse_flag) Return the first ChildChatRoom filtered by the report_abuse_flag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByReportAbuseBy(string $report_abuse_by) Return the first ChildChatRoom filtered by the report_abuse_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByReportAbuseDate(string $report_abuse_date) Return the first ChildChatRoom filtered by the report_abuse_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneBySchedulerModified(string $scheduler_modified) Return the first ChildChatRoom filtered by the scheduler_modified column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByReason(string $reason) Return the first ChildChatRoom filtered by the reason column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByModifiedByAdmin(string $modifed_by_admin) Return the first ChildChatRoom filtered by the modifed_by_admin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatRoom requireOneByModifiedDateByAdmin(string $modified_date_by_admin) Return the first ChildChatRoom filtered by the modified_date_by_admin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatRoom[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildChatRoom objects based on current ModelCriteria
 * @method     ChildChatRoom[]|ObjectCollection findById(int $id) Return ChildChatRoom objects filtered by the id column
 * @method     ChildChatRoom[]|ObjectCollection findByProjectId(string $project_id) Return ChildChatRoom objects filtered by the project_id column
 * @method     ChildChatRoom[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildChatRoom objects filtered by the created_by column
 * @method     ChildChatRoom[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildChatRoom objects filtered by the created_date column
 * @method     ChildChatRoom[]|ObjectCollection findByModifiedBy(string $modifed_by) Return ChildChatRoom objects filtered by the modifed_by column
 * @method     ChildChatRoom[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildChatRoom objects filtered by the modified_date column
 * @method     ChildChatRoom[]|ObjectCollection findByTitle(string $title) Return ChildChatRoom objects filtered by the title column
 * @method     ChildChatRoom[]|ObjectCollection findByAdminId(string $admin_id) Return ChildChatRoom objects filtered by the admin_id column
 * @method     ChildChatRoom[]|ObjectCollection findByStatus(int $status) Return ChildChatRoom objects filtered by the status column
 * @method     ChildChatRoom[]|ObjectCollection findByTotalMessage(string $total_message) Return ChildChatRoom objects filtered by the total_message column
 * @method     ChildChatRoom[]|ObjectCollection findByReportAbuseFlag(string $report_abuse_flag) Return ChildChatRoom objects filtered by the report_abuse_flag column
 * @method     ChildChatRoom[]|ObjectCollection findByReportAbuseBy(string $report_abuse_by) Return ChildChatRoom objects filtered by the report_abuse_by column
 * @method     ChildChatRoom[]|ObjectCollection findByReportAbuseDate(string $report_abuse_date) Return ChildChatRoom objects filtered by the report_abuse_date column
 * @method     ChildChatRoom[]|ObjectCollection findBySchedulerModified(string $scheduler_modified) Return ChildChatRoom objects filtered by the scheduler_modified column
 * @method     ChildChatRoom[]|ObjectCollection findByReason(string $reason) Return ChildChatRoom objects filtered by the reason column
 * @method     ChildChatRoom[]|ObjectCollection findByModifiedByAdmin(string $modifed_by_admin) Return ChildChatRoom objects filtered by the modifed_by_admin column
 * @method     ChildChatRoom[]|ObjectCollection findByModifiedDateByAdmin(string $modified_date_by_admin) Return ChildChatRoom objects filtered by the modified_date_by_admin column
 * @method     ChildChatRoom[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ChatRoomQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ChatRoomQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ChatRoom', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildChatRoomQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildChatRoomQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildChatRoomQuery) {
            return $criteria;
        }
        $query = new ChildChatRoomQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildChatRoom|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ChatRoomTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChatRoomTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatRoom A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, project_id, created_by, created_date, modifed_by, modified_date, title, admin_id, status, total_message, report_abuse_flag, report_abuse_by, report_abuse_date, scheduler_modified, reason, modifed_by_admin, modified_date_by_admin FROM chatroom WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildChatRoom $obj */
            $obj = new ChildChatRoom();
            $obj->hydrate($row);
            ChatRoomTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildChatRoom|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ChatRoomTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ChatRoomTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modifed_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modifed_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modifed_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_MODIFED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the admin_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAdminId('fooValue');   // WHERE admin_id = 'fooValue'
     * $query->filterByAdminId('%fooValue%'); // WHERE admin_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adminId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByAdminId($adminId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adminId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adminId)) {
                $adminId = str_replace('*', '%', $adminId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_ADMIN_ID, $adminId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the total_message column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalMessage('fooValue');   // WHERE total_message = 'fooValue'
     * $query->filterByTotalMessage('%fooValue%'); // WHERE total_message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $totalMessage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByTotalMessage($totalMessage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($totalMessage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $totalMessage)) {
                $totalMessage = str_replace('*', '%', $totalMessage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_TOTAL_MESSAGE, $totalMessage, $comparison);
    }

    /**
     * Filter the query on the report_abuse_flag column
     *
     * Example usage:
     * <code>
     * $query->filterByReportAbuseFlag('fooValue');   // WHERE report_abuse_flag = 'fooValue'
     * $query->filterByReportAbuseFlag('%fooValue%'); // WHERE report_abuse_flag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reportAbuseFlag The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByReportAbuseFlag($reportAbuseFlag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reportAbuseFlag)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $reportAbuseFlag)) {
                $reportAbuseFlag = str_replace('*', '%', $reportAbuseFlag);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_REPORT_ABUSE_FLAG, $reportAbuseFlag, $comparison);
    }

    /**
     * Filter the query on the report_abuse_by column
     *
     * Example usage:
     * <code>
     * $query->filterByReportAbuseBy('fooValue');   // WHERE report_abuse_by = 'fooValue'
     * $query->filterByReportAbuseBy('%fooValue%'); // WHERE report_abuse_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reportAbuseBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByReportAbuseBy($reportAbuseBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reportAbuseBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $reportAbuseBy)) {
                $reportAbuseBy = str_replace('*', '%', $reportAbuseBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_REPORT_ABUSE_BY, $reportAbuseBy, $comparison);
    }

    /**
     * Filter the query on the report_abuse_date column
     *
     * Example usage:
     * <code>
     * $query->filterByReportAbuseDate('2011-03-14'); // WHERE report_abuse_date = '2011-03-14'
     * $query->filterByReportAbuseDate('now'); // WHERE report_abuse_date = '2011-03-14'
     * $query->filterByReportAbuseDate(array('max' => 'yesterday')); // WHERE report_abuse_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $reportAbuseDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByReportAbuseDate($reportAbuseDate = null, $comparison = null)
    {
        if (is_array($reportAbuseDate)) {
            $useMinMax = false;
            if (isset($reportAbuseDate['min'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_REPORT_ABUSE_DATE, $reportAbuseDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($reportAbuseDate['max'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_REPORT_ABUSE_DATE, $reportAbuseDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_REPORT_ABUSE_DATE, $reportAbuseDate, $comparison);
    }

    /**
     * Filter the query on the scheduler_modified column
     *
     * Example usage:
     * <code>
     * $query->filterBySchedulerModified('2011-03-14'); // WHERE scheduler_modified = '2011-03-14'
     * $query->filterBySchedulerModified('now'); // WHERE scheduler_modified = '2011-03-14'
     * $query->filterBySchedulerModified(array('max' => 'yesterday')); // WHERE scheduler_modified > '2011-03-13'
     * </code>
     *
     * @param     mixed $schedulerModified The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterBySchedulerModified($schedulerModified = null, $comparison = null)
    {
        if (is_array($schedulerModified)) {
            $useMinMax = false;
            if (isset($schedulerModified['min'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_SCHEDULER_MODIFIED, $schedulerModified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($schedulerModified['max'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_SCHEDULER_MODIFIED, $schedulerModified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_SCHEDULER_MODIFIED, $schedulerModified, $comparison);
    }

    /**
     * Filter the query on the reason column
     *
     * Example usage:
     * <code>
     * $query->filterByReason('fooValue');   // WHERE reason = 'fooValue'
     * $query->filterByReason('%fooValue%'); // WHERE reason LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reason The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByReason($reason = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reason)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $reason)) {
                $reason = str_replace('*', '%', $reason);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_REASON, $reason, $comparison);
    }

    /**
     * Filter the query on the modifed_by_admin column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedByAdmin('fooValue');   // WHERE modifed_by_admin = 'fooValue'
     * $query->filterByModifiedByAdmin('%fooValue%'); // WHERE modifed_by_admin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedByAdmin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByModifiedByAdmin($modifiedByAdmin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedByAdmin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedByAdmin)) {
                $modifiedByAdmin = str_replace('*', '%', $modifiedByAdmin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_MODIFED_BY_ADMIN, $modifiedByAdmin, $comparison);
    }

    /**
     * Filter the query on the modified_date_by_admin column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDateByAdmin('2011-03-14'); // WHERE modified_date_by_admin = '2011-03-14'
     * $query->filterByModifiedDateByAdmin('now'); // WHERE modified_date_by_admin = '2011-03-14'
     * $query->filterByModifiedDateByAdmin(array('max' => 'yesterday')); // WHERE modified_date_by_admin > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDateByAdmin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByModifiedDateByAdmin($modifiedDateByAdmin = null, $comparison = null)
    {
        if (is_array($modifiedDateByAdmin)) {
            $useMinMax = false;
            if (isset($modifiedDateByAdmin['min'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_MODIFIED_DATE_BY_ADMIN, $modifiedDateByAdmin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDateByAdmin['max'])) {
                $this->addUsingAlias(ChatRoomTableMap::COL_MODIFIED_DATE_BY_ADMIN, $modifiedDateByAdmin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatRoomTableMap::COL_MODIFIED_DATE_BY_ADMIN, $modifiedDateByAdmin, $comparison);
    }

    /**
     * Filter the query by a related \ChatroomSetting object
     *
     * @param \ChatroomSetting|ObjectCollection $chatroomSetting the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildChatRoomQuery The current query, for fluid interface
     */
    public function filterByChatroomSetting($chatroomSetting, $comparison = null)
    {
        if ($chatroomSetting instanceof \ChatroomSetting) {
            return $this
                ->addUsingAlias(ChatRoomTableMap::COL_ID, $chatroomSetting->getChatroomId(), $comparison);
        } elseif ($chatroomSetting instanceof ObjectCollection) {
            return $this
                ->useChatroomSettingQuery()
                ->filterByPrimaryKeys($chatroomSetting->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChatroomSetting() only accepts arguments of type \ChatroomSetting or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatroomSetting relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function joinChatroomSetting($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatroomSetting');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatroomSetting');
        }

        return $this;
    }

    /**
     * Use the ChatroomSetting relation ChatroomSetting object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ChatroomSettingQuery A secondary query class using the current class as primary query
     */
    public function useChatroomSettingQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatroomSetting($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatroomSetting', '\ChatroomSettingQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildChatRoom $chatRoom Object to remove from the list of results
     *
     * @return $this|ChildChatRoomQuery The current query, for fluid interface
     */
    public function prune($chatRoom = null)
    {
        if ($chatRoom) {
            $this->addUsingAlias(ChatRoomTableMap::COL_ID, $chatRoom->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the chatroom table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatRoomTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChatRoomTableMap::clearInstancePool();
            ChatRoomTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatRoomTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ChatRoomTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ChatRoomTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ChatRoomTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ChatRoomQuery
