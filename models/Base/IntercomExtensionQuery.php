<?php

namespace Base;

use \IntercomExtension as ChildIntercomExtension;
use \IntercomExtensionQuery as ChildIntercomExtensionQuery;
use \Exception;
use \PDO;
use Map\IntercomExtensionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'intercomExtension' table.
 *
 *
 *
 * @method     ChildIntercomExtensionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildIntercomExtensionQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildIntercomExtensionQuery orderByIntercomTo($order = Criteria::ASC) Order by the intercomTo column
 * @method     ChildIntercomExtensionQuery orderByExtensionNo($order = Criteria::ASC) Order by the extensionNo column
 * @method     ChildIntercomExtensionQuery orderByIpAddress($order = Criteria::ASC) Order by the ipAddress column
 * @method     ChildIntercomExtensionQuery orderByCameraType($order = Criteria::ASC) Order by the cameraType column
 * @method     ChildIntercomExtensionQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildIntercomExtensionQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildIntercomExtensionQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildIntercomExtensionQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildIntercomExtensionQuery groupById() Group by the id column
 * @method     ChildIntercomExtensionQuery groupByProjectId() Group by the projectId column
 * @method     ChildIntercomExtensionQuery groupByIntercomTo() Group by the intercomTo column
 * @method     ChildIntercomExtensionQuery groupByExtensionNo() Group by the extensionNo column
 * @method     ChildIntercomExtensionQuery groupByIpAddress() Group by the ipAddress column
 * @method     ChildIntercomExtensionQuery groupByCameraType() Group by the cameraType column
 * @method     ChildIntercomExtensionQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildIntercomExtensionQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildIntercomExtensionQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildIntercomExtensionQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildIntercomExtensionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildIntercomExtensionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildIntercomExtensionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildIntercomExtensionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildIntercomExtensionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildIntercomExtensionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildIntercomExtension findOne(ConnectionInterface $con = null) Return the first ChildIntercomExtension matching the query
 * @method     ChildIntercomExtension findOneOrCreate(ConnectionInterface $con = null) Return the first ChildIntercomExtension matching the query, or a new ChildIntercomExtension object populated from the query conditions when no match is found
 *
 * @method     ChildIntercomExtension findOneById(string $id) Return the first ChildIntercomExtension filtered by the id column
 * @method     ChildIntercomExtension findOneByProjectId(string $projectId) Return the first ChildIntercomExtension filtered by the projectId column
 * @method     ChildIntercomExtension findOneByIntercomTo(string $intercomTo) Return the first ChildIntercomExtension filtered by the intercomTo column
 * @method     ChildIntercomExtension findOneByExtensionNo(string $extensionNo) Return the first ChildIntercomExtension filtered by the extensionNo column
 * @method     ChildIntercomExtension findOneByIpAddress(string $ipAddress) Return the first ChildIntercomExtension filtered by the ipAddress column
 * @method     ChildIntercomExtension findOneByCameraType(string $cameraType) Return the first ChildIntercomExtension filtered by the cameraType column
 * @method     ChildIntercomExtension findOneByCreatedBy(string $created_by) Return the first ChildIntercomExtension filtered by the created_by column
 * @method     ChildIntercomExtension findOneByCreatedDate(string $created_date) Return the first ChildIntercomExtension filtered by the created_date column
 * @method     ChildIntercomExtension findOneByModifiedBy(string $modified_by) Return the first ChildIntercomExtension filtered by the modified_by column
 * @method     ChildIntercomExtension findOneByModifiedDate(string $modified_date) Return the first ChildIntercomExtension filtered by the modified_date column *

 * @method     ChildIntercomExtension requirePk($key, ConnectionInterface $con = null) Return the ChildIntercomExtension by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOne(ConnectionInterface $con = null) Return the first ChildIntercomExtension matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildIntercomExtension requireOneById(string $id) Return the first ChildIntercomExtension filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOneByProjectId(string $projectId) Return the first ChildIntercomExtension filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOneByIntercomTo(string $intercomTo) Return the first ChildIntercomExtension filtered by the intercomTo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOneByExtensionNo(string $extensionNo) Return the first ChildIntercomExtension filtered by the extensionNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOneByIpAddress(string $ipAddress) Return the first ChildIntercomExtension filtered by the ipAddress column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOneByCameraType(string $cameraType) Return the first ChildIntercomExtension filtered by the cameraType column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOneByCreatedBy(string $created_by) Return the first ChildIntercomExtension filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOneByCreatedDate(string $created_date) Return the first ChildIntercomExtension filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOneByModifiedBy(string $modified_by) Return the first ChildIntercomExtension filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildIntercomExtension requireOneByModifiedDate(string $modified_date) Return the first ChildIntercomExtension filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildIntercomExtension[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildIntercomExtension objects based on current ModelCriteria
 * @method     ChildIntercomExtension[]|ObjectCollection findById(string $id) Return ChildIntercomExtension objects filtered by the id column
 * @method     ChildIntercomExtension[]|ObjectCollection findByProjectId(string $projectId) Return ChildIntercomExtension objects filtered by the projectId column
 * @method     ChildIntercomExtension[]|ObjectCollection findByIntercomTo(string $intercomTo) Return ChildIntercomExtension objects filtered by the intercomTo column
 * @method     ChildIntercomExtension[]|ObjectCollection findByExtensionNo(string $extensionNo) Return ChildIntercomExtension objects filtered by the extensionNo column
 * @method     ChildIntercomExtension[]|ObjectCollection findByIpAddress(string $ipAddress) Return ChildIntercomExtension objects filtered by the ipAddress column
 * @method     ChildIntercomExtension[]|ObjectCollection findByCameraType(string $cameraType) Return ChildIntercomExtension objects filtered by the cameraType column
 * @method     ChildIntercomExtension[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildIntercomExtension objects filtered by the created_by column
 * @method     ChildIntercomExtension[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildIntercomExtension objects filtered by the created_date column
 * @method     ChildIntercomExtension[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildIntercomExtension objects filtered by the modified_by column
 * @method     ChildIntercomExtension[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildIntercomExtension objects filtered by the modified_date column
 * @method     ChildIntercomExtension[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class IntercomExtensionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\IntercomExtensionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IntercomExtension', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildIntercomExtensionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildIntercomExtensionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildIntercomExtensionQuery) {
            return $criteria;
        }
        $query = new ChildIntercomExtensionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildIntercomExtension|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = IntercomExtensionTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(IntercomExtensionTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildIntercomExtension A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, intercomTo, extensionNo, ipAddress, cameraType, created_by, created_date, modified_by, modified_date FROM intercomExtension WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildIntercomExtension $obj */
            $obj = new ChildIntercomExtension();
            $obj->hydrate($row);
            IntercomExtensionTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildIntercomExtension|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the intercomTo column
     *
     * Example usage:
     * <code>
     * $query->filterByIntercomTo('fooValue');   // WHERE intercomTo = 'fooValue'
     * $query->filterByIntercomTo('%fooValue%'); // WHERE intercomTo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $intercomTo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByIntercomTo($intercomTo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($intercomTo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $intercomTo)) {
                $intercomTo = str_replace('*', '%', $intercomTo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_INTERCOMTO, $intercomTo, $comparison);
    }

    /**
     * Filter the query on the extensionNo column
     *
     * Example usage:
     * <code>
     * $query->filterByExtensionNo('fooValue');   // WHERE extensionNo = 'fooValue'
     * $query->filterByExtensionNo('%fooValue%'); // WHERE extensionNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extensionNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByExtensionNo($extensionNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extensionNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $extensionNo)) {
                $extensionNo = str_replace('*', '%', $extensionNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_EXTENSIONNO, $extensionNo, $comparison);
    }

    /**
     * Filter the query on the ipAddress column
     *
     * Example usage:
     * <code>
     * $query->filterByIpAddress('fooValue');   // WHERE ipAddress = 'fooValue'
     * $query->filterByIpAddress('%fooValue%'); // WHERE ipAddress LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipAddress The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByIpAddress($ipAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipAddress)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ipAddress)) {
                $ipAddress = str_replace('*', '%', $ipAddress);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_IPADDRESS, $ipAddress, $comparison);
    }

    /**
     * Filter the query on the cameraType column
     *
     * Example usage:
     * <code>
     * $query->filterByCameraType('fooValue');   // WHERE cameraType = 'fooValue'
     * $query->filterByCameraType('%fooValue%'); // WHERE cameraType LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cameraType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByCameraType($cameraType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cameraType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cameraType)) {
                $cameraType = str_replace('*', '%', $cameraType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_CAMERATYPE, $cameraType, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(IntercomExtensionTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(IntercomExtensionTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(IntercomExtensionTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(IntercomExtensionTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(IntercomExtensionTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildIntercomExtension $intercomExtension Object to remove from the list of results
     *
     * @return $this|ChildIntercomExtensionQuery The current query, for fluid interface
     */
    public function prune($intercomExtension = null)
    {
        if ($intercomExtension) {
            $this->addUsingAlias(IntercomExtensionTableMap::COL_ID, $intercomExtension->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the intercomExtension table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IntercomExtensionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            IntercomExtensionTableMap::clearInstancePool();
            IntercomExtensionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(IntercomExtensionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(IntercomExtensionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            IntercomExtensionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            IntercomExtensionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // IntercomExtensionQuery
