<?php

namespace Base;

use \PropertyProject as ChildPropertyProject;
use \PropertyProjectQuery as ChildPropertyProjectQuery;
use \Exception;
use \PDO;
use Map\PropertyProjectTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'propertyProject' table.
 *
 *
 *
 * @method     ChildPropertyProjectQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPropertyProjectQuery orderByProjectPrefix($order = Criteria::ASC) Order by the projectPrefix column
 * @method     ChildPropertyProjectQuery orderByProjectName($order = Criteria::ASC) Order by the projectName column
 * @method     ChildPropertyProjectQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildPropertyProjectQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildPropertyProjectQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildPropertyProjectQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildPropertyProjectQuery groupById() Group by the id column
 * @method     ChildPropertyProjectQuery groupByProjectPrefix() Group by the projectPrefix column
 * @method     ChildPropertyProjectQuery groupByProjectName() Group by the projectName column
 * @method     ChildPropertyProjectQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildPropertyProjectQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildPropertyProjectQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildPropertyProjectQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildPropertyProjectQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPropertyProjectQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPropertyProjectQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPropertyProjectQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPropertyProjectQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPropertyProjectQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPropertyProject findOne(ConnectionInterface $con = null) Return the first ChildPropertyProject matching the query
 * @method     ChildPropertyProject findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPropertyProject matching the query, or a new ChildPropertyProject object populated from the query conditions when no match is found
 *
 * @method     ChildPropertyProject findOneById(string $id) Return the first ChildPropertyProject filtered by the id column
 * @method     ChildPropertyProject findOneByProjectPrefix(string $projectPrefix) Return the first ChildPropertyProject filtered by the projectPrefix column
 * @method     ChildPropertyProject findOneByProjectName(string $projectName) Return the first ChildPropertyProject filtered by the projectName column
 * @method     ChildPropertyProject findOneByCreatedBy(string $created_by) Return the first ChildPropertyProject filtered by the created_by column
 * @method     ChildPropertyProject findOneByCreatedDate(string $created_date) Return the first ChildPropertyProject filtered by the created_date column
 * @method     ChildPropertyProject findOneByModifiedBy(string $modified_by) Return the first ChildPropertyProject filtered by the modified_by column
 * @method     ChildPropertyProject findOneByModifiedDate(string $modified_date) Return the first ChildPropertyProject filtered by the modified_date column *

 * @method     ChildPropertyProject requirePk($key, ConnectionInterface $con = null) Return the ChildPropertyProject by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyProject requireOne(ConnectionInterface $con = null) Return the first ChildPropertyProject matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPropertyProject requireOneById(string $id) Return the first ChildPropertyProject filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyProject requireOneByProjectPrefix(string $projectPrefix) Return the first ChildPropertyProject filtered by the projectPrefix column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyProject requireOneByProjectName(string $projectName) Return the first ChildPropertyProject filtered by the projectName column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyProject requireOneByCreatedBy(string $created_by) Return the first ChildPropertyProject filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyProject requireOneByCreatedDate(string $created_date) Return the first ChildPropertyProject filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyProject requireOneByModifiedBy(string $modified_by) Return the first ChildPropertyProject filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyProject requireOneByModifiedDate(string $modified_date) Return the first ChildPropertyProject filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPropertyProject[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPropertyProject objects based on current ModelCriteria
 * @method     ChildPropertyProject[]|ObjectCollection findById(string $id) Return ChildPropertyProject objects filtered by the id column
 * @method     ChildPropertyProject[]|ObjectCollection findByProjectPrefix(string $projectPrefix) Return ChildPropertyProject objects filtered by the projectPrefix column
 * @method     ChildPropertyProject[]|ObjectCollection findByProjectName(string $projectName) Return ChildPropertyProject objects filtered by the projectName column
 * @method     ChildPropertyProject[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildPropertyProject objects filtered by the created_by column
 * @method     ChildPropertyProject[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildPropertyProject objects filtered by the created_date column
 * @method     ChildPropertyProject[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildPropertyProject objects filtered by the modified_by column
 * @method     ChildPropertyProject[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildPropertyProject objects filtered by the modified_date column
 * @method     ChildPropertyProject[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PropertyProjectQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PropertyProjectQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\PropertyProject', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPropertyProjectQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPropertyProjectQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPropertyProjectQuery) {
            return $criteria;
        }
        $query = new ChildPropertyProjectQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPropertyProject|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PropertyProjectTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PropertyProjectTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPropertyProject A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectPrefix, projectName, created_by, created_date, modified_by, modified_date FROM propertyProject WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPropertyProject $obj */
            $obj = new ChildPropertyProject();
            $obj->hydrate($row);
            PropertyProjectTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPropertyProject|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PropertyProjectTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PropertyProjectTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyProjectTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectPrefix column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectPrefix('fooValue');   // WHERE projectPrefix = 'fooValue'
     * $query->filterByProjectPrefix('%fooValue%'); // WHERE projectPrefix LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectPrefix The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function filterByProjectPrefix($projectPrefix = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectPrefix)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectPrefix)) {
                $projectPrefix = str_replace('*', '%', $projectPrefix);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyProjectTableMap::COL_PROJECTPREFIX, $projectPrefix, $comparison);
    }

    /**
     * Filter the query on the projectName column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectName('fooValue');   // WHERE projectName = 'fooValue'
     * $query->filterByProjectName('%fooValue%'); // WHERE projectName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function filterByProjectName($projectName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectName)) {
                $projectName = str_replace('*', '%', $projectName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyProjectTableMap::COL_PROJECTNAME, $projectName, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyProjectTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(PropertyProjectTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(PropertyProjectTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyProjectTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PropertyProjectTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(PropertyProjectTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(PropertyProjectTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyProjectTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPropertyProject $propertyProject Object to remove from the list of results
     *
     * @return $this|ChildPropertyProjectQuery The current query, for fluid interface
     */
    public function prune($propertyProject = null)
    {
        if ($propertyProject) {
            $this->addUsingAlias(PropertyProjectTableMap::COL_ID, $propertyProject->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the propertyProject table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyProjectTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PropertyProjectTableMap::clearInstancePool();
            PropertyProjectTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyProjectTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PropertyProjectTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PropertyProjectTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PropertyProjectTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PropertyProjectQuery
