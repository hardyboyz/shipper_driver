<?php

namespace Base;

use \FacilityBooking as ChildFacilityBooking;
use \FacilityBookingQuery as ChildFacilityBookingQuery;
use \Exception;
use \PDO;
use Map\FacilityBookingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'facility_booking' table.
 *
 *
 *
 * @method     ChildFacilityBookingQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFacilityBookingQuery orderByBookingNumber($order = Criteria::ASC) Order by the booking_number column
 * @method     ChildFacilityBookingQuery orderByFacilityId($order = Criteria::ASC) Order by the facility_id column
 * @method     ChildFacilityBookingQuery orderByFacilityName($order = Criteria::ASC) Order by the facility_name column
 * @method     ChildFacilityBookingQuery orderByBookingDate($order = Criteria::ASC) Order by the booking_date column
 * @method     ChildFacilityBookingQuery orderByBookingTime($order = Criteria::ASC) Order by the booking_time column
 * @method     ChildFacilityBookingQuery orderByUnitId($order = Criteria::ASC) Order by the unit_id column
 * @method     ChildFacilityBookingQuery orderByBookingTimeslot($order = Criteria::ASC) Order by the booking_timeslot column
 * @method     ChildFacilityBookingQuery orderByBookingOwner($order = Criteria::ASC) Order by the booking_owner column
 * @method     ChildFacilityBookingQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildFacilityBookingQuery orderByTenantId($order = Criteria::ASC) Order by the tenant_id column
 * @method     ChildFacilityBookingQuery orderByBookingStatus($order = Criteria::ASC) Order by the booking_status column
 * @method     ChildFacilityBookingQuery orderByPaymentStatus($order = Criteria::ASC) Order by the payment_status column
 * @method     ChildFacilityBookingQuery orderByPaymentDate($order = Criteria::ASC) Order by the payment_date column
 * @method     ChildFacilityBookingQuery orderByCancelDate($order = Criteria::ASC) Order by the cancel_date column
 * @method     ChildFacilityBookingQuery orderByCancelReason($order = Criteria::ASC) Order by the cancel_reason column
 * @method     ChildFacilityBookingQuery orderByCancelBy($order = Criteria::ASC) Order by the cancel_by column
 * @method     ChildFacilityBookingQuery orderByDeposit($order = Criteria::ASC) Order by the deposit column
 * @method     ChildFacilityBookingQuery orderByCharge($order = Criteria::ASC) Order by the charge column
 * @method     ChildFacilityBookingQuery orderByTotal($order = Criteria::ASC) Order by the total column
 * @method     ChildFacilityBookingQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildFacilityBookingQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildFacilityBookingQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildFacilityBookingQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 * @method     ChildFacilityBookingQuery orderByUniqId($order = Criteria::ASC) Order by the uniqid column
 *
 * @method     ChildFacilityBookingQuery groupById() Group by the id column
 * @method     ChildFacilityBookingQuery groupByBookingNumber() Group by the booking_number column
 * @method     ChildFacilityBookingQuery groupByFacilityId() Group by the facility_id column
 * @method     ChildFacilityBookingQuery groupByFacilityName() Group by the facility_name column
 * @method     ChildFacilityBookingQuery groupByBookingDate() Group by the booking_date column
 * @method     ChildFacilityBookingQuery groupByBookingTime() Group by the booking_time column
 * @method     ChildFacilityBookingQuery groupByUnitId() Group by the unit_id column
 * @method     ChildFacilityBookingQuery groupByBookingTimeslot() Group by the booking_timeslot column
 * @method     ChildFacilityBookingQuery groupByBookingOwner() Group by the booking_owner column
 * @method     ChildFacilityBookingQuery groupByProjectId() Group by the project_id column
 * @method     ChildFacilityBookingQuery groupByTenantId() Group by the tenant_id column
 * @method     ChildFacilityBookingQuery groupByBookingStatus() Group by the booking_status column
 * @method     ChildFacilityBookingQuery groupByPaymentStatus() Group by the payment_status column
 * @method     ChildFacilityBookingQuery groupByPaymentDate() Group by the payment_date column
 * @method     ChildFacilityBookingQuery groupByCancelDate() Group by the cancel_date column
 * @method     ChildFacilityBookingQuery groupByCancelReason() Group by the cancel_reason column
 * @method     ChildFacilityBookingQuery groupByCancelBy() Group by the cancel_by column
 * @method     ChildFacilityBookingQuery groupByDeposit() Group by the deposit column
 * @method     ChildFacilityBookingQuery groupByCharge() Group by the charge column
 * @method     ChildFacilityBookingQuery groupByTotal() Group by the total column
 * @method     ChildFacilityBookingQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildFacilityBookingQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildFacilityBookingQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildFacilityBookingQuery groupByModifiedDate() Group by the modified_date column
 * @method     ChildFacilityBookingQuery groupByUniqId() Group by the uniqid column
 *
 * @method     ChildFacilityBookingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFacilityBookingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFacilityBookingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFacilityBookingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFacilityBookingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFacilityBookingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFacilityBooking findOne(ConnectionInterface $con = null) Return the first ChildFacilityBooking matching the query
 * @method     ChildFacilityBooking findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFacilityBooking matching the query, or a new ChildFacilityBooking object populated from the query conditions when no match is found
 *
 * @method     ChildFacilityBooking findOneById(string $id) Return the first ChildFacilityBooking filtered by the id column
 * @method     ChildFacilityBooking findOneByBookingNumber(string $booking_number) Return the first ChildFacilityBooking filtered by the booking_number column
 * @method     ChildFacilityBooking findOneByFacilityId(string $facility_id) Return the first ChildFacilityBooking filtered by the facility_id column
 * @method     ChildFacilityBooking findOneByFacilityName(string $facility_name) Return the first ChildFacilityBooking filtered by the facility_name column
 * @method     ChildFacilityBooking findOneByBookingDate(string $booking_date) Return the first ChildFacilityBooking filtered by the booking_date column
 * @method     ChildFacilityBooking findOneByBookingTime(string $booking_time) Return the first ChildFacilityBooking filtered by the booking_time column
 * @method     ChildFacilityBooking findOneByUnitId(string $unit_id) Return the first ChildFacilityBooking filtered by the unit_id column
 * @method     ChildFacilityBooking findOneByBookingTimeslot(string $booking_timeslot) Return the first ChildFacilityBooking filtered by the booking_timeslot column
 * @method     ChildFacilityBooking findOneByBookingOwner(string $booking_owner) Return the first ChildFacilityBooking filtered by the booking_owner column
 * @method     ChildFacilityBooking findOneByProjectId(string $project_id) Return the first ChildFacilityBooking filtered by the project_id column
 * @method     ChildFacilityBooking findOneByTenantId(string $tenant_id) Return the first ChildFacilityBooking filtered by the tenant_id column
 * @method     ChildFacilityBooking findOneByBookingStatus(string $booking_status) Return the first ChildFacilityBooking filtered by the booking_status column
 * @method     ChildFacilityBooking findOneByPaymentStatus(string $payment_status) Return the first ChildFacilityBooking filtered by the payment_status column
 * @method     ChildFacilityBooking findOneByPaymentDate(string $payment_date) Return the first ChildFacilityBooking filtered by the payment_date column
 * @method     ChildFacilityBooking findOneByCancelDate(string $cancel_date) Return the first ChildFacilityBooking filtered by the cancel_date column
 * @method     ChildFacilityBooking findOneByCancelReason(string $cancel_reason) Return the first ChildFacilityBooking filtered by the cancel_reason column
 * @method     ChildFacilityBooking findOneByCancelBy(string $cancel_by) Return the first ChildFacilityBooking filtered by the cancel_by column
 * @method     ChildFacilityBooking findOneByDeposit(double $deposit) Return the first ChildFacilityBooking filtered by the deposit column
 * @method     ChildFacilityBooking findOneByCharge(double $charge) Return the first ChildFacilityBooking filtered by the charge column
 * @method     ChildFacilityBooking findOneByTotal(double $total) Return the first ChildFacilityBooking filtered by the total column
 * @method     ChildFacilityBooking findOneByCreatedBy(string $created_by) Return the first ChildFacilityBooking filtered by the created_by column
 * @method     ChildFacilityBooking findOneByCreatedDate(string $created_date) Return the first ChildFacilityBooking filtered by the created_date column
 * @method     ChildFacilityBooking findOneByModifiedBy(string $modified_by) Return the first ChildFacilityBooking filtered by the modified_by column
 * @method     ChildFacilityBooking findOneByModifiedDate(string $modified_date) Return the first ChildFacilityBooking filtered by the modified_date column
 * @method     ChildFacilityBooking findOneByUniqId(string $uniqid) Return the first ChildFacilityBooking filtered by the uniqid column *

 * @method     ChildFacilityBooking requirePk($key, ConnectionInterface $con = null) Return the ChildFacilityBooking by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOne(ConnectionInterface $con = null) Return the first ChildFacilityBooking matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFacilityBooking requireOneById(string $id) Return the first ChildFacilityBooking filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByBookingNumber(string $booking_number) Return the first ChildFacilityBooking filtered by the booking_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByFacilityId(string $facility_id) Return the first ChildFacilityBooking filtered by the facility_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByFacilityName(string $facility_name) Return the first ChildFacilityBooking filtered by the facility_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByBookingDate(string $booking_date) Return the first ChildFacilityBooking filtered by the booking_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByBookingTime(string $booking_time) Return the first ChildFacilityBooking filtered by the booking_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByUnitId(string $unit_id) Return the first ChildFacilityBooking filtered by the unit_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByBookingTimeslot(string $booking_timeslot) Return the first ChildFacilityBooking filtered by the booking_timeslot column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByBookingOwner(string $booking_owner) Return the first ChildFacilityBooking filtered by the booking_owner column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByProjectId(string $project_id) Return the first ChildFacilityBooking filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByTenantId(string $tenant_id) Return the first ChildFacilityBooking filtered by the tenant_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByBookingStatus(string $booking_status) Return the first ChildFacilityBooking filtered by the booking_status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByPaymentStatus(string $payment_status) Return the first ChildFacilityBooking filtered by the payment_status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByPaymentDate(string $payment_date) Return the first ChildFacilityBooking filtered by the payment_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByCancelDate(string $cancel_date) Return the first ChildFacilityBooking filtered by the cancel_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByCancelReason(string $cancel_reason) Return the first ChildFacilityBooking filtered by the cancel_reason column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByCancelBy(string $cancel_by) Return the first ChildFacilityBooking filtered by the cancel_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByDeposit(double $deposit) Return the first ChildFacilityBooking filtered by the deposit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByCharge(double $charge) Return the first ChildFacilityBooking filtered by the charge column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByTotal(double $total) Return the first ChildFacilityBooking filtered by the total column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByCreatedBy(string $created_by) Return the first ChildFacilityBooking filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByCreatedDate(string $created_date) Return the first ChildFacilityBooking filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByModifiedBy(string $modified_by) Return the first ChildFacilityBooking filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByModifiedDate(string $modified_date) Return the first ChildFacilityBooking filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityBooking requireOneByUniqId(string $uniqid) Return the first ChildFacilityBooking filtered by the uniqid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFacilityBooking[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFacilityBooking objects based on current ModelCriteria
 * @method     ChildFacilityBooking[]|ObjectCollection findById(string $id) Return ChildFacilityBooking objects filtered by the id column
 * @method     ChildFacilityBooking[]|ObjectCollection findByBookingNumber(string $booking_number) Return ChildFacilityBooking objects filtered by the booking_number column
 * @method     ChildFacilityBooking[]|ObjectCollection findByFacilityId(string $facility_id) Return ChildFacilityBooking objects filtered by the facility_id column
 * @method     ChildFacilityBooking[]|ObjectCollection findByFacilityName(string $facility_name) Return ChildFacilityBooking objects filtered by the facility_name column
 * @method     ChildFacilityBooking[]|ObjectCollection findByBookingDate(string $booking_date) Return ChildFacilityBooking objects filtered by the booking_date column
 * @method     ChildFacilityBooking[]|ObjectCollection findByBookingTime(string $booking_time) Return ChildFacilityBooking objects filtered by the booking_time column
 * @method     ChildFacilityBooking[]|ObjectCollection findByUnitId(string $unit_id) Return ChildFacilityBooking objects filtered by the unit_id column
 * @method     ChildFacilityBooking[]|ObjectCollection findByBookingTimeslot(string $booking_timeslot) Return ChildFacilityBooking objects filtered by the booking_timeslot column
 * @method     ChildFacilityBooking[]|ObjectCollection findByBookingOwner(string $booking_owner) Return ChildFacilityBooking objects filtered by the booking_owner column
 * @method     ChildFacilityBooking[]|ObjectCollection findByProjectId(string $project_id) Return ChildFacilityBooking objects filtered by the project_id column
 * @method     ChildFacilityBooking[]|ObjectCollection findByTenantId(string $tenant_id) Return ChildFacilityBooking objects filtered by the tenant_id column
 * @method     ChildFacilityBooking[]|ObjectCollection findByBookingStatus(string $booking_status) Return ChildFacilityBooking objects filtered by the booking_status column
 * @method     ChildFacilityBooking[]|ObjectCollection findByPaymentStatus(string $payment_status) Return ChildFacilityBooking objects filtered by the payment_status column
 * @method     ChildFacilityBooking[]|ObjectCollection findByPaymentDate(string $payment_date) Return ChildFacilityBooking objects filtered by the payment_date column
 * @method     ChildFacilityBooking[]|ObjectCollection findByCancelDate(string $cancel_date) Return ChildFacilityBooking objects filtered by the cancel_date column
 * @method     ChildFacilityBooking[]|ObjectCollection findByCancelReason(string $cancel_reason) Return ChildFacilityBooking objects filtered by the cancel_reason column
 * @method     ChildFacilityBooking[]|ObjectCollection findByCancelBy(string $cancel_by) Return ChildFacilityBooking objects filtered by the cancel_by column
 * @method     ChildFacilityBooking[]|ObjectCollection findByDeposit(double $deposit) Return ChildFacilityBooking objects filtered by the deposit column
 * @method     ChildFacilityBooking[]|ObjectCollection findByCharge(double $charge) Return ChildFacilityBooking objects filtered by the charge column
 * @method     ChildFacilityBooking[]|ObjectCollection findByTotal(double $total) Return ChildFacilityBooking objects filtered by the total column
 * @method     ChildFacilityBooking[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildFacilityBooking objects filtered by the created_by column
 * @method     ChildFacilityBooking[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildFacilityBooking objects filtered by the created_date column
 * @method     ChildFacilityBooking[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildFacilityBooking objects filtered by the modified_by column
 * @method     ChildFacilityBooking[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildFacilityBooking objects filtered by the modified_date column
 * @method     ChildFacilityBooking[]|ObjectCollection findByUniqId(string $uniqid) Return ChildFacilityBooking objects filtered by the uniqid column
 * @method     ChildFacilityBooking[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FacilityBookingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\FacilityBookingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\FacilityBooking', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFacilityBookingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFacilityBookingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFacilityBookingQuery) {
            return $criteria;
        }
        $query = new ChildFacilityBookingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFacilityBooking|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FacilityBookingTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FacilityBookingTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFacilityBooking A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, booking_number, facility_id, facility_name, booking_date, booking_time, unit_id, booking_timeslot, booking_owner, project_id, tenant_id, booking_status, payment_status, payment_date, cancel_date, cancel_reason, cancel_by, deposit, charge, total, created_by, created_date, modified_by, modified_date, uniqid FROM facility_booking WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFacilityBooking $obj */
            $obj = new ChildFacilityBooking();
            $obj->hydrate($row);
            FacilityBookingTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFacilityBooking|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FacilityBookingTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FacilityBookingTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the booking_number column
     *
     * Example usage:
     * <code>
     * $query->filterByBookingNumber('fooValue');   // WHERE booking_number = 'fooValue'
     * $query->filterByBookingNumber('%fooValue%'); // WHERE booking_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bookingNumber The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByBookingNumber($bookingNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bookingNumber)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bookingNumber)) {
                $bookingNumber = str_replace('*', '%', $bookingNumber);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_BOOKING_NUMBER, $bookingNumber, $comparison);
    }

    /**
     * Filter the query on the facility_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFacilityId('fooValue');   // WHERE facility_id = 'fooValue'
     * $query->filterByFacilityId('%fooValue%'); // WHERE facility_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $facilityId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByFacilityId($facilityId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($facilityId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $facilityId)) {
                $facilityId = str_replace('*', '%', $facilityId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_FACILITY_ID, $facilityId, $comparison);
    }

    /**
     * Filter the query on the facility_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFacilityName('fooValue');   // WHERE facility_name = 'fooValue'
     * $query->filterByFacilityName('%fooValue%'); // WHERE facility_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $facilityName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByFacilityName($facilityName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($facilityName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $facilityName)) {
                $facilityName = str_replace('*', '%', $facilityName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_FACILITY_NAME, $facilityName, $comparison);
    }

    /**
     * Filter the query on the booking_date column
     *
     * Example usage:
     * <code>
     * $query->filterByBookingDate('2011-03-14'); // WHERE booking_date = '2011-03-14'
     * $query->filterByBookingDate('now'); // WHERE booking_date = '2011-03-14'
     * $query->filterByBookingDate(array('max' => 'yesterday')); // WHERE booking_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $bookingDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByBookingDate($bookingDate = null, $comparison = null)
    {
        if (is_array($bookingDate)) {
            $useMinMax = false;
            if (isset($bookingDate['min'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_BOOKING_DATE, $bookingDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bookingDate['max'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_BOOKING_DATE, $bookingDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_BOOKING_DATE, $bookingDate, $comparison);
    }

    /**
     * Filter the query on the booking_time column
     *
     * Example usage:
     * <code>
     * $query->filterByBookingTime('fooValue');   // WHERE booking_time = 'fooValue'
     * $query->filterByBookingTime('%fooValue%'); // WHERE booking_time LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bookingTime The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByBookingTime($bookingTime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bookingTime)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bookingTime)) {
                $bookingTime = str_replace('*', '%', $bookingTime);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_BOOKING_TIME, $bookingTime, $comparison);
    }

    /**
     * Filter the query on the unit_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId('fooValue');   // WHERE unit_id = 'fooValue'
     * $query->filterByUnitId('%fooValue%'); // WHERE unit_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitId)) {
                $unitId = str_replace('*', '%', $unitId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_UNIT_ID, $unitId, $comparison);
    }

    /**
     * Filter the query on the booking_timeslot column
     *
     * Example usage:
     * <code>
     * $query->filterByBookingTimeslot('fooValue');   // WHERE booking_timeslot = 'fooValue'
     * $query->filterByBookingTimeslot('%fooValue%'); // WHERE booking_timeslot LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bookingTimeslot The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByBookingTimeslot($bookingTimeslot = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bookingTimeslot)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bookingTimeslot)) {
                $bookingTimeslot = str_replace('*', '%', $bookingTimeslot);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_BOOKING_TIMESLOT, $bookingTimeslot, $comparison);
    }

    /**
     * Filter the query on the booking_owner column
     *
     * Example usage:
     * <code>
     * $query->filterByBookingOwner('fooValue');   // WHERE booking_owner = 'fooValue'
     * $query->filterByBookingOwner('%fooValue%'); // WHERE booking_owner LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bookingOwner The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByBookingOwner($bookingOwner = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bookingOwner)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bookingOwner)) {
                $bookingOwner = str_replace('*', '%', $bookingOwner);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_BOOKING_OWNER, $bookingOwner, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the tenant_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTenantId('fooValue');   // WHERE tenant_id = 'fooValue'
     * $query->filterByTenantId('%fooValue%'); // WHERE tenant_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tenantId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByTenantId($tenantId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tenantId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tenantId)) {
                $tenantId = str_replace('*', '%', $tenantId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_TENANT_ID, $tenantId, $comparison);
    }

    /**
     * Filter the query on the booking_status column
     *
     * Example usage:
     * <code>
     * $query->filterByBookingStatus('fooValue');   // WHERE booking_status = 'fooValue'
     * $query->filterByBookingStatus('%fooValue%'); // WHERE booking_status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bookingStatus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByBookingStatus($bookingStatus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bookingStatus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bookingStatus)) {
                $bookingStatus = str_replace('*', '%', $bookingStatus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_BOOKING_STATUS, $bookingStatus, $comparison);
    }

    /**
     * Filter the query on the payment_status column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentStatus('fooValue');   // WHERE payment_status = 'fooValue'
     * $query->filterByPaymentStatus('%fooValue%'); // WHERE payment_status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentStatus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByPaymentStatus($paymentStatus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentStatus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paymentStatus)) {
                $paymentStatus = str_replace('*', '%', $paymentStatus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_PAYMENT_STATUS, $paymentStatus, $comparison);
    }

    /**
     * Filter the query on the payment_date column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentDate('2011-03-14'); // WHERE payment_date = '2011-03-14'
     * $query->filterByPaymentDate('now'); // WHERE payment_date = '2011-03-14'
     * $query->filterByPaymentDate(array('max' => 'yesterday')); // WHERE payment_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $paymentDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByPaymentDate($paymentDate = null, $comparison = null)
    {
        if (is_array($paymentDate)) {
            $useMinMax = false;
            if (isset($paymentDate['min'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_PAYMENT_DATE, $paymentDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paymentDate['max'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_PAYMENT_DATE, $paymentDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_PAYMENT_DATE, $paymentDate, $comparison);
    }

    /**
     * Filter the query on the cancel_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCancelDate('2011-03-14'); // WHERE cancel_date = '2011-03-14'
     * $query->filterByCancelDate('now'); // WHERE cancel_date = '2011-03-14'
     * $query->filterByCancelDate(array('max' => 'yesterday')); // WHERE cancel_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $cancelDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByCancelDate($cancelDate = null, $comparison = null)
    {
        if (is_array($cancelDate)) {
            $useMinMax = false;
            if (isset($cancelDate['min'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_CANCEL_DATE, $cancelDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cancelDate['max'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_CANCEL_DATE, $cancelDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_CANCEL_DATE, $cancelDate, $comparison);
    }

    /**
     * Filter the query on the cancel_reason column
     *
     * Example usage:
     * <code>
     * $query->filterByCancelReason('fooValue');   // WHERE cancel_reason = 'fooValue'
     * $query->filterByCancelReason('%fooValue%'); // WHERE cancel_reason LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cancelReason The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByCancelReason($cancelReason = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cancelReason)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cancelReason)) {
                $cancelReason = str_replace('*', '%', $cancelReason);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_CANCEL_REASON, $cancelReason, $comparison);
    }

    /**
     * Filter the query on the cancel_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCancelBy('fooValue');   // WHERE cancel_by = 'fooValue'
     * $query->filterByCancelBy('%fooValue%'); // WHERE cancel_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cancelBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByCancelBy($cancelBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cancelBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cancelBy)) {
                $cancelBy = str_replace('*', '%', $cancelBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_CANCEL_BY, $cancelBy, $comparison);
    }

    /**
     * Filter the query on the deposit column
     *
     * Example usage:
     * <code>
     * $query->filterByDeposit(1234); // WHERE deposit = 1234
     * $query->filterByDeposit(array(12, 34)); // WHERE deposit IN (12, 34)
     * $query->filterByDeposit(array('min' => 12)); // WHERE deposit > 12
     * </code>
     *
     * @param     mixed $deposit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByDeposit($deposit = null, $comparison = null)
    {
        if (is_array($deposit)) {
            $useMinMax = false;
            if (isset($deposit['min'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_DEPOSIT, $deposit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deposit['max'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_DEPOSIT, $deposit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_DEPOSIT, $deposit, $comparison);
    }

    /**
     * Filter the query on the charge column
     *
     * Example usage:
     * <code>
     * $query->filterByCharge(1234); // WHERE charge = 1234
     * $query->filterByCharge(array(12, 34)); // WHERE charge IN (12, 34)
     * $query->filterByCharge(array('min' => 12)); // WHERE charge > 12
     * </code>
     *
     * @param     mixed $charge The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByCharge($charge = null, $comparison = null)
    {
        if (is_array($charge)) {
            $useMinMax = false;
            if (isset($charge['min'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_CHARGE, $charge['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($charge['max'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_CHARGE, $charge['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_CHARGE, $charge, $comparison);
    }

    /**
     * Filter the query on the total column
     *
     * Example usage:
     * <code>
     * $query->filterByTotal(1234); // WHERE total = 1234
     * $query->filterByTotal(array(12, 34)); // WHERE total IN (12, 34)
     * $query->filterByTotal(array('min' => 12)); // WHERE total > 12
     * </code>
     *
     * @param     mixed $total The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByTotal($total = null, $comparison = null)
    {
        if (is_array($total)) {
            $useMinMax = false;
            if (isset($total['min'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_TOTAL, $total['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($total['max'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_TOTAL, $total['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_TOTAL, $total, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(FacilityBookingTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query on the uniqid column
     *
     * Example usage:
     * <code>
     * $query->filterByUniqId('fooValue');   // WHERE uniqid = 'fooValue'
     * $query->filterByUniqId('%fooValue%'); // WHERE uniqid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uniqId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function filterByUniqId($uniqId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uniqId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $uniqId)) {
                $uniqId = str_replace('*', '%', $uniqId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityBookingTableMap::COL_UNIQID, $uniqId, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFacilityBooking $facilityBooking Object to remove from the list of results
     *
     * @return $this|ChildFacilityBookingQuery The current query, for fluid interface
     */
    public function prune($facilityBooking = null)
    {
        if ($facilityBooking) {
            $this->addUsingAlias(FacilityBookingTableMap::COL_ID, $facilityBooking->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the facility_booking table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityBookingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FacilityBookingTableMap::clearInstancePool();
            FacilityBookingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityBookingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FacilityBookingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FacilityBookingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FacilityBookingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FacilityBookingQuery
