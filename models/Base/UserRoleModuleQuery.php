<?php

namespace Base;

use \UserRoleModule as ChildUserRoleModule;
use \UserRoleModuleQuery as ChildUserRoleModuleQuery;
use \Exception;
use \PDO;
use Map\UserRoleModuleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'userRole_Module' table.
 *
 * 
 *
 * @method     ChildUserRoleModuleQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserRoleModuleQuery orderByUserRoleId($order = Criteria::ASC) Order by the userRoleId column
 * @method     ChildUserRoleModuleQuery orderByModuleId($order = Criteria::ASC) Order by the ModuleId column
 *
 * @method     ChildUserRoleModuleQuery groupById() Group by the id column
 * @method     ChildUserRoleModuleQuery groupByUserRoleId() Group by the userRoleId column
 * @method     ChildUserRoleModuleQuery groupByModuleId() Group by the ModuleId column
 *
 * @method     ChildUserRoleModuleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserRoleModuleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserRoleModuleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserRoleModuleQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserRoleModuleQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserRoleModuleQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserRoleModule findOne(ConnectionInterface $con = null) Return the first ChildUserRoleModule matching the query
 * @method     ChildUserRoleModule findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserRoleModule matching the query, or a new ChildUserRoleModule object populated from the query conditions when no match is found
 *
 * @method     ChildUserRoleModule findOneById(string $id) Return the first ChildUserRoleModule filtered by the id column
 * @method     ChildUserRoleModule findOneByUserRoleId(string $userRoleId) Return the first ChildUserRoleModule filtered by the userRoleId column
 * @method     ChildUserRoleModule findOneByModuleId(string $ModuleId) Return the first ChildUserRoleModule filtered by the ModuleId column *

 * @method     ChildUserRoleModule requirePk($key, ConnectionInterface $con = null) Return the ChildUserRoleModule by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserRoleModule requireOne(ConnectionInterface $con = null) Return the first ChildUserRoleModule matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserRoleModule requireOneById(string $id) Return the first ChildUserRoleModule filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserRoleModule requireOneByUserRoleId(string $userRoleId) Return the first ChildUserRoleModule filtered by the userRoleId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserRoleModule requireOneByModuleId(string $ModuleId) Return the first ChildUserRoleModule filtered by the ModuleId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserRoleModule[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserRoleModule objects based on current ModelCriteria
 * @method     ChildUserRoleModule[]|ObjectCollection findById(string $id) Return ChildUserRoleModule objects filtered by the id column
 * @method     ChildUserRoleModule[]|ObjectCollection findByUserRoleId(string $userRoleId) Return ChildUserRoleModule objects filtered by the userRoleId column
 * @method     ChildUserRoleModule[]|ObjectCollection findByModuleId(string $ModuleId) Return ChildUserRoleModule objects filtered by the ModuleId column
 * @method     ChildUserRoleModule[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserRoleModuleQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UserRoleModuleQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\UserRoleModule', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserRoleModuleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserRoleModuleQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserRoleModuleQuery) {
            return $criteria;
        }
        $query = new ChildUserRoleModuleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserRoleModule|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserRoleModuleTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserRoleModuleTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserRoleModule A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, userRoleId, ModuleId FROM userRole_Module WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);            
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserRoleModule $obj */
            $obj = new ChildUserRoleModule();
            $obj->hydrate($row);
            UserRoleModuleTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserRoleModule|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserRoleModuleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserRoleModuleTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserRoleModuleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserRoleModuleTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%', Criteria::LIKE); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserRoleModuleQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserRoleModuleTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the userRoleId column
     *
     * Example usage:
     * <code>
     * $query->filterByUserRoleId('fooValue');   // WHERE userRoleId = 'fooValue'
     * $query->filterByUserRoleId('%fooValue%', Criteria::LIKE); // WHERE userRoleId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userRoleId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserRoleModuleQuery The current query, for fluid interface
     */
    public function filterByUserRoleId($userRoleId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userRoleId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserRoleModuleTableMap::COL_USERROLEID, $userRoleId, $comparison);
    }

    /**
     * Filter the query on the ModuleId column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleId('fooValue');   // WHERE ModuleId = 'fooValue'
     * $query->filterByModuleId('%fooValue%', Criteria::LIKE); // WHERE ModuleId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $moduleId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserRoleModuleQuery The current query, for fluid interface
     */
    public function filterByModuleId($moduleId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($moduleId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserRoleModuleTableMap::COL_MODULEID, $moduleId, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserRoleModule $userRoleModule Object to remove from the list of results
     *
     * @return $this|ChildUserRoleModuleQuery The current query, for fluid interface
     */
    public function prune($userRoleModule = null)
    {
        if ($userRoleModule) {
            $this->addUsingAlias(UserRoleModuleTableMap::COL_ID, $userRoleModule->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the userRole_Module table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserRoleModuleTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserRoleModuleTableMap::clearInstancePool();
            UserRoleModuleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserRoleModuleTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserRoleModuleTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            
            UserRoleModuleTableMap::removeInstanceFromPool($criteria);
        
            $affectedRows += ModelCriteria::delete($con);
            UserRoleModuleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserRoleModuleQuery
