<?php

namespace Base;

use \RknAppID as ChildRknAppID;
use \RknAppIDQuery as ChildRknAppIDQuery;
use \Exception;
use \PDO;
use Map\RknAppIDTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'rkn_app_id' table.
 *
 *
 *
 * @method     ChildRknAppIDQuery orderByID($order = Criteria::ASC) Order by the id column
 * @method     ChildRknAppIDQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildRknAppIDQuery orderByPackageID($order = Criteria::ASC) Order by the packageId column
 * @method     ChildRknAppIDQuery orderByDescription($order = Criteria::ASC) Order by the description column
 *
 * @method     ChildRknAppIDQuery groupByID() Group by the id column
 * @method     ChildRknAppIDQuery groupByName() Group by the name column
 * @method     ChildRknAppIDQuery groupByPackageID() Group by the packageId column
 * @method     ChildRknAppIDQuery groupByDescription() Group by the description column
 *
 * @method     ChildRknAppIDQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRknAppIDQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRknAppIDQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRknAppIDQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRknAppIDQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRknAppIDQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRknAppID findOne(ConnectionInterface $con = null) Return the first ChildRknAppID matching the query
 * @method     ChildRknAppID findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRknAppID matching the query, or a new ChildRknAppID object populated from the query conditions when no match is found
 *
 * @method     ChildRknAppID findOneByID(int $id) Return the first ChildRknAppID filtered by the id column
 * @method     ChildRknAppID findOneByName(string $name) Return the first ChildRknAppID filtered by the name column
 * @method     ChildRknAppID findOneByPackageID(int $packageId) Return the first ChildRknAppID filtered by the packageId column
 * @method     ChildRknAppID findOneByDescription(string $description) Return the first ChildRknAppID filtered by the description column *

 * @method     ChildRknAppID requirePk($key, ConnectionInterface $con = null) Return the ChildRknAppID by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknAppID requireOne(ConnectionInterface $con = null) Return the first ChildRknAppID matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRknAppID requireOneByID(int $id) Return the first ChildRknAppID filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknAppID requireOneByName(string $name) Return the first ChildRknAppID filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknAppID requireOneByPackageID(int $packageId) Return the first ChildRknAppID filtered by the packageId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknAppID requireOneByDescription(string $description) Return the first ChildRknAppID filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRknAppID[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRknAppID objects based on current ModelCriteria
 * @method     ChildRknAppID[]|ObjectCollection findByID(int $id) Return ChildRknAppID objects filtered by the id column
 * @method     ChildRknAppID[]|ObjectCollection findByName(string $name) Return ChildRknAppID objects filtered by the name column
 * @method     ChildRknAppID[]|ObjectCollection findByPackageID(int $packageId) Return ChildRknAppID objects filtered by the packageId column
 * @method     ChildRknAppID[]|ObjectCollection findByDescription(string $description) Return ChildRknAppID objects filtered by the description column
 * @method     ChildRknAppID[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RknAppIDQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RknAppIDQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\RknAppID', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRknAppIDQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRknAppIDQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRknAppIDQuery) {
            return $criteria;
        }
        $query = new ChildRknAppIDQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRknAppID|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RknAppIDTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RknAppIDTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRknAppID A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, packageId, description FROM rkn_app_id WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRknAppID $obj */
            $obj = new ChildRknAppID();
            $obj->hydrate($row);
            RknAppIDTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRknAppID|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRknAppIDQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RknAppIDTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRknAppIDQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RknAppIDTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByID(1234); // WHERE id = 1234
     * $query->filterByID(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByID(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $iD The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknAppIDQuery The current query, for fluid interface
     */
    public function filterByID($iD = null, $comparison = null)
    {
        if (is_array($iD)) {
            $useMinMax = false;
            if (isset($iD['min'])) {
                $this->addUsingAlias(RknAppIDTableMap::COL_ID, $iD['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iD['max'])) {
                $this->addUsingAlias(RknAppIDTableMap::COL_ID, $iD['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RknAppIDTableMap::COL_ID, $iD, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknAppIDQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RknAppIDTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the packageId column
     *
     * Example usage:
     * <code>
     * $query->filterByPackageID(1234); // WHERE packageId = 1234
     * $query->filterByPackageID(array(12, 34)); // WHERE packageId IN (12, 34)
     * $query->filterByPackageID(array('min' => 12)); // WHERE packageId > 12
     * </code>
     *
     * @param     mixed $packageID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknAppIDQuery The current query, for fluid interface
     */
    public function filterByPackageID($packageID = null, $comparison = null)
    {
        if (is_array($packageID)) {
            $useMinMax = false;
            if (isset($packageID['min'])) {
                $this->addUsingAlias(RknAppIDTableMap::COL_PACKAGEID, $packageID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($packageID['max'])) {
                $this->addUsingAlias(RknAppIDTableMap::COL_PACKAGEID, $packageID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RknAppIDTableMap::COL_PACKAGEID, $packageID, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknAppIDQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RknAppIDTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRknAppID $rknAppID Object to remove from the list of results
     *
     * @return $this|ChildRknAppIDQuery The current query, for fluid interface
     */
    public function prune($rknAppID = null)
    {
        if ($rknAppID) {
            $this->addUsingAlias(RknAppIDTableMap::COL_ID, $rknAppID->getID(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the rkn_app_id table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RknAppIDTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RknAppIDTableMap::clearInstancePool();
            RknAppIDTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RknAppIDTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RknAppIDTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RknAppIDTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RknAppIDTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RknAppIDQuery
