<?php

namespace Base;

use \VideoCallSetting as ChildVideoCallSetting;
use \VideoCallSettingQuery as ChildVideoCallSettingQuery;
use \Exception;
use \PDO;
use Map\VideoCallSettingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'videoCallSetting' table.
 *
 *
 *
 * @method     ChildVideoCallSettingQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildVideoCallSettingQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildVideoCallSettingQuery orderByServer($order = Criteria::ASC) Order by the server column
 * @method     ChildVideoCallSettingQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildVideoCallSettingQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildVideoCallSettingQuery orderByCallDuration($order = Criteria::ASC) Order by the callDuration column
 * @method     ChildVideoCallSettingQuery orderByCallLimitEnable($order = Criteria::ASC) Order by the callLimitEnable column
 * @method     ChildVideoCallSettingQuery orderByTenantCallDuration($order = Criteria::ASC) Order by the tenantCallDuration column
 * @method     ChildVideoCallSettingQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildVideoCallSettingQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildVideoCallSettingQuery groupById() Group by the id column
 * @method     ChildVideoCallSettingQuery groupByProjectId() Group by the projectId column
 * @method     ChildVideoCallSettingQuery groupByServer() Group by the server column
 * @method     ChildVideoCallSettingQuery groupByUsername() Group by the username column
 * @method     ChildVideoCallSettingQuery groupByPassword() Group by the password column
 * @method     ChildVideoCallSettingQuery groupByCallDuration() Group by the callDuration column
 * @method     ChildVideoCallSettingQuery groupByCallLimitEnable() Group by the callLimitEnable column
 * @method     ChildVideoCallSettingQuery groupByTenantCallDuration() Group by the tenantCallDuration column
 * @method     ChildVideoCallSettingQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildVideoCallSettingQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildVideoCallSettingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVideoCallSettingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVideoCallSettingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVideoCallSettingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVideoCallSettingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVideoCallSettingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVideoCallSetting findOne(ConnectionInterface $con = null) Return the first ChildVideoCallSetting matching the query
 * @method     ChildVideoCallSetting findOneOrCreate(ConnectionInterface $con = null) Return the first ChildVideoCallSetting matching the query, or a new ChildVideoCallSetting object populated from the query conditions when no match is found
 *
 * @method     ChildVideoCallSetting findOneById(string $id) Return the first ChildVideoCallSetting filtered by the id column
 * @method     ChildVideoCallSetting findOneByProjectId(string $projectId) Return the first ChildVideoCallSetting filtered by the projectId column
 * @method     ChildVideoCallSetting findOneByServer(string $server) Return the first ChildVideoCallSetting filtered by the server column
 * @method     ChildVideoCallSetting findOneByUsername(string $username) Return the first ChildVideoCallSetting filtered by the username column
 * @method     ChildVideoCallSetting findOneByPassword(string $password) Return the first ChildVideoCallSetting filtered by the password column
 * @method     ChildVideoCallSetting findOneByCallDuration(int $callDuration) Return the first ChildVideoCallSetting filtered by the callDuration column
 * @method     ChildVideoCallSetting findOneByCallLimitEnable(int $callLimitEnable) Return the first ChildVideoCallSetting filtered by the callLimitEnable column
 * @method     ChildVideoCallSetting findOneByTenantCallDuration(int $tenantCallDuration) Return the first ChildVideoCallSetting filtered by the tenantCallDuration column
 * @method     ChildVideoCallSetting findOneByModifiedBy(string $modified_by) Return the first ChildVideoCallSetting filtered by the modified_by column
 * @method     ChildVideoCallSetting findOneByModifiedDate(string $modified_date) Return the first ChildVideoCallSetting filtered by the modified_date column *

 * @method     ChildVideoCallSetting requirePk($key, ConnectionInterface $con = null) Return the ChildVideoCallSetting by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOne(ConnectionInterface $con = null) Return the first ChildVideoCallSetting matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVideoCallSetting requireOneById(string $id) Return the first ChildVideoCallSetting filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOneByProjectId(string $projectId) Return the first ChildVideoCallSetting filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOneByServer(string $server) Return the first ChildVideoCallSetting filtered by the server column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOneByUsername(string $username) Return the first ChildVideoCallSetting filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOneByPassword(string $password) Return the first ChildVideoCallSetting filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOneByCallDuration(int $callDuration) Return the first ChildVideoCallSetting filtered by the callDuration column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOneByCallLimitEnable(int $callLimitEnable) Return the first ChildVideoCallSetting filtered by the callLimitEnable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOneByTenantCallDuration(int $tenantCallDuration) Return the first ChildVideoCallSetting filtered by the tenantCallDuration column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOneByModifiedBy(string $modified_by) Return the first ChildVideoCallSetting filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVideoCallSetting requireOneByModifiedDate(string $modified_date) Return the first ChildVideoCallSetting filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVideoCallSetting[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildVideoCallSetting objects based on current ModelCriteria
 * @method     ChildVideoCallSetting[]|ObjectCollection findById(string $id) Return ChildVideoCallSetting objects filtered by the id column
 * @method     ChildVideoCallSetting[]|ObjectCollection findByProjectId(string $projectId) Return ChildVideoCallSetting objects filtered by the projectId column
 * @method     ChildVideoCallSetting[]|ObjectCollection findByServer(string $server) Return ChildVideoCallSetting objects filtered by the server column
 * @method     ChildVideoCallSetting[]|ObjectCollection findByUsername(string $username) Return ChildVideoCallSetting objects filtered by the username column
 * @method     ChildVideoCallSetting[]|ObjectCollection findByPassword(string $password) Return ChildVideoCallSetting objects filtered by the password column
 * @method     ChildVideoCallSetting[]|ObjectCollection findByCallDuration(int $callDuration) Return ChildVideoCallSetting objects filtered by the callDuration column
 * @method     ChildVideoCallSetting[]|ObjectCollection findByCallLimitEnable(int $callLimitEnable) Return ChildVideoCallSetting objects filtered by the callLimitEnable column
 * @method     ChildVideoCallSetting[]|ObjectCollection findByTenantCallDuration(int $tenantCallDuration) Return ChildVideoCallSetting objects filtered by the tenantCallDuration column
 * @method     ChildVideoCallSetting[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildVideoCallSetting objects filtered by the modified_by column
 * @method     ChildVideoCallSetting[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildVideoCallSetting objects filtered by the modified_date column
 * @method     ChildVideoCallSetting[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VideoCallSettingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\VideoCallSettingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\VideoCallSetting', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVideoCallSettingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVideoCallSettingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildVideoCallSettingQuery) {
            return $criteria;
        }
        $query = new ChildVideoCallSettingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVideoCallSetting|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = VideoCallSettingTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VideoCallSettingTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVideoCallSetting A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, server, username, password, callDuration, callLimitEnable, tenantCallDuration, modified_by, modified_date FROM videoCallSetting WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVideoCallSetting $obj */
            $obj = new ChildVideoCallSetting();
            $obj->hydrate($row);
            VideoCallSettingTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildVideoCallSetting|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the server column
     *
     * Example usage:
     * <code>
     * $query->filterByServer('fooValue');   // WHERE server = 'fooValue'
     * $query->filterByServer('%fooValue%'); // WHERE server LIKE '%fooValue%'
     * </code>
     *
     * @param     string $server The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByServer($server = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($server)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $server)) {
                $server = str_replace('*', '%', $server);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_SERVER, $server, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%'); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $username)) {
                $username = str_replace('*', '%', $username);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the callDuration column
     *
     * Example usage:
     * <code>
     * $query->filterByCallDuration(1234); // WHERE callDuration = 1234
     * $query->filterByCallDuration(array(12, 34)); // WHERE callDuration IN (12, 34)
     * $query->filterByCallDuration(array('min' => 12)); // WHERE callDuration > 12
     * </code>
     *
     * @param     mixed $callDuration The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByCallDuration($callDuration = null, $comparison = null)
    {
        if (is_array($callDuration)) {
            $useMinMax = false;
            if (isset($callDuration['min'])) {
                $this->addUsingAlias(VideoCallSettingTableMap::COL_CALLDURATION, $callDuration['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($callDuration['max'])) {
                $this->addUsingAlias(VideoCallSettingTableMap::COL_CALLDURATION, $callDuration['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_CALLDURATION, $callDuration, $comparison);
    }

    /**
     * Filter the query on the callLimitEnable column
     *
     * Example usage:
     * <code>
     * $query->filterByCallLimitEnable(1234); // WHERE callLimitEnable = 1234
     * $query->filterByCallLimitEnable(array(12, 34)); // WHERE callLimitEnable IN (12, 34)
     * $query->filterByCallLimitEnable(array('min' => 12)); // WHERE callLimitEnable > 12
     * </code>
     *
     * @param     mixed $callLimitEnable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByCallLimitEnable($callLimitEnable = null, $comparison = null)
    {
        if (is_array($callLimitEnable)) {
            $useMinMax = false;
            if (isset($callLimitEnable['min'])) {
                $this->addUsingAlias(VideoCallSettingTableMap::COL_CALLLIMITENABLE, $callLimitEnable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($callLimitEnable['max'])) {
                $this->addUsingAlias(VideoCallSettingTableMap::COL_CALLLIMITENABLE, $callLimitEnable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_CALLLIMITENABLE, $callLimitEnable, $comparison);
    }

    /**
     * Filter the query on the tenantCallDuration column
     *
     * Example usage:
     * <code>
     * $query->filterByTenantCallDuration(1234); // WHERE tenantCallDuration = 1234
     * $query->filterByTenantCallDuration(array(12, 34)); // WHERE tenantCallDuration IN (12, 34)
     * $query->filterByTenantCallDuration(array('min' => 12)); // WHERE tenantCallDuration > 12
     * </code>
     *
     * @param     mixed $tenantCallDuration The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByTenantCallDuration($tenantCallDuration = null, $comparison = null)
    {
        if (is_array($tenantCallDuration)) {
            $useMinMax = false;
            if (isset($tenantCallDuration['min'])) {
                $this->addUsingAlias(VideoCallSettingTableMap::COL_TENANTCALLDURATION, $tenantCallDuration['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tenantCallDuration['max'])) {
                $this->addUsingAlias(VideoCallSettingTableMap::COL_TENANTCALLDURATION, $tenantCallDuration['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_TENANTCALLDURATION, $tenantCallDuration, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(VideoCallSettingTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(VideoCallSettingTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VideoCallSettingTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildVideoCallSetting $videoCallSetting Object to remove from the list of results
     *
     * @return $this|ChildVideoCallSettingQuery The current query, for fluid interface
     */
    public function prune($videoCallSetting = null)
    {
        if ($videoCallSetting) {
            $this->addUsingAlias(VideoCallSettingTableMap::COL_ID, $videoCallSetting->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the videoCallSetting table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VideoCallSettingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VideoCallSettingTableMap::clearInstancePool();
            VideoCallSettingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VideoCallSettingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VideoCallSettingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VideoCallSettingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VideoCallSettingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // VideoCallSettingQuery
