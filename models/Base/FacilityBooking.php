<?php

namespace Base;

use \FacilityBookingQuery as ChildFacilityBookingQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\FacilityBookingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'facility_booking' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class FacilityBooking implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\FacilityBookingTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the booking_number field.
     *
     * @var        string
     */
    protected $booking_number;

    /**
     * The value for the facility_id field.
     *
     * @var        string
     */
    protected $facility_id;

    /**
     * The value for the facility_name field.
     *
     * @var        string
     */
    protected $facility_name;

    /**
     * The value for the booking_date field.
     *
     * @var        \DateTime
     */
    protected $booking_date;

    /**
     * The value for the booking_time field.
     *
     * @var        string
     */
    protected $booking_time;

    /**
     * The value for the unit_id field.
     *
     * @var        string
     */
    protected $unit_id;

    /**
     * The value for the booking_timeslot field.
     *
     * @var        string
     */
    protected $booking_timeslot;

    /**
     * The value for the booking_owner field.
     *
     * @var        string
     */
    protected $booking_owner;

    /**
     * The value for the project_id field.
     *
     * @var        string
     */
    protected $project_id;

    /**
     * The value for the tenant_id field.
     *
     * @var        string
     */
    protected $tenant_id;

    /**
     * The value for the booking_status field.
     * AUTO CANCELLED, USER CANCELLED, PRE-BOOK, CONFIRMED
     * @var        string
     */
    protected $booking_status;

    /**
     * The value for the payment_status field.
     * NOT YET, AUTO CANCELLED, USER CANCELLED, PAID
     * @var        string
     */
    protected $payment_status;

    /**
     * The value for the payment_date field.
     *
     * @var        \DateTime
     */
    protected $payment_date;

    /**
     * The value for the cancel_date field.
     *
     * @var        \DateTime
     */
    protected $cancel_date;

    /**
     * The value for the cancel_reason field.
     *
     * @var        string
     */
    protected $cancel_reason;

    /**
     * The value for the cancel_by field.
     *
     * @var        string
     */
    protected $cancel_by;

    /**
     * The value for the deposit field.
     *
     * @var        double
     */
    protected $deposit;

    /**
     * The value for the charge field.
     *
     * @var        double
     */
    protected $charge;

    /**
     * The value for the total field.
     *
     * @var        double
     */
    protected $total;

    /**
     * The value for the created_by field.
     *
     * @var        string
     */
    protected $created_by;

    /**
     * The value for the created_date field.
     *
     * @var        \DateTime
     */
    protected $created_date;

    /**
     * The value for the modified_by field.
     *
     * @var        string
     */
    protected $modified_by;

    /**
     * The value for the modified_date field.
     *
     * @var        \DateTime
     */
    protected $modified_date;

    /**
     * The value for the uniqid field.
     *
     * @var        string
     */
    protected $uniqid;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\FacilityBooking object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>FacilityBooking</code> instance.  If
     * <code>obj</code> is an instance of <code>FacilityBooking</code>, delegates to
     * <code>equals(FacilityBooking)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|FacilityBooking The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [booking_number] column value.
     *
     * @return string
     */
    public function getBookingNumber()
    {
        return $this->booking_number;
    }

    /**
     * Get the [facility_id] column value.
     *
     * @return string
     */
    public function getFacilityId()
    {
        return $this->facility_id;
    }

    /**
     * Get the [facility_name] column value.
     *
     * @return string
     */
    public function getFacilityName()
    {
        return $this->facility_name;
    }

    /**
     * Get the [optionally formatted] temporal [booking_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBookingDate($format = NULL)
    {
        if ($format === null) {
            return $this->booking_date;
        } else {
            return $this->booking_date instanceof \DateTime ? $this->booking_date->format($format) : null;
        }
    }

    /**
     * Get the [booking_time] column value.
     *
     * @return string
     */
    public function getBookingTime()
    {
        return $this->booking_time;
    }

    /**
     * Get the [unit_id] column value.
     *
     * @return string
     */
    public function getUnitId()
    {
        return $this->unit_id;
    }

    /**
     * Get the [booking_timeslot] column value.
     *
     * @return string
     */
    public function getBookingTimeslot()
    {
        return $this->booking_timeslot;
    }

    /**
     * Get the [booking_owner] column value.
     *
     * @return string
     */
    public function getBookingOwner()
    {
        return $this->booking_owner;
    }

    /**
     * Get the [project_id] column value.
     *
     * @return string
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * Get the [tenant_id] column value.
     *
     * @return string
     */
    public function getTenantId()
    {
        return $this->tenant_id;
    }

    /**
     * Get the [booking_status] column value.
     * AUTO CANCELLED, USER CANCELLED, PRE-BOOK, CONFIRMED
     * @return string
     */
    public function getBookingStatus()
    {
        return $this->booking_status;
    }

    /**
     * Get the [payment_status] column value.
     * NOT YET, AUTO CANCELLED, USER CANCELLED, PAID
     * @return string
     */
    public function getPaymentStatus()
    {
        return $this->payment_status;
    }

    /**
     * Get the [optionally formatted] temporal [payment_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPaymentDate($format = NULL)
    {
        if ($format === null) {
            return $this->payment_date;
        } else {
            return $this->payment_date instanceof \DateTime ? $this->payment_date->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [cancel_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCancelDate($format = NULL)
    {
        if ($format === null) {
            return $this->cancel_date;
        } else {
            return $this->cancel_date instanceof \DateTime ? $this->cancel_date->format($format) : null;
        }
    }

    /**
     * Get the [cancel_reason] column value.
     *
     * @return string
     */
    public function getCancelReason()
    {
        return $this->cancel_reason;
    }

    /**
     * Get the [cancel_by] column value.
     *
     * @return string
     */
    public function getCancelBy()
    {
        return $this->cancel_by;
    }

    /**
     * Get the [deposit] column value.
     *
     * @return double
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Get the [charge] column value.
     *
     * @return double
     */
    public function getCharge()
    {
        return $this->charge;
    }

    /**
     * Get the [total] column value.
     *
     * @return double
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Get the [created_by] column value.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Get the [optionally formatted] temporal [created_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedDate($format = NULL)
    {
        if ($format === null) {
            return $this->created_date;
        } else {
            return $this->created_date instanceof \DateTime ? $this->created_date->format($format) : null;
        }
    }

    /**
     * Get the [modified_by] column value.
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Get the [optionally formatted] temporal [modified_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModifiedDate($format = NULL)
    {
        if ($format === null) {
            return $this->modified_date;
        } else {
            return $this->modified_date instanceof \DateTime ? $this->modified_date->format($format) : null;
        }
    }

    /**
     * Get the [uniqid] column value.
     *
     * @return string
     */
    public function getUniqId()
    {
        return $this->uniqid;
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [booking_number] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setBookingNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->booking_number !== $v) {
            $this->booking_number = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_BOOKING_NUMBER] = true;
        }

        return $this;
    } // setBookingNumber()

    /**
     * Set the value of [facility_id] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setFacilityId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->facility_id !== $v) {
            $this->facility_id = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_FACILITY_ID] = true;
        }

        return $this;
    } // setFacilityId()

    /**
     * Set the value of [facility_name] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setFacilityName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->facility_name !== $v) {
            $this->facility_name = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_FACILITY_NAME] = true;
        }

        return $this;
    } // setFacilityName()

    /**
     * Sets the value of [booking_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setBookingDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->booking_date !== null || $dt !== null) {
            if ($this->booking_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->booking_date->format("Y-m-d H:i:s")) {
                $this->booking_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FacilityBookingTableMap::COL_BOOKING_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setBookingDate()

    /**
     * Set the value of [booking_time] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setBookingTime($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->booking_time !== $v) {
            $this->booking_time = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_BOOKING_TIME] = true;
        }

        return $this;
    } // setBookingTime()

    /**
     * Set the value of [unit_id] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setUnitId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->unit_id !== $v) {
            $this->unit_id = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_UNIT_ID] = true;
        }

        return $this;
    } // setUnitId()

    /**
     * Set the value of [booking_timeslot] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setBookingTimeslot($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->booking_timeslot !== $v) {
            $this->booking_timeslot = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_BOOKING_TIMESLOT] = true;
        }

        return $this;
    } // setBookingTimeslot()

    /**
     * Set the value of [booking_owner] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setBookingOwner($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->booking_owner !== $v) {
            $this->booking_owner = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_BOOKING_OWNER] = true;
        }

        return $this;
    } // setBookingOwner()

    /**
     * Set the value of [project_id] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setProjectId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->project_id !== $v) {
            $this->project_id = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_PROJECT_ID] = true;
        }

        return $this;
    } // setProjectId()

    /**
     * Set the value of [tenant_id] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setTenantId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tenant_id !== $v) {
            $this->tenant_id = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_TENANT_ID] = true;
        }

        return $this;
    } // setTenantId()

    /**
     * Set the value of [booking_status] column.
     * AUTO CANCELLED, USER CANCELLED, PRE-BOOK, CONFIRMED
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setBookingStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->booking_status !== $v) {
            $this->booking_status = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_BOOKING_STATUS] = true;
        }

        return $this;
    } // setBookingStatus()

    /**
     * Set the value of [payment_status] column.
     * NOT YET, AUTO CANCELLED, USER CANCELLED, PAID
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setPaymentStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->payment_status !== $v) {
            $this->payment_status = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_PAYMENT_STATUS] = true;
        }

        return $this;
    } // setPaymentStatus()

    /**
     * Sets the value of [payment_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setPaymentDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->payment_date !== null || $dt !== null) {
            if ($this->payment_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->payment_date->format("Y-m-d H:i:s")) {
                $this->payment_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FacilityBookingTableMap::COL_PAYMENT_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setPaymentDate()

    /**
     * Sets the value of [cancel_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setCancelDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->cancel_date !== null || $dt !== null) {
            if ($this->cancel_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->cancel_date->format("Y-m-d H:i:s")) {
                $this->cancel_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FacilityBookingTableMap::COL_CANCEL_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setCancelDate()

    /**
     * Set the value of [cancel_reason] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setCancelReason($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cancel_reason !== $v) {
            $this->cancel_reason = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_CANCEL_REASON] = true;
        }

        return $this;
    } // setCancelReason()

    /**
     * Set the value of [cancel_by] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setCancelBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cancel_by !== $v) {
            $this->cancel_by = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_CANCEL_BY] = true;
        }

        return $this;
    } // setCancelBy()

    /**
     * Set the value of [deposit] column.
     *
     * @param double $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setDeposit($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->deposit !== $v) {
            $this->deposit = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_DEPOSIT] = true;
        }

        return $this;
    } // setDeposit()

    /**
     * Set the value of [charge] column.
     *
     * @param double $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setCharge($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->charge !== $v) {
            $this->charge = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_CHARGE] = true;
        }

        return $this;
    } // setCharge()

    /**
     * Set the value of [total] column.
     *
     * @param double $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setTotal($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->total !== $v) {
            $this->total = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_TOTAL] = true;
        }

        return $this;
    } // setTotal()

    /**
     * Set the value of [created_by] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setCreatedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->created_by !== $v) {
            $this->created_by = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_CREATED_BY] = true;
        }

        return $this;
    } // setCreatedBy()

    /**
     * Sets the value of [created_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setCreatedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_date !== null || $dt !== null) {
            if ($this->created_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->created_date->format("Y-m-d H:i:s")) {
                $this->created_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FacilityBookingTableMap::COL_CREATED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedDate()

    /**
     * Set the value of [modified_by] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setModifiedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modified_by !== $v) {
            $this->modified_by = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_MODIFIED_BY] = true;
        }

        return $this;
    } // setModifiedBy()

    /**
     * Sets the value of [modified_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setModifiedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified_date !== null || $dt !== null) {
            if ($this->modified_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->modified_date->format("Y-m-d H:i:s")) {
                $this->modified_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FacilityBookingTableMap::COL_MODIFIED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setModifiedDate()

    /**
     * Set the value of [uniqid] column.
     *
     * @param string $v new value
     * @return $this|\FacilityBooking The current object (for fluent API support)
     */
    public function setUniqId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uniqid !== $v) {
            $this->uniqid = $v;
            $this->modifiedColumns[FacilityBookingTableMap::COL_UNIQID] = true;
        }

        return $this;
    } // setUniqId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : FacilityBookingTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : FacilityBookingTableMap::translateFieldName('BookingNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->booking_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : FacilityBookingTableMap::translateFieldName('FacilityId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->facility_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : FacilityBookingTableMap::translateFieldName('FacilityName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->facility_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : FacilityBookingTableMap::translateFieldName('BookingDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->booking_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : FacilityBookingTableMap::translateFieldName('BookingTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->booking_time = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : FacilityBookingTableMap::translateFieldName('UnitId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unit_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : FacilityBookingTableMap::translateFieldName('BookingTimeslot', TableMap::TYPE_PHPNAME, $indexType)];
            $this->booking_timeslot = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : FacilityBookingTableMap::translateFieldName('BookingOwner', TableMap::TYPE_PHPNAME, $indexType)];
            $this->booking_owner = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : FacilityBookingTableMap::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->project_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : FacilityBookingTableMap::translateFieldName('TenantId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tenant_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : FacilityBookingTableMap::translateFieldName('BookingStatus', TableMap::TYPE_PHPNAME, $indexType)];
            $this->booking_status = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : FacilityBookingTableMap::translateFieldName('PaymentStatus', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payment_status = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : FacilityBookingTableMap::translateFieldName('PaymentDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->payment_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : FacilityBookingTableMap::translateFieldName('CancelDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->cancel_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : FacilityBookingTableMap::translateFieldName('CancelReason', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cancel_reason = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : FacilityBookingTableMap::translateFieldName('CancelBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cancel_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : FacilityBookingTableMap::translateFieldName('Deposit', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deposit = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : FacilityBookingTableMap::translateFieldName('Charge', TableMap::TYPE_PHPNAME, $indexType)];
            $this->charge = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : FacilityBookingTableMap::translateFieldName('Total', TableMap::TYPE_PHPNAME, $indexType)];
            $this->total = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : FacilityBookingTableMap::translateFieldName('CreatedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : FacilityBookingTableMap::translateFieldName('CreatedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : FacilityBookingTableMap::translateFieldName('ModifiedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modified_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : FacilityBookingTableMap::translateFieldName('ModifiedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->modified_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : FacilityBookingTableMap::translateFieldName('UniqId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uniqid = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 25; // 25 = FacilityBookingTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\FacilityBooking'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FacilityBookingTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildFacilityBookingQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see FacilityBooking::setDeleted()
     * @see FacilityBooking::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityBookingTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildFacilityBookingQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityBookingTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FacilityBookingTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FacilityBookingTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'booking_number';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_FACILITY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'facility_id';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_FACILITY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'facility_name';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'booking_date';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_TIME)) {
            $modifiedColumns[':p' . $index++]  = 'booking_time';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_UNIT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'unit_id';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_TIMESLOT)) {
            $modifiedColumns[':p' . $index++]  = 'booking_timeslot';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_OWNER)) {
            $modifiedColumns[':p' . $index++]  = 'booking_owner';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_PROJECT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'project_id';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_TENANT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'tenant_id';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'booking_status';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_PAYMENT_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'payment_status';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_PAYMENT_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'payment_date';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CANCEL_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'cancel_date';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CANCEL_REASON)) {
            $modifiedColumns[':p' . $index++]  = 'cancel_reason';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CANCEL_BY)) {
            $modifiedColumns[':p' . $index++]  = 'cancel_by';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_DEPOSIT)) {
            $modifiedColumns[':p' . $index++]  = 'deposit';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CHARGE)) {
            $modifiedColumns[':p' . $index++]  = 'charge';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_TOTAL)) {
            $modifiedColumns[':p' . $index++]  = 'total';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CREATED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'created_by';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CREATED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'created_date';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_MODIFIED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'modified_by';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_MODIFIED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'modified_date';
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_UNIQID)) {
            $modifiedColumns[':p' . $index++]  = 'uniqid';
        }

        $sql = sprintf(
            'INSERT INTO facility_booking (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                    case 'booking_number':
                        $stmt->bindValue($identifier, $this->booking_number, PDO::PARAM_STR);
                        break;
                    case 'facility_id':
                        $stmt->bindValue($identifier, $this->facility_id, PDO::PARAM_STR);
                        break;
                    case 'facility_name':
                        $stmt->bindValue($identifier, $this->facility_name, PDO::PARAM_STR);
                        break;
                    case 'booking_date':
                        $stmt->bindValue($identifier, $this->booking_date ? $this->booking_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'booking_time':
                        $stmt->bindValue($identifier, $this->booking_time, PDO::PARAM_STR);
                        break;
                    case 'unit_id':
                        $stmt->bindValue($identifier, $this->unit_id, PDO::PARAM_STR);
                        break;
                    case 'booking_timeslot':
                        $stmt->bindValue($identifier, $this->booking_timeslot, PDO::PARAM_STR);
                        break;
                    case 'booking_owner':
                        $stmt->bindValue($identifier, $this->booking_owner, PDO::PARAM_STR);
                        break;
                    case 'project_id':
                        $stmt->bindValue($identifier, $this->project_id, PDO::PARAM_STR);
                        break;
                    case 'tenant_id':
                        $stmt->bindValue($identifier, $this->tenant_id, PDO::PARAM_STR);
                        break;
                    case 'booking_status':
                        $stmt->bindValue($identifier, $this->booking_status, PDO::PARAM_STR);
                        break;
                    case 'payment_status':
                        $stmt->bindValue($identifier, $this->payment_status, PDO::PARAM_STR);
                        break;
                    case 'payment_date':
                        $stmt->bindValue($identifier, $this->payment_date ? $this->payment_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'cancel_date':
                        $stmt->bindValue($identifier, $this->cancel_date ? $this->cancel_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'cancel_reason':
                        $stmt->bindValue($identifier, $this->cancel_reason, PDO::PARAM_STR);
                        break;
                    case 'cancel_by':
                        $stmt->bindValue($identifier, $this->cancel_by, PDO::PARAM_STR);
                        break;
                    case 'deposit':
                        $stmt->bindValue($identifier, $this->deposit, PDO::PARAM_STR);
                        break;
                    case 'charge':
                        $stmt->bindValue($identifier, $this->charge, PDO::PARAM_STR);
                        break;
                    case 'total':
                        $stmt->bindValue($identifier, $this->total, PDO::PARAM_STR);
                        break;
                    case 'created_by':
                        $stmt->bindValue($identifier, $this->created_by, PDO::PARAM_STR);
                        break;
                    case 'created_date':
                        $stmt->bindValue($identifier, $this->created_date ? $this->created_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'modified_by':
                        $stmt->bindValue($identifier, $this->modified_by, PDO::PARAM_STR);
                        break;
                    case 'modified_date':
                        $stmt->bindValue($identifier, $this->modified_date ? $this->modified_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'uniqid':
                        $stmt->bindValue($identifier, $this->uniqid, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FacilityBookingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getBookingNumber();
                break;
            case 2:
                return $this->getFacilityId();
                break;
            case 3:
                return $this->getFacilityName();
                break;
            case 4:
                return $this->getBookingDate();
                break;
            case 5:
                return $this->getBookingTime();
                break;
            case 6:
                return $this->getUnitId();
                break;
            case 7:
                return $this->getBookingTimeslot();
                break;
            case 8:
                return $this->getBookingOwner();
                break;
            case 9:
                return $this->getProjectId();
                break;
            case 10:
                return $this->getTenantId();
                break;
            case 11:
                return $this->getBookingStatus();
                break;
            case 12:
                return $this->getPaymentStatus();
                break;
            case 13:
                return $this->getPaymentDate();
                break;
            case 14:
                return $this->getCancelDate();
                break;
            case 15:
                return $this->getCancelReason();
                break;
            case 16:
                return $this->getCancelBy();
                break;
            case 17:
                return $this->getDeposit();
                break;
            case 18:
                return $this->getCharge();
                break;
            case 19:
                return $this->getTotal();
                break;
            case 20:
                return $this->getCreatedBy();
                break;
            case 21:
                return $this->getCreatedDate();
                break;
            case 22:
                return $this->getModifiedBy();
                break;
            case 23:
                return $this->getModifiedDate();
                break;
            case 24:
                return $this->getUniqId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['FacilityBooking'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['FacilityBooking'][$this->hashCode()] = true;
        $keys = FacilityBookingTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getBookingNumber(),
            $keys[2] => $this->getFacilityId(),
            $keys[3] => $this->getFacilityName(),
            $keys[4] => $this->getBookingDate(),
            $keys[5] => $this->getBookingTime(),
            $keys[6] => $this->getUnitId(),
            $keys[7] => $this->getBookingTimeslot(),
            $keys[8] => $this->getBookingOwner(),
            $keys[9] => $this->getProjectId(),
            $keys[10] => $this->getTenantId(),
            $keys[11] => $this->getBookingStatus(),
            $keys[12] => $this->getPaymentStatus(),
            $keys[13] => $this->getPaymentDate(),
            $keys[14] => $this->getCancelDate(),
            $keys[15] => $this->getCancelReason(),
            $keys[16] => $this->getCancelBy(),
            $keys[17] => $this->getDeposit(),
            $keys[18] => $this->getCharge(),
            $keys[19] => $this->getTotal(),
            $keys[20] => $this->getCreatedBy(),
            $keys[21] => $this->getCreatedDate(),
            $keys[22] => $this->getModifiedBy(),
            $keys[23] => $this->getModifiedDate(),
            $keys[24] => $this->getUniqId(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[21]] instanceof \DateTime) {
            $result[$keys[21]] = $result[$keys[21]]->format('c');
        }

        if ($result[$keys[23]] instanceof \DateTime) {
            $result[$keys[23]] = $result[$keys[23]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\FacilityBooking
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FacilityBookingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\FacilityBooking
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setBookingNumber($value);
                break;
            case 2:
                $this->setFacilityId($value);
                break;
            case 3:
                $this->setFacilityName($value);
                break;
            case 4:
                $this->setBookingDate($value);
                break;
            case 5:
                $this->setBookingTime($value);
                break;
            case 6:
                $this->setUnitId($value);
                break;
            case 7:
                $this->setBookingTimeslot($value);
                break;
            case 8:
                $this->setBookingOwner($value);
                break;
            case 9:
                $this->setProjectId($value);
                break;
            case 10:
                $this->setTenantId($value);
                break;
            case 11:
                $this->setBookingStatus($value);
                break;
            case 12:
                $this->setPaymentStatus($value);
                break;
            case 13:
                $this->setPaymentDate($value);
                break;
            case 14:
                $this->setCancelDate($value);
                break;
            case 15:
                $this->setCancelReason($value);
                break;
            case 16:
                $this->setCancelBy($value);
                break;
            case 17:
                $this->setDeposit($value);
                break;
            case 18:
                $this->setCharge($value);
                break;
            case 19:
                $this->setTotal($value);
                break;
            case 20:
                $this->setCreatedBy($value);
                break;
            case 21:
                $this->setCreatedDate($value);
                break;
            case 22:
                $this->setModifiedBy($value);
                break;
            case 23:
                $this->setModifiedDate($value);
                break;
            case 24:
                $this->setUniqId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = FacilityBookingTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBookingNumber($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setFacilityId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setFacilityName($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setBookingDate($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setBookingTime($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUnitId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setBookingTimeslot($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setBookingOwner($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setProjectId($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setTenantId($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setBookingStatus($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setPaymentStatus($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setPaymentDate($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setCancelDate($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setCancelReason($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCancelBy($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setDeposit($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setCharge($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setTotal($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setCreatedBy($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setCreatedDate($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setModifiedBy($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setModifiedDate($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setUniqId($arr[$keys[24]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\FacilityBooking The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(FacilityBookingTableMap::DATABASE_NAME);

        if ($this->isColumnModified(FacilityBookingTableMap::COL_ID)) {
            $criteria->add(FacilityBookingTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_NUMBER)) {
            $criteria->add(FacilityBookingTableMap::COL_BOOKING_NUMBER, $this->booking_number);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_FACILITY_ID)) {
            $criteria->add(FacilityBookingTableMap::COL_FACILITY_ID, $this->facility_id);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_FACILITY_NAME)) {
            $criteria->add(FacilityBookingTableMap::COL_FACILITY_NAME, $this->facility_name);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_DATE)) {
            $criteria->add(FacilityBookingTableMap::COL_BOOKING_DATE, $this->booking_date);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_TIME)) {
            $criteria->add(FacilityBookingTableMap::COL_BOOKING_TIME, $this->booking_time);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_UNIT_ID)) {
            $criteria->add(FacilityBookingTableMap::COL_UNIT_ID, $this->unit_id);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_TIMESLOT)) {
            $criteria->add(FacilityBookingTableMap::COL_BOOKING_TIMESLOT, $this->booking_timeslot);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_OWNER)) {
            $criteria->add(FacilityBookingTableMap::COL_BOOKING_OWNER, $this->booking_owner);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_PROJECT_ID)) {
            $criteria->add(FacilityBookingTableMap::COL_PROJECT_ID, $this->project_id);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_TENANT_ID)) {
            $criteria->add(FacilityBookingTableMap::COL_TENANT_ID, $this->tenant_id);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_BOOKING_STATUS)) {
            $criteria->add(FacilityBookingTableMap::COL_BOOKING_STATUS, $this->booking_status);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_PAYMENT_STATUS)) {
            $criteria->add(FacilityBookingTableMap::COL_PAYMENT_STATUS, $this->payment_status);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_PAYMENT_DATE)) {
            $criteria->add(FacilityBookingTableMap::COL_PAYMENT_DATE, $this->payment_date);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CANCEL_DATE)) {
            $criteria->add(FacilityBookingTableMap::COL_CANCEL_DATE, $this->cancel_date);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CANCEL_REASON)) {
            $criteria->add(FacilityBookingTableMap::COL_CANCEL_REASON, $this->cancel_reason);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CANCEL_BY)) {
            $criteria->add(FacilityBookingTableMap::COL_CANCEL_BY, $this->cancel_by);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_DEPOSIT)) {
            $criteria->add(FacilityBookingTableMap::COL_DEPOSIT, $this->deposit);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CHARGE)) {
            $criteria->add(FacilityBookingTableMap::COL_CHARGE, $this->charge);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_TOTAL)) {
            $criteria->add(FacilityBookingTableMap::COL_TOTAL, $this->total);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CREATED_BY)) {
            $criteria->add(FacilityBookingTableMap::COL_CREATED_BY, $this->created_by);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_CREATED_DATE)) {
            $criteria->add(FacilityBookingTableMap::COL_CREATED_DATE, $this->created_date);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_MODIFIED_BY)) {
            $criteria->add(FacilityBookingTableMap::COL_MODIFIED_BY, $this->modified_by);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_MODIFIED_DATE)) {
            $criteria->add(FacilityBookingTableMap::COL_MODIFIED_DATE, $this->modified_date);
        }
        if ($this->isColumnModified(FacilityBookingTableMap::COL_UNIQID)) {
            $criteria->add(FacilityBookingTableMap::COL_UNIQID, $this->uniqid);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildFacilityBookingQuery::create();
        $criteria->add(FacilityBookingTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \FacilityBooking (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setBookingNumber($this->getBookingNumber());
        $copyObj->setFacilityId($this->getFacilityId());
        $copyObj->setFacilityName($this->getFacilityName());
        $copyObj->setBookingDate($this->getBookingDate());
        $copyObj->setBookingTime($this->getBookingTime());
        $copyObj->setUnitId($this->getUnitId());
        $copyObj->setBookingTimeslot($this->getBookingTimeslot());
        $copyObj->setBookingOwner($this->getBookingOwner());
        $copyObj->setProjectId($this->getProjectId());
        $copyObj->setTenantId($this->getTenantId());
        $copyObj->setBookingStatus($this->getBookingStatus());
        $copyObj->setPaymentStatus($this->getPaymentStatus());
        $copyObj->setPaymentDate($this->getPaymentDate());
        $copyObj->setCancelDate($this->getCancelDate());
        $copyObj->setCancelReason($this->getCancelReason());
        $copyObj->setCancelBy($this->getCancelBy());
        $copyObj->setDeposit($this->getDeposit());
        $copyObj->setCharge($this->getCharge());
        $copyObj->setTotal($this->getTotal());
        $copyObj->setCreatedBy($this->getCreatedBy());
        $copyObj->setCreatedDate($this->getCreatedDate());
        $copyObj->setModifiedBy($this->getModifiedBy());
        $copyObj->setModifiedDate($this->getModifiedDate());
        $copyObj->setUniqId($this->getUniqId());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \FacilityBooking Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->booking_number = null;
        $this->facility_id = null;
        $this->facility_name = null;
        $this->booking_date = null;
        $this->booking_time = null;
        $this->unit_id = null;
        $this->booking_timeslot = null;
        $this->booking_owner = null;
        $this->project_id = null;
        $this->tenant_id = null;
        $this->booking_status = null;
        $this->payment_status = null;
        $this->payment_date = null;
        $this->cancel_date = null;
        $this->cancel_reason = null;
        $this->cancel_by = null;
        $this->deposit = null;
        $this->charge = null;
        $this->total = null;
        $this->created_by = null;
        $this->created_date = null;
        $this->modified_by = null;
        $this->modified_date = null;
        $this->uniqid = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(FacilityBookingTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
