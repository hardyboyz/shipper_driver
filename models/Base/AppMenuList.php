<?php

namespace Base;

use \AppMenuListQuery as ChildAppMenuListQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\AppMenuListTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'appMenuList' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class AppMenuList implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\AppMenuListTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the projectid field.
     *
     * @var        string
     */
    protected $projectid;

    /**
     * The value for the name field.
     *
     * @var        string
     */
    protected $name;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the image field.
     *
     * @var        string
     */
    protected $image;

    /**
     * The value for the sidemenuorder field.
     *
     * @var        int
     */
    protected $sidemenuorder;

    /**
     * The value for the sidemenuenable field.
     * 1=Active, 0=Inactive
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $sidemenuenable;

    /**
     * The value for the homemenuorder field.
     *
     * @var        int
     */
    protected $homemenuorder;

    /**
     * The value for the homemenuenable field.
     * 1=Active, 0=Inactive
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $homemenuenable;

    /**
     * The value for the toprightmenuorder field.
     *
     * @var        int
     */
    protected $toprightmenuorder;

    /**
     * The value for the toprightmenuenable field.
     * 1=Active, 0=Inactive
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $toprightmenuenable;

    /**
     * The value for the tutorialimage field.
     *
     * @var        string
     */
    protected $tutorialimage;

    /**
     * The value for the tutorialenable field.
     * 1=Active, 0=Inactive
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $tutorialenable;

    /**
     * The value for the phonetutorialimage field.
     *
     * @var        string
     */
    protected $phonetutorialimage;

    /**
     * The value for the phonetutorialenable field.
     * 1=Active, 0=Inactive
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $phonetutorialenable;

    /**
     * The value for the phoneicon field.
     *
     * @var        string
     */
    protected $phoneicon;

    /**
     * The value for the module_type field.
     *
     * @var        int
     */
    protected $module_type;

    /**
     * The value for the created_by field.
     *
     * @var        string
     */
    protected $created_by;

    /**
     * The value for the created_date field.
     *
     * @var        \DateTime
     */
    protected $created_date;

    /**
     * The value for the modified_by field.
     *
     * @var        string
     */
    protected $modified_by;

    /**
     * The value for the modified_date field.
     *
     * @var        \DateTime
     */
    protected $modified_date;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->sidemenuenable = 0;
        $this->homemenuenable = 0;
        $this->toprightmenuenable = 0;
        $this->tutorialenable = 0;
        $this->phonetutorialenable = 0;
    }

    /**
     * Initializes internal state of Base\AppMenuList object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>AppMenuList</code> instance.  If
     * <code>obj</code> is an instance of <code>AppMenuList</code>, delegates to
     * <code>equals(AppMenuList)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|AppMenuList The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [projectid] column value.
     *
     * @return string
     */
    public function getProjectId()
    {
        return $this->projectid;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [image] column value.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Get the [sidemenuorder] column value.
     *
     * @return int
     */
    public function getSideMenuOrder()
    {
        return $this->sidemenuorder;
    }

    /**
     * Get the [sidemenuenable] column value.
     * 1=Active, 0=Inactive
     * @return int
     */
    public function getSideMenuEnable()
    {
        return $this->sidemenuenable;
    }

    /**
     * Get the [homemenuorder] column value.
     *
     * @return int
     */
    public function getHomeMenuOrder()
    {
        return $this->homemenuorder;
    }

    /**
     * Get the [homemenuenable] column value.
     * 1=Active, 0=Inactive
     * @return int
     */
    public function getHomeMenuEnable()
    {
        return $this->homemenuenable;
    }

    /**
     * Get the [toprightmenuorder] column value.
     *
     * @return int
     */
    public function getTopRightMenuOrder()
    {
        return $this->toprightmenuorder;
    }

    /**
     * Get the [toprightmenuenable] column value.
     * 1=Active, 0=Inactive
     * @return int
     */
    public function getTopRightMenuEnable()
    {
        return $this->toprightmenuenable;
    }

    /**
     * Get the [tutorialimage] column value.
     *
     * @return string
     */
    public function getTutorialImage()
    {
        return $this->tutorialimage;
    }

    /**
     * Get the [tutorialenable] column value.
     * 1=Active, 0=Inactive
     * @return int
     */
    public function getTutorialEnable()
    {
        return $this->tutorialenable;
    }

    /**
     * Get the [phonetutorialimage] column value.
     *
     * @return string
     */
    public function getPhoneTutorialImage()
    {
        return $this->phonetutorialimage;
    }

    /**
     * Get the [phonetutorialenable] column value.
     * 1=Active, 0=Inactive
     * @return int
     */
    public function getPhoneTutorialEnable()
    {
        return $this->phonetutorialenable;
    }

    /**
     * Get the [phoneicon] column value.
     *
     * @return string
     */
    public function getPhoneIcon()
    {
        return $this->phoneicon;
    }

    /**
     * Get the [module_type] column value.
     *
     * @return int
     */
    public function getModuleType()
    {
        return $this->module_type;
    }

    /**
     * Get the [created_by] column value.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * Get the [optionally formatted] temporal [created_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedDate($format = NULL)
    {
        if ($format === null) {
            return $this->created_date;
        } else {
            return $this->created_date instanceof \DateTime ? $this->created_date->format($format) : null;
        }
    }

    /**
     * Get the [modified_by] column value.
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modified_by;
    }

    /**
     * Get the [optionally formatted] temporal [modified_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModifiedDate($format = NULL)
    {
        if ($format === null) {
            return $this->modified_date;
        } else {
            return $this->modified_date instanceof \DateTime ? $this->modified_date->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [projectid] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setProjectId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->projectid !== $v) {
            $this->projectid = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_PROJECTID] = true;
        }

        return $this;
    } // setProjectId()

    /**
     * Set the value of [name] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [image] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setImage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->image !== $v) {
            $this->image = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_IMAGE] = true;
        }

        return $this;
    } // setImage()

    /**
     * Set the value of [sidemenuorder] column.
     *
     * @param int $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setSideMenuOrder($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sidemenuorder !== $v) {
            $this->sidemenuorder = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_SIDEMENUORDER] = true;
        }

        return $this;
    } // setSideMenuOrder()

    /**
     * Set the value of [sidemenuenable] column.
     * 1=Active, 0=Inactive
     * @param int $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setSideMenuEnable($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sidemenuenable !== $v) {
            $this->sidemenuenable = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_SIDEMENUENABLE] = true;
        }

        return $this;
    } // setSideMenuEnable()

    /**
     * Set the value of [homemenuorder] column.
     *
     * @param int $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setHomeMenuOrder($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->homemenuorder !== $v) {
            $this->homemenuorder = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_HOMEMENUORDER] = true;
        }

        return $this;
    } // setHomeMenuOrder()

    /**
     * Set the value of [homemenuenable] column.
     * 1=Active, 0=Inactive
     * @param int $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setHomeMenuEnable($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->homemenuenable !== $v) {
            $this->homemenuenable = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_HOMEMENUENABLE] = true;
        }

        return $this;
    } // setHomeMenuEnable()

    /**
     * Set the value of [toprightmenuorder] column.
     *
     * @param int $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setTopRightMenuOrder($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->toprightmenuorder !== $v) {
            $this->toprightmenuorder = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_TOPRIGHTMENUORDER] = true;
        }

        return $this;
    } // setTopRightMenuOrder()

    /**
     * Set the value of [toprightmenuenable] column.
     * 1=Active, 0=Inactive
     * @param int $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setTopRightMenuEnable($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->toprightmenuenable !== $v) {
            $this->toprightmenuenable = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_TOPRIGHTMENUENABLE] = true;
        }

        return $this;
    } // setTopRightMenuEnable()

    /**
     * Set the value of [tutorialimage] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setTutorialImage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tutorialimage !== $v) {
            $this->tutorialimage = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_TUTORIALIMAGE] = true;
        }

        return $this;
    } // setTutorialImage()

    /**
     * Set the value of [tutorialenable] column.
     * 1=Active, 0=Inactive
     * @param int $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setTutorialEnable($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->tutorialenable !== $v) {
            $this->tutorialenable = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_TUTORIALENABLE] = true;
        }

        return $this;
    } // setTutorialEnable()

    /**
     * Set the value of [phonetutorialimage] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setPhoneTutorialImage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phonetutorialimage !== $v) {
            $this->phonetutorialimage = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_PHONETUTORIALIMAGE] = true;
        }

        return $this;
    } // setPhoneTutorialImage()

    /**
     * Set the value of [phonetutorialenable] column.
     * 1=Active, 0=Inactive
     * @param int $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setPhoneTutorialEnable($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->phonetutorialenable !== $v) {
            $this->phonetutorialenable = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_PHONETUTORIALENABLE] = true;
        }

        return $this;
    } // setPhoneTutorialEnable()

    /**
     * Set the value of [phoneicon] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setPhoneIcon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phoneicon !== $v) {
            $this->phoneicon = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_PHONEICON] = true;
        }

        return $this;
    } // setPhoneIcon()

    /**
     * Set the value of [module_type] column.
     *
     * @param int $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setModuleType($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->module_type !== $v) {
            $this->module_type = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_MODULE_TYPE] = true;
        }

        return $this;
    } // setModuleType()

    /**
     * Set the value of [created_by] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setCreatedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->created_by !== $v) {
            $this->created_by = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_CREATED_BY] = true;
        }

        return $this;
    } // setCreatedBy()

    /**
     * Sets the value of [created_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setCreatedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_date !== null || $dt !== null) {
            if ($this->created_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->created_date->format("Y-m-d H:i:s")) {
                $this->created_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AppMenuListTableMap::COL_CREATED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedDate()

    /**
     * Set the value of [modified_by] column.
     *
     * @param string $v new value
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setModifiedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->modified_by !== $v) {
            $this->modified_by = $v;
            $this->modifiedColumns[AppMenuListTableMap::COL_MODIFIED_BY] = true;
        }

        return $this;
    } // setModifiedBy()

    /**
     * Sets the value of [modified_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\AppMenuList The current object (for fluent API support)
     */
    public function setModifiedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified_date !== null || $dt !== null) {
            if ($this->modified_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->modified_date->format("Y-m-d H:i:s")) {
                $this->modified_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AppMenuListTableMap::COL_MODIFIED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setModifiedDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->sidemenuenable !== 0) {
                return false;
            }

            if ($this->homemenuenable !== 0) {
                return false;
            }

            if ($this->toprightmenuenable !== 0) {
                return false;
            }

            if ($this->tutorialenable !== 0) {
                return false;
            }

            if ($this->phonetutorialenable !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : AppMenuListTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : AppMenuListTableMap::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->projectid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : AppMenuListTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : AppMenuListTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : AppMenuListTableMap::translateFieldName('Image', TableMap::TYPE_PHPNAME, $indexType)];
            $this->image = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : AppMenuListTableMap::translateFieldName('SideMenuOrder', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sidemenuorder = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : AppMenuListTableMap::translateFieldName('SideMenuEnable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sidemenuenable = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : AppMenuListTableMap::translateFieldName('HomeMenuOrder', TableMap::TYPE_PHPNAME, $indexType)];
            $this->homemenuorder = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : AppMenuListTableMap::translateFieldName('HomeMenuEnable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->homemenuenable = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : AppMenuListTableMap::translateFieldName('TopRightMenuOrder', TableMap::TYPE_PHPNAME, $indexType)];
            $this->toprightmenuorder = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : AppMenuListTableMap::translateFieldName('TopRightMenuEnable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->toprightmenuenable = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : AppMenuListTableMap::translateFieldName('TutorialImage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tutorialimage = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : AppMenuListTableMap::translateFieldName('TutorialEnable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tutorialenable = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : AppMenuListTableMap::translateFieldName('PhoneTutorialImage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phonetutorialimage = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : AppMenuListTableMap::translateFieldName('PhoneTutorialEnable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phonetutorialenable = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : AppMenuListTableMap::translateFieldName('PhoneIcon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phoneicon = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : AppMenuListTableMap::translateFieldName('ModuleType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->module_type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : AppMenuListTableMap::translateFieldName('CreatedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : AppMenuListTableMap::translateFieldName('CreatedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : AppMenuListTableMap::translateFieldName('ModifiedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modified_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : AppMenuListTableMap::translateFieldName('ModifiedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->modified_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 21; // 21 = AppMenuListTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\AppMenuList'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AppMenuListTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildAppMenuListQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see AppMenuList::setDeleted()
     * @see AppMenuList::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppMenuListTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildAppMenuListQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppMenuListTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AppMenuListTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AppMenuListTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_PROJECTID)) {
            $modifiedColumns[':p' . $index++]  = 'projectId';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_IMAGE)) {
            $modifiedColumns[':p' . $index++]  = 'image';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_SIDEMENUORDER)) {
            $modifiedColumns[':p' . $index++]  = 'sideMenuOrder';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_SIDEMENUENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'sideMenuEnable';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_HOMEMENUORDER)) {
            $modifiedColumns[':p' . $index++]  = 'homeMenuOrder';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_HOMEMENUENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'homeMenuEnable';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_TOPRIGHTMENUORDER)) {
            $modifiedColumns[':p' . $index++]  = 'topRightMenuOrder';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_TOPRIGHTMENUENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'topRightMenuEnable';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_TUTORIALIMAGE)) {
            $modifiedColumns[':p' . $index++]  = 'tutorialImage';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_TUTORIALENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'tutorialEnable';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_PHONETUTORIALIMAGE)) {
            $modifiedColumns[':p' . $index++]  = 'phoneTutorialImage';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_PHONETUTORIALENABLE)) {
            $modifiedColumns[':p' . $index++]  = 'phoneTutorialEnable';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_PHONEICON)) {
            $modifiedColumns[':p' . $index++]  = 'phoneIcon';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_MODULE_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'module_type';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_CREATED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'created_by';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_CREATED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'created_date';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_MODIFIED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'modified_by';
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_MODIFIED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'modified_date';
        }

        $sql = sprintf(
            'INSERT INTO appMenuList (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                    case 'projectId':
                        $stmt->bindValue($identifier, $this->projectid, PDO::PARAM_STR);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'image':
                        $stmt->bindValue($identifier, $this->image, PDO::PARAM_STR);
                        break;
                    case 'sideMenuOrder':
                        $stmt->bindValue($identifier, $this->sidemenuorder, PDO::PARAM_INT);
                        break;
                    case 'sideMenuEnable':
                        $stmt->bindValue($identifier, $this->sidemenuenable, PDO::PARAM_INT);
                        break;
                    case 'homeMenuOrder':
                        $stmt->bindValue($identifier, $this->homemenuorder, PDO::PARAM_INT);
                        break;
                    case 'homeMenuEnable':
                        $stmt->bindValue($identifier, $this->homemenuenable, PDO::PARAM_INT);
                        break;
                    case 'topRightMenuOrder':
                        $stmt->bindValue($identifier, $this->toprightmenuorder, PDO::PARAM_INT);
                        break;
                    case 'topRightMenuEnable':
                        $stmt->bindValue($identifier, $this->toprightmenuenable, PDO::PARAM_INT);
                        break;
                    case 'tutorialImage':
                        $stmt->bindValue($identifier, $this->tutorialimage, PDO::PARAM_STR);
                        break;
                    case 'tutorialEnable':
                        $stmt->bindValue($identifier, $this->tutorialenable, PDO::PARAM_INT);
                        break;
                    case 'phoneTutorialImage':
                        $stmt->bindValue($identifier, $this->phonetutorialimage, PDO::PARAM_STR);
                        break;
                    case 'phoneTutorialEnable':
                        $stmt->bindValue($identifier, $this->phonetutorialenable, PDO::PARAM_INT);
                        break;
                    case 'phoneIcon':
                        $stmt->bindValue($identifier, $this->phoneicon, PDO::PARAM_STR);
                        break;
                    case 'module_type':
                        $stmt->bindValue($identifier, $this->module_type, PDO::PARAM_INT);
                        break;
                    case 'created_by':
                        $stmt->bindValue($identifier, $this->created_by, PDO::PARAM_STR);
                        break;
                    case 'created_date':
                        $stmt->bindValue($identifier, $this->created_date ? $this->created_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'modified_by':
                        $stmt->bindValue($identifier, $this->modified_by, PDO::PARAM_STR);
                        break;
                    case 'modified_date':
                        $stmt->bindValue($identifier, $this->modified_date ? $this->modified_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = AppMenuListTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getProjectId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getDescription();
                break;
            case 4:
                return $this->getImage();
                break;
            case 5:
                return $this->getSideMenuOrder();
                break;
            case 6:
                return $this->getSideMenuEnable();
                break;
            case 7:
                return $this->getHomeMenuOrder();
                break;
            case 8:
                return $this->getHomeMenuEnable();
                break;
            case 9:
                return $this->getTopRightMenuOrder();
                break;
            case 10:
                return $this->getTopRightMenuEnable();
                break;
            case 11:
                return $this->getTutorialImage();
                break;
            case 12:
                return $this->getTutorialEnable();
                break;
            case 13:
                return $this->getPhoneTutorialImage();
                break;
            case 14:
                return $this->getPhoneTutorialEnable();
                break;
            case 15:
                return $this->getPhoneIcon();
                break;
            case 16:
                return $this->getModuleType();
                break;
            case 17:
                return $this->getCreatedBy();
                break;
            case 18:
                return $this->getCreatedDate();
                break;
            case 19:
                return $this->getModifiedBy();
                break;
            case 20:
                return $this->getModifiedDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['AppMenuList'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['AppMenuList'][$this->hashCode()] = true;
        $keys = AppMenuListTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getProjectId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getDescription(),
            $keys[4] => $this->getImage(),
            $keys[5] => $this->getSideMenuOrder(),
            $keys[6] => $this->getSideMenuEnable(),
            $keys[7] => $this->getHomeMenuOrder(),
            $keys[8] => $this->getHomeMenuEnable(),
            $keys[9] => $this->getTopRightMenuOrder(),
            $keys[10] => $this->getTopRightMenuEnable(),
            $keys[11] => $this->getTutorialImage(),
            $keys[12] => $this->getTutorialEnable(),
            $keys[13] => $this->getPhoneTutorialImage(),
            $keys[14] => $this->getPhoneTutorialEnable(),
            $keys[15] => $this->getPhoneIcon(),
            $keys[16] => $this->getModuleType(),
            $keys[17] => $this->getCreatedBy(),
            $keys[18] => $this->getCreatedDate(),
            $keys[19] => $this->getModifiedBy(),
            $keys[20] => $this->getModifiedDate(),
        );
        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTime) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\AppMenuList
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = AppMenuListTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\AppMenuList
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setProjectId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
            case 4:
                $this->setImage($value);
                break;
            case 5:
                $this->setSideMenuOrder($value);
                break;
            case 6:
                $this->setSideMenuEnable($value);
                break;
            case 7:
                $this->setHomeMenuOrder($value);
                break;
            case 8:
                $this->setHomeMenuEnable($value);
                break;
            case 9:
                $this->setTopRightMenuOrder($value);
                break;
            case 10:
                $this->setTopRightMenuEnable($value);
                break;
            case 11:
                $this->setTutorialImage($value);
                break;
            case 12:
                $this->setTutorialEnable($value);
                break;
            case 13:
                $this->setPhoneTutorialImage($value);
                break;
            case 14:
                $this->setPhoneTutorialEnable($value);
                break;
            case 15:
                $this->setPhoneIcon($value);
                break;
            case 16:
                $this->setModuleType($value);
                break;
            case 17:
                $this->setCreatedBy($value);
                break;
            case 18:
                $this->setCreatedDate($value);
                break;
            case 19:
                $this->setModifiedBy($value);
                break;
            case 20:
                $this->setModifiedDate($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = AppMenuListTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setProjectId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescription($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setImage($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSideMenuOrder($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSideMenuEnable($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setHomeMenuOrder($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setHomeMenuEnable($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTopRightMenuOrder($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setTopRightMenuEnable($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setTutorialImage($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setTutorialEnable($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setPhoneTutorialImage($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setPhoneTutorialEnable($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setPhoneIcon($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setModuleType($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setCreatedBy($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setCreatedDate($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setModifiedBy($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setModifiedDate($arr[$keys[20]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\AppMenuList The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AppMenuListTableMap::DATABASE_NAME);

        if ($this->isColumnModified(AppMenuListTableMap::COL_ID)) {
            $criteria->add(AppMenuListTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_PROJECTID)) {
            $criteria->add(AppMenuListTableMap::COL_PROJECTID, $this->projectid);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_NAME)) {
            $criteria->add(AppMenuListTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_DESCRIPTION)) {
            $criteria->add(AppMenuListTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_IMAGE)) {
            $criteria->add(AppMenuListTableMap::COL_IMAGE, $this->image);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_SIDEMENUORDER)) {
            $criteria->add(AppMenuListTableMap::COL_SIDEMENUORDER, $this->sidemenuorder);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_SIDEMENUENABLE)) {
            $criteria->add(AppMenuListTableMap::COL_SIDEMENUENABLE, $this->sidemenuenable);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_HOMEMENUORDER)) {
            $criteria->add(AppMenuListTableMap::COL_HOMEMENUORDER, $this->homemenuorder);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_HOMEMENUENABLE)) {
            $criteria->add(AppMenuListTableMap::COL_HOMEMENUENABLE, $this->homemenuenable);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_TOPRIGHTMENUORDER)) {
            $criteria->add(AppMenuListTableMap::COL_TOPRIGHTMENUORDER, $this->toprightmenuorder);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_TOPRIGHTMENUENABLE)) {
            $criteria->add(AppMenuListTableMap::COL_TOPRIGHTMENUENABLE, $this->toprightmenuenable);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_TUTORIALIMAGE)) {
            $criteria->add(AppMenuListTableMap::COL_TUTORIALIMAGE, $this->tutorialimage);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_TUTORIALENABLE)) {
            $criteria->add(AppMenuListTableMap::COL_TUTORIALENABLE, $this->tutorialenable);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_PHONETUTORIALIMAGE)) {
            $criteria->add(AppMenuListTableMap::COL_PHONETUTORIALIMAGE, $this->phonetutorialimage);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_PHONETUTORIALENABLE)) {
            $criteria->add(AppMenuListTableMap::COL_PHONETUTORIALENABLE, $this->phonetutorialenable);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_PHONEICON)) {
            $criteria->add(AppMenuListTableMap::COL_PHONEICON, $this->phoneicon);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_MODULE_TYPE)) {
            $criteria->add(AppMenuListTableMap::COL_MODULE_TYPE, $this->module_type);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_CREATED_BY)) {
            $criteria->add(AppMenuListTableMap::COL_CREATED_BY, $this->created_by);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_CREATED_DATE)) {
            $criteria->add(AppMenuListTableMap::COL_CREATED_DATE, $this->created_date);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_MODIFIED_BY)) {
            $criteria->add(AppMenuListTableMap::COL_MODIFIED_BY, $this->modified_by);
        }
        if ($this->isColumnModified(AppMenuListTableMap::COL_MODIFIED_DATE)) {
            $criteria->add(AppMenuListTableMap::COL_MODIFIED_DATE, $this->modified_date);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildAppMenuListQuery::create();
        $criteria->add(AppMenuListTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \AppMenuList (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setProjectId($this->getProjectId());
        $copyObj->setName($this->getName());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setImage($this->getImage());
        $copyObj->setSideMenuOrder($this->getSideMenuOrder());
        $copyObj->setSideMenuEnable($this->getSideMenuEnable());
        $copyObj->setHomeMenuOrder($this->getHomeMenuOrder());
        $copyObj->setHomeMenuEnable($this->getHomeMenuEnable());
        $copyObj->setTopRightMenuOrder($this->getTopRightMenuOrder());
        $copyObj->setTopRightMenuEnable($this->getTopRightMenuEnable());
        $copyObj->setTutorialImage($this->getTutorialImage());
        $copyObj->setTutorialEnable($this->getTutorialEnable());
        $copyObj->setPhoneTutorialImage($this->getPhoneTutorialImage());
        $copyObj->setPhoneTutorialEnable($this->getPhoneTutorialEnable());
        $copyObj->setPhoneIcon($this->getPhoneIcon());
        $copyObj->setModuleType($this->getModuleType());
        $copyObj->setCreatedBy($this->getCreatedBy());
        $copyObj->setCreatedDate($this->getCreatedDate());
        $copyObj->setModifiedBy($this->getModifiedBy());
        $copyObj->setModifiedDate($this->getModifiedDate());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \AppMenuList Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->projectid = null;
        $this->name = null;
        $this->description = null;
        $this->image = null;
        $this->sidemenuorder = null;
        $this->sidemenuenable = null;
        $this->homemenuorder = null;
        $this->homemenuenable = null;
        $this->toprightmenuorder = null;
        $this->toprightmenuenable = null;
        $this->tutorialimage = null;
        $this->tutorialenable = null;
        $this->phonetutorialimage = null;
        $this->phonetutorialenable = null;
        $this->phoneicon = null;
        $this->module_type = null;
        $this->created_by = null;
        $this->created_date = null;
        $this->modified_by = null;
        $this->modified_date = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AppMenuListTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
