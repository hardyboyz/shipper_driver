<?php

namespace Base;

use \AlarmStatus as ChildAlarmStatus;
use \AlarmStatusQuery as ChildAlarmStatusQuery;
use \Exception;
use \PDO;
use Map\AlarmStatusTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'alarmStatus' table.
 *
 *
 *
 * @method     ChildAlarmStatusQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildAlarmStatusQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildAlarmStatusQuery orderByUnitId($order = Criteria::ASC) Order by the unitId column
 * @method     ChildAlarmStatusQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildAlarmStatusQuery orderByAlarmHistoryId($order = Criteria::ASC) Order by the alarmHistoryId column
 * @method     ChildAlarmStatusQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildAlarmStatusQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 * @method     ChildAlarmStatusQuery orderByTriggeredDate($order = Criteria::ASC) Order by the triggered_date column
 *
 * @method     ChildAlarmStatusQuery groupById() Group by the id column
 * @method     ChildAlarmStatusQuery groupByProjectId() Group by the projectId column
 * @method     ChildAlarmStatusQuery groupByUnitId() Group by the unitId column
 * @method     ChildAlarmStatusQuery groupByStatus() Group by the status column
 * @method     ChildAlarmStatusQuery groupByAlarmHistoryId() Group by the alarmHistoryId column
 * @method     ChildAlarmStatusQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildAlarmStatusQuery groupByModifiedDate() Group by the modified_date column
 * @method     ChildAlarmStatusQuery groupByTriggeredDate() Group by the triggered_date column
 *
 * @method     ChildAlarmStatusQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAlarmStatusQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAlarmStatusQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAlarmStatusQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAlarmStatusQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAlarmStatusQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAlarmStatus findOne(ConnectionInterface $con = null) Return the first ChildAlarmStatus matching the query
 * @method     ChildAlarmStatus findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAlarmStatus matching the query, or a new ChildAlarmStatus object populated from the query conditions when no match is found
 *
 * @method     ChildAlarmStatus findOneById(string $id) Return the first ChildAlarmStatus filtered by the id column
 * @method     ChildAlarmStatus findOneByProjectId(string $projectId) Return the first ChildAlarmStatus filtered by the projectId column
 * @method     ChildAlarmStatus findOneByUnitId(string $unitId) Return the first ChildAlarmStatus filtered by the unitId column
 * @method     ChildAlarmStatus findOneByStatus(string $status) Return the first ChildAlarmStatus filtered by the status column
 * @method     ChildAlarmStatus findOneByAlarmHistoryId(string $alarmHistoryId) Return the first ChildAlarmStatus filtered by the alarmHistoryId column
 * @method     ChildAlarmStatus findOneByCreatedDate(string $created_date) Return the first ChildAlarmStatus filtered by the created_date column
 * @method     ChildAlarmStatus findOneByModifiedDate(string $modified_date) Return the first ChildAlarmStatus filtered by the modified_date column
 * @method     ChildAlarmStatus findOneByTriggeredDate(string $triggered_date) Return the first ChildAlarmStatus filtered by the triggered_date column *

 * @method     ChildAlarmStatus requirePk($key, ConnectionInterface $con = null) Return the ChildAlarmStatus by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatus requireOne(ConnectionInterface $con = null) Return the first ChildAlarmStatus matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAlarmStatus requireOneById(string $id) Return the first ChildAlarmStatus filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatus requireOneByProjectId(string $projectId) Return the first ChildAlarmStatus filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatus requireOneByUnitId(string $unitId) Return the first ChildAlarmStatus filtered by the unitId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatus requireOneByStatus(string $status) Return the first ChildAlarmStatus filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatus requireOneByAlarmHistoryId(string $alarmHistoryId) Return the first ChildAlarmStatus filtered by the alarmHistoryId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatus requireOneByCreatedDate(string $created_date) Return the first ChildAlarmStatus filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatus requireOneByModifiedDate(string $modified_date) Return the first ChildAlarmStatus filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAlarmStatus requireOneByTriggeredDate(string $triggered_date) Return the first ChildAlarmStatus filtered by the triggered_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAlarmStatus[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAlarmStatus objects based on current ModelCriteria
 * @method     ChildAlarmStatus[]|ObjectCollection findById(string $id) Return ChildAlarmStatus objects filtered by the id column
 * @method     ChildAlarmStatus[]|ObjectCollection findByProjectId(string $projectId) Return ChildAlarmStatus objects filtered by the projectId column
 * @method     ChildAlarmStatus[]|ObjectCollection findByUnitId(string $unitId) Return ChildAlarmStatus objects filtered by the unitId column
 * @method     ChildAlarmStatus[]|ObjectCollection findByStatus(string $status) Return ChildAlarmStatus objects filtered by the status column
 * @method     ChildAlarmStatus[]|ObjectCollection findByAlarmHistoryId(string $alarmHistoryId) Return ChildAlarmStatus objects filtered by the alarmHistoryId column
 * @method     ChildAlarmStatus[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildAlarmStatus objects filtered by the created_date column
 * @method     ChildAlarmStatus[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildAlarmStatus objects filtered by the modified_date column
 * @method     ChildAlarmStatus[]|ObjectCollection findByTriggeredDate(string $triggered_date) Return ChildAlarmStatus objects filtered by the triggered_date column
 * @method     ChildAlarmStatus[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AlarmStatusQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\AlarmStatusQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\AlarmStatus', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAlarmStatusQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAlarmStatusQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAlarmStatusQuery) {
            return $criteria;
        }
        $query = new ChildAlarmStatusQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAlarmStatus|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = AlarmStatusTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AlarmStatusTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAlarmStatus A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, unitId, status, alarmHistoryId, created_date, modified_date, triggered_date FROM alarmStatus WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAlarmStatus $obj */
            $obj = new ChildAlarmStatus();
            $obj->hydrate($row);
            AlarmStatusTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAlarmStatus|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AlarmStatusTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AlarmStatusTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the unitId column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId('fooValue');   // WHERE unitId = 'fooValue'
     * $query->filterByUnitId('%fooValue%'); // WHERE unitId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitId)) {
                $unitId = str_replace('*', '%', $unitId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusTableMap::COL_UNITID, $unitId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%'); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $status)) {
                $status = str_replace('*', '%', $status);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the alarmHistoryId column
     *
     * Example usage:
     * <code>
     * $query->filterByAlarmHistoryId('fooValue');   // WHERE alarmHistoryId = 'fooValue'
     * $query->filterByAlarmHistoryId('%fooValue%'); // WHERE alarmHistoryId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alarmHistoryId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterByAlarmHistoryId($alarmHistoryId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alarmHistoryId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alarmHistoryId)) {
                $alarmHistoryId = str_replace('*', '%', $alarmHistoryId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(AlarmStatusTableMap::COL_ALARMHISTORYID, $alarmHistoryId, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(AlarmStatusTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(AlarmStatusTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlarmStatusTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(AlarmStatusTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(AlarmStatusTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlarmStatusTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query on the triggered_date column
     *
     * Example usage:
     * <code>
     * $query->filterByTriggeredDate('2011-03-14'); // WHERE triggered_date = '2011-03-14'
     * $query->filterByTriggeredDate('now'); // WHERE triggered_date = '2011-03-14'
     * $query->filterByTriggeredDate(array('max' => 'yesterday')); // WHERE triggered_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $triggeredDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function filterByTriggeredDate($triggeredDate = null, $comparison = null)
    {
        if (is_array($triggeredDate)) {
            $useMinMax = false;
            if (isset($triggeredDate['min'])) {
                $this->addUsingAlias(AlarmStatusTableMap::COL_TRIGGERED_DATE, $triggeredDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($triggeredDate['max'])) {
                $this->addUsingAlias(AlarmStatusTableMap::COL_TRIGGERED_DATE, $triggeredDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AlarmStatusTableMap::COL_TRIGGERED_DATE, $triggeredDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAlarmStatus $alarmStatus Object to remove from the list of results
     *
     * @return $this|ChildAlarmStatusQuery The current query, for fluid interface
     */
    public function prune($alarmStatus = null)
    {
        if ($alarmStatus) {
            $this->addUsingAlias(AlarmStatusTableMap::COL_ID, $alarmStatus->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the alarmStatus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AlarmStatusTableMap::clearInstancePool();
            AlarmStatusTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AlarmStatusTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AlarmStatusTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AlarmStatusTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AlarmStatusQuery
