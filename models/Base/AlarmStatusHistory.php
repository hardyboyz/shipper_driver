<?php

namespace Base;

use \AlarmStatusHistoryQuery as ChildAlarmStatusHistoryQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\AlarmStatusHistoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'alarmStatusHistory' table.
 *
 *
 *
* @package    propel.generator..Base
*/
abstract class AlarmStatusHistory implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\AlarmStatusHistoryTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        string
     */
    protected $id;

    /**
     * The value for the projectid field.
     *
     * @var        string
     */
    protected $projectid;

    /**
     * The value for the unitid field.
     *
     * @var        string
     */
    protected $unitid;

    /**
     * The value for the zoneid field.
     *
     * @var        string
     */
    protected $zoneid;

    /**
     * The value for the zonename field.
     *
     * @var        string
     */
    protected $zonename;

    /**
     * The value for the zonetype field.
     *
     * @var        string
     */
    protected $zonetype;

    /**
     * The value for the areaid field.
     *
     * @var        string
     */
    protected $areaid;

    /**
     * The value for the areaname field.
     *
     * @var        string
     */
    protected $areaname;

    /**
     * The value for the triggered_date field.
     *
     * @var        \DateTime
     */
    protected $triggered_date;

    /**
     * The value for the acknowledged_date field.
     *
     * @var        \DateTime
     */
    protected $acknowledged_date;

    /**
     * The value for the acknowledged_by field.
     *
     * @var        string
     */
    protected $acknowledged_by;

    /**
     * The value for the remarks field.
     *
     * @var        string
     */
    protected $remarks;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\AlarmStatusHistory object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>AlarmStatusHistory</code> instance.  If
     * <code>obj</code> is an instance of <code>AlarmStatusHistory</code>, delegates to
     * <code>equals(AlarmStatusHistory)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|AlarmStatusHistory The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [projectid] column value.
     *
     * @return string
     */
    public function getProjectId()
    {
        return $this->projectid;
    }

    /**
     * Get the [unitid] column value.
     *
     * @return string
     */
    public function getUnitId()
    {
        return $this->unitid;
    }

    /**
     * Get the [zoneid] column value.
     *
     * @return string
     */
    public function getZoneId()
    {
        return $this->zoneid;
    }

    /**
     * Get the [zonename] column value.
     *
     * @return string
     */
    public function getZoneName()
    {
        return $this->zonename;
    }

    /**
     * Get the [zonetype] column value.
     *
     * @return string
     */
    public function getZoneType()
    {
        return $this->zonetype;
    }

    /**
     * Get the [areaid] column value.
     *
     * @return string
     */
    public function getAreaId()
    {
        return $this->areaid;
    }

    /**
     * Get the [areaname] column value.
     *
     * @return string
     */
    public function getAreaName()
    {
        return $this->areaname;
    }

    /**
     * Get the [optionally formatted] temporal [triggered_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getTriggeredDate($format = NULL)
    {
        if ($format === null) {
            return $this->triggered_date;
        } else {
            return $this->triggered_date instanceof \DateTime ? $this->triggered_date->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [acknowledged_date] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getAcknowledgedDate($format = NULL)
    {
        if ($format === null) {
            return $this->acknowledged_date;
        } else {
            return $this->acknowledged_date instanceof \DateTime ? $this->acknowledged_date->format($format) : null;
        }
    }

    /**
     * Get the [acknowledged_by] column value.
     *
     * @return string
     */
    public function getAcknowledgedBy()
    {
        return $this->acknowledged_by;
    }

    /**
     * Get the [remarks] column value.
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [projectid] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setProjectId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->projectid !== $v) {
            $this->projectid = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_PROJECTID] = true;
        }

        return $this;
    } // setProjectId()

    /**
     * Set the value of [unitid] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setUnitId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->unitid !== $v) {
            $this->unitid = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_UNITID] = true;
        }

        return $this;
    } // setUnitId()

    /**
     * Set the value of [zoneid] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setZoneId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->zoneid !== $v) {
            $this->zoneid = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_ZONEID] = true;
        }

        return $this;
    } // setZoneId()

    /**
     * Set the value of [zonename] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setZoneName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->zonename !== $v) {
            $this->zonename = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_ZONENAME] = true;
        }

        return $this;
    } // setZoneName()

    /**
     * Set the value of [zonetype] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setZoneType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->zonetype !== $v) {
            $this->zonetype = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_ZONETYPE] = true;
        }

        return $this;
    } // setZoneType()

    /**
     * Set the value of [areaid] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setAreaId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->areaid !== $v) {
            $this->areaid = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_AREAID] = true;
        }

        return $this;
    } // setAreaId()

    /**
     * Set the value of [areaname] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setAreaName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->areaname !== $v) {
            $this->areaname = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_AREANAME] = true;
        }

        return $this;
    } // setAreaName()

    /**
     * Sets the value of [triggered_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setTriggeredDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->triggered_date !== null || $dt !== null) {
            if ($this->triggered_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->triggered_date->format("Y-m-d H:i:s")) {
                $this->triggered_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setTriggeredDate()

    /**
     * Sets the value of [acknowledged_date] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setAcknowledgedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->acknowledged_date !== null || $dt !== null) {
            if ($this->acknowledged_date === null || $dt === null || $dt->format("Y-m-d H:i:s") !== $this->acknowledged_date->format("Y-m-d H:i:s")) {
                $this->acknowledged_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setAcknowledgedDate()

    /**
     * Set the value of [acknowledged_by] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setAcknowledgedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->acknowledged_by !== $v) {
            $this->acknowledged_by = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_BY] = true;
        }

        return $this;
    } // setAcknowledgedBy()

    /**
     * Set the value of [remarks] column.
     *
     * @param string $v new value
     * @return $this|\AlarmStatusHistory The current object (for fluent API support)
     */
    public function setRemarks($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remarks !== $v) {
            $this->remarks = $v;
            $this->modifiedColumns[AlarmStatusHistoryTableMap::COL_REMARKS] = true;
        }

        return $this;
    } // setRemarks()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->projectid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('UnitId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unitid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('ZoneId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->zoneid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('ZoneName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->zonename = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('ZoneType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->zonetype = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('AreaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->areaid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('AreaName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->areaname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('TriggeredDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->triggered_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('AcknowledgedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->acknowledged_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('AcknowledgedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->acknowledged_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : AlarmStatusHistoryTableMap::translateFieldName('Remarks', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remarks = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = AlarmStatusHistoryTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\AlarmStatusHistory'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AlarmStatusHistoryTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildAlarmStatusHistoryQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see AlarmStatusHistory::setDeleted()
     * @see AlarmStatusHistory::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusHistoryTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildAlarmStatusHistoryQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusHistoryTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $isInsert = $this->isNew();
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                AlarmStatusHistoryTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_PROJECTID)) {
            $modifiedColumns[':p' . $index++]  = 'projectId';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_UNITID)) {
            $modifiedColumns[':p' . $index++]  = 'unitId';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ZONEID)) {
            $modifiedColumns[':p' . $index++]  = 'zoneId';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ZONENAME)) {
            $modifiedColumns[':p' . $index++]  = 'zoneName';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ZONETYPE)) {
            $modifiedColumns[':p' . $index++]  = 'zoneType';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_AREAID)) {
            $modifiedColumns[':p' . $index++]  = 'areaId';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_AREANAME)) {
            $modifiedColumns[':p' . $index++]  = 'areaName';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'triggered_date';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'acknowledged_date';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'acknowledged_by';
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_REMARKS)) {
            $modifiedColumns[':p' . $index++]  = 'remarks';
        }

        $sql = sprintf(
            'INSERT INTO alarmStatusHistory (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                    case 'projectId':
                        $stmt->bindValue($identifier, $this->projectid, PDO::PARAM_STR);
                        break;
                    case 'unitId':
                        $stmt->bindValue($identifier, $this->unitid, PDO::PARAM_STR);
                        break;
                    case 'zoneId':
                        $stmt->bindValue($identifier, $this->zoneid, PDO::PARAM_STR);
                        break;
                    case 'zoneName':
                        $stmt->bindValue($identifier, $this->zonename, PDO::PARAM_STR);
                        break;
                    case 'zoneType':
                        $stmt->bindValue($identifier, $this->zonetype, PDO::PARAM_STR);
                        break;
                    case 'areaId':
                        $stmt->bindValue($identifier, $this->areaid, PDO::PARAM_STR);
                        break;
                    case 'areaName':
                        $stmt->bindValue($identifier, $this->areaname, PDO::PARAM_STR);
                        break;
                    case 'triggered_date':
                        $stmt->bindValue($identifier, $this->triggered_date ? $this->triggered_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'acknowledged_date':
                        $stmt->bindValue($identifier, $this->acknowledged_date ? $this->acknowledged_date->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'acknowledged_by':
                        $stmt->bindValue($identifier, $this->acknowledged_by, PDO::PARAM_STR);
                        break;
                    case 'remarks':
                        $stmt->bindValue($identifier, $this->remarks, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = AlarmStatusHistoryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getProjectId();
                break;
            case 2:
                return $this->getUnitId();
                break;
            case 3:
                return $this->getZoneId();
                break;
            case 4:
                return $this->getZoneName();
                break;
            case 5:
                return $this->getZoneType();
                break;
            case 6:
                return $this->getAreaId();
                break;
            case 7:
                return $this->getAreaName();
                break;
            case 8:
                return $this->getTriggeredDate();
                break;
            case 9:
                return $this->getAcknowledgedDate();
                break;
            case 10:
                return $this->getAcknowledgedBy();
                break;
            case 11:
                return $this->getRemarks();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['AlarmStatusHistory'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['AlarmStatusHistory'][$this->hashCode()] = true;
        $keys = AlarmStatusHistoryTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getProjectId(),
            $keys[2] => $this->getUnitId(),
            $keys[3] => $this->getZoneId(),
            $keys[4] => $this->getZoneName(),
            $keys[5] => $this->getZoneType(),
            $keys[6] => $this->getAreaId(),
            $keys[7] => $this->getAreaName(),
            $keys[8] => $this->getTriggeredDate(),
            $keys[9] => $this->getAcknowledgedDate(),
            $keys[10] => $this->getAcknowledgedBy(),
            $keys[11] => $this->getRemarks(),
        );
        if ($result[$keys[8]] instanceof \DateTime) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTime) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\AlarmStatusHistory
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = AlarmStatusHistoryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\AlarmStatusHistory
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setProjectId($value);
                break;
            case 2:
                $this->setUnitId($value);
                break;
            case 3:
                $this->setZoneId($value);
                break;
            case 4:
                $this->setZoneName($value);
                break;
            case 5:
                $this->setZoneType($value);
                break;
            case 6:
                $this->setAreaId($value);
                break;
            case 7:
                $this->setAreaName($value);
                break;
            case 8:
                $this->setTriggeredDate($value);
                break;
            case 9:
                $this->setAcknowledgedDate($value);
                break;
            case 10:
                $this->setAcknowledgedBy($value);
                break;
            case 11:
                $this->setRemarks($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = AlarmStatusHistoryTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setProjectId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setUnitId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setZoneId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setZoneName($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setZoneType($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setAreaId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setAreaName($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTriggeredDate($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setAcknowledgedDate($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setAcknowledgedBy($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setRemarks($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\AlarmStatusHistory The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(AlarmStatusHistoryTableMap::DATABASE_NAME);

        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ID)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_PROJECTID)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_PROJECTID, $this->projectid);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_UNITID)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_UNITID, $this->unitid);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ZONEID)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_ZONEID, $this->zoneid);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ZONENAME)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_ZONENAME, $this->zonename);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ZONETYPE)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_ZONETYPE, $this->zonetype);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_AREAID)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_AREAID, $this->areaid);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_AREANAME)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_AREANAME, $this->areaname);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE, $this->triggered_date);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE, $this->acknowledged_date);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_BY)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_BY, $this->acknowledged_by);
        }
        if ($this->isColumnModified(AlarmStatusHistoryTableMap::COL_REMARKS)) {
            $criteria->add(AlarmStatusHistoryTableMap::COL_REMARKS, $this->remarks);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildAlarmStatusHistoryQuery::create();
        $criteria->add(AlarmStatusHistoryTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \AlarmStatusHistory (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setProjectId($this->getProjectId());
        $copyObj->setUnitId($this->getUnitId());
        $copyObj->setZoneId($this->getZoneId());
        $copyObj->setZoneName($this->getZoneName());
        $copyObj->setZoneType($this->getZoneType());
        $copyObj->setAreaId($this->getAreaId());
        $copyObj->setAreaName($this->getAreaName());
        $copyObj->setTriggeredDate($this->getTriggeredDate());
        $copyObj->setAcknowledgedDate($this->getAcknowledgedDate());
        $copyObj->setAcknowledgedBy($this->getAcknowledgedBy());
        $copyObj->setRemarks($this->getRemarks());
        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \AlarmStatusHistory Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->projectid = null;
        $this->unitid = null;
        $this->zoneid = null;
        $this->zonename = null;
        $this->zonetype = null;
        $this->areaid = null;
        $this->areaname = null;
        $this->triggered_date = null;
        $this->acknowledged_date = null;
        $this->acknowledged_by = null;
        $this->remarks = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(AlarmStatusHistoryTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
