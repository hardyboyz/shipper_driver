<?php

namespace Base;

use \RknLicenceType as ChildRknLicenceType;
use \RknLicenceTypeQuery as ChildRknLicenceTypeQuery;
use \Exception;
use \PDO;
use Map\RknLicenceTypeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'rkn_license_type' table.
 *
 *
 *
 * @method     ChildRknLicenceTypeQuery orderByID($order = Criteria::ASC) Order by the id column
 * @method     ChildRknLicenceTypeQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildRknLicenceTypeQuery orderByDescription($order = Criteria::ASC) Order by the description column
 *
 * @method     ChildRknLicenceTypeQuery groupByID() Group by the id column
 * @method     ChildRknLicenceTypeQuery groupByName() Group by the name column
 * @method     ChildRknLicenceTypeQuery groupByDescription() Group by the description column
 *
 * @method     ChildRknLicenceTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRknLicenceTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRknLicenceTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRknLicenceTypeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRknLicenceTypeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRknLicenceTypeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRknLicenceTypeQuery leftJoinRknLicence($relationAlias = null) Adds a LEFT JOIN clause to the query using the RknLicence relation
 * @method     ChildRknLicenceTypeQuery rightJoinRknLicence($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RknLicence relation
 * @method     ChildRknLicenceTypeQuery innerJoinRknLicence($relationAlias = null) Adds a INNER JOIN clause to the query using the RknLicence relation
 *
 * @method     ChildRknLicenceTypeQuery joinWithRknLicence($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RknLicence relation
 *
 * @method     ChildRknLicenceTypeQuery leftJoinWithRknLicence() Adds a LEFT JOIN clause and with to the query using the RknLicence relation
 * @method     ChildRknLicenceTypeQuery rightJoinWithRknLicence() Adds a RIGHT JOIN clause and with to the query using the RknLicence relation
 * @method     ChildRknLicenceTypeQuery innerJoinWithRknLicence() Adds a INNER JOIN clause and with to the query using the RknLicence relation
 *
 * @method     \RknLicenceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRknLicenceType findOne(ConnectionInterface $con = null) Return the first ChildRknLicenceType matching the query
 * @method     ChildRknLicenceType findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRknLicenceType matching the query, or a new ChildRknLicenceType object populated from the query conditions when no match is found
 *
 * @method     ChildRknLicenceType findOneByID(int $id) Return the first ChildRknLicenceType filtered by the id column
 * @method     ChildRknLicenceType findOneByName(string $name) Return the first ChildRknLicenceType filtered by the name column
 * @method     ChildRknLicenceType findOneByDescription(string $description) Return the first ChildRknLicenceType filtered by the description column *

 * @method     ChildRknLicenceType requirePk($key, ConnectionInterface $con = null) Return the ChildRknLicenceType by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknLicenceType requireOne(ConnectionInterface $con = null) Return the first ChildRknLicenceType matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRknLicenceType requireOneByID(int $id) Return the first ChildRknLicenceType filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknLicenceType requireOneByName(string $name) Return the first ChildRknLicenceType filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknLicenceType requireOneByDescription(string $description) Return the first ChildRknLicenceType filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRknLicenceType[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRknLicenceType objects based on current ModelCriteria
 * @method     ChildRknLicenceType[]|ObjectCollection findByID(int $id) Return ChildRknLicenceType objects filtered by the id column
 * @method     ChildRknLicenceType[]|ObjectCollection findByName(string $name) Return ChildRknLicenceType objects filtered by the name column
 * @method     ChildRknLicenceType[]|ObjectCollection findByDescription(string $description) Return ChildRknLicenceType objects filtered by the description column
 * @method     ChildRknLicenceType[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RknLicenceTypeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RknLicenceTypeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\RknLicenceType', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRknLicenceTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRknLicenceTypeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRknLicenceTypeQuery) {
            return $criteria;
        }
        $query = new ChildRknLicenceTypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRknLicenceType|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RknLicenceTypeTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RknLicenceTypeTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRknLicenceType A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, description FROM rkn_license_type WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRknLicenceType $obj */
            $obj = new ChildRknLicenceType();
            $obj->hydrate($row);
            RknLicenceTypeTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRknLicenceType|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRknLicenceTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RknLicenceTypeTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRknLicenceTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RknLicenceTypeTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByID(1234); // WHERE id = 1234
     * $query->filterByID(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByID(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $iD The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknLicenceTypeQuery The current query, for fluid interface
     */
    public function filterByID($iD = null, $comparison = null)
    {
        if (is_array($iD)) {
            $useMinMax = false;
            if (isset($iD['min'])) {
                $this->addUsingAlias(RknLicenceTypeTableMap::COL_ID, $iD['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iD['max'])) {
                $this->addUsingAlias(RknLicenceTypeTableMap::COL_ID, $iD['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RknLicenceTypeTableMap::COL_ID, $iD, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknLicenceTypeQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RknLicenceTypeTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknLicenceTypeQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RknLicenceTypeTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related \RknLicence object
     *
     * @param \RknLicence|ObjectCollection $rknLicence the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRknLicenceTypeQuery The current query, for fluid interface
     */
    public function filterByRknLicence($rknLicence, $comparison = null)
    {
        if ($rknLicence instanceof \RknLicence) {
            return $this
                ->addUsingAlias(RknLicenceTypeTableMap::COL_ID, $rknLicence->getType(), $comparison);
        } elseif ($rknLicence instanceof ObjectCollection) {
            return $this
                ->useRknLicenceQuery()
                ->filterByPrimaryKeys($rknLicence->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRknLicence() only accepts arguments of type \RknLicence or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RknLicence relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRknLicenceTypeQuery The current query, for fluid interface
     */
    public function joinRknLicence($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RknLicence');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RknLicence');
        }

        return $this;
    }

    /**
     * Use the RknLicence relation RknLicence object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RknLicenceQuery A secondary query class using the current class as primary query
     */
    public function useRknLicenceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRknLicence($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RknLicence', '\RknLicenceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRknLicenceType $rknLicenceType Object to remove from the list of results
     *
     * @return $this|ChildRknLicenceTypeQuery The current query, for fluid interface
     */
    public function prune($rknLicenceType = null)
    {
        if ($rknLicenceType) {
            $this->addUsingAlias(RknLicenceTypeTableMap::COL_ID, $rknLicenceType->getID(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the rkn_license_type table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RknLicenceTypeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RknLicenceTypeTableMap::clearInstancePool();
            RknLicenceTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RknLicenceTypeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RknLicenceTypeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RknLicenceTypeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RknLicenceTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RknLicenceTypeQuery
