<?php

namespace Base;

use \RknPackage as ChildRknPackage;
use \RknPackageQuery as ChildRknPackageQuery;
use \Exception;
use \PDO;
use Map\RknPackageTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'rkn_package' table.
 *
 *
 *
 * @method     ChildRknPackageQuery orderByID($order = Criteria::ASC) Order by the id column
 * @method     ChildRknPackageQuery orderByAppID($order = Criteria::ASC) Order by the appId column
 * @method     ChildRknPackageQuery orderByPackageID($order = Criteria::ASC) Order by the packageId column
 * @method     ChildRknPackageQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildRknPackageQuery orderByDescription($order = Criteria::ASC) Order by the description column
 *
 * @method     ChildRknPackageQuery groupByID() Group by the id column
 * @method     ChildRknPackageQuery groupByAppID() Group by the appId column
 * @method     ChildRknPackageQuery groupByPackageID() Group by the packageId column
 * @method     ChildRknPackageQuery groupByName() Group by the name column
 * @method     ChildRknPackageQuery groupByDescription() Group by the description column
 *
 * @method     ChildRknPackageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRknPackageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRknPackageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRknPackageQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRknPackageQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRknPackageQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRknPackageQuery leftJoinRknLicence($relationAlias = null) Adds a LEFT JOIN clause to the query using the RknLicence relation
 * @method     ChildRknPackageQuery rightJoinRknLicence($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RknLicence relation
 * @method     ChildRknPackageQuery innerJoinRknLicence($relationAlias = null) Adds a INNER JOIN clause to the query using the RknLicence relation
 *
 * @method     ChildRknPackageQuery joinWithRknLicence($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RknLicence relation
 *
 * @method     ChildRknPackageQuery leftJoinWithRknLicence() Adds a LEFT JOIN clause and with to the query using the RknLicence relation
 * @method     ChildRknPackageQuery rightJoinWithRknLicence() Adds a RIGHT JOIN clause and with to the query using the RknLicence relation
 * @method     ChildRknPackageQuery innerJoinWithRknLicence() Adds a INNER JOIN clause and with to the query using the RknLicence relation
 *
 * @method     \RknLicenceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRknPackage findOne(ConnectionInterface $con = null) Return the first ChildRknPackage matching the query
 * @method     ChildRknPackage findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRknPackage matching the query, or a new ChildRknPackage object populated from the query conditions when no match is found
 *
 * @method     ChildRknPackage findOneByID(int $id) Return the first ChildRknPackage filtered by the id column
 * @method     ChildRknPackage findOneByAppID(int $appId) Return the first ChildRknPackage filtered by the appId column
 * @method     ChildRknPackage findOneByPackageID(int $packageId) Return the first ChildRknPackage filtered by the packageId column
 * @method     ChildRknPackage findOneByName(string $name) Return the first ChildRknPackage filtered by the name column
 * @method     ChildRknPackage findOneByDescription(string $description) Return the first ChildRknPackage filtered by the description column *

 * @method     ChildRknPackage requirePk($key, ConnectionInterface $con = null) Return the ChildRknPackage by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknPackage requireOne(ConnectionInterface $con = null) Return the first ChildRknPackage matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRknPackage requireOneByID(int $id) Return the first ChildRknPackage filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknPackage requireOneByAppID(int $appId) Return the first ChildRknPackage filtered by the appId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknPackage requireOneByPackageID(int $packageId) Return the first ChildRknPackage filtered by the packageId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknPackage requireOneByName(string $name) Return the first ChildRknPackage filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRknPackage requireOneByDescription(string $description) Return the first ChildRknPackage filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRknPackage[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRknPackage objects based on current ModelCriteria
 * @method     ChildRknPackage[]|ObjectCollection findByID(int $id) Return ChildRknPackage objects filtered by the id column
 * @method     ChildRknPackage[]|ObjectCollection findByAppID(int $appId) Return ChildRknPackage objects filtered by the appId column
 * @method     ChildRknPackage[]|ObjectCollection findByPackageID(int $packageId) Return ChildRknPackage objects filtered by the packageId column
 * @method     ChildRknPackage[]|ObjectCollection findByName(string $name) Return ChildRknPackage objects filtered by the name column
 * @method     ChildRknPackage[]|ObjectCollection findByDescription(string $description) Return ChildRknPackage objects filtered by the description column
 * @method     ChildRknPackage[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RknPackageQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RknPackageQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\RknPackage', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRknPackageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRknPackageQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRknPackageQuery) {
            return $criteria;
        }
        $query = new ChildRknPackageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRknPackage|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RknPackageTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RknPackageTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRknPackage A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, appId, packageId, name, description FROM rkn_package WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRknPackage $obj */
            $obj = new ChildRknPackage();
            $obj->hydrate($row);
            RknPackageTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRknPackage|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRknPackageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RknPackageTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRknPackageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RknPackageTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByID(1234); // WHERE id = 1234
     * $query->filterByID(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByID(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $iD The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknPackageQuery The current query, for fluid interface
     */
    public function filterByID($iD = null, $comparison = null)
    {
        if (is_array($iD)) {
            $useMinMax = false;
            if (isset($iD['min'])) {
                $this->addUsingAlias(RknPackageTableMap::COL_ID, $iD['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iD['max'])) {
                $this->addUsingAlias(RknPackageTableMap::COL_ID, $iD['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RknPackageTableMap::COL_ID, $iD, $comparison);
    }

    /**
     * Filter the query on the appId column
     *
     * Example usage:
     * <code>
     * $query->filterByAppID(1234); // WHERE appId = 1234
     * $query->filterByAppID(array(12, 34)); // WHERE appId IN (12, 34)
     * $query->filterByAppID(array('min' => 12)); // WHERE appId > 12
     * </code>
     *
     * @param     mixed $appID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknPackageQuery The current query, for fluid interface
     */
    public function filterByAppID($appID = null, $comparison = null)
    {
        if (is_array($appID)) {
            $useMinMax = false;
            if (isset($appID['min'])) {
                $this->addUsingAlias(RknPackageTableMap::COL_APPID, $appID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($appID['max'])) {
                $this->addUsingAlias(RknPackageTableMap::COL_APPID, $appID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RknPackageTableMap::COL_APPID, $appID, $comparison);
    }

    /**
     * Filter the query on the packageId column
     *
     * Example usage:
     * <code>
     * $query->filterByPackageID(1234); // WHERE packageId = 1234
     * $query->filterByPackageID(array(12, 34)); // WHERE packageId IN (12, 34)
     * $query->filterByPackageID(array('min' => 12)); // WHERE packageId > 12
     * </code>
     *
     * @param     mixed $packageID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknPackageQuery The current query, for fluid interface
     */
    public function filterByPackageID($packageID = null, $comparison = null)
    {
        if (is_array($packageID)) {
            $useMinMax = false;
            if (isset($packageID['min'])) {
                $this->addUsingAlias(RknPackageTableMap::COL_PACKAGEID, $packageID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($packageID['max'])) {
                $this->addUsingAlias(RknPackageTableMap::COL_PACKAGEID, $packageID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RknPackageTableMap::COL_PACKAGEID, $packageID, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknPackageQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RknPackageTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRknPackageQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RknPackageTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related \RknLicence object
     *
     * @param \RknLicence|ObjectCollection $rknLicence the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRknPackageQuery The current query, for fluid interface
     */
    public function filterByRknLicence($rknLicence, $comparison = null)
    {
        if ($rknLicence instanceof \RknLicence) {
            return $this
                ->addUsingAlias(RknPackageTableMap::COL_ID, $rknLicence->getPackageID(), $comparison);
        } elseif ($rknLicence instanceof ObjectCollection) {
            return $this
                ->useRknLicenceQuery()
                ->filterByPrimaryKeys($rknLicence->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRknLicence() only accepts arguments of type \RknLicence or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RknLicence relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRknPackageQuery The current query, for fluid interface
     */
    public function joinRknLicence($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RknLicence');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RknLicence');
        }

        return $this;
    }

    /**
     * Use the RknLicence relation RknLicence object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RknLicenceQuery A secondary query class using the current class as primary query
     */
    public function useRknLicenceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRknLicence($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RknLicence', '\RknLicenceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRknPackage $rknPackage Object to remove from the list of results
     *
     * @return $this|ChildRknPackageQuery The current query, for fluid interface
     */
    public function prune($rknPackage = null)
    {
        if ($rknPackage) {
            $this->addUsingAlias(RknPackageTableMap::COL_ID, $rknPackage->getID(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the rkn_package table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RknPackageTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RknPackageTableMap::clearInstancePool();
            RknPackageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RknPackageTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RknPackageTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RknPackageTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RknPackageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RknPackageQuery
