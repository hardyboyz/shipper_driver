<?php

namespace Base;

use \ChatroomSetting as ChildChatroomSetting;
use \ChatroomSettingQuery as ChildChatroomSettingQuery;
use \Exception;
use Map\ChatroomSettingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'chatroom_setting' table.
 *
 *
 *
 * @method     ChildChatroomSettingQuery orderByChatroomId($order = Criteria::ASC) Order by the chatroom_id column
 * @method     ChildChatroomSettingQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildChatroomSettingQuery orderByChatroomExpirydate($order = Criteria::ASC) Order by the chatroom_expirydate column
 * @method     ChildChatroomSettingQuery orderByDurationEnable($order = Criteria::ASC) Order by the duration_enable column
 * @method     ChildChatroomSettingQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildChatroomSettingQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildChatroomSettingQuery orderByModifiedBy($order = Criteria::ASC) Order by the modifed_by column
 * @method     ChildChatroomSettingQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildChatroomSettingQuery groupByChatroomId() Group by the chatroom_id column
 * @method     ChildChatroomSettingQuery groupByProjectId() Group by the project_id column
 * @method     ChildChatroomSettingQuery groupByChatroomExpirydate() Group by the chatroom_expirydate column
 * @method     ChildChatroomSettingQuery groupByDurationEnable() Group by the duration_enable column
 * @method     ChildChatroomSettingQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildChatroomSettingQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildChatroomSettingQuery groupByModifiedBy() Group by the modifed_by column
 * @method     ChildChatroomSettingQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildChatroomSettingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildChatroomSettingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildChatroomSettingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildChatroomSettingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildChatroomSettingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildChatroomSettingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildChatroomSettingQuery leftJoinChatRoom($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChatRoom relation
 * @method     ChildChatroomSettingQuery rightJoinChatRoom($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChatRoom relation
 * @method     ChildChatroomSettingQuery innerJoinChatRoom($relationAlias = null) Adds a INNER JOIN clause to the query using the ChatRoom relation
 *
 * @method     ChildChatroomSettingQuery joinWithChatRoom($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChatRoom relation
 *
 * @method     ChildChatroomSettingQuery leftJoinWithChatRoom() Adds a LEFT JOIN clause and with to the query using the ChatRoom relation
 * @method     ChildChatroomSettingQuery rightJoinWithChatRoom() Adds a RIGHT JOIN clause and with to the query using the ChatRoom relation
 * @method     ChildChatroomSettingQuery innerJoinWithChatRoom() Adds a INNER JOIN clause and with to the query using the ChatRoom relation
 *
 * @method     \ChatRoomQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildChatroomSetting findOne(ConnectionInterface $con = null) Return the first ChildChatroomSetting matching the query
 * @method     ChildChatroomSetting findOneOrCreate(ConnectionInterface $con = null) Return the first ChildChatroomSetting matching the query, or a new ChildChatroomSetting object populated from the query conditions when no match is found
 *
 * @method     ChildChatroomSetting findOneByChatroomId(int $chatroom_id) Return the first ChildChatroomSetting filtered by the chatroom_id column
 * @method     ChildChatroomSetting findOneByProjectId(string $project_id) Return the first ChildChatroomSetting filtered by the project_id column
 * @method     ChildChatroomSetting findOneByChatroomExpirydate(string $chatroom_expirydate) Return the first ChildChatroomSetting filtered by the chatroom_expirydate column
 * @method     ChildChatroomSetting findOneByDurationEnable(int $duration_enable) Return the first ChildChatroomSetting filtered by the duration_enable column
 * @method     ChildChatroomSetting findOneByCreatedBy(string $created_by) Return the first ChildChatroomSetting filtered by the created_by column
 * @method     ChildChatroomSetting findOneByCreatedDate(string $created_date) Return the first ChildChatroomSetting filtered by the created_date column
 * @method     ChildChatroomSetting findOneByModifiedBy(string $modifed_by) Return the first ChildChatroomSetting filtered by the modifed_by column
 * @method     ChildChatroomSetting findOneByModifiedDate(string $modified_date) Return the first ChildChatroomSetting filtered by the modified_date column *

 * @method     ChildChatroomSetting requirePk($key, ConnectionInterface $con = null) Return the ChildChatroomSetting by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatroomSetting requireOne(ConnectionInterface $con = null) Return the first ChildChatroomSetting matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatroomSetting requireOneByChatroomId(int $chatroom_id) Return the first ChildChatroomSetting filtered by the chatroom_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatroomSetting requireOneByProjectId(string $project_id) Return the first ChildChatroomSetting filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatroomSetting requireOneByChatroomExpirydate(string $chatroom_expirydate) Return the first ChildChatroomSetting filtered by the chatroom_expirydate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatroomSetting requireOneByDurationEnable(int $duration_enable) Return the first ChildChatroomSetting filtered by the duration_enable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatroomSetting requireOneByCreatedBy(string $created_by) Return the first ChildChatroomSetting filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatroomSetting requireOneByCreatedDate(string $created_date) Return the first ChildChatroomSetting filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatroomSetting requireOneByModifiedBy(string $modifed_by) Return the first ChildChatroomSetting filtered by the modifed_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatroomSetting requireOneByModifiedDate(string $modified_date) Return the first ChildChatroomSetting filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatroomSetting[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildChatroomSetting objects based on current ModelCriteria
 * @method     ChildChatroomSetting[]|ObjectCollection findByChatroomId(int $chatroom_id) Return ChildChatroomSetting objects filtered by the chatroom_id column
 * @method     ChildChatroomSetting[]|ObjectCollection findByProjectId(string $project_id) Return ChildChatroomSetting objects filtered by the project_id column
 * @method     ChildChatroomSetting[]|ObjectCollection findByChatroomExpirydate(string $chatroom_expirydate) Return ChildChatroomSetting objects filtered by the chatroom_expirydate column
 * @method     ChildChatroomSetting[]|ObjectCollection findByDurationEnable(int $duration_enable) Return ChildChatroomSetting objects filtered by the duration_enable column
 * @method     ChildChatroomSetting[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildChatroomSetting objects filtered by the created_by column
 * @method     ChildChatroomSetting[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildChatroomSetting objects filtered by the created_date column
 * @method     ChildChatroomSetting[]|ObjectCollection findByModifiedBy(string $modifed_by) Return ChildChatroomSetting objects filtered by the modifed_by column
 * @method     ChildChatroomSetting[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildChatroomSetting objects filtered by the modified_date column
 * @method     ChildChatroomSetting[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ChatroomSettingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ChatroomSettingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ChatroomSetting', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildChatroomSettingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildChatroomSettingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildChatroomSettingQuery) {
            return $criteria;
        }
        $query = new ChildChatroomSettingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildChatroomSetting|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        throw new LogicException('The ChatroomSetting object has no primary key');
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        throw new LogicException('The ChatroomSetting object has no primary key');
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        throw new LogicException('The ChatroomSetting object has no primary key');
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        throw new LogicException('The ChatroomSetting object has no primary key');
    }

    /**
     * Filter the query on the chatroom_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatroomId(1234); // WHERE chatroom_id = 1234
     * $query->filterByChatroomId(array(12, 34)); // WHERE chatroom_id IN (12, 34)
     * $query->filterByChatroomId(array('min' => 12)); // WHERE chatroom_id > 12
     * </code>
     *
     * @see       filterByChatRoom()
     *
     * @param     mixed $chatroomId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByChatroomId($chatroomId = null, $comparison = null)
    {
        if (is_array($chatroomId)) {
            $useMinMax = false;
            if (isset($chatroomId['min'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_CHATROOM_ID, $chatroomId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatroomId['max'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_CHATROOM_ID, $chatroomId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatroomSettingTableMap::COL_CHATROOM_ID, $chatroomId, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatroomSettingTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the chatroom_expirydate column
     *
     * Example usage:
     * <code>
     * $query->filterByChatroomExpirydate('2011-03-14'); // WHERE chatroom_expirydate = '2011-03-14'
     * $query->filterByChatroomExpirydate('now'); // WHERE chatroom_expirydate = '2011-03-14'
     * $query->filterByChatroomExpirydate(array('max' => 'yesterday')); // WHERE chatroom_expirydate > '2011-03-13'
     * </code>
     *
     * @param     mixed $chatroomExpirydate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByChatroomExpirydate($chatroomExpirydate = null, $comparison = null)
    {
        if (is_array($chatroomExpirydate)) {
            $useMinMax = false;
            if (isset($chatroomExpirydate['min'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_CHATROOM_EXPIRYDATE, $chatroomExpirydate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($chatroomExpirydate['max'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_CHATROOM_EXPIRYDATE, $chatroomExpirydate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatroomSettingTableMap::COL_CHATROOM_EXPIRYDATE, $chatroomExpirydate, $comparison);
    }

    /**
     * Filter the query on the duration_enable column
     *
     * Example usage:
     * <code>
     * $query->filterByDurationEnable(1234); // WHERE duration_enable = 1234
     * $query->filterByDurationEnable(array(12, 34)); // WHERE duration_enable IN (12, 34)
     * $query->filterByDurationEnable(array('min' => 12)); // WHERE duration_enable > 12
     * </code>
     *
     * @param     mixed $durationEnable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByDurationEnable($durationEnable = null, $comparison = null)
    {
        if (is_array($durationEnable)) {
            $useMinMax = false;
            if (isset($durationEnable['min'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_DURATION_ENABLE, $durationEnable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($durationEnable['max'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_DURATION_ENABLE, $durationEnable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatroomSettingTableMap::COL_DURATION_ENABLE, $durationEnable, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatroomSettingTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatroomSettingTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modifed_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modifed_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modifed_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatroomSettingTableMap::COL_MODIFED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(ChatroomSettingTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatroomSettingTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query by a related \ChatRoom object
     *
     * @param \ChatRoom|ObjectCollection $chatRoom The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function filterByChatRoom($chatRoom, $comparison = null)
    {
        if ($chatRoom instanceof \ChatRoom) {
            return $this
                ->addUsingAlias(ChatroomSettingTableMap::COL_CHATROOM_ID, $chatRoom->getId(), $comparison);
        } elseif ($chatRoom instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ChatroomSettingTableMap::COL_CHATROOM_ID, $chatRoom->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChatRoom() only accepts arguments of type \ChatRoom or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChatRoom relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function joinChatRoom($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChatRoom');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChatRoom');
        }

        return $this;
    }

    /**
     * Use the ChatRoom relation ChatRoom object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ChatRoomQuery A secondary query class using the current class as primary query
     */
    public function useChatRoomQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinChatRoom($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChatRoom', '\ChatRoomQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildChatroomSetting $chatroomSetting Object to remove from the list of results
     *
     * @return $this|ChildChatroomSettingQuery The current query, for fluid interface
     */
    public function prune($chatroomSetting = null)
    {
        if ($chatroomSetting) {
            throw new LogicException('ChatroomSetting object has no primary key');

        }

        return $this;
    }

    /**
     * Deletes all rows from the chatroom_setting table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatroomSettingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChatroomSettingTableMap::clearInstancePool();
            ChatroomSettingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatroomSettingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ChatroomSettingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ChatroomSettingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ChatroomSettingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ChatroomSettingQuery
