<?php

namespace Base;

use \ChatUser as ChildChatUser;
use \ChatUserQuery as ChildChatUserQuery;
use \Exception;
use \PDO;
use Map\ChatUserTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'chat_user' table.
 *
 *
 *
 * @method     ChildChatUserQuery orderByInvitedBy($order = Criteria::ASC) Order by the invited_by column
 * @method     ChildChatUserQuery orderByInvitedDate($order = Criteria::ASC) Order by the invited_date column
 * @method     ChildChatUserQuery orderByModifiedBy($order = Criteria::ASC) Order by the modifed_by column
 * @method     ChildChatUserQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 * @method     ChildChatUserQuery orderByChatroomId($order = Criteria::ASC) Order by the chatroom_id column
 * @method     ChildChatUserQuery orderByUnitId($order = Criteria::ASC) Order by the unit_id column
 * @method     ChildChatUserQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildChatUserQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildChatUserQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildChatUserQuery groupByInvitedBy() Group by the invited_by column
 * @method     ChildChatUserQuery groupByInvitedDate() Group by the invited_date column
 * @method     ChildChatUserQuery groupByModifiedBy() Group by the modifed_by column
 * @method     ChildChatUserQuery groupByModifiedDate() Group by the modified_date column
 * @method     ChildChatUserQuery groupByChatroomId() Group by the chatroom_id column
 * @method     ChildChatUserQuery groupByUnitId() Group by the unit_id column
 * @method     ChildChatUserQuery groupByProjectId() Group by the project_id column
 * @method     ChildChatUserQuery groupByUserId() Group by the user_id column
 * @method     ChildChatUserQuery groupByStatus() Group by the status column
 *
 * @method     ChildChatUserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildChatUserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildChatUserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildChatUserQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildChatUserQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildChatUserQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildChatUser findOne(ConnectionInterface $con = null) Return the first ChildChatUser matching the query
 * @method     ChildChatUser findOneOrCreate(ConnectionInterface $con = null) Return the first ChildChatUser matching the query, or a new ChildChatUser object populated from the query conditions when no match is found
 *
 * @method     ChildChatUser findOneByInvitedBy(string $invited_by) Return the first ChildChatUser filtered by the invited_by column
 * @method     ChildChatUser findOneByInvitedDate(string $invited_date) Return the first ChildChatUser filtered by the invited_date column
 * @method     ChildChatUser findOneByModifiedBy(string $modifed_by) Return the first ChildChatUser filtered by the modifed_by column
 * @method     ChildChatUser findOneByModifiedDate(string $modified_date) Return the first ChildChatUser filtered by the modified_date column
 * @method     ChildChatUser findOneByChatroomId(string $chatroom_id) Return the first ChildChatUser filtered by the chatroom_id column
 * @method     ChildChatUser findOneByUnitId(string $unit_id) Return the first ChildChatUser filtered by the unit_id column
 * @method     ChildChatUser findOneByProjectId(string $project_id) Return the first ChildChatUser filtered by the project_id column
 * @method     ChildChatUser findOneByUserId(string $user_id) Return the first ChildChatUser filtered by the user_id column
 * @method     ChildChatUser findOneByStatus(int $status) Return the first ChildChatUser filtered by the status column *

 * @method     ChildChatUser requirePk($key, ConnectionInterface $con = null) Return the ChildChatUser by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOne(ConnectionInterface $con = null) Return the first ChildChatUser matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatUser requireOneByInvitedBy(string $invited_by) Return the first ChildChatUser filtered by the invited_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByInvitedDate(string $invited_date) Return the first ChildChatUser filtered by the invited_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByModifiedBy(string $modifed_by) Return the first ChildChatUser filtered by the modifed_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByModifiedDate(string $modified_date) Return the first ChildChatUser filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByChatroomId(string $chatroom_id) Return the first ChildChatUser filtered by the chatroom_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByUnitId(string $unit_id) Return the first ChildChatUser filtered by the unit_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByProjectId(string $project_id) Return the first ChildChatUser filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByUserId(string $user_id) Return the first ChildChatUser filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildChatUser requireOneByStatus(int $status) Return the first ChildChatUser filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildChatUser[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildChatUser objects based on current ModelCriteria
 * @method     ChildChatUser[]|ObjectCollection findByInvitedBy(string $invited_by) Return ChildChatUser objects filtered by the invited_by column
 * @method     ChildChatUser[]|ObjectCollection findByInvitedDate(string $invited_date) Return ChildChatUser objects filtered by the invited_date column
 * @method     ChildChatUser[]|ObjectCollection findByModifiedBy(string $modifed_by) Return ChildChatUser objects filtered by the modifed_by column
 * @method     ChildChatUser[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildChatUser objects filtered by the modified_date column
 * @method     ChildChatUser[]|ObjectCollection findByChatroomId(string $chatroom_id) Return ChildChatUser objects filtered by the chatroom_id column
 * @method     ChildChatUser[]|ObjectCollection findByUnitId(string $unit_id) Return ChildChatUser objects filtered by the unit_id column
 * @method     ChildChatUser[]|ObjectCollection findByProjectId(string $project_id) Return ChildChatUser objects filtered by the project_id column
 * @method     ChildChatUser[]|ObjectCollection findByUserId(string $user_id) Return ChildChatUser objects filtered by the user_id column
 * @method     ChildChatUser[]|ObjectCollection findByStatus(int $status) Return ChildChatUser objects filtered by the status column
 * @method     ChildChatUser[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ChatUserQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ChatUserQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ChatUser', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildChatUserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildChatUserQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildChatUserQuery) {
            return $criteria;
        }
        $query = new ChildChatUserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array[$chatroom_id, $unit_id, $project_id] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildChatUser|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ChatUserTableMap::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1], (string) $key[2]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ChatUserTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildChatUser A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT invited_by, invited_date, modifed_by, modified_date, chatroom_id, unit_id, project_id, user_id, status FROM chat_user WHERE chatroom_id = :p0 AND unit_id = :p1 AND project_id = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildChatUser $obj */
            $obj = new ChildChatUser();
            $obj->hydrate($row);
            ChatUserTableMap::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1], (string) $key[2])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildChatUser|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ChatUserTableMap::COL_CHATROOM_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ChatUserTableMap::COL_UNIT_ID, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(ChatUserTableMap::COL_PROJECT_ID, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ChatUserTableMap::COL_CHATROOM_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ChatUserTableMap::COL_UNIT_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(ChatUserTableMap::COL_PROJECT_ID, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the invited_by column
     *
     * Example usage:
     * <code>
     * $query->filterByInvitedBy('fooValue');   // WHERE invited_by = 'fooValue'
     * $query->filterByInvitedBy('%fooValue%'); // WHERE invited_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invitedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByInvitedBy($invitedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invitedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $invitedBy)) {
                $invitedBy = str_replace('*', '%', $invitedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_INVITED_BY, $invitedBy, $comparison);
    }

    /**
     * Filter the query on the invited_date column
     *
     * Example usage:
     * <code>
     * $query->filterByInvitedDate('2011-03-14'); // WHERE invited_date = '2011-03-14'
     * $query->filterByInvitedDate('now'); // WHERE invited_date = '2011-03-14'
     * $query->filterByInvitedDate(array('max' => 'yesterday')); // WHERE invited_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $invitedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByInvitedDate($invitedDate = null, $comparison = null)
    {
        if (is_array($invitedDate)) {
            $useMinMax = false;
            if (isset($invitedDate['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_INVITED_DATE, $invitedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($invitedDate['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_INVITED_DATE, $invitedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_INVITED_DATE, $invitedDate, $comparison);
    }

    /**
     * Filter the query on the modifed_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modifed_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modifed_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_MODIFED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query on the chatroom_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatroomId('fooValue');   // WHERE chatroom_id = 'fooValue'
     * $query->filterByChatroomId('%fooValue%'); // WHERE chatroom_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chatroomId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByChatroomId($chatroomId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chatroomId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $chatroomId)) {
                $chatroomId = str_replace('*', '%', $chatroomId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_CHATROOM_ID, $chatroomId, $comparison);
    }

    /**
     * Filter the query on the unit_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId('fooValue');   // WHERE unit_id = 'fooValue'
     * $query->filterByUnitId('%fooValue%'); // WHERE unit_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitId)) {
                $unitId = str_replace('*', '%', $unitId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_UNIT_ID, $unitId, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId('fooValue');   // WHERE user_id = 'fooValue'
     * $query->filterByUserId('%fooValue%'); // WHERE user_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $userId)) {
                $userId = str_replace('*', '%', $userId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(ChatUserTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ChatUserTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildChatUser $chatUser Object to remove from the list of results
     *
     * @return $this|ChildChatUserQuery The current query, for fluid interface
     */
    public function prune($chatUser = null)
    {
        if ($chatUser) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ChatUserTableMap::COL_CHATROOM_ID), $chatUser->getChatroomId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ChatUserTableMap::COL_UNIT_ID), $chatUser->getUnitId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(ChatUserTableMap::COL_PROJECT_ID), $chatUser->getProjectId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the chat_user table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ChatUserTableMap::clearInstancePool();
            ChatUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ChatUserTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ChatUserTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ChatUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ChatUserQuery
