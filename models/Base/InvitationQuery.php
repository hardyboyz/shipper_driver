<?php

namespace Base;

use \Invitation as ChildInvitation;
use \InvitationQuery as ChildInvitationQuery;
use \Exception;
use \PDO;
use Map\InvitationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'invitation' table.
 *
 *
 *
 * @method     ChildInvitationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildInvitationQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildInvitationQuery orderByChatroomId($order = Criteria::ASC) Order by the chatroom_id column
 * @method     ChildInvitationQuery orderByUnitId($order = Criteria::ASC) Order by the unit_id column
 * @method     ChildInvitationQuery orderByGroupType($order = Criteria::ASC) Order by the group_type column
 * @method     ChildInvitationQuery orderByGroupTypeValue($order = Criteria::ASC) Order by the group_type_value column
 * @method     ChildInvitationQuery orderByGroupTypeDesc($order = Criteria::ASC) Order by the group_type_desc column
 * @method     ChildInvitationQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildInvitationQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildInvitationQuery orderByModifiedBy($order = Criteria::ASC) Order by the modifed_by column
 * @method     ChildInvitationQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 * @method     ChildInvitationQuery orderByStatus($order = Criteria::ASC) Order by the status column
 *
 * @method     ChildInvitationQuery groupById() Group by the id column
 * @method     ChildInvitationQuery groupByProjectId() Group by the project_id column
 * @method     ChildInvitationQuery groupByChatroomId() Group by the chatroom_id column
 * @method     ChildInvitationQuery groupByUnitId() Group by the unit_id column
 * @method     ChildInvitationQuery groupByGroupType() Group by the group_type column
 * @method     ChildInvitationQuery groupByGroupTypeValue() Group by the group_type_value column
 * @method     ChildInvitationQuery groupByGroupTypeDesc() Group by the group_type_desc column
 * @method     ChildInvitationQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildInvitationQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildInvitationQuery groupByModifiedBy() Group by the modifed_by column
 * @method     ChildInvitationQuery groupByModifiedDate() Group by the modified_date column
 * @method     ChildInvitationQuery groupByStatus() Group by the status column
 *
 * @method     ChildInvitationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildInvitationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildInvitationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildInvitationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildInvitationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildInvitationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildInvitation findOne(ConnectionInterface $con = null) Return the first ChildInvitation matching the query
 * @method     ChildInvitation findOneOrCreate(ConnectionInterface $con = null) Return the first ChildInvitation matching the query, or a new ChildInvitation object populated from the query conditions when no match is found
 *
 * @method     ChildInvitation findOneById(int $id) Return the first ChildInvitation filtered by the id column
 * @method     ChildInvitation findOneByProjectId(string $project_id) Return the first ChildInvitation filtered by the project_id column
 * @method     ChildInvitation findOneByChatroomId(string $chatroom_id) Return the first ChildInvitation filtered by the chatroom_id column
 * @method     ChildInvitation findOneByUnitId(string $unit_id) Return the first ChildInvitation filtered by the unit_id column
 * @method     ChildInvitation findOneByGroupType(string $group_type) Return the first ChildInvitation filtered by the group_type column
 * @method     ChildInvitation findOneByGroupTypeValue(string $group_type_value) Return the first ChildInvitation filtered by the group_type_value column
 * @method     ChildInvitation findOneByGroupTypeDesc(string $group_type_desc) Return the first ChildInvitation filtered by the group_type_desc column
 * @method     ChildInvitation findOneByCreatedBy(string $created_by) Return the first ChildInvitation filtered by the created_by column
 * @method     ChildInvitation findOneByCreatedDate(string $created_date) Return the first ChildInvitation filtered by the created_date column
 * @method     ChildInvitation findOneByModifiedBy(string $modifed_by) Return the first ChildInvitation filtered by the modifed_by column
 * @method     ChildInvitation findOneByModifiedDate(string $modified_date) Return the first ChildInvitation filtered by the modified_date column
 * @method     ChildInvitation findOneByStatus(int $status) Return the first ChildInvitation filtered by the status column *

 * @method     ChildInvitation requirePk($key, ConnectionInterface $con = null) Return the ChildInvitation by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOne(ConnectionInterface $con = null) Return the first ChildInvitation matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildInvitation requireOneById(int $id) Return the first ChildInvitation filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByProjectId(string $project_id) Return the first ChildInvitation filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByChatroomId(string $chatroom_id) Return the first ChildInvitation filtered by the chatroom_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByUnitId(string $unit_id) Return the first ChildInvitation filtered by the unit_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByGroupType(string $group_type) Return the first ChildInvitation filtered by the group_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByGroupTypeValue(string $group_type_value) Return the first ChildInvitation filtered by the group_type_value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByGroupTypeDesc(string $group_type_desc) Return the first ChildInvitation filtered by the group_type_desc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByCreatedBy(string $created_by) Return the first ChildInvitation filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByCreatedDate(string $created_date) Return the first ChildInvitation filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByModifiedBy(string $modifed_by) Return the first ChildInvitation filtered by the modifed_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByModifiedDate(string $modified_date) Return the first ChildInvitation filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildInvitation requireOneByStatus(int $status) Return the first ChildInvitation filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildInvitation[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildInvitation objects based on current ModelCriteria
 * @method     ChildInvitation[]|ObjectCollection findById(int $id) Return ChildInvitation objects filtered by the id column
 * @method     ChildInvitation[]|ObjectCollection findByProjectId(string $project_id) Return ChildInvitation objects filtered by the project_id column
 * @method     ChildInvitation[]|ObjectCollection findByChatroomId(string $chatroom_id) Return ChildInvitation objects filtered by the chatroom_id column
 * @method     ChildInvitation[]|ObjectCollection findByUnitId(string $unit_id) Return ChildInvitation objects filtered by the unit_id column
 * @method     ChildInvitation[]|ObjectCollection findByGroupType(string $group_type) Return ChildInvitation objects filtered by the group_type column
 * @method     ChildInvitation[]|ObjectCollection findByGroupTypeValue(string $group_type_value) Return ChildInvitation objects filtered by the group_type_value column
 * @method     ChildInvitation[]|ObjectCollection findByGroupTypeDesc(string $group_type_desc) Return ChildInvitation objects filtered by the group_type_desc column
 * @method     ChildInvitation[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildInvitation objects filtered by the created_by column
 * @method     ChildInvitation[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildInvitation objects filtered by the created_date column
 * @method     ChildInvitation[]|ObjectCollection findByModifiedBy(string $modifed_by) Return ChildInvitation objects filtered by the modifed_by column
 * @method     ChildInvitation[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildInvitation objects filtered by the modified_date column
 * @method     ChildInvitation[]|ObjectCollection findByStatus(int $status) Return ChildInvitation objects filtered by the status column
 * @method     ChildInvitation[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class InvitationQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\InvitationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Invitation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildInvitationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildInvitationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildInvitationQuery) {
            return $criteria;
        }
        $query = new ChildInvitationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildInvitation|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = InvitationTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(InvitationTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildInvitation A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, project_id, chatroom_id, unit_id, group_type, group_type_value, group_type_desc, created_by, created_date, modifed_by, modified_date, status FROM invitation WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildInvitation $obj */
            $obj = new ChildInvitation();
            $obj->hydrate($row);
            InvitationTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildInvitation|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(InvitationTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(InvitationTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(InvitationTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(InvitationTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the chatroom_id column
     *
     * Example usage:
     * <code>
     * $query->filterByChatroomId('fooValue');   // WHERE chatroom_id = 'fooValue'
     * $query->filterByChatroomId('%fooValue%'); // WHERE chatroom_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chatroomId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByChatroomId($chatroomId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chatroomId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $chatroomId)) {
                $chatroomId = str_replace('*', '%', $chatroomId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_CHATROOM_ID, $chatroomId, $comparison);
    }

    /**
     * Filter the query on the unit_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId('fooValue');   // WHERE unit_id = 'fooValue'
     * $query->filterByUnitId('%fooValue%'); // WHERE unit_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitId)) {
                $unitId = str_replace('*', '%', $unitId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_UNIT_ID, $unitId, $comparison);
    }

    /**
     * Filter the query on the group_type column
     *
     * Example usage:
     * <code>
     * $query->filterByGroupType('fooValue');   // WHERE group_type = 'fooValue'
     * $query->filterByGroupType('%fooValue%'); // WHERE group_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $groupType The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByGroupType($groupType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($groupType)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $groupType)) {
                $groupType = str_replace('*', '%', $groupType);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_GROUP_TYPE, $groupType, $comparison);
    }

    /**
     * Filter the query on the group_type_value column
     *
     * Example usage:
     * <code>
     * $query->filterByGroupTypeValue('fooValue');   // WHERE group_type_value = 'fooValue'
     * $query->filterByGroupTypeValue('%fooValue%'); // WHERE group_type_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $groupTypeValue The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByGroupTypeValue($groupTypeValue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($groupTypeValue)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $groupTypeValue)) {
                $groupTypeValue = str_replace('*', '%', $groupTypeValue);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_GROUP_TYPE_VALUE, $groupTypeValue, $comparison);
    }

    /**
     * Filter the query on the group_type_desc column
     *
     * Example usage:
     * <code>
     * $query->filterByGroupTypeDesc('fooValue');   // WHERE group_type_desc = 'fooValue'
     * $query->filterByGroupTypeDesc('%fooValue%'); // WHERE group_type_desc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $groupTypeDesc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByGroupTypeDesc($groupTypeDesc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($groupTypeDesc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $groupTypeDesc)) {
                $groupTypeDesc = str_replace('*', '%', $groupTypeDesc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_GROUP_TYPE_DESC, $groupTypeDesc, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(InvitationTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(InvitationTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modifed_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modifed_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modifed_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_MODIFED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(InvitationTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(InvitationTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(InvitationTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(InvitationTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InvitationTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildInvitation $invitation Object to remove from the list of results
     *
     * @return $this|ChildInvitationQuery The current query, for fluid interface
     */
    public function prune($invitation = null)
    {
        if ($invitation) {
            $this->addUsingAlias(InvitationTableMap::COL_ID, $invitation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the invitation table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InvitationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            InvitationTableMap::clearInstancePool();
            InvitationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InvitationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(InvitationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            InvitationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            InvitationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // InvitationQuery
