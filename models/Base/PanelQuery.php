<?php

namespace Base;

use \Panel as ChildPanel;
use \PanelQuery as ChildPanelQuery;
use \Exception;
use \PDO;
use Map\PanelTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'panel' table.
 *
 *
 *
 * @method     ChildPanelQuery orderByPanelId($order = Criteria::ASC) Order by the panel_id column
 * @method     ChildPanelQuery orderByPanelTypeId($order = Criteria::ASC) Order by the panel_type_id column
 * @method     ChildPanelQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildPanelQuery orderByTenantId($order = Criteria::ASC) Order by the tenant_id column
 * @method     ChildPanelQuery orderByIpAddress($order = Criteria::ASC) Order by the ip_address column
 * @method     ChildPanelQuery orderByProtocol($order = Criteria::ASC) Order by the protocol column
 * @method     ChildPanelQuery orderByPort($order = Criteria::ASC) Order by the port column
 * @method     ChildPanelQuery orderBySetting($order = Criteria::ASC) Order by the setting column
 * @method     ChildPanelQuery orderBySettingUpdateTs($order = Criteria::ASC) Order by the setting_update_ts column
 * @method     ChildPanelQuery orderByChecksum($order = Criteria::ASC) Order by the checksum column
 *
 * @method     ChildPanelQuery groupByPanelId() Group by the panel_id column
 * @method     ChildPanelQuery groupByPanelTypeId() Group by the panel_type_id column
 * @method     ChildPanelQuery groupByProjectId() Group by the project_id column
 * @method     ChildPanelQuery groupByTenantId() Group by the tenant_id column
 * @method     ChildPanelQuery groupByIpAddress() Group by the ip_address column
 * @method     ChildPanelQuery groupByProtocol() Group by the protocol column
 * @method     ChildPanelQuery groupByPort() Group by the port column
 * @method     ChildPanelQuery groupBySetting() Group by the setting column
 * @method     ChildPanelQuery groupBySettingUpdateTs() Group by the setting_update_ts column
 * @method     ChildPanelQuery groupByChecksum() Group by the checksum column
 *
 * @method     ChildPanelQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPanelQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPanelQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPanelQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPanelQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPanelQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPanelQuery leftJoinPanelType($relationAlias = null) Adds a LEFT JOIN clause to the query using the PanelType relation
 * @method     ChildPanelQuery rightJoinPanelType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PanelType relation
 * @method     ChildPanelQuery innerJoinPanelType($relationAlias = null) Adds a INNER JOIN clause to the query using the PanelType relation
 *
 * @method     ChildPanelQuery joinWithPanelType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PanelType relation
 *
 * @method     ChildPanelQuery leftJoinWithPanelType() Adds a LEFT JOIN clause and with to the query using the PanelType relation
 * @method     ChildPanelQuery rightJoinWithPanelType() Adds a RIGHT JOIN clause and with to the query using the PanelType relation
 * @method     ChildPanelQuery innerJoinWithPanelType() Adds a INNER JOIN clause and with to the query using the PanelType relation
 *
 * @method     \PanelTypeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPanel findOne(ConnectionInterface $con = null) Return the first ChildPanel matching the query
 * @method     ChildPanel findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPanel matching the query, or a new ChildPanel object populated from the query conditions when no match is found
 *
 * @method     ChildPanel findOneByPanelId(int $panel_id) Return the first ChildPanel filtered by the panel_id column
 * @method     ChildPanel findOneByPanelTypeId(int $panel_type_id) Return the first ChildPanel filtered by the panel_type_id column
 * @method     ChildPanel findOneByProjectId(string $project_id) Return the first ChildPanel filtered by the project_id column
 * @method     ChildPanel findOneByTenantId(string $tenant_id) Return the first ChildPanel filtered by the tenant_id column
 * @method     ChildPanel findOneByIpAddress(string $ip_address) Return the first ChildPanel filtered by the ip_address column
 * @method     ChildPanel findOneByProtocol(string $protocol) Return the first ChildPanel filtered by the protocol column
 * @method     ChildPanel findOneByPort(string $port) Return the first ChildPanel filtered by the port column
 * @method     ChildPanel findOneBySetting(string $setting) Return the first ChildPanel filtered by the setting column
 * @method     ChildPanel findOneBySettingUpdateTs(string $setting_update_ts) Return the first ChildPanel filtered by the setting_update_ts column
 * @method     ChildPanel findOneByChecksum(int $checksum) Return the first ChildPanel filtered by the checksum column *

 * @method     ChildPanel requirePk($key, ConnectionInterface $con = null) Return the ChildPanel by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOne(ConnectionInterface $con = null) Return the first ChildPanel matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPanel requireOneByPanelId(int $panel_id) Return the first ChildPanel filtered by the panel_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOneByPanelTypeId(int $panel_type_id) Return the first ChildPanel filtered by the panel_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOneByProjectId(string $project_id) Return the first ChildPanel filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOneByTenantId(string $tenant_id) Return the first ChildPanel filtered by the tenant_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOneByIpAddress(string $ip_address) Return the first ChildPanel filtered by the ip_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOneByProtocol(string $protocol) Return the first ChildPanel filtered by the protocol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOneByPort(string $port) Return the first ChildPanel filtered by the port column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOneBySetting(string $setting) Return the first ChildPanel filtered by the setting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOneBySettingUpdateTs(string $setting_update_ts) Return the first ChildPanel filtered by the setting_update_ts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPanel requireOneByChecksum(int $checksum) Return the first ChildPanel filtered by the checksum column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPanel[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPanel objects based on current ModelCriteria
 * @method     ChildPanel[]|ObjectCollection findByPanelId(int $panel_id) Return ChildPanel objects filtered by the panel_id column
 * @method     ChildPanel[]|ObjectCollection findByPanelTypeId(int $panel_type_id) Return ChildPanel objects filtered by the panel_type_id column
 * @method     ChildPanel[]|ObjectCollection findByProjectId(string $project_id) Return ChildPanel objects filtered by the project_id column
 * @method     ChildPanel[]|ObjectCollection findByTenantId(string $tenant_id) Return ChildPanel objects filtered by the tenant_id column
 * @method     ChildPanel[]|ObjectCollection findByIpAddress(string $ip_address) Return ChildPanel objects filtered by the ip_address column
 * @method     ChildPanel[]|ObjectCollection findByProtocol(string $protocol) Return ChildPanel objects filtered by the protocol column
 * @method     ChildPanel[]|ObjectCollection findByPort(string $port) Return ChildPanel objects filtered by the port column
 * @method     ChildPanel[]|ObjectCollection findBySetting(string $setting) Return ChildPanel objects filtered by the setting column
 * @method     ChildPanel[]|ObjectCollection findBySettingUpdateTs(string $setting_update_ts) Return ChildPanel objects filtered by the setting_update_ts column
 * @method     ChildPanel[]|ObjectCollection findByChecksum(int $checksum) Return ChildPanel objects filtered by the checksum column
 * @method     ChildPanel[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PanelQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PanelQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Panel', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPanelQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPanelQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPanelQuery) {
            return $criteria;
        }
        $query = new ChildPanelQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$panel_id, $project_id] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPanel|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PanelTableMap::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PanelTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPanel A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT panel_id, panel_type_id, project_id, tenant_id, ip_address, protocol, port, setting, setting_update_ts, checksum FROM panel WHERE panel_id = :p0 AND project_id = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPanel $obj */
            $obj = new ChildPanel();
            $obj->hydrate($row);
            PanelTableMap::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPanel|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(PanelTableMap::COL_PANEL_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(PanelTableMap::COL_PROJECT_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(PanelTableMap::COL_PANEL_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(PanelTableMap::COL_PROJECT_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the panel_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPanelId(1234); // WHERE panel_id = 1234
     * $query->filterByPanelId(array(12, 34)); // WHERE panel_id IN (12, 34)
     * $query->filterByPanelId(array('min' => 12)); // WHERE panel_id > 12
     * </code>
     *
     * @param     mixed $panelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByPanelId($panelId = null, $comparison = null)
    {
        if (is_array($panelId)) {
            $useMinMax = false;
            if (isset($panelId['min'])) {
                $this->addUsingAlias(PanelTableMap::COL_PANEL_ID, $panelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panelId['max'])) {
                $this->addUsingAlias(PanelTableMap::COL_PANEL_ID, $panelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_PANEL_ID, $panelId, $comparison);
    }

    /**
     * Filter the query on the panel_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPanelTypeId(1234); // WHERE panel_type_id = 1234
     * $query->filterByPanelTypeId(array(12, 34)); // WHERE panel_type_id IN (12, 34)
     * $query->filterByPanelTypeId(array('min' => 12)); // WHERE panel_type_id > 12
     * </code>
     *
     * @see       filterByPanelType()
     *
     * @param     mixed $panelTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByPanelTypeId($panelTypeId = null, $comparison = null)
    {
        if (is_array($panelTypeId)) {
            $useMinMax = false;
            if (isset($panelTypeId['min'])) {
                $this->addUsingAlias(PanelTableMap::COL_PANEL_TYPE_ID, $panelTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panelTypeId['max'])) {
                $this->addUsingAlias(PanelTableMap::COL_PANEL_TYPE_ID, $panelTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_PANEL_TYPE_ID, $panelTypeId, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the tenant_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTenantId('fooValue');   // WHERE tenant_id = 'fooValue'
     * $query->filterByTenantId('%fooValue%'); // WHERE tenant_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tenantId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByTenantId($tenantId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tenantId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tenantId)) {
                $tenantId = str_replace('*', '%', $tenantId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_TENANT_ID, $tenantId, $comparison);
    }

    /**
     * Filter the query on the ip_address column
     *
     * Example usage:
     * <code>
     * $query->filterByIpAddress('fooValue');   // WHERE ip_address = 'fooValue'
     * $query->filterByIpAddress('%fooValue%'); // WHERE ip_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipAddress The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByIpAddress($ipAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipAddress)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ipAddress)) {
                $ipAddress = str_replace('*', '%', $ipAddress);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_IP_ADDRESS, $ipAddress, $comparison);
    }

    /**
     * Filter the query on the protocol column
     *
     * Example usage:
     * <code>
     * $query->filterByProtocol('fooValue');   // WHERE protocol = 'fooValue'
     * $query->filterByProtocol('%fooValue%'); // WHERE protocol LIKE '%fooValue%'
     * </code>
     *
     * @param     string $protocol The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByProtocol($protocol = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($protocol)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $protocol)) {
                $protocol = str_replace('*', '%', $protocol);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_PROTOCOL, $protocol, $comparison);
    }

    /**
     * Filter the query on the port column
     *
     * Example usage:
     * <code>
     * $query->filterByPort('fooValue');   // WHERE port = 'fooValue'
     * $query->filterByPort('%fooValue%'); // WHERE port LIKE '%fooValue%'
     * </code>
     *
     * @param     string $port The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByPort($port = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($port)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $port)) {
                $port = str_replace('*', '%', $port);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_PORT, $port, $comparison);
    }

    /**
     * Filter the query on the setting column
     *
     * Example usage:
     * <code>
     * $query->filterBySetting('fooValue');   // WHERE setting = 'fooValue'
     * $query->filterBySetting('%fooValue%'); // WHERE setting LIKE '%fooValue%'
     * </code>
     *
     * @param     string $setting The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterBySetting($setting = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($setting)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $setting)) {
                $setting = str_replace('*', '%', $setting);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_SETTING, $setting, $comparison);
    }

    /**
     * Filter the query on the setting_update_ts column
     *
     * Example usage:
     * <code>
     * $query->filterBySettingUpdateTs('2011-03-14'); // WHERE setting_update_ts = '2011-03-14'
     * $query->filterBySettingUpdateTs('now'); // WHERE setting_update_ts = '2011-03-14'
     * $query->filterBySettingUpdateTs(array('max' => 'yesterday')); // WHERE setting_update_ts > '2011-03-13'
     * </code>
     *
     * @param     mixed $settingUpdateTs The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterBySettingUpdateTs($settingUpdateTs = null, $comparison = null)
    {
        if (is_array($settingUpdateTs)) {
            $useMinMax = false;
            if (isset($settingUpdateTs['min'])) {
                $this->addUsingAlias(PanelTableMap::COL_SETTING_UPDATE_TS, $settingUpdateTs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($settingUpdateTs['max'])) {
                $this->addUsingAlias(PanelTableMap::COL_SETTING_UPDATE_TS, $settingUpdateTs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_SETTING_UPDATE_TS, $settingUpdateTs, $comparison);
    }

    /**
     * Filter the query on the checksum column
     *
     * Example usage:
     * <code>
     * $query->filterByChecksum(1234); // WHERE checksum = 1234
     * $query->filterByChecksum(array(12, 34)); // WHERE checksum IN (12, 34)
     * $query->filterByChecksum(array('min' => 12)); // WHERE checksum > 12
     * </code>
     *
     * @param     mixed $checksum The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function filterByChecksum($checksum = null, $comparison = null)
    {
        if (is_array($checksum)) {
            $useMinMax = false;
            if (isset($checksum['min'])) {
                $this->addUsingAlias(PanelTableMap::COL_CHECKSUM, $checksum['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($checksum['max'])) {
                $this->addUsingAlias(PanelTableMap::COL_CHECKSUM, $checksum['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PanelTableMap::COL_CHECKSUM, $checksum, $comparison);
    }

    /**
     * Filter the query by a related \PanelType object
     *
     * @param \PanelType|ObjectCollection $panelType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPanelQuery The current query, for fluid interface
     */
    public function filterByPanelType($panelType, $comparison = null)
    {
        if ($panelType instanceof \PanelType) {
            return $this
                ->addUsingAlias(PanelTableMap::COL_PANEL_TYPE_ID, $panelType->getId(), $comparison);
        } elseif ($panelType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PanelTableMap::COL_PANEL_TYPE_ID, $panelType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPanelType() only accepts arguments of type \PanelType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PanelType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function joinPanelType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PanelType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PanelType');
        }

        return $this;
    }

    /**
     * Use the PanelType relation PanelType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PanelTypeQuery A secondary query class using the current class as primary query
     */
    public function usePanelTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPanelType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PanelType', '\PanelTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPanel $panel Object to remove from the list of results
     *
     * @return $this|ChildPanelQuery The current query, for fluid interface
     */
    public function prune($panel = null)
    {
        if ($panel) {
            $this->addCond('pruneCond0', $this->getAliasedColName(PanelTableMap::COL_PANEL_ID), $panel->getPanelId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(PanelTableMap::COL_PROJECT_ID), $panel->getProjectId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the panel table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PanelTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PanelTableMap::clearInstancePool();
            PanelTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PanelTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PanelTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PanelTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PanelTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PanelQuery
