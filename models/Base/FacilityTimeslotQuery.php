<?php

namespace Base;

use \FacilityTimeslot as ChildFacilityTimeslot;
use \FacilityTimeslotQuery as ChildFacilityTimeslotQuery;
use \Exception;
use \PDO;
use Map\FacilityTimeslotTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'facility_timeslot' table.
 *
 *
 *
 * @method     ChildFacilityTimeslotQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFacilityTimeslotQuery orderByFacilityId($order = Criteria::ASC) Order by the facility_id column
 * @method     ChildFacilityTimeslotQuery orderByStartTime($order = Criteria::ASC) Order by the starttime column
 * @method     ChildFacilityTimeslotQuery orderByEndTime($order = Criteria::ASC) Order by the endtime column
 * @method     ChildFacilityTimeslotQuery orderByActive($order = Criteria::ASC) Order by the active column
 *
 * @method     ChildFacilityTimeslotQuery groupById() Group by the id column
 * @method     ChildFacilityTimeslotQuery groupByFacilityId() Group by the facility_id column
 * @method     ChildFacilityTimeslotQuery groupByStartTime() Group by the starttime column
 * @method     ChildFacilityTimeslotQuery groupByEndTime() Group by the endtime column
 * @method     ChildFacilityTimeslotQuery groupByActive() Group by the active column
 *
 * @method     ChildFacilityTimeslotQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFacilityTimeslotQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFacilityTimeslotQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFacilityTimeslotQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFacilityTimeslotQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFacilityTimeslotQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFacilityTimeslotQuery leftJoinFacility($relationAlias = null) Adds a LEFT JOIN clause to the query using the Facility relation
 * @method     ChildFacilityTimeslotQuery rightJoinFacility($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Facility relation
 * @method     ChildFacilityTimeslotQuery innerJoinFacility($relationAlias = null) Adds a INNER JOIN clause to the query using the Facility relation
 *
 * @method     ChildFacilityTimeslotQuery joinWithFacility($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Facility relation
 *
 * @method     ChildFacilityTimeslotQuery leftJoinWithFacility() Adds a LEFT JOIN clause and with to the query using the Facility relation
 * @method     ChildFacilityTimeslotQuery rightJoinWithFacility() Adds a RIGHT JOIN clause and with to the query using the Facility relation
 * @method     ChildFacilityTimeslotQuery innerJoinWithFacility() Adds a INNER JOIN clause and with to the query using the Facility relation
 *
 * @method     \FacilityQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFacilityTimeslot findOne(ConnectionInterface $con = null) Return the first ChildFacilityTimeslot matching the query
 * @method     ChildFacilityTimeslot findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFacilityTimeslot matching the query, or a new ChildFacilityTimeslot object populated from the query conditions when no match is found
 *
 * @method     ChildFacilityTimeslot findOneById(string $id) Return the first ChildFacilityTimeslot filtered by the id column
 * @method     ChildFacilityTimeslot findOneByFacilityId(string $facility_id) Return the first ChildFacilityTimeslot filtered by the facility_id column
 * @method     ChildFacilityTimeslot findOneByStartTime(string $starttime) Return the first ChildFacilityTimeslot filtered by the starttime column
 * @method     ChildFacilityTimeslot findOneByEndTime(string $endtime) Return the first ChildFacilityTimeslot filtered by the endtime column
 * @method     ChildFacilityTimeslot findOneByActive(int $active) Return the first ChildFacilityTimeslot filtered by the active column *

 * @method     ChildFacilityTimeslot requirePk($key, ConnectionInterface $con = null) Return the ChildFacilityTimeslot by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityTimeslot requireOne(ConnectionInterface $con = null) Return the first ChildFacilityTimeslot matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFacilityTimeslot requireOneById(string $id) Return the first ChildFacilityTimeslot filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityTimeslot requireOneByFacilityId(string $facility_id) Return the first ChildFacilityTimeslot filtered by the facility_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityTimeslot requireOneByStartTime(string $starttime) Return the first ChildFacilityTimeslot filtered by the starttime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityTimeslot requireOneByEndTime(string $endtime) Return the first ChildFacilityTimeslot filtered by the endtime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFacilityTimeslot requireOneByActive(int $active) Return the first ChildFacilityTimeslot filtered by the active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFacilityTimeslot[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFacilityTimeslot objects based on current ModelCriteria
 * @method     ChildFacilityTimeslot[]|ObjectCollection findById(string $id) Return ChildFacilityTimeslot objects filtered by the id column
 * @method     ChildFacilityTimeslot[]|ObjectCollection findByFacilityId(string $facility_id) Return ChildFacilityTimeslot objects filtered by the facility_id column
 * @method     ChildFacilityTimeslot[]|ObjectCollection findByStartTime(string $starttime) Return ChildFacilityTimeslot objects filtered by the starttime column
 * @method     ChildFacilityTimeslot[]|ObjectCollection findByEndTime(string $endtime) Return ChildFacilityTimeslot objects filtered by the endtime column
 * @method     ChildFacilityTimeslot[]|ObjectCollection findByActive(int $active) Return ChildFacilityTimeslot objects filtered by the active column
 * @method     ChildFacilityTimeslot[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FacilityTimeslotQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\FacilityTimeslotQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\FacilityTimeslot', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFacilityTimeslotQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFacilityTimeslotQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFacilityTimeslotQuery) {
            return $criteria;
        }
        $query = new ChildFacilityTimeslotQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFacilityTimeslot|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FacilityTimeslotTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FacilityTimeslotTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFacilityTimeslot A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, facility_id, starttime, endtime, active FROM facility_timeslot WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFacilityTimeslot $obj */
            $obj = new ChildFacilityTimeslot();
            $obj->hydrate($row);
            FacilityTimeslotTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFacilityTimeslot|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FacilityTimeslotTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FacilityTimeslotTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityTimeslotTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the facility_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFacilityId('fooValue');   // WHERE facility_id = 'fooValue'
     * $query->filterByFacilityId('%fooValue%'); // WHERE facility_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $facilityId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function filterByFacilityId($facilityId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($facilityId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $facilityId)) {
                $facilityId = str_replace('*', '%', $facilityId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FacilityTimeslotTableMap::COL_FACILITY_ID, $facilityId, $comparison);
    }

    /**
     * Filter the query on the starttime column
     *
     * Example usage:
     * <code>
     * $query->filterByStartTime('2011-03-14'); // WHERE starttime = '2011-03-14'
     * $query->filterByStartTime('now'); // WHERE starttime = '2011-03-14'
     * $query->filterByStartTime(array('max' => 'yesterday')); // WHERE starttime > '2011-03-13'
     * </code>
     *
     * @param     mixed $startTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function filterByStartTime($startTime = null, $comparison = null)
    {
        if (is_array($startTime)) {
            $useMinMax = false;
            if (isset($startTime['min'])) {
                $this->addUsingAlias(FacilityTimeslotTableMap::COL_STARTTIME, $startTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startTime['max'])) {
                $this->addUsingAlias(FacilityTimeslotTableMap::COL_STARTTIME, $startTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTimeslotTableMap::COL_STARTTIME, $startTime, $comparison);
    }

    /**
     * Filter the query on the endtime column
     *
     * Example usage:
     * <code>
     * $query->filterByEndTime('2011-03-14'); // WHERE endtime = '2011-03-14'
     * $query->filterByEndTime('now'); // WHERE endtime = '2011-03-14'
     * $query->filterByEndTime(array('max' => 'yesterday')); // WHERE endtime > '2011-03-13'
     * </code>
     *
     * @param     mixed $endTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function filterByEndTime($endTime = null, $comparison = null)
    {
        if (is_array($endTime)) {
            $useMinMax = false;
            if (isset($endTime['min'])) {
                $this->addUsingAlias(FacilityTimeslotTableMap::COL_ENDTIME, $endTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endTime['max'])) {
                $this->addUsingAlias(FacilityTimeslotTableMap::COL_ENDTIME, $endTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTimeslotTableMap::COL_ENDTIME, $endTime, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(1234); // WHERE active = 1234
     * $query->filterByActive(array(12, 34)); // WHERE active IN (12, 34)
     * $query->filterByActive(array('min' => 12)); // WHERE active > 12
     * </code>
     *
     * @param     mixed $active The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_array($active)) {
            $useMinMax = false;
            if (isset($active['min'])) {
                $this->addUsingAlias(FacilityTimeslotTableMap::COL_ACTIVE, $active['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($active['max'])) {
                $this->addUsingAlias(FacilityTimeslotTableMap::COL_ACTIVE, $active['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FacilityTimeslotTableMap::COL_ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query by a related \Facility object
     *
     * @param \Facility|ObjectCollection $facility The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function filterByFacility($facility, $comparison = null)
    {
        if ($facility instanceof \Facility) {
            return $this
                ->addUsingAlias(FacilityTimeslotTableMap::COL_FACILITY_ID, $facility->getId(), $comparison);
        } elseif ($facility instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FacilityTimeslotTableMap::COL_FACILITY_ID, $facility->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFacility() only accepts arguments of type \Facility or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Facility relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function joinFacility($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Facility');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Facility');
        }

        return $this;
    }

    /**
     * Use the Facility relation Facility object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \FacilityQuery A secondary query class using the current class as primary query
     */
    public function useFacilityQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFacility($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Facility', '\FacilityQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFacilityTimeslot $facilityTimeslot Object to remove from the list of results
     *
     * @return $this|ChildFacilityTimeslotQuery The current query, for fluid interface
     */
    public function prune($facilityTimeslot = null)
    {
        if ($facilityTimeslot) {
            $this->addUsingAlias(FacilityTimeslotTableMap::COL_ID, $facilityTimeslot->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the facility_timeslot table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityTimeslotTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FacilityTimeslotTableMap::clearInstancePool();
            FacilityTimeslotTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityTimeslotTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FacilityTimeslotTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FacilityTimeslotTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FacilityTimeslotTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FacilityTimeslotQuery
