<?php

namespace Base;

use \EBulletin as ChildEBulletin;
use \EBulletinQuery as ChildEBulletinQuery;
use \Exception;
use \PDO;
use Map\EBulletinTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'ebulletin' table.
 *
 *
 *
 * @method     ChildEBulletinQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildEBulletinQuery orderByProjectId($order = Criteria::ASC) Order by the project_id column
 * @method     ChildEBulletinQuery orderBySubject($order = Criteria::ASC) Order by the subject column
 * @method     ChildEBulletinQuery orderByPreviewMessage($order = Criteria::ASC) Order by the previewMessage column
 * @method     ChildEBulletinQuery orderByExpiryDate($order = Criteria::ASC) Order by the expiryDate column
 * @method     ChildEBulletinQuery orderByMessage($order = Criteria::ASC) Order by the message column
 * @method     ChildEBulletinQuery orderByPhoto($order = Criteria::ASC) Order by the photo column
 * @method     ChildEBulletinQuery orderByVideo($order = Criteria::ASC) Order by the video column
 * @method     ChildEBulletinQuery orderByAudio($order = Criteria::ASC) Order by the audio column
 * @method     ChildEBulletinQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildEBulletinQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildEBulletinQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildEBulletinQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildEBulletinQuery groupById() Group by the id column
 * @method     ChildEBulletinQuery groupByProjectId() Group by the project_id column
 * @method     ChildEBulletinQuery groupBySubject() Group by the subject column
 * @method     ChildEBulletinQuery groupByPreviewMessage() Group by the previewMessage column
 * @method     ChildEBulletinQuery groupByExpiryDate() Group by the expiryDate column
 * @method     ChildEBulletinQuery groupByMessage() Group by the message column
 * @method     ChildEBulletinQuery groupByPhoto() Group by the photo column
 * @method     ChildEBulletinQuery groupByVideo() Group by the video column
 * @method     ChildEBulletinQuery groupByAudio() Group by the audio column
 * @method     ChildEBulletinQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildEBulletinQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildEBulletinQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildEBulletinQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildEBulletinQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEBulletinQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEBulletinQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEBulletinQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEBulletinQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEBulletinQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEBulletinQuery leftJoinEBulletinRecipient($relationAlias = null) Adds a LEFT JOIN clause to the query using the EBulletinRecipient relation
 * @method     ChildEBulletinQuery rightJoinEBulletinRecipient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EBulletinRecipient relation
 * @method     ChildEBulletinQuery innerJoinEBulletinRecipient($relationAlias = null) Adds a INNER JOIN clause to the query using the EBulletinRecipient relation
 *
 * @method     ChildEBulletinQuery joinWithEBulletinRecipient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EBulletinRecipient relation
 *
 * @method     ChildEBulletinQuery leftJoinWithEBulletinRecipient() Adds a LEFT JOIN clause and with to the query using the EBulletinRecipient relation
 * @method     ChildEBulletinQuery rightJoinWithEBulletinRecipient() Adds a RIGHT JOIN clause and with to the query using the EBulletinRecipient relation
 * @method     ChildEBulletinQuery innerJoinWithEBulletinRecipient() Adds a INNER JOIN clause and with to the query using the EBulletinRecipient relation
 *
 * @method     \EBulletinRecipientQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEBulletin findOne(ConnectionInterface $con = null) Return the first ChildEBulletin matching the query
 * @method     ChildEBulletin findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEBulletin matching the query, or a new ChildEBulletin object populated from the query conditions when no match is found
 *
 * @method     ChildEBulletin findOneById(int $id) Return the first ChildEBulletin filtered by the id column
 * @method     ChildEBulletin findOneByProjectId(string $project_id) Return the first ChildEBulletin filtered by the project_id column
 * @method     ChildEBulletin findOneBySubject(string $subject) Return the first ChildEBulletin filtered by the subject column
 * @method     ChildEBulletin findOneByPreviewMessage(string $previewMessage) Return the first ChildEBulletin filtered by the previewMessage column
 * @method     ChildEBulletin findOneByExpiryDate(string $expiryDate) Return the first ChildEBulletin filtered by the expiryDate column
 * @method     ChildEBulletin findOneByMessage(string $message) Return the first ChildEBulletin filtered by the message column
 * @method     ChildEBulletin findOneByPhoto(string $photo) Return the first ChildEBulletin filtered by the photo column
 * @method     ChildEBulletin findOneByVideo(string $video) Return the first ChildEBulletin filtered by the video column
 * @method     ChildEBulletin findOneByAudio(string $audio) Return the first ChildEBulletin filtered by the audio column
 * @method     ChildEBulletin findOneByCreatedBy(string $created_by) Return the first ChildEBulletin filtered by the created_by column
 * @method     ChildEBulletin findOneByCreatedDate(string $created_date) Return the first ChildEBulletin filtered by the created_date column
 * @method     ChildEBulletin findOneByModifiedBy(string $modified_by) Return the first ChildEBulletin filtered by the modified_by column
 * @method     ChildEBulletin findOneByModifiedDate(string $modified_date) Return the first ChildEBulletin filtered by the modified_date column *

 * @method     ChildEBulletin requirePk($key, ConnectionInterface $con = null) Return the ChildEBulletin by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOne(ConnectionInterface $con = null) Return the first ChildEBulletin matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEBulletin requireOneById(int $id) Return the first ChildEBulletin filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByProjectId(string $project_id) Return the first ChildEBulletin filtered by the project_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneBySubject(string $subject) Return the first ChildEBulletin filtered by the subject column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByPreviewMessage(string $previewMessage) Return the first ChildEBulletin filtered by the previewMessage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByExpiryDate(string $expiryDate) Return the first ChildEBulletin filtered by the expiryDate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByMessage(string $message) Return the first ChildEBulletin filtered by the message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByPhoto(string $photo) Return the first ChildEBulletin filtered by the photo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByVideo(string $video) Return the first ChildEBulletin filtered by the video column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByAudio(string $audio) Return the first ChildEBulletin filtered by the audio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByCreatedBy(string $created_by) Return the first ChildEBulletin filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByCreatedDate(string $created_date) Return the first ChildEBulletin filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByModifiedBy(string $modified_by) Return the first ChildEBulletin filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEBulletin requireOneByModifiedDate(string $modified_date) Return the first ChildEBulletin filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEBulletin[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEBulletin objects based on current ModelCriteria
 * @method     ChildEBulletin[]|ObjectCollection findById(int $id) Return ChildEBulletin objects filtered by the id column
 * @method     ChildEBulletin[]|ObjectCollection findByProjectId(string $project_id) Return ChildEBulletin objects filtered by the project_id column
 * @method     ChildEBulletin[]|ObjectCollection findBySubject(string $subject) Return ChildEBulletin objects filtered by the subject column
 * @method     ChildEBulletin[]|ObjectCollection findByPreviewMessage(string $previewMessage) Return ChildEBulletin objects filtered by the previewMessage column
 * @method     ChildEBulletin[]|ObjectCollection findByExpiryDate(string $expiryDate) Return ChildEBulletin objects filtered by the expiryDate column
 * @method     ChildEBulletin[]|ObjectCollection findByMessage(string $message) Return ChildEBulletin objects filtered by the message column
 * @method     ChildEBulletin[]|ObjectCollection findByPhoto(string $photo) Return ChildEBulletin objects filtered by the photo column
 * @method     ChildEBulletin[]|ObjectCollection findByVideo(string $video) Return ChildEBulletin objects filtered by the video column
 * @method     ChildEBulletin[]|ObjectCollection findByAudio(string $audio) Return ChildEBulletin objects filtered by the audio column
 * @method     ChildEBulletin[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildEBulletin objects filtered by the created_by column
 * @method     ChildEBulletin[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildEBulletin objects filtered by the created_date column
 * @method     ChildEBulletin[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildEBulletin objects filtered by the modified_by column
 * @method     ChildEBulletin[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildEBulletin objects filtered by the modified_date column
 * @method     ChildEBulletin[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EBulletinQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\EBulletinQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\EBulletin', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEBulletinQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEBulletinQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEBulletinQuery) {
            return $criteria;
        }
        $query = new ChildEBulletinQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEBulletin|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = EBulletinTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EBulletinTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEBulletin A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, project_id, subject, previewMessage, expiryDate, message, photo, video, audio, created_by, created_date, modified_by, modified_date FROM ebulletin WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEBulletin $obj */
            $obj = new ChildEBulletin();
            $obj->hydrate($row);
            EBulletinTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEBulletin|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EBulletinTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EBulletinTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(EBulletinTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(EBulletinTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the project_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE project_id = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE project_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_PROJECT_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the subject column
     *
     * Example usage:
     * <code>
     * $query->filterBySubject('fooValue');   // WHERE subject = 'fooValue'
     * $query->filterBySubject('%fooValue%'); // WHERE subject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subject The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterBySubject($subject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subject)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $subject)) {
                $subject = str_replace('*', '%', $subject);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_SUBJECT, $subject, $comparison);
    }

    /**
     * Filter the query on the previewMessage column
     *
     * Example usage:
     * <code>
     * $query->filterByPreviewMessage('fooValue');   // WHERE previewMessage = 'fooValue'
     * $query->filterByPreviewMessage('%fooValue%'); // WHERE previewMessage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $previewMessage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByPreviewMessage($previewMessage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($previewMessage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $previewMessage)) {
                $previewMessage = str_replace('*', '%', $previewMessage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_PREVIEWMESSAGE, $previewMessage, $comparison);
    }

    /**
     * Filter the query on the expiryDate column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiryDate('2011-03-14'); // WHERE expiryDate = '2011-03-14'
     * $query->filterByExpiryDate('now'); // WHERE expiryDate = '2011-03-14'
     * $query->filterByExpiryDate(array('max' => 'yesterday')); // WHERE expiryDate > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiryDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByExpiryDate($expiryDate = null, $comparison = null)
    {
        if (is_array($expiryDate)) {
            $useMinMax = false;
            if (isset($expiryDate['min'])) {
                $this->addUsingAlias(EBulletinTableMap::COL_EXPIRYDATE, $expiryDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiryDate['max'])) {
                $this->addUsingAlias(EBulletinTableMap::COL_EXPIRYDATE, $expiryDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_EXPIRYDATE, $expiryDate, $comparison);
    }

    /**
     * Filter the query on the message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
     * $query->filterByMessage('%fooValue%'); // WHERE message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $message)) {
                $message = str_replace('*', '%', $message);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the photo column
     *
     * Example usage:
     * <code>
     * $query->filterByPhoto('fooValue');   // WHERE photo = 'fooValue'
     * $query->filterByPhoto('%fooValue%'); // WHERE photo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $photo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByPhoto($photo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($photo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $photo)) {
                $photo = str_replace('*', '%', $photo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_PHOTO, $photo, $comparison);
    }

    /**
     * Filter the query on the video column
     *
     * Example usage:
     * <code>
     * $query->filterByVideo('fooValue');   // WHERE video = 'fooValue'
     * $query->filterByVideo('%fooValue%'); // WHERE video LIKE '%fooValue%'
     * </code>
     *
     * @param     string $video The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByVideo($video = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($video)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $video)) {
                $video = str_replace('*', '%', $video);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_VIDEO, $video, $comparison);
    }

    /**
     * Filter the query on the audio column
     *
     * Example usage:
     * <code>
     * $query->filterByAudio('fooValue');   // WHERE audio = 'fooValue'
     * $query->filterByAudio('%fooValue%'); // WHERE audio LIKE '%fooValue%'
     * </code>
     *
     * @param     string $audio The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByAudio($audio = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($audio)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $audio)) {
                $audio = str_replace('*', '%', $audio);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_AUDIO, $audio, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(EBulletinTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(EBulletinTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(EBulletinTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(EBulletinTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EBulletinTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Filter the query by a related \EBulletinRecipient object
     *
     * @param \EBulletinRecipient|ObjectCollection $eBulletinRecipient the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEBulletinQuery The current query, for fluid interface
     */
    public function filterByEBulletinRecipient($eBulletinRecipient, $comparison = null)
    {
        if ($eBulletinRecipient instanceof \EBulletinRecipient) {
            return $this
                ->addUsingAlias(EBulletinTableMap::COL_ID, $eBulletinRecipient->getEBulletinId(), $comparison);
        } elseif ($eBulletinRecipient instanceof ObjectCollection) {
            return $this
                ->useEBulletinRecipientQuery()
                ->filterByPrimaryKeys($eBulletinRecipient->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEBulletinRecipient() only accepts arguments of type \EBulletinRecipient or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EBulletinRecipient relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function joinEBulletinRecipient($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EBulletinRecipient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EBulletinRecipient');
        }

        return $this;
    }

    /**
     * Use the EBulletinRecipient relation EBulletinRecipient object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EBulletinRecipientQuery A secondary query class using the current class as primary query
     */
    public function useEBulletinRecipientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEBulletinRecipient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EBulletinRecipient', '\EBulletinRecipientQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEBulletin $eBulletin Object to remove from the list of results
     *
     * @return $this|ChildEBulletinQuery The current query, for fluid interface
     */
    public function prune($eBulletin = null)
    {
        if ($eBulletin) {
            $this->addUsingAlias(EBulletinTableMap::COL_ID, $eBulletin->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the ebulletin table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EBulletinTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EBulletinTableMap::clearInstancePool();
            EBulletinTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EBulletinTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EBulletinTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EBulletinTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EBulletinTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EBulletinQuery
