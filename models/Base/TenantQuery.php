<?php

namespace Base;

use \Tenant as ChildTenant;
use \TenantQuery as ChildTenantQuery;
use \Exception;
use \PDO;
use Map\TenantTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tenant' table.
 *
 *
 *
 * @method     ChildTenantQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildTenantQuery orderByProjectId($order = Criteria::ASC) Order by the projectId column
 * @method     ChildTenantQuery orderByUnitId($order = Criteria::ASC) Order by the unitId column
 * @method     ChildTenantQuery orderByUnitBlock($order = Criteria::ASC) Order by the unitBlock column
 * @method     ChildTenantQuery orderByUnitFloor($order = Criteria::ASC) Order by the unitFloor column
 * @method     ChildTenantQuery orderByUnitNo($order = Criteria::ASC) Order by the unitNo column
 * @method     ChildTenantQuery orderByUnitLayoutPlan($order = Criteria::ASC) Order by the unitLayoutPlan column
 * @method     ChildTenantQuery orderByLiftId($order = Criteria::ASC) Order by the liftId column
 * @method     ChildTenantQuery orderBySipServerIpAddress($order = Criteria::ASC) Order by the sipServerIpAddress column
 * @method     ChildTenantQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildTenantQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildTenantQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildTenantQuery orderByHomeNo($order = Criteria::ASC) Order by the homeNo column
 * @method     ChildTenantQuery orderByMobileNo($order = Criteria::ASC) Order by the mobileNo column
 * @method     ChildTenantQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildTenantQuery orderByMultipleIntercom($order = Criteria::ASC) Order by the multiple_intercom column
 * @method     ChildTenantQuery orderByDateOfBirth($order = Criteria::ASC) Order by the dateOfBirth column
 * @method     ChildTenantQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method     ChildTenantQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method     ChildTenantQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method     ChildTenantQuery orderByModifiedDate($order = Criteria::ASC) Order by the modified_date column
 *
 * @method     ChildTenantQuery groupById() Group by the id column
 * @method     ChildTenantQuery groupByProjectId() Group by the projectId column
 * @method     ChildTenantQuery groupByUnitId() Group by the unitId column
 * @method     ChildTenantQuery groupByUnitBlock() Group by the unitBlock column
 * @method     ChildTenantQuery groupByUnitFloor() Group by the unitFloor column
 * @method     ChildTenantQuery groupByUnitNo() Group by the unitNo column
 * @method     ChildTenantQuery groupByUnitLayoutPlan() Group by the unitLayoutPlan column
 * @method     ChildTenantQuery groupByLiftId() Group by the liftId column
 * @method     ChildTenantQuery groupBySipServerIpAddress() Group by the sipServerIpAddress column
 * @method     ChildTenantQuery groupByUsername() Group by the username column
 * @method     ChildTenantQuery groupByPassword() Group by the password column
 * @method     ChildTenantQuery groupByName() Group by the name column
 * @method     ChildTenantQuery groupByHomeNo() Group by the homeNo column
 * @method     ChildTenantQuery groupByMobileNo() Group by the mobileNo column
 * @method     ChildTenantQuery groupByEmail() Group by the email column
 * @method     ChildTenantQuery groupByMultipleIntercom() Group by the multiple_intercom column
 * @method     ChildTenantQuery groupByDateOfBirth() Group by the dateOfBirth column
 * @method     ChildTenantQuery groupByCreatedBy() Group by the created_by column
 * @method     ChildTenantQuery groupByCreatedDate() Group by the created_date column
 * @method     ChildTenantQuery groupByModifiedBy() Group by the modified_by column
 * @method     ChildTenantQuery groupByModifiedDate() Group by the modified_date column
 *
 * @method     ChildTenantQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTenantQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTenantQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTenantQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTenantQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTenantQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTenant findOne(ConnectionInterface $con = null) Return the first ChildTenant matching the query
 * @method     ChildTenant findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTenant matching the query, or a new ChildTenant object populated from the query conditions when no match is found
 *
 * @method     ChildTenant findOneById(string $id) Return the first ChildTenant filtered by the id column
 * @method     ChildTenant findOneByProjectId(string $projectId) Return the first ChildTenant filtered by the projectId column
 * @method     ChildTenant findOneByUnitId(string $unitId) Return the first ChildTenant filtered by the unitId column
 * @method     ChildTenant findOneByUnitBlock(string $unitBlock) Return the first ChildTenant filtered by the unitBlock column
 * @method     ChildTenant findOneByUnitFloor(string $unitFloor) Return the first ChildTenant filtered by the unitFloor column
 * @method     ChildTenant findOneByUnitNo(string $unitNo) Return the first ChildTenant filtered by the unitNo column
 * @method     ChildTenant findOneByUnitLayoutPlan(string $unitLayoutPlan) Return the first ChildTenant filtered by the unitLayoutPlan column
 * @method     ChildTenant findOneByLiftId(string $liftId) Return the first ChildTenant filtered by the liftId column
 * @method     ChildTenant findOneBySipServerIpAddress(string $sipServerIpAddress) Return the first ChildTenant filtered by the sipServerIpAddress column
 * @method     ChildTenant findOneByUsername(string $username) Return the first ChildTenant filtered by the username column
 * @method     ChildTenant findOneByPassword(string $password) Return the first ChildTenant filtered by the password column
 * @method     ChildTenant findOneByName(string $name) Return the first ChildTenant filtered by the name column
 * @method     ChildTenant findOneByHomeNo(string $homeNo) Return the first ChildTenant filtered by the homeNo column
 * @method     ChildTenant findOneByMobileNo(string $mobileNo) Return the first ChildTenant filtered by the mobileNo column
 * @method     ChildTenant findOneByEmail(string $email) Return the first ChildTenant filtered by the email column
 * @method     ChildTenant findOneByMultipleIntercom(int $multiple_intercom) Return the first ChildTenant filtered by the multiple_intercom column
 * @method     ChildTenant findOneByDateOfBirth(string $dateOfBirth) Return the first ChildTenant filtered by the dateOfBirth column
 * @method     ChildTenant findOneByCreatedBy(string $created_by) Return the first ChildTenant filtered by the created_by column
 * @method     ChildTenant findOneByCreatedDate(string $created_date) Return the first ChildTenant filtered by the created_date column
 * @method     ChildTenant findOneByModifiedBy(string $modified_by) Return the first ChildTenant filtered by the modified_by column
 * @method     ChildTenant findOneByModifiedDate(string $modified_date) Return the first ChildTenant filtered by the modified_date column *

 * @method     ChildTenant requirePk($key, ConnectionInterface $con = null) Return the ChildTenant by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOne(ConnectionInterface $con = null) Return the first ChildTenant matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTenant requireOneById(string $id) Return the first ChildTenant filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByProjectId(string $projectId) Return the first ChildTenant filtered by the projectId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByUnitId(string $unitId) Return the first ChildTenant filtered by the unitId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByUnitBlock(string $unitBlock) Return the first ChildTenant filtered by the unitBlock column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByUnitFloor(string $unitFloor) Return the first ChildTenant filtered by the unitFloor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByUnitNo(string $unitNo) Return the first ChildTenant filtered by the unitNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByUnitLayoutPlan(string $unitLayoutPlan) Return the first ChildTenant filtered by the unitLayoutPlan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByLiftId(string $liftId) Return the first ChildTenant filtered by the liftId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneBySipServerIpAddress(string $sipServerIpAddress) Return the first ChildTenant filtered by the sipServerIpAddress column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByUsername(string $username) Return the first ChildTenant filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByPassword(string $password) Return the first ChildTenant filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByName(string $name) Return the first ChildTenant filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByHomeNo(string $homeNo) Return the first ChildTenant filtered by the homeNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByMobileNo(string $mobileNo) Return the first ChildTenant filtered by the mobileNo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByEmail(string $email) Return the first ChildTenant filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByMultipleIntercom(int $multiple_intercom) Return the first ChildTenant filtered by the multiple_intercom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByDateOfBirth(string $dateOfBirth) Return the first ChildTenant filtered by the dateOfBirth column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByCreatedBy(string $created_by) Return the first ChildTenant filtered by the created_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByCreatedDate(string $created_date) Return the first ChildTenant filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByModifiedBy(string $modified_by) Return the first ChildTenant filtered by the modified_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTenant requireOneByModifiedDate(string $modified_date) Return the first ChildTenant filtered by the modified_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTenant[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTenant objects based on current ModelCriteria
 * @method     ChildTenant[]|ObjectCollection findById(string $id) Return ChildTenant objects filtered by the id column
 * @method     ChildTenant[]|ObjectCollection findByProjectId(string $projectId) Return ChildTenant objects filtered by the projectId column
 * @method     ChildTenant[]|ObjectCollection findByUnitId(string $unitId) Return ChildTenant objects filtered by the unitId column
 * @method     ChildTenant[]|ObjectCollection findByUnitBlock(string $unitBlock) Return ChildTenant objects filtered by the unitBlock column
 * @method     ChildTenant[]|ObjectCollection findByUnitFloor(string $unitFloor) Return ChildTenant objects filtered by the unitFloor column
 * @method     ChildTenant[]|ObjectCollection findByUnitNo(string $unitNo) Return ChildTenant objects filtered by the unitNo column
 * @method     ChildTenant[]|ObjectCollection findByUnitLayoutPlan(string $unitLayoutPlan) Return ChildTenant objects filtered by the unitLayoutPlan column
 * @method     ChildTenant[]|ObjectCollection findByLiftId(string $liftId) Return ChildTenant objects filtered by the liftId column
 * @method     ChildTenant[]|ObjectCollection findBySipServerIpAddress(string $sipServerIpAddress) Return ChildTenant objects filtered by the sipServerIpAddress column
 * @method     ChildTenant[]|ObjectCollection findByUsername(string $username) Return ChildTenant objects filtered by the username column
 * @method     ChildTenant[]|ObjectCollection findByPassword(string $password) Return ChildTenant objects filtered by the password column
 * @method     ChildTenant[]|ObjectCollection findByName(string $name) Return ChildTenant objects filtered by the name column
 * @method     ChildTenant[]|ObjectCollection findByHomeNo(string $homeNo) Return ChildTenant objects filtered by the homeNo column
 * @method     ChildTenant[]|ObjectCollection findByMobileNo(string $mobileNo) Return ChildTenant objects filtered by the mobileNo column
 * @method     ChildTenant[]|ObjectCollection findByEmail(string $email) Return ChildTenant objects filtered by the email column
 * @method     ChildTenant[]|ObjectCollection findByMultipleIntercom(int $multiple_intercom) Return ChildTenant objects filtered by the multiple_intercom column
 * @method     ChildTenant[]|ObjectCollection findByDateOfBirth(string $dateOfBirth) Return ChildTenant objects filtered by the dateOfBirth column
 * @method     ChildTenant[]|ObjectCollection findByCreatedBy(string $created_by) Return ChildTenant objects filtered by the created_by column
 * @method     ChildTenant[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildTenant objects filtered by the created_date column
 * @method     ChildTenant[]|ObjectCollection findByModifiedBy(string $modified_by) Return ChildTenant objects filtered by the modified_by column
 * @method     ChildTenant[]|ObjectCollection findByModifiedDate(string $modified_date) Return ChildTenant objects filtered by the modified_date column
 * @method     ChildTenant[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TenantQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TenantQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Tenant', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTenantQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTenantQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTenantQuery) {
            return $criteria;
        }
        $query = new ChildTenantQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$id, $projectId] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTenant|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TenantTableMap::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TenantTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTenant A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, projectId, unitId, unitBlock, unitFloor, unitNo, unitLayoutPlan, liftId, sipServerIpAddress, username, password, name, homeNo, mobileNo, email, multiple_intercom, dateOfBirth, created_by, created_date, modified_by, modified_date FROM tenant WHERE id = :p0 AND projectId = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTenant $obj */
            $obj = new ChildTenant();
            $obj->hydrate($row);
            TenantTableMap::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTenant|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(TenantTableMap::COL_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(TenantTableMap::COL_PROJECTID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(TenantTableMap::COL_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(TenantTableMap::COL_PROJECTID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $id)) {
                $id = str_replace('*', '%', $id);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the projectId column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId('fooValue');   // WHERE projectId = 'fooValue'
     * $query->filterByProjectId('%fooValue%'); // WHERE projectId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $projectId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($projectId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $projectId)) {
                $projectId = str_replace('*', '%', $projectId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_PROJECTID, $projectId, $comparison);
    }

    /**
     * Filter the query on the unitId column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId('fooValue');   // WHERE unitId = 'fooValue'
     * $query->filterByUnitId('%fooValue%'); // WHERE unitId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitId)) {
                $unitId = str_replace('*', '%', $unitId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_UNITID, $unitId, $comparison);
    }

    /**
     * Filter the query on the unitBlock column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitBlock('fooValue');   // WHERE unitBlock = 'fooValue'
     * $query->filterByUnitBlock('%fooValue%'); // WHERE unitBlock LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitBlock The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByUnitBlock($unitBlock = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitBlock)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitBlock)) {
                $unitBlock = str_replace('*', '%', $unitBlock);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_UNITBLOCK, $unitBlock, $comparison);
    }

    /**
     * Filter the query on the unitFloor column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitFloor('fooValue');   // WHERE unitFloor = 'fooValue'
     * $query->filterByUnitFloor('%fooValue%'); // WHERE unitFloor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitFloor The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByUnitFloor($unitFloor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitFloor)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitFloor)) {
                $unitFloor = str_replace('*', '%', $unitFloor);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_UNITFLOOR, $unitFloor, $comparison);
    }

    /**
     * Filter the query on the unitNo column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitNo('fooValue');   // WHERE unitNo = 'fooValue'
     * $query->filterByUnitNo('%fooValue%'); // WHERE unitNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByUnitNo($unitNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitNo)) {
                $unitNo = str_replace('*', '%', $unitNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_UNITNO, $unitNo, $comparison);
    }

    /**
     * Filter the query on the unitLayoutPlan column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitLayoutPlan('fooValue');   // WHERE unitLayoutPlan = 'fooValue'
     * $query->filterByUnitLayoutPlan('%fooValue%'); // WHERE unitLayoutPlan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unitLayoutPlan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByUnitLayoutPlan($unitLayoutPlan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unitLayoutPlan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $unitLayoutPlan)) {
                $unitLayoutPlan = str_replace('*', '%', $unitLayoutPlan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_UNITLAYOUTPLAN, $unitLayoutPlan, $comparison);
    }

    /**
     * Filter the query on the liftId column
     *
     * Example usage:
     * <code>
     * $query->filterByLiftId('fooValue');   // WHERE liftId = 'fooValue'
     * $query->filterByLiftId('%fooValue%'); // WHERE liftId LIKE '%fooValue%'
     * </code>
     *
     * @param     string $liftId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByLiftId($liftId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($liftId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $liftId)) {
                $liftId = str_replace('*', '%', $liftId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_LIFTID, $liftId, $comparison);
    }

    /**
     * Filter the query on the sipServerIpAddress column
     *
     * Example usage:
     * <code>
     * $query->filterBySipServerIpAddress('fooValue');   // WHERE sipServerIpAddress = 'fooValue'
     * $query->filterBySipServerIpAddress('%fooValue%'); // WHERE sipServerIpAddress LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sipServerIpAddress The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterBySipServerIpAddress($sipServerIpAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sipServerIpAddress)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sipServerIpAddress)) {
                $sipServerIpAddress = str_replace('*', '%', $sipServerIpAddress);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_SIPSERVERIPADDRESS, $sipServerIpAddress, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%'); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $username)) {
                $username = str_replace('*', '%', $username);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the homeNo column
     *
     * Example usage:
     * <code>
     * $query->filterByHomeNo('fooValue');   // WHERE homeNo = 'fooValue'
     * $query->filterByHomeNo('%fooValue%'); // WHERE homeNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $homeNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByHomeNo($homeNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($homeNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $homeNo)) {
                $homeNo = str_replace('*', '%', $homeNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_HOMENO, $homeNo, $comparison);
    }

    /**
     * Filter the query on the mobileNo column
     *
     * Example usage:
     * <code>
     * $query->filterByMobileNo('fooValue');   // WHERE mobileNo = 'fooValue'
     * $query->filterByMobileNo('%fooValue%'); // WHERE mobileNo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mobileNo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByMobileNo($mobileNo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mobileNo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mobileNo)) {
                $mobileNo = str_replace('*', '%', $mobileNo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_MOBILENO, $mobileNo, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the multiple_intercom column
     *
     * Example usage:
     * <code>
     * $query->filterByMultipleIntercom(1234); // WHERE multiple_intercom = 1234
     * $query->filterByMultipleIntercom(array(12, 34)); // WHERE multiple_intercom IN (12, 34)
     * $query->filterByMultipleIntercom(array('min' => 12)); // WHERE multiple_intercom > 12
     * </code>
     *
     * @param     mixed $multipleIntercom The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByMultipleIntercom($multipleIntercom = null, $comparison = null)
    {
        if (is_array($multipleIntercom)) {
            $useMinMax = false;
            if (isset($multipleIntercom['min'])) {
                $this->addUsingAlias(TenantTableMap::COL_MULTIPLE_INTERCOM, $multipleIntercom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($multipleIntercom['max'])) {
                $this->addUsingAlias(TenantTableMap::COL_MULTIPLE_INTERCOM, $multipleIntercom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_MULTIPLE_INTERCOM, $multipleIntercom, $comparison);
    }

    /**
     * Filter the query on the dateOfBirth column
     *
     * Example usage:
     * <code>
     * $query->filterByDateOfBirth('fooValue');   // WHERE dateOfBirth = 'fooValue'
     * $query->filterByDateOfBirth('%fooValue%'); // WHERE dateOfBirth LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dateOfBirth The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByDateOfBirth($dateOfBirth = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dateOfBirth)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dateOfBirth)) {
                $dateOfBirth = str_replace('*', '%', $dateOfBirth);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_DATEOFBIRTH, $dateOfBirth, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy('fooValue');   // WHERE created_by = 'fooValue'
     * $query->filterByCreatedBy('%fooValue%'); // WHERE created_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdBy)) {
                $createdBy = str_replace('*', '%', $createdBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(TenantTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(TenantTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy('fooValue');   // WHERE modified_by = 'fooValue'
     * $query->filterByModifiedBy('%fooValue%'); // WHERE modified_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modifiedBy The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modifiedBy)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modifiedBy)) {
                $modifiedBy = str_replace('*', '%', $modifiedBy);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the modified_date column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedDate('2011-03-14'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate('now'); // WHERE modified_date = '2011-03-14'
     * $query->filterByModifiedDate(array('max' => 'yesterday')); // WHERE modified_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function filterByModifiedDate($modifiedDate = null, $comparison = null)
    {
        if (is_array($modifiedDate)) {
            $useMinMax = false;
            if (isset($modifiedDate['min'])) {
                $this->addUsingAlias(TenantTableMap::COL_MODIFIED_DATE, $modifiedDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedDate['max'])) {
                $this->addUsingAlias(TenantTableMap::COL_MODIFIED_DATE, $modifiedDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TenantTableMap::COL_MODIFIED_DATE, $modifiedDate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTenant $tenant Object to remove from the list of results
     *
     * @return $this|ChildTenantQuery The current query, for fluid interface
     */
    public function prune($tenant = null)
    {
        if ($tenant) {
            $this->addCond('pruneCond0', $this->getAliasedColName(TenantTableMap::COL_ID), $tenant->getId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(TenantTableMap::COL_PROJECTID), $tenant->getProjectId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tenant table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TenantTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TenantTableMap::clearInstancePool();
            TenantTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TenantTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TenantTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TenantTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TenantTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TenantQuery
