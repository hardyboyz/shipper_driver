<?php

namespace Map;

use \Facility;
use \FacilityQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'facility' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class FacilityTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.FacilityTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'facility';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Facility';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Facility';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the id field
     */
    const COL_ID = 'facility.id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'facility.name';

    /**
     * the column name for the project_id field
     */
    const COL_PROJECT_ID = 'facility.project_id';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'facility.type';

    /**
     * the column name for the requirebooking field
     */
    const COL_REQUIREBOOKING = 'facility.requirebooking';

    /**
     * the column name for the advancedbooking field
     */
    const COL_ADVANCEDBOOKING = 'facility.advancedbooking';

    /**
     * the column name for the maxhour field
     */
    const COL_MAXHOUR = 'facility.maxhour';

    /**
     * the column name for the coolingoff field
     */
    const COL_COOLINGOFF = 'facility.coolingoff';

    /**
     * the column name for the starttime field
     */
    const COL_STARTTIME = 'facility.starttime';

    /**
     * the column name for the endtime field
     */
    const COL_ENDTIME = 'facility.endtime';

    /**
     * the column name for the deposit field
     */
    const COL_DEPOSIT = 'facility.deposit';

    /**
     * the column name for the charge field
     */
    const COL_CHARGE = 'facility.charge';

    /**
     * the column name for the rules field
     */
    const COL_RULES = 'facility.rules';

    /**
     * the column name for the enabled field
     */
    const COL_ENABLED = 'facility.enabled';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'facility.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'facility.created_date';

    /**
     * the column name for the modified_by field
     */
    const COL_MODIFIED_BY = 'facility.modified_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'facility.modified_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'ProjectId', 'Type', 'RequireBooking', 'AdvancedBooking', 'MaxHour', 'CoolingOff', 'StartTime', 'EndTime', 'Deposit', 'Charge', 'Rules', 'Enabled', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'projectId', 'type', 'requireBooking', 'advancedBooking', 'maxHour', 'coolingOff', 'startTime', 'endTime', 'deposit', 'charge', 'rules', 'enabled', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', ),
        self::TYPE_COLNAME       => array(FacilityTableMap::COL_ID, FacilityTableMap::COL_NAME, FacilityTableMap::COL_PROJECT_ID, FacilityTableMap::COL_TYPE, FacilityTableMap::COL_REQUIREBOOKING, FacilityTableMap::COL_ADVANCEDBOOKING, FacilityTableMap::COL_MAXHOUR, FacilityTableMap::COL_COOLINGOFF, FacilityTableMap::COL_STARTTIME, FacilityTableMap::COL_ENDTIME, FacilityTableMap::COL_DEPOSIT, FacilityTableMap::COL_CHARGE, FacilityTableMap::COL_RULES, FacilityTableMap::COL_ENABLED, FacilityTableMap::COL_CREATED_BY, FacilityTableMap::COL_CREATED_DATE, FacilityTableMap::COL_MODIFIED_BY, FacilityTableMap::COL_MODIFIED_DATE, ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'project_id', 'type', 'requirebooking', 'advancedbooking', 'maxhour', 'coolingoff', 'starttime', 'endtime', 'deposit', 'charge', 'rules', 'enabled', 'created_by', 'created_date', 'modified_by', 'modified_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'ProjectId' => 2, 'Type' => 3, 'RequireBooking' => 4, 'AdvancedBooking' => 5, 'MaxHour' => 6, 'CoolingOff' => 7, 'StartTime' => 8, 'EndTime' => 9, 'Deposit' => 10, 'Charge' => 11, 'Rules' => 12, 'Enabled' => 13, 'CreatedBy' => 14, 'CreatedDate' => 15, 'ModifiedBy' => 16, 'ModifiedDate' => 17, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'projectId' => 2, 'type' => 3, 'requireBooking' => 4, 'advancedBooking' => 5, 'maxHour' => 6, 'coolingOff' => 7, 'startTime' => 8, 'endTime' => 9, 'deposit' => 10, 'charge' => 11, 'rules' => 12, 'enabled' => 13, 'createdBy' => 14, 'createdDate' => 15, 'modifiedBy' => 16, 'modifiedDate' => 17, ),
        self::TYPE_COLNAME       => array(FacilityTableMap::COL_ID => 0, FacilityTableMap::COL_NAME => 1, FacilityTableMap::COL_PROJECT_ID => 2, FacilityTableMap::COL_TYPE => 3, FacilityTableMap::COL_REQUIREBOOKING => 4, FacilityTableMap::COL_ADVANCEDBOOKING => 5, FacilityTableMap::COL_MAXHOUR => 6, FacilityTableMap::COL_COOLINGOFF => 7, FacilityTableMap::COL_STARTTIME => 8, FacilityTableMap::COL_ENDTIME => 9, FacilityTableMap::COL_DEPOSIT => 10, FacilityTableMap::COL_CHARGE => 11, FacilityTableMap::COL_RULES => 12, FacilityTableMap::COL_ENABLED => 13, FacilityTableMap::COL_CREATED_BY => 14, FacilityTableMap::COL_CREATED_DATE => 15, FacilityTableMap::COL_MODIFIED_BY => 16, FacilityTableMap::COL_MODIFIED_DATE => 17, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'project_id' => 2, 'type' => 3, 'requirebooking' => 4, 'advancedbooking' => 5, 'maxhour' => 6, 'coolingoff' => 7, 'starttime' => 8, 'endtime' => 9, 'deposit' => 10, 'charge' => 11, 'rules' => 12, 'enabled' => 13, 'created_by' => 14, 'created_date' => 15, 'modified_by' => 16, 'modified_date' => 17, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('facility');
        $this->setPhpName('Facility');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Facility');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 10, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 50, null);
        $this->addColumn('project_id', 'ProjectId', 'VARCHAR', true, 15, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 10, null);
        $this->addColumn('requirebooking', 'RequireBooking', 'TINYINT', true, 1, null);
        $this->addColumn('advancedbooking', 'AdvancedBooking', 'INTEGER', true, 11, null);
        $this->addColumn('maxhour', 'MaxHour', 'INTEGER', true, 11, null);
        $this->addColumn('coolingoff', 'CoolingOff', 'INTEGER', true, 11, null);
        $this->addColumn('starttime', 'StartTime', 'TIME', true, null, null);
        $this->addColumn('endtime', 'EndTime', 'TIME', true, null, null);
        $this->addColumn('deposit', 'Deposit', 'DOUBLE', false, null, null);
        $this->addColumn('charge', 'Charge', 'DOUBLE', false, null, null);
        $this->addColumn('rules', 'Rules', 'LONGVARCHAR', false, null, null);
        $this->addColumn('enabled', 'Enabled', 'TINYINT', true, 1, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modified_by', 'ModifiedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('FacilityTimeslot', '\\FacilityTimeslot', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':facility_id',
    1 => ':id',
  ),
), null, null, 'FacilityTimeslots', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? FacilityTableMap::CLASS_DEFAULT : FacilityTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Facility object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = FacilityTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = FacilityTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + FacilityTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = FacilityTableMap::OM_CLASS;
            /** @var Facility $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            FacilityTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = FacilityTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = FacilityTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Facility $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                FacilityTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(FacilityTableMap::COL_ID);
            $criteria->addSelectColumn(FacilityTableMap::COL_NAME);
            $criteria->addSelectColumn(FacilityTableMap::COL_PROJECT_ID);
            $criteria->addSelectColumn(FacilityTableMap::COL_TYPE);
            $criteria->addSelectColumn(FacilityTableMap::COL_REQUIREBOOKING);
            $criteria->addSelectColumn(FacilityTableMap::COL_ADVANCEDBOOKING);
            $criteria->addSelectColumn(FacilityTableMap::COL_MAXHOUR);
            $criteria->addSelectColumn(FacilityTableMap::COL_COOLINGOFF);
            $criteria->addSelectColumn(FacilityTableMap::COL_STARTTIME);
            $criteria->addSelectColumn(FacilityTableMap::COL_ENDTIME);
            $criteria->addSelectColumn(FacilityTableMap::COL_DEPOSIT);
            $criteria->addSelectColumn(FacilityTableMap::COL_CHARGE);
            $criteria->addSelectColumn(FacilityTableMap::COL_RULES);
            $criteria->addSelectColumn(FacilityTableMap::COL_ENABLED);
            $criteria->addSelectColumn(FacilityTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(FacilityTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(FacilityTableMap::COL_MODIFIED_BY);
            $criteria->addSelectColumn(FacilityTableMap::COL_MODIFIED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.project_id');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.requirebooking');
            $criteria->addSelectColumn($alias . '.advancedbooking');
            $criteria->addSelectColumn($alias . '.maxhour');
            $criteria->addSelectColumn($alias . '.coolingoff');
            $criteria->addSelectColumn($alias . '.starttime');
            $criteria->addSelectColumn($alias . '.endtime');
            $criteria->addSelectColumn($alias . '.deposit');
            $criteria->addSelectColumn($alias . '.charge');
            $criteria->addSelectColumn($alias . '.rules');
            $criteria->addSelectColumn($alias . '.enabled');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modified_by');
            $criteria->addSelectColumn($alias . '.modified_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(FacilityTableMap::DATABASE_NAME)->getTable(FacilityTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(FacilityTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(FacilityTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new FacilityTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Facility or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Facility object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Facility) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(FacilityTableMap::DATABASE_NAME);
            $criteria->add(FacilityTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = FacilityQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            FacilityTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                FacilityTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the facility table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return FacilityQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Facility or Criteria object.
     *
     * @param mixed               $criteria Criteria or Facility object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Facility object
        }


        // Set the correct dbName
        $query = FacilityQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // FacilityTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
FacilityTableMap::buildTableMap();
