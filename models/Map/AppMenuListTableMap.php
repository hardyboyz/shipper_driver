<?php

namespace Map;

use \AppMenuList;
use \AppMenuListQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'appMenuList' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AppMenuListTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AppMenuListTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'appMenuList';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AppMenuList';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AppMenuList';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the id field
     */
    const COL_ID = 'appMenuList.id';

    /**
     * the column name for the projectId field
     */
    const COL_PROJECTID = 'appMenuList.projectId';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'appMenuList.name';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'appMenuList.description';

    /**
     * the column name for the image field
     */
    const COL_IMAGE = 'appMenuList.image';

    /**
     * the column name for the sideMenuOrder field
     */
    const COL_SIDEMENUORDER = 'appMenuList.sideMenuOrder';

    /**
     * the column name for the sideMenuEnable field
     */
    const COL_SIDEMENUENABLE = 'appMenuList.sideMenuEnable';

    /**
     * the column name for the homeMenuOrder field
     */
    const COL_HOMEMENUORDER = 'appMenuList.homeMenuOrder';

    /**
     * the column name for the homeMenuEnable field
     */
    const COL_HOMEMENUENABLE = 'appMenuList.homeMenuEnable';

    /**
     * the column name for the topRightMenuOrder field
     */
    const COL_TOPRIGHTMENUORDER = 'appMenuList.topRightMenuOrder';

    /**
     * the column name for the topRightMenuEnable field
     */
    const COL_TOPRIGHTMENUENABLE = 'appMenuList.topRightMenuEnable';

    /**
     * the column name for the tutorialImage field
     */
    const COL_TUTORIALIMAGE = 'appMenuList.tutorialImage';

    /**
     * the column name for the tutorialEnable field
     */
    const COL_TUTORIALENABLE = 'appMenuList.tutorialEnable';

    /**
     * the column name for the phoneTutorialImage field
     */
    const COL_PHONETUTORIALIMAGE = 'appMenuList.phoneTutorialImage';

    /**
     * the column name for the phoneTutorialEnable field
     */
    const COL_PHONETUTORIALENABLE = 'appMenuList.phoneTutorialEnable';

    /**
     * the column name for the phoneIcon field
     */
    const COL_PHONEICON = 'appMenuList.phoneIcon';

    /**
     * the column name for the module_type field
     */
    const COL_MODULE_TYPE = 'appMenuList.module_type';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'appMenuList.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'appMenuList.created_date';

    /**
     * the column name for the modified_by field
     */
    const COL_MODIFIED_BY = 'appMenuList.modified_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'appMenuList.modified_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'Name', 'Description', 'Image', 'SideMenuOrder', 'SideMenuEnable', 'HomeMenuOrder', 'HomeMenuEnable', 'TopRightMenuOrder', 'TopRightMenuEnable', 'TutorialImage', 'TutorialEnable', 'PhoneTutorialImage', 'PhoneTutorialEnable', 'PhoneIcon', 'ModuleType', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'name', 'description', 'image', 'sideMenuOrder', 'sideMenuEnable', 'homeMenuOrder', 'homeMenuEnable', 'topRightMenuOrder', 'topRightMenuEnable', 'tutorialImage', 'tutorialEnable', 'phoneTutorialImage', 'phoneTutorialEnable', 'phoneIcon', 'moduleType', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', ),
        self::TYPE_COLNAME       => array(AppMenuListTableMap::COL_ID, AppMenuListTableMap::COL_PROJECTID, AppMenuListTableMap::COL_NAME, AppMenuListTableMap::COL_DESCRIPTION, AppMenuListTableMap::COL_IMAGE, AppMenuListTableMap::COL_SIDEMENUORDER, AppMenuListTableMap::COL_SIDEMENUENABLE, AppMenuListTableMap::COL_HOMEMENUORDER, AppMenuListTableMap::COL_HOMEMENUENABLE, AppMenuListTableMap::COL_TOPRIGHTMENUORDER, AppMenuListTableMap::COL_TOPRIGHTMENUENABLE, AppMenuListTableMap::COL_TUTORIALIMAGE, AppMenuListTableMap::COL_TUTORIALENABLE, AppMenuListTableMap::COL_PHONETUTORIALIMAGE, AppMenuListTableMap::COL_PHONETUTORIALENABLE, AppMenuListTableMap::COL_PHONEICON, AppMenuListTableMap::COL_MODULE_TYPE, AppMenuListTableMap::COL_CREATED_BY, AppMenuListTableMap::COL_CREATED_DATE, AppMenuListTableMap::COL_MODIFIED_BY, AppMenuListTableMap::COL_MODIFIED_DATE, ),
        self::TYPE_FIELDNAME     => array('id', 'projectId', 'name', 'description', 'image', 'sideMenuOrder', 'sideMenuEnable', 'homeMenuOrder', 'homeMenuEnable', 'topRightMenuOrder', 'topRightMenuEnable', 'tutorialImage', 'tutorialEnable', 'phoneTutorialImage', 'phoneTutorialEnable', 'phoneIcon', 'module_type', 'created_by', 'created_date', 'modified_by', 'modified_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'Name' => 2, 'Description' => 3, 'Image' => 4, 'SideMenuOrder' => 5, 'SideMenuEnable' => 6, 'HomeMenuOrder' => 7, 'HomeMenuEnable' => 8, 'TopRightMenuOrder' => 9, 'TopRightMenuEnable' => 10, 'TutorialImage' => 11, 'TutorialEnable' => 12, 'PhoneTutorialImage' => 13, 'PhoneTutorialEnable' => 14, 'PhoneIcon' => 15, 'ModuleType' => 16, 'CreatedBy' => 17, 'CreatedDate' => 18, 'ModifiedBy' => 19, 'ModifiedDate' => 20, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'name' => 2, 'description' => 3, 'image' => 4, 'sideMenuOrder' => 5, 'sideMenuEnable' => 6, 'homeMenuOrder' => 7, 'homeMenuEnable' => 8, 'topRightMenuOrder' => 9, 'topRightMenuEnable' => 10, 'tutorialImage' => 11, 'tutorialEnable' => 12, 'phoneTutorialImage' => 13, 'phoneTutorialEnable' => 14, 'phoneIcon' => 15, 'moduleType' => 16, 'createdBy' => 17, 'createdDate' => 18, 'modifiedBy' => 19, 'modifiedDate' => 20, ),
        self::TYPE_COLNAME       => array(AppMenuListTableMap::COL_ID => 0, AppMenuListTableMap::COL_PROJECTID => 1, AppMenuListTableMap::COL_NAME => 2, AppMenuListTableMap::COL_DESCRIPTION => 3, AppMenuListTableMap::COL_IMAGE => 4, AppMenuListTableMap::COL_SIDEMENUORDER => 5, AppMenuListTableMap::COL_SIDEMENUENABLE => 6, AppMenuListTableMap::COL_HOMEMENUORDER => 7, AppMenuListTableMap::COL_HOMEMENUENABLE => 8, AppMenuListTableMap::COL_TOPRIGHTMENUORDER => 9, AppMenuListTableMap::COL_TOPRIGHTMENUENABLE => 10, AppMenuListTableMap::COL_TUTORIALIMAGE => 11, AppMenuListTableMap::COL_TUTORIALENABLE => 12, AppMenuListTableMap::COL_PHONETUTORIALIMAGE => 13, AppMenuListTableMap::COL_PHONETUTORIALENABLE => 14, AppMenuListTableMap::COL_PHONEICON => 15, AppMenuListTableMap::COL_MODULE_TYPE => 16, AppMenuListTableMap::COL_CREATED_BY => 17, AppMenuListTableMap::COL_CREATED_DATE => 18, AppMenuListTableMap::COL_MODIFIED_BY => 19, AppMenuListTableMap::COL_MODIFIED_DATE => 20, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'projectId' => 1, 'name' => 2, 'description' => 3, 'image' => 4, 'sideMenuOrder' => 5, 'sideMenuEnable' => 6, 'homeMenuOrder' => 7, 'homeMenuEnable' => 8, 'topRightMenuOrder' => 9, 'topRightMenuEnable' => 10, 'tutorialImage' => 11, 'tutorialEnable' => 12, 'phoneTutorialImage' => 13, 'phoneTutorialEnable' => 14, 'phoneIcon' => 15, 'module_type' => 16, 'created_by' => 17, 'created_date' => 18, 'modified_by' => 19, 'modified_date' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('appMenuList');
        $this->setPhpName('AppMenuList');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\AppMenuList');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 15, null);
        $this->addColumn('projectId', 'ProjectId', 'VARCHAR', true, 15, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 30, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 255, null);
        $this->addColumn('image', 'Image', 'VARCHAR', false, 255, null);
        $this->addColumn('sideMenuOrder', 'SideMenuOrder', 'INTEGER', false, 11, null);
        $this->addColumn('sideMenuEnable', 'SideMenuEnable', 'INTEGER', false, 1, 0);
        $this->addColumn('homeMenuOrder', 'HomeMenuOrder', 'INTEGER', false, 11, null);
        $this->addColumn('homeMenuEnable', 'HomeMenuEnable', 'INTEGER', false, 1, 0);
        $this->addColumn('topRightMenuOrder', 'TopRightMenuOrder', 'INTEGER', false, 11, null);
        $this->addColumn('topRightMenuEnable', 'TopRightMenuEnable', 'INTEGER', false, 1, 0);
        $this->addColumn('tutorialImage', 'TutorialImage', 'VARCHAR', false, 255, null);
        $this->addColumn('tutorialEnable', 'TutorialEnable', 'INTEGER', false, 1, 0);
        $this->addColumn('phoneTutorialImage', 'PhoneTutorialImage', 'VARCHAR', false, 255, null);
        $this->addColumn('phoneTutorialEnable', 'PhoneTutorialEnable', 'INTEGER', false, 1, 0);
        $this->addColumn('phoneIcon', 'PhoneIcon', 'VARCHAR', false, 255, null);
        $this->addColumn('module_type', 'ModuleType', 'INTEGER', false, null, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modified_by', 'ModifiedBy', 'VARCHAR', false, 10, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AppMenuListTableMap::CLASS_DEFAULT : AppMenuListTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AppMenuList object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AppMenuListTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AppMenuListTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AppMenuListTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AppMenuListTableMap::OM_CLASS;
            /** @var AppMenuList $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AppMenuListTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AppMenuListTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AppMenuListTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AppMenuList $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AppMenuListTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AppMenuListTableMap::COL_ID);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_PROJECTID);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_NAME);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_IMAGE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_SIDEMENUORDER);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_SIDEMENUENABLE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_HOMEMENUORDER);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_HOMEMENUENABLE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_TOPRIGHTMENUORDER);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_TOPRIGHTMENUENABLE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_TUTORIALIMAGE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_TUTORIALENABLE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_PHONETUTORIALIMAGE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_PHONETUTORIALENABLE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_PHONEICON);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_MODULE_TYPE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_MODIFIED_BY);
            $criteria->addSelectColumn(AppMenuListTableMap::COL_MODIFIED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.projectId');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.image');
            $criteria->addSelectColumn($alias . '.sideMenuOrder');
            $criteria->addSelectColumn($alias . '.sideMenuEnable');
            $criteria->addSelectColumn($alias . '.homeMenuOrder');
            $criteria->addSelectColumn($alias . '.homeMenuEnable');
            $criteria->addSelectColumn($alias . '.topRightMenuOrder');
            $criteria->addSelectColumn($alias . '.topRightMenuEnable');
            $criteria->addSelectColumn($alias . '.tutorialImage');
            $criteria->addSelectColumn($alias . '.tutorialEnable');
            $criteria->addSelectColumn($alias . '.phoneTutorialImage');
            $criteria->addSelectColumn($alias . '.phoneTutorialEnable');
            $criteria->addSelectColumn($alias . '.phoneIcon');
            $criteria->addSelectColumn($alias . '.module_type');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modified_by');
            $criteria->addSelectColumn($alias . '.modified_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AppMenuListTableMap::DATABASE_NAME)->getTable(AppMenuListTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AppMenuListTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AppMenuListTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AppMenuListTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AppMenuList or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AppMenuList object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppMenuListTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AppMenuList) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AppMenuListTableMap::DATABASE_NAME);
            $criteria->add(AppMenuListTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = AppMenuListQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AppMenuListTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AppMenuListTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the appMenuList table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AppMenuListQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AppMenuList or Criteria object.
     *
     * @param mixed               $criteria Criteria or AppMenuList object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppMenuListTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AppMenuList object
        }


        // Set the correct dbName
        $query = AppMenuListQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AppMenuListTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AppMenuListTableMap::buildTableMap();
