<?php

namespace Map;

use \CommonAreaCamera;
use \CommonAreaCameraQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'commonAreaCamera' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CommonAreaCameraTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.CommonAreaCameraTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'commonAreaCamera';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\CommonAreaCamera';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'CommonAreaCamera';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the id field
     */
    const COL_ID = 'commonAreaCamera.id';

    /**
     * the column name for the projectId field
     */
    const COL_PROJECTID = 'commonAreaCamera.projectId';

    /**
     * the column name for the location field
     */
    const COL_LOCATION = 'commonAreaCamera.location';

    /**
     * the column name for the cameraName field
     */
    const COL_CAMERANAME = 'commonAreaCamera.cameraName';

    /**
     * the column name for the protocol field
     */
    const COL_PROTOCOL = 'commonAreaCamera.protocol';

    /**
     * the column name for the ipAddress field
     */
    const COL_IPADDRESS = 'commonAreaCamera.ipAddress';

    /**
     * the column name for the port field
     */
    const COL_PORT = 'commonAreaCamera.port';

    /**
     * the column name for the cameraType field
     */
    const COL_CAMERATYPE = 'commonAreaCamera.cameraType';

    /**
     * the column name for the dvrUsername field
     */
    const COL_DVRUSERNAME = 'commonAreaCamera.dvrUsername';

    /**
     * the column name for the dvrPassword field
     */
    const COL_DVRPASSWORD = 'commonAreaCamera.dvrPassword';

    /**
     * the column name for the dvrChannel field
     */
    const COL_DVRCHANNEL = 'commonAreaCamera.dvrChannel';

    /**
     * the column name for the ftpPath field
     */
    const COL_FTPPATH = 'commonAreaCamera.ftpPath';

    /**
     * the column name for the displayOrder field
     */
    const COL_DISPLAYORDER = 'commonAreaCamera.displayOrder';

    /**
     * the column name for the enable field
     */
    const COL_ENABLE = 'commonAreaCamera.enable';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'commonAreaCamera.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'commonAreaCamera.created_date';

    /**
     * the column name for the modified_by field
     */
    const COL_MODIFIED_BY = 'commonAreaCamera.modified_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'commonAreaCamera.modified_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'Location', 'CameraName', 'Protocol', 'IpAddress', 'Port', 'CameraType', 'DvrUsername', 'DvrPassword', 'DvrChannel', 'FtpPath', 'DisplayOrder', 'Enable', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'location', 'cameraName', 'protocol', 'ipAddress', 'port', 'cameraType', 'dvrUsername', 'dvrPassword', 'dvrChannel', 'ftpPath', 'displayOrder', 'enable', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', ),
        self::TYPE_COLNAME       => array(CommonAreaCameraTableMap::COL_ID, CommonAreaCameraTableMap::COL_PROJECTID, CommonAreaCameraTableMap::COL_LOCATION, CommonAreaCameraTableMap::COL_CAMERANAME, CommonAreaCameraTableMap::COL_PROTOCOL, CommonAreaCameraTableMap::COL_IPADDRESS, CommonAreaCameraTableMap::COL_PORT, CommonAreaCameraTableMap::COL_CAMERATYPE, CommonAreaCameraTableMap::COL_DVRUSERNAME, CommonAreaCameraTableMap::COL_DVRPASSWORD, CommonAreaCameraTableMap::COL_DVRCHANNEL, CommonAreaCameraTableMap::COL_FTPPATH, CommonAreaCameraTableMap::COL_DISPLAYORDER, CommonAreaCameraTableMap::COL_ENABLE, CommonAreaCameraTableMap::COL_CREATED_BY, CommonAreaCameraTableMap::COL_CREATED_DATE, CommonAreaCameraTableMap::COL_MODIFIED_BY, CommonAreaCameraTableMap::COL_MODIFIED_DATE, ),
        self::TYPE_FIELDNAME     => array('id', 'projectId', 'location', 'cameraName', 'protocol', 'ipAddress', 'port', 'cameraType', 'dvrUsername', 'dvrPassword', 'dvrChannel', 'ftpPath', 'displayOrder', 'enable', 'created_by', 'created_date', 'modified_by', 'modified_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'Location' => 2, 'CameraName' => 3, 'Protocol' => 4, 'IpAddress' => 5, 'Port' => 6, 'CameraType' => 7, 'DvrUsername' => 8, 'DvrPassword' => 9, 'DvrChannel' => 10, 'FtpPath' => 11, 'DisplayOrder' => 12, 'Enable' => 13, 'CreatedBy' => 14, 'CreatedDate' => 15, 'ModifiedBy' => 16, 'ModifiedDate' => 17, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'location' => 2, 'cameraName' => 3, 'protocol' => 4, 'ipAddress' => 5, 'port' => 6, 'cameraType' => 7, 'dvrUsername' => 8, 'dvrPassword' => 9, 'dvrChannel' => 10, 'ftpPath' => 11, 'displayOrder' => 12, 'enable' => 13, 'createdBy' => 14, 'createdDate' => 15, 'modifiedBy' => 16, 'modifiedDate' => 17, ),
        self::TYPE_COLNAME       => array(CommonAreaCameraTableMap::COL_ID => 0, CommonAreaCameraTableMap::COL_PROJECTID => 1, CommonAreaCameraTableMap::COL_LOCATION => 2, CommonAreaCameraTableMap::COL_CAMERANAME => 3, CommonAreaCameraTableMap::COL_PROTOCOL => 4, CommonAreaCameraTableMap::COL_IPADDRESS => 5, CommonAreaCameraTableMap::COL_PORT => 6, CommonAreaCameraTableMap::COL_CAMERATYPE => 7, CommonAreaCameraTableMap::COL_DVRUSERNAME => 8, CommonAreaCameraTableMap::COL_DVRPASSWORD => 9, CommonAreaCameraTableMap::COL_DVRCHANNEL => 10, CommonAreaCameraTableMap::COL_FTPPATH => 11, CommonAreaCameraTableMap::COL_DISPLAYORDER => 12, CommonAreaCameraTableMap::COL_ENABLE => 13, CommonAreaCameraTableMap::COL_CREATED_BY => 14, CommonAreaCameraTableMap::COL_CREATED_DATE => 15, CommonAreaCameraTableMap::COL_MODIFIED_BY => 16, CommonAreaCameraTableMap::COL_MODIFIED_DATE => 17, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'projectId' => 1, 'location' => 2, 'cameraName' => 3, 'protocol' => 4, 'ipAddress' => 5, 'port' => 6, 'cameraType' => 7, 'dvrUsername' => 8, 'dvrPassword' => 9, 'dvrChannel' => 10, 'ftpPath' => 11, 'displayOrder' => 12, 'enable' => 13, 'created_by' => 14, 'created_date' => 15, 'modified_by' => 16, 'modified_date' => 17, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('commonAreaCamera');
        $this->setPhpName('CommonAreaCamera');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\CommonAreaCamera');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 15, null);
        $this->addColumn('projectId', 'ProjectId', 'VARCHAR', true, 15, null);
        $this->addColumn('location', 'Location', 'VARCHAR', true, 30, null);
        $this->addColumn('cameraName', 'CameraName', 'VARCHAR', true, 30, null);
        $this->addColumn('protocol', 'Protocol', 'VARCHAR', true, 8, null);
        $this->addColumn('ipAddress', 'IpAddress', 'VARCHAR', true, 255, null);
        $this->addColumn('port', 'Port', 'VARCHAR', true, 8, null);
        $this->addColumn('cameraType', 'CameraType', 'VARCHAR', true, 30, null);
        $this->addColumn('dvrUsername', 'DvrUsername', 'VARCHAR', false, 30, null);
        $this->addColumn('dvrPassword', 'DvrPassword', 'VARCHAR', false, 30, null);
        $this->addColumn('dvrChannel', 'DvrChannel', 'VARCHAR', false, 30, null);
        $this->addColumn('ftpPath', 'FtpPath', 'VARCHAR', false, 255, null);
        $this->addColumn('displayOrder', 'DisplayOrder', 'INTEGER', false, 11, null);
        $this->addColumn('enable', 'Enable', 'INTEGER', false, 1, 0);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modified_by', 'ModifiedBy', 'VARCHAR', false, 10, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CommonAreaCameraTableMap::CLASS_DEFAULT : CommonAreaCameraTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CommonAreaCamera object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CommonAreaCameraTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CommonAreaCameraTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CommonAreaCameraTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonAreaCameraTableMap::OM_CLASS;
            /** @var CommonAreaCamera $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CommonAreaCameraTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CommonAreaCameraTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CommonAreaCameraTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CommonAreaCamera $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonAreaCameraTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_ID);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_PROJECTID);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_LOCATION);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_CAMERANAME);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_PROTOCOL);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_IPADDRESS);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_PORT);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_CAMERATYPE);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_DVRUSERNAME);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_DVRPASSWORD);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_DVRCHANNEL);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_FTPPATH);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_DISPLAYORDER);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_ENABLE);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_MODIFIED_BY);
            $criteria->addSelectColumn(CommonAreaCameraTableMap::COL_MODIFIED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.projectId');
            $criteria->addSelectColumn($alias . '.location');
            $criteria->addSelectColumn($alias . '.cameraName');
            $criteria->addSelectColumn($alias . '.protocol');
            $criteria->addSelectColumn($alias . '.ipAddress');
            $criteria->addSelectColumn($alias . '.port');
            $criteria->addSelectColumn($alias . '.cameraType');
            $criteria->addSelectColumn($alias . '.dvrUsername');
            $criteria->addSelectColumn($alias . '.dvrPassword');
            $criteria->addSelectColumn($alias . '.dvrChannel');
            $criteria->addSelectColumn($alias . '.ftpPath');
            $criteria->addSelectColumn($alias . '.displayOrder');
            $criteria->addSelectColumn($alias . '.enable');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modified_by');
            $criteria->addSelectColumn($alias . '.modified_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CommonAreaCameraTableMap::DATABASE_NAME)->getTable(CommonAreaCameraTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CommonAreaCameraTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CommonAreaCameraTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CommonAreaCameraTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonAreaCamera or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CommonAreaCamera object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommonAreaCameraTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \CommonAreaCamera) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonAreaCameraTableMap::DATABASE_NAME);
            $criteria->add(CommonAreaCameraTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CommonAreaCameraQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CommonAreaCameraTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CommonAreaCameraTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the commonAreaCamera table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CommonAreaCameraQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CommonAreaCamera or Criteria object.
     *
     * @param mixed               $criteria Criteria or CommonAreaCamera object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommonAreaCameraTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CommonAreaCamera object
        }


        // Set the correct dbName
        $query = CommonAreaCameraQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CommonAreaCameraTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CommonAreaCameraTableMap::buildTableMap();
