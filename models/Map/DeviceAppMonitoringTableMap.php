<?php

namespace Map;

use \DeviceAppMonitoring;
use \DeviceAppMonitoringQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'deviceAppMonitoring' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class DeviceAppMonitoringTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.DeviceAppMonitoringTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'deviceAppMonitoring';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\DeviceAppMonitoring';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'DeviceAppMonitoring';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'deviceAppMonitoring.id';

    /**
     * the column name for the projectId field
     */
    const COL_PROJECTID = 'deviceAppMonitoring.projectId';

    /**
     * the column name for the deviceName field
     */
    const COL_DEVICENAME = 'deviceAppMonitoring.deviceName';

    /**
     * the column name for the refNo field
     */
    const COL_REFNO = 'deviceAppMonitoring.refNo';

    /**
     * the column name for the appName field
     */
    const COL_APPNAME = 'deviceAppMonitoring.appName';

    /**
     * the column name for the appVersion field
     */
    const COL_APPVERSION = 'deviceAppMonitoring.appVersion';

    /**
     * the column name for the appStatus field
     */
    const COL_APPSTATUS = 'deviceAppMonitoring.appStatus';

    /**
     * the column name for the remarks field
     */
    const COL_REMARKS = 'deviceAppMonitoring.remarks';

    /**
     * the column name for the lastUpdateDate field
     */
    const COL_LASTUPDATEDATE = 'deviceAppMonitoring.lastUpdateDate';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'DeviceName', 'RefNo', 'AppName', 'AppVersion', 'AppStatus', 'Remarks', 'LastUpdateDate', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'deviceName', 'refNo', 'appName', 'appVersion', 'appStatus', 'remarks', 'lastUpdateDate', ),
        self::TYPE_COLNAME       => array(DeviceAppMonitoringTableMap::COL_ID, DeviceAppMonitoringTableMap::COL_PROJECTID, DeviceAppMonitoringTableMap::COL_DEVICENAME, DeviceAppMonitoringTableMap::COL_REFNO, DeviceAppMonitoringTableMap::COL_APPNAME, DeviceAppMonitoringTableMap::COL_APPVERSION, DeviceAppMonitoringTableMap::COL_APPSTATUS, DeviceAppMonitoringTableMap::COL_REMARKS, DeviceAppMonitoringTableMap::COL_LASTUPDATEDATE, ),
        self::TYPE_FIELDNAME     => array('id', 'projectId', 'deviceName', 'refNo', 'appName', 'appVersion', 'appStatus', 'remarks', 'lastUpdateDate', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'DeviceName' => 2, 'RefNo' => 3, 'AppName' => 4, 'AppVersion' => 5, 'AppStatus' => 6, 'Remarks' => 7, 'LastUpdateDate' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'deviceName' => 2, 'refNo' => 3, 'appName' => 4, 'appVersion' => 5, 'appStatus' => 6, 'remarks' => 7, 'lastUpdateDate' => 8, ),
        self::TYPE_COLNAME       => array(DeviceAppMonitoringTableMap::COL_ID => 0, DeviceAppMonitoringTableMap::COL_PROJECTID => 1, DeviceAppMonitoringTableMap::COL_DEVICENAME => 2, DeviceAppMonitoringTableMap::COL_REFNO => 3, DeviceAppMonitoringTableMap::COL_APPNAME => 4, DeviceAppMonitoringTableMap::COL_APPVERSION => 5, DeviceAppMonitoringTableMap::COL_APPSTATUS => 6, DeviceAppMonitoringTableMap::COL_REMARKS => 7, DeviceAppMonitoringTableMap::COL_LASTUPDATEDATE => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'projectId' => 1, 'deviceName' => 2, 'refNo' => 3, 'appName' => 4, 'appVersion' => 5, 'appStatus' => 6, 'remarks' => 7, 'lastUpdateDate' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('deviceAppMonitoring');
        $this->setPhpName('DeviceAppMonitoring');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\DeviceAppMonitoring');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 15, null);
        $this->addColumn('projectId', 'ProjectId', 'VARCHAR', true, 15, null);
        $this->addColumn('deviceName', 'DeviceName', 'VARCHAR', false, 100, null);
        $this->addColumn('refNo', 'RefNo', 'VARCHAR', false, 100, null);
        $this->addColumn('appName', 'AppName', 'VARCHAR', false, 100, null);
        $this->addColumn('appVersion', 'AppVersion', 'VARCHAR', false, 10, null);
        $this->addColumn('appStatus', 'AppStatus', 'VARCHAR', false, 50, null);
        $this->addColumn('remarks', 'Remarks', 'LONGVARCHAR', false, null, null);
        $this->addColumn('lastUpdateDate', 'LastUpdateDate', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? DeviceAppMonitoringTableMap::CLASS_DEFAULT : DeviceAppMonitoringTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (DeviceAppMonitoring object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = DeviceAppMonitoringTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = DeviceAppMonitoringTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + DeviceAppMonitoringTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = DeviceAppMonitoringTableMap::OM_CLASS;
            /** @var DeviceAppMonitoring $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            DeviceAppMonitoringTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = DeviceAppMonitoringTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = DeviceAppMonitoringTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var DeviceAppMonitoring $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                DeviceAppMonitoringTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(DeviceAppMonitoringTableMap::COL_ID);
            $criteria->addSelectColumn(DeviceAppMonitoringTableMap::COL_PROJECTID);
            $criteria->addSelectColumn(DeviceAppMonitoringTableMap::COL_DEVICENAME);
            $criteria->addSelectColumn(DeviceAppMonitoringTableMap::COL_REFNO);
            $criteria->addSelectColumn(DeviceAppMonitoringTableMap::COL_APPNAME);
            $criteria->addSelectColumn(DeviceAppMonitoringTableMap::COL_APPVERSION);
            $criteria->addSelectColumn(DeviceAppMonitoringTableMap::COL_APPSTATUS);
            $criteria->addSelectColumn(DeviceAppMonitoringTableMap::COL_REMARKS);
            $criteria->addSelectColumn(DeviceAppMonitoringTableMap::COL_LASTUPDATEDATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.projectId');
            $criteria->addSelectColumn($alias . '.deviceName');
            $criteria->addSelectColumn($alias . '.refNo');
            $criteria->addSelectColumn($alias . '.appName');
            $criteria->addSelectColumn($alias . '.appVersion');
            $criteria->addSelectColumn($alias . '.appStatus');
            $criteria->addSelectColumn($alias . '.remarks');
            $criteria->addSelectColumn($alias . '.lastUpdateDate');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(DeviceAppMonitoringTableMap::DATABASE_NAME)->getTable(DeviceAppMonitoringTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(DeviceAppMonitoringTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(DeviceAppMonitoringTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new DeviceAppMonitoringTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a DeviceAppMonitoring or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or DeviceAppMonitoring object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceAppMonitoringTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \DeviceAppMonitoring) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(DeviceAppMonitoringTableMap::DATABASE_NAME);
            $criteria->add(DeviceAppMonitoringTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = DeviceAppMonitoringQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            DeviceAppMonitoringTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                DeviceAppMonitoringTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the deviceAppMonitoring table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return DeviceAppMonitoringQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a DeviceAppMonitoring or Criteria object.
     *
     * @param mixed               $criteria Criteria or DeviceAppMonitoring object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceAppMonitoringTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from DeviceAppMonitoring object
        }


        // Set the correct dbName
        $query = DeviceAppMonitoringQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // DeviceAppMonitoringTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
DeviceAppMonitoringTableMap::buildTableMap();
