<?php

namespace Map;

use \ChatroomSetting;
use \ChatroomSettingQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'chatroom_setting' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ChatroomSettingTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ChatroomSettingTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'chatroom_setting';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ChatroomSetting';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ChatroomSetting';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the chatroom_id field
     */
    const COL_CHATROOM_ID = 'chatroom_setting.chatroom_id';

    /**
     * the column name for the project_id field
     */
    const COL_PROJECT_ID = 'chatroom_setting.project_id';

    /**
     * the column name for the chatroom_expirydate field
     */
    const COL_CHATROOM_EXPIRYDATE = 'chatroom_setting.chatroom_expirydate';

    /**
     * the column name for the duration_enable field
     */
    const COL_DURATION_ENABLE = 'chatroom_setting.duration_enable';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'chatroom_setting.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'chatroom_setting.created_date';

    /**
     * the column name for the modifed_by field
     */
    const COL_MODIFED_BY = 'chatroom_setting.modifed_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'chatroom_setting.modified_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('ChatroomId', 'ProjectId', 'ChatroomExpirydate', 'DurationEnable', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', ),
        self::TYPE_CAMELNAME     => array('chatroomId', 'projectId', 'chatroomExpirydate', 'durationEnable', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', ),
        self::TYPE_COLNAME       => array(ChatroomSettingTableMap::COL_CHATROOM_ID, ChatroomSettingTableMap::COL_PROJECT_ID, ChatroomSettingTableMap::COL_CHATROOM_EXPIRYDATE, ChatroomSettingTableMap::COL_DURATION_ENABLE, ChatroomSettingTableMap::COL_CREATED_BY, ChatroomSettingTableMap::COL_CREATED_DATE, ChatroomSettingTableMap::COL_MODIFED_BY, ChatroomSettingTableMap::COL_MODIFIED_DATE, ),
        self::TYPE_FIELDNAME     => array('chatroom_id', 'project_id', 'chatroom_expirydate', 'duration_enable', 'created_by', 'created_date', 'modifed_by', 'modified_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('ChatroomId' => 0, 'ProjectId' => 1, 'ChatroomExpirydate' => 2, 'DurationEnable' => 3, 'CreatedBy' => 4, 'CreatedDate' => 5, 'ModifiedBy' => 6, 'ModifiedDate' => 7, ),
        self::TYPE_CAMELNAME     => array('chatroomId' => 0, 'projectId' => 1, 'chatroomExpirydate' => 2, 'durationEnable' => 3, 'createdBy' => 4, 'createdDate' => 5, 'modifiedBy' => 6, 'modifiedDate' => 7, ),
        self::TYPE_COLNAME       => array(ChatroomSettingTableMap::COL_CHATROOM_ID => 0, ChatroomSettingTableMap::COL_PROJECT_ID => 1, ChatroomSettingTableMap::COL_CHATROOM_EXPIRYDATE => 2, ChatroomSettingTableMap::COL_DURATION_ENABLE => 3, ChatroomSettingTableMap::COL_CREATED_BY => 4, ChatroomSettingTableMap::COL_CREATED_DATE => 5, ChatroomSettingTableMap::COL_MODIFED_BY => 6, ChatroomSettingTableMap::COL_MODIFIED_DATE => 7, ),
        self::TYPE_FIELDNAME     => array('chatroom_id' => 0, 'project_id' => 1, 'chatroom_expirydate' => 2, 'duration_enable' => 3, 'created_by' => 4, 'created_date' => 5, 'modifed_by' => 6, 'modified_date' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('chatroom_setting');
        $this->setPhpName('ChatroomSetting');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ChatroomSetting');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignKey('chatroom_id', 'ChatroomId', 'INTEGER', 'chatroom', 'id', true, null, null);
        $this->addColumn('project_id', 'ProjectId', 'VARCHAR', true, 5, null);
        $this->addColumn('chatroom_expirydate', 'ChatroomExpirydate', 'TIMESTAMP', true, null, null);
        $this->addColumn('duration_enable', 'DurationEnable', 'TINYINT', true, null, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 20, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modifed_by', 'ModifiedBy', 'VARCHAR', true, 20, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ChatRoom', '\\ChatRoom', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':chatroom_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return null;
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return '';
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ChatroomSettingTableMap::CLASS_DEFAULT : ChatroomSettingTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ChatroomSetting object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ChatroomSettingTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ChatroomSettingTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ChatroomSettingTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChatroomSettingTableMap::OM_CLASS;
            /** @var ChatroomSetting $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ChatroomSettingTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ChatroomSettingTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ChatroomSettingTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ChatroomSetting $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChatroomSettingTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChatroomSettingTableMap::COL_CHATROOM_ID);
            $criteria->addSelectColumn(ChatroomSettingTableMap::COL_PROJECT_ID);
            $criteria->addSelectColumn(ChatroomSettingTableMap::COL_CHATROOM_EXPIRYDATE);
            $criteria->addSelectColumn(ChatroomSettingTableMap::COL_DURATION_ENABLE);
            $criteria->addSelectColumn(ChatroomSettingTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(ChatroomSettingTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(ChatroomSettingTableMap::COL_MODIFED_BY);
            $criteria->addSelectColumn(ChatroomSettingTableMap::COL_MODIFIED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.chatroom_id');
            $criteria->addSelectColumn($alias . '.project_id');
            $criteria->addSelectColumn($alias . '.chatroom_expirydate');
            $criteria->addSelectColumn($alias . '.duration_enable');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modifed_by');
            $criteria->addSelectColumn($alias . '.modified_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ChatroomSettingTableMap::DATABASE_NAME)->getTable(ChatroomSettingTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ChatroomSettingTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ChatroomSettingTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ChatroomSettingTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ChatroomSetting or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChatroomSetting object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatroomSettingTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ChatroomSetting) { // it's a model object
            // create criteria based on pk value
            $criteria = $values->buildCriteria();
        } else { // it's a primary key, or an array of pks
            throw new LogicException('The ChatroomSetting object has no primary key');
        }

        $query = ChatroomSettingQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ChatroomSettingTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ChatroomSettingTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the chatroom_setting table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ChatroomSettingQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ChatroomSetting or Criteria object.
     *
     * @param mixed               $criteria Criteria or ChatroomSetting object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatroomSettingTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ChatroomSetting object
        }


        // Set the correct dbName
        $query = ChatroomSettingQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ChatroomSettingTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ChatroomSettingTableMap::buildTableMap();
