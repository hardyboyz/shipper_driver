<?php

namespace Map;

use \EBulletinRecipient;
use \EBulletinRecipientQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'ebulletin_recipient' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class EBulletinRecipientTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.EBulletinRecipientTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'ebulletin_recipient';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\EBulletinRecipient';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'EBulletinRecipient';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'ebulletin_recipient.id';

    /**
     * the column name for the eBulletinId field
     */
    const COL_EBULLETINID = 'ebulletin_recipient.eBulletinId';

    /**
     * the column name for the tenant_id field
     */
    const COL_TENANT_ID = 'ebulletin_recipient.tenant_id';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'ebulletin_recipient.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'ebulletin_recipient.created_date';

    /**
     * the column name for the modified_by field
     */
    const COL_MODIFIED_BY = 'ebulletin_recipient.modified_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'ebulletin_recipient.modified_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'EBulletinId', 'TenantId', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', ),
        self::TYPE_CAMELNAME     => array('id', 'eBulletinId', 'tenantId', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', ),
        self::TYPE_COLNAME       => array(EBulletinRecipientTableMap::COL_ID, EBulletinRecipientTableMap::COL_EBULLETINID, EBulletinRecipientTableMap::COL_TENANT_ID, EBulletinRecipientTableMap::COL_CREATED_BY, EBulletinRecipientTableMap::COL_CREATED_DATE, EBulletinRecipientTableMap::COL_MODIFIED_BY, EBulletinRecipientTableMap::COL_MODIFIED_DATE, ),
        self::TYPE_FIELDNAME     => array('id', 'eBulletinId', 'tenant_id', 'created_by', 'created_date', 'modified_by', 'modified_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'EBulletinId' => 1, 'TenantId' => 2, 'CreatedBy' => 3, 'CreatedDate' => 4, 'ModifiedBy' => 5, 'ModifiedDate' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'eBulletinId' => 1, 'tenantId' => 2, 'createdBy' => 3, 'createdDate' => 4, 'modifiedBy' => 5, 'modifiedDate' => 6, ),
        self::TYPE_COLNAME       => array(EBulletinRecipientTableMap::COL_ID => 0, EBulletinRecipientTableMap::COL_EBULLETINID => 1, EBulletinRecipientTableMap::COL_TENANT_ID => 2, EBulletinRecipientTableMap::COL_CREATED_BY => 3, EBulletinRecipientTableMap::COL_CREATED_DATE => 4, EBulletinRecipientTableMap::COL_MODIFIED_BY => 5, EBulletinRecipientTableMap::COL_MODIFIED_DATE => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'eBulletinId' => 1, 'tenant_id' => 2, 'created_by' => 3, 'created_date' => 4, 'modified_by' => 5, 'modified_date' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ebulletin_recipient');
        $this->setPhpName('EBulletinRecipient');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\EBulletinRecipient');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 15, null);
        $this->addForeignKey('eBulletinId', 'EBulletinId', 'VARCHAR', 'ebulletin', 'id', true, 15, null);
        $this->addColumn('tenant_id', 'TenantId', 'VARCHAR', true, 10, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modified_by', 'ModifiedBy', 'VARCHAR', false, 10, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('EBulletin', '\\EBulletin', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':eBulletinId',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? EBulletinRecipientTableMap::CLASS_DEFAULT : EBulletinRecipientTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (EBulletinRecipient object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = EBulletinRecipientTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = EBulletinRecipientTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + EBulletinRecipientTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EBulletinRecipientTableMap::OM_CLASS;
            /** @var EBulletinRecipient $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            EBulletinRecipientTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = EBulletinRecipientTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = EBulletinRecipientTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var EBulletinRecipient $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EBulletinRecipientTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EBulletinRecipientTableMap::COL_ID);
            $criteria->addSelectColumn(EBulletinRecipientTableMap::COL_EBULLETINID);
            $criteria->addSelectColumn(EBulletinRecipientTableMap::COL_TENANT_ID);
            $criteria->addSelectColumn(EBulletinRecipientTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(EBulletinRecipientTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(EBulletinRecipientTableMap::COL_MODIFIED_BY);
            $criteria->addSelectColumn(EBulletinRecipientTableMap::COL_MODIFIED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.eBulletinId');
            $criteria->addSelectColumn($alias . '.tenant_id');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modified_by');
            $criteria->addSelectColumn($alias . '.modified_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(EBulletinRecipientTableMap::DATABASE_NAME)->getTable(EBulletinRecipientTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(EBulletinRecipientTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(EBulletinRecipientTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new EBulletinRecipientTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a EBulletinRecipient or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or EBulletinRecipient object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EBulletinRecipientTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \EBulletinRecipient) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EBulletinRecipientTableMap::DATABASE_NAME);
            $criteria->add(EBulletinRecipientTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = EBulletinRecipientQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            EBulletinRecipientTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                EBulletinRecipientTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the ebulletin_recipient table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return EBulletinRecipientQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a EBulletinRecipient or Criteria object.
     *
     * @param mixed               $criteria Criteria or EBulletinRecipient object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EBulletinRecipientTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from EBulletinRecipient object
        }


        // Set the correct dbName
        $query = EBulletinRecipientQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // EBulletinRecipientTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
EBulletinRecipientTableMap::buildTableMap();
