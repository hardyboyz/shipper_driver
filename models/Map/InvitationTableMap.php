<?php

namespace Map;

use \Invitation;
use \InvitationQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'invitation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class InvitationTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.InvitationTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'invitation';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Invitation';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Invitation';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'invitation.id';

    /**
     * the column name for the project_id field
     */
    const COL_PROJECT_ID = 'invitation.project_id';

    /**
     * the column name for the chatroom_id field
     */
    const COL_CHATROOM_ID = 'invitation.chatroom_id';

    /**
     * the column name for the unit_id field
     */
    const COL_UNIT_ID = 'invitation.unit_id';

    /**
     * the column name for the group_type field
     */
    const COL_GROUP_TYPE = 'invitation.group_type';

    /**
     * the column name for the group_type_value field
     */
    const COL_GROUP_TYPE_VALUE = 'invitation.group_type_value';

    /**
     * the column name for the group_type_desc field
     */
    const COL_GROUP_TYPE_DESC = 'invitation.group_type_desc';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'invitation.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'invitation.created_date';

    /**
     * the column name for the modifed_by field
     */
    const COL_MODIFED_BY = 'invitation.modifed_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'invitation.modified_date';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'invitation.status';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'ChatroomId', 'UnitId', 'GroupType', 'GroupTypeValue', 'GroupTypeDesc', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', 'Status', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'chatroomId', 'unitId', 'groupType', 'groupTypeValue', 'groupTypeDesc', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', 'status', ),
        self::TYPE_COLNAME       => array(InvitationTableMap::COL_ID, InvitationTableMap::COL_PROJECT_ID, InvitationTableMap::COL_CHATROOM_ID, InvitationTableMap::COL_UNIT_ID, InvitationTableMap::COL_GROUP_TYPE, InvitationTableMap::COL_GROUP_TYPE_VALUE, InvitationTableMap::COL_GROUP_TYPE_DESC, InvitationTableMap::COL_CREATED_BY, InvitationTableMap::COL_CREATED_DATE, InvitationTableMap::COL_MODIFED_BY, InvitationTableMap::COL_MODIFIED_DATE, InvitationTableMap::COL_STATUS, ),
        self::TYPE_FIELDNAME     => array('id', 'project_id', 'chatroom_id', 'unit_id', 'group_type', 'group_type_value', 'group_type_desc', 'created_by', 'created_date', 'modifed_by', 'modified_date', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'ChatroomId' => 2, 'UnitId' => 3, 'GroupType' => 4, 'GroupTypeValue' => 5, 'GroupTypeDesc' => 6, 'CreatedBy' => 7, 'CreatedDate' => 8, 'ModifiedBy' => 9, 'ModifiedDate' => 10, 'Status' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'chatroomId' => 2, 'unitId' => 3, 'groupType' => 4, 'groupTypeValue' => 5, 'groupTypeDesc' => 6, 'createdBy' => 7, 'createdDate' => 8, 'modifiedBy' => 9, 'modifiedDate' => 10, 'status' => 11, ),
        self::TYPE_COLNAME       => array(InvitationTableMap::COL_ID => 0, InvitationTableMap::COL_PROJECT_ID => 1, InvitationTableMap::COL_CHATROOM_ID => 2, InvitationTableMap::COL_UNIT_ID => 3, InvitationTableMap::COL_GROUP_TYPE => 4, InvitationTableMap::COL_GROUP_TYPE_VALUE => 5, InvitationTableMap::COL_GROUP_TYPE_DESC => 6, InvitationTableMap::COL_CREATED_BY => 7, InvitationTableMap::COL_CREATED_DATE => 8, InvitationTableMap::COL_MODIFED_BY => 9, InvitationTableMap::COL_MODIFIED_DATE => 10, InvitationTableMap::COL_STATUS => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'project_id' => 1, 'chatroom_id' => 2, 'unit_id' => 3, 'group_type' => 4, 'group_type_value' => 5, 'group_type_desc' => 6, 'created_by' => 7, 'created_date' => 8, 'modifed_by' => 9, 'modified_date' => 10, 'status' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('invitation');
        $this->setPhpName('Invitation');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Invitation');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('project_id', 'ProjectId', 'VARCHAR', true, 5, null);
        $this->addColumn('chatroom_id', 'ChatroomId', 'VARCHAR', true, 20, null);
        $this->addColumn('unit_id', 'UnitId', 'VARCHAR', true, 15, null);
        $this->addColumn('group_type', 'GroupType', 'VARCHAR', true, 20, null);
        $this->addColumn('group_type_value', 'GroupTypeValue', 'VARCHAR', true, 50, null);
        $this->addColumn('group_type_desc', 'GroupTypeDesc', 'VARCHAR', true, 255, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 20, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modifed_by', 'ModifiedBy', 'VARCHAR', true, 20, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('status', 'Status', 'INTEGER', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? InvitationTableMap::CLASS_DEFAULT : InvitationTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Invitation object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = InvitationTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = InvitationTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + InvitationTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = InvitationTableMap::OM_CLASS;
            /** @var Invitation $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            InvitationTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = InvitationTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = InvitationTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Invitation $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                InvitationTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(InvitationTableMap::COL_ID);
            $criteria->addSelectColumn(InvitationTableMap::COL_PROJECT_ID);
            $criteria->addSelectColumn(InvitationTableMap::COL_CHATROOM_ID);
            $criteria->addSelectColumn(InvitationTableMap::COL_UNIT_ID);
            $criteria->addSelectColumn(InvitationTableMap::COL_GROUP_TYPE);
            $criteria->addSelectColumn(InvitationTableMap::COL_GROUP_TYPE_VALUE);
            $criteria->addSelectColumn(InvitationTableMap::COL_GROUP_TYPE_DESC);
            $criteria->addSelectColumn(InvitationTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(InvitationTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(InvitationTableMap::COL_MODIFED_BY);
            $criteria->addSelectColumn(InvitationTableMap::COL_MODIFIED_DATE);
            $criteria->addSelectColumn(InvitationTableMap::COL_STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.project_id');
            $criteria->addSelectColumn($alias . '.chatroom_id');
            $criteria->addSelectColumn($alias . '.unit_id');
            $criteria->addSelectColumn($alias . '.group_type');
            $criteria->addSelectColumn($alias . '.group_type_value');
            $criteria->addSelectColumn($alias . '.group_type_desc');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modifed_by');
            $criteria->addSelectColumn($alias . '.modified_date');
            $criteria->addSelectColumn($alias . '.status');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(InvitationTableMap::DATABASE_NAME)->getTable(InvitationTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(InvitationTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(InvitationTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new InvitationTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Invitation or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Invitation object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InvitationTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Invitation) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(InvitationTableMap::DATABASE_NAME);
            $criteria->add(InvitationTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = InvitationQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            InvitationTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                InvitationTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the invitation table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return InvitationQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Invitation or Criteria object.
     *
     * @param mixed               $criteria Criteria or Invitation object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(InvitationTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Invitation object
        }

        if ($criteria->containsKey(InvitationTableMap::COL_ID) && $criteria->keyContainsValue(InvitationTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.InvitationTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = InvitationQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // InvitationTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
InvitationTableMap::buildTableMap();
