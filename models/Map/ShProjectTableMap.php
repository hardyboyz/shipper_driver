<?php

namespace Map;

use \ShProject;
use \ShProjectQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'sh_project' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ShProjectTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ShProjectTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'sh_project';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ShProject';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ShProject';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'sh_project.id';

    /**
     * the column name for the project_id field
     */
    const COL_PROJECT_ID = 'sh_project.project_id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'sh_project.name';

    /**
     * the column name for the gatewayProtocol field
     */
    const COL_GATEWAYPROTOCOL = 'sh_project.gatewayProtocol';

    /**
     * the column name for the gatewayURL field
     */
    const COL_GATEWAYURL = 'sh_project.gatewayURL';

    /**
     * the column name for the gatewayPort field
     */
    const COL_GATEWAYPORT = 'sh_project.gatewayPort';

    /**
     * the column name for the ssid field
     */
    const COL_SSID = 'sh_project.ssid';

    /**
     * the column name for the publicIP field
     */
    const COL_PUBLICIP = 'sh_project.publicIP';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'Name', 'GatewayProtocol', 'GatewayURL', 'GatewayPort', 'SSID', 'PublicIP', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'name', 'gatewayProtocol', 'gatewayURL', 'gatewayPort', 'sSID', 'publicIP', ),
        self::TYPE_COLNAME       => array(ShProjectTableMap::COL_ID, ShProjectTableMap::COL_PROJECT_ID, ShProjectTableMap::COL_NAME, ShProjectTableMap::COL_GATEWAYPROTOCOL, ShProjectTableMap::COL_GATEWAYURL, ShProjectTableMap::COL_GATEWAYPORT, ShProjectTableMap::COL_SSID, ShProjectTableMap::COL_PUBLICIP, ),
        self::TYPE_FIELDNAME     => array('id', 'project_id', 'name', 'gatewayProtocol', 'gatewayURL', 'gatewayPort', 'ssid', 'publicIP', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'Name' => 2, 'GatewayProtocol' => 3, 'GatewayURL' => 4, 'GatewayPort' => 5, 'SSID' => 6, 'PublicIP' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'name' => 2, 'gatewayProtocol' => 3, 'gatewayURL' => 4, 'gatewayPort' => 5, 'sSID' => 6, 'publicIP' => 7, ),
        self::TYPE_COLNAME       => array(ShProjectTableMap::COL_ID => 0, ShProjectTableMap::COL_PROJECT_ID => 1, ShProjectTableMap::COL_NAME => 2, ShProjectTableMap::COL_GATEWAYPROTOCOL => 3, ShProjectTableMap::COL_GATEWAYURL => 4, ShProjectTableMap::COL_GATEWAYPORT => 5, ShProjectTableMap::COL_SSID => 6, ShProjectTableMap::COL_PUBLICIP => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'project_id' => 1, 'name' => 2, 'gatewayProtocol' => 3, 'gatewayURL' => 4, 'gatewayPort' => 5, 'ssid' => 6, 'publicIP' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sh_project');
        $this->setPhpName('ShProject');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ShProject');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 4, null);
        $this->addColumn('project_id', 'ProjectId', 'VARCHAR', true, 10, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 50, null);
        $this->addColumn('gatewayProtocol', 'GatewayProtocol', 'VARCHAR', true, 5, null);
        $this->addColumn('gatewayURL', 'GatewayURL', 'VARCHAR', true, 250, null);
        $this->addColumn('gatewayPort', 'GatewayPort', 'VARCHAR', true, 5, null);
        $this->addColumn('ssid', 'SSID', 'VARCHAR', false, 100, null);
        $this->addColumn('publicIP', 'PublicIP', 'VARCHAR', false, 50, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ShProjectTableMap::CLASS_DEFAULT : ShProjectTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ShProject object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ShProjectTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ShProjectTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ShProjectTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ShProjectTableMap::OM_CLASS;
            /** @var ShProject $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ShProjectTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ShProjectTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ShProjectTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ShProject $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ShProjectTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ShProjectTableMap::COL_ID);
            $criteria->addSelectColumn(ShProjectTableMap::COL_PROJECT_ID);
            $criteria->addSelectColumn(ShProjectTableMap::COL_NAME);
            $criteria->addSelectColumn(ShProjectTableMap::COL_GATEWAYPROTOCOL);
            $criteria->addSelectColumn(ShProjectTableMap::COL_GATEWAYURL);
            $criteria->addSelectColumn(ShProjectTableMap::COL_GATEWAYPORT);
            $criteria->addSelectColumn(ShProjectTableMap::COL_SSID);
            $criteria->addSelectColumn(ShProjectTableMap::COL_PUBLICIP);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.project_id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.gatewayProtocol');
            $criteria->addSelectColumn($alias . '.gatewayURL');
            $criteria->addSelectColumn($alias . '.gatewayPort');
            $criteria->addSelectColumn($alias . '.ssid');
            $criteria->addSelectColumn($alias . '.publicIP');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ShProjectTableMap::DATABASE_NAME)->getTable(ShProjectTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ShProjectTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ShProjectTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ShProjectTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ShProject or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ShProject object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShProjectTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ShProject) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ShProjectTableMap::DATABASE_NAME);
            $criteria->add(ShProjectTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ShProjectQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ShProjectTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ShProjectTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the sh_project table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ShProjectQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ShProject or Criteria object.
     *
     * @param mixed               $criteria Criteria or ShProject object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShProjectTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ShProject object
        }

        if ($criteria->containsKey(ShProjectTableMap::COL_ID) && $criteria->keyContainsValue(ShProjectTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ShProjectTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ShProjectQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ShProjectTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ShProjectTableMap::buildTableMap();
