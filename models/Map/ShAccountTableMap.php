<?php

namespace Map;

use \ShAccount;
use \ShAccountQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'sh_account' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ShAccountTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ShAccountTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'sh_account';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ShAccount';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ShAccount';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 19;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 19;

    /**
     * the column name for the id field
     */
    const COL_ID = 'sh_account.id';

    /**
     * the column name for the photo field
     */
    const COL_PHOTO = 'sh_account.photo';

    /**
     * the column name for the dob field
     */
    const COL_DOB = 'sh_account.dob';

    /**
     * the column name for the fullName field
     */
    const COL_FULLNAME = 'sh_account.fullName';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'sh_account.phone';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'sh_account.email';

    /**
     * the column name for the address field
     */
    const COL_ADDRESS = 'sh_account.address';

    /**
     * the column name for the idcardnumber field
     */
    const COL_IDCARDNUMBER = 'sh_account.idcardnumber';

    /**
     * the column name for the hashPassword field
     */
    const COL_HASHPASSWORD = 'sh_account.hashPassword';

    /**
     * the column name for the salt field
     */
    const COL_SALT = 'sh_account.salt';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'sh_account.created_by';

    /**
     * the column name for the createTS field
     */
    const COL_CREATETS = 'sh_account.createTS';

    /**
     * the column name for the pushID field
     */
    const COL_PUSHID = 'sh_account.pushID';

    /**
     * the column name for the deviceID field
     */
    const COL_DEVICEID = 'sh_account.deviceID';

    /**
     * the column name for the enable_pn field
     */
    const COL_ENABLE_PN = 'sh_account.enable_pn';

    /**
     * the column name for the login_ts field
     */
    const COL_LOGIN_TS = 'sh_account.login_ts';

    /**
     * the column name for the expire_ts field
     */
    const COL_EXPIRE_TS = 'sh_account.expire_ts';

    /**
     * the column name for the token field
     */
    const COL_TOKEN = 'sh_account.token';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'sh_account.status';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('ID', 'Photo', 'Dob', 'FullName', 'Phone', 'Email', 'Address', 'IdcardNumber', 'HashPassword', 'Salt', 'CreatedBy', 'CreateTS', 'PushID', 'DeviceID', 'EnablePN', 'LoginTs', 'ExpireTs', 'Token', 'Status', ),
        self::TYPE_CAMELNAME     => array('iD', 'photo', 'dob', 'fullName', 'phone', 'email', 'address', 'idcardNumber', 'hashPassword', 'salt', 'createdBy', 'createTS', 'pushID', 'deviceID', 'enablePN', 'loginTs', 'expireTs', 'token', 'status', ),
        self::TYPE_COLNAME       => array(ShAccountTableMap::COL_ID, ShAccountTableMap::COL_PHOTO, ShAccountTableMap::COL_DOB, ShAccountTableMap::COL_FULLNAME, ShAccountTableMap::COL_PHONE, ShAccountTableMap::COL_EMAIL, ShAccountTableMap::COL_ADDRESS, ShAccountTableMap::COL_IDCARDNUMBER, ShAccountTableMap::COL_HASHPASSWORD, ShAccountTableMap::COL_SALT, ShAccountTableMap::COL_CREATED_BY, ShAccountTableMap::COL_CREATETS, ShAccountTableMap::COL_PUSHID, ShAccountTableMap::COL_DEVICEID, ShAccountTableMap::COL_ENABLE_PN, ShAccountTableMap::COL_LOGIN_TS, ShAccountTableMap::COL_EXPIRE_TS, ShAccountTableMap::COL_TOKEN, ShAccountTableMap::COL_STATUS, ),
        self::TYPE_FIELDNAME     => array('id', 'photo', 'dob', 'fullName', 'phone', 'email', 'address', 'idcardnumber', 'hashPassword', 'salt', 'created_by', 'createTS', 'pushID', 'deviceID', 'enable_pn', 'login_ts', 'expire_ts', 'token', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('ID' => 0, 'Photo' => 1, 'Dob' => 2, 'FullName' => 3, 'Phone' => 4, 'Email' => 5, 'Address' => 6, 'IdcardNumber' => 7, 'HashPassword' => 8, 'Salt' => 9, 'CreatedBy' => 10, 'CreateTS' => 11, 'PushID' => 12, 'DeviceID' => 13, 'EnablePN' => 14, 'LoginTs' => 15, 'ExpireTs' => 16, 'Token' => 17, 'Status' => 18, ),
        self::TYPE_CAMELNAME     => array('iD' => 0, 'photo' => 1, 'dob' => 2, 'fullName' => 3, 'phone' => 4, 'email' => 5, 'address' => 6, 'idcardNumber' => 7, 'hashPassword' => 8, 'salt' => 9, 'createdBy' => 10, 'createTS' => 11, 'pushID' => 12, 'deviceID' => 13, 'enablePN' => 14, 'loginTs' => 15, 'expireTs' => 16, 'token' => 17, 'status' => 18, ),
        self::TYPE_COLNAME       => array(ShAccountTableMap::COL_ID => 0, ShAccountTableMap::COL_PHOTO => 1, ShAccountTableMap::COL_DOB => 2, ShAccountTableMap::COL_FULLNAME => 3, ShAccountTableMap::COL_PHONE => 4, ShAccountTableMap::COL_EMAIL => 5, ShAccountTableMap::COL_ADDRESS => 6, ShAccountTableMap::COL_IDCARDNUMBER => 7, ShAccountTableMap::COL_HASHPASSWORD => 8, ShAccountTableMap::COL_SALT => 9, ShAccountTableMap::COL_CREATED_BY => 10, ShAccountTableMap::COL_CREATETS => 11, ShAccountTableMap::COL_PUSHID => 12, ShAccountTableMap::COL_DEVICEID => 13, ShAccountTableMap::COL_ENABLE_PN => 14, ShAccountTableMap::COL_LOGIN_TS => 15, ShAccountTableMap::COL_EXPIRE_TS => 16, ShAccountTableMap::COL_TOKEN => 17, ShAccountTableMap::COL_STATUS => 18, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'photo' => 1, 'dob' => 2, 'fullName' => 3, 'phone' => 4, 'email' => 5, 'address' => 6, 'idcardnumber' => 7, 'hashPassword' => 8, 'salt' => 9, 'created_by' => 10, 'createTS' => 11, 'pushID' => 12, 'deviceID' => 13, 'enable_pn' => 14, 'login_ts' => 15, 'expire_ts' => 16, 'token' => 17, 'status' => 18, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sh_account');
        $this->setPhpName('ShAccount');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ShAccount');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'ID', 'VARCHAR', true, 20, null);
        $this->addColumn('photo', 'Photo', 'VARCHAR', true, 35, null);
        $this->addColumn('dob', 'Dob', 'VARCHAR', true, 15, null);
        $this->addColumn('fullName', 'FullName', 'VARCHAR', true, 50, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', true, 50, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 50, null);
        $this->addColumn('address', 'Address', 'VARCHAR', true, 100, null);
        $this->addColumn('idcardnumber', 'IdcardNumber', 'VARCHAR', true, 25, null);
        $this->addColumn('hashPassword', 'HashPassword', 'VARCHAR', true, 100, null);
        $this->addColumn('salt', 'Salt', 'VARCHAR', true, 100, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('createTS', 'CreateTS', 'TIMESTAMP', true, null, null);
        $this->addColumn('pushID', 'PushID', 'VARCHAR', false, 250, null);
        $this->addColumn('deviceID', 'DeviceID', 'VARCHAR', false, 100, null);
        $this->addColumn('enable_pn', 'EnablePN', 'INTEGER', true, null, 1);
        $this->addColumn('login_ts', 'LoginTs', 'TIMESTAMP', true, null, null);
        $this->addColumn('expire_ts', 'ExpireTs', 'TIMESTAMP', true, null, null);
        $this->addColumn('token', 'Token', 'VARCHAR', true, 100, null);
        $this->addColumn('status', 'Status', 'INTEGER', true, null, 1);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ID', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ID', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ID', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ID', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ID', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('ID', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('ID', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }
    
    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ShAccountTableMap::CLASS_DEFAULT : ShAccountTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ShAccount object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ShAccountTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ShAccountTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ShAccountTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ShAccountTableMap::OM_CLASS;
            /** @var ShAccount $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ShAccountTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ShAccountTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ShAccountTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ShAccount $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ShAccountTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ShAccountTableMap::COL_ID);
            $criteria->addSelectColumn(ShAccountTableMap::COL_PHOTO);
            $criteria->addSelectColumn(ShAccountTableMap::COL_DOB);
            $criteria->addSelectColumn(ShAccountTableMap::COL_FULLNAME);
            $criteria->addSelectColumn(ShAccountTableMap::COL_PHONE);
            $criteria->addSelectColumn(ShAccountTableMap::COL_EMAIL);
            $criteria->addSelectColumn(ShAccountTableMap::COL_ADDRESS);
            $criteria->addSelectColumn(ShAccountTableMap::COL_IDCARDNUMBER);
            $criteria->addSelectColumn(ShAccountTableMap::COL_HASHPASSWORD);
            $criteria->addSelectColumn(ShAccountTableMap::COL_SALT);
            $criteria->addSelectColumn(ShAccountTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(ShAccountTableMap::COL_CREATETS);
            $criteria->addSelectColumn(ShAccountTableMap::COL_PUSHID);
            $criteria->addSelectColumn(ShAccountTableMap::COL_DEVICEID);
            $criteria->addSelectColumn(ShAccountTableMap::COL_ENABLE_PN);
            $criteria->addSelectColumn(ShAccountTableMap::COL_LOGIN_TS);
            $criteria->addSelectColumn(ShAccountTableMap::COL_EXPIRE_TS);
            $criteria->addSelectColumn(ShAccountTableMap::COL_TOKEN);
            $criteria->addSelectColumn(ShAccountTableMap::COL_STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.photo');
            $criteria->addSelectColumn($alias . '.dob');
            $criteria->addSelectColumn($alias . '.fullName');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.address');
            $criteria->addSelectColumn($alias . '.idcardnumber');
            $criteria->addSelectColumn($alias . '.hashPassword');
            $criteria->addSelectColumn($alias . '.salt');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.createTS');
            $criteria->addSelectColumn($alias . '.pushID');
            $criteria->addSelectColumn($alias . '.deviceID');
            $criteria->addSelectColumn($alias . '.enable_pn');
            $criteria->addSelectColumn($alias . '.login_ts');
            $criteria->addSelectColumn($alias . '.expire_ts');
            $criteria->addSelectColumn($alias . '.token');
            $criteria->addSelectColumn($alias . '.status');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ShAccountTableMap::DATABASE_NAME)->getTable(ShAccountTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ShAccountTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ShAccountTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ShAccountTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ShAccount or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ShAccount object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShAccountTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ShAccount) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ShAccountTableMap::DATABASE_NAME);
            $criteria->add(ShAccountTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ShAccountQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ShAccountTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ShAccountTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the sh_account table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ShAccountQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ShAccount or Criteria object.
     *
     * @param mixed               $criteria Criteria or ShAccount object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShAccountTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ShAccount object
        }


        // Set the correct dbName
        $query = ShAccountQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ShAccountTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ShAccountTableMap::buildTableMap();
