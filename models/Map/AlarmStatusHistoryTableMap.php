<?php

namespace Map;

use \AlarmStatusHistory;
use \AlarmStatusHistoryQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'alarmStatusHistory' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AlarmStatusHistoryTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AlarmStatusHistoryTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'alarmStatusHistory';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AlarmStatusHistory';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AlarmStatusHistory';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'alarmStatusHistory.id';

    /**
     * the column name for the projectId field
     */
    const COL_PROJECTID = 'alarmStatusHistory.projectId';

    /**
     * the column name for the unitId field
     */
    const COL_UNITID = 'alarmStatusHistory.unitId';

    /**
     * the column name for the zoneId field
     */
    const COL_ZONEID = 'alarmStatusHistory.zoneId';

    /**
     * the column name for the zoneName field
     */
    const COL_ZONENAME = 'alarmStatusHistory.zoneName';

    /**
     * the column name for the zoneType field
     */
    const COL_ZONETYPE = 'alarmStatusHistory.zoneType';

    /**
     * the column name for the areaId field
     */
    const COL_AREAID = 'alarmStatusHistory.areaId';

    /**
     * the column name for the areaName field
     */
    const COL_AREANAME = 'alarmStatusHistory.areaName';

    /**
     * the column name for the triggered_date field
     */
    const COL_TRIGGERED_DATE = 'alarmStatusHistory.triggered_date';

    /**
     * the column name for the acknowledged_date field
     */
    const COL_ACKNOWLEDGED_DATE = 'alarmStatusHistory.acknowledged_date';

    /**
     * the column name for the acknowledged_by field
     */
    const COL_ACKNOWLEDGED_BY = 'alarmStatusHistory.acknowledged_by';

    /**
     * the column name for the remarks field
     */
    const COL_REMARKS = 'alarmStatusHistory.remarks';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'UnitId', 'ZoneId', 'ZoneName', 'ZoneType', 'AreaId', 'AreaName', 'TriggeredDate', 'AcknowledgedDate', 'AcknowledgedBy', 'Remarks', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'unitId', 'zoneId', 'zoneName', 'zoneType', 'areaId', 'areaName', 'triggeredDate', 'acknowledgedDate', 'acknowledgedBy', 'remarks', ),
        self::TYPE_COLNAME       => array(AlarmStatusHistoryTableMap::COL_ID, AlarmStatusHistoryTableMap::COL_PROJECTID, AlarmStatusHistoryTableMap::COL_UNITID, AlarmStatusHistoryTableMap::COL_ZONEID, AlarmStatusHistoryTableMap::COL_ZONENAME, AlarmStatusHistoryTableMap::COL_ZONETYPE, AlarmStatusHistoryTableMap::COL_AREAID, AlarmStatusHistoryTableMap::COL_AREANAME, AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE, AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE, AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_BY, AlarmStatusHistoryTableMap::COL_REMARKS, ),
        self::TYPE_FIELDNAME     => array('id', 'projectId', 'unitId', 'zoneId', 'zoneName', 'zoneType', 'areaId', 'areaName', 'triggered_date', 'acknowledged_date', 'acknowledged_by', 'remarks', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'UnitId' => 2, 'ZoneId' => 3, 'ZoneName' => 4, 'ZoneType' => 5, 'AreaId' => 6, 'AreaName' => 7, 'TriggeredDate' => 8, 'AcknowledgedDate' => 9, 'AcknowledgedBy' => 10, 'Remarks' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'unitId' => 2, 'zoneId' => 3, 'zoneName' => 4, 'zoneType' => 5, 'areaId' => 6, 'areaName' => 7, 'triggeredDate' => 8, 'acknowledgedDate' => 9, 'acknowledgedBy' => 10, 'remarks' => 11, ),
        self::TYPE_COLNAME       => array(AlarmStatusHistoryTableMap::COL_ID => 0, AlarmStatusHistoryTableMap::COL_PROJECTID => 1, AlarmStatusHistoryTableMap::COL_UNITID => 2, AlarmStatusHistoryTableMap::COL_ZONEID => 3, AlarmStatusHistoryTableMap::COL_ZONENAME => 4, AlarmStatusHistoryTableMap::COL_ZONETYPE => 5, AlarmStatusHistoryTableMap::COL_AREAID => 6, AlarmStatusHistoryTableMap::COL_AREANAME => 7, AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE => 8, AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE => 9, AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_BY => 10, AlarmStatusHistoryTableMap::COL_REMARKS => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'projectId' => 1, 'unitId' => 2, 'zoneId' => 3, 'zoneName' => 4, 'zoneType' => 5, 'areaId' => 6, 'areaName' => 7, 'triggered_date' => 8, 'acknowledged_date' => 9, 'acknowledged_by' => 10, 'remarks' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('alarmStatusHistory');
        $this->setPhpName('AlarmStatusHistory');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\AlarmStatusHistory');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 15, null);
        $this->addColumn('projectId', 'ProjectId', 'VARCHAR', true, 15, null);
        $this->addColumn('unitId', 'UnitId', 'VARCHAR', true, 15, null);
        $this->addColumn('zoneId', 'ZoneId', 'VARCHAR', false, 15, null);
        $this->addColumn('zoneName', 'ZoneName', 'VARCHAR', false, 15, null);
        $this->addColumn('zoneType', 'ZoneType', 'VARCHAR', false, 15, null);
        $this->addColumn('areaId', 'AreaId', 'VARCHAR', false, 15, null);
        $this->addColumn('areaName', 'AreaName', 'VARCHAR', false, 15, null);
        $this->addColumn('triggered_date', 'TriggeredDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('acknowledged_date', 'AcknowledgedDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('acknowledged_by', 'AcknowledgedBy', 'VARCHAR', false, 15, null);
        $this->addColumn('remarks', 'Remarks', 'VARCHAR', false, 100, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AlarmStatusHistoryTableMap::CLASS_DEFAULT : AlarmStatusHistoryTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AlarmStatusHistory object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AlarmStatusHistoryTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AlarmStatusHistoryTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AlarmStatusHistoryTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AlarmStatusHistoryTableMap::OM_CLASS;
            /** @var AlarmStatusHistory $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AlarmStatusHistoryTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AlarmStatusHistoryTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AlarmStatusHistoryTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AlarmStatusHistory $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AlarmStatusHistoryTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_ID);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_PROJECTID);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_UNITID);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_ZONEID);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_ZONENAME);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_ZONETYPE);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_AREAID);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_AREANAME);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_TRIGGERED_DATE);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_DATE);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_ACKNOWLEDGED_BY);
            $criteria->addSelectColumn(AlarmStatusHistoryTableMap::COL_REMARKS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.projectId');
            $criteria->addSelectColumn($alias . '.unitId');
            $criteria->addSelectColumn($alias . '.zoneId');
            $criteria->addSelectColumn($alias . '.zoneName');
            $criteria->addSelectColumn($alias . '.zoneType');
            $criteria->addSelectColumn($alias . '.areaId');
            $criteria->addSelectColumn($alias . '.areaName');
            $criteria->addSelectColumn($alias . '.triggered_date');
            $criteria->addSelectColumn($alias . '.acknowledged_date');
            $criteria->addSelectColumn($alias . '.acknowledged_by');
            $criteria->addSelectColumn($alias . '.remarks');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AlarmStatusHistoryTableMap::DATABASE_NAME)->getTable(AlarmStatusHistoryTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AlarmStatusHistoryTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AlarmStatusHistoryTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AlarmStatusHistoryTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AlarmStatusHistory or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AlarmStatusHistory object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusHistoryTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AlarmStatusHistory) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AlarmStatusHistoryTableMap::DATABASE_NAME);
            $criteria->add(AlarmStatusHistoryTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = AlarmStatusHistoryQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AlarmStatusHistoryTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AlarmStatusHistoryTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the alarmStatusHistory table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AlarmStatusHistoryQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AlarmStatusHistory or Criteria object.
     *
     * @param mixed               $criteria Criteria or AlarmStatusHistory object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusHistoryTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AlarmStatusHistory object
        }


        // Set the correct dbName
        $query = AlarmStatusHistoryQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AlarmStatusHistoryTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AlarmStatusHistoryTableMap::buildTableMap();
