<?php

namespace Map;

use \Tenant;
use \TenantQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'tenant' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TenantTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.TenantTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'tenant';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Tenant';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Tenant';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the id field
     */
    const COL_ID = 'tenant.id';

    /**
     * the column name for the projectId field
     */
    const COL_PROJECTID = 'tenant.projectId';

    /**
     * the column name for the unitId field
     */
    const COL_UNITID = 'tenant.unitId';

    /**
     * the column name for the unitBlock field
     */
    const COL_UNITBLOCK = 'tenant.unitBlock';

    /**
     * the column name for the unitFloor field
     */
    const COL_UNITFLOOR = 'tenant.unitFloor';

    /**
     * the column name for the unitNo field
     */
    const COL_UNITNO = 'tenant.unitNo';

    /**
     * the column name for the unitLayoutPlan field
     */
    const COL_UNITLAYOUTPLAN = 'tenant.unitLayoutPlan';

    /**
     * the column name for the liftId field
     */
    const COL_LIFTID = 'tenant.liftId';

    /**
     * the column name for the sipServerIpAddress field
     */
    const COL_SIPSERVERIPADDRESS = 'tenant.sipServerIpAddress';

    /**
     * the column name for the username field
     */
    const COL_USERNAME = 'tenant.username';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'tenant.password';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'tenant.name';

    /**
     * the column name for the homeNo field
     */
    const COL_HOMENO = 'tenant.homeNo';

    /**
     * the column name for the mobileNo field
     */
    const COL_MOBILENO = 'tenant.mobileNo';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'tenant.email';

    /**
     * the column name for the multiple_intercom field
     */
    const COL_MULTIPLE_INTERCOM = 'tenant.multiple_intercom';

    /**
     * the column name for the dateOfBirth field
     */
    const COL_DATEOFBIRTH = 'tenant.dateOfBirth';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'tenant.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'tenant.created_date';

    /**
     * the column name for the modified_by field
     */
    const COL_MODIFIED_BY = 'tenant.modified_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'tenant.modified_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'UnitId', 'UnitBlock', 'UnitFloor', 'UnitNo', 'UnitLayoutPlan', 'LiftId', 'SipServerIpAddress', 'Username', 'Password', 'Name', 'HomeNo', 'MobileNo', 'Email', 'MultipleIntercom', 'DateOfBirth', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'unitId', 'unitBlock', 'unitFloor', 'unitNo', 'unitLayoutPlan', 'liftId', 'sipServerIpAddress', 'username', 'password', 'name', 'homeNo', 'mobileNo', 'email', 'multipleIntercom', 'dateOfBirth', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', ),
        self::TYPE_COLNAME       => array(TenantTableMap::COL_ID, TenantTableMap::COL_PROJECTID, TenantTableMap::COL_UNITID, TenantTableMap::COL_UNITBLOCK, TenantTableMap::COL_UNITFLOOR, TenantTableMap::COL_UNITNO, TenantTableMap::COL_UNITLAYOUTPLAN, TenantTableMap::COL_LIFTID, TenantTableMap::COL_SIPSERVERIPADDRESS, TenantTableMap::COL_USERNAME, TenantTableMap::COL_PASSWORD, TenantTableMap::COL_NAME, TenantTableMap::COL_HOMENO, TenantTableMap::COL_MOBILENO, TenantTableMap::COL_EMAIL, TenantTableMap::COL_MULTIPLE_INTERCOM, TenantTableMap::COL_DATEOFBIRTH, TenantTableMap::COL_CREATED_BY, TenantTableMap::COL_CREATED_DATE, TenantTableMap::COL_MODIFIED_BY, TenantTableMap::COL_MODIFIED_DATE, ),
        self::TYPE_FIELDNAME     => array('id', 'projectId', 'unitId', 'unitBlock', 'unitFloor', 'unitNo', 'unitLayoutPlan', 'liftId', 'sipServerIpAddress', 'username', 'password', 'name', 'homeNo', 'mobileNo', 'email', 'multiple_intercom', 'dateOfBirth', 'created_by', 'created_date', 'modified_by', 'modified_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'UnitId' => 2, 'UnitBlock' => 3, 'UnitFloor' => 4, 'UnitNo' => 5, 'UnitLayoutPlan' => 6, 'LiftId' => 7, 'SipServerIpAddress' => 8, 'Username' => 9, 'Password' => 10, 'Name' => 11, 'HomeNo' => 12, 'MobileNo' => 13, 'Email' => 14, 'MultipleIntercom' => 15, 'DateOfBirth' => 16, 'CreatedBy' => 17, 'CreatedDate' => 18, 'ModifiedBy' => 19, 'ModifiedDate' => 20, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'unitId' => 2, 'unitBlock' => 3, 'unitFloor' => 4, 'unitNo' => 5, 'unitLayoutPlan' => 6, 'liftId' => 7, 'sipServerIpAddress' => 8, 'username' => 9, 'password' => 10, 'name' => 11, 'homeNo' => 12, 'mobileNo' => 13, 'email' => 14, 'multipleIntercom' => 15, 'dateOfBirth' => 16, 'createdBy' => 17, 'createdDate' => 18, 'modifiedBy' => 19, 'modifiedDate' => 20, ),
        self::TYPE_COLNAME       => array(TenantTableMap::COL_ID => 0, TenantTableMap::COL_PROJECTID => 1, TenantTableMap::COL_UNITID => 2, TenantTableMap::COL_UNITBLOCK => 3, TenantTableMap::COL_UNITFLOOR => 4, TenantTableMap::COL_UNITNO => 5, TenantTableMap::COL_UNITLAYOUTPLAN => 6, TenantTableMap::COL_LIFTID => 7, TenantTableMap::COL_SIPSERVERIPADDRESS => 8, TenantTableMap::COL_USERNAME => 9, TenantTableMap::COL_PASSWORD => 10, TenantTableMap::COL_NAME => 11, TenantTableMap::COL_HOMENO => 12, TenantTableMap::COL_MOBILENO => 13, TenantTableMap::COL_EMAIL => 14, TenantTableMap::COL_MULTIPLE_INTERCOM => 15, TenantTableMap::COL_DATEOFBIRTH => 16, TenantTableMap::COL_CREATED_BY => 17, TenantTableMap::COL_CREATED_DATE => 18, TenantTableMap::COL_MODIFIED_BY => 19, TenantTableMap::COL_MODIFIED_DATE => 20, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'projectId' => 1, 'unitId' => 2, 'unitBlock' => 3, 'unitFloor' => 4, 'unitNo' => 5, 'unitLayoutPlan' => 6, 'liftId' => 7, 'sipServerIpAddress' => 8, 'username' => 9, 'password' => 10, 'name' => 11, 'homeNo' => 12, 'mobileNo' => 13, 'email' => 14, 'multiple_intercom' => 15, 'dateOfBirth' => 16, 'created_by' => 17, 'created_date' => 18, 'modified_by' => 19, 'modified_date' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tenant');
        $this->setPhpName('Tenant');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Tenant');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 20, null);
        $this->addPrimaryKey('projectId', 'ProjectId', 'VARCHAR', true, 15, null);
        $this->addColumn('unitId', 'UnitId', 'VARCHAR', true, 15, null);
        $this->addColumn('unitBlock', 'UnitBlock', 'VARCHAR', false, 15, null);
        $this->addColumn('unitFloor', 'UnitFloor', 'VARCHAR', false, 15, null);
        $this->addColumn('unitNo', 'UnitNo', 'VARCHAR', false, 15, null);
        $this->addColumn('unitLayoutPlan', 'UnitLayoutPlan', 'VARCHAR', true, 20, null);
        $this->addColumn('liftId', 'LiftId', 'VARCHAR', true, 20, null);
        $this->addColumn('sipServerIpAddress', 'SipServerIpAddress', 'VARCHAR', true, 20, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 20, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 20, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 50, null);
        $this->addColumn('homeNo', 'HomeNo', 'VARCHAR', false, 15, null);
        $this->addColumn('mobileNo', 'MobileNo', 'VARCHAR', false, 15, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addColumn('multiple_intercom', 'MultipleIntercom', 'TINYINT', false, 2, 0);
        $this->addColumn('dateOfBirth', 'DateOfBirth', 'VARCHAR', false, 10, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modified_by', 'ModifiedBy', 'VARCHAR', false, 10, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Tenant $obj A \Tenant object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize(array((string) $obj->getId(), (string) $obj->getProjectId()));
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Tenant object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Tenant) {
                $key = serialize(array((string) $value->getId(), (string) $value->getProjectId()));

            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key";
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Tenant object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize(array((string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], (string) $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 1 + $offset
                : self::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TenantTableMap::CLASS_DEFAULT : TenantTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Tenant object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TenantTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TenantTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TenantTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TenantTableMap::OM_CLASS;
            /** @var Tenant $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TenantTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TenantTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TenantTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Tenant $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TenantTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TenantTableMap::COL_ID);
            $criteria->addSelectColumn(TenantTableMap::COL_PROJECTID);
            $criteria->addSelectColumn(TenantTableMap::COL_UNITID);
            $criteria->addSelectColumn(TenantTableMap::COL_UNITBLOCK);
            $criteria->addSelectColumn(TenantTableMap::COL_UNITFLOOR);
            $criteria->addSelectColumn(TenantTableMap::COL_UNITNO);
            $criteria->addSelectColumn(TenantTableMap::COL_UNITLAYOUTPLAN);
            $criteria->addSelectColumn(TenantTableMap::COL_LIFTID);
            $criteria->addSelectColumn(TenantTableMap::COL_SIPSERVERIPADDRESS);
            $criteria->addSelectColumn(TenantTableMap::COL_USERNAME);
            $criteria->addSelectColumn(TenantTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(TenantTableMap::COL_NAME);
            $criteria->addSelectColumn(TenantTableMap::COL_HOMENO);
            $criteria->addSelectColumn(TenantTableMap::COL_MOBILENO);
            $criteria->addSelectColumn(TenantTableMap::COL_EMAIL);
            $criteria->addSelectColumn(TenantTableMap::COL_MULTIPLE_INTERCOM);
            $criteria->addSelectColumn(TenantTableMap::COL_DATEOFBIRTH);
            $criteria->addSelectColumn(TenantTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(TenantTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(TenantTableMap::COL_MODIFIED_BY);
            $criteria->addSelectColumn(TenantTableMap::COL_MODIFIED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.projectId');
            $criteria->addSelectColumn($alias . '.unitId');
            $criteria->addSelectColumn($alias . '.unitBlock');
            $criteria->addSelectColumn($alias . '.unitFloor');
            $criteria->addSelectColumn($alias . '.unitNo');
            $criteria->addSelectColumn($alias . '.unitLayoutPlan');
            $criteria->addSelectColumn($alias . '.liftId');
            $criteria->addSelectColumn($alias . '.sipServerIpAddress');
            $criteria->addSelectColumn($alias . '.username');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.homeNo');
            $criteria->addSelectColumn($alias . '.mobileNo');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.multiple_intercom');
            $criteria->addSelectColumn($alias . '.dateOfBirth');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modified_by');
            $criteria->addSelectColumn($alias . '.modified_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TenantTableMap::DATABASE_NAME)->getTable(TenantTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TenantTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TenantTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TenantTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Tenant or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Tenant object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TenantTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Tenant) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TenantTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(TenantTableMap::COL_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(TenantTableMap::COL_PROJECTID, $value[1]));
                $criteria->addOr($criterion);
            }
        }

        $query = TenantQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TenantTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TenantTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the tenant table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TenantQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Tenant or Criteria object.
     *
     * @param mixed               $criteria Criteria or Tenant object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TenantTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Tenant object
        }


        // Set the correct dbName
        $query = TenantQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TenantTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TenantTableMap::buildTableMap();
