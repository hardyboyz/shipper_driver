<?php

namespace Map;

use \ChatUser;
use \ChatUserQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'chat_user' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ChatUserTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ChatUserTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'chat_user';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ChatUser';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ChatUser';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the invited_by field
     */
    const COL_INVITED_BY = 'chat_user.invited_by';

    /**
     * the column name for the invited_date field
     */
    const COL_INVITED_DATE = 'chat_user.invited_date';

    /**
     * the column name for the modifed_by field
     */
    const COL_MODIFED_BY = 'chat_user.modifed_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'chat_user.modified_date';

    /**
     * the column name for the chatroom_id field
     */
    const COL_CHATROOM_ID = 'chat_user.chatroom_id';

    /**
     * the column name for the unit_id field
     */
    const COL_UNIT_ID = 'chat_user.unit_id';

    /**
     * the column name for the project_id field
     */
    const COL_PROJECT_ID = 'chat_user.project_id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'chat_user.user_id';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'chat_user.status';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('InvitedBy', 'InvitedDate', 'ModifiedBy', 'ModifiedDate', 'ChatroomId', 'UnitId', 'ProjectId', 'UserId', 'Status', ),
        self::TYPE_CAMELNAME     => array('invitedBy', 'invitedDate', 'modifiedBy', 'modifiedDate', 'chatroomId', 'unitId', 'projectId', 'userId', 'status', ),
        self::TYPE_COLNAME       => array(ChatUserTableMap::COL_INVITED_BY, ChatUserTableMap::COL_INVITED_DATE, ChatUserTableMap::COL_MODIFED_BY, ChatUserTableMap::COL_MODIFIED_DATE, ChatUserTableMap::COL_CHATROOM_ID, ChatUserTableMap::COL_UNIT_ID, ChatUserTableMap::COL_PROJECT_ID, ChatUserTableMap::COL_USER_ID, ChatUserTableMap::COL_STATUS, ),
        self::TYPE_FIELDNAME     => array('invited_by', 'invited_date', 'modifed_by', 'modified_date', 'chatroom_id', 'unit_id', 'project_id', 'user_id', 'status', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('InvitedBy' => 0, 'InvitedDate' => 1, 'ModifiedBy' => 2, 'ModifiedDate' => 3, 'ChatroomId' => 4, 'UnitId' => 5, 'ProjectId' => 6, 'UserId' => 7, 'Status' => 8, ),
        self::TYPE_CAMELNAME     => array('invitedBy' => 0, 'invitedDate' => 1, 'modifiedBy' => 2, 'modifiedDate' => 3, 'chatroomId' => 4, 'unitId' => 5, 'projectId' => 6, 'userId' => 7, 'status' => 8, ),
        self::TYPE_COLNAME       => array(ChatUserTableMap::COL_INVITED_BY => 0, ChatUserTableMap::COL_INVITED_DATE => 1, ChatUserTableMap::COL_MODIFED_BY => 2, ChatUserTableMap::COL_MODIFIED_DATE => 3, ChatUserTableMap::COL_CHATROOM_ID => 4, ChatUserTableMap::COL_UNIT_ID => 5, ChatUserTableMap::COL_PROJECT_ID => 6, ChatUserTableMap::COL_USER_ID => 7, ChatUserTableMap::COL_STATUS => 8, ),
        self::TYPE_FIELDNAME     => array('invited_by' => 0, 'invited_date' => 1, 'modifed_by' => 2, 'modified_date' => 3, 'chatroom_id' => 4, 'unit_id' => 5, 'project_id' => 6, 'user_id' => 7, 'status' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('chat_user');
        $this->setPhpName('ChatUser');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ChatUser');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addColumn('invited_by', 'InvitedBy', 'VARCHAR', true, 20, null);
        $this->addColumn('invited_date', 'InvitedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modifed_by', 'ModifiedBy', 'VARCHAR', true, 20, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', true, null, null);
        $this->addPrimaryKey('chatroom_id', 'ChatroomId', 'VARCHAR', true, 20, null);
        $this->addPrimaryKey('unit_id', 'UnitId', 'VARCHAR', true, 15, null);
        $this->addPrimaryKey('project_id', 'ProjectId', 'VARCHAR', true, 5, null);
        $this->addColumn('user_id', 'UserId', 'VARCHAR', true, 20, null);
        $this->addColumn('status', 'Status', 'TINYINT', true, 4, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \ChatUser $obj A \ChatUser object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize(array((string) $obj->getChatroomId(), (string) $obj->getUnitId(), (string) $obj->getProjectId()));
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \ChatUser object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \ChatUser) {
                $key = serialize(array((string) $value->getChatroomId(), (string) $value->getUnitId(), (string) $value->getProjectId()));

            } elseif (is_array($value) && count($value) === 3) {
                // assume we've been passed a primary key";
                $key = serialize(array((string) $value[0], (string) $value[1], (string) $value[2]));
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \ChatUser object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('ChatroomId', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 5 + $offset : static::translateFieldName('UnitId', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 6 + $offset : static::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize(array((string) $row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('ChatroomId', TableMap::TYPE_PHPNAME, $indexType)], (string) $row[TableMap::TYPE_NUM == $indexType ? 5 + $offset : static::translateFieldName('UnitId', TableMap::TYPE_PHPNAME, $indexType)], (string) $row[TableMap::TYPE_NUM == $indexType ? 6 + $offset : static::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 4 + $offset
                : self::translateFieldName('ChatroomId', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 5 + $offset
                : self::translateFieldName('UnitId', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 6 + $offset
                : self::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ChatUserTableMap::CLASS_DEFAULT : ChatUserTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ChatUser object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ChatUserTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ChatUserTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ChatUserTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChatUserTableMap::OM_CLASS;
            /** @var ChatUser $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ChatUserTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ChatUserTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ChatUserTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ChatUser $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChatUserTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChatUserTableMap::COL_INVITED_BY);
            $criteria->addSelectColumn(ChatUserTableMap::COL_INVITED_DATE);
            $criteria->addSelectColumn(ChatUserTableMap::COL_MODIFED_BY);
            $criteria->addSelectColumn(ChatUserTableMap::COL_MODIFIED_DATE);
            $criteria->addSelectColumn(ChatUserTableMap::COL_CHATROOM_ID);
            $criteria->addSelectColumn(ChatUserTableMap::COL_UNIT_ID);
            $criteria->addSelectColumn(ChatUserTableMap::COL_PROJECT_ID);
            $criteria->addSelectColumn(ChatUserTableMap::COL_USER_ID);
            $criteria->addSelectColumn(ChatUserTableMap::COL_STATUS);
        } else {
            $criteria->addSelectColumn($alias . '.invited_by');
            $criteria->addSelectColumn($alias . '.invited_date');
            $criteria->addSelectColumn($alias . '.modifed_by');
            $criteria->addSelectColumn($alias . '.modified_date');
            $criteria->addSelectColumn($alias . '.chatroom_id');
            $criteria->addSelectColumn($alias . '.unit_id');
            $criteria->addSelectColumn($alias . '.project_id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.status');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ChatUserTableMap::DATABASE_NAME)->getTable(ChatUserTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ChatUserTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ChatUserTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ChatUserTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ChatUser or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChatUser object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ChatUser) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ChatUserTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(ChatUserTableMap::COL_CHATROOM_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(ChatUserTableMap::COL_UNIT_ID, $value[1]));
                $criterion->addAnd($criteria->getNewCriterion(ChatUserTableMap::COL_PROJECT_ID, $value[2]));
                $criteria->addOr($criterion);
            }
        }

        $query = ChatUserQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ChatUserTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ChatUserTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the chat_user table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ChatUserQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ChatUser or Criteria object.
     *
     * @param mixed               $criteria Criteria or ChatUser object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatUserTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ChatUser object
        }


        // Set the correct dbName
        $query = ChatUserQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ChatUserTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ChatUserTableMap::buildTableMap();
