<?php

namespace Map;

use \AlarmStatus;
use \AlarmStatusQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'alarmStatus' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AlarmStatusTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AlarmStatusTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'alarmStatus';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AlarmStatus';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AlarmStatus';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'alarmStatus.id';

    /**
     * the column name for the projectId field
     */
    const COL_PROJECTID = 'alarmStatus.projectId';

    /**
     * the column name for the unitId field
     */
    const COL_UNITID = 'alarmStatus.unitId';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'alarmStatus.status';

    /**
     * the column name for the alarmHistoryId field
     */
    const COL_ALARMHISTORYID = 'alarmStatus.alarmHistoryId';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'alarmStatus.created_date';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'alarmStatus.modified_date';

    /**
     * the column name for the triggered_date field
     */
    const COL_TRIGGERED_DATE = 'alarmStatus.triggered_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'UnitId', 'Status', 'AlarmHistoryId', 'CreatedDate', 'ModifiedDate', 'TriggeredDate', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'unitId', 'status', 'alarmHistoryId', 'createdDate', 'modifiedDate', 'triggeredDate', ),
        self::TYPE_COLNAME       => array(AlarmStatusTableMap::COL_ID, AlarmStatusTableMap::COL_PROJECTID, AlarmStatusTableMap::COL_UNITID, AlarmStatusTableMap::COL_STATUS, AlarmStatusTableMap::COL_ALARMHISTORYID, AlarmStatusTableMap::COL_CREATED_DATE, AlarmStatusTableMap::COL_MODIFIED_DATE, AlarmStatusTableMap::COL_TRIGGERED_DATE, ),
        self::TYPE_FIELDNAME     => array('id', 'projectId', 'unitId', 'status', 'alarmHistoryId', 'created_date', 'modified_date', 'triggered_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'UnitId' => 2, 'Status' => 3, 'AlarmHistoryId' => 4, 'CreatedDate' => 5, 'ModifiedDate' => 6, 'TriggeredDate' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'unitId' => 2, 'status' => 3, 'alarmHistoryId' => 4, 'createdDate' => 5, 'modifiedDate' => 6, 'triggeredDate' => 7, ),
        self::TYPE_COLNAME       => array(AlarmStatusTableMap::COL_ID => 0, AlarmStatusTableMap::COL_PROJECTID => 1, AlarmStatusTableMap::COL_UNITID => 2, AlarmStatusTableMap::COL_STATUS => 3, AlarmStatusTableMap::COL_ALARMHISTORYID => 4, AlarmStatusTableMap::COL_CREATED_DATE => 5, AlarmStatusTableMap::COL_MODIFIED_DATE => 6, AlarmStatusTableMap::COL_TRIGGERED_DATE => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'projectId' => 1, 'unitId' => 2, 'status' => 3, 'alarmHistoryId' => 4, 'created_date' => 5, 'modified_date' => 6, 'triggered_date' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('alarmStatus');
        $this->setPhpName('AlarmStatus');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\AlarmStatus');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 15, null);
        $this->addColumn('projectId', 'ProjectId', 'VARCHAR', true, 15, null);
        $this->addColumn('unitId', 'UnitId', 'VARCHAR', true, 15, null);
        $this->addColumn('status', 'Status', 'VARCHAR', true, 15, null);
        $this->addColumn('alarmHistoryId', 'AlarmHistoryId', 'VARCHAR', false, 15, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('triggered_date', 'TriggeredDate', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AlarmStatusTableMap::CLASS_DEFAULT : AlarmStatusTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AlarmStatus object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AlarmStatusTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AlarmStatusTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AlarmStatusTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AlarmStatusTableMap::OM_CLASS;
            /** @var AlarmStatus $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AlarmStatusTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AlarmStatusTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AlarmStatusTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AlarmStatus $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AlarmStatusTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AlarmStatusTableMap::COL_ID);
            $criteria->addSelectColumn(AlarmStatusTableMap::COL_PROJECTID);
            $criteria->addSelectColumn(AlarmStatusTableMap::COL_UNITID);
            $criteria->addSelectColumn(AlarmStatusTableMap::COL_STATUS);
            $criteria->addSelectColumn(AlarmStatusTableMap::COL_ALARMHISTORYID);
            $criteria->addSelectColumn(AlarmStatusTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(AlarmStatusTableMap::COL_MODIFIED_DATE);
            $criteria->addSelectColumn(AlarmStatusTableMap::COL_TRIGGERED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.projectId');
            $criteria->addSelectColumn($alias . '.unitId');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.alarmHistoryId');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modified_date');
            $criteria->addSelectColumn($alias . '.triggered_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AlarmStatusTableMap::DATABASE_NAME)->getTable(AlarmStatusTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AlarmStatusTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AlarmStatusTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AlarmStatusTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AlarmStatus or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AlarmStatus object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AlarmStatus) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AlarmStatusTableMap::DATABASE_NAME);
            $criteria->add(AlarmStatusTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = AlarmStatusQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AlarmStatusTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AlarmStatusTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the alarmStatus table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AlarmStatusQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AlarmStatus or Criteria object.
     *
     * @param mixed               $criteria Criteria or AlarmStatus object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AlarmStatusTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AlarmStatus object
        }


        // Set the correct dbName
        $query = AlarmStatusQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AlarmStatusTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AlarmStatusTableMap::buildTableMap();
