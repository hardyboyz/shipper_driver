<?php

namespace Map;

use \PropertyProject;
use \PropertyProjectQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'propertyProject' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PropertyProjectTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PropertyProjectTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'propertyProject';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\PropertyProject';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'PropertyProject';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'propertyProject.id';

    /**
     * the column name for the projectPrefix field
     */
    const COL_PROJECTPREFIX = 'propertyProject.projectPrefix';

    /**
     * the column name for the projectName field
     */
    const COL_PROJECTNAME = 'propertyProject.projectName';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'propertyProject.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'propertyProject.created_date';

    /**
     * the column name for the modified_by field
     */
    const COL_MODIFIED_BY = 'propertyProject.modified_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'propertyProject.modified_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectPrefix', 'ProjectName', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', ),
        self::TYPE_CAMELNAME     => array('id', 'projectPrefix', 'projectName', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', ),
        self::TYPE_COLNAME       => array(PropertyProjectTableMap::COL_ID, PropertyProjectTableMap::COL_PROJECTPREFIX, PropertyProjectTableMap::COL_PROJECTNAME, PropertyProjectTableMap::COL_CREATED_BY, PropertyProjectTableMap::COL_CREATED_DATE, PropertyProjectTableMap::COL_MODIFIED_BY, PropertyProjectTableMap::COL_MODIFIED_DATE, ),
        self::TYPE_FIELDNAME     => array('id', 'projectPrefix', 'projectName', 'created_by', 'created_date', 'modified_by', 'modified_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectPrefix' => 1, 'ProjectName' => 2, 'CreatedBy' => 3, 'CreatedDate' => 4, 'ModifiedBy' => 5, 'ModifiedDate' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectPrefix' => 1, 'projectName' => 2, 'createdBy' => 3, 'createdDate' => 4, 'modifiedBy' => 5, 'modifiedDate' => 6, ),
        self::TYPE_COLNAME       => array(PropertyProjectTableMap::COL_ID => 0, PropertyProjectTableMap::COL_PROJECTPREFIX => 1, PropertyProjectTableMap::COL_PROJECTNAME => 2, PropertyProjectTableMap::COL_CREATED_BY => 3, PropertyProjectTableMap::COL_CREATED_DATE => 4, PropertyProjectTableMap::COL_MODIFIED_BY => 5, PropertyProjectTableMap::COL_MODIFIED_DATE => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'projectPrefix' => 1, 'projectName' => 2, 'created_by' => 3, 'created_date' => 4, 'modified_by' => 5, 'modified_date' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('propertyProject');
        $this->setPhpName('PropertyProject');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\PropertyProject');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 15, null);
        $this->addColumn('projectPrefix', 'ProjectPrefix', 'VARCHAR', true, 50, null);
        $this->addColumn('projectName', 'ProjectName', 'VARCHAR', true, 50, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modified_by', 'ModifiedBy', 'VARCHAR', false, 10, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PropertyProjectTableMap::CLASS_DEFAULT : PropertyProjectTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PropertyProject object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PropertyProjectTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PropertyProjectTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PropertyProjectTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PropertyProjectTableMap::OM_CLASS;
            /** @var PropertyProject $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PropertyProjectTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PropertyProjectTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PropertyProjectTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PropertyProject $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PropertyProjectTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PropertyProjectTableMap::COL_ID);
            $criteria->addSelectColumn(PropertyProjectTableMap::COL_PROJECTPREFIX);
            $criteria->addSelectColumn(PropertyProjectTableMap::COL_PROJECTNAME);
            $criteria->addSelectColumn(PropertyProjectTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(PropertyProjectTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(PropertyProjectTableMap::COL_MODIFIED_BY);
            $criteria->addSelectColumn(PropertyProjectTableMap::COL_MODIFIED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.projectPrefix');
            $criteria->addSelectColumn($alias . '.projectName');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modified_by');
            $criteria->addSelectColumn($alias . '.modified_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PropertyProjectTableMap::DATABASE_NAME)->getTable(PropertyProjectTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PropertyProjectTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PropertyProjectTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PropertyProjectTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PropertyProject or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PropertyProject object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyProjectTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \PropertyProject) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PropertyProjectTableMap::DATABASE_NAME);
            $criteria->add(PropertyProjectTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PropertyProjectQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PropertyProjectTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PropertyProjectTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the propertyProject table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PropertyProjectQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PropertyProject or Criteria object.
     *
     * @param mixed               $criteria Criteria or PropertyProject object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyProjectTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PropertyProject object
        }


        // Set the correct dbName
        $query = PropertyProjectQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PropertyProjectTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PropertyProjectTableMap::buildTableMap();
