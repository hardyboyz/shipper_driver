<?php

namespace Map;

use \ChatRoom;
use \ChatRoomQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'chatroom' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ChatRoomTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ChatRoomTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'chatroom';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ChatRoom';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ChatRoom';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 17;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 17;

    /**
     * the column name for the id field
     */
    const COL_ID = 'chatroom.id';

    /**
     * the column name for the project_id field
     */
    const COL_PROJECT_ID = 'chatroom.project_id';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'chatroom.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'chatroom.created_date';

    /**
     * the column name for the modifed_by field
     */
    const COL_MODIFED_BY = 'chatroom.modifed_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'chatroom.modified_date';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'chatroom.title';

    /**
     * the column name for the admin_id field
     */
    const COL_ADMIN_ID = 'chatroom.admin_id';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'chatroom.status';

    /**
     * the column name for the total_message field
     */
    const COL_TOTAL_MESSAGE = 'chatroom.total_message';

    /**
     * the column name for the report_abuse_flag field
     */
    const COL_REPORT_ABUSE_FLAG = 'chatroom.report_abuse_flag';

    /**
     * the column name for the report_abuse_by field
     */
    const COL_REPORT_ABUSE_BY = 'chatroom.report_abuse_by';

    /**
     * the column name for the report_abuse_date field
     */
    const COL_REPORT_ABUSE_DATE = 'chatroom.report_abuse_date';

    /**
     * the column name for the scheduler_modified field
     */
    const COL_SCHEDULER_MODIFIED = 'chatroom.scheduler_modified';

    /**
     * the column name for the reason field
     */
    const COL_REASON = 'chatroom.reason';

    /**
     * the column name for the modifed_by_admin field
     */
    const COL_MODIFED_BY_ADMIN = 'chatroom.modifed_by_admin';

    /**
     * the column name for the modified_date_by_admin field
     */
    const COL_MODIFIED_DATE_BY_ADMIN = 'chatroom.modified_date_by_admin';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', 'Title', 'AdminId', 'Status', 'TotalMessage', 'ReportAbuseFlag', 'ReportAbuseBy', 'ReportAbuseDate', 'SchedulerModified', 'Reason', 'ModifiedByAdmin', 'ModifiedDateByAdmin', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', 'title', 'adminId', 'status', 'totalMessage', 'reportAbuseFlag', 'reportAbuseBy', 'reportAbuseDate', 'schedulerModified', 'reason', 'modifiedByAdmin', 'modifiedDateByAdmin', ),
        self::TYPE_COLNAME       => array(ChatRoomTableMap::COL_ID, ChatRoomTableMap::COL_PROJECT_ID, ChatRoomTableMap::COL_CREATED_BY, ChatRoomTableMap::COL_CREATED_DATE, ChatRoomTableMap::COL_MODIFED_BY, ChatRoomTableMap::COL_MODIFIED_DATE, ChatRoomTableMap::COL_TITLE, ChatRoomTableMap::COL_ADMIN_ID, ChatRoomTableMap::COL_STATUS, ChatRoomTableMap::COL_TOTAL_MESSAGE, ChatRoomTableMap::COL_REPORT_ABUSE_FLAG, ChatRoomTableMap::COL_REPORT_ABUSE_BY, ChatRoomTableMap::COL_REPORT_ABUSE_DATE, ChatRoomTableMap::COL_SCHEDULER_MODIFIED, ChatRoomTableMap::COL_REASON, ChatRoomTableMap::COL_MODIFED_BY_ADMIN, ChatRoomTableMap::COL_MODIFIED_DATE_BY_ADMIN, ),
        self::TYPE_FIELDNAME     => array('id', 'project_id', 'created_by', 'created_date', 'modifed_by', 'modified_date', 'title', 'admin_id', 'status', 'total_message', 'report_abuse_flag', 'report_abuse_by', 'report_abuse_date', 'scheduler_modified', 'reason', 'modifed_by_admin', 'modified_date_by_admin', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'CreatedBy' => 2, 'CreatedDate' => 3, 'ModifiedBy' => 4, 'ModifiedDate' => 5, 'Title' => 6, 'AdminId' => 7, 'Status' => 8, 'TotalMessage' => 9, 'ReportAbuseFlag' => 10, 'ReportAbuseBy' => 11, 'ReportAbuseDate' => 12, 'SchedulerModified' => 13, 'Reason' => 14, 'ModifiedByAdmin' => 15, 'ModifiedDateByAdmin' => 16, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'createdBy' => 2, 'createdDate' => 3, 'modifiedBy' => 4, 'modifiedDate' => 5, 'title' => 6, 'adminId' => 7, 'status' => 8, 'totalMessage' => 9, 'reportAbuseFlag' => 10, 'reportAbuseBy' => 11, 'reportAbuseDate' => 12, 'schedulerModified' => 13, 'reason' => 14, 'modifiedByAdmin' => 15, 'modifiedDateByAdmin' => 16, ),
        self::TYPE_COLNAME       => array(ChatRoomTableMap::COL_ID => 0, ChatRoomTableMap::COL_PROJECT_ID => 1, ChatRoomTableMap::COL_CREATED_BY => 2, ChatRoomTableMap::COL_CREATED_DATE => 3, ChatRoomTableMap::COL_MODIFED_BY => 4, ChatRoomTableMap::COL_MODIFIED_DATE => 5, ChatRoomTableMap::COL_TITLE => 6, ChatRoomTableMap::COL_ADMIN_ID => 7, ChatRoomTableMap::COL_STATUS => 8, ChatRoomTableMap::COL_TOTAL_MESSAGE => 9, ChatRoomTableMap::COL_REPORT_ABUSE_FLAG => 10, ChatRoomTableMap::COL_REPORT_ABUSE_BY => 11, ChatRoomTableMap::COL_REPORT_ABUSE_DATE => 12, ChatRoomTableMap::COL_SCHEDULER_MODIFIED => 13, ChatRoomTableMap::COL_REASON => 14, ChatRoomTableMap::COL_MODIFED_BY_ADMIN => 15, ChatRoomTableMap::COL_MODIFIED_DATE_BY_ADMIN => 16, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'project_id' => 1, 'created_by' => 2, 'created_date' => 3, 'modifed_by' => 4, 'modified_date' => 5, 'title' => 6, 'admin_id' => 7, 'status' => 8, 'total_message' => 9, 'report_abuse_flag' => 10, 'report_abuse_by' => 11, 'report_abuse_date' => 12, 'scheduler_modified' => 13, 'reason' => 14, 'modifed_by_admin' => 15, 'modified_date_by_admin' => 16, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('chatroom');
        $this->setPhpName('ChatRoom');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ChatRoom');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('project_id', 'ProjectId', 'VARCHAR', true, 5, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 20, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modifed_by', 'ModifiedBy', 'VARCHAR', true, 20, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('admin_id', 'AdminId', 'VARCHAR', true, 20, null);
        $this->addColumn('status', 'Status', 'TINYINT', true, null, null);
        $this->addColumn('total_message', 'TotalMessage', 'VARCHAR', false, 20, null);
        $this->addColumn('report_abuse_flag', 'ReportAbuseFlag', 'VARCHAR', true, 5, null);
        $this->addColumn('report_abuse_by', 'ReportAbuseBy', 'VARCHAR', true, 20, null);
        $this->addColumn('report_abuse_date', 'ReportAbuseDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('scheduler_modified', 'SchedulerModified', 'TIMESTAMP', true, null, null);
        $this->addColumn('reason', 'Reason', 'VARCHAR', true, 255, null);
        $this->addColumn('modifed_by_admin', 'ModifiedByAdmin', 'VARCHAR', true, 20, null);
        $this->addColumn('modified_date_by_admin', 'ModifiedDateByAdmin', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ChatroomSetting', '\\ChatroomSetting', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':chatroom_id',
    1 => ':id',
  ),
), null, null, 'ChatroomSettings', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ChatRoomTableMap::CLASS_DEFAULT : ChatRoomTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ChatRoom object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ChatRoomTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ChatRoomTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ChatRoomTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChatRoomTableMap::OM_CLASS;
            /** @var ChatRoom $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ChatRoomTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ChatRoomTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ChatRoomTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ChatRoom $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChatRoomTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChatRoomTableMap::COL_ID);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_PROJECT_ID);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_MODIFED_BY);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_MODIFIED_DATE);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_TITLE);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_ADMIN_ID);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_STATUS);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_TOTAL_MESSAGE);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_REPORT_ABUSE_FLAG);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_REPORT_ABUSE_BY);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_REPORT_ABUSE_DATE);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_SCHEDULER_MODIFIED);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_REASON);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_MODIFED_BY_ADMIN);
            $criteria->addSelectColumn(ChatRoomTableMap::COL_MODIFIED_DATE_BY_ADMIN);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.project_id');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modifed_by');
            $criteria->addSelectColumn($alias . '.modified_date');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.admin_id');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.total_message');
            $criteria->addSelectColumn($alias . '.report_abuse_flag');
            $criteria->addSelectColumn($alias . '.report_abuse_by');
            $criteria->addSelectColumn($alias . '.report_abuse_date');
            $criteria->addSelectColumn($alias . '.scheduler_modified');
            $criteria->addSelectColumn($alias . '.reason');
            $criteria->addSelectColumn($alias . '.modifed_by_admin');
            $criteria->addSelectColumn($alias . '.modified_date_by_admin');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ChatRoomTableMap::DATABASE_NAME)->getTable(ChatRoomTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ChatRoomTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ChatRoomTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ChatRoomTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ChatRoom or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChatRoom object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatRoomTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ChatRoom) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ChatRoomTableMap::DATABASE_NAME);
            $criteria->add(ChatRoomTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ChatRoomQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ChatRoomTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ChatRoomTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the chatroom table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ChatRoomQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ChatRoom or Criteria object.
     *
     * @param mixed               $criteria Criteria or ChatRoom object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChatRoomTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ChatRoom object
        }

        if ($criteria->containsKey(ChatRoomTableMap::COL_ID) && $criteria->keyContainsValue(ChatRoomTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ChatRoomTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ChatRoomQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ChatRoomTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ChatRoomTableMap::buildTableMap();
