<?php

namespace Map;

use \FacilityBooking;
use \FacilityBookingQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'facility_booking' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class FacilityBookingTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.FacilityBookingTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'facility_booking';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\FacilityBooking';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'FacilityBooking';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 25;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 25;

    /**
     * the column name for the id field
     */
    const COL_ID = 'facility_booking.id';

    /**
     * the column name for the booking_number field
     */
    const COL_BOOKING_NUMBER = 'facility_booking.booking_number';

    /**
     * the column name for the facility_id field
     */
    const COL_FACILITY_ID = 'facility_booking.facility_id';

    /**
     * the column name for the facility_name field
     */
    const COL_FACILITY_NAME = 'facility_booking.facility_name';

    /**
     * the column name for the booking_date field
     */
    const COL_BOOKING_DATE = 'facility_booking.booking_date';

    /**
     * the column name for the booking_time field
     */
    const COL_BOOKING_TIME = 'facility_booking.booking_time';

    /**
     * the column name for the unit_id field
     */
    const COL_UNIT_ID = 'facility_booking.unit_id';

    /**
     * the column name for the booking_timeslot field
     */
    const COL_BOOKING_TIMESLOT = 'facility_booking.booking_timeslot';

    /**
     * the column name for the booking_owner field
     */
    const COL_BOOKING_OWNER = 'facility_booking.booking_owner';

    /**
     * the column name for the project_id field
     */
    const COL_PROJECT_ID = 'facility_booking.project_id';

    /**
     * the column name for the tenant_id field
     */
    const COL_TENANT_ID = 'facility_booking.tenant_id';

    /**
     * the column name for the booking_status field
     */
    const COL_BOOKING_STATUS = 'facility_booking.booking_status';

    /**
     * the column name for the payment_status field
     */
    const COL_PAYMENT_STATUS = 'facility_booking.payment_status';

    /**
     * the column name for the payment_date field
     */
    const COL_PAYMENT_DATE = 'facility_booking.payment_date';

    /**
     * the column name for the cancel_date field
     */
    const COL_CANCEL_DATE = 'facility_booking.cancel_date';

    /**
     * the column name for the cancel_reason field
     */
    const COL_CANCEL_REASON = 'facility_booking.cancel_reason';

    /**
     * the column name for the cancel_by field
     */
    const COL_CANCEL_BY = 'facility_booking.cancel_by';

    /**
     * the column name for the deposit field
     */
    const COL_DEPOSIT = 'facility_booking.deposit';

    /**
     * the column name for the charge field
     */
    const COL_CHARGE = 'facility_booking.charge';

    /**
     * the column name for the total field
     */
    const COL_TOTAL = 'facility_booking.total';

    /**
     * the column name for the created_by field
     */
    const COL_CREATED_BY = 'facility_booking.created_by';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'facility_booking.created_date';

    /**
     * the column name for the modified_by field
     */
    const COL_MODIFIED_BY = 'facility_booking.modified_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'facility_booking.modified_date';

    /**
     * the column name for the uniqid field
     */
    const COL_UNIQID = 'facility_booking.uniqid';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'BookingNumber', 'FacilityId', 'FacilityName', 'BookingDate', 'BookingTime', 'UnitId', 'BookingTimeslot', 'BookingOwner', 'ProjectId', 'TenantId', 'BookingStatus', 'PaymentStatus', 'PaymentDate', 'CancelDate', 'CancelReason', 'CancelBy', 'Deposit', 'Charge', 'Total', 'CreatedBy', 'CreatedDate', 'ModifiedBy', 'ModifiedDate', 'UniqId', ),
        self::TYPE_CAMELNAME     => array('id', 'bookingNumber', 'facilityId', 'facilityName', 'bookingDate', 'bookingTime', 'unitId', 'bookingTimeslot', 'bookingOwner', 'projectId', 'tenantId', 'bookingStatus', 'paymentStatus', 'paymentDate', 'cancelDate', 'cancelReason', 'cancelBy', 'deposit', 'charge', 'total', 'createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', 'uniqId', ),
        self::TYPE_COLNAME       => array(FacilityBookingTableMap::COL_ID, FacilityBookingTableMap::COL_BOOKING_NUMBER, FacilityBookingTableMap::COL_FACILITY_ID, FacilityBookingTableMap::COL_FACILITY_NAME, FacilityBookingTableMap::COL_BOOKING_DATE, FacilityBookingTableMap::COL_BOOKING_TIME, FacilityBookingTableMap::COL_UNIT_ID, FacilityBookingTableMap::COL_BOOKING_TIMESLOT, FacilityBookingTableMap::COL_BOOKING_OWNER, FacilityBookingTableMap::COL_PROJECT_ID, FacilityBookingTableMap::COL_TENANT_ID, FacilityBookingTableMap::COL_BOOKING_STATUS, FacilityBookingTableMap::COL_PAYMENT_STATUS, FacilityBookingTableMap::COL_PAYMENT_DATE, FacilityBookingTableMap::COL_CANCEL_DATE, FacilityBookingTableMap::COL_CANCEL_REASON, FacilityBookingTableMap::COL_CANCEL_BY, FacilityBookingTableMap::COL_DEPOSIT, FacilityBookingTableMap::COL_CHARGE, FacilityBookingTableMap::COL_TOTAL, FacilityBookingTableMap::COL_CREATED_BY, FacilityBookingTableMap::COL_CREATED_DATE, FacilityBookingTableMap::COL_MODIFIED_BY, FacilityBookingTableMap::COL_MODIFIED_DATE, FacilityBookingTableMap::COL_UNIQID, ),
        self::TYPE_FIELDNAME     => array('id', 'booking_number', 'facility_id', 'facility_name', 'booking_date', 'booking_time', 'unit_id', 'booking_timeslot', 'booking_owner', 'project_id', 'tenant_id', 'booking_status', 'payment_status', 'payment_date', 'cancel_date', 'cancel_reason', 'cancel_by', 'deposit', 'charge', 'total', 'created_by', 'created_date', 'modified_by', 'modified_date', 'uniqid', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'BookingNumber' => 1, 'FacilityId' => 2, 'FacilityName' => 3, 'BookingDate' => 4, 'BookingTime' => 5, 'UnitId' => 6, 'BookingTimeslot' => 7, 'BookingOwner' => 8, 'ProjectId' => 9, 'TenantId' => 10, 'BookingStatus' => 11, 'PaymentStatus' => 12, 'PaymentDate' => 13, 'CancelDate' => 14, 'CancelReason' => 15, 'CancelBy' => 16, 'Deposit' => 17, 'Charge' => 18, 'Total' => 19, 'CreatedBy' => 20, 'CreatedDate' => 21, 'ModifiedBy' => 22, 'ModifiedDate' => 23, 'UniqId' => 24, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'bookingNumber' => 1, 'facilityId' => 2, 'facilityName' => 3, 'bookingDate' => 4, 'bookingTime' => 5, 'unitId' => 6, 'bookingTimeslot' => 7, 'bookingOwner' => 8, 'projectId' => 9, 'tenantId' => 10, 'bookingStatus' => 11, 'paymentStatus' => 12, 'paymentDate' => 13, 'cancelDate' => 14, 'cancelReason' => 15, 'cancelBy' => 16, 'deposit' => 17, 'charge' => 18, 'total' => 19, 'createdBy' => 20, 'createdDate' => 21, 'modifiedBy' => 22, 'modifiedDate' => 23, 'uniqId' => 24, ),
        self::TYPE_COLNAME       => array(FacilityBookingTableMap::COL_ID => 0, FacilityBookingTableMap::COL_BOOKING_NUMBER => 1, FacilityBookingTableMap::COL_FACILITY_ID => 2, FacilityBookingTableMap::COL_FACILITY_NAME => 3, FacilityBookingTableMap::COL_BOOKING_DATE => 4, FacilityBookingTableMap::COL_BOOKING_TIME => 5, FacilityBookingTableMap::COL_UNIT_ID => 6, FacilityBookingTableMap::COL_BOOKING_TIMESLOT => 7, FacilityBookingTableMap::COL_BOOKING_OWNER => 8, FacilityBookingTableMap::COL_PROJECT_ID => 9, FacilityBookingTableMap::COL_TENANT_ID => 10, FacilityBookingTableMap::COL_BOOKING_STATUS => 11, FacilityBookingTableMap::COL_PAYMENT_STATUS => 12, FacilityBookingTableMap::COL_PAYMENT_DATE => 13, FacilityBookingTableMap::COL_CANCEL_DATE => 14, FacilityBookingTableMap::COL_CANCEL_REASON => 15, FacilityBookingTableMap::COL_CANCEL_BY => 16, FacilityBookingTableMap::COL_DEPOSIT => 17, FacilityBookingTableMap::COL_CHARGE => 18, FacilityBookingTableMap::COL_TOTAL => 19, FacilityBookingTableMap::COL_CREATED_BY => 20, FacilityBookingTableMap::COL_CREATED_DATE => 21, FacilityBookingTableMap::COL_MODIFIED_BY => 22, FacilityBookingTableMap::COL_MODIFIED_DATE => 23, FacilityBookingTableMap::COL_UNIQID => 24, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'booking_number' => 1, 'facility_id' => 2, 'facility_name' => 3, 'booking_date' => 4, 'booking_time' => 5, 'unit_id' => 6, 'booking_timeslot' => 7, 'booking_owner' => 8, 'project_id' => 9, 'tenant_id' => 10, 'booking_status' => 11, 'payment_status' => 12, 'payment_date' => 13, 'cancel_date' => 14, 'cancel_reason' => 15, 'cancel_by' => 16, 'deposit' => 17, 'charge' => 18, 'total' => 19, 'created_by' => 20, 'created_date' => 21, 'modified_by' => 22, 'modified_date' => 23, 'uniqid' => 24, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('facility_booking');
        $this->setPhpName('FacilityBooking');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\FacilityBooking');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 15, null);
        $this->addColumn('booking_number', 'BookingNumber', 'VARCHAR', true, 15, null);
        $this->addColumn('facility_id', 'FacilityId', 'VARCHAR', true, 10, null);
        $this->addColumn('facility_name', 'FacilityName', 'VARCHAR', true, 50, null);
        $this->addColumn('booking_date', 'BookingDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('booking_time', 'BookingTime', 'VARCHAR', true, 100, null);
        $this->addColumn('unit_id', 'UnitId', 'VARCHAR', true, 20, null);
        $this->addColumn('booking_timeslot', 'BookingTimeslot', 'VARCHAR', false, 250, null);
        $this->addColumn('booking_owner', 'BookingOwner', 'VARCHAR', true, 100, null);
        $this->addColumn('project_id', 'ProjectId', 'VARCHAR', true, 15, null);
        $this->addColumn('tenant_id', 'TenantId', 'VARCHAR', true, 10, null);
        $this->addColumn('booking_status', 'BookingStatus', 'VARCHAR', true, 20, null);
        $this->addColumn('payment_status', 'PaymentStatus', 'VARCHAR', true, 20, null);
        $this->addColumn('payment_date', 'PaymentDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('cancel_date', 'CancelDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('cancel_reason', 'CancelReason', 'VARCHAR', false, 255, null);
        $this->addColumn('cancel_by', 'CancelBy', 'VARCHAR', false, 10, null);
        $this->addColumn('deposit', 'Deposit', 'DOUBLE', false, null, null);
        $this->addColumn('charge', 'Charge', 'DOUBLE', false, null, null);
        $this->addColumn('total', 'Total', 'DOUBLE', false, null, null);
        $this->addColumn('created_by', 'CreatedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('modified_by', 'ModifiedBy', 'VARCHAR', true, 10, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('uniqid', 'UniqId', 'VARCHAR', true, 30, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? FacilityBookingTableMap::CLASS_DEFAULT : FacilityBookingTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (FacilityBooking object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = FacilityBookingTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = FacilityBookingTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + FacilityBookingTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = FacilityBookingTableMap::OM_CLASS;
            /** @var FacilityBooking $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            FacilityBookingTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = FacilityBookingTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = FacilityBookingTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var FacilityBooking $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                FacilityBookingTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_ID);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_BOOKING_NUMBER);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_FACILITY_ID);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_FACILITY_NAME);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_BOOKING_DATE);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_BOOKING_TIME);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_UNIT_ID);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_BOOKING_TIMESLOT);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_BOOKING_OWNER);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_PROJECT_ID);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_TENANT_ID);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_BOOKING_STATUS);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_PAYMENT_STATUS);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_PAYMENT_DATE);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_CANCEL_DATE);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_CANCEL_REASON);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_CANCEL_BY);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_DEPOSIT);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_CHARGE);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_TOTAL);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_CREATED_BY);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_MODIFIED_BY);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_MODIFIED_DATE);
            $criteria->addSelectColumn(FacilityBookingTableMap::COL_UNIQID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.booking_number');
            $criteria->addSelectColumn($alias . '.facility_id');
            $criteria->addSelectColumn($alias . '.facility_name');
            $criteria->addSelectColumn($alias . '.booking_date');
            $criteria->addSelectColumn($alias . '.booking_time');
            $criteria->addSelectColumn($alias . '.unit_id');
            $criteria->addSelectColumn($alias . '.booking_timeslot');
            $criteria->addSelectColumn($alias . '.booking_owner');
            $criteria->addSelectColumn($alias . '.project_id');
            $criteria->addSelectColumn($alias . '.tenant_id');
            $criteria->addSelectColumn($alias . '.booking_status');
            $criteria->addSelectColumn($alias . '.payment_status');
            $criteria->addSelectColumn($alias . '.payment_date');
            $criteria->addSelectColumn($alias . '.cancel_date');
            $criteria->addSelectColumn($alias . '.cancel_reason');
            $criteria->addSelectColumn($alias . '.cancel_by');
            $criteria->addSelectColumn($alias . '.deposit');
            $criteria->addSelectColumn($alias . '.charge');
            $criteria->addSelectColumn($alias . '.total');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.modified_by');
            $criteria->addSelectColumn($alias . '.modified_date');
            $criteria->addSelectColumn($alias . '.uniqid');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(FacilityBookingTableMap::DATABASE_NAME)->getTable(FacilityBookingTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(FacilityBookingTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(FacilityBookingTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new FacilityBookingTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a FacilityBooking or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or FacilityBooking object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityBookingTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \FacilityBooking) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(FacilityBookingTableMap::DATABASE_NAME);
            $criteria->add(FacilityBookingTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = FacilityBookingQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            FacilityBookingTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                FacilityBookingTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the facility_booking table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return FacilityBookingQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a FacilityBooking or Criteria object.
     *
     * @param mixed               $criteria Criteria or FacilityBooking object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacilityBookingTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from FacilityBooking object
        }


        // Set the correct dbName
        $query = FacilityBookingQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // FacilityBookingTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
FacilityBookingTableMap::buildTableMap();
