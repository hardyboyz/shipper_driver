<?php

namespace Map;

use \Panel;
use \PanelQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'panel' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PanelTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PanelTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'panel';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Panel';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Panel';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the panel_id field
     */
    const COL_PANEL_ID = 'panel.panel_id';

    /**
     * the column name for the panel_type_id field
     */
    const COL_PANEL_TYPE_ID = 'panel.panel_type_id';

    /**
     * the column name for the project_id field
     */
    const COL_PROJECT_ID = 'panel.project_id';

    /**
     * the column name for the tenant_id field
     */
    const COL_TENANT_ID = 'panel.tenant_id';

    /**
     * the column name for the ip_address field
     */
    const COL_IP_ADDRESS = 'panel.ip_address';

    /**
     * the column name for the protocol field
     */
    const COL_PROTOCOL = 'panel.protocol';

    /**
     * the column name for the port field
     */
    const COL_PORT = 'panel.port';

    /**
     * the column name for the setting field
     */
    const COL_SETTING = 'panel.setting';

    /**
     * the column name for the setting_update_ts field
     */
    const COL_SETTING_UPDATE_TS = 'panel.setting_update_ts';

    /**
     * the column name for the checksum field
     */
    const COL_CHECKSUM = 'panel.checksum';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('PanelId', 'PanelTypeId', 'ProjectId', 'TenantId', 'IpAddress', 'Protocol', 'Port', 'Setting', 'SettingUpdateTs', 'Checksum', ),
        self::TYPE_CAMELNAME     => array('panelId', 'panelTypeId', 'projectId', 'tenantId', 'ipAddress', 'protocol', 'port', 'setting', 'settingUpdateTs', 'checksum', ),
        self::TYPE_COLNAME       => array(PanelTableMap::COL_PANEL_ID, PanelTableMap::COL_PANEL_TYPE_ID, PanelTableMap::COL_PROJECT_ID, PanelTableMap::COL_TENANT_ID, PanelTableMap::COL_IP_ADDRESS, PanelTableMap::COL_PROTOCOL, PanelTableMap::COL_PORT, PanelTableMap::COL_SETTING, PanelTableMap::COL_SETTING_UPDATE_TS, PanelTableMap::COL_CHECKSUM, ),
        self::TYPE_FIELDNAME     => array('panel_id', 'panel_type_id', 'project_id', 'tenant_id', 'ip_address', 'protocol', 'port', 'setting', 'setting_update_ts', 'checksum', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('PanelId' => 0, 'PanelTypeId' => 1, 'ProjectId' => 2, 'TenantId' => 3, 'IpAddress' => 4, 'Protocol' => 5, 'Port' => 6, 'Setting' => 7, 'SettingUpdateTs' => 8, 'Checksum' => 9, ),
        self::TYPE_CAMELNAME     => array('panelId' => 0, 'panelTypeId' => 1, 'projectId' => 2, 'tenantId' => 3, 'ipAddress' => 4, 'protocol' => 5, 'port' => 6, 'setting' => 7, 'settingUpdateTs' => 8, 'checksum' => 9, ),
        self::TYPE_COLNAME       => array(PanelTableMap::COL_PANEL_ID => 0, PanelTableMap::COL_PANEL_TYPE_ID => 1, PanelTableMap::COL_PROJECT_ID => 2, PanelTableMap::COL_TENANT_ID => 3, PanelTableMap::COL_IP_ADDRESS => 4, PanelTableMap::COL_PROTOCOL => 5, PanelTableMap::COL_PORT => 6, PanelTableMap::COL_SETTING => 7, PanelTableMap::COL_SETTING_UPDATE_TS => 8, PanelTableMap::COL_CHECKSUM => 9, ),
        self::TYPE_FIELDNAME     => array('panel_id' => 0, 'panel_type_id' => 1, 'project_id' => 2, 'tenant_id' => 3, 'ip_address' => 4, 'protocol' => 5, 'port' => 6, 'setting' => 7, 'setting_update_ts' => 8, 'checksum' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('panel');
        $this->setPhpName('Panel');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Panel');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('panel_id', 'PanelId', 'INTEGER', true, 5, null);
        $this->addForeignKey('panel_type_id', 'PanelTypeId', 'INTEGER', 'panel_type', 'id', true, 1, null);
        $this->addPrimaryKey('project_id', 'ProjectId', 'VARCHAR', true, 20, null);
        $this->addColumn('tenant_id', 'TenantId', 'VARCHAR', true, 20, null);
        $this->addColumn('ip_address', 'IpAddress', 'VARCHAR', true, 18, null);
        $this->addColumn('protocol', 'Protocol', 'VARCHAR', true, 10, null);
        $this->addColumn('port', 'Port', 'VARCHAR', true, 5, null);
        $this->addColumn('setting', 'Setting', 'LONGVARCHAR', false, null, null);
        $this->addColumn('setting_update_ts', 'SettingUpdateTs', 'TIMESTAMP', true, null, null);
        $this->addColumn('checksum', 'Checksum', 'INTEGER', true, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PanelType', '\\PanelType', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':panel_type_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Panel $obj A \Panel object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize(array((string) $obj->getPanelId(), (string) $obj->getProjectId()));
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Panel object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Panel) {
                $key = serialize(array((string) $value->getPanelId(), (string) $value->getProjectId()));

            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key";
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Panel object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('PanelId', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize(array((string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('PanelId', TableMap::TYPE_PHPNAME, $indexType)], (string) $row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('PanelId', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 2 + $offset
                : self::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PanelTableMap::CLASS_DEFAULT : PanelTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Panel object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PanelTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PanelTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PanelTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PanelTableMap::OM_CLASS;
            /** @var Panel $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PanelTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PanelTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PanelTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Panel $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PanelTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PanelTableMap::COL_PANEL_ID);
            $criteria->addSelectColumn(PanelTableMap::COL_PANEL_TYPE_ID);
            $criteria->addSelectColumn(PanelTableMap::COL_PROJECT_ID);
            $criteria->addSelectColumn(PanelTableMap::COL_TENANT_ID);
            $criteria->addSelectColumn(PanelTableMap::COL_IP_ADDRESS);
            $criteria->addSelectColumn(PanelTableMap::COL_PROTOCOL);
            $criteria->addSelectColumn(PanelTableMap::COL_PORT);
            $criteria->addSelectColumn(PanelTableMap::COL_SETTING);
            $criteria->addSelectColumn(PanelTableMap::COL_SETTING_UPDATE_TS);
            $criteria->addSelectColumn(PanelTableMap::COL_CHECKSUM);
        } else {
            $criteria->addSelectColumn($alias . '.panel_id');
            $criteria->addSelectColumn($alias . '.panel_type_id');
            $criteria->addSelectColumn($alias . '.project_id');
            $criteria->addSelectColumn($alias . '.tenant_id');
            $criteria->addSelectColumn($alias . '.ip_address');
            $criteria->addSelectColumn($alias . '.protocol');
            $criteria->addSelectColumn($alias . '.port');
            $criteria->addSelectColumn($alias . '.setting');
            $criteria->addSelectColumn($alias . '.setting_update_ts');
            $criteria->addSelectColumn($alias . '.checksum');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PanelTableMap::DATABASE_NAME)->getTable(PanelTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PanelTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PanelTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PanelTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Panel or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Panel object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PanelTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Panel) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PanelTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(PanelTableMap::COL_PANEL_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(PanelTableMap::COL_PROJECT_ID, $value[1]));
                $criteria->addOr($criterion);
            }
        }

        $query = PanelQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PanelTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PanelTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the panel table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PanelQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Panel or Criteria object.
     *
     * @param mixed               $criteria Criteria or Panel object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PanelTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Panel object
        }

        if ($criteria->containsKey(PanelTableMap::COL_PANEL_ID) && $criteria->keyContainsValue(PanelTableMap::COL_PANEL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PanelTableMap::COL_PANEL_ID.')');
        }


        // Set the correct dbName
        $query = PanelQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PanelTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PanelTableMap::buildTableMap();
