<?php

namespace Map;

use \AppClientSetting;
use \AppClientSettingQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'appClientSetting' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class AppClientSettingTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.AppClientSettingTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'appClientSetting';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AppClientSetting';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'AppClientSetting';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 32;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 32;

    /**
     * the column name for the id field
     */
    const COL_ID = 'appClientSetting.id';

    /**
     * the column name for the projectId field
     */
    const COL_PROJECTID = 'appClientSetting.projectId';

    /**
     * the column name for the appHomeIdleTime field
     */
    const COL_APPHOMEIDLETIME = 'appClientSetting.appHomeIdleTime';

    /**
     * the column name for the appStandbyIdleTime field
     */
    const COL_APPSTANDBYIDLETIME = 'appClientSetting.appStandbyIdleTime';

    /**
     * the column name for the appHomeLogoImage field
     */
    const COL_APPHOMELOGOIMAGE = 'appClientSetting.appHomeLogoImage';

    /**
     * the column name for the receiptLogoImage field
     */
    const COL_RECEIPTLOGOIMAGE = 'appClientSetting.receiptLogoImage';

    /**
     * the column name for the facilityCheckingOnlyFlag field
     */
    const COL_FACILITYCHECKINGONLYFLAG = 'appClientSetting.facilityCheckingOnlyFlag';

    /**
     * the column name for the callingFormatPattern field
     */
    const COL_CALLINGFORMATPATTERN = 'appClientSetting.callingFormatPattern';

    /**
     * the column name for the interFacilityFlag field
     */
    const COL_INTERFACILITYFLAG = 'appClientSetting.interFacilityFlag';

    /**
     * the column name for the projectName field
     */
    const COL_PROJECTNAME = 'appClientSetting.projectName';

    /**
     * the column name for the projectNameId field
     */
    const COL_PROJECTNAMEID = 'appClientSetting.projectNameId';

    /**
     * the column name for the centralizedServerUrl field
     */
    const COL_CENTRALIZEDSERVERURL = 'appClientSetting.centralizedServerUrl';

    /**
     * the column name for the remoteServerDomain field
     */
    const COL_REMOTESERVERDOMAIN = 'appClientSetting.remoteServerDomain';

    /**
     * the column name for the remoteServerProtocol field
     */
    const COL_REMOTESERVERPROTOCOL = 'appClientSetting.remoteServerProtocol';

    /**
     * the column name for the remoteServerPort field
     */
    const COL_REMOTESERVERPORT = 'appClientSetting.remoteServerPort';

    /**
     * the column name for the localServerDomain field
     */
    const COL_LOCALSERVERDOMAIN = 'appClientSetting.localServerDomain';

    /**
     * the column name for the localServerProtocol field
     */
    const COL_LOCALSERVERPROTOCOL = 'appClientSetting.localServerProtocol';

    /**
     * the column name for the localServerPort field
     */
    const COL_LOCALSERVERPORT = 'appClientSetting.localServerPort';

    /**
     * the column name for the pnDomain field
     */
    const COL_PNDOMAIN = 'appClientSetting.pnDomain';

    /**
     * the column name for the pnProtocol field
     */
    const COL_PNPROTOCOL = 'appClientSetting.pnProtocol';

    /**
     * the column name for the pnPort field
     */
    const COL_PNPORT = 'appClientSetting.pnPort';

    /**
     * the column name for the remoteCctvDomain field
     */
    const COL_REMOTECCTVDOMAIN = 'appClientSetting.remoteCctvDomain';

    /**
     * the column name for the remoteCctvProtocol field
     */
    const COL_REMOTECCTVPROTOCOL = 'appClientSetting.remoteCctvProtocol';

    /**
     * the column name for the remoteCctvPort field
     */
    const COL_REMOTECCTVPORT = 'appClientSetting.remoteCctvPort';

    /**
     * the column name for the localCctvDomain field
     */
    const COL_LOCALCCTVDOMAIN = 'appClientSetting.localCctvDomain';

    /**
     * the column name for the localCctvProtocol field
     */
    const COL_LOCALCCTVPROTOCOL = 'appClientSetting.localCctvProtocol';

    /**
     * the column name for the localCctvPort field
     */
    const COL_LOCALCCTVPORT = 'appClientSetting.localCctvPort';

    /**
     * the column name for the tutorialImage field
     */
    const COL_TUTORIALIMAGE = 'appClientSetting.tutorialImage';

    /**
     * the column name for the tutorialImageEnable field
     */
    const COL_TUTORIALIMAGEENABLE = 'appClientSetting.tutorialImageEnable';

    /**
     * the column name for the appAutoSyncInterval field
     */
    const COL_APPAUTOSYNCINTERVAL = 'appClientSetting.appAutoSyncInterval';

    /**
     * the column name for the modified_by field
     */
    const COL_MODIFIED_BY = 'appClientSetting.modified_by';

    /**
     * the column name for the modified_date field
     */
    const COL_MODIFIED_DATE = 'appClientSetting.modified_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'AppHomeIdleTime', 'AppStandbyIdleTime', 'AppHomeLogoImage', 'ReceiptLogoImage', 'FacilityCheckingOnlyFlag', 'CallingFormatPattern', 'interFacilityFlag', 'ProjectName', 'ProjectNameId', 'CentralizedServerUrl', 'RemoteServerDomain', 'RemoteServerProtocol', 'RemoteServerPort', 'LocalServerDomain', 'LocalServerProtocol', 'LocalServerPort', 'PNDomain', 'PNProtocol', 'PNPort', 'RemoteCctvDomain', 'RemoteCctvProtocol', 'RemoteCctvPort', 'LocalCctvDomain', 'LocalCctvProtocol', 'LocalCctvPort', 'TutorialImage', 'TutorialImageEnable', 'AppAutoSyncInterval', 'ModifiedBy', 'ModifiedDate', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'appHomeIdleTime', 'appStandbyIdleTime', 'appHomeLogoImage', 'receiptLogoImage', 'facilityCheckingOnlyFlag', 'callingFormatPattern', 'interFacilityFlag', 'projectName', 'projectNameId', 'centralizedServerUrl', 'remoteServerDomain', 'remoteServerProtocol', 'remoteServerPort', 'localServerDomain', 'localServerProtocol', 'localServerPort', 'pNDomain', 'pNProtocol', 'pNPort', 'remoteCctvDomain', 'remoteCctvProtocol', 'remoteCctvPort', 'localCctvDomain', 'localCctvProtocol', 'localCctvPort', 'tutorialImage', 'tutorialImageEnable', 'appAutoSyncInterval', 'modifiedBy', 'modifiedDate', ),
        self::TYPE_COLNAME       => array(AppClientSettingTableMap::COL_ID, AppClientSettingTableMap::COL_PROJECTID, AppClientSettingTableMap::COL_APPHOMEIDLETIME, AppClientSettingTableMap::COL_APPSTANDBYIDLETIME, AppClientSettingTableMap::COL_APPHOMELOGOIMAGE, AppClientSettingTableMap::COL_RECEIPTLOGOIMAGE, AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG, AppClientSettingTableMap::COL_CALLINGFORMATPATTERN, AppClientSettingTableMap::COL_INTERFACILITYFLAG, AppClientSettingTableMap::COL_PROJECTNAME, AppClientSettingTableMap::COL_PROJECTNAMEID, AppClientSettingTableMap::COL_CENTRALIZEDSERVERURL, AppClientSettingTableMap::COL_REMOTESERVERDOMAIN, AppClientSettingTableMap::COL_REMOTESERVERPROTOCOL, AppClientSettingTableMap::COL_REMOTESERVERPORT, AppClientSettingTableMap::COL_LOCALSERVERDOMAIN, AppClientSettingTableMap::COL_LOCALSERVERPROTOCOL, AppClientSettingTableMap::COL_LOCALSERVERPORT, AppClientSettingTableMap::COL_PNDOMAIN, AppClientSettingTableMap::COL_PNPROTOCOL, AppClientSettingTableMap::COL_PNPORT, AppClientSettingTableMap::COL_REMOTECCTVDOMAIN, AppClientSettingTableMap::COL_REMOTECCTVPROTOCOL, AppClientSettingTableMap::COL_REMOTECCTVPORT, AppClientSettingTableMap::COL_LOCALCCTVDOMAIN, AppClientSettingTableMap::COL_LOCALCCTVPROTOCOL, AppClientSettingTableMap::COL_LOCALCCTVPORT, AppClientSettingTableMap::COL_TUTORIALIMAGE, AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE, AppClientSettingTableMap::COL_APPAUTOSYNCINTERVAL, AppClientSettingTableMap::COL_MODIFIED_BY, AppClientSettingTableMap::COL_MODIFIED_DATE, ),
        self::TYPE_FIELDNAME     => array('id', 'projectId', 'appHomeIdleTime', 'appStandbyIdleTime', 'appHomeLogoImage', 'receiptLogoImage', 'facilityCheckingOnlyFlag', 'callingFormatPattern', 'interFacilityFlag', 'projectName', 'projectNameId', 'centralizedServerUrl', 'remoteServerDomain', 'remoteServerProtocol', 'remoteServerPort', 'localServerDomain', 'localServerProtocol', 'localServerPort', 'pnDomain', 'pnProtocol', 'pnPort', 'remoteCctvDomain', 'remoteCctvProtocol', 'remoteCctvPort', 'localCctvDomain', 'localCctvProtocol', 'localCctvPort', 'tutorialImage', 'tutorialImageEnable', 'appAutoSyncInterval', 'modified_by', 'modified_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'AppHomeIdleTime' => 2, 'AppStandbyIdleTime' => 3, 'AppHomeLogoImage' => 4, 'ReceiptLogoImage' => 5, 'FacilityCheckingOnlyFlag' => 6, 'CallingFormatPattern' => 7, 'interFacilityFlag' => 8, 'ProjectName' => 9, 'ProjectNameId' => 10, 'CentralizedServerUrl' => 11, 'RemoteServerDomain' => 12, 'RemoteServerProtocol' => 13, 'RemoteServerPort' => 14, 'LocalServerDomain' => 15, 'LocalServerProtocol' => 16, 'LocalServerPort' => 17, 'PNDomain' => 18, 'PNProtocol' => 19, 'PNPort' => 20, 'RemoteCctvDomain' => 21, 'RemoteCctvProtocol' => 22, 'RemoteCctvPort' => 23, 'LocalCctvDomain' => 24, 'LocalCctvProtocol' => 25, 'LocalCctvPort' => 26, 'TutorialImage' => 27, 'TutorialImageEnable' => 28, 'AppAutoSyncInterval' => 29, 'ModifiedBy' => 30, 'ModifiedDate' => 31, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'appHomeIdleTime' => 2, 'appStandbyIdleTime' => 3, 'appHomeLogoImage' => 4, 'receiptLogoImage' => 5, 'facilityCheckingOnlyFlag' => 6, 'callingFormatPattern' => 7, 'interFacilityFlag' => 8, 'projectName' => 9, 'projectNameId' => 10, 'centralizedServerUrl' => 11, 'remoteServerDomain' => 12, 'remoteServerProtocol' => 13, 'remoteServerPort' => 14, 'localServerDomain' => 15, 'localServerProtocol' => 16, 'localServerPort' => 17, 'pNDomain' => 18, 'pNProtocol' => 19, 'pNPort' => 20, 'remoteCctvDomain' => 21, 'remoteCctvProtocol' => 22, 'remoteCctvPort' => 23, 'localCctvDomain' => 24, 'localCctvProtocol' => 25, 'localCctvPort' => 26, 'tutorialImage' => 27, 'tutorialImageEnable' => 28, 'appAutoSyncInterval' => 29, 'modifiedBy' => 30, 'modifiedDate' => 31, ),
        self::TYPE_COLNAME       => array(AppClientSettingTableMap::COL_ID => 0, AppClientSettingTableMap::COL_PROJECTID => 1, AppClientSettingTableMap::COL_APPHOMEIDLETIME => 2, AppClientSettingTableMap::COL_APPSTANDBYIDLETIME => 3, AppClientSettingTableMap::COL_APPHOMELOGOIMAGE => 4, AppClientSettingTableMap::COL_RECEIPTLOGOIMAGE => 5, AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG => 6, AppClientSettingTableMap::COL_CALLINGFORMATPATTERN => 7, AppClientSettingTableMap::COL_INTERFACILITYFLAG => 8, AppClientSettingTableMap::COL_PROJECTNAME => 9, AppClientSettingTableMap::COL_PROJECTNAMEID => 10, AppClientSettingTableMap::COL_CENTRALIZEDSERVERURL => 11, AppClientSettingTableMap::COL_REMOTESERVERDOMAIN => 12, AppClientSettingTableMap::COL_REMOTESERVERPROTOCOL => 13, AppClientSettingTableMap::COL_REMOTESERVERPORT => 14, AppClientSettingTableMap::COL_LOCALSERVERDOMAIN => 15, AppClientSettingTableMap::COL_LOCALSERVERPROTOCOL => 16, AppClientSettingTableMap::COL_LOCALSERVERPORT => 17, AppClientSettingTableMap::COL_PNDOMAIN => 18, AppClientSettingTableMap::COL_PNPROTOCOL => 19, AppClientSettingTableMap::COL_PNPORT => 20, AppClientSettingTableMap::COL_REMOTECCTVDOMAIN => 21, AppClientSettingTableMap::COL_REMOTECCTVPROTOCOL => 22, AppClientSettingTableMap::COL_REMOTECCTVPORT => 23, AppClientSettingTableMap::COL_LOCALCCTVDOMAIN => 24, AppClientSettingTableMap::COL_LOCALCCTVPROTOCOL => 25, AppClientSettingTableMap::COL_LOCALCCTVPORT => 26, AppClientSettingTableMap::COL_TUTORIALIMAGE => 27, AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE => 28, AppClientSettingTableMap::COL_APPAUTOSYNCINTERVAL => 29, AppClientSettingTableMap::COL_MODIFIED_BY => 30, AppClientSettingTableMap::COL_MODIFIED_DATE => 31, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'projectId' => 1, 'appHomeIdleTime' => 2, 'appStandbyIdleTime' => 3, 'appHomeLogoImage' => 4, 'receiptLogoImage' => 5, 'facilityCheckingOnlyFlag' => 6, 'callingFormatPattern' => 7, 'interFacilityFlag' => 8, 'projectName' => 9, 'projectNameId' => 10, 'centralizedServerUrl' => 11, 'remoteServerDomain' => 12, 'remoteServerProtocol' => 13, 'remoteServerPort' => 14, 'localServerDomain' => 15, 'localServerProtocol' => 16, 'localServerPort' => 17, 'pnDomain' => 18, 'pnProtocol' => 19, 'pnPort' => 20, 'remoteCctvDomain' => 21, 'remoteCctvProtocol' => 22, 'remoteCctvPort' => 23, 'localCctvDomain' => 24, 'localCctvProtocol' => 25, 'localCctvPort' => 26, 'tutorialImage' => 27, 'tutorialImageEnable' => 28, 'appAutoSyncInterval' => 29, 'modified_by' => 30, 'modified_date' => 31, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('appClientSetting');
        $this->setPhpName('AppClientSetting');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\AppClientSetting');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 15, null);
        $this->addColumn('projectId', 'ProjectId', 'VARCHAR', true, 15, null);
        $this->addColumn('appHomeIdleTime', 'AppHomeIdleTime', 'INTEGER', false, 11, 0);
        $this->addColumn('appStandbyIdleTime', 'AppStandbyIdleTime', 'INTEGER', false, 11, 0);
        $this->addColumn('appHomeLogoImage', 'AppHomeLogoImage', 'VARCHAR', false, 255, null);
        $this->addColumn('receiptLogoImage', 'ReceiptLogoImage', 'VARCHAR', false, 255, null);
        $this->addColumn('facilityCheckingOnlyFlag', 'FacilityCheckingOnlyFlag', 'INTEGER', false, 1, 0);
        $this->addColumn('callingFormatPattern', 'CallingFormatPattern', 'VARCHAR', false, 30, null);
        $this->addColumn('interFacilityFlag', 'interFacilityFlag', 'INTEGER', false, 1, 0);
        $this->addColumn('projectName', 'ProjectName', 'VARCHAR', false, 30, null);
        $this->addColumn('projectNameId', 'ProjectNameId', 'VARCHAR', false, 30, null);
        $this->addColumn('centralizedServerUrl', 'CentralizedServerUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('remoteServerDomain', 'RemoteServerDomain', 'VARCHAR', false, 255, null);
        $this->addColumn('remoteServerProtocol', 'RemoteServerProtocol', 'VARCHAR', false, 10, null);
        $this->addColumn('remoteServerPort', 'RemoteServerPort', 'VARCHAR', false, 5, null);
        $this->addColumn('localServerDomain', 'LocalServerDomain', 'VARCHAR', false, 255, null);
        $this->addColumn('localServerProtocol', 'LocalServerProtocol', 'VARCHAR', false, 10, null);
        $this->addColumn('localServerPort', 'LocalServerPort', 'VARCHAR', false, 5, null);
        $this->addColumn('pnDomain', 'PNDomain', 'VARCHAR', false, 255, null);
        $this->addColumn('pnProtocol', 'PNProtocol', 'VARCHAR', false, 10, null);
        $this->addColumn('pnPort', 'PNPort', 'VARCHAR', false, 6, null);
        $this->addColumn('remoteCctvDomain', 'RemoteCctvDomain', 'VARCHAR', false, 255, null);
        $this->addColumn('remoteCctvProtocol', 'RemoteCctvProtocol', 'VARCHAR', false, 10, null);
        $this->addColumn('remoteCctvPort', 'RemoteCctvPort', 'VARCHAR', false, 5, null);
        $this->addColumn('localCctvDomain', 'LocalCctvDomain', 'VARCHAR', false, 255, null);
        $this->addColumn('localCctvProtocol', 'LocalCctvProtocol', 'VARCHAR', false, 10, null);
        $this->addColumn('localCctvPort', 'LocalCctvPort', 'VARCHAR', false, 5, null);
        $this->addColumn('tutorialImage', 'TutorialImage', 'VARCHAR', false, 255, null);
        $this->addColumn('tutorialImageEnable', 'TutorialImageEnable', 'INTEGER', false, 1, 0);
        $this->addColumn('appAutoSyncInterval', 'AppAutoSyncInterval', 'VARCHAR', false, 10, null);
        $this->addColumn('modified_by', 'ModifiedBy', 'VARCHAR', false, 10, null);
        $this->addColumn('modified_date', 'ModifiedDate', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AppClientSettingTableMap::CLASS_DEFAULT : AppClientSettingTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AppClientSetting object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AppClientSettingTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AppClientSettingTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AppClientSettingTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AppClientSettingTableMap::OM_CLASS;
            /** @var AppClientSetting $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AppClientSettingTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AppClientSettingTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AppClientSettingTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AppClientSetting $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AppClientSettingTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_ID);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_PROJECTID);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_APPHOMEIDLETIME);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_APPSTANDBYIDLETIME);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_APPHOMELOGOIMAGE);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_RECEIPTLOGOIMAGE);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_FACILITYCHECKINGONLYFLAG);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_CALLINGFORMATPATTERN);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_INTERFACILITYFLAG);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_PROJECTNAME);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_PROJECTNAMEID);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_CENTRALIZEDSERVERURL);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_REMOTESERVERDOMAIN);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_REMOTESERVERPROTOCOL);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_REMOTESERVERPORT);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_LOCALSERVERDOMAIN);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_LOCALSERVERPROTOCOL);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_LOCALSERVERPORT);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_PNDOMAIN);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_PNPROTOCOL);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_PNPORT);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_REMOTECCTVDOMAIN);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_REMOTECCTVPROTOCOL);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_REMOTECCTVPORT);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_LOCALCCTVDOMAIN);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_LOCALCCTVPROTOCOL);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_LOCALCCTVPORT);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_TUTORIALIMAGE);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_TUTORIALIMAGEENABLE);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_APPAUTOSYNCINTERVAL);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_MODIFIED_BY);
            $criteria->addSelectColumn(AppClientSettingTableMap::COL_MODIFIED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.projectId');
            $criteria->addSelectColumn($alias . '.appHomeIdleTime');
            $criteria->addSelectColumn($alias . '.appStandbyIdleTime');
            $criteria->addSelectColumn($alias . '.appHomeLogoImage');
            $criteria->addSelectColumn($alias . '.receiptLogoImage');
            $criteria->addSelectColumn($alias . '.facilityCheckingOnlyFlag');
            $criteria->addSelectColumn($alias . '.callingFormatPattern');
            $criteria->addSelectColumn($alias . '.interFacilityFlag');
            $criteria->addSelectColumn($alias . '.projectName');
            $criteria->addSelectColumn($alias . '.projectNameId');
            $criteria->addSelectColumn($alias . '.centralizedServerUrl');
            $criteria->addSelectColumn($alias . '.remoteServerDomain');
            $criteria->addSelectColumn($alias . '.remoteServerProtocol');
            $criteria->addSelectColumn($alias . '.remoteServerPort');
            $criteria->addSelectColumn($alias . '.localServerDomain');
            $criteria->addSelectColumn($alias . '.localServerProtocol');
            $criteria->addSelectColumn($alias . '.localServerPort');
            $criteria->addSelectColumn($alias . '.pnDomain');
            $criteria->addSelectColumn($alias . '.pnProtocol');
            $criteria->addSelectColumn($alias . '.pnPort');
            $criteria->addSelectColumn($alias . '.remoteCctvDomain');
            $criteria->addSelectColumn($alias . '.remoteCctvProtocol');
            $criteria->addSelectColumn($alias . '.remoteCctvPort');
            $criteria->addSelectColumn($alias . '.localCctvDomain');
            $criteria->addSelectColumn($alias . '.localCctvProtocol');
            $criteria->addSelectColumn($alias . '.localCctvPort');
            $criteria->addSelectColumn($alias . '.tutorialImage');
            $criteria->addSelectColumn($alias . '.tutorialImageEnable');
            $criteria->addSelectColumn($alias . '.appAutoSyncInterval');
            $criteria->addSelectColumn($alias . '.modified_by');
            $criteria->addSelectColumn($alias . '.modified_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AppClientSettingTableMap::DATABASE_NAME)->getTable(AppClientSettingTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AppClientSettingTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AppClientSettingTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AppClientSettingTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AppClientSetting or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AppClientSetting object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppClientSettingTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AppClientSetting) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AppClientSettingTableMap::DATABASE_NAME);
            $criteria->add(AppClientSettingTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = AppClientSettingQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AppClientSettingTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AppClientSettingTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the appClientSetting table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AppClientSettingQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AppClientSetting or Criteria object.
     *
     * @param mixed               $criteria Criteria or AppClientSetting object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppClientSettingTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AppClientSetting object
        }


        // Set the correct dbName
        $query = AppClientSettingQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AppClientSettingTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AppClientSettingTableMap::buildTableMap();
