<?php

use Base\ShAccountQuery as BaseShAccountQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'sh_account' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ShAccountQuery extends BaseShAccountQuery
{
	/*
	 * @author HardyBoyz
	* @since 1.0.0 getSaltByUsername
	* @param string $username username
	* @return string base64 encoded salt, null is no salt return
	* */
	static function getSaltByUsername($username){
		$objAccount = self::create()
		->filterByID($username)
		->limit(1)->find();
		$arrAcount = $objAccount->getData();
		
		if((sizeof($arrAcount) > 0)){
			$objData = $arrAcount[0];
			return $objData->getSalt();
		} else {
			return null;
		}
		
	}
	/*
	* @author HardyBoyz
	* @since 1.0.0 getAccountByUsernamePassword
	* @param string $username username
	* @param string $password hash password, base64 encoded
	* @return mixed[] ShAccount
	* */
	static function getAccountByUsernamePassword($username,$password){
		$objAccount = self::create()
		->filterByID($username)
		->filterByHashPassword($password)
		->limit(1)->find();
		$arrAcount = $objAccount->getData();
		if((sizeof($arrAcount) > 0)){
			return $arrAcount[0];
		} else {
			return new stdClass();
		}
	}
	/*
	 * @author HardyBoyz
	* @since 1.0.0 getAccountByUsernameToken
	* @param string $username username
	* @param string $token session token
	* @return mixed[] ShAccount
	* */
	static function getAccountByUsernameToken($username,$token){
		$objAccount = self::create()
		->filterByID($username)
		->filterByToken($token)
		->limit(1)->find();
		$arrAcount = $objAccount->getData();
		if((sizeof($arrAcount) > 0)){
			return $arrAcount[0];
		} else {
			return new stdClass();
		}
	}
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 updateToken
	* @param string $userID username
	* @param string $token 
	* @param timestamp $loginTS 
	* @param timestamp $expireTS 
	* @return mixed[] ShAccount
	* */
	static function updateToken($userID,$token,$loginTS, $expireTS){
		$objAccount = self::create()
		->findOneByID($userID);
		$objAccount->setToken($token);
		$objAccount->setLoginTs($loginTS);
		$objAccount->setExpireTs($expireTS);
		$objAccount->save();
		
		return $objAccount;
	}
	
	
	//status 0 = not available, 1=available, 2=disabled
	static function updateStatus($id, $status){
		$objAccount = self::create()->findOneByID($id);
		$objAccount->setStatus($status);
		$objAccount->save();
		
		return $objAccount;
	}
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 updateImei
	* @param string $userID username
	* @param string $imei 
	* @return mixed[] ShAccount
	* */
	static function updateImei($userID,$imei){
		$objAccount = self::create()->findOneByID($userID);
		$objAccount->setDeviceID($imei);
		$objAccount->save();
	}
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 enablePN
	* @param string $userID username
	* @param string $imei 
	* @return mixed[] ShAccount
	* */
	static function enablePN($userID,$imei, $enable){
		$objAccount = self::create()->findOneByID($userID);
		$objAccount->setDeviceID($imei);
		$objAccount->setEnablePN($enable);
		$objAccount->save();
	}
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 statusPN
	* @param string $userID username
	* @param string $imei 
	* @return mixed[] ShAccount
	* */
	static function statusPN($userID){
		try{
				
			$objAccount = self::create()->filterByID($userID)->findOne();
			return $objAccount->getEnablePN();
		
		}catch (exception $e){
			return $e->getMessage();
		}
	}
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 getImei
	* @param string $imei 
	* @return mixed[] ShAccount
	* */
	static function getImei($users = null, $unitId = null){
		try{
			if(!is_null($unitId)){
				
				$objAccount = self::create()->where('CASE WHEN LENGTH(id) = 11 THEN SUBSTRING(id,4,6)="'.$unitId.'" ELSE SUBSTRING(id,4,7)="'.$unitId.'" END ')->find();
				
				$arrImei[] = $objAccount->toArray();
				
				//print_r($arrImei);exit;
				/*foreach ($arrImei as $imei){
					$aImei[] = $imei['DeviceID'];
				}*/
				foreach ($arrImei as $imei){
					foreach ($imei as $im){
						$aImei[] = $im['DeviceID'];
					}
				}
				
			}else{
				foreach($users as $q => $v){
					$tenant = substr($v->getUserId(),0,-2);
					$objAccount = self::create()->where('CASE WHEN LENGTH(id) = 11 THEN SUBSTRING(id,1,9)="'.$tenant.'" ELSE SUBSTRING(id,1,10)="'.$tenant.'" END ')->find();
					
					$arrImei[] = $objAccount->toArray();
				}
				
				//print_r($arrImei);exit;
				foreach ($arrImei as $imei){
					foreach ($imei as $im){
						$aImei[] = $im['DeviceID'];
					}
				}
			}
			
			$imei = implode("|", $aImei);
			
			return $imei;
		
		}catch (exception $e){
			return $e->getMessage();
		}
	}
	
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 updateImei
	* @param string $userID username
	* @param string $imei 
	* @return mixed[] ShAccount
	* */
	static function isLogin($userID){
		$objAccount = self::create()->filterByID($userID)->findOne();
		return $objAccount;
	}
	
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 getImeiByUsername
	* @param string $users 
	* @return mixed[] DeviceID
	* */
	static function getImeiByUsername($users){
		try{
				
			$objAccount = self::create()->filterByID($users)->findOne();
			return $objAccount->getDeviceID();
		
		}catch (exception $e){
			return $e->getMessage();
		}
	}
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 getImeiByUsername
	* @param string $users 
	* @return mixed[] DeviceID
	* */
	static function checkImei($imei, $user){
		try{
				
			$objAccount = self::create()->filterByDeviceID($imei)->where("id != '".$user."'")->findOne();
			return $objAccount;
		
		}catch (exception $e){
			return $e->getMessage();
		}
	}
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 getUserList
	* @return mixed[] 
	* */
	static function getDriverList(){
		try{
				
			$objAccount = self::create()->filterByStatus(1)->find();
			return $objAccount->toArray();
		
		}catch (exception $e){
			return $e->getMessage();
		}
	}
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 getDriver
	* @return mixed[] 
	* */
	static function getDriver($id){
		try{
				
			$objAccount = self::create()->filterByStatus(1)->filterByID($id)->findOne();
			if(is_object($objAccount)){
				return $objAccount->toArray();
			}else{
				return false;
			}
		
		}catch (\exception $e){
			return $e->getMessage();
		}
	}
	
	/*
	 * @author HardyBoyz
	* @since 1.0.0 updateAvailability
	* @return mixed[] 
	* */
	static function updateAvailability($data, $header){
		try{
				
			$objAccount = self::create()->filterByID($header->username)->findOne();
			//print_r($objAccount);exit;
			$objAccount->setStatus($data->status);
			$objAccount->save();
			
			if(is_object($objAccount)){
				return true;
			}else{
				return false;
			}
		
		}catch (\Exception $e){
			return $e->getMessage();
		}
	}
}
